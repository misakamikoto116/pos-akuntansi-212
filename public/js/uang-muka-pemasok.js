var base_url = window.location.origin + '/akuntansi';
var token_ = $("meta[name='csrf-token']").attr("content");

function enable_cb() {
  if ($("#group1").is(':checked')) {
    $("input.group2").removeAttr("disabled");
  } else {
    $("input.group2").attr("disabled", true).removeAttr("checked");
  }
}

 function sumQtyAndUnit(){
    var amount  = 0;
    let disc    = 0;

    amount = parseInt($('#qty_produk').val()) * parseInt(changeMaskingToNumber($('#unit_harga_produk').val()));

    if (isNaN($('#diskon_produk').val()) || $('#diskon_produk').val() == '') {
    } else {
        disc = amount * parseFloat($('#diskon_produk').val()) / 100;
    }
    var final_amount = amount - disc;
    $('#amount_produk').val(final_amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
    calculate();
}

function calculate(){
    var subTotal     = 0;
    var grandtotal   = 0;
    var diskon       = 0;
    var tax          = 0;
    var in_tax       = 0;
    var amount       = [];
    var sorting      = [];
    // $(".amount_produk").autoNumeric('init');
    $('#taxNaration').html('');

    $(".amount_produk").each(function() {
        amount.push([changeMaskingToNumber($(this).val())]);
    });

    $.each(amount, function(index, key){
        var total_tax = 0;
        $('#tax_produk option:selected').each(function(){
            var str = $(this).text();
            var spl = str.split("/");
            total_tax += parseFloat(spl[0].replace(/ [0-9]*/, ''));
        });
        $('#tax_produk option:selected').each(function(){
            var str = $(this).text();
            var spl = str.split("/");
            let temp_tax = (100 / (100 + total_tax) * parseFloat(key));
            var sort_temp = {
                label : spl[1].replace(/ [0-9]*/, ''),
                nilai : spl[0].replace(/ [0-9]*/, ''),
                total : parseFloat(key) * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100,
                total_in_tax : temp_tax * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100
            }
            sorting.push(sort_temp);                  
        });
    });
    var sorted = sorting.sort(function(a, b) {
        return a.nilai - b.nilai;
    })

    var sorted_temp = sorted; 
    var unduplicateItem = [];
    for (var i = 0; i < sorted_temp.length; i++) {            
        if(sorted_temp.length >= 2){
            if(sorted_temp[i + 1] != null){
                if(sorted_temp[i]['nilai'] != sorted_temp[i + 1]['nilai'] && sorted_temp[i]['label'] != sorted_temp[i + 1]['label']){
                    unduplicateItem.push(sorted_temp[i]);
                }else{
                    sorted_temp[i + 1]['total'] = sorted_temp[i]['total'] + sorted_temp[i + 1]['total']; 
                    sorted_temp[i + 1]['total_in_tax'] = sorted_temp[i]['total_in_tax'] + sorted_temp[i + 1]['total_in_tax']; 
                }
            }else{
                unduplicateItem.push(sorted_temp[i]);                                        
            }
        }else{
            unduplicateItem.push(sorted_temp[i]);                    
        }
    }

    $(".amount_produk").each(function() {
        subTotal += parseInt(changeMaskingToNumber($(this).val()));
        amount[$(this).index()].push(changeMaskingToNumber($(this).val()));
    });
    $.each(unduplicateItem, function(index, key){
        if($('input.in_tax').is(':checked')){
            tax += key['total_in_tax'];
            $("#taxNaration").append(key['label']+" "+key['nilai']+"% = "+key['total_in_tax'].toFixed(2)+"<br>");            
        }else{
            tax += key['total'];
            $("#taxNaration").append(key['label']+" "+key['nilai']+"% = "+key['total'].toFixed(2)+"<br>");
        }
    });

    if($('input.taxable').is(':checked')){
        $('.tax_produk').attr('disabled',false);  
        $(".tax_produk").select2({
            maximumSelectionLength: 2
        }); 
        tax = tax;
        $('#taxNaration').show();
        if($('input.in_tax').is(':checked')){
            tax = 0;
            //balik lagi 0 karena harga sudah sama ongkir
        }
    }else{
        $('.tax_produk').attr('disabled',true);   
        $('#taxNaration').hide();
        tax = 0;
    }

    diskon = subTotal * $('#diskonTotal').val() / 100;
    grandtotal = subTotal - diskon + tax;

    $('#subtotal').html("<strong>" + Number(subTotal).toLocaleString() +"</strong>");
    $('.grandtotal').html("<strong>" + Number(grandtotal).toLocaleString() +"</strong>");
    $('.kolom_grandtotal').val(grandtotal);
    $('#totalPotonganRupiah').val(diskon);

}

function sortPajakPemasok(id) {
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-sort-pajak",
        data: {
            id : id, type: 'pemasok',
            _token: token_,
        },
        dataType: "json",
        success: function (response) {
            $('#tax_produk').find('option').remove().end();
            if (response.kode_pajak) {
                $.each(response.kode_pajak, function (index, item){
                    if (old_kode_pajak_id !== null) {
                        if (item.id == old_kode_pajak_id[0] || item.id == old_kode_pajak_id[1]) {
                            $('#tax_produk').append( 
                                $("<option></option>")
                                .text(item.nilai+ "/" +item.nama)
                                .val(item.id)
                                .attr('selected','selected')
                            );
                        }else {
                            $('#tax_produk').append( 
                                $("<option></option>")
                                .text(item.nilai+ "/" +item.nama)
                                .val(item.id)
                            );        
                        }
                    }else {
                        $('#tax_produk').append( 
                            $("<option></option>")
                            .text(item.nilai+ "/" +item.nama)
                            .val(item.id)
                        );
                    }
                });
            }
        }
    });
}