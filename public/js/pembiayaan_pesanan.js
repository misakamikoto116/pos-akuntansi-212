var base_url = window.location.origin + '/akuntansi';

var token_ = $("meta[name='csrf-token']").attr("content");
var createData = new Boolean(true);
var updateRowId = null;
var q = $(".produk_id").length;
var x = $('.akun_beban_id').length;
$(function () {
    let exist_product = $('.produk_id').length;
    for (let a = 0; a < exist_product; a++) {
        callSelect2AjaxProdukID(a, $('#produk_id_temp' + a).val());
        callSelect2AjaxProduk(a, 'modal');
    }

    let exist_akun = $('.akun_beban_id').length;
        for (let b = 0; b < exist_akun; b++) {
            setAKunID(b, $('#akun_beban_temp' + b).val());
            setAkun(b);
        }

    $('.add-item').on('click', function () {
        duplicateForm();
    });
    $('.duplicate-beban-sections').on('click', '.add-beban', function () {
        duplicateBeban();
    });

    $('.duplicate-beban-sections').on('click', '.remove-rincian-beban', function () {
        if ($(this).closest('.purchaseBeban').find('tr').length > 0) {
            $(this).closest('tr').remove();
            $('.purchaseBeban tr:last').find('td:last').append('<button href="" class="btn btn-danger remove-rincian-beban" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button>');
            calculate();
            x -= 1;
        }
    });

    removeDelButton();
    removeDelAkunButton();
    sumQtyAndUnit(exist_product);
});


function removeDelButton() {
    $('.remove-rincian-item').not(':last').remove();
}

function removeDelAkunButton() {
    $('.remove-rincian-beban').not(':last').remove();
}

function duplicateForm() {
    var row = $('#table_item_section').html();
    var clone = $(row).clone();
    clone.find('.no_produk').attr('name', 'no_produk[]').attr('id', 'no_produk' + q).attr('onclick', 'awas(' + q + ')').attr('class', 'form-control no_produk').val('');
    clone.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('class', 'form-control produk_id').val('');
    clone.find('.keterangan_produk').attr('name', 'keterangan_produk[]').attr('id', 'keterangan_produk' + q).val('');
    clone.find('.qty_produk').attr('name', 'qty_produk[]').attr('id', 'qty_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')').val('');
    clone.find('.amount_produk').attr('name', 'amount_produk[]').attr('id', 'amount_produk' + q).attr('readonly');
    clone.find('.harga_modal').attr('name', 'harga_modal[]').attr('id', 'harga_modal' + q).val('');
    clone.find('.unit_produk').attr('name', 'unit_produk[]').attr('id', 'unit_produk' + q).val('');
    clone.find('.gudang_id').attr('name', 'gudang_id[]').attr('id', 'gudang_id' + q).attr('class', 'select2').val('');
    clone.find('.sn').attr('name', 'sn[]').attr('id', 'sn' + q).val('');
    clone.find('.tanggal_produk').attr('name', 'tanggal_produk[]').attr('id', 'tanggal_produk' + x).val('');
    $(clone).appendTo($('.duplicate-item-sections').closest('.item-sections').find('.purchaseItem'));
    $('.format-tanggal').datepicker({ format: 'dd M yyyy', autoclose: true });
    $('#amount_produk' + q).val('');
    $('.select2').select2();
    // callSelect2AjaxProduk(q);
    removeDelButton();
    q++;
}

function duplicateBeban() {
    var row = $('#table_beban_section').html();
    var clone = $(row).clone();
    clone.find('.akun_beban_id').attr('name', 'akun_beban_id[]').attr('id', 'akun_beban_id' + x).attr('onchange', 'setNamaAkun(' + x + ')');
    clone.find('.amount_beban').attr('name', 'amount_beban[]').attr('id', 'amount_beban' + x).attr('onchange', 'calculate()').autoNumeric('init',{aPad: false}).val(0);
    clone.find('.notes_beban').attr('name', 'notes_beban[]').attr('id', 'notes_beban' + x);
    clone.find('.tanggal_beban').attr('name', 'tanggal_beban[]').attr('id', 'tanggal_beban' + x);
    clone.find('.akun_beban_nama').attr('name', 'akun_beban_nama[]').attr('id', 'akun_beban_nama' + x);
    $(clone).appendTo($('.duplicate-beban-sections').closest('.beban-sections').find('.purchaseBeban'));
    $('.format-tanggal').datepicker({ format: 'dd M yyyy', autoclose: true });
    $('.select3').select2();
    // callSelect2AjaxProduk();
    removeDelAkunButton()
    setAkun();
    x++;
}

function callSelect2AjaxProduk(q = null, type = null) {
    if(type === null){
        $('.produk_id').select2({
            minimumInputLength: 3,
            formatInputTooShort: function () {
                return "Ketik 3 Karakter";
            },
            ajax: {
                url: "" + base_url + "/ajax/get-nama-produk",
                type: 'GET',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                dataType: 'JSON',
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });    
    }else if(type == 'modal'){
        $('.modal_kode_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            keyPressModalKodeBarang(q, x, kategori);
        });

        $('.modal_nama_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            keyPressModalNamaBarang(q, x, kategori);
        });
    }    
}

function callSelect2AjaxProdukID(q, id) {
    $.ajax({
        url: "" + base_url + "/ajax/get-id-produk",
        type: 'GET',
        data: 'search=' + id,
        dataType: 'JSON',
        success: function (data) {
            $.each(data.item, function (key, value) {
                $('#produk_id'+q).val(value.id);
                $('#no_produk'+q).val(value.no_barang);
                // $('#produk_id' + q).append('<option selected value="' + value.id + '">' + value.text + '</option>');
            });
        }
    });
}

$('.duplicate-item-sections').on('click', '.remove-rincian-item', function () {
    if ($(this).closest('.purchaseItem').find('tr').length > 0) {
        $(this).closest('tr').remove();
        $('.purchaseItem tr:last').find('td:last').append('<button href="" class="btn btn-danger remove-rincian-item" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button>');
        calculate();
        q -= 1;
    }
});
$('.tanggal1').datepicker({ format: 'yyyy-mm-dd', autoclose: true });
$('.tanggal1').datepicker('setDate', new Date());

// function test(vale) {
//     var produk_id = $('#produk_id' + vale).val();
//     $.ajax({
//         type: "GET",
//         url: base_url + "/ajax/get-produk",
//         data: {
//             id: produk_id,
//             _token: token_
//         },
//         dataType: "json",
//         success: function (response) {
//             $('#keterangan_produk' + vale).val(response.keterangan);
//             $('#qty_produk' + vale).val('1');
//             $('#harga_modal' + vale).val(response.harga_modal.harga_modal);
//             $('#unit_produk' + vale).val(response.satuan);
//             $('#amount_produk' + vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.harga_modal.harga_modal); //karena defaultnya di * 1, maka cukup unitprice saja
//             $('#diskon_produk' + vale).val('0');
//             if (response.multiGudang) {
//                 $('#gudang_id' + vale).empty();
//             }
//             $(response.multiGudang).each(function (val, text) {
//                 $(text.gudang).each(function (index, item) {
//                     $('#gudang_id' + vale).append(`<option value="${item.id}">${item.nama}</option>`);
//                 });
//             });
//         }, failure: function (errMsg) {
//             alert(errMsg);
//         }
//     });
// }

function setAkun(q = null) {
    $('.akun_id').select2({
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Ketik 3 Karakter";
        },
        ajax: {
            url: "" + base_url + "/ajax/get-nama-akun",
            type: 'GET',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            dataType: 'JSON',
            processResults: function (data) {
                return {
                    results: data
                };
            }
        }
    });
}

function setAKunID(q, id) {
    $.ajax({
        url: "" + base_url + "/ajax/get-id-akun",
        type: 'GET',
        data: 'search=' + id,
        dataType: 'JSON',
        success: function (data) {
            $.each(data, function (key, value) {
                $('#akun_beban_id' + q).append('<option selected value="' + value.id + '">' + value.text + '</option>');
            });
        }
    });
}

function setNamaAkun(vale) {
    var akun_id = $('#akun_beban_id' + vale).val();
    $.ajax({
        type: "GET",
        url: base_url + "/ajax/get-akun_id",
        data: {
            id: akun_id,
            _token: token_
        },
        dataType: "json",
        success: function (response) {
            $('#akun_beban_nama' + vale).val(response.nama_akun);
        }, error: function (errMsg) {
            alert(errMsg);
        }
    });
}

function setNamaAkunMain(akun_id) {
    $.ajax({
        type: "GET",
        url: base_url + "/ajax/get-akun_id",
        data: {
            id: akun_id,
            _token: token_
        },
        dataType: "json",
        success: function (response) {
            $('#akun').val(response.kode_akun);
        }, error: function (errMsg) {
            alert(errMsg);
        }
    });
}


function sumQtyAndUnit(param) {
    var amount = 0;
    let unit = $('#harga_modal' + param).val();
    amount = parseInt($('#qty_produk' + param).val()) * unit;
    $('#amount_produk' + param).autoNumeric('init',{aPad: false}).autoNumeric('set', amount);
    calculate();
    // need be fixed
}

function calculate() {
    var subTotal = 0;
    var grandtotal = 0;
    var totalBeban = 0;
    

    if($(".amount_beban").length > 0){
        $(".amount_beban").each(function () {
            let temp_beban = parseInt(changeMaskingToNumber($(this).val()));
            if(typeof temp_beban == 'number'){
                totalBeban += temp_beban;
            }
        });
    }
    
    grandtotal =  totalBeban;

    $('#subtotal').html("<strong>" + Number(subTotal).toLocaleString() + "</strong>");
    $('.sub-total-cetak').val(subTotal);
    $('.grandtotal').html("<strong>" + Number(grandtotal).toLocaleString() + "</strong>");
    $('.grand_tot').val(grandtotal);
    $('#grandtotal').val(grandtotal);
}

function keyPressModalKodeBarang(q, x, kategori = null){
    var no_barang = x;
    var data = {
        kategoriBySearch : kategori,
        no_barang : no_barang,
        _token: token_,
    }
    $('.modal_nama_barang').val('');
    callAjaxKeypress(q, no_barang, data)
}

function keyPressModalNamaBarang(q, x, kategori){
    var keterangan = x;
    var data = {
        kategoriBySearch : kategori,
        keterangan : keterangan,
        _token: token_,
    }
    $('.modal_kode_barang').val('');
    callAjaxKeypress(q, keterangan, data)
}


function awas(q = null){
    var type = 'modal';
    $('#modalBarang').modal({
        backdrop: 'static',
        keyboard: false, 
        show: true
    });
    var row = $('#modal_thead_barang').html();
    var clone = $(row).clone();
    clone.find('.modal_kode_barang').attr('name', 'modal_kode_barang[]').attr('id', 'modal_kode_barang' + q).attr('class', 'form-control modal_kode_barang');
    clone.find('.modal_nama_barang').attr('name', 'modal_nama_barang[]').attr('id', 'modal_nama_barang' + q).attr('class', 'form-control modal_nama_barang');;
    $(clone).appendTo($('.duplicate-modal-sections').closest('.modal-sections').find('.modal_thead'));

    callSelect2AjaxProduk(q, type);
    callAjaxListGudang();
}

function barangBatal() {
    $('.modal_thead_barang_tr').remove();
    $('.table_tr_modal').remove();
}

function clearQtyGudang(){
    $('#modal_gudang_temp').val(' ');
    $('label[id*="qty_gudang_footer"]').text('');
    $('#modal_gudang').val(0).trigger('change');
}

$('#kategori_barang').change(function() {
    var searchBy = $('#searchByKategori');
    clearQtyGudang();
    if ($('#kategori_barang').val() == 1) {
        searchBy.prop('disabled', false);
        ajaxModalKategori(searchBy);

        setTimeout(function() {
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        }, 600);

        $('#searchByKategori').change(function() {
            clearQtyGudang();
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        });
    }else if($('#kategori_barang').val() == 0){
        searchBy.html(null);
        searchBy.prop('disabled', true);
        
        var kode_barang = $('.modal_kode_barang').val();
        var nama_barang = $('.modal_nama_barang').val();            
        if (kode_barang !== '' && nama_barang === '') {
            keyPressModalKodeBarang(null, kode_barang, null);
        }else if (kode_barang === '' && nama_barang !== '') {
            keyPressModalNamaBarang(null, nama_barang, null);
        }
    }
});

function ajaxModalKategori(searchBy){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-all-kategori-produk",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                searchBy.append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxKeypress(n = null, searchKey, dataKey){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-nama-produk",
        data: dataKey,
        dataType: 'JSON',
        success: function (data) {
            $('.table_tr_modal').remove();
            if (data == 'null') {
                $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="table_tr_modal">'
                        + '<td colspan="3" style="padding:5px; 20px" align="center">'
                        +   '<span class="alert alert-warning"> Tidak ada Barang dengan kata Kunci '+ searchKey +' </span>'
                        + '</td>' +
                    '</tr>').appendTo('.modal_tbody');
            }else{
                $.each(data, function (key, value) {
                    var idP = value.id;

                    $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="modal_barang_'+ key +' table_tr_modal">'
                            + '<td width="26%" style="padding:5px 15px">'
                                + value.no_barang
                            + '</td>'
                            + '<td width="3%"></td>'
                            + '<td width="55%" style="padding:5px 15px">'
                                + value.keterangan
                            + '</td>'+
                        '</tr>').appendTo('.modal_tbody');

                    // hey
                    $('.modal_barang_'+ key).hover(
                        function(){
                            $(this).attr('style', 'border: 2px solid limegreen; border-radius: 5px ;display:table; width:100%; table-layout:fixed;');
                        }, function() {
                            $(this).attr('style', 'border: none; display:table; width:100%; table-layout:fixed;');
                        }
                    );

                    $('.table_tr_modal').click(function () {
                        var ini = $(this);
                        if(ini.is(':hover') === true){
                            $('.table_tr_modal').removeClass('warnatr');
                            ini.addClass('warnatr');
                        }
                    });
                    // yeh

                    $('.modal_barang_'+ key).click(function() {
                        var ini = $(this);
                        var gudang = $('#modal_gudang').val();
                        $('#modal_gudang_temp').val(idP);
                        
                        callAjaxQtyGudang(idP, gudang);
                    });

                    $('.modal_barang_'+ key).dblclick(function(){
                        setTimeout(function() {
                            clearQtyGudang();
                        }, 0);
                        $('#produk_id'+n).val(idP);
                        $('#no_produk'+n).val(value.no_barang);
                        test(idP, n);
                        barangBatal();
                        $('#modalBarang').modal('toggle');
                    });
                    
                    $('#modal_gudang').change(function() {
                        var gudang = $('#modal_gudang').val();
                        var produk = $('#modal_gudang_temp').val();

                        callAjaxQtyGudang(produk, gudang);
                    });
                });
            }
        }
    });
}

function callAjaxQtyGudang(produk, gudang){
    $.ajax({
        type : 'GET',
        url  : "" + base_url + "/ajax/get-qty-gudang-stock",
        data : {
            gudang : gudang,
            produk : produk,
            _token : token_
        },
        dataType : 'json',
        success: function (resuponse){
            $('#qty_gudang_footer').html(resuponse);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxListGudang(){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-list-gudang",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                $('#modal_gudang').append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function test(idP, vale) { // js dari blade create dan edit
    var produk_id   = idP;
        $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-produk",
        data: {
            id : produk_id,
            _token: token_
        },
        dataType: "json",
        success: function(response){
            $('#keterangan_produk' + vale).val(response.keterangan);
            $('#qty_produk' + vale).val('1');
            $('#harga_modal' + vale).val(response.harga_modal.harga_modal);
            $('#unit_produk' + vale).val(response.satuan);
            $('#amount_produk' + vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.harga_modal.harga_modal); //karena defaultnya di * 1, maka cukup unitprice saja
            $('#diskon_produk' + vale).val('0');
            if (response.multiGudang) {
                $('#gudang_id' + vale).empty();
            }
            $(response.multiGudang).each(function (val, text) {
                $(text.gudang).each(function (index, item) {
                    $('#gudang_id' + vale).append(`<option value="${item.id}">${item.nama}</option>`);
                });
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}