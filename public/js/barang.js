var base_url 	= window.location.origin + '/akuntansi';

var token_ 		= $("meta[name='csrf-token']").attr("content");

$(document).ready(function($) {

	$('.button-submit-barang').on('click', function (e) {
		
		e.preventDefault();

		var form = $(this).parents('form.form-submit-barang');

		$.ajax({
			url: "" + base_url + "/ajax/get-cek-preferensi",
			type: 'GET',
			dataType: 'JSON',
			data: {
				kuantitas: $('.kuantitas-gudang').val(),
				_token: token_,
			},
		})
		.done(function(response) {
			
			console.log(response);

			if (response !== null) {

				if (response === true) {

					form.submit();

				}else {

					swal({
						icon: 'error',
						text: 'Preferensi Penyesuaian Masih Kosong',
					});

				}

			}

		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});

	});

});
