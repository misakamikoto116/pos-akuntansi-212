var base_url = window.location.origin + '/akuntansi';

var token_ = $("meta[name='csrf-token']").attr("content");
var createData = new Boolean(true);
var updateRowId = null;
var q = $(".produk_id").length;

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

$(function () {

    if(typeof GetURLParameter('pembiayaan_pesanan_id') == 'string'){
      $('#pembiayaan_id').val(GetURLParameter('pembiayaan_pesanan_id')).trigger('change');
    }

    if ($("#pembiayaan_id").val() != "") {
        getPembiayaan($("#pembiayaan_id").val());
    }

    if ($(".kode_akun_item").val() != "") {
        setNamaAkun();
    }
    let exist_product = $('.produk_id').length;
    for (let a = 0; a < exist_product; a++) {
        callSelect2AjaxProdukID(a, $('#produk_id_temp' + a).val());
        callSelect2AjaxProduk(a, 'modal');
    }
    $('.add-item').on('click', function () {
        duplicateForm();
    });

    showAndHideTipePenyelesaian();
});

function removeDelButton() {
    $('.remove-rincian-item').not(':last').remove();
}

function duplicateForm() {
    var row = $('#table_item_section').html();
    var clone = $(row).clone();
    clone.find('.no_produk').attr('name', 'no_produk[]').attr('id', 'no_produk' + q).attr('onclick', 'awas(' + q + ')').attr('class', 'form-control no_produk').val('');
    clone.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('class', 'form-control produk_id').val('');
    clone.find('.keterangan_produk').attr('name', 'keterangan_produk[]').attr('id', 'keterangan_produk' + q).val('');
    clone.find('.qty_produk').attr('name', 'qty_produk[]').attr('id', 'qty_produk' + q).attr('onchange', 'sumQtyAndUnit()').val('');
    clone.find('.amount_produk').attr('name', 'amount_produk[]').attr('id', 'amount_produk' + q).val('');
    clone.find('.alokasi_nilai').attr('name', 'alokasi_nilai[]').attr('id', 'alokasi_nilai' + q).attr('onchange', 'sumQtyAndUnit()').val('');
    clone.find('.diskon_produk').attr('name', 'diskon_produk[]').attr('id', 'diskon_produk' + q).attr('onchange', 'sumQtyAndUnit()').val('');
    clone.find('.presentase_produk').attr('name', 'presentase_produk[]').attr('id', 'presentase_produk' + q).attr('onchange', 'sumQtyAndUnit()').val('');
    
    clone.find('.gudang_id').attr('name', 'gudang_id[]').attr('id', 'gudang_id' + q).attr('readonly', '').attr('class','select2').val('');
    clone.find('.sn').attr('name', 'sn[]').attr('id', 'sn' + q).attr('readonly', '');

    $(clone).appendTo($('.duplicate-item-sections').closest('.item-sections').find('.purchaseItem'));
    $('.format-tanggal').datepicker({ format: 'dd M yyyy', autoclose: true });
    $('.select2').select2();
    // callSelect2AjaxProduk(q);
    removeDelButton();
    q++;
}

function callSelect2AjaxProduk(q = null, type = null) {
    if(type === null){
        $('.produk_id').select2({
            minimumInputLength: 3,
            formatInputTooShort: function () {
                return "Ketik 3 Karakter";
            },
            ajax: {
                url: "" + base_url + "/ajax/get-nama-produk",
                type: 'GET',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                dataType: 'JSON',
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });    
    }else if(type == 'modal'){
        $('.modal_kode_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            keyPressModalKodeBarang(q, x, kategori);
        });

        $('.modal_nama_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            keyPressModalNamaBarang(q, x, kategori);
        });
    }    
}

// function callSelect2AjaxProduk(q = null) {
//     $('.produk_id').select2({
//         minimumInputLength: 3,
//         formatInputTooShort: function () {
//             return "Ketik 3 Karakter";
//         },
//         ajax: {
//             url: "" + base_url + "/ajax/get-nama-produk",
//             type: 'GET',
//             data: function (params) {
//                 var query = {
//                     search: params.term,
//                     type: 'public'
//                 }
//                 return query;
//             },
//             dataType: 'JSON',
//             processResults: function (data) {
//                 return {
//                     results: data
//                 };
//             }
//         }
//     });
// }

function callSelect2AjaxProdukID(q, id) {
    $.ajax({
        url: "" + base_url + "/ajax/get-id-produk",
        type: 'GET',
        data: 'search=' + id,
        dataType: 'JSON',
        success: function (data) {
            $.each(data.item, function (key, value) {
                $('#produk_id'+q).val(value.id);
                $('#no_produk'+q).val(value.no_barang);
                // $('#produk_id' + q).append('<option selected value="' + value.id + '">' + value.text + '</option>');
            });
        }
    });
}

$('.duplicate-item-sections').on('click', '.remove-rincian-item', function () {
    if ($(this).closest('.purchaseItem').find('tr').length > 0) {
        $(this).closest('tr').remove();
        $('.purchaseItem tr:last').find('td:last').append('<button href="" class="remove btn btn-danger remove-rincian-item" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button>');
        q -= 1;
    }
});
$('.tanggal1').datepicker({ format: 'yyyy-mm-dd', autoclose: true });
$('.tanggal1').datepicker('setDate', new Date());

// function test(vale) {
//     var produk_id = $('#produk_id' + vale).val();
//     $.ajax({
//         type: "GET",
//         url: base_url +"/ajax/get-produk",
//         data: {
//             id: produk_id,
//             _token: token_
//         },
//         dataType: "json",
//         success: function (response) {
//             $('#keterangan_produk' + vale).val(response.keterangan);
//             $('#qty_produk' + vale).val('1');
//             $('#amount_produk' + vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0);
//             $('#alokasi_nilai' + vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0);
//             $('#diskon_produk' + vale).val('0');
//             if(response.multiGudang){
//                 $('#gudang_id' + vale).empty();
//             }
//             $(response.multiGudang).each(function (val, text) {
//                 $(text.gudang).each(function (index, item) {
//                     $('#gudang_id' + vale).append(`<option value="${item.id}">${item.nama}</option>`);
//                 });
//             });
//         }, failure: function (errMsg) {
//             alert(errMsg);
//         }
//     });
// }

function getPembiayaan(id) {
    $.ajax({
        type: "GET",
        url: base_url + "/ajax/get-pembiayaan",
        data: {
            id: id,
            _token: token_
        },
        dataType: "json",
        success: function (response) {
            $('#date').val(response.date);
            $('.tanggal').datepicker({ format: 'dd M yyyy', autoclose: true });
            $('#akun_id').val(response.akun_pembiayaan_pesanan_id).trigger('change');
            $('#keterangan').val(response.deskripsi);
            $('#total').autoNumeric('init',{aPad: false}).autoNumeric('set', response.total_pembiayaan);
            setNamaAkunMain();
        }, failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function sumQtyAndUnit() {
    let total = changeMaskingToNumber($('#total').val());
    if (typeof total === 'undefined' || total <= 0) {
        swal({
            title: "Error!",
            text: "Harap pilih Nomor Pesanan terlebih dahulu!",
            icon: "warning",
            dangerMode: true,
        });
        $('.purchaseItem').find('tr').remove();
        q = 0;
    }
    let total_presentase = 0;

    $('.presentase_produk').each(function (idx){
        if ($(this).context.value > 0){
            // Hitung presentase
            let alokasi_nilai = total * parseFloat($(this).context.value) / 100;
            $('#alokasi_nilai' + idx).autoNumeric('init',{aPad: false}).autoNumeric('set', alokasi_nilai);
        
            let amount_produk = alokasi_nilai / $('#qty_produk' + idx).val();     
            $('#amount_produk' + idx).autoNumeric('init',{aPad: false}).autoNumeric('set', amount_produk);
        } 
        total_presentase += parseFloat($(this).context.value);
    });

    if(total_presentase >= 100){
        $('.add-item').hide(500);
        $('#btn-submit').prop('disabled',false);
    }else{
        $('.add-item').show(500);
        $('#btn-submit').prop('disabled', true);
    }
}

function changeTab(params) {
    if(params == 'account'){
        $('.tipe-item').hide(500);
        $('.tipe-account').show(500);
    }else{
        $('.tipe-item').show(500);
        $('.tipe-account').hide(500);
    }
}

function setNamaAkunMain() {
    var akun_id = $('#akun_id option:selected').val();
    $.ajax({
        type: "GET",
        url: base_url + "/ajax/get-akun_id",
        data: {
            id: akun_id,
            _token: token_
        },
        dataType: "json",
        success: function (response) {
            $('#akun').val(response.kode_akun);
        }, error: function (errMsg) {
            alert(errMsg);
        }
    });
}

function setNamaAkun() {
    var akun_id = $('.kode_akun_item option:selected').val();
    $.ajax({
        type: "GET",
        url: base_url + "/ajax/get-akun_id",
        data: {
            id: akun_id,
            _token: token_
        },
        dataType: "json",
        success: function (response) {
            $('.nama_akun_item').val(response.kode_akun);
        }, error: function (errMsg) {
            alert(errMsg);
        }
    });
}

function showAndHideTipePenyelesaian() {
    if ($('#tipe_penyelesaian_account').is(':checked')) {
        changeTab('account');
    }else if($('#tipe_penyelesaian_item').is(':checked')) {
        changeTab('item');
    }else {
        $('.tipe-item').hide(500);
        $('.tipe-account').hide(500);
    }
}

function keyPressModalKodeBarang(q, x, kategori = null){
    var no_barang = x;
    var data = {
        kategoriBySearch : kategori,
        no_barang : no_barang,
        _token: token_,
    }
    $('.modal_nama_barang').val('');
    callAjaxKeypress(q, no_barang, data)
}

function keyPressModalNamaBarang(q, x, kategori){
    var keterangan = x;
    var data = {
        kategoriBySearch : kategori,
        keterangan : keterangan,
        _token: token_,
    }
    $('.modal_kode_barang').val('');
    callAjaxKeypress(q, keterangan, data)
}


function awas(q = null){
    var type = 'modal';
    $('#modalBarang').modal({
        backdrop: 'static',
        keyboard: false, 
        show: true
    });
    var row = $('#modal_thead_barang').html();
    var clone = $(row).clone();
    clone.find('.modal_kode_barang').attr('name', 'modal_kode_barang[]').attr('id', 'modal_kode_barang' + q).attr('class', 'form-control modal_kode_barang');
    clone.find('.modal_nama_barang').attr('name', 'modal_nama_barang[]').attr('id', 'modal_nama_barang' + q).attr('class', 'form-control modal_nama_barang');;
    $(clone).appendTo($('.duplicate-modal-sections').closest('.modal-sections').find('.modal_thead'));

    callSelect2AjaxProduk(q, type);
    callAjaxListGudang();
}

function barangBatal() {
    $('.modal_thead_barang_tr').remove();
    $('.table_tr_modal').remove();
}

function clearQtyGudang(){
    $('#modal_gudang_temp').val(' ');
    $('label[id*="qty_gudang_footer"]').text('');
    $('#modal_gudang').val(0).trigger('change');
}

$('#kategori_barang').change(function() {
    var searchBy = $('#searchByKategori');
    clearQtyGudang();
    if ($('#kategori_barang').val() == 1) {
        searchBy.prop('disabled', false);
        ajaxModalKategori(searchBy);

        setTimeout(function() {
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        }, 600);

        $('#searchByKategori').change(function() {
            clearQtyGudang();
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        });
    }else if($('#kategori_barang').val() == 0){
        searchBy.html(null);
        searchBy.prop('disabled', true);
        
        var kode_barang = $('.modal_kode_barang').val();
        var nama_barang = $('.modal_nama_barang').val();            
        if (kode_barang !== '' && nama_barang === '') {
            keyPressModalKodeBarang(null, kode_barang, null);
        }else if (kode_barang === '' && nama_barang !== '') {
            keyPressModalNamaBarang(null, nama_barang, null);
        }
    }
});

function ajaxModalKategori(searchBy){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-all-kategori-produk",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                searchBy.append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxKeypress(n = null, searchKey, dataKey){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-nama-produk",
        data: dataKey,
        dataType: 'JSON',
        success: function (data) {
            $('.table_tr_modal').remove();
            if (data == 'null') {
                $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="table_tr_modal">'
                        + '<td colspan="3" style="padding:5px; 20px" align="center">'
                        +   '<span class="alert alert-warning"> Tidak ada Barang dengan kata Kunci '+ searchKey +' </span>'
                        + '</td>' +
                    '</tr>').appendTo('.modal_tbody');
            }else{
                $.each(data, function (key, value) {
                    var idP = value.id;

                    $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="modal_barang_'+ key +' table_tr_modal">'
                            + '<td width="26%" style="padding:5px 15px">'
                                + value.no_barang
                            + '</td>'
                            + '<td width="3%"></td>'
                            + '<td width="55%" style="padding:5px 15px">'
                                + value.keterangan
                            + '</td>'+
                        '</tr>').appendTo('.modal_tbody');

                    // hey
                    $('.modal_barang_'+ key).hover(
                        function(){
                            $(this).attr('style', 'border: 2px solid limegreen; border-radius: 5px ;display:table; width:100%; table-layout:fixed;');
                        }, function() {
                            $(this).attr('style', 'border: none; display:table; width:100%; table-layout:fixed;');
                        }
                    );

                    $('.table_tr_modal').click(function () {
                        var ini = $(this);
                        if(ini.is(':hover') === true){
                            $('.table_tr_modal').removeClass('warnatr');
                            ini.addClass('warnatr');
                        }
                    });
                    // yeh

                    $('.modal_barang_'+ key).click(function() {
                        var ini = $(this);
                        var gudang = $('#modal_gudang').val();
                        $('#modal_gudang_temp').val(idP);
                        
                        callAjaxQtyGudang(idP, gudang);
                    });

                    $('.modal_barang_'+ key).dblclick(function(){
                        setTimeout(function() {
                            clearQtyGudang();
                        }, 0);
                        $('#produk_id'+n).val(idP);
                        $('#no_produk'+n).val(value.no_barang);
                        test(idP, n);
                        barangBatal();
                        $('#modalBarang').modal('toggle');
                    });
                    
                    $('#modal_gudang').change(function() {
                        var gudang = $('#modal_gudang').val();
                        var produk = $('#modal_gudang_temp').val();

                        callAjaxQtyGudang(produk, gudang);
                    });
                });
            }
        }
    });
}

function callAjaxQtyGudang(produk, gudang){
    $.ajax({
        type : 'GET',
        url  : "" + base_url + "/ajax/get-qty-gudang-stock",
        data : {
            gudang : gudang,
            produk : produk,
            _token : token_
        },
        dataType : 'json',
        success: function (resuponse){
            $('#qty_gudang_footer').html(resuponse);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxListGudang(){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-list-gudang",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                $('#modal_gudang').append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function test(idP, vale) { // js dari blade create dan edit
    var produk_id   = idP;
        $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-produk",
        data: {
            id : produk_id,
            _token: token_
        },
        dataType: "json",
        success: function(response){
            $('#keterangan_produk' + vale).val(response.keterangan);
            $('#qty_produk' + vale).val('1');
            $('#amount_produk' + vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0);
            $('#alokasi_nilai' + vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0);
            $('#diskon_produk' + vale).val('0');
            if(response.multiGudang){
                $('#gudang_id' + vale).empty();
            }
            $(response.multiGudang).each(function (val, text) {
                $(text.gudang).each(function (index, item) {
                    $('#gudang_id' + vale).append(`<option value="${item.id}">${item.nama}</option>`);
                });
            });        
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}