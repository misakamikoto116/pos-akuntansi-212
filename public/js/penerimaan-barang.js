var base_url = window.location.origin + '/akuntansi';

var token_ = $("meta[name='csrf-token']").attr("content");
var createData = new Boolean(true);
var updateRowId = null;

$('.tanggal_penerimaan').datepicker({ format: 'dd MM yyyy', autoclose: true });

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function enable_cb() {
    if (this.checked) {
        $("input.group2").removeAttr("disabled");
    } else {
        $("input.group2").attr("disabled", true).removeAttr("checked");
    }
}

$('#selectAll').click(function (e) {
    var table = $(e.target).closest('table');
    $('th input:checkbox', table).prop('checked', this.checked);
});

$(document).ready(function () {
    let exist_product = $('.produk_id').length;
    for (let a = 0; a < exist_product; a++) {
        callSelect2AjaxProdukID(a, $('#produk_id_temp' + a).val());
        callSelect2AjaxProduk(a, 'modal');
    }
});

function callSelect2AjaxProduk(q = null, type = null) {
    if(type === null){
        $('.produk_id').select2({
            minimumInputLength: 3,
            formatInputTooShort: function () {
                return "Ketik 3 Karakter";
            },
            ajax: {
                url: "" + base_url + "/ajax/get-nama-produk",
                type: 'GET',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                dataType: 'JSON',
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });
    }else if(type == 'modal'){
        $('.modal_kode_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            keyPressModalKodeBarang(q, x, kategori);
        });

        $('.modal_nama_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            keyPressModalNamaBarang(q, x, kategori);
        });
    }
}

function callSelect2AjaxProdukID(q, id) {
    $.ajax({
        url: "" + base_url + "/ajax/get-id-produk",
        type: 'GET',
        data: 'search=' + id,
        dataType: 'JSON',
        success: function (data) {
            $.each(data.item, function (key, value) {
                $('#produk_id'+q).val(value.id);
                $('#no_produk'+q).val(value.no_barang);
                // $('#produk_id' + q).append('<option selected value="' + value.id + '">' + value.text + '</option>');
            });
        }
    });
}

// function test(vale) {
//     var produk_id = $('#produk_id' + vale).val();
//     $.ajax({
//         type: "GET",
//         url: base_url + '/ajax/get-produk/',
//         data: {
//             id: produk_id,
//             _token: token_
//         },
//         dataType: "json",
//         success: function (response) {
//             $('#gudang_id' + vale).find('option').remove().end();
//             $('#keterangan_produk' + vale).val(response.keterangan)
//             $('#qty_produk' + vale).val('1')
//             $('#satuan_produk' + vale).val(response.satuan)
//             $('#diskon_produk' + vale).val(response.diskon)
//             $('#tax_produk' + vale).val(response.tax)
//             $("#unit_price" + vale).val(response.unitPrice);
//             let total = (response.jumlah * response.unitPrice);
//             let disc = total * response.diskon / 100;
//             $('#total_produk' + vale).val(total - disc);
//             $(response.multiGudang).each(function (val, text) {
//                 $(text.gudang).each(function (index, item) {
//                     $('#gudang_id' + vale).append(`<option class="form-control select2" value="${item.id}">${item.nama}</option>`);
//                 });
//             });
//             if (response.harga_modal === null) {
//                 $('#harga_modal' + vale).val(0);
//             } else {
//                 $('#harga_modal' + vale).val(response.harga_modal.harga_modal);
//                 $('#harga_terakhir' + vale).val(response.harga_modal.harga_terakhir);
//             }
//         }, failure: function (errMsg) {
//             alert(errMsg);
//         }
//     });
// }
$('.duplicate-penawaran-sections').on('click', '.remove-rincian-penawaran', function () {
    if ($(this).closest('.purchasePenawaran').find('tr').length > 0) {
        $(this).closest('tr').remove();
        $('.purchasePenawaran tr:last').find('td:last').append('<button href="" class="remove btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button>');
        q -= 1;
    }
});

$("#pemasok_id").change(function () {
    setDataPemasok();
});

function setDataPemasok() {
    var pemasok_id = $('#pemasok_id').val();
    var sub = GetURLParameter('pesanan_id');
    $.ajax({
        type: "GET",
        url: base_url + '/ajax/get-alamat-pemasok',
        data: {
            id: pemasok_id,
            _token: token_
        },
        dataType: "json",
        success: function (response) {
            $('#table-list-pesanan').find('tr').remove().end();
            $('.purchasePenawaran').find('tr').remove().end();
            q = 0;
            $('#alamat_asal').val(response.alamat);
            $('#no_pemasok').val(response.no_pemasok);
            if (response.pesanan) {
                $.each(response.pesanan, function (index, item) {
                    if (item.sum_updated_qty !== 0) {
                        if (sub == item.id) {
                            $('#table-list-pesanan').append('<tr><th><input type="checkbox" checked value="' + item.id + '" name="pesanan[]" id="pesanan[]" class="pesanan_item"/></th><td>' + item.po_number + '</td><td>' + item.po_date + '</td></tr>');
                            filterSelectedRequest();
                        } else {
                            $('#table-list-pesanan').append('<tr><th><input type="checkbox" value="' + item.id + '" name="pesanan[]" id="pesanan[]" class="pesanan_item"/></th><td>' + item.po_number + '</td><td>' + item.po_date + '</td></tr>');
                            checkAfterSetPemasok();
                        }
                    }
                });
            }
        }, failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function setDataPemasokWhenOld(pemasok_id) {
    $.ajax({
        type: "GET",
        url: base_url + '/ajax/get-alamat-pemasok',
        data: {
            id: pemasok_id,
            _token: token_
        },
        dataType: "JSON",
        success: function (response) {
            $("#table-list-pesanan").find('tr').remove().end();
            if (response.pesanan) {
                $.each(response.pesanan, function (index, item) {
                    $("#table-list-pesanan").append('<tr><th><input type="checkbox" value="' + item.id + '" name="pesanan[]" id="pesanan[]"/></th><td>' + item.po_number + '</td><td>' + item.po_date + '</td></tr> ');
                });
            }   
        }, failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function filterSelectedRequest() {
    var checkCounted = $('input[name="pesanan[]"]:checked').length;
    if (checkCounted > 0) {
        $('input[name="pesanan[]"]:checked').each(function () {
            var val = $(this).val();
            $.ajax({
                type: "GET",
                url: base_url + '/ajax/get-detail-barang',
                data: {
                    id: val, type: 'pesanan-pembelian',
                    _token: token_
                },
                dataType: "json",
                success: function (response) {
                    $.each(response.barang, function (index, item) {
                        if (item.updated_qty !== 0 || item.updated_qty !== null) {
                            duplicateForm();
                            let countTemp = q - 1;
                            callSelect2AjaxProdukID(countTemp,item.produk_id);
                            $('#produk_id' + countTemp).val(item.produk_id);
                            // $('.select2').select2();
                            insertItemToForm(countTemp, item.id); //-1 karena diatas (diplicateForm) sdh di increment
                        }
                    });
                    for (let a = 0; a < q; a++) {
                        callSelect2AjaxProduk(a, 'modal');
                    }
                }, failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        });
    }
}

function insertItemToForm(vale, item) {
    var url = base_url + '/ajax/get-produk-by-val';
    var pemasok_id = $("#pemasok_id").val();
    $.ajax({
        type: "GET",
        url: url,
        data: {
            id: item, type: 'pesanan-pembelian', pemasok_id: pemasok_id,
            _token: token_
        },
        dataType: "json",
        success: function (response) {
            $('#gudang_id' + vale).find('option').remove().end();
            $('#barang_pesanan_pembelian_id' + vale).val(response.id);
            $('#keterangan_produk' + vale).val(response.keterangan);
            $("#diskon_produk" + vale).val(response.diskon);
            $('#qty_produk' + vale).val(response.updated_qty);
            $('#satuan_produk' + vale).val(response.satuan);
            $("#unit_price" + vale).val(response.unitPrice);
            $('#fob_id').val(response.fob);
            $('#ship_id').val(response.ship_id);
            $("#catatan").val(response.taxable.keterangan);
            let total = (response.jumlah * response.unitPrice);
            let disc = total * response.diskon / 100;
            $('#total_produk' + vale).val(total - disc);
            $(response.multiGudang).each(function (val, text) {
                $(text.gudang).each(function (index, item) {
                    $('#gudang_id' + vale).append(`<option class="form-control select2" value="${item.id}">${item.nama}</option>`);
                });
            });
            if (response.harga_modal === null) {
                $('#harga_modal' + vale).val(0);
            } else {
                $('#harga_modal' + vale).val(response.harga_modal.harga_modal);
                $("#harga_terakhir" + vale).val(response.harga_modal.harga_terakhir);
            }
            $('.select2').select2();
            for (let a = 0; a < q; a++) {
                callSelect2AjaxProduk(a, 'modal');
            }
        }, failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function duplicateForm() {
    var row = $('#table_penawaran_section').html();
    var clone = $(row).clone();

    clone.find('.total_produk').attr('name', 'total_produk[' + q + ']').attr('id', 'total_produk' + q);
    clone.find('.produk_id').attr('name', 'produk_id[' + q + ']').attr('id', 'produk_id' + q).attr('class', 'produk_id');
    clone.find('.no_produk').attr('name', 'no_produk[' + q + ']').attr('id', 'no_produk' + q).attr('onclick', 'awas(' + q + ')').attr('class', 'form-control no_produk').attr('readonly', true);
    clone.find('.produk_id_temp').attr('name', 'produk_id_temp[' + q + ']').attr('id', 'produk_id_temp' + q);
    clone.find('.barang_pesanan_pembelian_id').attr('name', 'barang_pesanan_pembelian_id[' + q + ']').attr('id', 'barang_pesanan_pembelian_id' + q);
    clone.find('.harga_modal').attr('name', 'harga_modal[' + q + ']').attr('id', 'harga_modal' + q);
    clone.find('.harga_terakhir').attr('name', 'harga_terakhir[' + q + ']').attr('id', 'harga_terakhir' + q);
    clone.find('.unit_price').attr('name', 'unit_price[' + q + ']').attr('id', 'unit_price' + q).attr('onchange', 'cekQtyandPrice(' + q + ')');;
    clone.find('.diskon_produk').attr('name', 'diskon_produk[' + q + ']').attr('id', 'diskon_produk' + q);
    clone.find('.keterangan_produk').attr('name', 'keterangan_produk[' + q + ']').attr('id', 'keterangan_produk' + q);
    clone.find('.qty_produk').attr('name', 'qty_produk[' + q + ']').attr('id', 'qty_produk' + q).attr('onchange', 'cekQtyandPrice(' + q + ')');
    clone.find('.satuan_produk').attr('name', 'satuan_produk[' + q + ']').attr('id', 'satuan_produk' + q);
    clone.find('.dept_id').attr('name', 'dept_q[' + q + ']').attr('id', 'dept_id' + q);
    clone.find('.proyek_id').attr('name', 'proyek_q[' + q + ']').attr('id', 'proyek_id' + q);
    clone.find('.gudang_id').attr('name', 'gudang_id[' + q + ']').attr('id', 'gudang_id' + q);
    clone.find('.sn').attr('name', 'sn[' + q + ']').attr('id', 'sn' + q);
    $(clone).appendTo($('.duplicate-penawaran-sections').closest('.penawaran-sections').find('.purchasePenawaran'));
    $('.select2').select2();
    // callSelect2AjaxProduk(q);
    removeDelButton();
    q++;
}

function duplicateFormWhenOld() {
    var row = $('#table_penawaran_section').html();
    var clone = $(row).clone();

    clone.find('.total_produk').attr('name', 'total_produk[' + q + ']').attr('id', 'total_produk' + q).val('');
    clone.find('.produk_id').attr('name', 'produk_id[' + q + ']').attr('id', 'produk_id' + q).attr('class', 'produk_id').val('');
    clone.find('.no_produk').attr('name', 'no_produk[' + q + ']').attr('id', 'no_produk' + q).attr('onclick', 'awas(' + q + ')').attr('class', 'form-control no_produk').attr('readonly', true).val('');
    clone.find('.produk_id_temp').attr('name', 'produk_id_temp[' + q + ']').attr('id', 'produk_id_temp' + q).val('');
    clone.find('.barang_pesanan_pembelian_id').attr('name', 'barang_pesanan_pembelian_id[' + q + ']').attr('id', 'barang_pesanan_pembelian_id' + q).val('');
    clone.find('.harga_modal').attr('name', 'harga_modal[' + q + ']').attr('id', 'harga_modal' + q).val('');
    clone.find('.harga_terakhir').attr('name', 'harga_terakhir[' + q + ']').attr('id', 'harga_terakhir' + q).val('');
    clone.find('.unit_price').attr('name', 'unit_price[' + q + ']').attr('id', 'unit_price' + q).attr('onchange', 'cekQtyandPrice(' + q + ')').val('');;
    clone.find('.diskon_produk').attr('name', 'diskon_produk[' + q + ']').attr('id', 'diskon_produk' + q).val('');
    clone.find('.keterangan_produk').attr('name', 'keterangan_produk[' + q + ']').attr('id', 'keterangan_produk' + q).val('');
    clone.find('.qty_produk').attr('name', 'qty_produk[' + q + ']').attr('id', 'qty_produk' + q).attr('onchange', 'cekQtyandPrice(' + q + ')').val('');
    clone.find('.satuan_produk').attr('name', 'satuan_produk[' + q + ']').attr('id', 'satuan_produk' + q).val('');
    clone.find('.dept_id').attr('name', 'dept_q[' + q + ']').attr('id', 'dept_id' + q).val('');
    clone.find('.proyek_id').attr('name', 'proyek_q[' + q + ']').attr('id', 'proyek_id' + q).val('');
    clone.find('.gudang_id').attr('name', 'gudang_id[' + q + ']').attr('id', 'gudang_id' + q).val('');
    clone.find('.sn').attr('name', 'sn[' + q + ']').attr('id', 'sn' + q).val('');
    $(clone).appendTo($('.duplicate-penawaran-sections').closest('.penawaran-sections').find('.purchasePenawaran'));
    $('.select2').select2();
    // callSelect2AjaxProduk(q);
    removeDelButton();
    q++;
}

// Fungsi button cetak di klik
$("#btn-cetak").on("click", function (e) {
    e.preventDefault();
    btnCetak()
});

function cekQtyandPrice(param) {
    if ($("#qty_produk" + param).val() == "" || $("#qty_produk" + param).val() <= 0) {
        $("#qty_produk" + param).val(1);
        swal({
            icon: "error",
            text: "Kuantitas harus lebih besar dari 0"
        });
    }
    if ($("#unit_harga_produk" + param).val() == "") {
        $("#unit_harga_produk" + param).val(0);
    }

    let total = ($('#qty_produk' + param).val() * $('#unit_price' + param).val());
    let disc = total * $('#diskon_produk' + param).val() / 100;
    $('#total_produk' + param).val(total - disc);
}

function removeDelButton() {
    $('.remove-rincian-penawaran').not(':last').remove();
}

$("#btn-submit").on('click', function (e) {
    e.preventDefault();
    cekProdukId();
});

function cekProdukId() {
    let count = $(".produk_id").length;
    if (count <= 0) {
        swal({
            icon: "error",
            text: "Produk tidak boleh kosong",
        });
    }
}

function checkAfterSetPemasok() {
    var item_pesanan = $('.pesanan_item').length;
    if (item_pesanan > 0) {
        swal({
            title: "Peringatan!",
            text: "Pemasok ini memiliki pesanan dan belum digunakan, ingin gunakan pesanan?",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: true,
            },
        }).then((result) => {
            if (result) {
                $('#exampleModal').modal('show');
            }
        });
    }
}

function keyPressModalKodeBarang(q, x, kategori = null){
    var no_barang = x;
    var data = {
        kategoriBySearch : kategori,
        no_barang : no_barang,
        _token: token_,
    }
    $('.modal_nama_barang').val('');
    callAjaxKeypress(q, no_barang, data)
}

function keyPressModalNamaBarang(q, x, kategori){
    var keterangan = x;
    var data = {
        kategoriBySearch : kategori,
        keterangan : keterangan,
        _token: token_,
    }
    $('.modal_kode_barang').val('');
    callAjaxKeypress(q, keterangan, data)
}


function awas(q = null){
    var type = 'modal';
    $('#modalBarang').modal({
        backdrop: 'static',
        keyboard: false, 
        show: true
    });
    var row = $('#modal_thead_barang').html();
    var clone = $(row).clone();
    clone.find('.modal_kode_barang').attr('name', 'modal_kode_barang[]').attr('id', 'modal_kode_barang' + q).attr('class', 'form-control modal_kode_barang');
    clone.find('.modal_nama_barang').attr('name', 'modal_nama_barang[]').attr('id', 'modal_nama_barang' + q).attr('class', 'form-control modal_nama_barang');;
    $(clone).appendTo($('.duplicate-modal-sections').closest('.modal-sections').find('.modal_thead'));

    callSelect2AjaxProduk(q, type);
    callAjaxListGudang();

    
    $('body').keydown(function(e) {
        var keyCode = e.keyCode || e.which;
        if ( keyCode == 27 ) {
            barangBatal();
            $('#modalBarang').modal('toggle');
        }
    });
}

function barangBatal() {
    $('.modal_thead_barang_tr').remove();
    $('.table_tr_modal').remove();
}

function clearQtyGudang(){
    $('#modal_gudang_temp').val(' ');
    $('label[id*="qty_gudang_footer"]').text('');
    $('#modal_gudang').val(0).trigger('change');
}

$('#kategori_barang').change(function() {
    var searchBy = $('#searchByKategori');
    clearQtyGudang();
    if ($('#kategori_barang').val() == 1) {
        searchBy.prop('disabled', false);
        ajaxModalKategori(searchBy);

        setTimeout(function() {
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        }, 600);

        $('#searchByKategori').change(function() {
            clearQtyGudang();
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        });
    }else if($('#kategori_barang').val() == 0){
        searchBy.html(null);
        searchBy.prop('disabled', true);
        
        var kode_barang = $('.modal_kode_barang').val();
        var nama_barang = $('.modal_nama_barang').val();            
        if (kode_barang !== '' && nama_barang === '') {
            keyPressModalKodeBarang(null, kode_barang, null);
        }else if (kode_barang === '' && nama_barang !== '') {
            keyPressModalNamaBarang(null, nama_barang, null);
        }
    }
});

function ajaxModalKategori(searchBy){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-all-kategori-produk",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                searchBy.append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxKeypress(n = null, searchKey, dataKey){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-nama-produk",
        data: dataKey,
        dataType: 'JSON',
        success: function (data) {
            $('.table_tr_modal').remove();
            if (data == 'null') {
                $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="table_tr_modal">'
                        + '<td colspan="3" style="padding:5px; 20px" align="center">'
                        +   '<span class="alert alert-warning"> Tidak ada Barang dengan kata Kunci '+ searchKey +' </span>'
                        + '</td>' +
                    '</tr>').appendTo('.modal_tbody');
            }else{
                $.each(data, function (key, value) {
                    var idP = value.id;

                    $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="modal_barang_'+ key +' table_tr_modal">'
                            + '<td width="26%" style="padding:5px 15px">'
                                + value.no_barang
                            + '</td>'
                            + '<td width="3%"></td>'
                            + '<td width="55%" style="padding:5px 15px">'
                                + value.keterangan
                            + '</td>'+
                        '</tr>').appendTo('.modal_tbody');

                    $('.modal_barang_'+ key).hover(
                        function(){
                            $(this).attr('style', 'border: 2px solid limegreen; border-radius: 5px ;display:table; width:100%; table-layout:fixed;');
                        }, function() {
                            $(this).attr('style', 'border: none; display:table; width:100%; table-layout:fixed;');
                        }
                    );

                    $('.table_tr_modal').click(function () {
                        var ini = $(this);
                        if(ini.is(':hover') === true){
                            $('.table_tr_modal').removeClass('warnatr');
                            ini.addClass('warnatr');
                        }
                    });

                    $('.modal_barang_'+ key).click(function() {
                        var ini = $(this);
                        var gudang = $('#modal_gudang').val();
                        $('#modal_gudang_temp').val(idP);
                        
                        callAjaxQtyGudang(idP, gudang);
                    });

                    $('.modal_barang_'+ key).dblclick(function(){
                        setTimeout(function() {
                            clearQtyGudang();
                        }, 0);
                        $('#produk_id'+n).val(idP);
                        $('#no_produk'+n).val(value.no_barang);
                        test(idP, n);
                        barangBatal();
                        $('#modalBarang').modal('toggle');
                    });
                    
                    $('#modal_gudang').change(function() {
                        var gudang = $('#modal_gudang').val();
                        var produk = $('#modal_gudang_temp').val();

                        callAjaxQtyGudang(produk, gudang);
                    });
                });
            }
        }
    });
}

function callAjaxQtyGudang(produk, gudang){
    $.ajax({
        type : 'GET',
        url  : "" + base_url + "/ajax/get-qty-gudang-stock",
        data : {
            gudang : gudang,
            produk : produk,
            _token : token_
        },
        dataType : 'json',
        success: function (resuponse){
            $('#qty_gudang_footer').html(resuponse);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxListGudang(){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-list-gudang",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                $('#modal_gudang').append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function test(idP, vale) { // js dari blade create dan edit
    var produk_id   = idP;
    var pemasok_id   = $('#pemasok_id').val();
        $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-produk",
        data: {
            id : produk_id,
                pemasok_id : pemasok_id,
            _token: token_
        },
        dataType: "json",
        success: function(response){
            $('#gudang_id' + vale).find('option').remove().end();
            $('#keterangan_produk' + vale).val(response.keterangan)
            $('#qty_produk' + vale).val('1')
            $('#satuan_produk' + vale).val(response.satuan)
            $('#diskon_produk' + vale).val(response.diskon)
            $('#tax_produk' + vale).val(response.tax)
            $("#unit_price" + vale).val(response.unitPrice);
            let total = (response.jumlah * response.unitPrice);
            let disc = total * response.diskon / 100;
            $('#total_produk' + vale).val(total - disc);
            $(response.multiGudang).each(function (val, text) {
                $(text.gudang).each(function (index, item) {
                    $('#gudang_id' + vale).append(`<option class="form-control select2" value="${item.id}">${item.nama}</option>`);
                });
            });
            if (response.harga_modal === null) {
                $('#harga_modal' + vale).val(0);
            } else {
                $('#harga_modal' + vale).val(response.harga_modal.harga_modal);
                $('#harga_terakhir' + vale).val(response.harga_modal.harga_terakhir);
            }
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}