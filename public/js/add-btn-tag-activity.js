var base_url = window.location.origin + '/akuntansi';
var token_ = $("meta[name='csrf-token']").attr("content");

        if ( $('.tag-terms').length > 0 ) {
        $('.tag-terms').append('<a href="'+ base_url +'/syarat-pembayaran/create" class="btn btn-xs btn-primary" style="' +
                'float : right; ' +
                'margin-top : -3px;' +
                'margin-right : -3px;' +
                'padding: 3.5px 7px;"><i class="fa fa-plus"></i></a>');   
        }
        
        if ( $('.tag-ships').length > 0 ) {
                $('.tag-ships').append('<a href="'+ base_url +'/jasa-pengiriman/create" class="btn btn-xs btn-primary" style="' +
                'float : right; ' +
                'margin-top : -3px;' +
                'margin-right : -3px;' +
                'padding: 3.5px 7px;"><i class="fa fa-plus"></i></a>');   
        }

        $(document).ready( function () {                 
                $('#ship_id').select2({
                        ajax: {
                                url: "" + base_url + "/ajax/get-list-ships",
                                type: 'GET',
                                quietMillis: 1000,
                                dataType: 'JSON',
                                data: function (params) {
                                        var query = {
                                                search: params.term,
                                                type: 'public'
                                        }
                                        return query;
                                },
                                processResults: function (data) {
                                        return {
                                                results: $.map(data, function(obj) {
                                                        return {
                                                                id: obj.id,
                                                                text: obj.nama
                                                        };
                                                })
                                        };
                                }
                        }
                });

                $('#term_id').select2({
                        ajax: {
                                url: "" + base_url + "/ajax/get-list-terms",
                                type: 'GET',
                                quietMillis: 1000,
                                dataType: 'JSON',
                                data: function (params) {
                                        var query = {
                                                search: params.term,
                                                type: 'public'
                                        }
                                        return query;
                                },
                                processResults: function (data) {
                                        return {
                                                results: $.map(data, function(obj) {
                                                        return {
                                                                id: obj.id,
                                                                text: obj.nama
                                                        };
                                                })
                                        };
                                }
                        }
                });
        });