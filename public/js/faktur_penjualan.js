var base_url = window.location.origin + '/akuntansi';

var token_ = $("meta[name='csrf-token']").attr("content");
var createData = new Boolean(true);
var updateRowId = null;

var typingTimer;
var doneTypingInterval = 500;

// ajax akun di cart
$(".akun_id").change(function () {
    var id_akun = $('.akun_id').val();
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-akun_id",
        data: {
            id: id_akun,
            _token: token_
        },
        dataType: "json",
        success: function (response) {
            $('.akun_kode').val(response.kode_akun);
        },
        fail: function (errMsg) {
            alert(errMsg);
        }
    });
});


function setAkun() {
    $('.akun_id').select2({
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Ketik 3 Karakter";
        },
        ajax: {
            url: "" + base_url + "/ajax/get-nama-akun",
            type: 'GET',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            dataType: 'JSON',
            processResults: function (data) {
                return {
                    results: data
                };
            }
        }
    });
}

            function enable_cb() {
              if ($("input#group1").is(':checked')) {
                $("input.group2").removeAttr("disabled");
              } else {
                $("input.group2").attr("disabled", true).removeAttr("checked");
              }
            }

        function duplicateForm(){
            var row = $('#table_penawaran_section').html();
            var clone = $(row).clone();
                clone.find('.no_produk').attr('name', 'no_produk[]').attr('id', 'no_produk' + q).attr('onclick', 'awas(' + q + ')').attr('class', 'form-control no_produk').attr('readonly', true);
                clone.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('class', 'form-control produk_id');
                // clone.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('onchange', 'test(' + q + ')').attr('class', 'form-control produk_id');
                clone.find('.produk_id_temp').attr('name', 'produk_id_temp[]').attr('id','produk_id_temp'+q);
                clone.find('.keterangan_produk').attr('name','keterangan_produk[]').attr('id','keterangan_produk'+q);
                clone.find('.qty_produk').attr('name','qty_produk[]').attr('id','qty_produk'+q).attr('onchange','sumQtyAndUnit('+q+')');
                clone.find('.satuan_produk').attr('name','satuan_produk[]').attr('id','satuan_produk'+q);
                clone.find('.unit_harga_produk').attr('name','unit_harga_produk[]').attr('id','unit_harga_produk'+q).attr('onchange','sumQtyAndUnit('+q+')').attr('ondblclick', 'getListPriceHistory('+ q +')');;
                clone.find('.diskon_produk').attr('name','diskon_produk[]').attr('id','diskon_produk'+q).attr('onchange','sumQtyAndUnit('+q+')');
                clone.find('.tax_produk').attr('name', 'kode_pajak_id[' + q + '][]').attr('id', 'tax_produk' + q).attr('onchange', 'calculate()');
                clone.find('.amount_produk').attr('name','amount_produk[]').attr('id','amount_produk'+q).attr('readonly','');
                clone.find('.dept_id').attr('name','dept_q[]').attr('id','dept_id'+q);
                clone.find('.proyek_id').attr('name','proyek_q[]').attr('id','proyek_id'+q);
                clone.find('.barang_id').attr('name','barang_id[]').attr('id','barang_id'+q);
                clone.find('.barang_pengiriman_penjualan_id').attr('name', 'barang_pengiriman_penjualan_id[]').attr('id','barang_pengiriman_penjualan_id'+q);
                clone.find('.gudang_id').attr('name','gudang_id[]').attr('id','gudang_id'+q).attr('onchange','cekGudangStock('+q+')');
                clone.find('.sn').attr('name','sn[]').attr('id','sn'+q);
                clone.find('.no_so').attr('name','no_so[]').attr('id','no_so'+q);
                clone.find('.no_do').attr('name','no_do[]').attr('id','no_do'+q);
                clone.find('.harga_modal').attr('name','harga_modal[]').attr('id','harga_modal'+q);
                clone.find('.harga_terakhir').attr('name','harga_terakhir[]').attr('id','harga_terakhir'+q);
                clone.find('.selisih_diskon_amount').attr('name','selisih_diskon_amount[]').attr('id','selisih_diskon_amount'+q);
            $(clone).appendTo($('.duplicate-penawaran-sections').closest('.penawaran-sections').find('.purchasePenawaran'));
            // callSelect2AjaxProduk(q);
            $('.select3').select2();
            setAkun();
            removeDelButton();
            q++;
        }

        function callSelect2AjaxProduk(q = 0, type = null) {
            if(type === null){
                $('.produk_id').select2({
                    minimumInputLength: 3,
                    formatInputTooShort: function () {
                        return "Ketik 3 Karakter";
                    },
                    ajax: {
                        url: "" + base_url + "/ajax/get-nama-produk",
                        type: 'GET',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                type: 'public'
                            }
                            return query;
                        },
                        dataType: 'JSON',
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        }
                    }
                });
            }else if(type == 'modal'){
                $('.modal_kode_barang').keyup(function(e){
                    var kategori = $('#searchByKategori').val();
                    var x = $(this).val();
                    clearTimeout(typingTimer);
                    typingTimer = setTimeout( function () {
                        keyPressModalKodeBarang(q, x, kategori);
                    }, doneTypingInterval);
                });
        
                $('.modal_nama_barang').keyup(function(e){
                    var kategori = $('#searchByKategori').val();
                    var x = $(this).val();
                    clearTimeout(typingTimer);
                    typingTimer = setTimeout( function () {
                        keyPressModalNamaBarang(q, x, kategori);
                    }, doneTypingInterval);
                });
            }    
        }

        function callSelect2AjaxProdukID(q, id) {
            $.ajax({
                url: "" + base_url + "/ajax/get-id-produk",
                type: 'GET',
                data: 'search=' + id,
                dataType: 'JSON',
                success: function (data) {
                    $.each(data.item, function (key, value) {
                        $('#produk_id'+q).val(value.id);
                        $('#no_produk'+q).val(value.no_barang);
                        // $('#produk_id' + q).append('<option selected value="' + value.id + '">' + value.text + '</option>');
                        // $('#produk_id' + q).val(value.id);
                    });
                }
            });
        }

        function duplicateDP(){
            var row = $('#table_dp_section').html();
            var clone = $(row).clone();
                clone.find('.faktur_uang_muka_pelanggan_id').attr('name','faktur_uang_muka_pelanggan_id[]').attr('id','faktur_uang_muka_pelanggan_id'+x);
                clone.find('.akun_id_dp').attr('name', 'akun_id_dp[]').attr('id', 'akun_id_dp' + x);
                clone.find('.penjelasan_dp').attr('name', 'penjelasan_dp[]').attr('id', 'penjelasan_dp' + x);
                clone.find('.pajak_dp').attr('name', 'pajak_dp[' + x + '][]').attr('id','pajak_dp'+x).attr('onchange', 'calculate()');
                clone.find('.total_uang_muka_dp').attr('name', 'total_uang_muka_dp[]').attr('id', 'total_uang_muka_dp' + x).attr('onchange','counterDp()');
                clone.find('.no_faktur_dp').attr('name', 'no_faktur_dp[]').attr('id', 'no_faktur_dp' + x);
                clone.find('.no_pesanan_dp').attr('name', 'no_pesanan_dp[]').attr('id','no_pesanan_dp'+x);
                clone.find('.in_tax_dp').attr('name','in_tax_dp[]').attr('id','in_tax_dp'+x);

            $(clone).appendTo($('.duplicate-dp-sections').closest('.dp-sections').find('.dpList'));
            $('.select3').select2();
            x++;
            // callSelect2AjaxProduk();
            setAkun();
        }

        $(document).ready(function () {

            if ($('#ongkir').val() > 0) {
                $('.akun_ongkir').show(500);
            } else {
                $('.akun_ongkir').hide(500);
            }
            $('.select3').select2();

            $('.duplicate-penawaran-sections').on('click', '.add-penawaran', function () {
                checkIfEmpty();
            });
            $('.dp-sections').on('click', '.add-dp', function () {
                duplicateDP();
            });

            let exist_product = $('.produk_id').length;
            for (let a = 0; a < exist_product; a++) {
                callSelect2AjaxProdukID(a, $('#produk_id_temp' + a).val());
            }
            // callSelect2AjaxProduk();
            setAkun();
        });

        $('.duplicate-penawaran-sections').on('click', '.remove-rincian-penawaran', function () {
            if ($(this).closest('.purchasePenawaran').find('tr').length > 0) {
                $(this).closest('tr').remove();
                $('.purchasePenawaran tr:last').find('td:last').append('<button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button>');
                calculate();
                q -= 1;
            }
        });

        $('.dp-sections').on('click', '.remove-kolom-dp', function () {
            if ($(this).closest('.dpList').find('tr').length > 0) {
                $(this).closest('tr').remove();
                counterDp();
            }
        });

        $('.tanggal1').datepicker({format: 'yyyy-mm-dd', autoclose: true});
        $('.tanggal1').datepicker('setDate', new Date());

        
    function sumQtyAndUnit(param){
        cekQtyandPrice(param);
        var amount  = 0;
        let disc    = 0;
        let unit = changeMaskingToNumber($('#unit_harga_produk' + param).val());
        amount = parseInt($('#qty_produk'+param).val()) * unit;
        if (isNaN($('#diskon_produk' + param).val()) || $('#diskon_produk' + param).val() == '') {
        } else {
            disc = amount * parseFloat($('#diskon_produk' + param).val()) / 100;
        }         
        var final_amount = amount - disc;
        $('#amount_produk' + param).val(final_amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
        $('#selisih_diskon_amount'+param).val(disc);
        calculate();
        cekGudangStock(param);
    }

    function cekQtyandPrice(param) {
        if ($("#qty_produk"+ param).val() == "" || $("#qty_produk"+ param).val() <= 0) {
            $("#qty_produk"+ param).val(1);
            swal({
                icon: "error",
                text: "Barang Masih Kosong"
            });
        }
        if (changeMaskingToNumber($("#unit_harga_produk" + param).val()) == "") {
            $("#unit_harga_produk"+ param).val(0);   
        }
    }

    function calculate() {
        var subTotal            = 0;
        var grandtotal          = 0;
        var diskon              = changeMaskingToNumber($("#totalPotonganRupiah").val());
        var diskon_untuk_pajak  = changeMaskingToNumber($("#totalPotonganRupiah").val());
        var ongkir              = changeMaskingToNumber($('#ongkir').val());
        var tax                 = 0;
        var in_tax              = 0;
        $(".amount_produk").autoNumeric('init');
        /** 
         * Pajak Barang start
         **/
        var amount = [];
        var sorting = [];
        $('#taxNaration').html('');

        $(".amount_produk").each(function () {
            amount.push([changeMaskingToNumber($(this).val())]);
        });

        $(".amount_produk").each(function () {
            subTotal += parseInt(changeMaskingToNumber($(this).val()));
            // amount[$(this).index()].push($(this).val());
        });

        if ($("#diskonTotal").val() > 0) {
            diskon_untuk_pajak = subTotal * $('#diskonTotal').val() / 100;
        }
        
        // diskon_per_item = diskon_untuk_pajak / $(".produk_id").length; 
        $.each(amount, function (index, key) {
            var total_tax = 0;
            $('#tax_produk' + index + ' option:selected').each(function () {
                var str = $(this).text();
                var spl = str.split("/");
                total_tax += parseFloat(spl[0].replace(/ [0-9]*/, ''));
            });
            // var amount_after_dikurang = parseFloat(key) - diskon_per_item;
            var after_dikurang_diskon = key / subTotal * diskon_untuk_pajak; 
            $('#tax_produk' + index + ' option:selected').each(function () {
                var str = $(this).text();
                var spl = str.split("/");
                let temp_tax = (100 / (100 + total_tax) * (key - after_dikurang_diskon));
                var sort_temp = {
                    label: spl[1].replace(/ [0-9]*/, ''),
                    nilai: spl[0].replace(/ [0-9]*/, ''),
                    total: (key - after_dikurang_diskon) * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100,
                    total_in_tax: temp_tax * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100
                }
                sorting.push(sort_temp);
            });
        });

        var sorted = sorting.sort(function (a, b) {
            return a.nilai - b.nilai;
        })


        var sorted_temp = sorted;
        var unduplicateItem = [];
        for (var i = 0; i < sorted_temp.length; i++) {
            if (sorted_temp.length >= 2) {
                if (sorted_temp[i + 1] != null) {
                    if (sorted_temp[i]['nilai'] != sorted_temp[i + 1]['nilai'] && sorted_temp[i]['label'] != sorted_temp[i + 1]['label']) {
                        unduplicateItem.push(sorted_temp[i]);
                    } else {
                        sorted_temp[i + 1]['total'] = sorted_temp[i]['total'] + sorted_temp[i + 1]['total'];
                        sorted_temp[i + 1]['total_in_tax'] = sorted_temp[i]['total_in_tax'] + sorted_temp[i + 1]['total_in_tax'];
                    }
                } else {
                    unduplicateItem.push(sorted_temp[i]);
                }
            } else {
                unduplicateItem.push(sorted_temp[i]);
            }
        }

        $.each(unduplicateItem, function (index, key) {
            var tot_tax = 0;
            var tax_label = '';
            if ($('input.in_tax').is(':checked')) {
                tax += key['total_in_tax'];
                $("#taxNaration").append(key['label'] + " " + key['nilai'] + "% = " + key['total_in_tax'].toFixed(2) + "<br>");
                tot_tax = key['total_in_tax'];
                tax_label   = key['label'];
            } else {
                tax += key['total'];
                $("#taxNaration").append(key['label'] + " " + key['nilai'] + "% = " + key['total'].toFixed(2) + "<br>");
                tot_tax = key['total'];
                tax_label   = key['label'];
            }
            $("#tax_cetak_label"+index).val(tax_label);
            $("#tax_cetak"+index).val(tot_tax.toFixed(2));
        });

        if ($('input.taxable').is(':checked')) {
            $('.tax_produk').attr('disabled', false);
            $(".tax_produk").select2({
                maximumSelectionLength: 2
            });
            tax = tax;
            $('#taxNaration').show();
            if ($('input.in_tax').is(':checked')) {
                tax = 0;
                //balik lagi 0 karena harga sudah sama ongkir
            }
        } else {
            $('.tax_produk').attr('disabled', true);
            $('#taxNaration').hide();
            tax = 0;
        }

        /** 
         * Pajak Barang stop
         **/

         /** 
         * Pajak DP start
         **/

        var tax_dp = 0;
        var in_tax_dp = 0;
        
        var total_dp = [];
        var sorting_dp = [];

         $('#taxNarationDp').html('');

        $(".total_uang_muka_dp").each(function () {
            total_dp.push([$(this).val()]);
        });

        $(".total_uang_muka_dp").each(function () {
            total_dp[$(this).index()].push($(this).val());
        });

        $.each(total_dp, function (index, key) {
            var total_tax_dp = 0;
            $('#pajak_dp' + index + ' option:selected').each(function () {
                var str_dp = $(this).text();
                var spl_dp = str_dp.split("/");
                total_tax_dp += parseFloat(spl_dp[0].replace(/ [0-9]*/, ''));
            });
            $('#pajak_dp' + index + ' option:selected').each(function () {
                var str_dp = $(this).text();
                var spl_dp = str_dp.split("/");
                let temp_tax_dp = (100 / (100 + total_tax_dp) * parseFloat(key));
                var sort_temp_dp = {
                    total: parseFloat(key) * parseFloat(spl_dp[0].replace(/ [0-9]*/, '')) / 100,
                    total_in_tax: temp_tax_dp * parseFloat(spl_dp[0].replace(/ [0-9]*/, '')) / 100
                }
            });
        });
        /** 
         * Pajak DP stop
         **/
        
        if (ongkir === ''){
            ongkir = 0;
        }

        if ($("#diskonTotal").val() > 0) {
            diskon = subTotal * $('#diskonTotal').val() / 100;
        }

        grandtotal = subTotal - diskon + tax + parseInt(ongkir);
        var total_murni = subTotal - diskon + parseInt(ongkir);
        $('#subtotal').html("<strong>" + Number(subTotal).toLocaleString() + "</strong>");
        $('.kolom_subtotal').val(subTotal);
        $('.grandtotal').html("<strong>" + Number(grandtotal).toLocaleString() + "</strong>");
        $('#totalPotonganRupiah').autoNumeric('init',{aPad: false}).autoNumeric('set', diskon);
        $('.kolom_grandtotal').val(grandtotal);
        $('.kolom_totalmurni').val(total_murni);
        
        akunOngkir(ongkir);
    }

        function checkAfterSetPelanggan() {
            // var item_pengiriman = $('.pengiriman_item').length;
            // callBackDp();
            // if(item_pengiriman > 0){
            //     swal({
            //         title: "Peringatan!",
            //         text: "Pelanggan ini memiliki pengiriman dan belum digunakan, ingin gunakan pengiriman?",
            //         icon: "warning",
            //         buttons: {
            //             cancel: true,
            //             confirm: true,
            //         },
            //     }).then((result) => {
            //         if (result) {
            //             $(".pengiriman_item").each(function () {
            //                 $(this).prop('checked', true);
            //             });
            //             filterSelectedMethod("pengiriman");
            //         }
            //         callBackDp();
            //     });
            // }
        }

        function callBackDp() {
            var item_dp = $('.uangmuka_item').length;
            if (item_dp > 0) {
                swal({
                    title: "Peringatan!",
                    text: "Pelanggan ini memiliki uang muka dan belum digunakan, ingin gunakan uang muka?",
                    icon: "warning",
                    buttons: {
                        cancel: true,
                        confirm: true,
                    },
                }).then((result) => {
                    if (result) {
                        $(".uangmuka_item").each(function () {
                            $(this).prop('checked', true);
                        });
                        $("#uang-muka").modal('show');
                    }
                });
            }
        }

        function checkIfEmpty() {
            if ($('#pelanggan_idk').val() === ""){
                swal({
                    title: "Kesalahan!",
                    text: "Anda belum memilih pelanggan!",
                    icon: "error",
                    buttons: {
                        confirm: true,
                    },
                })
            }else{
                duplicateForm();                
            }
        }
        // $('#btn-submit').on('click',function(e){
        //     var item_uangmuka = $('.uangmuka_item').length;
        //     var checkCounted = $('.detail_informasi_pelanggan').length;
        //     if(item_uangmuka > 0){
        //         if(checkCounted < 1){
        //             e.preventDefault();
        //             var form = $(this).parents('form');
        //             swal({
        //                 title: "Peringatan!",
        //                 text: "Pelanggan ini memiliki DP dan tidak digunakan, lanjutkan penyimpanan?",
        //                 icon: "warning",
        //                 buttons: {
        //                     cancel: true,
        //                     confirm: true,
        //                 },
        //             }).then((result) => {
        //                 if (result) {
        //                     $('.form').submit();
        //                 }
        //             });
        //         }            
        //     }
        // });


function akunOngkir(val) {
    if (val > 0) {
        $('.akun_ongkir').show(500);
    } else {
        $('.akun_ongkir').hide(500);
    }
}

function removeDelButton() {
    $('.remove-rincian-penawaran').not(':last').remove();
}

/** Fungsi Filter Barang*/
function keyPressModalKodeBarang(q, x, kategori = null){
    var no_barang = x;
    var data = {
        kategoriBySearch : kategori,
        no_barang : no_barang,
        _token: token_,
    }
    $('.modal_nama_barang').val('');
    callAjaxKeypress(q, no_barang, data)
}

function keyPressModalNamaBarang(q, x, kategori){
    var keterangan = x;
    var data = {
        kategoriBySearch : kategori,
        keterangan : keterangan,
        _token: token_,
    }
    $('.modal_kode_barang').val('');
    callAjaxKeypress(q, keterangan, data)
}


function awas(q = null){
    var type = 'modal';
    $('#modalBarang').modal({
        backdrop: 'static',
        keyboard: false, 
        show: true
    });
    var row = $('#modal_thead_barang').html();
    var clone = $(row).clone();
    clone.find('.modal_kode_barang').attr('name', 'modal_kode_barang[]').attr('id', 'modal_kode_barang' + q).attr('class', 'form-control modal_kode_barang');
    clone.find('.modal_nama_barang').attr('name', 'modal_nama_barang[]').attr('id', 'modal_nama_barang' + q).attr('class', 'form-control modal_nama_barang');;
    $(clone).appendTo($('.duplicate-modal-sections').closest('.modal-sections').find('.modal_thead'));

    callSelect2AjaxProduk(q, type);
    callAjaxListGudang();

    $('body').keydown(function(e) {
        var keyCode = e.keyCode || e.which;
        if ( keyCode == 27 ) {
            barangBatal();
            $('#modalBarang').modal('toggle');
        }
    });
}

function barangBatal() {
    $('.modal_thead_barang_tr').remove();
    $('.table_tr_modal').remove();
}

function clearQtyGudang(){
    $('#modal_gudang_temp').val(' ');
    $('label[id*="qty_gudang_footer"]').text('');
    $('#modal_gudang').val(0).trigger('change');
}

$('#kategori_barang').change(function() {
    var searchBy = $('#searchByKategori');
    clearQtyGudang();
    if ($('#kategori_barang').val() == 1) {
        searchBy.prop('disabled', false);
        ajaxModalKategori(searchBy);

        setTimeout(function() {
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        }, 600);

        $('#searchByKategori').change(function() {
            clearQtyGudang();
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        });
    }else if($('#kategori_barang').val() == 0){
        searchBy.html(null);
        searchBy.prop('disabled', true);
        
        var kode_barang = $('.modal_kode_barang').val();
        var nama_barang = $('.modal_nama_barang').val();            
        if (kode_barang !== '' && nama_barang === '') {
            keyPressModalKodeBarang(null, kode_barang, null);
        }else if (kode_barang === '' && nama_barang !== '') {
            keyPressModalNamaBarang(null, nama_barang, null);
        }
    }
});

function ajaxModalKategori(searchBy){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-all-kategori-produk",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                searchBy.append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxKeypress(n = null, searchKey, dataKey){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-nama-produk",
        data: dataKey,
        dataType: 'JSON',
        success: function (data) {
            $('.table_tr_modal').remove();
            if (data == 'null') {
                $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="table_tr_modal">'
                        + '<td colspan="3" style="padding:5px; 20px" align="center">'
                        +   '<span class="alert alert-warning"> Tidak ada Barang dengan kata Kunci '+ searchKey +' </span>'
                        + '</td>' +
                    '</tr>').appendTo('.modal_tbody');
            }else{
                $.each(data, function (key, value) {
                    var idP = value.id;

                    $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="modal_barang_'+ key +' table_tr_modal">'
                            + '<td width="26%" style="padding:5px 15px">'
                                + value.no_barang
                            + '</td>'
                            + '<td width="3%"></td>'
                            + '<td width="55%" style="padding:5px 15px">'
                                + value.keterangan
                            + '</td>'+
                        '</tr>').appendTo('.modal_tbody');

                    $('.modal_barang_'+ key).hover(
                        function(){
                            $(this).attr('style', 'border: 2px solid limegreen; border-radius: 5px ;display:table; width:100%; table-layout:fixed;');
                        }, function() {
                            $(this).attr('style', 'border: none; display:table; width:100%; table-layout:fixed;');
                        }
                    );

                    $('.table_tr_modal').click(function () {
                        var ini = $(this);
                        if(ini.is(':hover') === true){
                            $('.table_tr_modal').removeClass('warnatr');
                            ini.addClass('warnatr');
                        }
                    });

                    $('.modal_barang_'+ key).click(function() {
                        var ini = $(this);
                        var gudang = $('#modal_gudang').val();
                        $('#modal_gudang_temp').val(idP);
                        
                        callAjaxQtyGudang(idP, gudang);
                    });

                    $('.modal_barang_'+ key).dblclick(function(){
                        setTimeout(function() {
                            clearQtyGudang();
                        }, 0);
                        $('#produk_id'+n).val(idP);
                        $('#no_produk'+n).val(value.no_barang);
                        test(idP, n);
                        barangBatal();
                        $('#modalBarang').modal('toggle');
                    });
                    
                    $('#modal_gudang').change(function() {
                        var gudang = $('#modal_gudang').val();
                        var produk = $('#modal_gudang_temp').val();
                        clearTimeout(typingTimer);
                        typingTimer = setTimeout( function () {
                            callAjaxQtyGudang(produk, gudang);
                        }, doneTypingInterval);
                    });
                });
            }
        }
    });
}

function callAjaxQtyGudang(produk, gudang){
    $.ajax({
        type : 'GET',
        url  : "" + base_url + "/ajax/get-qty-gudang-stock",
        data : {
            gudang : gudang,
            produk : produk,
            _token : token_
        },
        dataType : 'json',
        success: function (resuponse){
            $('#qty_gudang_footer').html(resuponse);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxListGudang(){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-list-gudang",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                $('#modal_gudang').append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function test(idP, vale) { // js dari blade create dan edit
    var pelanggan_id   = $('#pelanggan_id').val();
        $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-produk",
        data: {
            id : idP,
                pelanggan_id : pelanggan_id,
            _token: token_
        },
        dataType: "json",
        success: function(response){
            $('#tax_produk'+vale).find('option').remove().end();
            $('#gudang_id'+vale).find('option').remove().end();
            $('#keterangan_produk'+vale).val(response.keterangan);
            $('#qty_produk'+vale).val('1');
            $('#satuan_produk'+vale).val(response.satuan);
            $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
            $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0);
            $('#diskon_produk'+vale).val('0');
            $('#selisih_diskon_amount'+vale).val(0);
            if (response.harga_modal !== null) {
                $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
                $('#harga_terakhir'+vale).val(response.harga_modal.harga_terakhir);
            }else {
                $('#harga_modal'+vale).val(0);
            }
            $.each(response.tax, function (index, item){
                $('#tax_produk'+vale).append( 
                $("<option></option>")
                .text(item.nilai+ "/" +item.nama)
                .val(item.id)
                );
            });
            $(response.multiGudang).each(function (val, text) {
                $(text.gudang).each(function (index, item) {
                    $('#gudang_id'+vale).append(`<option value="${item.id}">${item.nama}</option>`);
                });
            });
            sumQtyAndUnit(vale);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function getListPriceHistory(q) {
    var pelanggan       = $('#pelanggan_id').val();
    var produk          = $('#produk_id' + q).val();
    var harga           = $('#unit_harga_produk' + q);

    if (pelanggan == '' || pelanggan == null) {
        swal({
            icon: "error",
            text: "Pelanggan Tidak Boleh Kosong!"
        });
    }else if ( produk == '' || produk == null ) {
        swal({  
            icon: "error",
            text: "Produk Tidak Boleh Kosong!"
        });
    } else {
        $('#modalPriceHistory').modal({
            backdrop: 'static',
            keyboard: false, 
            show: true
        });
        $('.price_history_name').html( $('#keterangan_produk' + q).val() );
        $('.pelanggan_price_history').html( $('#pelanggan_id')[0].selectedOptions[0].text );
        $.ajax({
            type: "POST",
            url: "" + base_url + "/ajax/get-list-history-price-item",
            data:   { 
                        sov : pelanggan,
                        type : 'penjualan',
                        item : produk,
                        _token: token_
                    },
            dataType: "json",
            success: function(response){
                $.map(response, function(item, index) {
                    $(".modal_tbody_price_history").append("<tr class='post_price table_tr_modal' id='row_price_history_"+ index +"'> "+
                                                "<td>"+item['tanggal']+"</td>"+
                                                "<td>"+item['kuantitas']+"</td>"+
                                                "<td>"+item['unit']+"</td>"+
                                                "<td class='harga_history'>"+item['harga']+"</td>"+
                                                "<td>"+item['diskon']+"</td>"+
                                            "</tr>");
                    $('.harga_history').autoNumeric('init', {vMin: '0', vMax: '999999999' })
                    harga.autoNumeric('init', {vMin: '0', vMax: '999999999' })
                    $('#row_price_history_'+ index).dblclick(function(){
                        harga.autoNumeric('init').autoNumeric('set', item['harga']);
                        $('.table_tr_modal').remove();
                        $('.price_history_name').html();
                        $('#modalPriceHistory').modal('toggle');
                        sumQtyAndUnit(q);
                    });
                });
            }, failure: function(errMsg) {
                alert(errMsg);
            }
        });
    }
}
