var base_url = window.location.origin + '/akuntansi';
var token_ = $("meta[name='csrf-token']").attr("content");

function checkMataUang(type) {
    var arrayData = [];
    if (type == 'mata-uang') {
        arrayData = [
            "akun_hutang_id",
            "akun_piutang_id",
            "uang_muka_pembelian_id",
            "uang_muka_penjualan_id",
            "diskon_penjualan_id",
            "laba_rugi_terealisir_id",
            "laba_rugi_tak_terealisir_id",
            "akun_penyesuaian_id",
        ];
    }else if(type == 'barang') {
        arrayData = [
            "akun_persediaan_id",
            "akun_penjualan_id",
            "akun_retur_penjualan_id",
            "akun_diskon_barang_id",
            "akun_barang_terkirim_id",
            "akun_hpp_id",
            "akun_retur_pembelian_id",
            "akun_beban_id",
            "akun_belum_tertagih_id",
        ];
    }
    $.each(arrayData, function(index, val) {        
        var replacement = val.replace(/id/g, "nama");
        if ($('.' + val).find(":selected").val() != null && $('.'+replacement).val() == ''){
            changeNamaAkun($('.' + val).find(":selected").val(), replacement);            
        }        
    })
    
}

function changeNamaAkun(id, target) {
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-akun_id_penerimaan",
        data: {
            id: id,
            _token: token_
        },
        dataType: "json",
        success: function (response) {
            $('#akun_id').val(response.kode_akun)
            $('input[name="' + target + '"]').val(response.kode_akun);
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
}