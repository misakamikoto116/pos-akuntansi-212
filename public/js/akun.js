$(document).ready(function () {
    hideAll();
    if ($('.use_parent').is(":checked")){
        $('.parent-row').show(300);
    }else{
        $('.parent-row').hide(300);
    }
    
    $('.form-horizontal').on('change', '#tipe_akun_id', function () {
        trigger(); 
    });
    trigger(); 
});

function hideAll() {
    $('.alokasi_ke_produksi').hide();
    $('.fiksal').hide();
    $('.saldo_awal').hide();
    $('.mata_uang').hide();
    $('.tanggal_masuk').hide();
}

function trigger() {
    hideAll();
    var thisvalue = $('#tipe_akun_id').find("option:selected").text();

    $('#tipe_akun_nama').val(thisvalue);

    if (thisvalue === 'Kas/Bank') {
        $('.saldo_awal').show(500);
        $('.mata_uang').show(500);
        $('.tanggal_masuk').show(500);
    } else if (thisvalue === 'Akun Piutang') {
        $('.mata_uang').show(500);
    } else if (thisvalue === 'Persediaan') {
        $('.saldo_awal').show(500);
        $('.tanggal_masuk').show(500);
    } else if (thisvalue === 'Aktiva Lancar') {
        $('.saldo_awal').show(500);
        $('.mata_uang').show(500);
        $('.tanggal_masuk').show(500);
    } else if (thisvalue === 'Aktiva Lainnya') {
        $('.saldo_awal').show(500);
        $('.mata_uang').show(500);
        $('.tanggal_masuk').show(500);
    } else if (thisvalue === 'Akun Hutang') {
        $('.mata_uang').show(500);
        $('.tanggal_masuk').show(500);
    } else if (thisvalue === 'Hutang Lancar Lainnya') {
        $('.saldo_awal').show(500);
        $('.mata_uang').show(500);
        $('.tanggal_masuk').show(500);
    } else if (thisvalue === 'Ekuitas') {
        $('.saldo_awal').show(500);
        $('.tanggal_masuk').show(500);
    } else if (thisvalue === 'Pendapatan') {
        $('.saldo_awal').show(500);
        $('.fiksal').show(500);
        $('.tanggal_masuk').show(500);
    } else if (thisvalue === 'Harga Pokok Penjualan') {
        $('.saldo_awal').show(500);
        $('.alokasi_ke_produksi').show(500);
        $('.fiksal').show(500);
        $('.tanggal_masuk').show(500);
    } else if (thisvalue === 'Pendapatan Lain') {
        $('.saldo_awal').show(500);
        $('.tanggal_masuk').show(500);
        $('.fiksal').show(500);
    } else if (thisvalue === 'Hutang Jangka Panjang') {
        $('.saldo_awal').show(500);
        $('.mata_uang').show(500);
        $('.tanggal_masuk').show(500);
    } else if (thisvalue === 'Beban' || thisvalue === 'Beban lain-lain') {
        $('.saldo_awal').show(500);
        $('.fiksal').show(500);
        $('.tanggal_masuk').show(500);
        $('.alokasi_ke_produksi').show(500);
    }
}

$('.use_parent').on('change',function(){
    if ($('.use_parent').is(":checked")){
        $('.parent-row').show(300);
    }else{
        $('.parent-row').hide(300);
    }
});