$(document).ready(function(){
    $('.warn_pass').hide();
    formlistRole();
});
$('.password').on('change', function(){
    if(this.value != $('.password2').val()){
        $('.btn-submit').prop('disabled',true);
        $('.warn_pass').show(500);
    } else {
        $('.btn-submit').prop('disabled',false);
        $('.warn_pass').hide(500);
    }
});

$('.password2').on('change', function(){
    if(this.value != $('.password').val()){
        $('.btn-submit').prop('disabled',true);
        $('.warn_pass').show(500);
    } else {
        $('.btn-submit').prop('disabled',false);
        $('.warn_pass').hide(500);
    }
});

$('#role').change( function (e) {
    formlistRole();
});

function formlistRole() {
    if($('#role').val() == 'akuntansi'){
        $('#list-role').show();
    }else{
        $('#list-role').hide();
    }
}

// $("#checkAll").click(function(){
//     $('input[type="checkbox"]').not(this).prop('checked', this.checked);
// });