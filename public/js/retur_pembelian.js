var base_url = window.location.origin + '/akuntansi';

var token_ = $("meta[name='csrf-token']").attr("content");
var createData = new Boolean(true);
var updateRowId = null;

function enable_cb() {
  if ($("input#group1").is(':checked')) {
    $("input.group2").removeAttr("disabled");
    $(".tax").show();
    $("#no_fiscal_id").removeAttr("disabled");
    $("#tgl_fiscal_id").removeAttr("disabled");
  } else {
    $("input.group2").attr("disabled", true).removeAttr("checked");
    $("#no_fiscal_id").attr("disabled", true);
    $("#tgl_fiscal_id").attr("disabled", true);
    $(".tax").hide();
  }
}

$(function () {
    let exist_product = $('.produk_id').length;
    for (let a = 0; a < exist_product; a++) {
        callSelect2AjaxProdukID(a, $('#produk_id_temp' + a).val());
        // callSelect2AjaxProduk(a);
    } 
    $('.select3').select2();
});
        
function callSelect2AjaxProduk(q) {
    $('.produk_id').select2({
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Ketik 3 Karakter";
        },
        ajax: {
            url: "" + base_url + "/ajax/get-nama-produk",
            type: 'GET',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            dataType: 'JSON',
            processResults: function (data) {
                return {
                    results: data
                };
            }
        }
    });    
}

function callSelect2AjaxProdukID(q, id) {
    $.ajax({
        url: "" + base_url + "/ajax/get-id-produk",
        type: 'GET',
        data: 'search=' + id,
        dataType: 'JSON',
        success: function (data) {
            $.each(data.item, function (key, value) {
                $('#no_produk'+ q).val(value.text); 
                $('#produk_id'+ q).val(value.id); 
                // $('#produk_id' + q).append('<option selected value="' + value.id + '">' + value.text + '</option>');
            });
        }
    });
}

$('.duplicate-faktur-sections').on('click', '.remove-rincian-penawaran', function () {
    if ($(this).closest('.purchaseFaktur').find('tr').length > 0) {
        $(this).closest('tr').remove();
        calculate();
        q -= 1;
    }
});

$('.tanggal1').datepicker({ format: 'dd MM yyyy', autoclose: true });
$('.tanggal1').datepicker('setDate', new Date());

$(".list-purchase-invoice").change(function () {
    filterSelectedId($(this).val());
});

function duplicateForm() {
    var row = $('#table_faktur_section').html();
    var clone = $(row).clone();
    clone.find('.barang_faktur_pembelian_id').attr('name', 'barang_faktur_pembelian_id[]').attr('id', 'barang_faktur_pembelian_id' + q);
    clone.find('.no_produk').attr('name', 'no_produk[]').attr('id', 'no_produk' + q).attr('class', 'form-control no_produk');
    clone.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('class', 'form-control produk_id');
    clone.find('.keterangan_produk').attr('name', 'keterangan_produk[]').attr('id', 'keterangan_produk' + q);
    clone.find('.qty_produk').attr('name', 'qty_produk[]').attr('id', 'qty_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')');
    clone.find('.satuan_produk').attr('name', 'satuan_produk[]').attr('id', 'satuan_produk' + q);
    clone.find('.unit_harga_produk').attr('name', 'unit_harga_produk[]').attr('id', 'unit_harga_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')').attr('ondblclick', 'getListPriceHistory('+ q +')');
    clone.find('.tax_produk').attr('name', 'tax_produk[' + q + '][]').attr('id', 'tax_produk' + q).attr('onchange', 'calculate()');
    clone.find('.amount_produk').attr('name', 'amount_produk[]').attr('id', 'amount_produk' + q).attr('readonly', '');
    clone.find('.dept_id').attr('name', 'dept_q[]').attr('id', 'dept_id' + q);
    clone.find('.proyek_id').attr('name', 'proyek_q[]').attr('id', 'proyek_id' + q);
    clone.find('.gudang_id').attr('name', 'gudang_id[]').attr('id', 'gudang_id' + q);
    clone.find('.sn').attr('name', 'sn[]').attr('id', 'sn' + q);
    clone.find('.harga_modal').attr('name', 'harga_modal[]').attr('id', 'harga_modal' + q);
    clone.find('.harga_terakhir').attr('name', 'harga_terakhir[]').attr('id', 'harga_terakhir' + q);
    $(clone).appendTo($('.duplicate-faktur-sections').closest('.faktur-sections').find('.purchaseFaktur'));
    $('.select3').select2();
    // callSelect2AjaxProduk(q);
    q++;
}

function sumQtyAndUnit(param) {
    cekQtyandPrice(param);
    var amount = 0;

    amount = parseInt($('#qty_produk' + param).val()) * parseInt(changeMaskingToNumber($('#unit_harga_produk' + param).val()));

    $('#amount_produk' + param).val(amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
    calculate();
    cekGudangStock(param);
}

function calculate() {
    var subTotal = 0;
    var grandtotal = 0;
    var tax = 0;
    var in_tax = 0;
    var amount = [];
    var sorting = [];
    $('#taxNaration').html('');

    $(".amount_produk").each(function () {
        amount.push([changeMaskingToNumber($(this).val())]);
    });

    $.each(amount, function (index, key) {
        var total_tax = 0;
        $('#tax_produk' + index + ' option:selected').each(function () {
            var str = $(this).text();
            var spl = str.split("/");
            total_tax += parseFloat(spl[0].replace(/ [0-9]*/, ''));
        });
        $('#tax_produk' + index + ' option:selected').each(function () {
            var str = $(this).text();
            var spl = str.split("/");
            let temp_tax = (100 / (100 + total_tax) * parseFloat(key));

            var sort_temp = {
                label: spl[1].replace(/ [0-9]*/, ''),
                nilai: spl[0].replace(/ [0-9]*/, ''),
                total: parseFloat(key) * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100,
                total_in_tax: temp_tax * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100
            }
            sorting.push(sort_temp);
        });
    });

    var sorted = sorting.sort(function (a, b) {
        return a.nilai - b.nilai;
    })

    var sorted_temp = sorted;
    var unduplicateItem = [];
    for (var i = 0; i < sorted_temp.length; i++) {
        if (sorted_temp.length >= 2) {
            if (sorted_temp[i + 1] != null) {
                if (sorted_temp[i]['nilai'] != sorted_temp[i + 1]['nilai'] && sorted_temp[i]['label'] != sorted_temp[i + 1]['label']) {
                    unduplicateItem.push(sorted_temp[i]);
                } else {
                    sorted_temp[i + 1]['total'] = sorted_temp[i]['total'] + sorted_temp[i + 1]['total'];
                    sorted_temp[i + 1]['total_in_tax'] = sorted_temp[i]['total_in_tax'] + sorted_temp[i + 1]['total_in_tax'];
                }
            } else {
                unduplicateItem.push(sorted_temp[i]);
            }
        } else {
            unduplicateItem.push(sorted_temp[i]);
        }
    }

    $(".amount_produk").each(function () {
        subTotal += parseInt(changeMaskingToNumber($(this).val()));
        amount[$(this).index()].push(changeMaskingToNumber($(this).val()));
    });
    $.each(unduplicateItem, function (index, key) {
        var tot_tax = 0;
        if ($('input.in_tax').is(':checked')) {
            tax += key['total_in_tax'];
            $("#taxNaration").append(key['label'] + " " + key['nilai'] + "% = " + key['total_in_tax'].toFixed(2) + "<br>");
            tot_tax     = key['total_in_tax'];
        } else {
            tax += key['total'];
            $("#taxNaration").append(key['label'] + " " + key['nilai'] + "% = " + key['total'].toFixed(2) + "<br>");
            tot_tax = key['total'];
        }
        $("#tax_cetak"+index).val(tot_tax.toFixed(2));
    });

    if ($('input.taxable').is(':checked')) {
        $('.tax_produk').attr('disabled', false);
        $(".tax_produk").select2({
            maximumSelectionLength: 2
        });
        tax = tax;
        $('#taxNaration').show();
        if ($('input.in_tax').is(':checked')) {
            tax = 0;
            //balik lagi 0 karena harga sudah sama ongkir
        }
    } else {
        $('.tax_produk').attr('disabled', true);
        $('#taxNaration').hide();
        tax = 0;
    }

    grandtotal = subTotal + tax;

    $('#subtotal').html("<strong>" + Number(subTotal).toLocaleString() + "</strong>");
    $('.grandtotal').html("<strong>" + Number(grandtotal).toLocaleString() + "</strong>");
    $('#grandtotal').val(grandtotal);

}


function cekGudangStock(param) {
    var goSubmit = 0;
    var gudang_stock_id = $('#gudang_id'+ param).val();
    var produk_stock_id = $('#produk_id'+ param).val();
    var qty_stock_id    = $('#qty_produk'+ param).val();
    $.ajax({
        type: "GET",
        url: ""+base_url+"/ajax/get-gudang-stock",
        data: {
            gudang_id : gudang_stock_id, produk_id : produk_stock_id, qty_id: qty_stock_id , _token: token_
        },
        dataType: "json",
        success: function (response) {
            if (response.status === 0) {
                swal({
                    icon: 'error',
                    text: 'Stock '+ $("#keterangan_produk"+ param).val() +' di '+ response.gudang +' tersisa '+ response.kuantitas +' harap masukkan jumlah yang sesuai !!!'  
                });
            }
        }, failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function cekQtyandPrice(param) {
    if ($("#qty_produk"+ param).val() == "" || $("#qty_produk"+ param).val() <= 0) {
        $("#qty_produk"+ param).val(1);
        swal({
            icon: "error",
            text: "Barang Masih Kosong"
        });
    }
    if (changeMaskingToNumber($("#unit_harga_produk"+ param).val()) == "") {
        $("#unit_harga_produk"+ param).val(0);   
    }
}

function getListPriceHistory(q) {
    var pemasok         = $('#pemasok_id').val();
    var produk          = $('#produk_id' + q).val();
    var harga           = $('#unit_harga_produk' + q);

    if (pemasok == '' || pemasok == null) {
        swal({
            icon: "error",
            text: "Pemasok Tidak Boleh Kosong!"
        });
    }else if ( produk == '' || produk == null ) {
        swal({  
            icon: "error",
            text: "Produk Tidak Boleh Kosong!"
        });
    } else {
        $('#modalPriceHistory').modal({
            backdrop: 'static',
            keyboard: false, 
            show: true
        });
        $('.price_history_name').html( $('#keterangan_produk' + q).val() );
        $('.pelanggan_price_history').html( $('#pemasok_id')[0].selectedOptions[0].text );
        $.ajax({
            type: "POST",
            url: "" + base_url + "/ajax/get-list-history-price-item",
            data:   { 
                        sov : pemasok,
                        type : 'pembelian',
                        item : produk,
                        _token: token_
                    },
            dataType: "json",
            success: function(response){
                $.map(response, function(item, index) {
                    $(".modal_tbody_price_history").append("<tr class='post_price table_tr_modal' id='row_price_history_"+ index +"'> "+
                                                "<td>"+item['tanggal']+"</td>"+
                                                "<td>"+item['kuantitas']+"</td>"+
                                                "<td>"+item['unit']+"</td>"+
                                                "<td class='harga_history'>"+item['harga']+"</td>"+
                                                "<td>"+item['diskon']+"</td>"+
                                            "</tr>");
                    $('.harga_history').autoNumeric('init', {vMin: '0', vMax: '999999999' })
                    harga.autoNumeric('init', {vMin: '0', vMax: '999999999' })
                    $('#row_price_history_'+ index).dblclick(function(){
                        harga.autoNumeric('init').autoNumeric('set', item['harga']);
                        $('.table_tr_modal').remove();
                        $('#modalPriceHistory').modal('toggle');
                        sumQtyAndUnit(q);
                    });
                });
            }, failure: function(errMsg) {
                alert(errMsg);
            }
        });
    }
}