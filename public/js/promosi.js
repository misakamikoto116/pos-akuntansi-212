var base_url = window.location.origin + '/akuntansi';
var token_ = $("meta[name='csrf-token']").attr("content");
var q      = 0;
var r      = 0;
var x      = 0;
var y      = 0;

$(document).ready(function () {
    $('#duplicate_sebab').hide();
    $("#add-sebab").on('click', function () {
        $('#duplicate_sebab').show();
        duplicateFormSebab();
    });

    $('#duplicate_akibat').hide();
    $("#add-akibat").on('click', function () {
        $('#duplicate_akibat').show();
        duplicateFormAkibatPromosi();
    });

    $("#reset").click(function () {
        $(".table-sebab tbody").empty();
        $('#duplicate_sebab').hide();
        $(".table-akibat tbody").empty();
        $('#duplicate_akibat').hide();
        q = 0;
        r = 0;
        x = 0;
        y = 0;
    });
});




function callSelect2AjaxProduk() {
    $('.produk_id').select2({
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Ketik 3 Karakter";
        },
        ajax: {
            url: "" + base_url + "/ajax/get-nama-produk",
            type: 'GET',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            dataType: 'JSON',
            processResults: function (data) {
                return {
                    results: data
                };
            }
        }
    });
}

function checkItem(theSelect, idTotal) {
    $.ajax({
        url: "" + base_url + "/ajax/get-id-produk",
        type: 'GET',
        data: 'search=' + theSelect.value,
        dataType: 'JSON',
        success: function (response) {
            $.each(response.item, function (key, value) {
                theSelect.append('<option selected value="' + value.id + '">' + value.text + '</option>');
            });
            $('#total' + idTotal).val(response.harga); 
        }
    });
}

function duplicateFormSebab() {
    $('.produk_id').select2('destroy');
    var row_sebab       = $("#template_sebab").html();
    var clone_sebab     = $(row_sebab).clone();
        clone_sebab.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('onchange', 'checkItem(this,'+ q +')'); 
        clone_sebab.find('.qty').attr('name', 'qty[]').attr('id', 'qty' + q);  
        clone_sebab.find('.total').attr('name', 'total[]').attr('id', 'total' + q); 
        clone_sebab.find('.type_sebab').attr('name', 'type_sebab[]').attr('id', 'type_sebab' + q); 
        clone_sebab.find('.kelipatan').attr('name', 'kelipatan[]').attr('id', 'kelipatan' + q); 
        clone_sebab.find('.tipe_barang').attr('name', 'tipe_barang[]').attr('id', 'tipe_barang' + q); 
        clone_sebab.find('.add-sebab-field').attr('id', 'add-sebab-field' + q).attr('onclick', 'addColAkibat('+ q +')'); 
        clone_sebab.find('.rowAkibat').attr('id', 'rowAkibat' + q);
    $(clone_sebab).appendTo($('.table-sebab').closest('#duplicate_sebab').find('#table_sebab'));
    callSelect2AjaxProduk();
    q++;
    $(".select3").select2();
}

function duplicateFormAkibat(q, r) {
    $('.produk_id').select2('destroy');
    var row_akibat       = $("#template_akibat").html();
    var clone_akibat     = $(row_akibat).clone();
        clone_akibat.find('.produk_id').attr('name', 'produk_id_akibat[' + q + ']['+ r +']').attr('id', 'produk_id_akibat' + q + r).attr('onchange', 'checkItem(this, "_akibat' + q + r +'")'); 
        clone_akibat.find('.qty_akibat').attr('name', 'qty_akibat['+ q +']['+ r +']').attr('id', 'qty_akibat' + q + r); 
        clone_akibat.find('.presentase_akibat').attr('name', 'presentase_akibat['+ q +']['+ r +']').attr('id', 'presentase_akibat' + q + r); 
        clone_akibat.find('.total_akibat').attr('name', 'total_akibat['+ q +']['+ r +']').attr('id', 'total_akibat' + q); 
    $(clone_akibat).appendTo($('#rowAkibat' + q));
    callSelect2AjaxProduk();
}

function addColAkibat(q) {
    duplicateFormAkibat(q, r);
    q++;
    r++;
}

// Akibat
function duplicateFormAkibatPromosi() {
    $('.produk_id').select2('destroy');
    var row_akibat       = $("#template_akibat_promosi").html();
    var clone_akibat     = $(row_akibat).clone();
        clone_akibat.find('.produk_id').attr('name', 'produk_id_akibat_promosi[' + x + ']').attr('id', 'produk_id_akibat_promosi' + x).attr('onchange', 'checkItem(this, "_akibat_promosi' + x +'")'); 
        clone_akibat.find('.qty_sebab').attr('name', 'qty_sebab['+ x +']').attr('id', 'qty_sebab' + x); 
        clone_akibat.find('.presentase_sebab').attr('name', 'presentase_sebab['+ x +']').attr('id', 'presentase_sebab' + x); 

        clone_akibat.find('.type_sebab_promosi').attr('name', 'type_sebab_promosi[' + x + ']').attr('id', 'type_sebab_promosi' + x);
        clone_akibat.find('.kelipatan_sebab_promosi').attr('name', 'kelipatan_sebab_promosi[' + x + ']').attr('id', 'kelipatan_sebab_promosi' + x); 
        clone_akibat.find('.total_akibat_promosi').attr('name', 'total_akibat_promosi[' + x + ']').attr('id', 'total_akibat_promosi' + x); 
        clone_akibat.find('.add-akibat-field').attr('id', 'add-akibat-field' + x).attr('onclick', 'addColSebab('+ x +')');
        clone_akibat.find('.rowSebab').attr('id', 'rowSebab' + x);
    $(clone_akibat).appendTo($('.table-akibat').closest('#duplicate_akibat').find('#table_akibat'));
    callSelect2AjaxProduk();
    x++;
    $(".select3").select2();
}

function duplicateFormSebabPromosi(x, y) {
    $('.produk_id').select2('destroy');
    var row_sebab       = $("#template_sebab_promosi").html();
    var clone_sebab     = $(row_sebab).clone();
        clone_sebab.find('.produk_id').attr('name', 'produk_id_sebab_promosi[' + x + '][' + y + ']').attr('id', 'produk_id_sebab_promosi' + x + y).attr('onchange', 'checkItem(this, "_sebab_promosi' + x + y +'")'); 
        clone_sebab.find('.qty_sebab_promosi').attr('name', 'qty_sebab_promosi[' + x + '][' + y + ']').attr('id', 'qty_sebab_promosi' + x + y);  
        clone_sebab.find('.total_sebab_promosi').attr('name', 'total_sebab_promosi['+ x +']['+ y +']').attr('id', 'total_sebab_promosi' + x + y); 
    $(clone_sebab).appendTo($('#rowSebab' + x));
    callSelect2AjaxProduk();
    $(".select3").select2();
}

function addColSebab(x) {
    duplicateFormSebabPromosi(x, y);
    x++;
    y++;
}