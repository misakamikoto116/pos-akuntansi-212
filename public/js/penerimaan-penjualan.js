var base_url = window.location.origin+'/akuntansi';
var token_ = $("meta[name='csrf-token']").attr("content")

function enable_cb() {
    if (this.checked) {
        $("input.group2").removeAttr("disabled");
    } else {
        $("input.group2").attr("disabled", true).removeAttr("checked");
    }
}

$(function(){
    setAkun();
    $('.select3').select2();
});

function setAkun() {
    $('#akun_id').select2({
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Ketik 3 Karakter";
        },
        ajax: {
            url: "" + base_url + "/ajax/get-nama-akun",
            type: 'GET',
            data: function (params) {
                var query = {
                    search: params.term,
                    kas: 1,
                    type: 'public'
                }
                return query;
            },
            dataType: 'JSON',
            processResults: function (data) {
                return {
                    results: data
                };
            }
        }
    });
}

function duplicateForm() {
    var row = $('#table_invoice_section').html();
    var clone = $(row).clone();
    clone.find('.faktur_id').attr('name', 'faktur_id[]').attr('id', 'faktur_id' + q);
    clone.find('.no_faktur').attr('name', 'no_faktur[]').attr('id', 'no_faktur' + q);
    clone.find('.tanggal').attr('name', 'tanggal[]').attr('id', 'tanggal' + q);
    clone.find('.amount').attr('name', 'amount[]').attr('id', 'amount' + q);
    clone.find('.owing_asli').attr('name', 'owing_asli[]').attr('id', 'owing_asli' + q);
    clone.find('.owing').attr('name', 'owing[]').attr('id', 'owing' + q);
    clone.find('.payment_amount').attr('name', 'payment_amount[]').attr('id', 'payment_amount' + q).attr('onchange', 'isPayed(this.value,' + q + ')');
    clone.find('.diskon').attr('name', 'diskon[]').attr('id', 'diskon' + q);
    clone.find('.btn-diskon').attr('id', 'btn-diskon' + q).attr('onclick', 'checkTermin('+ q +')').attr('onfocus', 'checkTermin('+ q +')');
    clone.find('.diskon_date').attr('name', 'diskon_date[]').attr('id', 'diskon_date' + q);
    clone.find('.status_pay').attr('name', 'status_pay[]').attr('id', 'status_pay' + q).attr('onchange','isPayChecked(' + q + ')');
    clone.find('.status_pay_hidden').attr('name', 'status_pay[]').attr('id', 'status_pay_hidden' + q);
    clone.find('.membayar_antara').attr('name', 'membayar_antara[]').attr('id', 'membayar_antara' + q);
    clone.find('.btn-tambah').attr('onclick', 'passingTambah(' + q + ')');
    clone.find('.btn-detail').attr('onclick', 'passingDetail(' + q + ')');
    $(clone).appendTo($('.duplicate-invoice-sections').closest('.invoice-sections').find('.listInvoice'));
    duplicateMdl(q);   
    $('.select3').select2();
    q++;
}

function duplicateMdl(q) {
    var row_mdl     = $("#mdl_termin").html();
    var clone_mdl   = $(row_mdl).clone();
    clone_mdl.attr('id','modal_termin' + q);
    clone_mdl.find('.no_faktur_mdl').attr('id', 'no_faktur_mdl' + q);    
    clone_mdl.find('.nilai_faktur_mdl').attr('id', 'nilai_faktur_mdl' + q);    
    clone_mdl.find('.nilai_terutang_mdl').attr('id', 'nilai_terutang_mdl' + q);    
    clone_mdl.find('.syarat_pembayaran_mdl').attr('id', 'syarat_pembayaran_mdl' + q);    
    clone_mdl.find('.diskon_anjuran_mdl').attr('id', 'diskon_anjuran_mdl' + q);    
    clone_mdl.find('.jml_diskon_mdl').attr('id', 'jml_diskon_mdl' + q);    
    clone_mdl.find('.akun_diskon_mdl').attr('id', 'akun_diskon_mdl' + q).attr('name', 'akun_diskon_mdl[]').attr('class', 'akun_diskon_mdl');
    clone_mdl.find('.dept_mdl').attr('id', 'dept_mdl' + q);    
    clone_mdl.find('.proyek_mdl').attr('id', 'proyek_mdl' + q);    
    clone_mdl.find('.ok_mdl').attr('id', 'ok_mdl' + q);    
    $(clone_mdl).appendTo($('.duplicate-invoice-sections').closest('.invoice-sections').find('.listInvoice'));
    $('#akun_diskon_mdl' + q).select2({
        dropdownParent: $('#modal_termin' + q)
    });
}

function isPayed(val, index) {
    $("#jml_diskon_mdl" + index).autoNumeric('init',{aPad: false});
    if ($.isNumeric(changeMaskingToNumber(val))) {
        if (parseFloat(val) <= parseFloat($("#owing"+ index).autoNumeric('get')) ) {
            if( parseFloat($('#payment_amount' + index).autoNumeric('get')) > parseFloat($("#owing"+ index).autoNumeric('get')) ){
                $('#status_pay' + index).prop('checked', false);
                $("#status_pay_hidden").removeAttr('disabled');
                checkAmountOwing(index)
            }else{ 
                $('#status_pay' + index).prop('checked', true);
                $("#status_pay_hidden" + index).attr('disabled','disabled');
            }
        }
        count = parseFloat($('#payment_amount' + index).autoNumeric('get'));
        disc = parseFloat($("#jml_diskon_mdl" + index).autoNumeric('get'));
        owe = $('#owing_asli' + index).val();
        $('#owing' + index).autoNumeric('set',owe - (count + disc));
        sumOwing();
        sumPaymentAmount();
    }else {
        swal({
            icon: "error",
            text: "Maaf, nominal yang anda masukkan salah"
        });    
        $('#payment_amount' + index).autoNumeric('set',0);
    }
}

function isDiscount(val, index) {    
    if ($.isNumeric(changeMaskingToNumber(val))) {
        disc = parseFloat($("#jml_diskon_mdl" + index).autoNumeric('get'));
        pay = parseFloat($("#payment_amount" + index).autoNumeric('get'));
        owe = $('#owing_asli' + index).val();
        $('#owing' + index).autoNumeric('set',owe - (disc + pay));
        sumOwing();
    }
}

function sumPaymentAmount() {
    let c = 0;
    let e = 0;
    $('.payment_amount').each(function () {
        c += parseFloat(changeMaskingToNumber(this.value));
    });
    $('.diskon').each(function () {
        e += parseFloat(changeMaskingToNumber(this.value) * 1);
    });
    $('#cheque_amount').autoNumeric('init',{aPad: false}).autoNumeric('set',c + e);
    $("#payment-label").text(Number(c).toLocaleString());
}

function sumOwing() {
    let d = 0;
    $('.owing').each(function () {
        d += parseFloat(changeMaskingToNumber(this.value) * 1);
    });
    $("#tot_owing").val(d);
    $("#owing-label").text(Number(d).toLocaleString());
}

function sumDisc() {
    let e = 0;
    $('.diskon').each(function () {
        e += parseFloat(changeMaskingToNumber(this.value) * 1);
    });
    $("#tot_diskon").val(e);
    $("#discount-label").text(Number(e).toLocaleString());
}

function checkAmountOwing(index) {
    swal({
        icon: "error",
        text: "Maaf, jumlah yang di bayar lebih besar dari pada terutang"
    });
    // $("#payment_amount"+ index).val(0);
    $('#payment_amount' + index).autoNumeric('set',0);
}

function checkTermin(index) {
    $("#jml_diskon_mdl" + index).autoNumeric('init',{aPad: false});
    var termin      = $("#membayar_antara" + index).val();
    var faktur_id   = $("#faktur_id" + index).val();
    // if ($('#status_pay' + index).is(':checked')) {
        $.ajax({
            type: "GET",
            url: ""+base_url+"/ajax/check-termin",
            data: {id : termin, faktur_id : faktur_id,
                _token: token_},
            dataType: "json",
            success: function(response){
                console.log('-----------');
                console.log(response.code);
                console.log('-----------');
                if(response.code == 200){
                    // $("#diskon" + index).val(response.diskon);
                    swal({
                        title: "Peringatan!",
                        text: "Faktur ini mempunyai syarat pembayaran, ingin melihat info diskon?",
                        icon: "warning",
                        buttons: {
                            cancel: true,
                            confirm: true,
                        },
                    }).then((result) => {
                        if (result) {
                            var diskon_value = $("#jml_diskon_mdl" + index);
                            $("#modal_termin"+ index).modal("show");
                            $("#no_faktur_mdl"+ index).text($("#no_faktur" + index).val());
                            $("#nilai_faktur_mdl"+ index).text($("#amount" + index).val());
                            $("#nilai_terutang_mdl"+ index).text($("#owing" + index).val());
                            $("#syarat_pembayaran_mdl"+ index).text(response.termin);
                            $("#diskon_anjuran_mdl"+ index).text(Number(response.diskon.toFixed(1)).toLocaleString());
                            $("#ok_mdl"+ index).on('click', function () {
                                var termin_ori = $("#owing_asli"+ index).val();
                                if ( parseFloat(changeMaskingToNumber(diskon_value.val())) >= termin_ori || parseFloat(changeMaskingToNumber(diskon_value.val())) < 0) {
                                    swal({
                                        icon: "error",
                                        text: "Maaf, Jumlah Diskon Tidak Boleh Lebih dari Jumlah Terhutang",
                                    });
                                }else if( $('#akun_diskon_mdl'+ index).val() === '' && parseFloat(changeMaskingToNumber(diskon_value.val())) > 0){
                                    swal({
                                        icon: "error",
                                        text: "Maaf, Akun Diskon Tidak Boleh Kosong",
                                    });
                                }else if( $('#akun_diskon_mdl'+ index).val() !== '' && parseFloat(changeMaskingToNumber(diskon_value.val())) == 0){
                                    swal({
                                        icon: "error",
                                        text: "Maaf, Jumlah Diskon Tidak Boleh Kosong/Nol",
                                    });
                                }else{
                                    $("#diskon" + index).val($("#jml_diskon_mdl" + index).val());
                                    isDiscount(diskon_value.val(), index);
                                    sumDisc();
                                    sumPaymentAmount();
                                    setTimeout(function() {
                                        $('.modal_termin').modal('hide');;
                                    }, 0);
                                }
                            });
                        }
                    });
                }else {
                    var diskon_value = $("#jml_diskon_mdl" + index);
                    $("#modal_termin"+ index).modal({ backdrop : 'static', keyboard : false, show : true});
                    $("#no_faktur_mdl"+ index).text($("#no_faktur" + index).val());
                    $("#nilai_faktur_mdl"+ index).text($("#amount" + index).val());
                    $("#nilai_terutang_mdl"+ index).text($("#owing" + index).val());
                    $("#syarat_pembayaran_mdl"+ index).text(response.termin);
                    $("#diskon_anjuran_mdl"+ index).text(Number(response.diskon.toFixed(1)).toLocaleString());
                    $("#ok_mdl"+ index).on('click', function () {
                        var termin_ori = $("#owing_asli"+ index).val();
                        if ( parseFloat(changeMaskingToNumber(diskon_value.val())) >= termin_ori || parseFloat(changeMaskingToNumber(diskon_value.val())) < 0) {
                            swal({
                                icon: "error",
                                text: "Maaf, Jumlah Diskon Tidak Boleh Lebih dari Jumlah Terhutang",
                            });
                        }else if( $('#akun_diskon_mdl'+ index).val() === '' && parseFloat(changeMaskingToNumber(diskon_value.val())) > 0){
                            swal({
                                icon: "error",
                                text: "Maaf, Akun Diskon Tidak Boleh Kosong",
                            });
                        }else if( $('#akun_diskon_mdl'+ index).val() !== '' && parseFloat(changeMaskingToNumber(diskon_value.val())) == 0){
                            swal({
                                icon: "error",
                                text: "Maaf, Jumlah Diskon Tidak Boleh Kosong/Nol",
                            });
                        }else{
                            $("#diskon" + index).val($("#jml_diskon_mdl" + index).val());
                            isDiscount(diskon_value.val(), index);
                            sumDisc();
                            sumPaymentAmount();
                            setTimeout(function() {
                                $('.modal_termin').modal('hide');;
                            }, 0);
                        }
                    });
                }          
            }, failure: function(errMsg) {
                alert(errMsg);
            },
        });
    // }
}

function checkCheque() {
    var x = $('#cheque_no');
    if (x.val().length > 0) {
        $('#cheque_date').prop('disabled', false);
    } else {
        $('#cheque_date').prop('disabled', true);
    }
}

$("#btn-submit").on("click", function (e) {
    e.preventDefault();
    checkPaymentAmount($(this));
});
function checkPaymentAmount(el) {
        var form = $(el).parents('form#form-penerimaan-penjualan');
        var sum = 0;
        $(".payment_amount").map(function(index, elem) {
            sum += $(elem).val();
        });
        if (sum == 0) {
            swal({
                icon: "error",
                text: "Faktur Belum Dimasukkan"
            });
        }else {
            form.submit();
        }
}

function isPayChecked(q) {
    if ($("#status_pay" + q).is(':checked')) {
        $("#status_pay_hidden" + q).attr('disabled','disabled');
    }else {
        $("#status_pay_hidden" + q).removeAttr('disabled');
    }
}