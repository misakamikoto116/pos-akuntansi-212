var base_url = window.location.origin + '/akuntansi';

var token_ = $("meta[name='csrf-token']").attr("content");
var createData = new Boolean(true);
var updateRowId = null;

$('.tanggal_permintaan').datepicker({ format: 'dd MM yyyy', autoclose: true });
$('.tanggal_permintaan').datepicker('setDate', new Date());

$(document).ready(function () {
    let exist_product = $('.produk_id').length;
    for (let a = 0; a < exist_product; a++) {
        callSelect2AjaxProdukID(a, $('#produk_id_temp' + a).val());
        callSelect2AjaxProduk(a, 'modal');
    }
});

function duplicateForm() {
    var row = $('#table_penawaran_section').html();
    var clone = $(row).clone();
    clone.find('.no_produk').attr('name', 'no_produk[]').attr('id', 'no_produk' + q).attr('onclick', 'awas(' + q + ')').attr('class', 'form-control no_produk').attr('readonly', true);
    clone.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('class', 'form-control produk_id');
    clone.find('.keterangan_produk').attr('name', 'keterangan_produk[]').attr('id', 'keterangan_produk' + q);
    clone.find('.qty_produk').attr('name', 'qty_produk[]').attr('id', 'qty_produk' + q).attr('onchange', 'cekQtyandPrice(' + q + ')');
    clone.find('.satuan_produk').attr('name', 'satuan_produk[]').attr('id', 'satuan_produk' + q);
    clone.find('.required_date').attr('name', 'required_date[]').attr('id', 'required_date' + q).attr('class', 'tanggal1 form-control format-tanggal');
    clone.find('.notes').attr('name', 'notes[]').attr('id', 'notes' + q);
    clone.find('.qty_ordered').attr('name', 'qty_ordered[]').attr('id', 'qty_ordered' + q).attr('readonly');
    clone.find('.qty_received').attr('name', 'qty_received[]').attr('id', 'qty_received' + q).attr('readonly');
    clone.find('.dept_id').attr('name', 'dept_q[]').attr('id', 'dept_id' + q);
    clone.find('.proyek_id').attr('name', 'proyek_q[]').attr('id', 'proyek_id' + q);
    $(clone).appendTo($('.duplicate-penawaran-sections').closest('.penawaran-sections').find('.purchasePenawaran'));

    $('.format-tanggal').datepicker({ format: 'dd M yyyy', autoclose: true });
    $('.select2').select2();
    // callSelect2AjaxProduk(q);
    removeDelButton();
    q++;
}

function duplicateFormWhenOld() {
    var row = $('#table_penawaran_section').html();
    var clone = $(row).clone();
    clone.find('.no_produk').attr('name', 'no_produk[]').attr('id', 'no_produk' + q).attr('onclick', 'awas(' + q + ')').attr('class', 'form-control no_produk').attr('readonly', true).val('');
    clone.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('class', 'form-control produk_id').val('');
    clone.find('.keterangan_produk').attr('name', 'keterangan_produk[]').attr('id', 'keterangan_produk' + q).val('');
    clone.find('.qty_produk').attr('name', 'qty_produk[]').attr('id', 'qty_produk' + q).attr('onchange', 'cekQtyandPrice(' + q + ')').val('');
    clone.find('.satuan_produk').attr('name', 'satuan_produk[]').attr('id', 'satuan_produk' + q).val('');
    clone.find('.required_date').attr('name', 'required_date[]').attr('id', 'required_date' + q).attr('class', 'tanggal1 form-control format-tanggal').val('');
    clone.find('.notes').attr('name', 'notes[]').attr('id', 'notes' + q).val('');
    clone.find('.qty_ordered').attr('name', 'qty_ordered[]').attr('id', 'qty_ordered' + q).val('').attr('readonly');
    clone.find('.qty_received').attr('name', 'qty_received[]').attr('id', 'qty_received' + q).val('').attr('readonly');
    clone.find('.dept_id').attr('name', 'dept_q[]').attr('id', 'dept_id' + q).val('');
    clone.find('.proyek_id').attr('name', 'proyek_q[]').attr('id', 'proyek_id' + q).val('');
    $(clone).appendTo($('.duplicate-penawaran-sections').closest('.penawaran-sections').find('.purchasePenawaran'));

    $('.format-tanggal').datepicker({ format: 'dd M yyyy', autoclose: true });
    $('.select2').select2();
    // callSelect2AjaxProduk(q);
    removeDelButton();
    q++;
}

function callSelect2AjaxProduk(q = null, type = null) {
    if(type === null){
        $('.produk_id').select2({
            minimumInputLength: 3,
            formatInputTooShort: function () {
                return "Ketik 3 Karakter";
            },
            ajax: {
                url: "" + base_url + "/ajax/get-nama-produk",
                type: 'GET',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                dataType: 'JSON',
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });
    }else if(type == 'modal'){
        $('.modal_kode_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            keyPressModalKodeBarang(q, x, kategori);
        });

        $('.modal_nama_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            keyPressModalNamaBarang(q, x, kategori);
        });
    }
}

function callSelect2AjaxProdukID(q, id) {
    $.ajax({
        url: "" + base_url + "/ajax/get-id-produk",
        type: 'GET',
        data: 'search=' + id,
        dataType: 'JSON',
        success: function (data) {
            $.each(data.item, function (key, value) {
                $('#produk_id'+q).val(value.id);
                $('#no_produk'+q).val(value.no_barang);
                // $('#produk_id' + q).append('<option selected value="' + value.id + '">' + value.text + '</option>');
            });
        }
    });
}

// function test(vale) {
//     var produk_id = $('#produk_id' + vale).val();
//     $.ajax({
//         type: "GET",
//         url: "" + base_url + "/ajax/get-produk",
//         data: {
//             id: produk_id,
//             _token: token_
//         },
//         dataType: "json",
//         success: function (response) {
//             $('#keterangan_produk' + vale).val(response.keterangan)
//             $('#qty_produk' + vale).val('1')
//             $('#satuan_produk' + vale).val(response.satuan)
//             $('#unit_harga_produk' + vale).val(response.unitPrice)
//             $('#diskon_produk' + vale).val(response.diskon)
//             $('#qty_ordered' + vale).val(0)
//             $('#qty_received' + vale).val(0)
//             $(response.multiGudang).each(function (val, text) {
//                 $('#gudang_id' + vale).append(`<option class="form-control select2" value="${text.id}">${text.text}</option>`);
//             });
//             // $('#gudang_id'+vale).select2('data', response.multiGudang)
//         }, failure: function (errMsg) {
//             alert(errMsg);
//         }
//     });
// }


$('.duplicate-penawaran-sections').on('click', '.remove-rincian-penawaran', function () {
    if ($(this).closest('.purchasePenawaran').find('tr').length > 0) {
        $(this).closest('tr').remove();
        $('.purchasePenawaran tr:last').find('td:last').append('<button href="" class="remove btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button>');
        q -= 1;
    }
});

// Fungsi button cetak di klik
$("#btn-cetak").on("click", function (e) {
    e.preventDefault();
    btnCetak()
});

function btnCetak() {
    if ($('.produk_id').val() == "" || $('.produk_id').val() == undefined) {
        swal({
            icon: "warning",
            text: "Barang Masih Kosong"
        });
    } else {
        var form = $("#form-penjualan-penawaran");
        form.attr('target', '_blank');
        form.attr('action', '' + base_url + '/laporan/cetak-permintaan-pembelian');
        form.submit();
    }
}

function cekQtyandPrice(param) {
    if ($("#qty_produk" + param).val() == "" || $("#qty_produk" + param).val() <= 0) {
        $("#qty_produk" + param).val(1);
        swal({
            icon: "error",
            text: "Kuantitas harus lebih besar dari 0"
        });
    }
    if ($("#unit_harga_produk" + param).val() == "") {
        $("#unit_harga_produk" + param).val(0);
    }
}

function removeDelButton() {
    $('.remove-rincian-penawaran').not(':last').remove();
}

function keyPressModalKodeBarang(q, x, kategori = null){
    var no_barang = x;
    var data = {
        kategoriBySearch : kategori,
        no_barang : no_barang,
        _token: token_,
    }
    $('.modal_nama_barang').val('');
    callAjaxKeypress(q, no_barang, data)
}

function keyPressModalNamaBarang(q, x, kategori){
    var keterangan = x;
    var data = {
        kategoriBySearch : kategori,
        keterangan : keterangan,
        _token: token_,
    }
    $('.modal_kode_barang').val('');
    callAjaxKeypress(q, keterangan, data)
}


function awas(q = null){
    var type = 'modal';
    $('#modalBarang').modal({
        backdrop: 'static',
        keyboard: false, 
        show: true
    });
    var row = $('#modal_thead_barang').html();
    var clone = $(row).clone();
    clone.find('.modal_kode_barang').attr('name', 'modal_kode_barang[]').attr('id', 'modal_kode_barang' + q).attr('class', 'form-control modal_kode_barang');
    clone.find('.modal_nama_barang').attr('name', 'modal_nama_barang[]').attr('id', 'modal_nama_barang' + q).attr('class', 'form-control modal_nama_barang');;
    $(clone).appendTo($('.duplicate-modal-sections').closest('.modal-sections').find('.modal_thead'));

    callSelect2AjaxProduk(q, type);
    callAjaxListGudang();

    $('body').keydown(function(e) {
        var keyCode = e.keyCode || e.which;
        if ( keyCode == 27 ) {
            barangBatal();
            $('#modalBarang').modal('toggle');
        }
    });
}

function barangBatal() {
    $('.modal_thead_barang_tr').remove();
    $('.table_tr_modal').remove();
}

function clearQtyGudang(){
    $('#modal_gudang_temp').val(' ');
    $('label[id*="qty_gudang_footer"]').text('');
    $('#modal_gudang').val(0).trigger('change');
}

$('#kategori_barang').change(function() {
    var searchBy = $('#searchByKategori');
    clearQtyGudang();
    if ($('#kategori_barang').val() == 1) {
        searchBy.prop('disabled', false);
        ajaxModalKategori(searchBy);

        setTimeout(function() {
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        }, 600);

        $('#searchByKategori').change(function() {
            clearQtyGudang();
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        });
    }else if($('#kategori_barang').val() == 0){
        searchBy.html(null);
        searchBy.prop('disabled', true);
        
        var kode_barang = $('.modal_kode_barang').val();
        var nama_barang = $('.modal_nama_barang').val();            
        if (kode_barang !== '' && nama_barang === '') {
            keyPressModalKodeBarang(null, kode_barang, null);
        }else if (kode_barang === '' && nama_barang !== '') {
            keyPressModalNamaBarang(null, nama_barang, null);
        }
    }
});

function ajaxModalKategori(searchBy){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-all-kategori-produk",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                searchBy.append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxKeypress(n = null, searchKey, dataKey){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-nama-produk",
        data: dataKey,
        dataType: 'JSON',
        success: function (data) {
            $('.table_tr_modal').remove();
            if (data == 'null') {
                $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="table_tr_modal">'
                        + '<td colspan="3" style="padding:5px; 20px" align="center">'
                        +   '<span class="alert alert-warning"> Tidak ada Barang dengan kata Kunci '+ searchKey +' </span>'
                        + '</td>' +
                    '</tr>').appendTo('.modal_tbody');
            }else{
                $.each(data, function (key, value) {
                    var idP = value.id;

                    $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="modal_barang_'+ key +' table_tr_modal">'
                            + '<td width="26%" style="padding:5px 15px">'
                                + value.no_barang
                            + '</td>'
                            + '<td width="3%"></td>'
                            + '<td width="55%" style="padding:5px 15px">'
                                + value.keterangan
                            + '</td>'+
                        '</tr>').appendTo('.modal_tbody');

                    // hey
                    $('.modal_barang_'+ key).hover(
                        function(){
                            $(this).attr('style', 'border: 2px solid limegreen; border-radius: 5px ;display:table; width:100%; table-layout:fixed;');
                        }, function() {
                            $(this).attr('style', 'border: none; display:table; width:100%; table-layout:fixed;');
                        }
                    );

                    $('.table_tr_modal').click(function () {
                        var ini = $(this);
                        if(ini.is(':hover') === true){
                            $('.table_tr_modal').removeClass('warnatr');
                            ini.addClass('warnatr');
                        }
                    });
                    // yeh

                    $('.modal_barang_'+ key).click(function() {
                        var ini = $(this);
                        var gudang = $('#modal_gudang').val();
                        $('#modal_gudang_temp').val(idP);
                        
                        callAjaxQtyGudang(idP, gudang);
                    });

                    $('.modal_barang_'+ key).dblclick(function(){
                        setTimeout(function() {
                            clearQtyGudang();
                        }, 0);
                        $('#produk_id'+n).val(idP);
                        $('#no_produk'+n).val(value.no_barang);
                        test(idP, n);
                        barangBatal();
                        $('#modalBarang').modal('toggle');
                    });
                    
                    $('#modal_gudang').change(function() {
                        var gudang = $('#modal_gudang').val();
                        var produk = $('#modal_gudang_temp').val();

                        callAjaxQtyGudang(produk, gudang);
                    });
                });
            }
        }
    });
}

function callAjaxQtyGudang(produk, gudang){
    $.ajax({
        type : 'GET',
        url  : "" + base_url + "/ajax/get-qty-gudang-stock",
        data : {
            gudang : gudang,
            produk : produk,
            _token : token_
        },
        dataType : 'json',
        success: function (resuponse){
            $('#qty_gudang_footer').html(resuponse);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxListGudang(){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-list-gudang",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                $('#modal_gudang').append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function test(idP, vale) {
    var produk_id   = idP;
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-produk",
        data: {
            id : produk_id,
            _token: token_
        },
        dataType: "json",
        success: function(response){
            $('#keterangan_produk'+vale).val(response.keterangan);
            $('#qty_produk'+vale).val(response.jumlah);
            $('#satuan_produk'+vale).val(response.satuan);
            $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
            $('#diskon_produk'+vale).val(response.diskon);
            $('#qty_ordered' + vale).val(0)
            $('#qty_received' + vale).val(0)
            if (response.harga_modal !== null) {
                $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
            }else {
                $('#harga_modal'+vale).val('0');
            }     
            $(response.multiGudang).each(function (val, text) {
                $('#gudang_id' + vale).append(`<option class="form-control select2" value="${text.id}">${text.text}</option>`);
            });
            $('#gudang_id'+vale).select2('data', response.multiGudang)
            sumQtyAndUnit(vale);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}