var current_url     = $(location).attr('href');
var split_segment   = current_url.split('/');
var last_segment    = split_segment.pop();

var q = $('.produk_id').length;
var base_url = window.location.origin + '/akuntansi';
var token_ = $("meta[name='csrf-token']").attr("content");

$(function () {
    let exist_product = $('.produk_id').length;
    for (let a = 0; a < exist_product; a++) {
        callSelect2AjaxProdukID(a, $('#produk_id' + a).val());
        callSelect2AjaxProduk(a, 'modal');
    }
});

function duplicatePenyesuaianPersediaan(){
    var row = $('#table_penyesuaian_barang_section').html();
    var clone = $(row).clone();
    clone.find('.no_produk').attr('name','no_produk[]').attr('id','no_produk'+q).attr('onclick','awas('+q+')').attr('class','form-control no_produk').attr('readonly', true).val('');
    clone.find('.produk_id').attr('name','produk_id[]').attr('id','produk_id'+q).attr('class','form-control produk_id');
    clone.find('.keterangan_produk').attr('name','keterangan_produk[]').attr('id','keterangan_produk'+q).attr('readonly');
    clone.find('.satuan_produk').attr('name','satuan_produk[]').attr('id','satuan_produk'+q);
    clone.find('.qty_produk').attr('name','qty_produk[]').attr('id','qty_produk'+q).attr('readonly');
    clone.find('.qty_baru').attr('name', 'qty_baru[]').attr('id', 'qty_baru' + q);
    clone.find('.nilai_skrg').attr('name', 'nilai_skrg[]').attr('id', 'nilai_skrg' + q).attr('readonly');
    clone.find('.nilai_baru').attr('name', 'nilai_baru[]').attr('id', 'nilai_baru' + q).autoNumeric('init',{aPad: false});
    clone.find('.departemen').attr('name', 'departemen[]').attr('id', 'departemen' + q);
    clone.find('.gudang_id').attr('name', 'gudang_id[]').attr('id','gudang_id'+q);
    clone.find('.proyek').attr('name','proyek[]').attr('id','proyek'+q);
    clone.find('.serial_number').attr('name','serial_number[]').attr('id','serial_number'+q);
    clone.find('.produk_detail_id').attr('name','produk_detail_id[]').attr('id','produk_detail_id'+q);
    $(clone).appendTo($('.duplicate-penyesuaian-persediaan-sections').closest('.penyesuaian-persediaan-sections').find('.penyesuaianpersediaan'));
    q++;
    $('.select2').select2();
    // saveToSession();
    checkValueAdjustment();
}

function duplicatePenyesuaianPersediaanEdit(){
    var row = $('#table_penyesuaian_barang_section_edit').html();
    var clone = $(row).clone();
    clone.find('.no_produk').attr('name','no_produk[]').attr('id','no_produk'+q).attr('onclick','awas('+q+')').attr('class','form-control no_produk').val('');
    clone.find('.produk_id').attr('name','produk_id[]').attr('id','produk_id'+q).attr('class','form-control produk_id').val('');
    clone.find('.keterangan_produk').attr('name','keterangan_produk[]').attr('id','keterangan_produk'+q).attr('readonly');
    clone.find('.satuan_produk').attr('name','satuan_produk[]').attr('id','satuan_produk'+q);
    clone.find('.qty_diff').attr('name','qty_diff[]').attr('id','qty_diff'+q);
    clone.find('.nilai_skrg').attr('name', 'nilai_skrg[]').attr('id', 'nilai_skrg' + q).attr('readonly');
    clone.find('.nilai_baru').attr('name', 'nilai_baru[]').attr('id', 'nilai_baru' + q).autoNumeric('init',{aPad: false});
    clone.find('.departemen').attr('name', 'departemen[]').attr('id', 'departemen' + q);
    clone.find('.gudang_id').attr('name', 'gudang_id[]').attr('id','gudang_id'+q);
    clone.find('.proyek').attr('name','proyek[]').attr('id','proyek'+q);
    clone.find('.serial_number').attr('name','serial_number[]').attr('id','serial_number'+q);
    clone.find('.produk_detail_id').attr('name','produk_detail_id[]').attr('id','produk_detail_id'+q);
    $(clone).appendTo($('.duplicate-penyesuaian-persediaan-sections').closest('.penyesuaian-persediaan-sections').find('.penyesuaianpersediaan'));
    q++;
    $('.select2').select2();
    // saveToSession();
    checkValueAdjustment();
}

// function test(vale) {
//     var produk_id   = $('#produk_id'+vale).val();
//             $.ajax({
//             type: "GET",
//             url: base_url + "/ajax/get-produk",
//             data: {id : produk_id,
//                 _token: token_},
//             dataType: "json",
//             success: function(response){
            
//             var harga_modal_total = response.harga_modal.harga_modal * response.sum_current_qty;

//             $('#nilai_skrg' + vale).autoNumeric('init',{aPad:false});

//             $('#keterangan_produk'+vale).val(response.keterangan);
//             $('#qty_produk'+vale).val(response.sum_current_qty);
//             $('#satuan_produk'+vale).val(response.satuan);
//             if (response.harga_modal !== null) {
//                 $('#produk_detail_id'+vale).val(response.harga_modal.id);
//                 $('#nilai_skrg' + vale).autoNumeric('set', harga_modal_total);
//             }
                
//             if(response.multiGudang != null){
//                 document.getElementById('gudang_id'+vale).options.length = 0;
//             }
//             $(response.multiGudang).each(function (val, text) {
//                 $(text.gudang).each(function (index, item) {
//                     $('#gudang_id' + vale).append(`<option class="form-control select2" value="${item.id}">${item.nama}</option>`);
//                 });
//             });
        
//             // sumQtyAndUnit(vale);            
//             }, failure: function(errMsg) {
//                 alert(errMsg);
//             }
//         });
// }
$(document).ready(function () {
    checkValueAdjustment();
    $('.duplicate-penyesuaian-persediaan-sections').on('click', '.add-penyesuaian-persediaan', function () {
        if (last_segment == 'edit') {
            duplicatePenyesuaianPersediaanEdit();
        }else {
            duplicatePenyesuaianPersediaan();
        }
    });

    $('#check_isi').on('click',function () {
        checkValueAdjustment();
    });
});
$('.duplicate-penyesuaian-persediaan-sections').on('click', '.remove-rincian-penyesuaian-persediaan', function () {
    if ($(this).closest('.penyesuaianpersediaan').find('tr').length > 0) {
        $(this).closest('tr').remove();
    }
});
$( "#akun_penyesuaian" ).change(function() {
    var akun_penyesuaian   = $('#akun_penyesuaian').val();
    $.ajax({
        type: "GET",
        url: base_url + "/ajax/get-akun-kode",
        data: {id : akun_penyesuaian,
            _token: token_},
        dataType: "json",
        success: function(response){
            $('#kode_akun_id').val(response.kode_akun);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
});

function checkValueAdjustment() {

    if ($('#check_isi').is(':checked')){
        $('.kol_nilai_skrg').show();
        $('.kol_nilai_baru').show();
    }else{
        $('.kol_nilai_skrg').hide();
        $('.kol_nilai_baru').hide();
    }
}

function callSelect2AjaxProduk(q = null, type = null) {
    if(type === null){
        $('.produk_id').select2({
            minimumInputLength: 3,
            formatInputTooShort: function () {
                return "Ketik 3 Karakter";
            },
            ajax: {
                url: "" + base_url + "/ajax/get-nama-produk",
                type: 'GET',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                dataType: 'JSON',
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });    
    }else if(type == 'modal'){
        $('.modal_kode_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            keyPressModalKodeBarang(q, x, kategori);
        });

        $('.modal_nama_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            keyPressModalNamaBarang(q, x, kategori);
        });
    }    
}

function callSelect2AjaxProdukID(q, id) {
    $.ajax({
        url: "" + base_url + "/ajax/get-id-produk",
        type: 'GET',
        data: 'search=' + id,
        dataType: 'JSON',
        success: function (data) {
            $.each(data.item, function (key, value) {
                $('#produk_id'+q).val(value.id);
                $('#no_produk'+q).val(value.no_barang);
            });
        }
    });
}

function keyPressModalKodeBarang(q, x, kategori = null){
    var no_barang = x;
    var data = {
        kategoriBySearch : kategori,
        no_barang : no_barang,
        _token: token_,
    }
    $('.modal_nama_barang').val('');
    callAjaxKeypress(q, no_barang, data)
}

function keyPressModalNamaBarang(q, x, kategori){
    var keterangan = x;
    var data = {
        kategoriBySearch : kategori,
        keterangan : keterangan,
        _token: token_,
    }
    $('.modal_kode_barang').val('');
    callAjaxKeypress(q, keterangan, data)
}


function awas(q = null){
    var type = 'modal';
    $('#modalBarang').modal({
        backdrop: 'static',
        keyboard: false, 
        show: true
    });
    var row = $('#modal_thead_barang').html();
    var clone = $(row).clone();
    clone.find('.modal_kode_barang').attr('name', 'modal_kode_barang[]').attr('id', 'modal_kode_barang' + q).attr('class', 'form-control modal_kode_barang');
    clone.find('.modal_nama_barang').attr('name', 'modal_nama_barang[]').attr('id', 'modal_nama_barang' + q).attr('class', 'form-control modal_nama_barang');;
    $(clone).appendTo($('.duplicate-modal-sections').closest('.modal-sections').find('.modal_thead'));

    callSelect2AjaxProduk(q, type);
    callAjaxListGudang();
}

function barangBatal() {
    $('.modal_thead_barang_tr').remove();
    $('.table_tr_modal').remove();
}

function clearQtyGudang(){
    $('#modal_gudang_temp').val(' ');
    $('label[id*="qty_gudang_footer"]').text('');
    $('#modal_gudang').val(0).trigger('change');
}

$('#kategori_barang').change(function() {
    var searchBy = $('#searchByKategori');
    clearQtyGudang();
    if ($('#kategori_barang').val() == 1) {
        searchBy.prop('disabled', false);
        ajaxModalKategori(searchBy);

        setTimeout(function() {
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        }, 600);

        $('#searchByKategori').change(function() {
            clearQtyGudang();
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        });
    }else if($('#kategori_barang').val() == 0){
        searchBy.html(null);
        searchBy.prop('disabled', true);
        
        var kode_barang = $('.modal_kode_barang').val();
        var nama_barang = $('.modal_nama_barang').val();            
        if (kode_barang !== '' && nama_barang === '') {
            keyPressModalKodeBarang(null, kode_barang, null);
        }else if (kode_barang === '' && nama_barang !== '') {
            keyPressModalNamaBarang(null, nama_barang, null);
        }
    }
});

function ajaxModalKategori(searchBy){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-all-kategori-produk",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                searchBy.append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxKeypress(n = null, searchKey, dataKey){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-nama-produk",
        data: dataKey,
        dataType: 'JSON',
        success: function (data) {
            $('.table_tr_modal').remove();
            if (data == 'null') {
                $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="table_tr_modal">'
                        + '<td colspan="3" style="padding:5px; 20px" align="center">'
                        +   '<span class="alert alert-warning"> Tidak ada Barang dengan kata Kunci '+ searchKey +' </span>'
                        + '</td>' +
                    '</tr>').appendTo('.modal_tbody');
            }else{
                $.each(data, function (key, value) {
                    var idP = value.id;

                    $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="modal_barang_'+ key +' table_tr_modal">'
                            + '<td width="26%" style="padding:5px 15px">'
                                + value.no_barang
                            + '</td>'
                            + '<td width="3%"></td>'
                            + '<td width="55%" style="padding:5px 15px">'
                                + value.keterangan
                            + '</td>'+
                        '</tr>').appendTo('.modal_tbody');

                    // hey
                    $('.modal_barang_'+ key).hover(
                        function(){
                            $(this).attr('style', 'border: 2px solid limegreen; border-radius: 5px ;display:table; width:100%; table-layout:fixed;');
                        }, function() {
                            $(this).attr('style', 'border: none; display:table; width:100%; table-layout:fixed;');
                        }
                    );

                    $('.table_tr_modal').click(function () {
                        var ini = $(this);
                        if(ini.is(':hover') === true){
                            $('.table_tr_modal').removeClass('warnatr');
                            ini.addClass('warnatr');
                        }
                    });
                    // yeh

                    $('.modal_barang_'+ key).click(function() {
                        var ini = $(this);
                        var gudang = $('#modal_gudang').val();
                        $('#modal_gudang_temp').val(idP);
                        
                        callAjaxQtyGudang(idP, gudang);
                    });

                    $('.modal_barang_'+ key).dblclick(function(){
                        setTimeout(function() {
                            clearQtyGudang();
                        }, 0);
                        $('#produk_id'+n).val(idP);
                        $('#no_produk'+n).val(value.no_barang);
                        test(idP, n);
                        barangBatal();
                        $('#modalBarang').modal('toggle');
                    });
                    
                    $('#modal_gudang').change(function() {
                        var gudang = $('#modal_gudang').val();
                        var produk = $('#modal_gudang_temp').val();

                        callAjaxQtyGudang(produk, gudang);
                    });
                });
            }
        }
    });
}

function callAjaxQtyGudang(produk, gudang){
    $.ajax({
        type : 'GET',
        url  : "" + base_url + "/ajax/get-qty-gudang-stock",
        data : {
            gudang : gudang,
            produk : produk,
            _token : token_
        },
        dataType : 'json',
        success: function (resuponse){
            $('#qty_gudang_footer').html(resuponse);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxListGudang(){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-list-gudang",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                $('#modal_gudang').append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function test(idP, vale) {
    var produk_id   = idP;
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-produk",
        data: {
            id : produk_id,
            _token: token_
        },
        dataType: "json",
        success: function(response){
            
            var harga_modal_total = response.harga_modal.harga_modal * response.sum_current_qty;

            $('#nilai_skrg' + vale).autoNumeric('init',{aPad:false});

            $('#keterangan_produk'+vale).val(response.keterangan);
            $('#qty_produk'+vale).val(response.sum_current_qty);
            $('#satuan_produk'+vale).val(response.satuan);
            if (response.harga_modal !== null) {
                $('#produk_detail_id'+vale).val(response.harga_modal.id);
                $('#nilai_skrg' + vale).autoNumeric('set', harga_modal_total);
            }
                
            if(response.multiGudang != null){
                document.getElementById('gudang_id'+vale).options.length = 0;
            }
            $(response.multiGudang).each(function (val, text) {
                $(text.gudang).each(function (index, item) {
                    $('#gudang_id' + vale).append(`<option class="form-control select2" value="${item.id}">${item.nama}</option>`);
                });
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}