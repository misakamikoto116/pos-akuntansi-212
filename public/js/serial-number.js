var base_url = window.location.origin + '/akuntansi';

var token_ = $("meta[name='csrf-token']").attr("content");
var createData = new Boolean(true);
var updateRowId = null;

$(function () {
    let exist_product = $('.produk_id').length;
    for (let a = 0; a < exist_product; a++) {
        callSelect2AjaxProdukID(a, $('#produk_id' + a).val());
        callSelect2AjaxProduk(a, 'modal');
    }
});

function callSelect2AjaxProduk(q = null, type = null) {
    if(type === null){
        $('.produk_id').select2({
            minimumInputLength: 3,
            formatInputTooShort: function () {
                return "Ketik 3 Karakter";
            },
            ajax: {
                url: "" + base_url + "/ajax/get-nama-produk",
                type: 'GET',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                dataType: 'JSON',
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });    
    }else if(type == 'modal'){
        $('.modal_kode_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            keyPressModalKodeBarang(q, x, kategori);
        });

        $('.modal_nama_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            keyPressModalNamaBarang(q, x, kategori);
        });
    }    
}

function callSelect2AjaxProdukID(q, id) {
    $.ajax({
        url: "" + base_url + "/ajax/get-id-produk",
        type: 'GET',
        data: 'search=' + id,
        dataType: 'JSON',
        success: function (data) {
            $.each(data.item, function (key, value) {
                $('#produk_id'+q).val(value.id);
                $('#no_produk'+q).val(value.no_barang);
            });
        }
    });
}

function keyPressModalKodeBarang(q, x, kategori = null){
    var no_barang = x;
    var data = {
        kategoriBySearch : kategori,
        no_barang : no_barang,
        _token: token_,
    }
    $('.modal_nama_barang').val('');
    callAjaxKeypress(q, no_barang, data)
}

function keyPressModalNamaBarang(q, x, kategori){
    var keterangan = x;
    var data = {
        kategoriBySearch : kategori,
        keterangan : keterangan,
        _token: token_,
    }
    $('.modal_kode_barang').val('');
    callAjaxKeypress(q, keterangan, data)
}


function awas(q = null){
    var type = 'modal';
    $('#modalBarang').modal({
        backdrop: 'static',
        keyboard: false, 
        show: true
    });
    var row = $('#modal_thead_barang').html();
    var clone = $(row).clone();
    clone.find('.modal_kode_barang').attr('name', 'modal_kode_barang[]').attr('id', 'modal_kode_barang' + q).attr('class', 'form-control modal_kode_barang');
    clone.find('.modal_nama_barang').attr('name', 'modal_nama_barang[]').attr('id', 'modal_nama_barang' + q).attr('class', 'form-control modal_nama_barang');;
    $(clone).appendTo($('.duplicate-modal-sections').closest('.modal-sections').find('.modal_thead'));

    callSelect2AjaxProduk(q, type);
    callAjaxListGudang();
}

function barangBatal() {
    $('.modal_thead_barang_tr').remove();
    $('.table_tr_modal').remove();
}

function clearQtyGudang(){
    $('#modal_gudang_temp').val(' ');
    $('label[id*="qty_gudang_footer"]').text('');
    $('#modal_gudang').val(0).trigger('change');
}

$('#kategori_barang').change(function() {
    var searchBy = $('#searchByKategori');
    clearQtyGudang();
    if ($('#kategori_barang').val() == 1) {
        searchBy.prop('disabled', false);
        ajaxModalKategori(searchBy);

        setTimeout(function() {
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        }, 600);

        $('#searchByKategori').change(function() {
            clearQtyGudang();
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        });
    }else if($('#kategori_barang').val() == 0){
        searchBy.html(null);
        searchBy.prop('disabled', true);
        
        var kode_barang = $('.modal_kode_barang').val();
        var nama_barang = $('.modal_nama_barang').val();            
        if (kode_barang !== '' && nama_barang === '') {
            keyPressModalKodeBarang(null, kode_barang, null);
        }else if (kode_barang === '' && nama_barang !== '') {
            keyPressModalNamaBarang(null, nama_barang, null);
        }
    }
});

function ajaxModalKategori(searchBy){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-all-kategori-produk",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                searchBy.append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxKeypress(n = null, searchKey, dataKey){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-nama-produk",
        data: dataKey,
        dataType: 'JSON',
        success: function (data) {
            $('.table_tr_modal').remove();
            if (data == 'null') {
                $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="table_tr_modal">'
                        + '<td colspan="3" style="padding:5px; 20px" align="center">'
                        +   '<span class="alert alert-warning"> Tidak ada Barang dengan kata Kunci '+ searchKey +' </span>'
                        + '</td>' +
                    '</tr>').appendTo('.modal_tbody');
            }else{
                $.each(data, function (key, value) {
                    var idP = value.id;

                    $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="modal_barang_'+ key +' table_tr_modal">'
                            + '<td width="26%" style="padding:5px 15px">'
                                + value.no_barang
                            + '</td>'
                            + '<td width="3%"></td>'
                            + '<td width="55%" style="padding:5px 15px">'
                                + value.keterangan
                            + '</td>'+
                        '</tr>').appendTo('.modal_tbody');

                    // hey
                    $('.modal_barang_'+ key).hover(
                        function(){
                            $(this).attr('style', 'border: 2px solid limegreen; border-radius: 5px ;display:table; width:100%; table-layout:fixed;');
                        }, function() {
                            $(this).attr('style', 'border: none; display:table; width:100%; table-layout:fixed;');
                        }
                    );

                    $('.table_tr_modal').click(function () {
                        var ini = $(this);
                        if(ini.is(':hover') === true){
                            $('.table_tr_modal').removeClass('warnatr');
                            ini.addClass('warnatr');
                        }
                    });
                    // yeh

                    $('.modal_barang_'+ key).click(function() {
                        var ini = $(this);
                        var gudang = $('#modal_gudang').val();
                        $('#modal_gudang_temp').val(idP);
                        
                        callAjaxQtyGudang(idP, gudang);
                    });

                    $('.modal_barang_'+ key).dblclick(function(){
                        setTimeout(function() {
                            clearQtyGudang();
                        }, 0);
                        $('#produk_id'+n).val(idP);
                        $('#no_produk'+n).val(value.no_barang);
                        test(idP, n);
                        barangBatal();
                        $('#modalBarang').modal('toggle');
                    });
                    
                    $('#modal_gudang').change(function() {
                        var gudang = $('#modal_gudang').val();
                        var produk = $('#modal_gudang_temp').val();

                        callAjaxQtyGudang(produk, gudang);
                    });
                });
            }
        }
    });
}

function callAjaxQtyGudang(produk, gudang){
    $.ajax({
        type : 'GET',
        url  : "" + base_url + "/ajax/get-qty-gudang-stock",
        data : {
            gudang : gudang,
            produk : produk,
            _token : token_
        },
        dataType : 'json',
        success: function (resuponse){
            $('#qty_gudang_footer').html(resuponse);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxListGudang(){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-list-gudang",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                $('#modal_gudang').append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function test(idP, vale) {
    var produk_id   = idP;
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-produk",
        data: {
            id : produk_id,
            _token: token_
        },
        dataType: "json",
        success: function(response){
            $('#keterangan_produk'+vale).val(response.keterangan);
            $('#qty_produk'+vale).val('1');
            $('#satuan_produk'+vale).val(response.satuan);
            $('#nomor_serial'+vale).val(response.nomor_serial);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}