var base_url = window.location.origin + '/akuntansi';
var token_ = $("meta[name='csrf-token']").attr("content");
var createData = new Boolean(true);
var updateRowId = null;

var typingTimer;
var doneTypingInterval = 500;

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

$(function () {
    let exist_product = $('.produk_id').length;
    for (let a = 0; a < exist_product; a++) {
        callSelect2AjaxProdukID(a, $('#produk_id_temp' + a).val());
        callSelect2AjaxProduk(a, 'modal');
    } 
    let exist_beban = $('.amount_beban').length;
    for (let b = 0; b < exist_beban; b++) {
        callAkun(b, $('#akun_beban_id_temp').val());
    }
    setAkun();
    $('.select3').select2();
});
        
function callSelect2AjaxProduk(q = null, type = null) {
    if(type === null){
        $('.produk_id').select2({
            minimumInputLength: 3,
            formatInputTooShort: function () {
                return "Ketik 3 Karakter";
            },
            ajax: {
                url: "" + base_url + "/ajax/get-nama-produk",
                type: 'GET',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                dataType: 'JSON',
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });
    }else if(type == 'modal'){
        $('.modal_kode_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            clearTimeout(typingTimer);
            typingTimer = setTimeout( function () {
                keyPressModalKodeBarang(q, x, kategori);
            }, doneTypingInterval);
        });

        $('.modal_nama_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            clearTimeout(typingTimer);
            typingTimer = setTimeout( function () {
                keyPressModalNamaBarang(q, x, kategori);
            }, doneTypingInterval);
        });
    }
}

function callSelect2AjaxProdukID(q, id) {
    $.ajax({
        url: "" + base_url + "/ajax/get-id-produk",
        type: 'GET',
        data: 'search=' + id,
        dataType: 'JSON',
        success: function (data) {
            $.each(data.item, function (key, value) {
                $('#produk_id'+q).val(value.id);
                $('#no_produk'+q).val(value.no_barang);
                // $('#produk_id' + q).append('<option selected value="' + value.id + '">' + value.text + '</option>');
            });
        }
    });
}

function setAkun() {
    $('.akun_id').select2({
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Ketik 3 Karakter";
        },
        ajax: {
            url: "" + base_url + "/ajax/get-nama-akun",
            type: 'GET',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            dataType: 'JSON',
            processResults: function (data) {
                return {
                    results: data
                };
            }
        }
    });
}

function callAkun(q, id){
    $.ajax({
        url: "" + base_url + "/ajax/get-id-akun",
        type: 'GET',
        data: 'search=' + id,
        dataType: 'JSON',
        success: function (data) {
            $.each(data, function (key, value) {
                $('#akun_beban_id' + q).append('<option selected value="' + value.id + '">' + value.text + '</option>');
            });
        }
    });
}

        function paymentAmount() {
            $("#amount_calc").val($(".grandtotal").text());
        }
        function enable_cb() {
          if ($("input#group1").is(':checked')) {
            $("input.group2").removeAttr("disabled");
          } else {
            $("input.group2").attr("disabled", true).removeAttr("checked");
          }
        }

        $('#selectAll').click(function(e){
        var table= $(e.target).closest('table');
        $('th input:checkbox',table).prop('checked',this.checked);
        });

        $(document).ready(function () {
            $('.duplicate-penawaran-sections').on('click', '.add-penawaran', function () {
                if (old_produk_id !== null) {
                    duplicateFormWhenOld();
                }else {
                    checkIfEmpty();
                }
            });
        });

        $('.duplicate-penawaran-sections').on('click', '.remove-rincian-penawaran', function () {
            if ($(this).closest('.purchasePenawaran').find('tr').length > 0) {
                $(this).closest('tr').remove();
                calculate();
                valueOnTabBarang();
                $('.purchasePenawaran tr:last').find('td:last').append('<button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button>');
                q -= 1;
            }
        });

        $(document).ready(function () {
            $('.duplicate-beban-sections').on('click', '.add-beban', function () {
                if (old_produk_id !== null) {
                    duplicateBebanWhenOld();
                }else {
                    duplicateBeban();
                }
            });
        });

        $('.duplicate-beban-sections').on('click', '.remove-rincian-beban', function () {
            if ($(this).closest('.purchaseBeban').find('tr').length > 0) {
                $(this).closest('tr').remove();
                calculate();
            }
        });

        $('.duplicate-dp-sections').on('click', '.remove-kolom-dp', function () {
            if ($(this).closest('.purchaseDP').find('tr').length > 0) {
                $(this).closest('tr').remove();
                valueOnTabUangMuka();
            }
        });

        function duplicateForm() {
            var row = $('#table_penawaran_section').html();
            var clone = $(row).clone();
            clone.find('.no_produk').attr('name', 'no_produk[' + q + ']').attr('id', 'no_produk' + q).attr('onclick', 'awas(' + q + ')').attr('class', 'form-control no_produk').attr('readonly', true);
            clone.find('.produk_id').attr('name', 'produk_id[' + q + ']').attr('id', 'produk_id' + q).attr('class', 'form-control produk_id');
            clone.find('.keterangan_produk').attr('name', 'keterangan_produk[' + q + ']').attr('id', 'keterangan_produk' + q);
            clone.find('.qty_produk').attr('name', 'qty_produk[' + q + ']').attr('id', 'qty_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')');
            clone.find('.satuan_produk').attr('name', 'satuan_produk[' + q + ']').attr('id', 'satuan_produk' + q);
            clone.find('.unit_harga_produk').attr('name', 'unit_harga_produk[' + q + ']').attr('id', 'unit_harga_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')').attr('ondblclick', 'getListPriceHistory('+ q +')');;
            clone.find('.diskon_produk').attr('name', 'diskon_produk[' + q + ']').attr('id', 'diskon_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')');
            clone.find('.tax_produk').attr('name', 'kode_pajak_id[' + q + '][]').attr('id', 'tax_produk' + q).attr('onchange', 'calculate()');
            clone.find('.amount_produk').attr('name', 'amount_produk[' + q + ']').attr('id', 'amount_produk' + q).attr('readonly','');

            clone.find('.akun_faktur_old').attr('name', 'akun_faktur_old[' + q + ']').attr('id', 'akun_faktur_old' + q);
            clone.find('.amount_old_modal_produk').attr('name', 'amount_old_modal_produk[' + q + ']').attr('id', 'amount_old_modal_produk' + q);
            clone.find('.amount_old_qty_produk').attr('name', 'amount_old_qty_produk[' + q + ']').attr('id', 'amount_old_qty_produk' + q);
            clone.find('.amount_modal_produk').attr('name', 'amount_modal_produk[' + q + ']').attr('id', 'amount_modal_produk' + q);
            clone.find('.harga_modal').attr('name', 'harga_modal[' + q + ']').attr('id', 'harga_modal' + q);
            clone.find('.gudang_id').attr('name','gudang_id[]').attr('id','gudang_id'+q);
            clone.find('.expired_date').attr('name','expired_date[]').attr('id','expired_date'+q);
            clone.find('.dept_id').attr('name', 'dept_id[' + q + ']').attr('id', 'dept_id' + q);
            clone.find('.proyek_id').attr('name', 'proyek_id[' + q + ']').attr('id', 'proyek_id' + q);
            clone.find('.sn').attr('name', 'sn[' + q + ']').attr('id', 'sn' + q);
            clone.find('.barang_penerimaan_pembelian_id').attr('name', 'barang_penerimaan_pembelian_id[' + q + ']').attr('id', 'barang_penerimaan_pembelian_id' + q);
            $(clone).appendTo($('.duplicate-penawaran-sections').closest('.penawaran-sections').find('.purchasePenawaran'));
            $('.select3').select2();
            $('.tanggal_faktur').datepicker({format: 'dd MM yyyy', autoclose: true});
            // callSelect2AjaxProduk(q);
            setAkun();
            removeDelButton();
            q++;
        }

        function duplicateFormWhenOld() {
            var row = $('#table_penawaran_section').html();
            var clone = $(row).clone();
            clone.find('.produk_id').attr('name', 'produk_id[' + q + ']').attr('id', 'produk_id' + q).attr('class', 'form-control produk_id').val("");
            clone.find('.no_produk').attr('name', 'no_produk[' + q + ']').attr('id', 'no_produk' + q).attr('onclick', 'awas(' + q + ')').attr('class', 'form-control no_produk').attr('readonly', true).val("");
            clone.find('.keterangan_produk').attr('name', 'keterangan_produk[' + q + ']').attr('id', 'keterangan_produk' + q).val("");
            clone.find('.qty_produk').attr('name', 'qty_produk[' + q + ']').attr('id', 'qty_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')').val("");
            clone.find('.satuan_produk').attr('name', 'satuan_produk[' + q + ']').attr('id', 'satuan_produk' + q).val("");
            clone.find('.unit_harga_produk').attr('name', 'unit_harga_produk[' + q + ']').attr('id', 'unit_harga_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')').attr('ondblclick', 'getListPriceHistory('+ q +')').val("");
            clone.find('.diskon_produk').attr('name', 'diskon_produk[' + q + ']').attr('id', 'diskon_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')').val("");
            clone.find('.tax_produk').attr('name', 'kode_pajak_id[' + q + '][]').attr('id', 'tax_produk' + q).attr('onchange', 'calculate()').val("");
            clone.find('.amount_produk').attr('name', 'amount_produk[' + q + ']').attr('id', 'amount_produk' + q).attr('readonly','').val("");

            clone.find('.akun_faktur_old').attr('name', 'akun_faktur_old[' + q + ']').attr('id', 'akun_faktur_old' + q).val("");
            clone.find('.amount_old_modal_produk').attr('name', 'amount_old_modal_produk[' + q + ']').attr('id', 'amount_old_modal_produk' + q).val("");
            clone.find('.amount_old_qty_produk').attr('name', 'amount_old_qty_produk[' + q + ']').attr('id', 'amount_old_qty_produk' + q).val("");
            clone.find('.amount_modal_produk').attr('name', 'amount_modal_produk[' + q + ']').attr('id', 'amount_modal_produk' + q).val("");
            clone.find('.harga_modal').attr('name', 'harga_modal[' + q + ']').attr('id', 'harga_modal' + q).val("");
            clone.find('.gudang_id').attr('name','gudang_id[]').attr('id','gudang_id'+q);
            clone.find('.expired_date').attr('name','expired_date[]').attr('id','expired_date'+q).val("");
            clone.find('.dept_id').attr('name', 'dept_id[' + q + ']').attr('id', 'dept_id' + q).val("");
            clone.find('.proyek_id').attr('name', 'proyek_id[' + q + ']').attr('id', 'proyek_id' + q).val("");
            clone.find('.sn').attr('name', 'sn[' + q + ']').attr('id', 'sn' + q).val("");
            clone.find('.barang_penerimaan_pembelian_id').attr('name', 'barang_penerimaan_pembelian_id[' + q + ']').attr('id', 'barang_penerimaan_pembelian_id' + q).val("");
            $(clone).appendTo($('.duplicate-penawaran-sections').closest('.penawaran-sections').find('.purchasePenawaran'));
            $('.select3').select2();
            $('.tanggal_faktur').datepicker({format: 'dd MM yyyy', autoclose: true});
            // callSelect2AjaxProduk(q);
            setAkun();
            removeDelButton();
            q++;
        }

        function duplicateBeban() {
            var row = $('#table_beban_section').html();
            var clone = $(row).clone();
            clone.find('.akun_beban_id').attr('name', 'akun_beban_id[' + x + ']').attr('id', 'akun_beban_id' + x).attr('onchange', 'setNamaAkun(' + x + ')');
            clone.find('.amount_beban').attr('name', 'amount_beban[' + x + ']').attr('id', 'amount_beban' + x).attr('onchange', 'calculate()').autoNumeric('init',{aPad: false}).val(0);
            clone.find('.notes_beban').attr('name', 'notes_beban[' + x + ']').attr('id', 'notes_beban' + x);
            clone.find('.dept_beban_id').attr('name', 'dept_beban_id[' + x + ']').attr('id', 'dept_beban_id' + x);
            clone.find('.proyek_beban_id').attr('name', 'proyek_beban_id[' + x + ']').attr('id', 'proyek_beban_id' + x);
            clone.find('.buka_pemasok').attr('name', 'buka_pemasok[' + x + ']').attr('id', 'buka_pemasok' + x).attr('onchange', 'checkSesamaFungsi('+ x +')');
            clone.find('.alokasi_ke_barang').attr('name', 'alokasi_ke_barang[' + x + ']').attr('id', 'alokasi_ke_barang' + x).attr('onchange', 'checkSesamaFungsi('+ x +')');
            clone.find('.txt_alokasi_ke_barang').attr('name', 'txt_alokasi_ke_barang[' + x + ']').attr('id', 'txt_alokasi_ke_barang' + x);
            clone.find('.pemasok_beban_id').attr('name', 'pemasok_beban_id[' + x + ']').attr('id', 'pemasok_beban_id' + x).attr('class', 'select3 form-control').attr('onchange', 'checkSesamaFungsi('+ x +')').attr('disabled','disabled');
            clone.find('.pemasok_beban_id_real').attr('name', 'pemasok_beban_id_real[' + x + ']').attr('id', 'pemasok_beban_id_real' + x).attr('class', 'pemasok_beban_id_real');
            $(clone).appendTo($('.duplicate-beban-sections').closest('.beban-sections').find('.purchaseBeban'));
            $('.select3').select2();
            // callSelect2AjaxProduk();
            setAkun();
            checkSesamaFungsi(x);
            x++;
        }

        function duplicateBebanWhenOld() {
            var row = $('#table_beban_section').html();
            var clone = $(row).clone();
            clone.find('.akun_beban_id').attr('name', 'akun_beban_id[' + x + ']').attr('id', 'akun_beban_id' + x).attr('onchange', 'setNamaAkun(' + x + ')').val("");
            clone.find('.amount_beban').attr('name', 'amount_beban[' + x + ']').attr('id', 'amount_beban' + x).attr('onchange', 'calculate()').val("").autoNumeric('init',{aPad: false}).val(0);
            clone.find('.notes_beban').attr('name', 'notes_beban[' + x + ']').attr('id', 'notes_beban' + x).val("");
            clone.find('.dept_beban_id').attr('name', 'dept_beban_id[' + x + ']').attr('id', 'dept_beban_id' + x).val("");
            clone.find('.proyek_beban_id').attr('name', 'proyek_beban_id[' + x + ']').attr('id', 'proyek_beban_id' + x).val("");
            clone.find('.buka_pemasok').attr('name', 'buka_pemasok[' + x + ']').attr('id', 'buka_pemasok' + x).attr('onchange', 'checkSesamaFungsi('+ x +')').removeAttr('checked');
            clone.find('.alokasi_ke_barang').attr('name', 'alokasi_ke_barang[' + x + ']').attr('id', 'alokasi_ke_barang' + x).attr('onchange', 'checkSesamaFungsi()').removeAttr('checked');
            clone.find('.txt_alokasi_ke_barang').attr('name', 'txt_alokasi_ke_barang[' + x + ']').attr('id', 'txt_alokasi_ke_barang' + x);
            clone.find('.pemasok_beban_id').attr('name', 'pemasok_beban_id[' + x + ']').attr('id', 'pemasok_beban_id' + x).attr('class', 'select3 form-control').attr('onchange', 'checkSesamaFungsi('+ x +')').val("");
            clone.find('.pemasok_beban_id_real').attr('name', 'pemasok_beban_id_real[' + x + ']').attr('id', 'pemasok_beban_id_real' + x).attr('class', 'pemasok_beban_id_real');
            $(clone).appendTo($('.duplicate-beban-sections').closest('.beban-sections').find('.purchaseBeban'));
            $('.select3').select2();
            // callSelect2AjaxProduk();
            setAkun();
            checkSesamaFungsi(x);
            x++;
        }

        function duplicateDP() {
            var row = $('#table_dp_section').html();
            var clone = $(row).clone();
            clone.find('.faktur_dp_id').attr('name', 'faktur_dp_id[' + z + ']').attr('id', 'faktur_dp_id' + z);
            clone.find('.akun_dp_id').attr('name', 'akun_dp_id[' + z + ']').attr('id', 'akun_dp_id' + z);
            clone.find('.keterangan_dp').attr('name', 'keterangan_dp[' + z + ']').attr('id', 'keterangan_dp' + z);
            clone.find('.pajak_dp').attr('name', 'pajak_dp[' + z + '][]').attr('id', 'pajak_dp' + z);
            clone.find('.total_dp').attr('name', 'total_dp[' + z + ']').attr('id', 'total_dp' + z).attr('onchange','valueOnTabUangMuka()');
            clone.find('.no_faktur_dp').attr('name', 'no_faktur_dp[' + x + ']').attr('id', 'no_faktur_dp' + z);
            clone.find('.no_po_dp').attr('name', 'no_po_dp[' + z + ']').attr('id', 'no_po_dp' + z);
            clone.find('.include_tax_dp').attr('name', 'include_tax_dp[' + z + ']').attr('id', 'include_tax_dp' + z);
            $(clone).appendTo($('.duplicate-dp-sections').closest('.dp-sections').find('.purchaseDP'));
            $('.select3').select2();
            z++;
            // callSelect2AjaxProduk();
            setAkun();
        }

function sumQtyAndUnit(param) {
    cekQtyandPrice(param);
    var amount = 0;
    let disc = 0;

    amount = parseInt($('#qty_produk' + param).val()) * parseInt(changeMaskingToNumber($('#unit_harga_produk' + param).val()));

    if (isNaN($('#diskon_produk' + param).val()) || $('#diskon_produk' + param).val() == '') {
    } else {
        disc = amount * parseFloat($('#diskon_produk' + param).val()) / 100;
    }
    var final_amount = amount - disc;
    $('#amount_produk' + param).val(final_amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
    $('#amount_modal_produk' + param).val(amount - disc);
    calculate();
}

function checkSesamaFungsi(x) {
    $('.akun_beban_id').each(function (index, key) {
        if ($('#pemasok_beban_id' + x).val() != "") {
            $('#alokasi_ke_barang' + x).prop('checked', true);
        }

        if (!$('#buka_pemasok' + x).is(':checked')){
            $('#pemasok_beban_id' + x).val("");
            $('#pemasok_beban_id_real' + x).val("");
            $('#pemasok_beban_id' + x).hide().prop('disabled', true);   
        }else{
            $('#pemasok_beban_id' + x).hide().prop('disabled', false);
            let selected_pemasok_beban = $('#pemasok_beban_id' + x).find(":selected").val();
            $('#pemasok_beban_id_real' + x).val(selected_pemasok_beban);
            $('#alokasi_ke_barang' + x).prop('checked', true);
        }

        if (!$('#alokasi_ke_barang' + x).is(':checked')) {
            $('#txt_alokasi_ke_barang' + x).val(0);
        }else{
            $('#txt_alokasi_ke_barang' + x).val(1);            
        }
    });
    calculate();
}

function checkIfEmpty() {
    if ($('#pemasok_id').val() === "") {
        swal({
            title: "Kesalahan!",
            text: "Anda belum memilih pemasok!",
            icon: "error",
            buttons: {
                confirm: true,
            },
        })
    } else {
        duplicateForm();
    }
}

function checkAfterSetPemasok() {
    // var item_penerimaan = $('.penerimaan_item').length;
    // callBackDp();
    // if (item_penerimaan > 0) {
    //     swal({
    //         title: "Peringatan!",
    //         text: "Pemasok ini memiliki penerimaan dan belum digunakan, ingin gunakan penerimaan?",
    //         icon: "warning",
    //         buttons: {
    //             cancel: true,
    //             confirm: true,
    //         },
    //     }).then((result) => {
    //         if (result) {
    //             $('#penerimaan').modal('show');
    //         }
    //         callBackDp();
    //     });
    // }
}

function callBackDp() {
    var item_penawaran = $('.uangmuka_item').length;
    if (item_penawaran > 0) {
        swal({
            title: "Peringatan!",
            text: "Pemasok ini memiliki uang muka dan tidak digunakan, ingin gunakan uang muka?",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: true,
            },
        }).then((result) => {
            if (result) {
                $('#pengiriman').modal('show');
            }
        });
    }
    // callSelect2AjaxProduk();
}

function calculate() {
    var subTotal = 0;
    var subTotalWithBeban = 0;
    var grandtotal = 0;
    var diskon = changeMaskingToNumber($("#totalPotonganRupiah").val());
    var ongkir = changeMaskingToNumber($('#ongkir').val());
    var tax = 0;
    var tax_val = 0;
    var in_tax = 0;
    var amount = [];
    var sorting = [];
    $(".amount_produk").autoNumeric('init',{aPad: false});
    $('#taxNaration').html('');
    $(".amount_produk").each(function () {
        amount.push([$(this).autoNumeric('get')]);
    });

    
    $(".amount_produk").each(function () {
        subTotal += parseInt($(this).autoNumeric('get'));
        subTotalWithBeban += parseInt(changeMaskingToNumber($(this).val()));
        // amount[$(this).index()].push($(this).val());
    });

    //hitung total beban 
    let total_beban = 0;
    $('.amount_beban').each(function(){
        total_beban += parseInt(changeMaskingToNumber($(this).val()));       
    });
    let beban_masuk_ke_total = 0;
    
    $('.amount_modal_produk').each(function (ind, kunci) {
        let harga_modal = $('#amount_modal_produk' + ind).val();
        let jumlah = $('#qty_produk' + ind).val();

        let harga_modal_lama = $('#amount_old_modal_produk' + ind).val();
        let jumlah_lama = $('#amount_old_qty_produk' + ind).val();

        let harga_modal_jadi = 0;
        harga_modal_jadi = (parseFloat(harga_modal) + parseFloat(harga_modal_lama)) / (parseInt(jumlah) + parseInt(jumlah_lama));
        // set
        $('#amount_modal_produk' + ind).val(harga_modal_jadi);
    });    

    // masukkan total beban ke barang
    $('.akun_beban_id').each(function (index, key) {        

        if ($('#alokasi_ke_barang'+index).is(':checked')){
            $('.amount_modal_produk').each(function (ind, kunci) {
                let harga_modal = $('#amount_modal_produk' + ind).val();
                let jumlah = $('#qty_produk' + ind).val();
                
                let harga_modal_lama = $('#amount_old_modal_produk' + ind).val();
                let jumlah_lama = $('#amount_old_qty_produk' + ind).val();
                
                let harga_modal_jadi = 0;
                harga_modal_jadi = (parseFloat(harga_modal) + parseFloat(total_beban) + parseFloat(harga_modal_lama)) / (parseInt(jumlah) + parseInt(jumlah_lama) );
                // set
                $('#amount_modal_produk' + ind).val(harga_modal_jadi);
            });               
        }

        if ($('#pemasok_beban_id' + index).val() == ""){
            beban_masuk_ke_total += parseFloat(changeMaskingToNumber($('#amount_beban' + index).val()));
            if (isNaN(beban_masuk_ke_total)){
                beban_masuk_ke_total = 0;
            }
        }
        
    });
    subTotalWithBeban += parseFloat(beban_masuk_ke_total);

    if ($("#diskonTotal").val() > 0) {
        diskon = subTotal * $('#diskonTotal').val() / 100;
    }

    // diskon_per_item = diskon / $(".produk_id").length;
    $.each(amount, function (index, key) {
        var total_tax = 0;
        $('#tax_produk' + index + ' option:selected').each(function () {
            var str = $(this).text();
            var spl = str.split("/");
            total_tax += parseFloat(spl[0].replace(/ [0-9]*/, ''));     
        });
        // var amount_after_dikurang = parseFloat(key) - diskon_per_item;
        var after_dikurang_diskon = key / subTotal * diskon;
        $('#tax_produk' + index + ' option:selected').each(function () {
            var str = $(this).text();
            var spl = str.split("/");
            let temp_tax = (100 / (100 + total_tax) * (key - after_dikurang_diskon));
            var sort_temp = {
                label: spl[1].replace(/ [0-9]*/, ''),
                nilai: spl[0].replace(/ [0-9]*/, ''),
                total: (key - after_dikurang_diskon) * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100,
                total_in_tax: temp_tax * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100
            }
            sorting.push(sort_temp);
        });
    });

    var sorted = sorting.sort(function (a, b) {
        return a.nilai - b.nilai;
    });

    var sorted_temp = sorted;
    var unduplicateItem = [];
    for (var i = 0; i < sorted_temp.length; i++) {
        if (sorted_temp.length >= 2) {
            if (sorted_temp[i + 1] != null) {
                if (sorted_temp[i]['nilai'] != sorted_temp[i + 1]['nilai'] && sorted_temp[i]['label'] != sorted_temp[i + 1]['label']) {
                    unduplicateItem.push(sorted_temp[i]);
                } else {
                    sorted_temp[i + 1]['total'] = sorted_temp[i]['total'] + sorted_temp[i + 1]['total'];
                    sorted_temp[i + 1]['total_in_tax'] = sorted_temp[i]['total_in_tax'] + sorted_temp[i + 1]['total_in_tax'];
                }
            } else {
                unduplicateItem.push(sorted_temp[i]);
            }
        } else {
            unduplicateItem.push(sorted_temp[i]);
        }
    }

    $.each(unduplicateItem, function (index, key) {
        var tot_tax = 0;
        var tax_label = ''; 
        if ($('input.in_tax').is(':checked')) {
            tax += key['total_in_tax'];
            tax_val += key['total_in_tax'];
            $("#taxNaration").append(key['label'] + " " + key['nilai'] + "% = " + key['total_in_tax'].toFixed(2) + "<br>");
            tot_tax = key['total_in_tax'];
            tax_label = key['label'];
        } else {
            tax += key['total'];
            tax_val += key['total'];
            $("#taxNaration").append(key['label'] + " " + key['nilai'] + "% = " + key['total'].toFixed(2) + "<br>");
            tot_tax = key['total'];
            tax_label = key['label'];
        }
        $("#tax_cetak"+index).val(tot_tax.toFixed(2));
        $("#tax_cetak_label"+index).val(tax_label);
    });

    if ($('input.taxable').is(':checked')) {
        $('.tax_produk').attr('disabled', false);
        $(".tax_produk").select2({
            maximumSelectionLength: 2
        });
        tax = tax;
        $('#taxNaration').show();
        if ($('input.in_tax').is(':checked')) {
            tax = 0;
            //balik lagi 0 karena harga sudah sama ongkir
        }
    } else {
        $('.tax_produk').attr('disabled', true);
        $('#taxNaration').hide();
        tax = 0;
    }

    if (beban_masuk_ke_total == 0) {
        // grandtotal = parseFloat(subTotal) - parseFloat(diskon) + parseFloat(tax) + parseInt(ongkir);
        grandtotal = parseFloat(subTotal) - parseFloat(diskon) + parseFloat(tax);
        $('#subtotal').html("<strong>" + Number(subTotal).toLocaleString() + "</strong>");
    }else {
        grandtotal = parseFloat(subTotalWithBeban) - parseFloat(diskon) + parseFloat(tax) ;
        $('#subtotal').html("<strong>" + Number(subTotalWithBeban).toLocaleString() + "</strong>");
    }

    
    // view
    $('.grandtotal').html("<strong>" + Number(parseFloat(grandtotal)).toLocaleString() + "</strong>");
    // value
    $('#totalPotonganRupiah').autoNumeric('init').autoNumeric('set', diskon);
    $('.kolom_grandtotal').val(parseFloat(grandtotal));
    $('.kolom_subtotal').val(parseFloat(subTotal));
    $('.kolom_subtotal_beban').val(parseFloat(subTotalWithBeban));
    $('.kolom_pajaktotal').val(tax_val);
    valueOnTabBeban();
    
}

function valueOnTabBarang() {
    let value_amount = 0;
    $('.amount_produk').each(function () {
        value_amount += parseFloat(changeMaskingToNumber(this.value) * 1);
    });
    $("#pills-barang-tab").find("strong#total-amount-barang").text(Number(value_amount).toLocaleString());
}

function valueOnTabBeban() {
    let value_amount = 0;
    $('.amount_beban').each(function () {
        value_amount += parseFloat(changeMaskingToNumber(this.value) * 1);
    });
    $("#pills-beban-tab").find("strong#total-amount-beban").text(Number(value_amount).toLocaleString());
}

function valueOnTabUangMuka() {
    let value_amount = 0;
    $('.total_dp').each(function () {
        value_amount += parseFloat(changeMaskingToNumber(this.value) * 1);
    });
    $("#pills-uang-tab").find("strong#counterDp").text(Number(value_amount).toLocaleString());
}

function cekQtyandPrice(param) {
    if ($("#qty_produk"+ param).val() == "" || $("#qty_produk"+ param).val() <= 0) {
        $("#qty_produk"+ param).val(1);
        swal({
            icon: "error",
            text: "Kuantitas harus lebih besar dari 0"
        });
    }
    if ($("#unit_harga_produk"+ param).val() == "") {
        $("#unit_harga_produk"+ param).val(0);   
    }
}

function removeDelButton() {
    $('.remove-rincian-penawaran').not(':last').remove();
}

function setDataPemasokWhenOld(pemasok_id){  
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-alamat-pemasok",
        data: {id : pemasok_id,
            _token: token_},
        dataType: "json",
        success: function(response){
        $('#table-list-pesanan').find('tr').remove().end();
        $('#table-list-penerimaan').find('tr').remove().end();
        $('#table-list-uang-muka').find('tr').remove().end();

        if (response.pesanan) {
            $.each(response.pesanan, function (index, item){
                $('#table-list-pesanan').append('<tr><th><input type="checkbox" value="'+item.id+'" name="pesanan-pembelian[]" id="pesanan-pembelian[]"/></th><td>'+item.po_number+'</td><td>'+item.po_date+'</td></tr>');
            });
        }
        if (response.penerimaan) {
            $.each(response.penerimaan, function (index, item){
                $('#table-list-penerimaan').append('<tr><th><input type="checkbox" value="'+item.id+'" name="penerimaan-pembelian[]" class="penerimaan_item" id="penerimaan-pembelian[]"/></th><td>'+item.form_no+'</td><td>'+item.receipt_no+'</td><td>'+item.receive_date+'</td></tr>');                        
            });
        }
        if (response.uangmuka) {
            $.each(response.uangmuka, function (index, item){
                if (item.updated_uang_muka > 0) {
                    $('#table-list-uang-muka').append('<tr><th><input type="checkbox" value="'+item.id+'" name="uangmuka[]" class="uangmuka_item" id="uangmuka[]"/></th><td>'+item.no_faktur+'</td><td>'+item.invoice_date+'</td><td>'+item.updated_uang_muka+'</td></tr>');
                }
            });
        }
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function keyPressModalKodeBarang(q, x, kategori = null){
    var no_barang = x;
    var data = {
        kategoriBySearch : kategori,
        no_barang : no_barang,
        _token: token_,
    }
    $('.modal_nama_barang').val('');
    callAjaxKeypress(q, no_barang, data)
}

function keyPressModalNamaBarang(q, x, kategori){
    var keterangan = x;
    var data = {
        kategoriBySearch : kategori,
        keterangan : keterangan,
        _token: token_,
    }
    $('.modal_kode_barang').val('');
    callAjaxKeypress(q, keterangan, data)
}


function awas(q = null){
    var type = 'modal';
    $('#modalBarang').modal({
        backdrop: 'static',
        keyboard: false, 
        show: true
    });
    var row = $('#modal_thead_barang').html();
    var clone = $(row).clone();
    clone.find('.modal_kode_barang').attr('name', 'modal_kode_barang[]').attr('id', 'modal_kode_barang' + q).attr('class', 'form-control modal_kode_barang');
    clone.find('.modal_nama_barang').attr('name', 'modal_nama_barang[]').attr('id', 'modal_nama_barang' + q).attr('class', 'form-control modal_nama_barang');;
    $(clone).appendTo($('.duplicate-modal-sections').closest('.modal-sections').find('.modal_thead'));

    callSelect2AjaxProduk(q, type);
    callAjaxListGudang();

    $('body').keydown(function(e) {
        var keyCode = e.keyCode || e.which;
        if ( keyCode == 27 ) {
            barangBatal();
            $('#modalBarang').modal('toggle');
        }
    });
}

function barangBatal() {
    $('.modal_thead_barang_tr').remove();
    $('.table_tr_modal').remove();
}

function clearQtyGudang(){
    $('#modal_gudang_temp').val(' ');
    $('label[id*="qty_gudang_footer"]').text('');
    $('#modal_gudang').val(0).trigger('change');
}

$('#kategori_barang').change(function() {
    var searchBy = $('#searchByKategori');
    clearQtyGudang();
    if ($('#kategori_barang').val() == 1) {
        searchBy.prop('disabled', false);
        ajaxModalKategori(searchBy);

        setTimeout(function() {
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        }, 600);

        $('#searchByKategori').change(function() {
            clearQtyGudang();
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        });
    }else if($('#kategori_barang').val() == 0){
        searchBy.html(null);
        searchBy.prop('disabled', true);
        
        var kode_barang = $('.modal_kode_barang').val();
        var nama_barang = $('.modal_nama_barang').val();            
        if (kode_barang !== '' && nama_barang === '') {
            keyPressModalKodeBarang(null, kode_barang, null);
        }else if (kode_barang === '' && nama_barang !== '') {
            keyPressModalNamaBarang(null, nama_barang, null);
        }
    }
});

function ajaxModalKategori(searchBy){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-all-kategori-produk",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                searchBy.append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxKeypress(n = null, searchKey, dataKey){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-nama-produk",
        data: dataKey,
        dataType: 'JSON',
        success: function (data) {
            $('.table_tr_modal').remove();
            if (data == 'null') {
                $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="table_tr_modal">'
                        + '<td colspan="3" style="padding:5px; 20px" align="center">'
                        +   '<span class="alert alert-warning"> Tidak ada Barang dengan kata Kunci '+ searchKey +' </span>'
                        + '</td>' +
                    '</tr>').appendTo('.modal_tbody');
            }else{
                $.each(data, function (key, value) {
                    var idP = value.id;

                    $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="modal_barang_'+ key +' table_tr_modal">'
                            + '<td width="26%" style="padding:5px 15px">'
                                + value.no_barang
                            + '</td>'
                            + '<td width="3%"></td>'
                            + '<td width="55%" style="padding:5px 15px">'
                                + value.keterangan
                            + '</td>'+
                        '</tr>').appendTo('.modal_tbody');

                    $('.modal_barang_'+ key).hover(
                        function(){
                            $(this).attr('style', 'border: 2px solid limegreen; border-radius: 5px ;display:table; width:100%; table-layout:fixed;');
                        }, function() {
                            $(this).attr('style', 'border: none; display:table; width:100%; table-layout:fixed;');
                        }
                    );

                    $('.table_tr_modal').click(function () {
                        var ini = $(this);
                        if(ini.is(':hover') === true){
                            $('.table_tr_modal').removeClass('warnatr');
                            ini.addClass('warnatr');
                        }
                    });

                    $('.modal_barang_'+ key).click(function() {
                        var ini = $(this);
                        var gudang = $('#modal_gudang').val();
                        $('#modal_gudang_temp').val(idP);
                        
                        callAjaxQtyGudang(idP, gudang);
                    });

                    $('.modal_barang_'+ key).dblclick(function(){
                        setTimeout(function() {
                            clearQtyGudang();
                        }, 0);
                        $('#produk_id'+n).val(idP);
                        $('#no_produk'+n).val(value.no_barang);
                        test(idP, n);
                        barangBatal();
                        $('#modalBarang').modal('toggle');
                    });
                    
                    $('#modal_gudang').change(function() {
                        var gudang = $('#modal_gudang').val();
                        var produk = $('#modal_gudang_temp').val();
                        clearTimeout(typingTimer);
                        typingTimer = setTimeout( function () {
                            callAjaxQtyGudang(produk, gudang);
                        }, doneTypingInterval);
                    });
                });
            }
        }
    });
}

function callAjaxQtyGudang(produk, gudang){
    $.ajax({
        type : 'GET',
        url  : "" + base_url + "/ajax/get-qty-gudang-stock",
        data : {
            gudang : gudang,
            produk : produk,
            _token : token_
        },
        dataType : 'json',
        success: function (resuponse){
            $('#qty_gudang_footer').html(resuponse);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxListGudang(){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-list-gudang",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                $('#modal_gudang').append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function test(idP, vale) { // js dari blade create dan edit
    var produk_id   = idP;
    var pemasok_id   = $('#pemasok_id').val();
        $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-produk",
        data: {
            id : produk_id,
                pemasok_id : pemasok_id,
            _token: token_
        },
        dataType: "json",
        success: function(response){
            $('#tax_produk'+vale).find('option').remove().end();   
            $('#gudang_id'+vale).find('option').remove().end();                                                                 
            $('#keterangan_produk'+vale).val(response.keterangan);
            $('#qty_produk'+vale).val('1');
            $('#satuan_produk'+vale).val(response.satuan);
            $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
            $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0);
            // if(!$.trim(response.harga_modal)){
            //     $('#amount_old_modal_produk'+vale).val(response.harga_modal.harga_modal);
            //     $('#amount_old_qty_produk'+vale).val(response.harga_modal.kuantitas);
            // }else{
            //     $('#amount_old_modal_produk'+vale).val(0);
            //     $('#amount_old_qty_produk'+vale).val(0);
            // }

            if(response.harga_modal !== null){
                $('#amount_old_modal_produk'+vale).val(response.harga_modal.harga_modal);
                $('#amount_old_qty_produk'+vale).val(response.harga_modal.kuantitas);
            }else{
                $('#amount_old_modal_produk'+vale).val(0);
                $('#amount_old_qty_produk'+vale).val(0);
            }

            $('#diskon_produk'+vale).val('0');
            $.each(response.tax, function (index, item){
                $('#tax_produk'+vale).append( 
                $("<option></option>")
                .text(item.nilai+ "/" +item.nama)
                .val(item.id)
                );
            });
            
            $(response.multiGudang).each(function (val, text) {
                $(text.gudang).each(function (index, item) {
                    $('#gudang_id'+vale).append(`<option value="${item.id}">${item.nama}</option>`);
                });
            });

            sumQtyAndUnit(vale);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function getListPriceHistory(q) {
    var pemasok         = $('#pemasok_id').val();
    var produk          = $('#produk_id' + q).val();
    var harga           = $('#unit_harga_produk' + q);

    if (pemasok == '' || pemasok == null) {
        swal({
            icon: "error",
            text: "Pemasok Tidak Boleh Kosong!"
        });
    }else if ( produk == '' || produk == null ) {
        swal({  
            icon: "error",
            text: "Produk Tidak Boleh Kosong!"
        });
    } else {
        $('#modalPriceHistory').modal({
            backdrop: 'static',
            keyboard: false, 
            show: true
        });
        $('.price_history_name').html( $('#keterangan_produk' + q).val() );
        $('.pelanggan_price_history').html( $('#pemasok_id')[0].selectedOptions[0].text );
        $.ajax({
            type: "POST",
            url: "" + base_url + "/ajax/get-list-history-price-item",
            data:   { 
                        sov : pemasok,
                        type : 'pembelian',
                        item : produk,
                        _token: token_
                    },
            dataType: "json",
            success: function(response){
                $.map(response, function(item, index) {
                    $(".modal_tbody_price_history").append("<tr class='post_price table_tr_modal' id='row_price_history_"+ index +"'> "+
                                                "<td>"+item['tanggal']+"</td>"+
                                                "<td>"+item['kuantitas']+"</td>"+
                                                "<td>"+item['unit']+"</td>"+
                                                "<td class='harga_history'>"+item['harga']+"</td>"+
                                                "<td>"+item['diskon']+"</td>"+
                                            "</tr>");
                    $('.harga_history').autoNumeric('init', {vMin: '0', vMax: '999999999' })
                    harga.autoNumeric('init', {vMin: '0', vMax: '999999999' })
                    $('#row_price_history_'+ index).dblclick(function(){
                        harga.autoNumeric('init').autoNumeric('set', item['harga']);
                        $('.table_tr_modal').remove();
                        $('#modalPriceHistory').modal('toggle');
                        sumQtyAndUnit(q);
                    });
                });
            }, failure: function(errMsg) {
                alert(errMsg);
            }
        });
    }
}