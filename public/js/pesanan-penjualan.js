var base_url = window.location.origin + '/akuntansi';

var token_ = $("meta[name='csrf-token']").attr("content");
var createData = new Boolean(true);
var updateRowId = null;

var typingTimer;
var doneTypingInterval = 500;

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

$(function () {
    let exist_product = $('.produk_id').length;
    for (let a = 0; a < exist_product; a++) {
        callSelect2AjaxProdukID(a, $('#produk_id_temp' + a).val());
        callSelect2AjaxProduk(a, 'modal');
    }
    $('.select3').select2();
});
        
function callSelect2AjaxProduk(q = null, type = null) {
    if(type === null){
        $('.produk_id').select2({
            minimumInputLength: 3,
            formatInputTooShort: function () {
                return "Ketik 3 Karakter";
            },
            ajax: {
                url: "" + base_url + "/ajax/get-nama-produk",
                type: 'GET',
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                dataType: 'JSON',
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });    
    }else if(type == 'modal'){
        $('.modal_kode_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            clearTimeout(typingTimer);
            typingTimer = setTimeout( function () {
                keyPressModalKodeBarang(q, x, kategori);
            }, doneTypingInterval);
        });
        
        $('.modal_nama_barang').keyup(function(e){
            var kategori = $('#searchByKategori').val();
            var x = $(this).val();
            clearTimeout(typingTimer);
            typingTimer = setTimeout( function () {
                keyPressModalNamaBarang(q, x, kategori);
            }, doneTypingInterval);
            
        });
    }
}

function callSelect2AjaxProdukID(q, id) {
    $.ajax({
        url: "" + base_url + "/ajax/get-id-produk",
        type: 'GET',
        data: 'search=' + id,
        dataType: 'JSON',
        success: function (data) {
            $.each(data.item, function (key, value) {
                $('#produk_id'+q).val(value.id);
                $('#no_produk'+q).val(value.no_barang);
            //     $('#produk_id' + q).append('<option selected value="' + value.id + '">' + value.text + '</option>');
            });
        }
    });
}

function removeDelButton() {
    $('.remove-rincian-pesanan').not(':last').remove();
}

function duplicateForm() {
    var row = $('#table_pesanan_section').html();
    var clone = $(row).clone();
    clone.find('.no_produk').attr('name', 'no_produk[]').attr('id', 'no_produk' + q).attr('onclick', 'awas(' + q + ')').attr('class', 'form-control no_produk').attr('readonly', true);
    clone.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('class', 'form-control produk_id');
    clone.find('.penawaran_id').attr('name', 'penawaran_id[]').attr('id', 'penawaran_id' + q).attr('class', 'form-control penawaran_id');
    // clone.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('onchange', 'test(' + q + ')').attr('class', 'select2 form-control produk_id');
    // clone.find('.penawaran_id').attr('id', 'penawaran_id' + q).attr('name', 'penawaran_id[]');
    clone.find('.keterangan_produk').attr('name', 'keterangan_produk[]').attr('id', 'keterangan_produk' + q);
    clone.find('.qty_produk').attr('name', 'qty_produk[]').attr('id', 'qty_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')');
    clone.find('.satuan_produk').attr('name', 'satuan_produk[]').attr('id', 'satuan_produk' + q);
    clone.find('.unit_harga_produk').attr('name', 'unit_harga_produk[]').attr('id', 'unit_harga_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')').attr('ondblclick', 'getListPriceHistory('+ q +')');
    clone.find('.diskon_produk').attr('name', 'diskon_produk[]').attr('id', 'diskon_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')');
    clone.find('.tax_produk').attr('name', 'kode_pajak_id[' + q + '][]').attr('id', 'tax_produk' + q).attr('onchange', 'calculate()');
    clone.find('.amount_produk').attr('name', 'amount_produk[]').attr('id', 'amount_produk' + q).attr('readonly', '');
    clone.find('.dept_id').attr('name', 'dept_q[]').attr('id', 'dept_id' + q);
    clone.find('.proyek_id').attr('name', 'proyek_q[]').attr('id', 'proyek_id' + q);
    clone.find('.barang_id').attr('name', 'barang_id[]').attr('id', 'barang_id' + q);
    clone.find('.terproses').attr('name', 'terproses[]').attr('id', 'terproses' + q);
    clone.find('.ditutup').attr('name', 'ditutup[]').attr('id', 'ditutup' + q);
    clone.find('.harga_modal').attr('name', 'harga_modal[]').attr('id', 'harga_modal' + q);
    $(clone).appendTo($('.duplicate-pesanan-sections').closest('.pesanan-sections').find('.purchasePesanan'));
    $('.select3').select2();
    // callSelect2AjaxProduk(q);
    removeDelButton();
    q++;
}

function duplicatePesanan() {
    var row = $('#table_pesanan_section').html();
    var clone = $(row).clone();
    clone.find('.no_produk').attr('name', 'no_produk[]').attr('id', 'no_produk' + q).attr('onclick', 'awas(' + q + ')').attr('class', 'form-control no_produk').attr('readonly', true).val('');
    clone.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('class', 'form-control produk_id').val('');
    clone.find('.penawaran_id').attr('name', 'penawaran_id[]').attr('id', 'penawaran_id' + q).attr('class', 'form-control penawaran_id').val('');
    clone.find('.produk_id_temp').attr('name', 'produk_id_temp[]').attr('id', 'produk_id_temp' + q).attr('class', 'form-control produk_id_temp').val('');
    // clone.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('onchange', 'test(' + q + ')').attr('class', 'select2 form-control produk_id').val("");
    // clone.find('.penawaran_id').attr('id', 'penawaran_id' + q).attr('name', 'penawaran_id[]').val("");
    clone.find('.keterangan_produk').attr('name', 'keterangan_produk[]').attr('id', 'keterangan_produk' + q).val("");
    clone.find('.qty_produk').attr('name', 'qty_produk[]').attr('id', 'qty_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')').val("");
    clone.find('.satuan_produk').attr('name', 'satuan_produk[]').attr('id', 'satuan_produk' + q).val("");
    clone.find('.unit_harga_produk').attr('name', 'unit_harga_produk[]').attr('id', 'unit_harga_produk' + q).attr('ondblclick', 'getListPriceHistory('+ q +')').val("");
    clone.find('.diskon_produk').attr('name', 'diskon_produk[]').attr('id', 'diskon_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')').val("");
    clone.find('.tax_produk').attr('name', 'kode_pajak_id[' + q + '][]').attr('id', 'tax_produk' + q).attr('onchange', 'calculate()').val("");
    clone.find('.amount_produk').attr('name', 'amount_produk[]').attr('id', 'amount_produk' + q).attr('readonly', '').val("");
    // .attr('onchange', 'sumQtyAndUnit(' + q + ')').
    clone.find('.dept_id').attr('name', 'dept_q[]').attr('id', 'dept_id' + q).val("");
    clone.find('.proyek_id').attr('name', 'proyek_q[]').attr('id', 'proyek_id' + q).val("");
    clone.find('.barang_id').attr('name', 'barang_id[]').attr('id', 'barang_id' + q).val("");
    clone.find('.terproses').attr('name', 'terproses[]').attr('id', 'terproses' + q).val("");
    clone.find('.ditutup').attr('name', 'ditutup[]').attr('id', 'ditutup' + q).val("");
    clone.find('.harga_modal').attr('name', 'harga_modal[]').attr('id', 'harga_modal' + q).val("");
    $(clone).appendTo($('.duplicate-pesanan-sections').closest('.pesanan-sections').find('.purchasePesanan'));
    $('.select3').select2();
    // callSelect2AjaxProduk(q);
    removeDelButton();
    q++;
}

$('.duplicate-pesanan-sections').on('click', '.remove-rincian-pesanan', function () {
    if ($(this).closest('.purchasePesanan').find('tr').length > 0) {
        $(this).closest('tr').remove();
        $('.purchasePesanan tr:last').find('td:last').append('<button href="" class="remove btn btn-danger remove-rincian-pesanan" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button>');
        calculate();
        q -= 1;
    }
});

function sumQtyAndUnit(param){
    // cekQtyandPrice(param);
    var amount  = 0;
    let disc    = 0;

    amount = parseInt($('#qty_produk'+param).val()) * parseInt(changeMaskingToNumber($('#unit_harga_produk'+param).val()));
    if (isNaN($('#diskon_produk' + param).val()) || $('#diskon_produk' + param).val() == ''){
    }else{
        disc = amount * parseFloat($('#diskon_produk' + param).val()) / 100;                            
    }

    $('#amount_produk'+param).autoNumeric('init',{aPad: false}).autoNumeric('set', amount - disc);
    calculate();
}

function cekQtyandPrice(param) {
    if ($("#qty_produk"+ param).val() == "" || $("#qty_produk"+ param).val() <= 0) {
        $("#qty_produk"+ param).val(1);
        swal({
            icon: "error",
            text: "Barang Masih Kosong"
        });
    }
    if ($("#unit_harga_produk"+ param).val() == "") {
        $("#unit_harga_produk"+ param).val(0);   
    }
}

function calculate(){
    var subTotal                = 0;
    var grandtotal              = 0;
    var diskon                  = $("#totalPotonganRupiah").autoNumeric('init').autoNumeric('get');
    var diskon_untuk_pajak      = $("#totalPotonganRupiah").autoNumeric('init').autoNumeric('get');
    var ongkir                  = $('#ongkir').autoNumeric('init').autoNumeric('get');
    var tax                     = 0;
    var in_tax                  = 0;
    var amount                  = [];
    var sorting                 = [];
    $(".amount_produk").autoNumeric('init');
    $('#taxNaration').html('');

    $(".amount_produk").each(function() {
        amount.push([$(this).autoNumeric('get')]);
    });

    $(".amount_produk").each(function() {
        subTotal += parseInt($(this).autoNumeric('get'));
        // amount[$(this).index()].push($(this).val());
    });
    if ($("#diskonTotal").val() > 0) {
        diskon_untuk_pajak = subTotal * $('#diskonTotal').val() / 100;
    }

    // diskon_per_item = diskon_untuk_pajak / $(".produk_id").length;
    $.each(amount, function(index, key){
        var total_tax = 0;
        $('#tax_produk'+index+' option:selected').each(function(){
            var str = $(this).text();
            var spl = str.split("/");
            total_tax += parseFloat(spl[0].replace(/ [0-9]*/, ''));
        });                
        // var amount_after_dikurang = parseFloat(key) - diskon_per_item;
        var after_dikurang_diskon = key / subTotal * diskon_untuk_pajak;
        $('#tax_produk'+index+' option:selected').each(function(){
            var str = $(this).text();
            var spl = str.split("/");
            let temp_tax = (100 / (100 + total_tax) * (key - after_dikurang_diskon));
            var sort_temp = {
                label : spl[1].replace(/ [0-9]*/, ''),
                nilai : spl[0].replace(/ [0-9]*/, ''),
                total : (key - after_dikurang_diskon) * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100,
                total_in_tax : temp_tax * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100
            }
            sorting.push(sort_temp);                  
        });
    });

    var sorted = sorting.sort(function(a, b) {
        return a.nilai - b.nilai;
    })

    var sorted_temp = sorted; 
    var unduplicateItem = [];
    for (var i = 0; i < sorted_temp.length; i++) {            
        if(sorted_temp.length >= 2){
            if(sorted_temp[i + 1] != null){
                if(sorted_temp[i]['nilai'] != sorted_temp[i + 1]['nilai'] && sorted_temp[i]['label'] != sorted_temp[i + 1]['label']){
                    unduplicateItem.push(sorted_temp[i]);
                }else{
                    sorted_temp[i + 1]['total'] = sorted_temp[i]['total'] + sorted_temp[i + 1]['total']; 
                    sorted_temp[i + 1]['total_in_tax'] = sorted_temp[i]['total_in_tax'] + sorted_temp[i + 1]['total_in_tax']; 
                }
            }else{
                unduplicateItem.push(sorted_temp[i]);                                        
            }
        }else{
            unduplicateItem.push(sorted_temp[i]);                    
        }
    }

    $.each(unduplicateItem, function(index, key){
        var tot_tax = 0;
        var tax_label = ''; 
        if($('input.in_tax').is(':checked')){
            tax += key['total_in_tax'];
            $("#taxNaration").append(key['label']+" "+key['nilai']+"% = "+key['total_in_tax'].toFixed(2)+"<br>");
            tot_tax     = key['total_in_tax'];
            tax_label   = key['label']; 
        }else{
            tax += key['total'];
            $("#taxNaration").append(key['label']+" "+key['nilai']+"% = "+key['total'].toFixed(2)+"<br>");
            tot_tax = key['total'];
            tax_label   = key['label']; 
        }
        $("#tax_cetak"+index).val(tot_tax.toFixed(2));
        $("#tax_cetak_label"+index).val(tax_label);            
        $("#nilai_pajak_id").val(tax);
    });

    if($('input.taxable').is(':checked')){
        $('.tax_produk').attr('disabled',false);
        $("#nilai_pajak_id").attr('disabled',false);  
        $(".tax_produk").select2({
            maximumSelectionLength: 2
        }); 
        tax = tax;
        $('#taxNaration').show();
        if($('input.in_tax').is(':checked')){
            tax = 0;
            //balik lagi 0 karena harga sudah sama ongkir
        }
    }else{
        $('.tax_produk').attr('disabled',true);
        $("#nilai_pajak_id").attr('disabled',true);   
        $('#taxNaration').hide();
        tax = 0;
    }

    if ($("#diskonTotal").val() > 0) {
        diskon = subTotal * $('#diskonTotal').val() / 100;
    }

    grandtotal = subTotal - diskon + tax + parseInt(ongkir);

    $('#totalPotonganRupiah').autoNumeric('init', {
        aPad: false
    });

    $('#subtotal').html("<strong>" + Number(subTotal).toLocaleString()+"</strong>");
    $('.sub-total-cetak').val(subTotal);
    $('.grandtotal').html("<strong>" + Number(grandtotal).toLocaleString()+"</strong>");
    $('#grand_total').val(grandtotal);
    $('#totalPotonganRupiah').autoNumeric('set', diskon);

}

function keyPressModalKodeBarang(q, x, kategori = null){
    var no_barang = x;
    var data = {
        kategoriBySearch : kategori,
        no_barang : no_barang,
        _token: token_,
    }
    $('.modal_nama_barang').val('');
    callAjaxKeypress(q, no_barang, data)
}

function keyPressModalNamaBarang(q, x, kategori){
    var keterangan = x;
    var data = {
        kategoriBySearch : kategori,
        keterangan : keterangan,
        _token: token_,
    }
    $('.modal_kode_barang').val('');
    callAjaxKeypress(q, keterangan, data)
}

function awas(q = null){
    var type = 'modal';
    $('#modalBarang').modal({
        backdrop: 'static',
        keyboard: false, 
        show: true
    });
    var row = $('#modal_thead_barang').html();
    var clone = $(row).clone();
    clone.find('.modal_kode_barang').attr('name', 'modal_kode_barang[]').attr('id', 'modal_kode_barang' + q).attr('class', 'form-control modal_kode_barang');
    clone.find('.modal_nama_barang').attr('name', 'modal_nama_barang[]').attr('id', 'modal_nama_barang' + q).attr('class', 'form-control modal_nama_barang');;
    $(clone).appendTo($('.duplicate-modal-sections').closest('.modal-sections').find('.modal_thead'));

    callSelect2AjaxProduk(q, type);
    callAjaxListGudang();

    $('body').keydown(function(e) {
        var keyCode = e.keyCode || e.which;
        if ( keyCode == 27 ) {
            barangBatal();
            $('#modalBarang').modal('toggle');
        }
    });
}

function barangBatal() {
    $('.modal_thead_barang_tr').remove();
    $('.table_tr_modal').remove();
}

function clearQtyGudang(){
    $('#modal_gudang_temp').val(' ');
    $('label[id*="qty_gudang_footer"]').text('');
    $('#modal_gudang').val(0).trigger('change');
}

$('#kategori_barang').change(function() {
    var searchBy = $('#searchByKategori');
    clearQtyGudang();
    if ($('#kategori_barang').val() == 1) {
        searchBy.prop('disabled', false);
        ajaxModalKategori(searchBy);

        setTimeout(function() {
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        }, 600);

        $('#searchByKategori').change(function() {
            clearQtyGudang();
            var kategori = $('#searchByKategori').val();;
            var kode_barang = $('.modal_kode_barang').val();
            var nama_barang = $('.modal_nama_barang').val();            
            if (kode_barang !== '' && nama_barang === '') {
                keyPressModalKodeBarang(null, kode_barang, kategori);
            }else if (kode_barang === '' && nama_barang !== '') {
                keyPressModalNamaBarang(null, nama_barang, kategori);
            }
        });
    }else if($('#kategori_barang').val() == 0){
        searchBy.html(null);
        searchBy.prop('disabled', true);
        
        var kode_barang = $('.modal_kode_barang').val();
        var nama_barang = $('.modal_nama_barang').val();            
        if (kode_barang !== '' && nama_barang === '') {
            keyPressModalKodeBarang(null, kode_barang, null);
        }else if (kode_barang === '' && nama_barang !== '') {
            keyPressModalNamaBarang(null, nama_barang, null);
        }
    }
});

function ajaxModalKategori(searchBy){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-all-kategori-produk",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                searchBy.append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxKeypress(n = null, searchKey, dataKey){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-nama-produk",
        data: dataKey,
        dataType: 'JSON',
        success: function (data) {
            $('.table_tr_modal').remove();
            if (data == 'null') {
                $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="table_tr_modal">'
                        + '<td colspan="3" style="padding:5px; 20px" align="center">'
                        +   '<span class="alert alert-warning"> Tidak ada Barang dengan kata Kunci '+ searchKey +' </span>'
                        + '</td>' +
                    '</tr>').appendTo('.modal_tbody');
            }else{
                $.each(data, function (key, value) {
                    var idP = value.id;

                    $(  '<tr style="display:table; width:100%; table-layout:fixed;" class="modal_barang_'+ key +' table_tr_modal">'
                            + '<td width="26%" style="padding:5px 15px">'
                                + value.no_barang
                            + '</td>'
                            + '<td width="3%"></td>'
                            + '<td width="55%" style="padding:5px 15px">'
                                + value.keterangan
                            + '</td>'+
                        '</tr>').appendTo('.modal_tbody');

                    // hey
                    $('.modal_barang_'+ key).hover(
                        function(){
                            $(this).attr('style', 'border: 2px solid limegreen; border-radius: 5px ;display:table; width:100%; table-layout:fixed;');
                        }, function() {
                            $(this).attr('style', 'border: none; display:table; width:100%; table-layout:fixed;');
                        }
                    );

                    $('.table_tr_modal').click(function () {
                        var ini = $(this);
                        if(ini.is(':hover') === true){
                            $('.table_tr_modal').removeClass('warnatr');
                            ini.addClass('warnatr');
                        }
                    });
                    // yeh

                    $('.modal_barang_'+ key).click(function() {
                        var ini = $(this);
                        var gudang = $('#modal_gudang').val();
                        $('#modal_gudang_temp').val(idP);
                        
                        callAjaxQtyGudang(idP, gudang);
                    });

                    $('.modal_barang_'+ key).dblclick(function(){
                        setTimeout(function() {
                            clearQtyGudang();
                        }, 0);
                        $('#produk_id'+n).val(idP);
                        $('#no_produk'+n).val(value.no_barang);
                        test(idP, n);
                        barangBatal();
                        $('#modalBarang').modal('toggle');
                    });
                    
                    $('#modal_gudang').change(function() {
                        var gudang = $('#modal_gudang').val();
                        var produk = $('#modal_gudang_temp').val();
                        clearTimeout(typingTimer);
                        typingTimer = setTimeout( function () {
                            callAjaxQtyGudang(produk, gudang);
                        }, doneTypingInterval);
                    });
                });
            }
        }
    });
}

function callAjaxQtyGudang(produk, gudang){
    $.ajax({
        type : 'GET',
        url  : "" + base_url + "/ajax/get-qty-gudang-stock",
        data : {
            gudang : gudang,
            produk : produk,
            _token : token_
        },
        dataType : 'json',
        success: function (resuponse){
            $('#qty_gudang_footer').html(resuponse);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function callAjaxListGudang(){
    $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-list-gudang",
        data: { _token: token_ },
        dataType: "json",
        success: function(response){
            $.each(response, function (key, item){
                $('#modal_gudang').append( 
                    $("<option></option>")
                    .text(item)
                    .val(key)
                );
            });
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function test(idP, vale) { // js dari blade create dan edit
    var produk_id   = idP;
    var pelanggan_id   = $('#pelanggan_id').val();
        $.ajax({
        type: "GET",
        url: "" + base_url + "/ajax/get-produk",
        data: {
            id : produk_id,
                pelanggan_id : pelanggan_id,
            _token: token_
        },
        dataType: "json",
        success: function(response){
            $('#tax_produk'+vale).find('option').remove().end();
            $('#penawaran_id'+vale).val("");
            $('#keterangan_produk'+vale).val(response.keterangan);
            $('#qty_produk'+vale).val(response.jumlah);
            $('#satuan_produk'+vale).val(response.satuan);
            $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
            $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice); //karena defaultnya di * 1, maka cukup unitprice saja
            $('#diskon_produk'+vale).val('0');
            $('#terproses'+vale).val(response.terproses);
            if (response.harga_modal !== null) {
                $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
            }else {
                $('#harga_modal'+vale).val('0');
            }     
            $.each(response.tax, function (index, item){
                $('#tax_produk'+vale).append( 
                $("<option></option>")
                .text(item.nilai+ "/" +item.nama)
                .val(item.id)
                );
            });
            sumQtyAndUnit(vale);
        }, failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function getListPriceHistory(q) {
    var pelanggan       = $('#pelanggan_id').val();
    var produk          = $('#produk_id' + q).val();
    var harga           = $('#unit_harga_produk' + q);

    if (pelanggan == '' || pelanggan == null) {
        swal({
            icon: "error",
            text: "Pelanggan Tidak Boleh Kosong!"
        });
    }else if ( produk == '' || produk == null ) {
        swal({  
            icon: "error",
            text: "Produk Tidak Boleh Kosong!"
        });
    } else {
        $('#modalPriceHistory').modal({
            backdrop: 'static',
            keyboard: false, 
            show: true
        });
        $('.price_history_name').html( $('#keterangan_produk' + q).val() );
        $('.pelanggan_price_history').html( $('#pelanggan_id')[0].selectedOptions[0].text );
        $.ajax({
            type: "POST",
            url: "" + base_url + "/ajax/get-list-history-price-item",
            data:   { 
                        sov : pelanggan,
                        type : 'penjualan',
                        item : produk,
                        _token: token_
                    },
            dataType: "json",
            success: function(response){
                $.map(response, function(item, index) {
                    $(".modal_tbody_price_history").append("<tr class='post_price table_tr_modal' id='row_price_history_"+ index +"'> "+
                                                "<td>"+item['tanggal']+"</td>"+
                                                "<td>"+item['kuantitas']+"</td>"+
                                                "<td>"+item['unit']+"</td>"+
                                                "<td class='harga_history'>"+item['harga']+"</td>"+
                                                "<td>"+item['diskon']+"</td>"+
                                            "</tr>");
                    $('.harga_history').autoNumeric('init', {vMin: '0', vMax: '999999999' })
                    harga.autoNumeric('init', {vMin: '0', vMax: '999999999' })
                    $('#row_price_history_'+ index).dblclick(function(){
                        harga.autoNumeric('init').autoNumeric('set', item['harga']);
                        $('.table_tr_modal').remove();
                        $('.price_history_name').html();
                        $('#modalPriceHistory').modal('toggle');
                        sumQtyAndUnit(q);
                    });
                });
            }, failure: function(errMsg) {
                alert(errMsg);
            }
        });
    }
}