var q = $('.akun_id').length;

$('.jumlah_pengeluaran').autoNumeric('init',{aPad: false});

function duplicatePengeluaran(){
    var ini = $('#tgl_pakai').val();
    var row = $('#table_pengeluaran_section').html();
    var clone = $(row).clone();
    clone.find('.akun_id').attr('name','akun_pengeluaran_aktiva_id[]').attr('id','akun_id'+q).attr('class','select2 form-control').attr('onchange','changeTab('+ q +')');
    clone.find('.tanggal_pengeluaran').attr('name','tanggal_pengeluaran[]').attr('id','tanggal_pengeluaran'+q).attr('class','tanggal_pengeluaranX form-control');
    clone.find('.keterangan_pengeluaran').attr('name','keterangan_pengeluaran[]').attr('id','keterangan_pengeluaran'+q);
    clone.find('.jumlah_pengeluaran').attr('name','jumlah_pengeluaran[]').attr( 'id','jumlah_pengeluaran'+q).attr('onblur','checkJumlahPengeluaran('+ q +')').autoNumeric('init',{aPad: false}).autoNumeric('set', 0);
    $(clone).appendTo($('.duplicate-pengeluaran-sections').closest('.pengeluaran-sections').find('.tabel_pengeluaran'));
    $('#tanggal_pengeluaran'+ q).attr('readonly', 'readonly');
    // $('#tanggal_pengeluaran'+ q).datepicker({format: 'dd MM yyyy', autoclose: true});
    // $('#tanggal_pengeluaran'+ q).datepicker('setDate', new Date());
    $('#jumlah_pengeluaran' +q).on('keydown', function(e) {
        let keyAngka = [8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
        if (keyAngka.indexOf(e.which) !== -1){
            return true;
        }else{
            return false;
        }
    });
    $('#tanggal_pengeluaran'+ q).val(ini);
    q++;
    $('.select2').select2();
}
$(document).ready(function () {
    $('.duplicate-pengeluaran-sections').on('click', '.add-pengeluaran', function () {
        duplicatePengeluaran();
    });
});
$('.duplicate-pengeluaran-sections').on('click', '.remove-rincian-pengeluaran', function () {
    if ($(this).closest('.tabel_pengeluaran').find('tr').length > 0) {
        $(this).closest('tr').remove();
        sumJumlahPengeluaran();
    }
});

function calculateRasio() {
    var metode  = $('#metode_penyusutan_id').val();
    var umur = parseFloat($('#tahun').val()) + parseFloat($('#bulan').val() / 12);
    var hasil = 0;
    if (metode == '1') {
        hasil = 100/umur;
        $("#div-tidak-berwujud").show();
        $("#div-tetap-fisikal").show();
        $("#div-akun-penyusutan").show();
        $("#div-akun-beban-penyusutan").show();
        $("#akun_akum_penyusutan_id").val('').change();
    }else if(metode == '2' ){
        hasil = 200/umur;
        $("#div-tidak-berwujud").show();
        $("#div-tetap-fisikal").show();
        $("#div-akun-penyusutan").show();
        $("#div-akun-beban-penyusutan").show();
        $("#akun_akum_penyusutan_id").val('').change();
    }else if(metode == '4'){
        $("#div-tidak-berwujud").hide();
        $("#div-tetap-fisikal").hide();
        $("input[name='aktiva_tidak_berwujud']").prop('checked',false);
        $("input[name='aktiva_tetap_fisikal']").prop('checked',false);
        $("#div-akun-penyusutan").show();
        $("#div-akun-beban-penyusutan").show();
        $("#akun_akum_penyusutan_id").val('').change();
    }else if(metode == '3'){
        hasil = 0;
        $("#div-tidak-berwujud").show();
        $("#div-tetap-fisikal").show();
        $("#div-akun-penyusutan").hide();
        $("#div-akun-beban-penyusutan").hide();
        $("#akun_akum_penyusutan_id").val('').change();
    }
    $('#rasio').val(hasil.toFixed(2));
}

function checkboxChanged() {
    if ($("#aktiva_tidak_berwujud").is(":checked")) {
        $("#div-akun-penyusutan").hide();
        $("#akun_akum_penyusutan_id").val('').change();
    }else {
        $("#div-akun-penyusutan").show();
        $("#akun_akum_penyusutan_id").val('').change();
    }
}

function changeTab(id) {
    var text_swal = '';
    if ($("#tipe_aktiva_tetap_id").val() === "") {
        text_swal = 'Tipe Aktiva Belum Dimasukkan';
    }else if ($("#keterangan").val() === "") {
        text_swal = 'Keterangan Aktiva Belum Dimasukkan';    
    }else if ($("#kuantitas").val() <= 0) {
        text_swal = 'Kuantitas tidak boleh kurang atau sama dengan 0';
        $("#kuantitas").val(1);
    }else if ($("#metode_penyusutan_id").val() === "" || $("#akun_aktiva_id").val() === "") {
        text_swal = 'Metode Penyusutan / Akun Aktiva Belum Dimasukkan';
    }else if ($("#tahun").val() <= 0 && $("#metode_penyusutan_id").val() == 1) {
        text_swal = 'Umur estimasi harus lebih besar atau sama dengan 1 tahun';
    }else if ($("#tahun").val() <= 0 && $("#metode_penyusutan_id").val() == 4) {
        text_swal = 'Umur estimasi harus lebih besar atau sama dengan 1 tahun';
    }else if ($("#tahun").val() <= 2 && $("#metode_penyusutan_id").val() == 2) {
        text_swal = 'Umur estimasi harus lebih besar dari 2 tahun';
    }


    if (text_swal) {
        if ($('#akun_id'+ id).closest('.tabel_pengeluaran').find('tr').length > 0) {
            $('#akun_id'+ id).closest('tr').remove();
        }
        $('.nav-pills a[href="#pills-alamat"]').tab('show');
        swal({
            icon: "error",
            text: text_swal,
        });
    }else {
        fillKeterangan(id);
    }
}

function changeQty(val) {
    if (val <= 0) {
        swal({
            icon: "error",
            text: 'Kuantitas tidak boleh kurang atau sama dengan 0',
        });
        $('#kuantitas').val(1);
    }
}

$(document).on('blur','.jumlah_pengeluaran',function () {
    // checkJumlahPengeluaran();
});

function checkJumlahPengeluaran(index) {
    if($('#jumlah_pengeluaran'+ index).autoNumeric('get') == '' || $('#jumlah_pengeluaran'+ index).autoNumeric('get') === '0'){
        var el = $('#jumlah_pengeluaran'+ index);
        swal({
            icon: "error",
            text: 'Jumlah tidak boleh kosong',
        }).then(()=> {
            el.focus();
        });
    }
}


$('.tabel_pengeluaran').on('change','.jumlah_pengeluaran',function () {
    sumJumlahPengeluaran();    
});

$(document).ready(function() {
    calculateRasio();
    sumJumlahPengeluaran();    
});

function sumJumlahPengeluaran() {
    var sum = 0;

    $(".jumlah_pengeluaran").each(function() {
        sum += parseFloat($(this).autoNumeric('get'));
    });
    $("#span-harga-perolehan").text(Number(sum).toLocaleString());
    $(".harga-perolehan").val(sum);
    $("#span-nilai-buku").text(Number(sum).toLocaleString());
    $(".nilai-buku").val(sum);
}

function fillKeterangan(id) {
    var no_akun = $("#akun_id"+ id).select2('data')[0];

    $("#keterangan_pengeluaran"+ id).val(no_akun.text);
}

$('#tgl_beli').on('change', function(e){
    e.preventDefault();
    var ini = $('#tgl_beli').val();
    $('#tgl_pakai').val(ini);
    $('.tanggal_pengeluaranX').val(ini);
})

$('#tgl_pakai').on('change', function(e){
    e.preventDefault();
    var ini = $('#tgl_pakai').val();
    $('.tanggal_pengeluaranX').val(ini);
})

$('#kuantitas').on('keydown', function(e) {
    let keyAngka = [8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
    if (keyAngka.indexOf(e.which) !== -1){
        return true;
    }else{
        return false;
    }
});

$('#sisa').on('keydown', function(e) {
    let keyAngka = [8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
    if (keyAngka.indexOf(e.which) !== -1){
        return true;
    }else{
        return false;
    }
});

$('#bulan').on('keydown', function(e) {
    let keyAngka = [8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
    if (keyAngka.indexOf(e.which) !== -1){
        return true;
    }else{
        return false;
    }
});

$('#tahun').on('keydown', function(e) {
    let keyAngka = [8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
    if (keyAngka.indexOf(e.which) !== -1){
        return true;
    }else{
        return false;
    }
});