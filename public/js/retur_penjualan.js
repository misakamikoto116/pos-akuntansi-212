var base_url = window.location.origin + '/akuntansi';
var token_ = $("meta[name='csrf-token']").attr("content")

$('.tanggal1').datepicker({ format: 'dd MM yyyy', autoclose: true });
$('.tanggal1').datepicker('setDate', new Date());

$(".list-sales-invoice").change(function () {
    listInvoice($(this).val());  
});

function listInvoice(val) {
  filterSelectedId(val, 'faktur');
};

function duplicateForm() {
  var row = $('#table_faktur_section').html();
  var clone = $(row).clone();
  clone.find('.barang_faktur_penjualan_id').attr('name','barang_faktur_penjualan_id[]').attr('id', 'barang_faktur_penjualan_id' + q);
  clone.find('.no_produk').attr('name', 'no_produk[]').attr('id', 'no_produk' + q).attr('class', 'form-control no_produk');
  clone.find('.produk_id').attr('name', 'produk_id[]').attr('id', 'produk_id' + q).attr('class', 'form-control produk_id');
  clone.find('.keterangan_produk').attr('name', 'keterangan_produk[]').attr('id', 'keterangan_produk' + q);
  clone.find('.qty_produk').attr('name', 'qty_produk[]').attr('id', 'qty_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')');
  clone.find('.satuan_produk').attr('name', 'satuan_produk[]').attr('id', 'satuan_produk' + q);
  clone.find('.unit_harga_produk').attr('name', 'unit_harga_produk[]').attr('id', 'unit_harga_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')').attr('ondblclick', 'getListPriceHistory('+ q +')');
  clone.find('.diskon_produk').attr('name', 'diskon_produk[]').attr('id', 'diskon_produk' + q).attr('onchange', 'sumQtyAndUnit(' + q + ')');
  clone.find('.tax_produk').attr('name', 'tax_produk[' + q + '][]').attr('id', 'tax_produk' + q).attr('onchange', 'calculate()');
  clone.find('.amount_produk').attr('name', 'amount_produk[]').attr('id', 'amount_produk' + q).attr('readonly', '');
  clone.find('.dept_id').attr('name', 'dept_q[]').attr('id', 'dept_id' + q);
  clone.find('.proyek_id').attr('name', 'proyek_q[]').attr('id', 'proyek_id' + q);
  clone.find('.gudang_id').attr('name', 'gudang_id[]').attr('id', 'gudang_id' + q);
  clone.find('.sn').attr('name', 'sn[]').attr('id', 'sn' + q);
  clone.find('.harga_modal').attr('name', 'harga_modal[]').attr('id', 'harga_modal' + q);
  clone.find('.harga_terakhir').attr('name', 'harga_terakhir[]').attr('id', 'harga_terakhir' + q);
  clone.find('.harga_dgn_pajak').attr('name', 'harga_dgn_pajak[]').attr('id', 'harga_dgn_pajak' + q);
  $(clone).appendTo($('.duplicate-faktur-sections').closest('.faktur-sections').find('.purchaseFaktur'));
  $('.select2').select2();
  q++;
}


function sumQtyAndUnit(param) {
  cekQtyandPrice(param);
  var amount = 0;
  let disc = 0;

  amount = parseFloat($('#qty_produk' + param).val()) * parseInt(changeMaskingToNumber($('#unit_harga_produk' + param).val()));

  disc = amount * parseFloat($('#diskon_produk' + param).val()) / 100;

  $('#amount_produk' + param).autoNumeric('init',{aPad: false}).autoNumeric('set', amount - disc);
  calculate();
  cekGudangStock(param);
}

function calculate() {
  var subTotal            = 0;
  var grandtotal          = 0;
  var diskon              = changeMaskingToNumber($("#totalPotonganRupiah").val());
  var diskon_untuk_pajak  = changeMaskingToNumber($("#totalPotonganRupiah").val());
  var tax                 = 0;
  var in_tax              = 0;
  var amount              = [];
  var sorting             = [];
  $(".amount_produk").autoNumeric('init',{aPad: false});
  $('#taxNaration').html('');

  $(".amount_produk").each(function () {
    amount.push([changeMaskingToNumber($(this).val())]);
  });

  $(".amount_produk").each(function () {
    subTotal += parseInt(changeMaskingToNumber($(this).val()));
    // amount[$(this).index()].push($(this).val());
  });

  if ($("#diskonTotal").val() > 0) {
      diskon_untuk_pajak = subTotal * $('#diskonTotal').val() / 100;
  }
  
  // diskon_per_item = diskon_untuk_pajak / $(".produk_id").length;
  $.each(amount, function (index, key) {
    var total_tax = 0;
    //default
    $('#harga_dgn_pajak' + index).val(parseFloat(key));
    
    $('#tax_produk' + index + ' option:selected').each(function () {
      var str = $(this).text();
      var spl = str.split("/");
      total_tax += parseFloat(spl[0].replace(/ [0-9]*/, ''));
    });

    let temp_calc = 0;
  
    // var amount_after_dikurang = parseFloat(key) - diskon_per_item;
    var after_dikurang_diskon = key / subTotal * diskon_untuk_pajak;
    $('#tax_produk' + index + ' option:selected').each(function (i) {
      var str = $(this).text();
      var spl = str.split("/");
      let temp_tax = (100 / (100 + total_tax) * (key - after_dikurang_diskon));
      var sort_temp = {
        label: spl[1].replace(/ [0-9]*/, ''),
        nilai: spl[0].replace(/ [0-9]*/, ''),
        total: (key - after_dikurang_diskon) * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100,
        total_in_tax: temp_tax * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100
      }
        sorting.push(sort_temp);
        temp_calc += parseFloat(key) * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100;
        $('#harga_dgn_pajak' + index).val(parseFloat(key) + temp_calc);
    });
  });

  var sorted = sorting.sort(function (a, b) {
    return a.nilai - b.nilai;
  })

  var sorted_temp = sorted;
  var unduplicateItem = [];
  for (var i = 0; i < sorted_temp.length; i++) {
    if (sorted_temp.length >= 2) {
      if (sorted_temp[i + 1] != null) {
        if (sorted_temp[i]['nilai'] != sorted_temp[i + 1]['nilai'] && sorted_temp[i]['label'] != sorted_temp[i + 1]['label']) {
          unduplicateItem.push(sorted_temp[i]);
        } else {
          sorted_temp[i + 1]['total'] = sorted_temp[i]['total'] + sorted_temp[i + 1]['total'];
          sorted_temp[i + 1]['total_in_tax'] = sorted_temp[i]['total_in_tax'] + sorted_temp[i + 1]['total_in_tax'];
        }
      } else {
        unduplicateItem.push(sorted_temp[i]);
      }
    } else {
      unduplicateItem.push(sorted_temp[i]);
    }
  }

  
  $.each(unduplicateItem, function (index, key) {
    var tot_tax = 0;
    var tax_label = '';
    if ($('input.in_tax').is(':checked')) {
      tax += key['total_in_tax'];
      $("#taxNaration").append(key['label'] + " " + key['nilai'] + "% = " + key['total_in_tax'].toFixed(2) + "<br>");
      tot_tax     = key['total_in_tax'];
      tax_label   = key['label'];
    } else {
      tax += key['total'];
      $("#taxNaration").append(key['label'] + " " + key['nilai'] + "% = " + key['total'].toFixed(2) + "<br>");
      $("#tax_cetak"+index).val(key['total'].toFixed(2));
      tot_tax = key['total'];
      tax_label   = key['label'];
    }
    $("#tax_cetak"+index).val(tot_tax.toFixed(2));
    $("#tax_cetak_label"+index).val(tax_label);
  });

  if ($('#group1').is(':checked')) {
    $('.tax_produk').attr('disabled', false);
    $(".tax_produk").select2({
      maximumSelectionLength: 2
    });
    tax = tax;
    $('#taxNaration').show();
    if ($('#group2').is(':checked')) {
      tax = 0;
      //balik lagi 0 karena harga sudah sama ongkir
    }
  } else {
    $('.tax_produk').attr('disabled', true);
    $('#taxNaration').hide();
    tax = 0;
  }

  if ($("#diskonTotal").val() > 0) {
        diskon = subTotal * $('#diskonTotal').val() / 100;
  }

  grandtotal = subTotal - diskon + tax;
  var grandtotal_murni = subTotal - diskon;

  $('#subtotal').html("<strong>" + Number(subTotal).toLocaleString() + "</strong>");
  $('.grandtotal').html("<strong>" + Number(grandtotal).toLocaleString() + "</strong>");
  $('#totalPotonganRupiah').autoNumeric('init',{aPad: false},{aPad: false}).autoNumeric('set', diskon);
  $('.kolom_grandtotal').val(grandtotal_murni);

}

$('.duplicate-faktur-sections').on('click', '.remove-rincian-penawaran', function () {
    if ($(this).closest('.purchaseFaktur').find('tr').length > 0) {
        $(this).closest('tr').remove();
        calculate();
        q -= 1;
    }
});

function cekGudangStock(param) {
    var goSubmit = 0;
    var gudang_stock_id = $('#gudang_id'+ param).val();
    var produk_stock_id = $('#produk_id'+ param).val();
    var qty_stock_id    = $('#qty_produk'+ param).val();
    $.ajax({
        type: "GET",
        url: ""+base_url+"/ajax/get-gudang-stock",
        data: {
            gudang_id : gudang_stock_id, produk_id : produk_stock_id, qty_id: qty_stock_id , _token: token_
        },
        dataType: "json",
        success: function (response) {
            if (response.status === 0) {
                swal({
                    icon: 'error',
                    text: 'Stock '+ $("#keterangan_produk"+ param).val() +' di '+ response.gudang +' tersisa '+ response.kuantitas +' harap masukkan jumlah yang sesuai !!!'  
                });
            }
        }, failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

function cekQtyandPrice(param) {
    if ($("#qty_produk"+ param).val() == "" || $("#qty_produk"+ param).val() <= 0) {
        $("#qty_produk"+ param).val(1);
        swal({
            icon: "error",
            text: "Barang Masih Kosong"
        });
    }
    if ($("#unit_harga_produk"+ param).val() == "") {
        $("#unit_harga_produk"+ param).val(0);   
    }
}

function getListPriceHistory(q) {
  var pelanggan       = $('#pelanggan_id').val();
  var produk          = $('#produk_id' + q).val();
  var harga           = $('#unit_harga_produk' + q);

  if (pelanggan == '' || pelanggan == null) {
      swal({
          icon: "error",
          text: "Pelanggan Tidak Boleh Kosong!"
      });
  }else if ( produk == '' || produk == null ) {
      swal({  
          icon: "error",
          text: "Produk Tidak Boleh Kosong!"
      });
  } else {
      $('#modalPriceHistory').modal({
          backdrop: 'static',
          keyboard: false, 
          show: true
      });
      $('.price_history_name').html( $('#keterangan_produk' + q).val() );
      $('.pelanggan_price_history').html( $('#pelanggan_id')[0].selectedOptions[0].text );
      $.ajax({
          type: "POST",
          url: "" + base_url + "/ajax/get-list-history-price-item",
          data:   { 
                      sov : pelanggan,
                      type : 'penjualan',
                      item : produk,
                      _token: token_
                  },
          dataType: "json",
          success: function(response){
              $.map(response, function(item, index) {
                  $(".modal_tbody_price_history").append("<tr class='post_price table_tr_modal' id='row_price_history_"+ index +"'> "+
                                              "<td>"+item['tanggal']+"</td>"+
                                              "<td>"+item['kuantitas']+"</td>"+
                                              "<td>"+item['unit']+"</td>"+
                                              "<td class='harga_history'>"+item['harga']+"</td>"+
                                              "<td>"+item['diskon']+"</td>"+
                                          "</tr>");
                  $('.harga_history').autoNumeric('init', {vMin: '0', vMax: '999999999' })
                  harga.autoNumeric('init', {vMin: '0', vMax: '999999999' })
                  $('#row_price_history_'+ index).dblclick(function(){
                      harga.autoNumeric('init').autoNumeric('set', item['harga']);
                      $('.table_tr_modal').remove();
                      $('.price_history_name').html();
                      $('#modalPriceHistory').modal('toggle');
                      sumQtyAndUnit(q);
                  });
              });
          }, failure: function(errMsg) {
              alert(errMsg);
          }
      });
  }
}

function barangBatal() {
  $('.table_tr_modal').remove();
}