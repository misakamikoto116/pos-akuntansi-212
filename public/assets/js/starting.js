          $('.duplicate-sections').on('click', '.simpan-saldo-pelanggan', function () {
            var nextToPelanggan;
            var totalPelanggan = 0;
            var itemPelanggan = $(this).closest('.modal').find('.purchaseItemsPelanggan tr');
            var sectionsPelanggan = $(itemPelanggan).find('.pelanggan_required');
            for(var i = 0; i < sectionsPelanggan.length; i++) {
                if (!sectionsPelanggan[i].validity.valid) {
                    nextToPelanggan = false;
                }
            }

            if (nextToPelanggan == false) {
                swal({
                    icon: "error",
                    text: "Tolong Lengkapi Inputan Anda"
                });
            } else {
                $(itemPelanggan).each(function(index, el) {
                  var saldoPelanggan = $(el).find('.saldo_pelanggan').autoNumeric('get');
                  totalPelanggan += parseFloat(saldoPelanggan);
                });
                  var hasil_pelanggan = $(this).closest('.form-section').find('.saldo_awal_pelanggan'); 
                  $(hasil_pelanggan).autoNumeric('init', {aForm:false});
                  $(hasil_pelanggan).autoNumeric('set', totalPelanggan);
                  
                  $(this).closest('.modal').modal('hide');
            }
          });
            $('.tambah-sections').on('click', '.simpan-saldo-pemasok', function () {
            var nextToPemasok;
            var totalPemasok = 0;
            var itemPemasok = $(this).closest('.modal').find('.purchaseItemsPemasok tr');
            var sectionsPemasok = $(itemPemasok).find('.pemasok_required');
            for(var i = 0; i < sectionsPemasok.length; i++) {
                if (!sectionsPemasok[i].validity.valid) {
                    nextToPemasok = false;
                }
            }

            if (nextToPemasok == false) {
                swal({
                    icon: "error",
                    text: "Tolong Lengkapi Inputan Anda"
                });
            } else {
                $(itemPemasok).each(function(index, el) {
                  var saldoPemasok = $(el).find('.saldo_pemasok').autoNumeric('get');
                  totalPemasok += parseFloat(saldoPemasok);
                });
                  var hasil_pemasok = $(this).closest('.formz-section').find('.saldo_awal_pemasok'); 
                  $(hasil_pemasok).autoNumeric('init', {aForm:false});
                  $(hasil_pemasok).autoNumeric('set', totalPemasok);
                  
                  $(this).closest('.modal').modal('hide');
            }
          });
            $(document).ready(function () {
                $('.duplicate-sections').on('click', '.add', function () {
                    var row = $('#table_section_pelanggan').html();
                    var clone = $(row).clone();
                    var id = $(this).closest('.form-section').data('id');

                    // clear the values
                    clone.find('input[type=text]').val('');

                    clone.find('.no_faktur_pelanggan').attr('name','no_faktur_pelanggan['+id+'][]');
                    clone.find('.tanggal_pelanggan').attr('name','tanggal_pelanggan['+id+'][]');
                    clone.find('.saldo_pelanggan').attr('name','saldo_pelanggan['+id+'][]');

                    $(clone).appendTo($(this).closest('.form-section').find('.purchaseItemsPelanggan'));

                    $('.date-saldo').datepicker({format: 'dd M yyyy', autoclose: true});
                    $('.date-saldo').datepicker('setDate', new Date());
                    $('.mask').autoNumeric('init', {aForm:false});

                });
                $('.duplicate-sections').on('click', '.removeRow', function () {
                    if ($(this).closest('.purchaseItemsPelanggan').find('tr').length > 0) {
                        $(this).closest('tr').remove();
                    }
                });
            });
            $(document).ready(function () {
                $('.tambah-sections').on('click', '.addPemasok', function () {
                    var row = $('#table_section_pemasok').html();
                    var clone = $(row).clone();
                    var id = $(this).closest('.formz-section').data('id');
                    // clear the values
                    clone.find('input[type=text]').val('');

                    clone.find('.no_faktur_pemasok').attr('name','no_faktur_pemasok['+id+'][]');
                    clone.find('.tanggal_pemasok').attr('name','tanggal_pemasok['+id+'][]');
                    clone.find('.saldo_pemasok').attr('name','saldo_pemasok['+id+'][]');

                    $(clone).appendTo($(this).closest('.formz-section').find('.purchaseItemsPemasok'));

                    $('.date-saldo').datepicker({format: 'dd M yyyy', autoclose: true});
                    $('.date-saldo').datepicker('setDate', new Date());
                    $('.mask').autoNumeric('init', {aForm:false});

                });
                $('.tambah-sections').on('click', '.removeRowPemasok', function () {
                    if ($(this).closest('.purchaseItemsPemasok').find('tr').length > 0) {
                        $(this).closest('tr').remove();
                    }
                });
            });

                $('.admore-fields').fieldsaddmore({
                    templateEle: "#fieldsaddmore-template",
                    rowEle: ".fieldsaddmore-row",
                    addbtn: ".fieldsaddmore-addbtn",
                    removebtn: ".fieldsaddmore-removebtn",
                    min: 1,
                    callbackAfterAdd: function(ele, options) {
                        var rowId = ele['context']['lastElementChild']['classList'][1];

                        $('.' + rowId).find('.date-saldo').datepicker({format: 'dd M yyyy', autoclose: true})
                                                    .datepicker('setDate', new Date());
                    }
                });
                $('.admore-fields2').fieldsaddmore({
                    templateEle: "#fieldsaddmore-template2",
                    rowEle: ".fieldsaddmore-row2",
                    addbtn: ".fieldsaddmore-addbtn2",
                    removebtn: ".fieldsaddmore-removebtn2",
                    min: 1,
                    callbackAfterAdd: function(ele, options) {
                        var rowId = ele['context']['lastElementChild']['classList'][1];

                        $('.' + rowId).find('.date-saldo').datepicker({format: 'dd M yyyy', autoclose: true})
                                                    .datepicker('setDate', new Date());
                        $('.mask').autoNumeric('init', {aForm:false});
                    }
                });
                $('.admore-fields3').fieldsaddmore({
                    templateEle: "#fieldsaddmore-template3",
                    rowEle: ".fieldsaddmore-row3",
                    addbtn: ".fieldsaddmore-addbtn3",
                    removebtn: ".fieldsaddmore-removebtn3",
                    min: 1,
                     callbackAfterAdd: function(ele, options) {
                        var rowId = ele['context']['lastElementChild']['classList'][1];

                        $('.' + rowId).find('.date-saldo').datepicker({format: 'dd M yyyy', autoclose: true})
                                                    .datepicker('setDate', new Date());
                        $('.mask').autoNumeric('init', {aForm:false});
                    }
                });
                $('.admore-fields4').fieldsaddmore({
                    templateEle: "#fieldsaddmore-template4",
                    rowEle: ".fieldsaddmore-row4",
                    addbtn: ".fieldsaddmore-addbtn4",
                    removebtn: ".fieldsaddmore-removebtn4",
                    min: 1,
                    callbackAfterAdd: function(ele, options) {
                        var rowId = ele['context']['lastElementChild']['classList'][1];

                        $('.' + rowId).find('.date-saldo').datepicker({format: 'dd M yyyy', autoclose: true})
                                                    .datepicker('setDate', new Date());
                    }
                });

            $('#tutupbuku').datepicker({format: 'dd MM yyyy', autoclose: true});
            // $('#tutupbuku').datepicker('setDate', new Date());

            $('.datepicker1').datepicker({format: 'dd M yyyy', autoclose: true});
            $('.datepicker1').datepicker('setDate', new Date());

            $('#datepicker2').datepicker({format: 'dd M yyyy', autoclose: true});
            $('#datepicker2').datepicker('setDate', new Date());

            $('#datepicker3').datepicker({format: 'dd M yyyy', autoclose: true});
            $('#datepicker3').datepicker('setDate', new Date());

            $('#datepicker4').datepicker({format: 'dd M yyyy', autoclose: true});
            $('#datepicker4').datepicker('setDate', new Date());

            $('#datepicker5').datepicker({format: 'dd M yyyy', autoclose: true});
            $('#datepicker5').datepicker('setDate', new Date());

            $('#datepicker6').datepicker('destroy').removeAttr('id');
            $('#datepicker6').datepicker();

            $('.date-saldo').datepicker({format: 'dd M yyyy', autoclose: true});
            // $('.date-saldo').datepicker('setDate', new Date());

            //DUPLICATE FORM BOX
            //define template
            var forms = [];

            $('.duplicate-sections .form-section').each(function(){
              forms.push($(this).clone());
            })

            //define counter
            var sectionsCount = 1;



            //add new section
            $('body').on('click', '.addsection', function() {
            //   var template = forms[$(this).data('section')];

                var template = $('#form_section_pelanggan').html();


                //loop through each input
                var section = $(template).clone().end();


                //inject new section
                section.find('.nama_pelanggan_umum').attr('name', 'nama_pelanggan_umum['+sectionsCount+']');
                section.find('.saldo_awal_pelanggan').attr('name', 'saldo_awal_pelanggan['+sectionsCount+']');
                section.attr('data-id', sectionsCount);

                //increment
                sectionsCount++;
                
                // section.find('.no_faktur_pelanggan').attr('name','no_faktur_pelanggan['+sectionsCount+'][]');
                // section.find('.tanggal_pelanggan').attr('name','tanggal_pelanggan['+sectionsCount+'][]');
                // section.find('.saldo_pelanggan').attr('name','saldo_pelanggan['+sectionsCount+'][]');

                section.appendTo($(this).prev('.duplicate-sections'));
                return false;
            });

            //remove section
            $('.duplicate-sections').on('click', '.remove', function() {
              //fade out section
              $(this).closest('.form-section').fadeOut(300, function(){
                $(this).closest('.form-section').remove();
              });
              return false;
            });

            //DUPLICATE FORM BOX
            //define template
            var formsd = [];

            $('.tambah-sections .formz-section').each(function(){
              formsd.push($(this).clone());
            })

            //define counter
            var sectionCount = 1;

            //add new section
            $('body').on('click', '.addzsection', function() {
              var template_pemasok = $('#form_section_pemasok').html();


              //loop through each input
              var section_pemasok = $(template_pemasok).clone().end();

              //inject new section
              section_pemasok.find('.nama_pemasok_umum').attr('name', 'nama_pemasok_umum['+sectionCount+']');
              section_pemasok.find('.saldo_awal_pemasok').attr('name', 'saldo_awal_pemasok['+sectionCount+']');
              section_pemasok.attr('data-id', sectionCount);

              //increment
              sectionCount++;
              
              // section.find('.no_faktur_pemasok').attr('name','no_faktur_pemasok['+sectionCount+'][]');
              // section.find('.tanggal_pemasok').attr('name','tanggal_pemasok['+sectionCount+'][]');
              // section.find('.saldo_pemasok').attr('name','saldo_pemasok['+sectionCount+'][]');

              section_pemasok.appendTo($(this).prev('.tambah-sections'));

              return false;
            });

            //remove section
            $('.tambah-sections').on('click', '.removez', function() {
              //fade out section
              $(this).closest('.formz-section').fadeOut(300, function(){
                $(this).closest('.formz-section').remove();
              });
              return false;
            });
            //DUPLICATE FORM BOX
            //define template
            var formsBarang = [];
            $('.duplicate-barang .form-barang').each(function(){
              formsBarang.push($(this).clone());
            })


            //add new section
            $('body').on('click', '.addsectionBarang', function() {

                var templateBarang = $('#form_section_barang').html();

                //loop through each input
                var sectionBarang = $(templateBarang).clone().end();
                // sectionsCountBarang++;

                sectionBarang.appendTo($('.duplicate-barang'));
                $('.mask').autoNumeric('init', {aForm:false});
                $('.date-saldo').datepicker({format: 'dd M yyyy', autoclose: true});
                return false;
            });

            //remove section
            $('.duplicate-barang').on('click', '.removeBarang', function() {
              //fade out section
              $(this).closest('.form-barang').fadeOut(300, function(){
                $(this).closest('.form-barang').remove();
              });
              return false;
            });
            //DUPLICATE FORM BOX
            //define template
            var formsJasa = [];
            $('.duplicate-jasa .form-jasa').each(function(){
              formsJasa.push($(this).clone());
            })


            //add new section
            $('body').on('click', '.addsectionJasa', function() {

                var templateJasa = $('#form_section_jasa').html();

                //loop through each input
                var sectionJasa = $(templateJasa).clone().end();
                // sectionsCountBarang++;

                sectionJasa.appendTo($('.duplicate-jasa'));
                $('.mask').autoNumeric('init', {aForm:false});
                $('.date-saldo').datepicker({format: 'dd M yyyy', autoclose: true});
                return false;
            });

            //remove section
            $('.duplicate-jasa').on('click', '.removeJasa', function() {
              //fade out section
              $(this).closest('.form-jasa').fadeOut(300, function(){
                $(this).closest('.form-jasa').remove();
              });
              return false;
            });
            //DUPLICATE FORM BOX
            //define template
            var formsKasBank = [];
            $('.duplicate-kas-bank .form-kas-bank').each(function(){
              formsKasBank.push($(this).clone());
            })


            //add new section
            $('body').on('click', '.addsectionKasBank', function() {

                var templateKasBank = $('#form_section_kas_bank').html();

                //loop through each input
                var sectionKasBank = $(templateKasBank).clone().end();
                // sectionsCountBarang++;

                sectionKasBank.appendTo($('.duplicate-kas-bank'));
                $('.mask').autoNumeric('init', {aForm:false});
                $('.date-saldo').datepicker({format: 'dd M yyyy', autoclose: true});
                return false;
            });

            //remove section
            $('.duplicate-kas-bank').on('click', '.removeKasBank', function() {
              //fade out section
              $(this).closest('.form-kas-bank').fadeOut(300, function(){
                $(this).closest('.form-kas-bank').remove();
              });
              return false;
            });
           $('.duplicate-sections').on('click', '.tombol_pelanggan', function(e) {
              e.preventDefault();
              $(this).closest('.form-section').find('.modal').modal({backdrop: 'static', keyboard: false, togggle: true});
          });
           $('.tambah-sections').on('click', '.tombol_pemasok', function(e) {
              e.preventDefault();
              $(this).closest('.formz-section').find('.modal').modal({backdrop: 'static', keyboard: false, togggle: true});
          });

          function changeMaskingToNumber(value) {
                if (value) {
                    if (typeof value == "string") {
                        return parseFloat(value.replace(/\,/gi, '').replace('.00', ''));                        
                    }else{
                        return value;
                    }
                }
          }