 //DUPLICATE FORM BOX
    //define template
    var forms = [];

    $('.duplicate-sections .form-section').each(function(){
      forms.push($(this).clone());                
    })

    //define counter
    var sectionsCount = 1;

    //add new section
    $('body').on('click', '.addsection', function() {
      var template = forms[$(this).data('section')];

      //increment
      sectionsCount++;

      //loop through each input
      var section = template.clone().find(':input').each(function(){

        //set id to store the updated section number
        var newId = this.id + sectionsCount;

        //update for label
        $(this).prev().attr('for', newId);

        //update id
        this.id = newId;

      }).end()

      //inject new section
      .appendTo($(this).prev('.duplicate-sections'));
      return false;
    });

    //remove section
    $('.duplicate-sections').on('click', '.remove', function() {
      //fade out section
      $(this).closest('.form-section').fadeOut(300, function(){
        $(this).closest('.form-section').empty();
      });
      return false;
    }); 
