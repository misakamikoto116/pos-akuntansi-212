var base_url = window.location.origin+'/akuntansi';
var token_ = $("meta[name='csrf-token']").attr("content");
var createData = new Boolean(true);
var updateRowId = null;

// ajax akun di cart
$( ".nama_akun_ubah" ).change(function() {
    var triggersAkun = $( ".nama_akun_ubah" ).val();
    if( triggersAkun == '' ){
        $('.bersih').find("input[type=text]").val("");
    }else{
        var nama_akun   = $('#detail_nama_akun').val();
        $.ajax({
            type: "GET",
            url: ""+base_url+"/ajax/get-akun-in-cart_penerimaan",
            data: {id : nama_akun,
                _token: token_},
            dataType: "json",
            success: function(response){
            $('#detail_akun_id').val(response.kode_akun)
            },
            fail: function(errMsg) {
                alert(errMsg);
            }
        });
    }
});


// ajax session bawah
    $('#myForm input[type=text]').on('change',function(){
        var keterangan   = $('#keterangan').val();
        var akun_id      = $('#akun_id').val();
        var nama_akun      = $('#nama_akun').val();
        var no_faktur    = $('#no_faktur').val();
        var tanggal       = $('#tanggal').val();
        var nominal       = $('#nominal').val();
        var terbilang       = $('#terbilang').val();
$.ajax({
    type: "POST",
    url: ""+base_url+"/ajax/ajax-master-penerimaan",
    data: { keterangan: keterangan,
            akun_id: akun_id,
            nama_akun: nama_akun,
            no_faktur: no_faktur,
            tanggal: tanggal,
            nominal: nominal,
            terbilang: terbilang,
            _token: token_
          },
    dataType: "json",
    success: function(data){
        alert(data.keterangan);
    },
    failure: function(errMsg) {
        alert(errMsg);
    }
});
});

// edit row
$('#example').on('click', '.edit_cart', function (e) {
    e.preventDefault();

    $('#detail_akun_id').val($(this).closest('tr').find('.td_id').text());
    $('#detail_nama_akun').val($(this).closest('tr').find('.td_nama').text()).trigger('change');
    $('#detail_nominal').val($(this).closest('tr').find('.harga').text());
    $('#detail_catatan').val($(this).closest('tr').find('.td_catatan').text());

    createData = false;
    updateRowId = $(this).closest('tr').attr('id');

    $('.block-hide').fadeIn();
    $('.block-hide').html(
                        $('#btnUpdate').html()
                    );
    $('#detail_nama_akun').focus();
});

// tampilkan tombol batal
$('.block-hide').on('click', '#btnBatal', function (e) {
    e.preventDefault();

    $('.bersih').find("input[type=text]").val("");
    $('#detail_nama_akun').val("").trigger('change');

    updateRowId = null;
    createData = true;

    $('.block-hide').fadeOut();
    $('.block-hide').empty();
});

// delete row
$('#example').on('click','.remove_cart', function (e) {
    e.preventDefault();

    if (updateRowId != null) {
        if ($(this).data('id') == updateRowId) {
            return false;
        }
    }
     swal({
      title: "Hapus Data",
      text: "Apakah Anda yakin?",
      icon: "error",
      buttons: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        var route = $(this).data('link');
        var row = $(this);
        $.ajax({
            type: "GET",
            url: route,
            data: {id: row.data('id')},
            success: function (result) {
                row.parents('tr').fadeOut(300,function() {
                    $(this).remove();
                    $('#total').autoNumeric('init', {aPad: false, aForm:false});
                    var total = $('#total').autoNumeric('get');
                    var jumlah = total - parseInt(row.closest('tr').find('.harga').text().replace(/\,/gi,'').replace('.00',''));

                    $('#total').autoNumeric('set', jumlah);
                });
            },
            fail: function(errMsg) {
                alert(errMsg);
            }
        })
          } else {
        swal("Data batal di Dihapus", {
            icon: "warning",
        });
      }
    });
});

// ajax cart
$('#detail_catatan').on('keydown',function (event) {
    var keyCode = event.keyCode || event.which;
    if ( keyCode == 9 ) {
        event.preventDefault();

        var itemPenerimaan = $('.tbody_rincian').find('tr');
        var sectionsPenerimaan = $(itemPenerimaan).find('.rincian_penerimaan_required');
        var nextToPenerimaan = true;

        for(var i = 0; i < sectionsPenerimaan.length; i++) {
            if (!sectionsPenerimaan[i].validity.valid) {
                nextToPenerimaan = false;
            }
        }
        if (nextToPenerimaan == false) {
            swal({
                icon: "error",
                text: "Tolong Lengkapi Inputan Rincian Penerimaan Anda"
            });

            return false;
        }

        if (createData == true) {
            $.ajax({
                type: "POST",
                url: ""+base_url+"/ajax/add-cart_penerimaan",
                data: { akun_detail_id: $('#detail_akun_id').val(),
                        nama_detail_akun: $('#detail_nama_akun').val(),
                        nominal_detail: $('#detail_nominal').autoNumeric('get'),
                        catatan_detail: $('#detail_catatan').val(),
                        _token: token_
                      },
                dataType: "json",
                success: function(resp){
                    $('#total').autoNumeric('destroy');
                    $('#example').append('<tr id="' + resp.rowId + '">'+
                        '<td class="td_id">'+resp.options.akun_id+'</td>'+
                        '<td class="td_nama">'+resp.options.nama_detail_akun+'</td>'+
                        '<td class="harga duit">'+resp.price+'</td>'+
                        '<td class="td_catatan">'+resp.options.catatan_detail+'</td>'+
                        '<td>'+
                            '<a href="#" style="margin-right: 4px" class="btn btn-warning edit_cart" data-id="'+resp.rowId+'" data-link=""><i class="fa fa-pencil"></i> </a>' +
                            '<a href="#" class="btn btn-danger remove_cart" data-id="'+resp.rowId+'" data-link="'+base_url+'/ajax/delete-cart_penerimaan"> <i class="fa fa-trash"></i></a>'+
                        '</td>'+
                        '</tr>');
                        $('#'+resp.rowId+'').find('.duit').text(resp.price).autoNumeric('init',{aPad: false});
                        var sum = 0;
                        $(".harga").each(function(){
                          sum += parseFloat($(this).text().replace(/\,/gi,'').replace('.00',''));
                        });
                        $('#total').text(sum).autoNumeric('init', {aPad: false, aSign: "Rp "});

                        $('.bersih').find("input[type=text]").val("");
                        $('#detail_nama_akun').val("").trigger('change');
                },
                fail: function(errMsg) {
                    alert(errMsg);
                }
            });
        } else {
            $.ajax({
                type: "GET",
                url: ""+base_url+"/ajax/edit-cart_penerimaan",
                data: { rowId: updateRowId,
                        akun_detail_id: $('#detail_akun_id').val(),
                        nama_detail_akun: $('#detail_nama_akun').val(),
                        nominal_detail: $('#detail_nominal').autoNumeric('get'),
                        catatan_detail: $('#detail_catatan').val(),
                        _token: token_
                      },
                dataType: "json",
                success: function (resp) {
                    if (resp.sukses) {
                        $('#total').autoNumeric('destroy');
                        var eleId = $('tr#' + updateRowId);

                        eleId.find('.td_id').html($('#detail_akun_id').val());
                        eleId.find('.td_nama').html($('#detail_nama_akun').val());
                        eleId.find('.harga').html($('#detail_nominal').val());
                        eleId.find('.td_catatan').html($('#detail_catatan').val());

                        eleId.attr('id', resp.data.rowId);
                        eleId.find('.edit_cart').data('id', resp.data.rowId);
                        eleId.find('.remove_cart').data('id', resp.data.rowId);

                        var sum = 0;
                        $(".harga").each(function(){
                          sum += parseFloat($(this).text().replace(/\,/gi,'').replace('.00',''));
                        });

                         $('#total').text(sum).autoNumeric('init', {aPad: false,aSign: "Rp "});

                        $('.bersih').find("input[type=text]").val("");
                        $('#detail_nama_akun').val("").trigger('change');

                        updateRowId = null;
                        createData = true;

                        $('.block-hide').fadeOut();
                        $('.block-hide').empty();
                    }
                },
                fail: function(errMsg) {
                    alert(errMsg);
                }

            });
        }

        return false;
    }
});

    // ajax get kode_akun
    $( ".target" ).change(function() {
        changeNamaAkun();            
    });


    function changeNamaAkun() {
        var nama_akun = $('#nama_akun').val();

        $.ajax({
            type: "GET",
            url: "" + base_url + "/ajax/get-akun_id_penerimaan",
            data: {
                id: nama_akun,
                _token: token_
            },
            dataType: "json",
            success: function (response) {
                $('#akun_id').val(response.kode_akun)
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    }

//ajax validate nominal
$(document).ready(function() {
    changeNamaAkun();
  $("#btnSubmit").click(function(e){
    e.preventDefault();
    var nominal = $("input[name='nominal']").val();
    var type = 'penerimaan';
    $.ajax({
        url: ""+base_url+"/ajax/validate-pembayaran",
        type:'POST',
        data: {_token:token_, nominal:nominal, type: type},
        success: function(data) {
            if($.isEmptyObject(data.error)){
                 var form = $('.myForm');
                swal({
                  title: "Simpan Data",
                  text: data.success,
                  icon: "info",
                  buttons: true,
                })
                .then((willSave) => {
                  if (willSave) {
                    form.submit();
                  } else {
                    swal("Data batal di Simpan", {
                        icon: "success",
                    });
                  }
                });
            }else{
               swal({
                    icon: "error",
                    text: data.error
                    });
            }
        }
    });
  });


  $('#balancing').on('click', function (e) {
    e.preventDefault();
    $.ajax({
        url: ""+base_url+"/ajax/getTotal",
        type: 'GET',
        data: {
            type: 'penerimaan'
        },
        dataType: 'JSON',
        success: function (result) {
            if (result.success) {
                $('#nominal').autoNumeric('set', result.data.total);
            } else {
                swal({
                    icon: 'Error',
                    text: 'Error Saat Menyeimbangkan Jumlah'
                });
            }
        },
        error: function () {
            swal({
                icon: 'Error',
                text: 'Error Saat Menyeimbangkan Jumlah'
            });
        }
    });

  });

});

// Fungsi Terbilang
    $(document).ready(function($) {
        terbilang();
    });

    function terbilang(){

    var bilangan=$("#nominal").val().replace(/\,/gi,'').replace('.00','');
    var kalimat="";
    var angka   = new Array('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');
    var kata    = new Array('','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan');
    var tingkat = new Array('','Ribu','Juta','Milyar','Triliun');
    var panjang_bilangan = bilangan.length;

    /* pengujian panjang bilangan */
    if(panjang_bilangan > 15){
        kalimat = "Diluar Batas";
    }else{
        /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
        for(i = 1; i <= panjang_bilangan; i++) {
            angka[i] = bilangan.substr(-(i),1);
        }

        var i = 1;
        var j = 0;

        /* mulai proses iterasi terhadap array angka */
        while(i <= panjang_bilangan){
            subkalimat = "";
            kata1 = "";
            kata2 = "";
            kata3 = "";

            /* untuk Ratusan */
            if(angka[i+2] != "0"){
                if(angka[i+2] == "1"){
                    kata1 = "Seratus";
                }else{
                    kata1 = kata[angka[i+2]] + " Ratus";
                }
            }

            /* untuk Puluhan atau Belasan */
            if(angka[i+1] != "0"){
                if(angka[i+1] == "1"){
                    if(angka[i] == "0"){
                        kata2 = "Sepuluh";
                    }else if(angka[i] == "1"){
                        kata2 = "Sebelas";
                    }else{
                        kata2 = kata[angka[i]] + " Belas";
                    }
                }else{
                    kata2 = kata[angka[i+1]] + " Puluh";
                }
            }

            /* untuk Satuan */
            if (angka[i] != "0"){
                if (angka[i+1] != "1"){
                    kata3 = kata[angka[i]];
                }
            }

            /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
            if ((angka[i] != "0") || (angka[i+1] != "0") || (angka[i+2] != "0")){
                subkalimat = kata1+" "+kata2+" "+kata3+" "+tingkat[j]+" ";
            }

            /* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
            kalimat = subkalimat + kalimat;
            i = i + 3;
            j = j + 1;
        }

        /* mengganti Satu Ribu jadi Seribu jika diperlukan */
        if ((angka[5] == "0") && (angka[6] == "0")){
            kalimat = kalimat.replace("Satu Ribu","Seribu");
        }
    }
    document.getElementById("terbilang").innerHTML=kalimat;
}