/**
* Theme: Ubold Admin Template
* Author: Coderthemes
* Form wizard page
*/

!function($) {
    "use strict";

    var FormWizard = function() {};

    FormWizard.prototype.createBasic = function($form_container) {
        $form_container.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onFinishing: function (event, currentIndex) {
                //NOTE: Here you can do form validation and return true or false based on your validation logic
                console.log("Form has been validated!");
                return true;
            },
            onFinished: function (event, currentIndex) {
               //NOTE: Submit the form, if all validation passed.
                console.log("Form can be submitted using submit method. E.g. $('#basic-form').submit()");
                $("#basic-form").submit();

            }
        });
        return $form_container;
    },
    //creates form with validation
    FormWizard.prototype.createValidatorForm = function($form_container) {
        $form_container.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.after(error);
            }
        });
        $form_container.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onStepChanging: function (event, currentIndex, newIndex) {
                $form_container.validate().settings.ignore = ":disabled,:hidden";
                return $form_container.valid();
            },
            onFinishing: function (event, currentIndex) {
                $form_container.validate().settings.ignore = ":disabled";
                return $form_container.valid();
            },
            onFinished: function (event, currentIndex) {
                alert("Submitted!");
            }
        });

        return $form_container;
    },
    //creates vertical form
    FormWizard.prototype.createVertical = function($form_container) {
        $form_container.steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "fade",
            stepsOrientation: "vertical",
            onStepChanging: function (event, currentIndex, nextIndex) {
                var nextTo;
                if (currentIndex == 0) {
                  var form_perusahaan = $('.perusahaan').serializeArray();
                  $.ajax({
                    url: 'ajax/perusahaan-validasi',
                    type: 'GET',
                    async: false,
                    dataType: 'JSON',
                    data: {form_perusahaan: form_perusahaan}
                  })
                  .done(function(resp) {
                    if (resp.success == false) {
                      nextTo = false;
                      swal({
                            icon: "error",
                            text: "Tolong Lengkapi Inputan Anda"
                      });
                      // var v_error = "<div class='alert alert-danger'>Tolong lengkapi inputan anda</div>";
                      // $('.form-error-perusahaan').html(v_error);
                    } else if(resp.success == true){
                      nextTo = true;
                    }
                  });
                  return nextTo;

                }else if (currentIndex == 1) {
                  $('.select2').select2();
                  var inputValidateKasBank = $('.kas_bank_sections .kas_bank_required');

                    for(var i = 0; i < inputValidateKasBank.length; i++) {
                        if (!inputValidateKasBank[i].validity.valid) {
                            nextTo = false;
                        }
                    }

                    if (nextTo == false) {
                        swal({
                            icon: "error",
                            text: "Tolong Lengkapi Inputan Anda"
                        });
                    } else {
                        nextTo = true;
                    }

                    return nextTo;
                }else if (currentIndex == 2) {
                    var inputValidate = $('.pelanggan_sections .pelanggan_required');

                    for(var i = 0; i < inputValidate.length; i++) {
                        if (!inputValidate[i].validity.valid) {
                            nextTo = false;
                        }
                    }

                    var sections = $('.pelanggan_sections .form-section');

                    for (var i = 0; i < sections.length; i++) {
                        var rowSection = $(sections[i]).find('.purchaseItemsPelanggan');
                        if ($(rowSection).find('tr').length <= 0) {
                            nextTo = false;
                        }
                    }

                    if (nextTo == false) {
                        swal({
                            icon: "error",
                            text: "Tolong Lengkapi Inputan Anda"
                        });
                    } else {
                        nextTo = true;
                    }

                    return nextTo;
                }else if (currentIndex == 3) {
                    var inputValidatePemasok = $('.pemasok_sections .pemasok_required');

                    for(var i = 0; i < inputValidatePemasok.length; i++) {
                        if (!inputValidatePemasok[i].validity.valid) {
                            nextTo = false;
                        }
                    }

                    var sections_pemasok = $('.pemasok_sections .formz-section');

                    for (var i = 0; i < sections_pemasok.length; i++) {
                        var rowSection = $(sections_pemasok[i]).find('.purchaseItemsPemasok');
                        if ($(rowSection).find('tr').length <= 0) {
                            nextTo = false;
                        }
                    }

                    if (nextTo == false) {
                        // alert('Tolong Periksa Kembali');
                        swal({
                            icon: "error",
                            text: "Tolong Lengkapi Inputan Anda"
                        });
                    } else {
                        nextTo = true;
                        swal("Pilih untuk import atau input barang ?", {
                          closeOnClickOutside: false,
                          closeOnEsc: false,
                          icon: "warning",
                          buttons: {
                            import: {
                              text: "Import Data",
                              value: "import",
                            },
                            input: {
                                text: "Input Data",
                                value: "input",
                            },
                          },
                        })
                        .then((value) => {
                          switch (value) {
                            case "import":
                                $('div').find('#import-barang').show();
                                $('div').find('#input-barang').hide();
                              break;
                            case "input":
                              break;

                          }
                        });
                    }

                    return nextTo;
                }else if (currentIndex == 4) {
                  nextTo = true;
                  return nextTo;
                }else if (currentIndex == 5) {
                    var inputValidateBarang = $('.barang_sections .barang_required');

                    for(var i = 0; i < inputValidateBarang.length; i++) {
                        if (!inputValidateBarang[i].validity.valid) {
                            nextTo = false;
                        }
                    }

                    if (nextTo == false) {
                        swal({
                            icon: "error",
                            text: "Tolong Lengkapi Inputan Anda"
                        });
                    } else {
                        nextTo = true;
                    }

                    return nextTo;  
                }else if (currentIndex == 6) {
                    var inputValidateJasa = $('.jasa_sections .jasa_required');

                        for(var i = 0; i < inputValidateJasa.length; i++) {
                            if (!inputValidateJasa[i].validity.valid) {
                                nextTo = false;
                            }
                        }

                        if (nextTo == false) {
                            swal({
                                icon: "error",
                                text: "Tolong Lengkapi Inputan Anda"
                            });
                            } else {
                                nextTo = true;
                        }

                        return nextTo;
                }
            },
            onStepChanged: function (event, currentIndex, prevIndex) {

            },
            onFinishing: function (event, currentIndex) {
                var nextTo;
                var inputValidateJasa = $('.jasa_sections .jasa_required');
                var formFinish = $('.submitForm');
                    for(var i = 0; i < inputValidateJasa.length; i++) {
                        if (!inputValidateJasa[i].validity.valid) {
                            nextTo = false;
                        }
                    }

                    if (nextTo == false) {
                        swal({
                            icon: "error",
                            text: "Tolong Lengkapi Inputan Anda"
                        });
                        } else {
                            swal({
                              title: "Simpan Data Awal",
                              text: 'Apakah anda yakin untuk menyimpan data awal ini?',
                              icon: "info",
                              buttons: true,
                            })
                            .then((willSave) => {
                              if (willSave) {
                                formFinish.submit();
                              } else {
                                swal("Data batal di Simpan", {
                                    icon: "success",
                                });
                              }
                            });
                    } 
            }, 
        });
        return $form_container;
    },
    FormWizard.prototype.init = function() {
        //initialzing various forms

        //basic form
        this.createBasic($("#basic-form"));

        //form with validation
        this.createValidatorForm($("#wizard-validation-form"));

        //vertical form
        this.createVertical($("#wizard-vertical"));
    },
    //init
    $.FormWizard = new FormWizard, $.FormWizard.Constructor = FormWizard
}(window.jQuery),

//initializing
function($) {
    "use strict";
    $.FormWizard.init()
}(window.jQuery);
