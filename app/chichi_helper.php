<?php
/**
 * Created by vsCode
 * by: efendi
 * Date: 12/11/17
 * Time: 11:00 PM
 * Pastikan menggunakan prefix mac_ karena custom function utk project
 * mac ex. mac_defaultWriter, mac_themes.
 */
use GuzzleHttp\Client;

if (!function_exists('app_config_identitas_id')) {
	/**
	 * Get current theme.
	 *
	 * @return string
	 */
	function app_config_identitas_id()
	{
		$identitas    = DB::table('identitas')->get();
		$identitas_id = null;

		if ($identitas->isNotEmpty()) {
			$identitas_id = $identitas->first()->id;
		}

		return $identitas_id;
	}
}

if (!function_exists('app_config_logo')) {
	/**
	 * Get current theme.
	 *
	 * @return string
	 */
	function app_config_logo()
	{
		$identitas = DB::table('identitas')->first();

		$path = null;

		if (!empty($identitas)) {
			$path = null === $identitas->logo_perusahaan ? $identitas->nama_perusahaan : $identitas->logo_perusahaan;
		}

		return $path;
	}
}

if (!function_exists('app_config_account')) {
	/**
	 * Get current theme.
	 *
	 * @return string
	 */
	function app_config_account()
	{
		$user = DB::table('users')->first();

		return [
			'user_name' => $user->email,
			'logged'    => $user->logged,
		];
	}
}

if (!function_exists('app_config_title')) {
	/**
	 * Get current theme.
	 *
	 * @return string
	 */
	function app_config_title()
	{
		$identitas = DB::table('identitas')->first();

		$nama_perusahaan = null;

		if (!empty($identitas)) {
			$nama_perusahaan = $identitas->nama_perusahaan;
		}

		return $nama_perusahaan;
	}
}

if (!function_exists('chichi_themes')) {
	/**
	 * Get current theme.
	 *
	 * @return string
	 */
	function chichi_themes()
	{
		return config('app.chichi.theme');
	}
}

if (!function_exists('chichi_title')) {
	/**
	 * Get current theme.
	 *
	 * @return string
	 */
	function chichi_title()
	{
		return app_config_title();
	}
}

if (!function_exists('chichi_logo')) {
	/**
	 * Get current theme.
	 *
	 * @return string
	 */
	function chichi_logo()
	{
		return app_config_logo();
	}
}

if (!function_exists('chichi_account')) {
	/**
	 * Get current theme.
	 *
	 * @return string
	 */
	function chichi_account()
	{
		return app_config_account();
	}
}

if (!function_exists('chichi_identitas_id')) {
	/**
	 * Get current theme.
	 *
	 * @return string
	 */
	function chichi_identitas_id()
	{
		return app_config_identitas_id();
	}
}

if (!function_exists('custom_session')) {
	/**
	 * Create Custom Session like chart session.
	 *
	 * @key string
	 * @array_value array content
	 *
	 * @return mix
	 */
	function custom_session($key, array $array_value)
	{
		session()->put([$key => $array_value]);
	}
}

if (!function_exists('send_notif_socket')) {
	/**
	 * Send data to socket.io server.
	 *
	 * @key string
	 * @array_value array content
	 *
	 * @return array|string
	 */
	function send_notif_socket($channel, $notif)
	{
		try {
			$client   = new Client();
			$url      = env('SOCKET_URL') . 'channel';
			$response = $client->request('POST', $url, [
				'headers'     => ['x-token' => '1c2VyX2lkIjoxLCJpYXQiOjE1MzQxNDg4OD'],
				'form_params' => [
					'_channel_socket' => $channel,
					'_message'        => $notif,
				],
			]);
			$status = $response->getStatusCode();
			if (200 == $status) {
				return true;
			} else {
				return false;
			}
		} catch (\GuzzleHttp\Exception\ConnectException $e) {
			return false;
		}
	}
}
if (!function_exists('send_display_kasir')) {
	/**
	 * @param array $data
	 */
	function send_display_kasir(array $data)
	{
		$client = new Client(['headers' => [
			'x-token'      => '1c2VyX2lkIjoxLCJpYXQiOjE1MzQxNDg4OD',
			'Content-Type' => 'application/json',
		]]);

		$url = env('NODE_PORT', false) ? 'localhost:' . env('NODE_PORT') . '/displaykasir' : env('SOCKET_URL') . 'displaykasir';
		// $url = env('SOCKET_URL').'displaykasir';
		$req = $client->post($url, [
			\GuzzleHttp\RequestOptions::JSON   => $data,
			\GuzzleHttp\RequestOptions::VERIFY => false,
		]);
	}
}
