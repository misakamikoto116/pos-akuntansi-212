<?php

namespace App\Jobs;

use Helpers\PenjualanApi;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PenjualanQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $dataPenjualan; // arrayBerupa data yg diKirim ke Antrian

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    /**
     * Create a new job instance.
     *
     * @param $dataPenjualan
     */
    public function __construct($dataPenjualan)
    {
        $this->dataPenjualan = $dataPenjualan;
    }

    /**
     * Execute the job.
     * kerjakan proses kirim ke API Koperasi di Sini
     * @return void
     */
    public function handle()
    {
        $apiToKop = new  PenjualanApi();
        $apiToKop->sendPenjualanApi($this->dataPenjualan);
        dd($this->dataPenjualan);
    }
}
