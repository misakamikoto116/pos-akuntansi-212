<?php
namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [];

	/**
	 * A list of the inputs that are never flashed for validation exceptions.
	 *
	 * @var array
	 */
	protected $dontFlash = [
		'password',
		'password_confirmation',
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param \Exception $exception
	 */
	public function report(Exception $e)
	{
		if (app()->bound('sentry') && $this->shouldReport($e)) {
			app('sentry')->captureException($e);
		}

		parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Exception               $exception
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $exception)
	{
		if ("Illuminate\Database\Eloquent\ModelNotFoundException" == get_class($exception)) {
			if ('api' == $request->segment(1)) {
				return response()->json([
					'status'  => false,
					'message' => 'tidak ditemukan',
					'data'    => [null],
				], 404);
			}

			return response()->view('exception.error',
				['message'      => 'Data Tidak di temukan',
					'full_message' => 'Harap perhatikan INPUT yang Anda masukkan',
					'code'         => '4</span><i class="ti-face-sad text-pink"></i><span class="text-info">4</span>', ], 404
			);
		}
		if ($this->isHttpException($exception)) {
			// if ($this->isHttpException($exception)) {
			switch ($exception->getStatusCode()) {
				/*Not Found*/
				case 404:
					if ('api' == $request->segment(1)) {
						return response()->json([
							'status'  => false,
							'message' => 'not found !!',
							'data'    => [null],
						], $exception->getStatusCode());
					}

					return response()->view('exception.error',
						['message'      => 'Halaman Tidak di temukan',
							'full_message' => 'Harap perhatikan URL yang Anda masukkan',
							'code'         => '4</span><i class="ti-face-sad text-pink"></i><span class="text-info">4</span>', ], 404
					);
					break;

				/*Forbidden status in response*/
				case 403:
					if ('api' == $request->segment(1)) {
						return response()->json(['sukses' => false, 'message' => 'tidak di ijinkan'], $exception->getStatusCode());
					}

					return response()->view('exception.error',
						['message'      => 'Akses Tidak di perkenankan',
							'full_message' => 'Apabila Anda menemukan halaman ini, harap kontak Developer Website',
							'code'         => '4</span><i class="ti-face-sad text-pink"></i><span class="text-info">3</span>',    ], 403
					);
					break;
				case 401:
					if ('api' == $request->segment(1)) {
						return response()->json(['sukses' => false, 'message' => 'Unauthorized'], $exception->getStatusCode());
					}
					break;
				/*Service Unavailable ex: Maintenance*/
				case 503:
					if ('api' == $request->segment(1)) {
						return response()->json(['sukses' => false, 'message' => 'Website Sedang dalam keadaan Maintenance'], $exception->getStatusCode());
					}

					return response()->view('exception.error',
						['message'      => 'Website Sedang dalam keadaan Maintenance',
							'full_message' => 'Harap datang kembali lagi nanti :)',
							'code'         => '<span class="text-primary">Site is</span> <span class="text-pink">Under</span> <span class="text-info">Maintenance</span>', ], 503
					);
					break;

				/*Internal Server*/
				case 500:
					break;

				default:
					return response()->view('exception.error',
						['message'      => 'Data Tidak di temukan',
							'full_message' => 'Ada data yang belum ada. Apabila Anda menemukan halaman ini, harap kontak Developer Website',
							'code'         => '5</span><i class="ti-face-sad text-pink"></i><i class="ti-face-sad text-info"></i>', ], 403
					);
					break;
			}
		}

		if ('live' == env('APP_ENV') && $exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
			// if ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
			if ('api' == $request->segment(1)) {
				return response()->json(['error' => 'not_found', 'sukses' => false], $exception->getStatusCode());
			}

			return response()->view('exception.error',
				['message'      => 'Data Tidak di temukan',
					'full_message' => 'Harap perhatikan URL yang Anda masukkan',
					'code'         => '4</span><i class="ti-face-sad text-pink"></i><span class="text-info">4</span>', ], 404
			);
		}

		return parent::render($request, $exception);
	}

	/**
	 * Convert an authentication exception into an unauthenticated response.
	 *
	 * @param \Illuminate\Http\Request                 $request
	 * @param \Illuminate\Auth\AuthenticationException $exception
	 *
	 * @return \Illuminate\Http\Response
	 */
	protected function unauthenticated($request, AuthenticationException $exception)
	{
		if ($request->expectsJson()) {
			return response()->json(['error' => 'Unauthenticated.'], 401);
		}
		if ('api' == $request->segment(1)) {
			return response()->json(['sukses' => false, 'message' => 'Unauthenticated'], 401);
		}

		return redirect()->guest(route('login'));
	}
}
