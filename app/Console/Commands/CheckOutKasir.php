<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Pos\Models\KasirCek;
use App\Modules\Pos\Models\KasirMesin;

class CheckOutKasir extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkout:kasir';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checkout Semua Kasir yang Menyala';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
            KasirMesin $kasirMesin,
            KasirCek $kasirCek
        ) {
            parent::__construct();
            $this->kasirMesin                       = $kasirMesin;
            $this->kasirCek                         = $kasirCek;
        }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return $this->kasirMesin->where('status', 1)->update(array('status' => 0));
    }
}
