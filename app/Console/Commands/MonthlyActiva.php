<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Akuntansi\Models\DaftarAktivaTetap;
use App\Modules\Akuntansi\Models\PengeluaranAktiva;
use App\Modules\Akuntansi\Repositories\DaftarAktivaTetapRepository;

class MonthlyActiva extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'activa:monthly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        DaftarAktivaTetap $daftarAktivaTetap,
        PengeluaranAktiva $pengeluaranAktiva,
        DaftarAktivaTetapRepository $daftarAktivaTetapRepository)
    {
        parent::__construct();
        $this->daftarAktivaTetap            =   $daftarAktivaTetap;
        $this->pengeluaranAktiva            =   $pengeluaranAktiva;
        $this->daftarAktivaTetapRepository  =   $daftarAktivaTetapRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Menjalankan Command');
        $pluckID    =   $this->pengeluaranAktiva->pluck('daftar_aktiva_tetap_id');
        $data       =   $this->daftarAktivaTetap->find($pluckID);
        if($data){
            $this->daftarAktivaTetapRepository->iPengeluaranAktiva($data);
        }
        return $this->info('Command Berakhir');
    }
}
