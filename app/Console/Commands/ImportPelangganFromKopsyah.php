<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use App\Modules\Akuntansi\Models\InformasiPelanggan;


class ImportPelangganFromKopsyah extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $pos_api_url;
    protected $signature = 'import:pelanggan:kopsyah';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import semua pelanggan dari kopsyah dan simpan ke POS (Repeatable)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->pos_api_url = env('API_KOPSYAH', null);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client;
        $response = $client->get($this->pos_api_url.'/api/get-all-anggota');
            
        $datas = [];
        $anggota = json_decode($response->getBody()->getContents());
        $InformasiPelanggan = InformasiPelanggan::pluck('no_pelanggan');
        $anggotaFilter = collect($anggota->data)->whereNotIn('no_pelanggan', $InformasiPelanggan);
        foreach ($anggotaFilter as $data) {
            $datas[] = [
                'no_pelanggan'      => $data->no_pelanggan,
                'nama'              => $data->nama,
                'alamat'            => $data->alamat,
                'alamat_pajak'      => $data->alamat_pajak,
                'kota'              => $data->kota,
                'telepon'           => $data->telepon,
                'personal_kontak'   => $data->personal_kontak,
                'tipe_pelanggan_id' => 5,
                'email'             => $data->email,
                'nik'               => $data->nik
            ];
        }
        
        InformasiPelanggan::insert($datas);

        $this->info('Berhasil Diimport silahkan update data pelanggan');

    }
}
