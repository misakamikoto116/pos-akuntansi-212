<?php
namespace App\Console\Commands;

use App\Imports\ProdukWithAccountImport;
use Excel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\PenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\DetailPenyesuaianPersediaan;

class ImportProduk extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'import:produk {nama}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import Produk';

	/**
	 * Create a new command instance.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		ini_set('memory_limit', '1024M'); // or you could use 1G
		$fileImport = $this->argument('nama');
		$path       = file_exists($fileImport) ? $fileImport : storage_path($fileImport);
		if (!file_exists($path)) {
			return $this->error('File tidak ditemukan');
		}
		$this->info("file $path ditemukan...");
		$this->info('mempersiapkan file yang akan diimport');
		$file          = file($path, FILE_SKIP_EMPTY_LINES);
		$countFileLine = count($file);
		if (count($file) < 1) {
			return $this->error('tidak ada data yang di import, maka proses import dihentikan');
		}
		$this->info("ada $countFileLine barang yang akan diimport");
		$this->info('persiapan import barang');
		// $skr  = date('Y-m-d H:i:s');
		// $akun = new Akun();
		// // $kodeAkun                = ['7200-004', '5100-001', '4201-001', '4202', '4101-001', '1105-999', '1105-001', '2106'];
		// // $akun_beban_id           = $akun->where('kode_akun', $kodeAkun[0])->first()->id;
		// // $akun_hpp_id             = $akun->where('kode_akun', $kodeAkun[1])->first()->id;
		// // $akun_ret_penjualan_id   = $akun->where('kode_akun', $kodeAkun[2])->first()->id;
		// // $akun_disk_penjualan_id  = $akun->where('kode_akun', $kodeAkun[3])->first()->id;
		// // $akun_persedian_id       = $akun->where('kode_akun', $kodeAkun[1])->first()->id;
		// // $akun_penjualan_id       = $akun->where('kode_akun', $kodeAkun[4])->first()->id;
		// // $akun_barang_terkirim_id = $akun->where('kode_akun', $kodeAkun[5])->first()->id;
		// // $akun_ret_pembelian_id   = $akun->where('kode_akun', $kodeAkun[5])->first()->id;
		// // $akun_belum_tertagih_id  = $akun->where('kode_akun', $kodeAkun[7])->first()->id;
		// $this->info("file $path mulai diimport ke database");
		// try {
		// 	DB::beginTransaction();
		// 	Excel::filter('chunk')->load($path)->chunk(500, function ($results) use ($akun, $skr) {
		// 		foreach ($results as $key => $value) {
		// 			dd($value);
		// 			if (null != $value->harga_jual && $value->harga_jual >= 200):
		// 				$product_master_list = [
		// 					'tipe_barang'             => 0,
		// 					'parent_id'               => null,
		// 					'no_barang'               => $value->no_barang,
		// 					'status'                  => 1,
		// 					'tipe_persedian'          => $value->tipe_persedian,
		// 					'keterangan'              => $value->keterangan,
		// 					'harga_standar_def'       => $value->harga_standar_def,
		// 					'kuantitas'               => $value->kuantitas,
		// 					'harga_jual'              => $value->harga_jual,
		// 					'unit'                    => $value->unit,
		// 					'status_umkm'             => $value->status_umkm,
		// 					'gudang_id'               => 6,
		// 					'kategori_produk_id'      => 12, //(int) $value->kategori_produk_id ?? 12,						'pemasok_id'              => null, //$value->pemasok,
		// 					'min_jumlah_reorder'      => $value->min_jumlah_reorder,
		// 					'kode_pajak_perse_id'     => 'NULL' == (int) $value->kode_pajak_perse_id ? null : $value->kode_pajak_perse_id,
		// 					'catatan'                 => $value->catatan,
		// 					'akun_beban_id'           => 129, //(null == $value->akun_beban ? null : $akun->where('kode_akun', $value->akun_beban)->first()->id),
		// 					'akun_hpp_id'             => 94, //(null == $value->akun_hpp ? null : $akun->where('kode_akun', $value->akun_hpp)->first()->id), //$value->akun_hpp_id,
		// 					'akun_ret_penjualan_id'   => 86, //(null == $value->akun_retur_penjualan ? null : $akun->where('kode_akun', $value->akun_retur_penjualan)->first()->id), //86,
		// 					'akun_disk_penjualan_id'  => 92, //(null == $value->akun_disk_penjualan ? null : $akun->where('kode_akun', $value->akun_disk_penjualan)->first()->id), //92,
		// 					'akun_persedian_id'       => 21, //(null == $value->akun_persediaan ? null : $akun->where('kode_akun', $value->akun_persediaan)->first()->id),
		// 					'akun_penjualan_id'       => 77, //(null == $value->akun_penjualan ? null : $akun->where('kode_akun', $value->akun_penjualan)->first()->id), //77,
		// 					'akun_barang_terkirim_id' => 28, //(null == $value->akun_barang_terkirim ? null : $akun->where('kode_akun', $value->akun_barang_terkirim)->first()->id), //28,
		// 					'akun_ret_pembelian_id'   => 21, //(null == $value->akun_retur_pembelian ? null : $akun->where('kode_akun', $value->akun_retur_pembelian)->first()->id),
		// 					'akun_belum_tertagih_id'  => 68, //(null == $value->akun_belum_tertagih ? null : $akun->where('kode_akun', $value->akun_belum_tertagih)->first()->id),
		// 					'deleted_at'              => null,
		// 					'diskon'                  => null,
		// 					'created_at'              => $skr,
		// 				];
		// 			$produk = new Produk($product_master_list);
		// 			$produk->save();
		// 			if ($value->kuantitas > 0) :
		// 					$x = 0;
		// 			$penyesuaian_persediaan = [
		// 				'no_penyesuaian'    => 'SALDO-AWAL/' . $produk->id,
		// 				'tgl_penyesuaian'   => $skr,
		// 				'kode_akun_id'      => 21,
		// 				'keterangan_produk' => $value->catatan,
		// 				'akun_penyesuaian'  => 75,
		// 				'keterangan'        => 'IMPORT CSV',
		// 				'created_at'        => $skr,
		// 				'updated_at'        => $skr,
		// 				'deleted_at'        => null,
		// 			];
		// 			$masterPenyesuaianPersediaan = new PenyesuaianPersediaan();
		// 			$masterPenyesuaianPersediaan->fill($penyesuaian_persediaan)->save();
		// 			$detail_penyesuaian = [
		// 				'penyesuaian_persediaan_id' => $masterPenyesuaianPersediaan->id, //detail dari $penyesuaian_persediaan
		// 				'produk_id'                 => $produk->id,
		// 				'keterangan_produk'         => $value->keterangan,
		// 				'satuan_produk'             => $value->unit,
		// 				'qty_produk'                => 0,
		// 				'qty_baru'                  => $value->kuantitas,
		// 				'nilai_skrg'                => 0,
		// 				'nilai_baru'                => $value->harga_modal,
		// 				'departemen'                => null,
		// 				'gudang_id'                 => 6,
		// 				'proyek'                    => null,
		// 				'serial_number'             => $value->no_barang,
		// 				'created_at'                => $skr,
		// 				'updated_at'                => $skr,
		// 				'deleted_at'                => null,
		// 			];
		// 			$detailPenyesuaian = new DetailPenyesuaianPersediaan();
		// 			$detailPenyesuaian->insert($detail_penyesuaian);
		// 			$transaksiKredit = [
		// 				//'id' => $i++,
		// 				'nominal'             => $value->harga_modal * $value->kuantitas,
		// 				'tanggal'             => $skr,
		// 				'akun_id'             => 75,
		// 				'produk_id'           => $produk->id,
		// 				'item_id'             => $produk->id,
		// 				'item_type'           => Produk::class,
		// 				'status'              => 0,
		// 				'status_rekonsiliasi' => 0,
		// 				'sumber'              => 'SALDO AWAL',
		// 				'keterangan'          => 'SALDO-AWAL/' . $produk->id,
		// 				'dari'                => 'Akun Penyesuaian',
		// 				'no_transaksi'        => 'SALDO-AWAL/' . $produk->id,
		// 				'desc_dev'            => null,
		// 				'deleted_at'          => null,
		// 				'created_at'          => $skr,
		// 				'updated_at'          => $skr,
		// 				'created_by'          => 2, //krn dari console should be auth()->user()->id,
		// 				'updated_by'          => 2, //krn dari console should be auth()->user()->id,
		// 				'deleted_by'          => null,
		// 			];
		// 			//dd($value->harga_modal * $value->kuantitas);
		// 			$transaksiKreditInsert = new Transaksi();
		// 			$transaksiKreditInsert->fill($transaksiKredit);
		// 			$transaksiKreditInsert->save();
		// 			$transaksiDebit = [
		// 				'nominal'             => $value->harga_modal * $value->kuantitas,
		// 				'tanggal'             => $skr,
		// 				'akun_id'             => 21,
		// 				'produk_id'           => $produk->id,
		// 				'item_id'             => $produk->id,
		// 				'item_type'           => Produk::class,
		// 				'status'              => 1,
		// 				'status_rekonsiliasi' => 0,
		// 				'sumber'              => 'SALDO AWAL',
		// 				'keterangan'          => 'SALDO-AWAL/' . $produk->id,
		// 				'dari'                => 'Akun Persediaan',
		// 				'no_transaksi'        => 'SALDO-AWAL/' . $produk->id,
		// 				'desc_dev'            => null,
		// 				'deleted_at'          => null,
		// 				'created_at'          => $skr,
		// 				'updated_at'          => $skr,
		// 				'created_by'          => 2, //krn dari console
		// 				'updated_by'          => 2, //krn dari console
		// 				'deleted_by'          => null,
		// 			];
		// 			$transaksiDebitInsert = new Transaksi();
		// 			$transaksiDebitInsert->fill($transaksiDebit);
		// 			$transaksiDebitInsert->save();
		// 			endif;

		// 			$product_detail_list = [
		// 				'no_faktur'      => 'POS-AWAL',
		// 				'tanggal'        => $skr,
		// 				'kuantitas'      => (null != $value->kuantitas ? $value->kuantitas : 0),
		// 				'harga_modal'    => (null != $value->harga_modal ? $value->harga_modal : 0),
		// 				'harga_terakhir' => (null != $value->harga_modal ? $value->harga_modal : 0),
		// 				'gudang_id'      => 6,
		// 				'transaksi_id'   => ($value->kuantitas > 0 ? $transaksiDebitInsert->id : null),
		// 				'produk_id'      => $produk->id,
		// 				'sn'             => $value->no_barang,
		// 				'status'         => 0,
		// 				'item_type'      => Produk::class, //jika
		// 				'item_id'        => $produk->id, //harus dari transasksi
		// 				'deleted_at'     => null,
		// 				'created_at'     => $skr,
		// 				'updated_at'     => $skr,
		// 			];
		// 			SaldoAwalBarang::insert($product_detail_list);
		// 			endif;
		// 		}
		// 	});
		// 	DB::commit();

		// 	return $this->info('Data berhasil di import');
		// } catch (Exception $exception) {
		// 	DB::rollback();
		// 	$this->error($exception->getMessage());
		// 	$this->info('error Trace :');
		// 	$this->info($exception->getTraceAsString());
		// }
		// // ---------------------------------->
		
		$this->info("file $path mulai diimport ke database");
		try {
			DB::beginTransaction();
			Excel::import(new ProdukWithAccountImport, $path);
			// Excel::filter('chunk')->load($path)->chunk(500, function ($results) use ($skr, $akuns) {
// 				$i = 0;
// 				foreach ($results as $key => $value) {
// 					if ($value->no_barang !== null && $value->keterangan !== null):
// 						$opening_balance			= $akuns->where('kode_akun', '3300' )->first()->id;
// 						$akun_hpp_id 				= null == $value->akun_hpp_id ? null : $akuns->where('kode_akun', $value->akun_hpp_id )->first()->id ?? null;
// 						$akun_ret_pembelian_id 		= null == $value->akun_ret_pembelian_id ? null : $akuns->where('kode_akun', $value->akun_ret_pembelian_id )->first()->id ?? null;
// 						$akun_penjualan_id 			= null == $value->akun_penjualan_id ? null : $akuns->where('kode_akun', $value->akun_penjualan_id )->first()->id ?? null;
// 						$akun_ret_penjualan_id 		= null == $value->akun_ret_penjualan_id ? null : $akuns->where('kode_akun', $value->akun_ret_penjualan_id )->first()->id ?? null;
// 						$akun_persedian_id			= null == $value->akun_persedian_id ? null : $akuns->where('kode_akun', $value->akun_persedian_id )->first()->id ?? null;
// 						$akun_disk_penjualan_id 	= null == $value->akun_disk_penjualan_id ? null : $akuns->where('kode_akun', $value->akun_disk_penjualan_id )->first()->id ?? null;
// 						$akun_barang_terkirim_id 	= null == $value->akun_barang_terkirim_id ? null : $akuns->where('kode_akun', $value->akun_barang_terkirim_id )->first()->id ?? null;
// 						$akun_belum_tertagih_id 	= null == $value->akun_belum_tertagih_id ? null : $akuns->where('kode_akun', $value->akun_belum_tertagih_id )->first()->id ?? null;
// 						$product_master_list = [
// 							'tipe_barang'             => $value->tipe_barang,
// 							'parent_id'               => ( null == $value->parent_id ? null : $value->parent_id ),
// 							'no_barang'               => $value->no_barang,
// 							'status'                  => $value->status,
// 							'tipe_persedian'          => $value->tipe_persedian ?? null,
// 							'keterangan'              => $value->keterangan,
// 							'harga_standar_def'       => $value->harga_standar_def ?? null,
// 							'kuantitas'               => $value->kuantitas,
// 							'harga_jual'              => $value->harga_jual,
// 							'unit'                    => $value->unit,
// 							'status_umkm'             => null == $value->status_umkm ? null : $value->status_umkm,
// 							'gudang_id'               => ( null == $value->gudang_id ? 6 : $value->gudang_id ),
// 							'kategori_produk_id'      => ( null == $value->kategori_produk_id ? 12 : $value->kategori_produk_id ),
// 							'pemasok_id'              => ( null == $value->pemasok_id ? null : $value->pemasok_id ),
// 							'min_jumlah_reorder'      => null == $value->min_jumlah_reorder ? null : $value->min_jumlah_reorder,
// 							'kode_pajak_perse_id'     => ( null == (int) $value->kode_pajak_perse_id ? null : $value->kode_pajak_perse_id ),
// 							'catatan'                 => $value->catatan,
// 							'akun_beban_id'           => ( null == $value->akun_beban_id ? null : $value->akun_beban_id ),
// 							'akun_hpp_id'             => (null == $value->akun_hpp_id ? null : $akun_hpp_id),
// 							'akun_ret_penjualan_id'   => (null == $value->akun_ret_penjualan_id ? null : $akun_ret_penjualan_id),
// 							'akun_disk_penjualan_id'  => (null == $value->akun_disk_penjualan_id ? null : $akun_disk_penjualan_id),
// 							'akun_persedian_id'       => (null == $value->akun_persedian_id ? null : $akun_persedian_id),
// 							'akun_penjualan_id'       => (null == $value->akun_penjualan_id ? null : $akun_penjualan_id),
// 							'akun_barang_terkirim_id' => (null == $value->akun_barang_terkirim_id ? null : $akun_barang_terkirim_id),
// 							'akun_ret_pembelian_id'   => (null == $value->akun_ret_pembelian_id ? null : $akun_ret_pembelian_id),
// 							'akun_belum_tertagih_id'  => (null == $value->akun_belum_tertagih_id ? null : $akun_belum_tertagih_id),
// 							'deleted_at'              => null,
// 							'diskon'                  => null,
// 							'created_at'              => $skr,
// 						];
// 						$produk = new Produk();
// 						$saveMasterProduk = $produk->fill($product_master_list);
// 						$saveMasterProduk->save();
// // -------------------->
// 						// if ($value->kuantitas > 0) :
// 							$x = 0;
// 							$penyesuaian_persediaan = [
// 								'no_penyesuaian'    => 'SALDO-AWAL-BARANG/IMPORT/ '.$skr.'/ '.$saveMasterProduk->id,
// 								'tgl_penyesuaian'   => $skr,
// 								'kode_akun_id'      => $akun_persedian_id,// 21,
// 								'keterangan_produk' => $value->catatan,
// 								'akun_penyesuaian'  => $opening_balance, //75,
// 								'keterangan'        => 'IMPORT XLSX',
// 								'created_at'        => $skr,
// 								'updated_at'        => $skr,
// 								'deleted_at'        => null,
// 								'status'            => 0,
// 							];
// 							$masterPenyesuaianPersediaan = new PenyesuaianPersediaan();
// 							$masterPenyesuaianPersediaan->fill($penyesuaian_persediaan)->save();
// 							$detail_penyesuaian = [
// 								'penyesuaian_persediaan_id' => $masterPenyesuaianPersediaan->id, //detail dari $penyesuaian_persediaan
// 								'produk_id'                 => $produk->id,
// 								'keterangan_produk'         => $value->keterangan,
// 								'satuan_produk'             => $value->unit,
// 								'qty_produk'                => 0,
// 								'qty_baru'                  => $value->kuantitas,
// 								'nilai_skrg'                => 0,
// 								'nilai_baru'                => $value->harga_modal,
// 								'departemen'                => null,
// 								'gudang_id'                 => 6,
// 								'proyek'                    => null,
// 								'serial_number'             => $value->no_barang,
// 								'created_at'                => $skr,
// 								'updated_at'                => $skr,
// 								'deleted_at'                => null,
// 							];
// 							$detailPenyesuaian = new DetailPenyesuaianPersediaan();
// 							$detailPenyesuaian->insert($detail_penyesuaian);
// 							$transaksiKredit = [
// 								//'id' => $i++,
// 								'nominal'             => $value->harga_modal * $value->kuantitas,
// 								'tanggal'             => $skr,
// 								'akun_id'             => $opening_balance, //75,
// 								'produk_id'           => $produk->id,
// 								'item_id'             => $produk->id,
// 								'item_type'           => Produk::class,
// 								'status'              => 0,
// 								'status_rekonsiliasi' => 0,
// 								'sumber'              => 'SALDO AWAL BARANG',
// 								'keterangan'          => 'SALDO-AWAL-BARANG/' . $produk->id,
// 								'dari'                => 'Akun Penyesuaian',
// 								'no_transaksi'        => 'SALDO-AWAL-BARANG/' . $produk->id,
// 								'desc_dev'            => null,
// 								'deleted_at'          => null,
// 								'created_at'          => $skr,
// 								'updated_at'          => $skr,
// 								'created_by'          => 2,
// 								'updated_by'          => 2,
// 								'deleted_by'          => null,
// 							];
// 							$transaksiKreditInsert = new Transaksi();
// 							$transaksiKreditInsert->fill($transaksiKredit);
// 							$transaksiKreditInsert->save();
// 							$transaksiDebit = [
// 								'nominal'             => $value->harga_modal * $value->kuantitas,
// 								'tanggal'             => $skr,
// 								'akun_id'             => $akun_persedian_id, //21,
// 								'produk_id'           => $produk->id,
// 								'item_id'             => $produk->id,
// 								'item_type'           => Produk::class,
// 								'status'              => 1,
// 								'status_rekonsiliasi' => 0,
// 								'sumber'              => 'SALDO AWAL BARANG',
// 								'keterangan'          => 'SALDO-AWAL-BARANG/' . $produk->id,
// 								'dari'                => 'Akun Persediaan',
// 								'no_transaksi'        => 'SALDO-AWAL-BARANG/' . $produk->id,
// 								'desc_dev'            => null,
// 								'deleted_at'          => null,
// 								'created_at'          => $skr,
// 								'updated_at'          => $skr,
// 								'created_by'          => 2,
// 								'updated_by'          => 2,
// 								'deleted_by'          => null,
// 							];
// 							$transaksiDebitInsert = new Transaksi();
// 							$transaksiDebitInsert->fill($transaksiDebit);
// 							$transaksiDebitInsert->save();
// 						// endif;
// // -------------------->
// 						$product_detail_list = [
// 							'no_faktur'      => 'POS-AWAL',
// 							'tanggal'        => $skr,
// 							'kuantitas'      => (null != $value->kuantitas ? $value->kuantitas : 0),
// 							'harga_modal'    => (null != $value->harga_modal ? $value->harga_modal : 0),
// 							'harga_terakhir' => (null != $value->harga_modal ? $value->harga_modal : 0),
// 							'gudang_id'      => null == $value->gudang_id ? 6 : $value->gudang_id,
// 							'transaksi_id'   => ($value->kuantitas > 0 ? $transaksiDebitInsert->id : null),
// 							'produk_id'      => $produk->id,
// 							'sn'             => $value->no_barang,
// 							'status'         => 0,
// 							'item_type'      => get_class($masterPenyesuaianPersediaan),
// 							'item_id'        => $masterPenyesuaianPersediaan->id,
// 							'deleted_at'     => null,
// 							'created_at'     => $skr,
// 							'updated_at'     => $skr,
// 						];
// 						SaldoAwalBarang::insert($product_detail_list);
// 					endif;
// 				}
			// });
			DB::commit();
			return $this->info('Data berhasil di import');
		} catch (Exception $exception) {
			DB::rollback();
			return $this->error($exception->getMessage());
		}
	}
}
