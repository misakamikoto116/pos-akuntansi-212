<?php

namespace App\Console\Commands;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\TipeAkun;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\Transaksi;
use Carbon\Carbon;
// use Excel;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;
use Helpers\ImportAkunHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use Exception;
use App\Imports\AkunImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportAkun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import_akun:nama {nama}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Akun';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            // Set Memorylimit
            ini_set('memory_limit', '1024M');

            // Validasi file import
            $fileImport = $this->argument('nama');
            $path       = file_exists($fileImport) ? $fileImport : storage_path($fileImport);
            if (!file_exists($path)) {
                return $this->error('File tidak ditemukan');
            }
            $this->info("file $path ditemukan...");
            $this->info('mempersiapkan file yang akan diimport');
            $file          = file($path, FILE_SKIP_EMPTY_LINES);
            $countFileLine = count($file);
            if (count($file) < 1) {
                return $this->error('tidak ada data yang di import, maka proses import dihentikan');
            }
            $this->info("ada $countFileLine barang yang akan diimport");
            $this->info('persiapan import barang');

            // $skr                     = date('Y-m-d H:i:s');
            // $akun_preferensi         = PreferensiMataUang::all();
            // $code_generator          = new CodeHelper();
            // $transaksi_helper        = new TransaksiHelper();
            // $import_akun_helper      = new ImportAkunHelper();
            // info inputing data to database
            $this->info("file $path mulai diimport ke database");

            try {
                DB::beginTransaction();
                Excel::import(new AkunImport, $path);
                // Excel::filter('chunk')->load($path)->chunk(500, function ($results) use ($skr, $import_akun_helper) {
                //     foreach ($results as $key => $value) {
                //         if ($value->kode_akun !== null && $value->nama_akun !== null ) {
                //             $tanggal_masuk      = !empty($value->tanggal_masuk) ? Carbon::parse($value->tanggal_masuk)->format('Y-m-d H:i:s') : $skr;
                //             $tipe_akun          = null == $value->tipe_akun_id ? null : TipeAkun::where('title', $value->tipe_akun_id)->first()->id ?? null;
                //             $parent_dari        = null == $value->parent_dari ? null : Akun::where('nama_akun', $value->parent_dari)->first()->id ?? null;
                //             if ($value->parent_dari === null) {
                //                 $save_akun = [
                //                     'kode_akun'         => $value->kode_akun,
                //                     'nama_akun'         => $value->nama_akun,
                //                     'money_function'    => 0,
                //                     'tanggal_masuk'     => $tanggal_masuk ?? null,
                //                     'parent_id'         => $parent_dari,
                //                     'tipe_akun_id'      => $tipe_akun,
                //                     'mata_uang_id'      => $value->mata_uang_id,   
                //                 ];

                //                 $createAkun = Akun::create($save_akun);

                //                 if ($value->nominal !== 0 && $value->nominal !== null) {
                                        
                //                     $import_akun_helper->insertTransaction($createAkun, $value, $skr);

                //                 }

                //             }else {
                //                 $parents = Akun::where('nama_akun', $value->parent_dari)->first()->id ?? null;

                //                 $save_akun = [
                //                     'kode_akun'         => $value->kode_akun,
                //                     'nama_akun'         => $value->nama_akun,
                //                     'money_function'    => 0,
                //                     'tanggal_masuk'     => $tanggal_masuk ?? null,
                //                     'parent_id'         => $parents,
                //                     'tipe_akun_id'      => $tipe_akun,
                //                     'mata_uang_id'      => $value->mata_uang_id,   
                //                 ];

                //                 $createAkun = Akun::create($save_akun);

                //                 if ($value->nominal !== 0 && $value->nominal !== null) {
                                        
                //                     $import_akun_helper->insertTransaction($createAkun, $value, $skr);

                //                 }

                //             }
                //         }
                //     }
                // });
                DB::commit();

                return $this->info('Data berhasil di import');
            } catch (Exception $exception) {
                DB::rollback();

                return $this->error($exception->getMessage());
            }
    }
}
