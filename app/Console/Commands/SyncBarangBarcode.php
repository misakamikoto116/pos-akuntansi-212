<?php

namespace App\Console\Commands;

use App\Modules\Akuntansi\Models\HargaJual;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncBarangBarcode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:barangbarcode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sinkronisasi antara table harga jual dan barang';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
            parent::__construct();
        }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return DB::statement('UPDATE produk SET harga_jual = (SELECT harga_jual FROM harga_jual WHERE produk.no_barang = harga_jual.no_barang), barcode = (SELECT barcode FROM harga_jual WHERE produk.no_barang = harga_jual.no_barang), nama_singkat = (SELECT nama_singkat FROM harga_jual WHERE produk.no_barang = harga_jual.no_barang)');        
    }
}
