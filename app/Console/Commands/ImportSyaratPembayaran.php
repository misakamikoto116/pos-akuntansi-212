<?php

namespace App\Console\Commands;

use Exception;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

use App\Imports\SyaratPembayaranImport;
use App\Modules\Akuntansi\Models\SyaratPembayaran;

class ImportSyaratPembayaran extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import_syarat_pembayaran:nama {nama}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Syarat Pembayaran';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            // Set Memorylimit
            ini_set('memory_limit', '1024M');

            // Validasi file import
            $fileImport = $this->argument('nama');
            $path       = file_exists($fileImport) ? $fileImport : storage_path($fileImport);
            if (!file_exists($path)) {
                return $this->error('File tidak ditemukan');
            }
            $this->info("file $path ditemukan...");
            $this->info('mempersiapkan file yang akan diimport');
            $file          = file($path, FILE_SKIP_EMPTY_LINES);
            $countFileLine = count($file);
            if (count($file) < 1) {
                return $this->error('tidak ada data yang di import, maka proses import dihentikan');
            }
            $this->info("ada $countFileLine barang yang akan diimport");
            $this->info('persiapan import barang');

            $skr                     = date('Y-m-d H:i:s');
            // info inputing data to database
            $this->info("file $path mulai diimport ke database");

            try {
                DB::beginTransaction();
                Excel::import(new SyaratPembayaranImport, $path);
                // Excel::filter('chunk')->load($path)->chunk(500, function ($results) use ($skr) {

                //     $nama                   = [];

                //     foreach ($results as $key => $value) {
                //         if ($value->nama !== null) {
                //             $nama[] = [
                //                 'nama'                          => $value->nama,
                //                 'akan_dapat_diskon'             => $value->discount,
                //                 'jika_membayar_antara'          => $value->disc_day,
                //                 'jatuh_tempo'                   => $value->net_days,
                //                 'created_at'                    => $skr,
                //                 'updated_at'                    => $skr,
                //             ];
                //         }
                //     }

                //     SyaratPembayaran::insert($nama);
                
                // });

                DB::commit();

                return $this->info('Data berhasil di import');
            } catch (Exception $exception) {
                DB::rollback();

                return $this->error($exception->getMessage());
            }
    }
}
