<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Excel;

use App\Imports\ProdukWithoutAccountImport;

class ImportByAkun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import_produk_by_akun:nama {nama} 
                                                       {akun_persediaan_id} 
                                                       {akun_penjualan_id} 
                                                       {akun_retur_penjualan_id}
                                                       {akun_diskon_barang_id}
                                                       {akun_barang_terkirim_id}
                                                       {akun_hpp_id}
                                                       {akun_retur_pembelian_id}
                                                       {akun_beban_id}
                                                       {akun_belum_tertagih_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Produk By Akun';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            // Set Memorylimit
            ini_set('memory_limit', '1024M');

            // Validasi file import
            $fileImport = $this->argument('nama');
            $path       = file_exists($fileImport) ? $fileImport : storage_path($fileImport);
            if (!file_exists($path)) {
                return $this->error('File tidak ditemukan');
            }
            $this->info("file $path ditemukan...");
            $this->info('mempersiapkan file yang akan diimport');
            $file          = file($path, FILE_SKIP_EMPTY_LINES);
            $countFileLine = count($file);
            if (count($file) < 1) {
                return $this->error('tidak ada data yang di import, maka proses import dihentikan');
            }
            $this->info("ada $countFileLine barang yang akan diimport");
            $this->info('persiapan import barang');

            $pureIt = [
                'skr'                     => date('Y-m-d H:i:s'),
                'akun_beban_id'           => $this->argument('akun_beban_id'),
                'akun_hpp_id'             => $this->argument('akun_hpp_id'),
                'akun_ret_penjualan_id'   => $this->argument('akun_retur_penjualan_id'),
                'akun_disk_penjualan_id'  => $this->argument('akun_diskon_barang_id'),
                'akun_persedian_id'       => $this->argument('akun_persediaan_id'),
                'akun_penjualan_id'       => $this->argument('akun_penjualan_id'),
                'akun_barang_terkirim_id' => $this->argument('akun_barang_terkirim_id'),
                'akun_ret_pembelian_id'   => $this->argument('akun_retur_pembelian_id'),
                'akun_belum_tertagih_id'  => $this->argument('akun_belum_tertagih_id'),
            ];

            // info inputing data to database
            $this->info("file $path mulai diimport ke database");

            try {
                DB::beginTransaction();
                Excel::import(new ProdukWithoutAccountImport($pureIt), $path);
                // Excel::filter('chunk')->load($path)->chunk(500, function ($results) use (
                // $akun_hpp_id,
                // $akun_beban_id,
                // $akun_ret_penjualan_id,
                // $akun_disk_penjualan_id,
                // $akun_persedian_id,
                // $akun_penjualan_id,
                // $akun_barang_terkirim_id,
                // $akun_ret_pembelian_id,
                // $akun_belum_tertagih_id,
                // $skr) {
                    // foreach ($results as $key => $value) {
                    //     if ($value->no_barang !== null && $value->keterangan !== null && $value->kategori_produk_id !== null):
                    //     $product_master_list = [
                    //         'tipe_barang'             => 0,
                    //         'parent_id'               => null,
                    //         'no_barang'               => $value->no_barang,
                    //         'status'                  => 1,
                    //         'tipe_persedian'          => $value->tipe_persedian,
                    //         'keterangan'              => $value->keterangan,
                    //         'harga_standar_def'       => $value->harga_standar_def,
                    //         'kuantitas'               => $value->kuantitas,
                    //         'harga_jual'              => $value->harga_jual,
                    //         'unit'                    => $value->unit,
                    //         'status_umkm'             => $value->status_umkm,
                    //         'gudang_id'               => 6,
                    //         'kategori_produk_id'      => (int) $value->kategori_produk_id ?? 12,
                    //         'pemasok_id'              => null, //$value->pemasok,
                    //         'min_jumlah_reorder'      => $value->min_jumlah_reorder,
                    //         'kode_pajak_perse_id'     => 'NULL' == (int) $value->kode_pajak_perse_id ? null : $value->kode_pajak_perse_id,
                    //         'catatan'                 => $value->catatan,
                    //         'akun_beban_id'           => (null == $akun_beban_id ? null : $akun_beban_id),
                    //         'akun_hpp_id'             => (null == $akun_hpp_id ? null : $akun_hpp_id), //$value->akun_hpp_id,
                    //         'akun_ret_penjualan_id'   => (null == $akun_ret_penjualan_id ? null : $akun_ret_penjualan_id), //86,
                    //         'akun_disk_penjualan_id'  => (null == $akun_disk_penjualan_id ? null : $akun_disk_penjualan_id), //92,
                    //         'akun_persedian_id'       => (null == $akun_persedian_id ? null : $akun_persedian_id),
                    //         'akun_penjualan_id'       => (null == $akun_penjualan_id ? null : $akun_penjualan_id), //77,
                    //         'akun_barang_terkirim_id' => (null == $akun_barang_terkirim_id ? null : $akun_barang_terkirim_id), //28,
                    //         'akun_ret_pembelian_id'   => (null == $akun_ret_pembelian_id ? null : $akun_ret_pembelian_id), //21, //$value->akun_ret_pembelian_id,
                    //         'akun_belum_tertagih_id'  => (null == $akun_belum_tertagih_id ? null : $akun_belum_tertagih_id), //68, // $value->akun_belum_tertagih_id,
                    //         'deleted_at'              => null,
                    //         'diskon'                  => null,
                    //         'created_at'              => $skr,
                    //     ];
                    //     $produk = new Produk();
                    //     $saveMasterProduk = $produk->fill($product_master_list);
                    //     $saveMasterProduk->save();

                    //     if ($value->kuantitas > 0) :
                    //         $x = 0;
                    //     $penyesuaian_persediaan = [
                    //         'no_penyesuaian'    => 'SALDO-AWAL-BARANG/IMPORT/ '.$skr.'/ '.$saveMasterProduk->id,
                    //         'tgl_penyesuaian'   => $skr,
                    //         'kode_akun_id'      => 21,
                    //         'keterangan_produk' => $value->catatan,
                    //         'akun_penyesuaian'  => 75,
                    //         'keterangan'        => 'IMPORT CSV',
                    //         'created_at'        => $skr,
                    //         'updated_at'        => $skr,
                    //         'deleted_at'        => null,
                    //         'status'            => 0,
                    //     ];

                    //     $masterPenyesuaianPersediaan = new PenyesuaianPersediaan();
                    //     $masterPenyesuaianPersediaan->fill($penyesuaian_persediaan)->save();
                    //     $detail_penyesuaian = [
                    //         'penyesuaian_persediaan_id' => $masterPenyesuaianPersediaan->id, //detail dari $penyesuaian_persediaan
                    //         'produk_id'                 => $produk->id,
                    //         'keterangan_produk'         => $value->keterangan,
                    //         'satuan_produk'             => $value->unit,
                    //         'qty_produk'                => 0,
                    //         'qty_baru'                  => $value->kuantitas,
                    //         'nilai_skrg'                => 0,
                    //         'nilai_baru'                => $value->harga_modal,
                    //         'departemen'                => null,
                    //         'gudang_id'                 => 6,
                    //         'proyek'                    => null,
                    //         'serial_number'             => $value->no_barang,
                    //         'created_at'                => $skr,
                    //         'updated_at'                => $skr,
                    //         'deleted_at'                => null,
                    //     ];
                    //     $detailPenyesuaian = new DetailPenyesuaianPersediaan();
                    //     $detailPenyesuaian->insert($detail_penyesuaian);
                    //     $transaksiKredit = [
                    //         //'id' => $i++,
                    //         'nominal'             => $value->harga_modal * $value->kuantitas,
                    //         'tanggal'             => $skr,
                    //         'akun_id'             => 75,
                    //         'produk_id'           => $produk->id,
                    //         'item_id'             => $produk->id,
                    //         'item_type'           => Produk::class,
                    //         'status'              => 0,
                    //         'status_rekonsiliasi' => 0,
                    //         'sumber'              => 'SALDO AWAL BARANG',
                    //         'keterangan'          => 'SALDO-AWAL-BARANG/' . $produk->id,
                    //         'dari'                => 'Akun Penyesuaian',
                    //         'no_transaksi'        => 'SALDO-AWAL-BARANG/' . $produk->id,
                    //         'desc_dev'            => null,
                    //         'deleted_at'          => null,
                    //         'created_at'          => $skr,
                    //         'updated_at'          => $skr,
                    //         'created_by'          => 2, //krn dari console should be auth()->user()->id,
                    //         'updated_by'          => 2, //krn dari console should be auth()->user()->id,
                    //         'deleted_by'          => null,
                    //     ];
                    //     //dd($value->harga_modal * $value->kuantitas);
                    //     $transaksiKreditInsert = new Transaksi();
                    //     $transaksiKreditInsert->fill($transaksiKredit);
                    //     $transaksiKreditInsert->save();
                    //     $transaksiDebit = [
                    //         'nominal'             => $value->harga_modal * $value->kuantitas,
                    //         'tanggal'             => $skr,
                    //         'akun_id'             => 21,
                    //         'produk_id'           => $produk->id,
                    //         'item_id'             => $produk->id,
                    //         'item_type'           => Produk::class,
                    //         'status'              => 1,
                    //         'status_rekonsiliasi' => 0,
                    //         'sumber'              => 'SALDO AWAL BARANG',
                    //         'keterangan'          => 'SALDO-AWAL-BARANG/' . $produk->id,
                    //         'dari'                => 'Akun Persediaan',
                    //         'no_transaksi'        => 'SALDO-AWAL-BARANG/' . $produk->id,
                    //         'desc_dev'            => null,
                    //         'deleted_at'          => null,
                    //         'created_at'          => $skr,
                    //         'updated_at'          => $skr,
                    //         'created_by'          => 2, //krn dari console
                    //         'updated_by'          => 2, //krn dari console
                    //         'deleted_by'          => null,
                    //     ];
                    //     $transaksiDebitInsert = new Transaksi();
                    //     $transaksiDebitInsert->fill($transaksiDebit);
                    //     $transaksiDebitInsert->save();
                    //     endif;

                    //     $product_detail_list = [
                    //         'no_faktur'      => 'POS-AWAL',
                    //         'tanggal'        => $skr,
                    //         'kuantitas'      => (null != $value->kuantitas ? $value->kuantitas : 0),
                    //         'harga_modal'    => (null != $value->harga_modal ? $value->harga_modal : 0),
                    //         'harga_terakhir' => (null != $value->harga_modal ? $value->harga_modal : 0),
                    //         'gudang_id'      => 6,
                    //         'transaksi_id'   => ($value->kuantitas > 0 ? $transaksiDebitInsert->id : null),
                    //         'produk_id'      => $produk->id,
                    //         'sn'             => $value->no_barang,
                    //         'status'         => 0,
                    //         'item_type'      => get_class($masterPenyesuaianPersediaan), //jika
                    //         'item_id'        => $masterPenyesuaianPersediaan->id, //harus dari transasksi
                    //         'deleted_at'     => null,
                    //         'created_at'     => $skr,
                    //         'updated_at'     => $skr,
                    //     ];
                    //     SaldoAwalBarang::insert($product_detail_list);
                    //     endif;
                    // }
                // });
                DB::commit();

                return $this->info('Data berhasil di import');
            } catch (Exception $exception) {
                DB::rollback();

                return $this->error($exception->getMessage());
            }
    }
}
