<?php

namespace App\Console\Commands;

use App\Modules\Akuntansi\Models\HargaJual;
use App\Imports\BarcodeImport;
use Carbon\Carbon;
use Excel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportBarcode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import_barcode:nama {nama}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Barcode';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            // Set Memorylimit
            ini_set('memory_limit', '1024M');

            // Validasi file import
            $fileImport = $this->argument('nama');
            $path       = file_exists($fileImport) ? $fileImport : storage_path($fileImport);
            if (!file_exists($path)) {
                return $this->error('File tidak ditemukan');
            }
            $this->info("file $path ditemukan...");
            $this->info('mempersiapkan file yang akan diimport');
            $file          = file($path, FILE_SKIP_EMPTY_LINES);
            $countFileLine = count($file);
            if (count($file) < 1) {
                return $this->error('tidak ada data yang di import, maka proses import dihentikan');
            }
            $this->info("ada $countFileLine barang yang akan diimport");
            $this->info('persiapan import barang');

            // $skr                     = date('Y-m-d H:i:s');
            // info inputing data to database
            $this->info("file $path mulai diimport ke database");

            try {
                DB::beginTransaction();
                Excel::import(new BarcodeImport, $path);
                // Excel::filter('chunk')->load($path)->chunk(500, function ($results) use ($skr) {

                //     $nama                   = [];

                //     foreach ($results as $key => $value) {
                //         if ($value->no_barang !== null) {
                //             $nama[] = [
                //                 'no_barang'                             => $value->no_barang,
                //                 'barcode'                               => $value->barcode,
                //                 'keterangan'                            => $value->keterangan,
                //                 'harga_jual'                            => $value->harga_jual,
                //                 'nama_singkat'                          => $value->nama_singkat,
                //                 'created_at'                            => $skr,
                //                 'updated_at'                            => $skr,
                //             ];
                //         }
                //     }

                //     HargaJual::insert($nama);
                
                // });

                DB::commit();

                return $this->info('Data berhasil di import');
            } catch (Exception $exception) {
                DB::rollback();

                return $this->error($exception->getMessage());
            }
    }
}
