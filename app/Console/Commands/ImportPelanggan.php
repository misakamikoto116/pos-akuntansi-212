<?php

namespace App\Console\Commands;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\DetailInformasiPelanggan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Transaksi;
use Carbon\Carbon;
use App\Imports\PelangganImport;
use Maatwebsite\Excel\Facades\Excel;
// use Excel;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportPelanggan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import_pelanggan:nama {nama}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Pelanggan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            // Set Memorylimit
            ini_set('memory_limit', '1024M');

            // Validasi file import
            $fileImport = $this->argument('nama');
            $path       = file_exists($fileImport) ? $fileImport : storage_path($fileImport);
            if (!file_exists($path)) {
                return $this->error('File tidak ditemukan');
            }
            $this->info("file $path ditemukan...");
            $this->info('mempersiapkan file yang akan diimport');
            $file          = file($path, FILE_SKIP_EMPTY_LINES);
            $countFileLine = count($file);
            if (count($file) < 1) {
                return $this->error('tidak ada data yang di import, maka proses import dihentikan');
            }
            $this->info("ada $countFileLine barang yang akan diimport");
            $this->info('persiapan import barang');

            // $skr                     = date('Y-m-d H:i:s');
            // $saldo_awal_produk       = Produk::where('tipe_barang', 5)->first();
            // $akun_preferensi         = PreferensiMataUang::all();
            // $akun_balance            = Akun::where('nama_akun', 'Opening Balance Equity')->pluck('id');
            // $code_generator          = new CodeHelper();
            // $transaksi_helper        = new TransaksiHelper();
            // info inputing data to database
            $this->info("file $path mulai diimport ke database");

            try {
                DB::beginTransaction();
                Excel::import(new PelangganImport, $path);
            //     Excel::filter('chunk')->load($path)->chunk(500, function ($results) use ($skr, $saldo_awal_produk, $akun_preferensi, $akun_balance, $code_generator, $transaksi_helper) {

            //         $arrSaldo                   = [];
            //         $arrItemSaldoAwalPurpose    = [];

            //         foreach ($results as $key => $value) {
            //             if ($value->nama !== null && $value->no_pelanggan !== null ) {
            //                 $pelanggan_master_list = [
            //                     'nama'             => $value->nama,
            //                     'no_pelanggan'     => $value->no_pelanggan,
            //                     'alamat'           => $value->alamat,
            //                     'telepon'          => $value->telepon,
            //                 ];
            //                 $pelanggan              = new InformasiPelanggan();
            //                 $saveMasterPelanggan    = $pelanggan->fill($pelanggan_master_list);
            //                 $saveMasterPelanggan->save();

            //                     $saldo_awal         = str_replace(['Rp ', '.00', ','], '', $value->saldo_awal);

            //                     if ($saldo_awal != 0) {
            //                         // overriding
            //                         $tanggal_saldo      = !empty($value->tanggal) ? Carbon::parse($value->tanggal)->format('Y-m-d H:i:s') : $skr;
            //                         $no_faktur          = $code_generator->generateNoFakturPelangganImport($value);

            //                         $arrSaldoAwalPurpose = [
            //                             'no_faktur'         => $no_faktur,
            //                             'pelanggan_id'      => $saveMasterPelanggan->id,
            //                             'form_no'           => '0',
            //                             'invoice_date'      => $tanggal_saldo,
            //                             'no_fp_std'         => '.',
            //                             'no_fp_std2'        => '.',
            //                             'no_fp_std_date'    => '.',
            //                             'keterangan'        => 'Saldo Awal',
            //                             'uang_muka'         => '0',
            //                             'total'             => $saldo_awal,
            //                             'term_id'           => $value->syarat_pembayaran_id ?? null,
            //                         ];

            //                         $fakturPenjualan = FakturPenjualan::create($arrSaldoAwalPurpose);

            //                         $arrSaldo[] = [
            //                             'no_faktur'                 => $no_faktur,
            //                             'tanggal'                   => $tanggal_saldo,
            //                             'saldo_awal'                => $saldo_awal,
            //                             'syarat_pembayaran_id'      => $value->syarat_pembayaran_id ?? null,
            //                             'informasi_pelanggan_id'    => $saveMasterPelanggan->id,
            //                             'created_at'                => $skr,
            //                             'updated_at'                => $skr,
            //                         ];

            //                         $arrItemSaldoAwalPurpose[] = [
            //                             'produk_id'             => $saldo_awal_produk->id,
            //                             'faktur_penjualan_id'   => $fakturPenjualan->id,
            //                             'item_deskripsi'        => $saldo_awal_produk->keterangan,
            //                             'item_unit'             => $saldo_awal_produk->unit,
            //                             'jumlah'                => $saldo_awal_produk->kuantitas,
            //                             'unit_price'            => $saldo_awal,
            //                             'diskon'                => $saldo_awal_produk->diskon,
            //                             'harga_modal'           => $saldo_awal,
            //                             'harga_final'           => $saldo_awal,
            //                             'created_at'            => $skr,
            //                             'updated_at'            => $skr,
            //                         ];

            //                         $transaksiDebit = [
            //                             'nominal'       => $saldo_awal,
            //                             'tanggal'       => $tanggal_saldo,
            //                             'dari'          => 'Saldo Awal per Item '.$key,
            //                             'id'            => $no_faktur,
            //                             'akun'          => $akun_preferensi->first()->akun_piutang_id,
            //                             'item_id'       => $fakturPenjualan->id,
            //                         ];
            //                         $transaksiKredit = [
            //                             'nominal'       => $saldo_awal,
            //                             'tanggal'       => $tanggal_saldo,
            //                             'dari'          => 'Saldo Awal per Item balance '.$key,
            //                             'id'            => $no_faktur,
            //                             'akun'          => $akun_balance[0],
            //                             'item_id'       => $fakturPenjualan->id,
            //                         ];

            //                         $transaksi_helper->ExecuteTransactionSingle('insert', $fakturPenjualan, $transaksiKredit); //kredit
            //                         $transaksi_helper->ExecuteTransactionSingle('insert', $fakturPenjualan, $transaksiDebit, 1); //debit
            //                     }
            //             }
            //         }

            //         DetailInformasiPelanggan::insert($arrSaldo);
            //         BarangFakturPenjualan::insert($arrItemSaldoAwalPurpose);
            //     });
                DB::commit();

                return $this->info('Data berhasil di import');
            } catch (Exception $exception) {
                DB::rollback();

                return $this->error($exception->getMessage());
            }
    }
}
