<?php

namespace App\Providers;

use Generator\Interfaces\RepositoryInterface;
use App\Http\Controllers\Api\Pelanggan\Anggota;
use App\Http\Controllers\Api\Pos\PosController;
use App\Http\Controllers\Api\Pos\PosRepository;
use App\Http\Controllers\Api\KasirCheckIn\KasirCheckInController;
use App\Http\Controllers\Api\KasirCheckIn\KasirCheckInRepository;
use App\Http\Controllers\Api\KasirCheckOut\KasirCheckOutController;
use App\Http\Controllers\Api\KasirCheckOut\KasirCheckOutRepository;
use App\Modules\Admin\Repositories\RoleRepository;
use App\Modules\Admin\Repositories\UserRepository;
use App\Http\Controllers\Api\Produk\ProdukController;
use App\Http\Controllers\Api\Produk\ProdukRepository;
use App\Modules\Admin\Http\Controllers\RoleController;
use App\Modules\Admin\Http\Controllers\UserController;
use App\Modules\Akuntansi\Repositories\AkunRepository;
use App\Modules\Akuntansi\Repositories\IklanRepository;
use App\Modules\Pos\Repositories\PenjualanRepositories;
use Generator\Providers\InterfaceBinderServiceProvider;
use App\Modules\Admin\Repositories\PermissionRepository;
use App\Modules\Akuntansi\Repositories\BarangRepository;
use App\Modules\Akuntansi\Repositories\GudangRepository;
use App\Modules\Akuntansi\Repositories\CartLogRepository;
use App\Modules\Akuntansi\Repositories\PromosiRepository;
use App\Modules\Pos\Http\Controllers\PenjualanController;
use App\Modules\Akuntansi\Http\Controllers\AkunController;
use App\Modules\Akuntansi\Repositories\MataUangRepository;
use App\Modules\Akuntansi\Repositories\TipeAkunRepository;
use App\Modules\Gudang\Repositories\DashboardRepositories;
use App\Http\Controllers\Api\Penjualan\PenjualanRepository;
use App\Modules\Akuntansi\Repositories\IdentitasRepository;
use App\Modules\Akuntansi\Repositories\KodePajakRepository;
use App\Modules\Akuntansi\Repositories\SaldoAkunRepository;
use App\Modules\Admin\Http\Controllers\PermissionController;
use App\Modules\Akuntansi\Http\Controllers\BarangController;
use App\Modules\Akuntansi\Http\Controllers\GudangController;
use App\Modules\Akuntansi\Repositories\JurnalUmumRepository;
use App\Modules\Akuntansi\Repositories\NomorPajakRepository;
use App\Modules\Akuntansi\Repositories\PembayaranRepository;
use App\Modules\Akuntansi\Repositories\PenerimaanRepository;
use App\Modules\Akuntansi\Repositories\PreferensiRepository;
use App\Modules\Gudang\Http\Controllers\DashboardController;
use App\Modules\Akuntansi\Http\Controllers\CartLogController;
use App\Modules\Akuntansi\Http\Controllers\PromosiController;
use App\Modules\Akuntansi\Repositories\GroupBarangRepository;
use App\Modules\Gudang\Repositories\DaftarGudangRepositories;
use App\Http\Controllers\Api\Penjualan\APIPenjualanController;
use App\Modules\Akuntansi\Http\Controllers\MataUangController;
use App\Modules\Akuntansi\Http\Controllers\TipeAkunController;
use App\Modules\Akuntansi\Repositories\DataKaryawanRepository;
use App\Modules\Akuntansi\Repositories\DisplayIklanRepository;
use App\Modules\Akuntansi\Repositories\SerialNumberRepository;
use App\Modules\Akuntansi\Http\Controllers\IdentitasController;
use App\Modules\Akuntansi\Http\Controllers\KodePajakController;
use App\Modules\Akuntansi\Http\Controllers\SaldoAkunController;
use App\Modules\Akuntansi\Repositories\PemberitahuanRepository;
use App\Modules\Akuntansi\Repositories\TipePelangganRepository;
use App\Modules\Gudang\Http\Controllers\DaftarGudangController;
use App\Modules\Gudang\Repositories\KategoriProdukRepositories;
use App\Modules\Akuntansi\Http\Controllers\JurnalUmumController;
use App\Modules\Akuntansi\Http\Controllers\NomorPajakController;
use App\Modules\Akuntansi\Http\Controllers\PembayaranController;
use App\Modules\Akuntansi\Http\Controllers\TypeEdcController;

// POS
use App\Modules\Akuntansi\Http\Controllers\PenerimaanController;
use App\Modules\Akuntansi\Http\Controllers\PreferensiController;
use App\Modules\Akuntansi\Repositories\DataPerusahaanRepository;

// Gudang
use App\Modules\Akuntansi\Repositories\JasaPengirimanRepository;
use App\Modules\Akuntansi\Repositories\KategoriProdukRepository;
use App\Modules\Akuntansi\Repositories\RekapTransaksiRepository;
use App\Modules\Akuntansi\Repositories\ReturPembelianRepository;
use App\Modules\Akuntansi\Repositories\ReturPenjualanRepository;

use App\Modules\Akuntansi\Http\Controllers\GroupBarangController;
use App\Modules\Akuntansi\Repositories\BarangPergudangRepository;

// Pemisah
use App\Modules\Akuntansi\Repositories\FakturPembelianRepository;
use App\Modules\Akuntansi\Repositories\FakturPenjualanRepository;
use App\Modules\Akuntansi\Repositories\PreferensiPosRepositories;
use App\Modules\Akuntansi\Repositories\TipeAktivaTetapRepository;
use App\Modules\Akuntansi\Repositories\UangMukaPemasokRepository;
use App\Modules\Pos\Repositories\PesananPenjualanPosRepositories;
use App\Modules\Akuntansi\Http\Controllers\DataKaryawanController;
use App\Modules\Akuntansi\Http\Controllers\DisplayIklanController;
use App\Modules\Akuntansi\Http\Controllers\SerialNumberController;
use App\Modules\Akuntansi\Repositories\HistoryBukuBesarRepository;
use App\Modules\Akuntansi\Repositories\InformasiPemasokRepository;
use App\Modules\Akuntansi\Repositories\PemindahanBarangRepository;
use App\Modules\Akuntansi\Repositories\PenerimaanBarangRepository;
use App\Modules\Akuntansi\Repositories\PesananPembelianRepository;
use App\Modules\Akuntansi\Repositories\PesananPenjualanRepository;
use App\Modules\Akuntansi\Repositories\RekonsiliasiBankRepository;
use App\Modules\Akuntansi\Repositories\SyaratPembayaranRepository;
use App\Modules\Akuntansi\Http\Controllers\PemberitahuanController;
use App\Modules\Akuntansi\Http\Controllers\PreferensiPosController;
use App\Modules\Akuntansi\Http\Controllers\TipePelangganController;
use App\Modules\Akuntansi\Repositories\DaftarAktivaTetapRepository;
use App\Modules\Akuntansi\Repositories\PembiayaanPesananRepository;
use App\Modules\Akuntansi\Repositories\UangMukaPelangganRepository;
use App\Modules\Pos\Http\Controllers\PesananPenjualanPosController;
use App\Modules\Akuntansi\Http\Controllers\DataPerusahaanController;
use App\Modules\Akuntansi\Http\Controllers\JasaPengirimanController;
use App\Modules\Akuntansi\Http\Controllers\KategoriProdukController;
use App\Modules\Akuntansi\Http\Controllers\RekapTransaksiController;
use App\Modules\Akuntansi\Http\Controllers\ReturPembelianController;
use App\Modules\Akuntansi\Http\Controllers\ReturPenjualanController;
use App\Modules\Akuntansi\Repositories\InformasiPelangganRepository;
use App\Modules\Akuntansi\Repositories\PenawaranPenjualanRepository;
use App\Modules\Akuntansi\Repositories\PenyesuaianPesananRepository;
use App\Modules\Akuntansi\Http\Controllers\BarangPergudangController;
use App\Modules\Akuntansi\Http\Controllers\FakturPembelianController;
use App\Modules\Akuntansi\Http\Controllers\FakturPenjualanController;
use App\Modules\Akuntansi\Http\Controllers\TipeAktivaTetapController;
use App\Modules\Akuntansi\Http\Controllers\UangMukaPemasokController;
use App\Modules\Akuntansi\Repositories\PembayaranPembelianRepository;
use App\Modules\Akuntansi\Repositories\PenerimaanPenjualanRepository;
use App\Modules\Akuntansi\Repositories\PengirimanPenjualanRepository;
use App\Modules\Akuntansi\Repositories\PenyelesaianPesananRepository;
use App\Modules\Akuntansi\Repositories\PermintaanPembelianRepository;
use App\Modules\Akuntansi\Http\Controllers\HistoryBukuBesarController;
use App\Modules\Akuntansi\Http\Controllers\InformasiPemasokController;
use App\Modules\Akuntansi\Http\Controllers\PemindahanBarangController;
use App\Modules\Akuntansi\Http\Controllers\PenerimaanBarangController;
use App\Modules\Akuntansi\Http\Controllers\PesananPembelianController;
use App\Modules\Akuntansi\Http\Controllers\PesananPenjualanController;
use App\Modules\Akuntansi\Http\Controllers\RekonsiliasiBankController;
use App\Modules\Akuntansi\Http\Controllers\SyaratPembayaranController;
use App\Modules\Akuntansi\Repositories\PenyesuaianHargaJualRepository;
use App\Modules\Akuntansi\Repositories\TipeAktivaTetapPajakRepository;
use App\Modules\Akuntansi\Repositories\TypeEdcRepository;
use App\Modules\Akuntansi\Http\Controllers\DaftarAktivaTetapController;
use App\Modules\Akuntansi\Http\Controllers\PembiayaanPesananController;
use App\Modules\Akuntansi\Http\Controllers\UangMukaPelangganController;

use App\Modules\Akuntansi\Repositories\PenyesuaianPersediaanRepository;
use App\Modules\Gudang\Repositories\PemindahanBarangGudangRepositories;
use App\Modules\Akuntansi\Http\Controllers\InformasiPelangganController;
// POS
use App\Modules\Akuntansi\Http\Controllers\PenawaranPenjualanController;
use App\Modules\Akuntansi\Http\Controllers\PenyesuaianPesananController;

// Gudang
use App\Modules\Akuntansi\Http\Controllers\PembayaranPembelianController;
use App\Modules\Akuntansi\Http\Controllers\PenerimaanPenjualanController;
use App\Modules\Akuntansi\Http\Controllers\PengirimanPenjualanController;
use App\Modules\Akuntansi\Http\Controllers\PenyelesaianPesananController;
use App\Modules\Akuntansi\Http\Controllers\PermintaanPembelianController;

use App\Modules\Gudang\Http\Controllers\PemindahanBarangGudangController;
use App\Modules\Akuntansi\Http\Controllers\PenyesuaianHargaJualController;
//API
use App\Modules\Akuntansi\Http\Controllers\TipeAktivaTetapPajakController;

use App\Modules\Akuntansi\Http\Controllers\PenyesuaianPersediaanController;
use App\Modules\Gudang\Http\Controllers\BarangPergudangController as BarangPergudang;
use App\Modules\Gudang\Repositories\BarangPergudangRepositories as BarangPergudangRepositories;
use App\Modules\Gudang\Http\Controllers\KategoriProdukController as KategoriProdukGudangController;

//POS RETUR
use App\Modules\Pos\Repositories\ReturRepositories;
use App\Modules\Pos\Http\Controllers\ReturController;

// PENGAWAS
use App\Modules\Pengawas\Repositories\PengawasRepository;
use App\Modules\Pengawas\Http\Controllers\PengawasController;


class RepositoryServiceProvider extends InterfaceBinderServiceProvider
{           
    public function bindingItems()
    {
        return [RepositoryInterface::class => [
            AkunController::class => AkunRepository::class,
            MataUangController::class => MataUangRepository::class,
            TipeAkunController::class => TipeAkunRepository::class,
            DataPerusahaanController::class => DataPerusahaanRepository::class,
            PembayaranController::class => PembayaranRepository::class,
            PenerimaanController::class => PenerimaanRepository::class,
            JurnalUmumController::class => JurnalUmumRepository::class,
            IdentitasController::class => IdentitasRepository::class,
            SyaratPembayaranController::class => SyaratPembayaranRepository::class,
            InformasiPelangganController::class => InformasiPelangganRepository::class,
            NomorPajakController::class => NomorPajakRepository::class,
            TipePelangganController::class => TipePelangganRepository::class,
            KodePajakController::class => KodePajakRepository::class,
            JasaPengirimanController::class => JasaPengirimanRepository::class,
            TipeAktivaTetapPajakController::class => TipeAktivaTetapPajakRepository::class,
            TipeAktivaTetapController::class => TipeAktivaTetapRepository::class,
            GudangController::class => GudangRepository::class,
            KategoriProdukController::class => KategoriProdukRepository::class,
            InformasiPemasokController::class => InformasiPemasokRepository::class,
            BarangController::class => BarangRepository::class,
            PenawaranPenjualanController::class => PenawaranPenjualanRepository::class,
            PesananPenjualanController::class => PesananPenjualanRepository::class,
            PengirimanPenjualanController::class => PengirimanPenjualanRepository::class,
            FakturPenjualanController::class => FakturPenjualanRepository::class,
            PenerimaanPenjualanController::class => PenerimaanPenjualanRepository::class,
            PermintaanPembelianController::class => PermintaanPembelianRepository::class,
            PesananPembelianController::class => PesananPembelianRepository::class,
            PenerimaanBarangController::class => PenerimaanBarangRepository::class,
            FakturPembelianController::class => FakturPembelianRepository::class,
            ReturPenjualanController::class => ReturPenjualanRepository::class,
            UangMukaPelangganController::class => UangMukaPelangganRepository::class,
            PembayaranPembelianController::class => PembayaranPembelianRepository::class,
            UangMukaPemasokController::class => UangMukaPemasokRepository::class,
            ReturPembelianController::class => ReturPembelianRepository::class,
            CartLogController::class => CartLogRepository::class,
            TypeEdcController::class => TypeEdcRepository::class,

            SerialNumberController::class => SerialNumberRepository::class,
            // PenyesuaianPesananController::class => PenyesuaianPesananRepository::class,
            PenyesuaianPersediaanController::class => PenyesuaianPersediaanRepository::class,
            BarangPergudangController::class => BarangPergudangRepository::class,
            PemindahanBarangController::class => PemindahanBarangRepository::class,
            PenyesuaianHargaJualController::class => PenyesuaianHargaJualRepository::class,
            DaftarAktivaTetapController::class => DaftarAktivaTetapRepository::class,
            PreferensiController::class => PreferensiRepository::class,
            PembiayaanPesananController::class => PembiayaanPesananRepository::class,
            PenyelesaianPesananController::class => PenyelesaianPesananRepository::class,
            HistoryBukuBesarController::class => HistoryBukuBesarRepository::class,
            SaldoAkunController::class => SaldoAkunRepository::class,

            DataKaryawanController::class => DataKaryawanRepository::class,
            GroupBarangController::class => GroupBarangRepository::class,
            RekonsiliasiBankController::class => RekonsiliasiBankRepository::class,

            // Gudang
            DashboardController::class => DashboardRepositories::class,
            DaftarGudangController::class => DaftarGudangRepositories::class,
            BarangPergudang::class => BarangPergudangRepositories::class,
            KategoriProdukGudangController::class => KategoriProdukRepositories::class,
            PemindahanBarangGudangController::class => PemindahanBarangGudangRepositories::class,

            UserController::class => UserRepository::class,
            PenjualanController::class => PenjualanRepositories::class,
            PreferensiPosController::class => PreferensiPosRepositories::class,
            RoleController::class => RoleRepository::class,
            PermissionController::class => PermissionRepository::class,
            PromosiController::class => PromosiRepository::class,
            PemberitahuanController::class => PemberitahuanRepository::class,
            PesananPenjualanPosController::class => PesananPenjualanPosRepositories::class,

            DisplayIklanController::class => DisplayIklanRepository::class,
            ReturController::class => ReturRepositories::class,

            // Pengawas
            PengawasController::class => PengawasRepository::class,

            //API
            ProdukController::class => ProdukRepository::class,
            PosController::class => PosRepository::class,
            RekapTransaksiController::class => RekapTransaksiRepository::class,
            Anggota::class => InformasiPelangganRepository::class,
            ApiPenjualanController::class => PenjualanRepository::class,
            KasirCheckInController::class => KasirCheckInRepository::class,
            

        ]];
    }
}
