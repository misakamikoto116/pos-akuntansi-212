<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\DetailInformasiPelanggan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Transaksi;
use DB;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;

class PelangganImport implements WithHeadingRow, ToCollection, WithChunkReading
{
    public function __construct() {
        $this->skr                     = date('Y-m-d H:i:s');
        $this->saldo_awal_produk       = Produk::where('tipe_barang', 5)->first();
        $this->akun_preferensi         = PreferensiMataUang::all();
        $this->akun_balance            = Akun::where('nama_akun', 'Opening Balance Equity')->pluck('id');
        $this->code_generator          = new CodeHelper();
        $this->transaksi_helper        = new TransaksiHelper();
     }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $arrSaldo                   = [];
        $arrItemSaldoAwalPurpose    = [];

        foreach ($rows as $key => $value) {
            if ($value['nama'] !== null && $value['no_pelanggan'] !== null ) {
                $pelanggan_master_list = [
                    'nama'                  => $value['nama'],
                    'no_pelanggan'          => $value['no_pelanggan'],
                    'alamat'                => $value['alamat'],
                    'telepon'               => $value['telepon'],
                    'tipe_pelanggan_id'     =>  DB::table('tipe_pelanggan')->where('nama', $value['tipe_pelanggan'])->first()->id ?? null,
                ];
                $pelanggan              = new InformasiPelanggan();
                $saveMasterPelanggan    = $pelanggan->fill($pelanggan_master_list);
                $saveMasterPelanggan->save();
                $saldo_awal         = str_replace(['Rp ', '.00', ','], '', $value['saldo_awal']);
                if ($saldo_awal != 0) {
                    $tanggal_saldo      = !empty($value['tanggal']) ? Carbon::parse($value['tanggal'])->format('Y-m-d H:i:s') : $this->skr;
                    $no_faktur          = $this->code_generator->generateNoFakturPelangganImport($value);
                    $arrSaldoAwalPurpose = [
                        'no_faktur'         => $no_faktur,
                        'pelanggan_id'      => $saveMasterPelanggan->id,
                        'form_no'           => '0',
                        'invoice_date'      => $tanggal_saldo,
                        'no_fp_std'         => '.',
                        'no_fp_std2'        => '.',
                        'no_fp_std_date'    => '.',
                        'keterangan'        => 'Saldo Awal',
                        'uang_muka'         => '0',
                        'total'             => $saldo_awal,
                        'term_id'           => $value['syarat_pembayaran_id'] ?? null,
                    ];
                    $fakturPenjualan = FakturPenjualan::create($arrSaldoAwalPurpose);
                    $arrSaldo[] = [
                        'no_faktur'                 => $no_faktur,
                        'tanggal'                   => $tanggal_saldo,
                        'saldo_awal'                => $saldo_awal,
                        'syarat_pembayaran_id'      => $value['syarat_pembayaran_id'] ?? null,
                        'informasi_pelanggan_id'    => $saveMasterPelanggan->id,
                        'created_at'                => $this->skr,
                        'updated_at'                => $this->skr,
                    ];
                    $arrItemSaldoAwalPurpose[] = [
                        'produk_id'             => $this->saldo_awal_produk->id,
                        'faktur_penjualan_id'   => $fakturPenjualan->id,
                        'item_deskripsi'        => $this->saldo_awal_produk->keterangan,
                        'item_unit'             => $this->saldo_awal_produk->unit,
                        'jumlah'                => $this->saldo_awal_produk->kuantitas,
                        'unit_price'            => $saldo_awal,
                        'diskon'                => $this->saldo_awal_produk->diskon,
                        'harga_modal'           => $saldo_awal,
                        'harga_final'           => $saldo_awal,
                        'created_at'            => $this->skr,
                        'updated_at'            => $this->skr,
                    ];
                    $transaksiDebit = [
                        'nominal'       => $saldo_awal,
                        'tanggal'       => $tanggal_saldo,
                        'dari'          => 'Saldo Awal per Item '.$key,
                        'id'            => $no_faktur,
                        'akun'          => $this->akun_preferensi->first()->akun_piutang_id,
                        'item_id'       => $fakturPenjualan->id,
                    ];
                    $transaksiKredit = [
                        'nominal'       => $saldo_awal,
                        'tanggal'       => $tanggal_saldo,
                        'dari'          => 'Saldo Awal per Item balance '.$key,
                        'id'            => $no_faktur,
                        'akun'          => $this->akun_balance[0],
                        'item_id'       => $fakturPenjualan->id,
                    ];
                    $this->transaksi_helper->ExecuteTransactionSingle('insert', $fakturPenjualan, $transaksiKredit); //kredit
                    $this->transaksi_helper->ExecuteTransactionSingle('insert', $fakturPenjualan, $transaksiDebit, 1); //debit
                }
            }
        }
        DetailInformasiPelanggan::insert($arrSaldo);
        BarangFakturPenjualan::insert($arrItemSaldoAwalPurpose);
    }

    public function chunkSize(): int
    {
        return 500;
    }
}
