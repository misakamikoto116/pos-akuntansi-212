<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\TipeAkun;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\Transaksi;

use Carbon\Carbon;
use Helpers\CodeHelper;
use Helpers\ImportAkunHelper;
use Helpers\TransaksiHelper;

class AkunImport implements ToCollection, WithHeadingRow, WithChunkReading
{
    public function __construct()
    {
        $this->skr                     = date('Y-m-d H:i:s');
        $this->import_akun_helper      = new ImportAkunHelper();
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $key => $value) {
            if ($value['kode_akun'] !== null && $value['nama_akun'] !== null ) {
                $tanggal_masuk      = !empty($value['tanggal_masuk']) ? Carbon::parse($value['tanggal_masuk'])->format('Y-m-d H:i:s') : $this->skr;
                $tipe_akun          = null == $value['tipe_akun_id'] ? null : TipeAkun::where('title', $value['tipe_akun_id'])->first()->id ?? null;
                $parent_dari        = null == $value['parent_dari'] ? null : Akun::where('nama_akun', $value['parent_dari'])->first()->id ?? null;
                if ($value['parent_dari'] === null) {
                    $save_akun = [
                        'kode_akun'         => $value['kode_akun'],
                        'nama_akun'         => $value['nama_akun'],
                        'money_function'    => 0,
                        'tanggal_masuk'     => $tanggal_masuk ?? null,
                        'parent_id'         => $parent_dari,
                        'tipe_akun_id'      => $tipe_akun,
                        'mata_uang_id'      => $value['mata_uang_id'],   
                    ];
                    $createAkun = Akun::create($save_akun);
                    if ($value['nominal'] !== 0 && $value['nominal'] !== null) {
                        $this->import_akun_helper->insertTransaction($createAkun, $value, $this->skr);
                    }
                }else {
                    $parents = Akun::where('nama_akun', $value['parent_dari'])->first()->id ?? null;
                    $save_akun = [
                        'kode_akun'         => $value['kode_akun'],
                        'nama_akun'         => $value['nama_akun'],
                        'money_function'    => 0,
                        'tanggal_masuk'     => $tanggal_masuk ?? null,
                        'parent_id'         => $parents,
                        'tipe_akun_id'      => $tipe_akun,
                        'mata_uang_id'      => $value['mata_uang_id'],   
                    ];
                    $createAkun = Akun::create($save_akun);
                    if ($value['nominal'] !== 0 && $value['nominal'] !== null) {
                        $this->import_akun_helper->insertTransaction($createAkun, $value, $this->skr);
                    }
                }
            }
        }
    }

    public function chunkSize(): int
    {
        return 500;
    }
}
