<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

use App\Modules\Akuntansi\Models\HargaJual;
use Carbon\Carbon;

class BarcodeImport implements ToCollection, WithHeadingRow, WithChunkReading
{
    public function __construct()
    {
        $this->skr          =   date('Y-m-d H:i:s');
        $this->hargaJual    =   new HargaJual();
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $nama = [];
        foreach ($collection as $key => $value) {
            if ($value['no_barang'] !== null) {
                $nama[] = [
                    'no_barang'                             => $value['no_barang'],
                    'barcode'                               => $value['barcode'],
                    'keterangan'                            => $value['keterangan'],
                    'harga_jual'                            => $value['harga_jual'],
                    'nama_singkat'                          => $value['nama_singkat'] ?? null,
                    'created_at'                            => $this->skr,
                    'updated_at'                            => $this->skr,
                ];
            }
        }
        $this->hargaJual::insert($nama);
    }

    public function chunkSize(): int
    {
        return 500;
    }
}
