<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use App\Modules\Akuntansi\Models\JasaPengiriman;

class JasaPengirimanImport implements ToCollection, WithChunkReading, WithHeadingRow
{
    public function __construct()
    {
        $this->skr      =   date('Y-m-d H:i:s');
        $this->jasaPengiriman    =   new JasaPengiriman ();
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $nama                   = [];
        foreach ($collection as $key => $value) {
            if ($value['nama'] !== null) {
                $nama[] = [
                    'nama'          => $value['nama'],
                    'created_at'    => $this->skr,
                    'updated_at'    => $this->skr,
                ];
            }
        }
        $this->jasaPengiriman->insert($nama);
    }

    public function chunkSize() : int
    {
        return 500;
    }
}
