<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\DetailPenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\PenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\KategoriProduk;
use App\Modules\Akuntansi\Models\Transaksi;

class ProdukWithoutAccountImport implements ToCollection, WithChunkReading, WithHeadingRow
{
    public function __construct($pureIt)
    {
        $this->pureIt           = $pureIt;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $key => $value) {
            if ($value['no_barang'] !== null && $value['keterangan'] !== null && $value['kategori_produk_id'] !== null):
                $kategori_produk_id = KategoriProduk::where('nama', $value['kategori_produk_id'])->first();
                $product_master_list = [
                    'tipe_barang'             => 0,
                    'parent_id'               => null,
                    'no_barang'               => $value['no_barang'],
                    'barcode'                 => $value['no_barang'],
                    'status'                  => 1,
                    'tipe_persedian'          => $value['tipe_persedian'],
                    'keterangan'              => $value['keterangan'],
                    'harga_standar_def'       => $value['harga_standar_def'],
                    'kuantitas'               => $value['kuantitas'],
                    'harga_jual'              => $value['harga_jual'],
                    'unit'                    => $value['unit'],
                    'status_umkm'             => array_key_exists('status_umkm', $value) ? $value['status_umkm'] : null,
                    'gudang_id'               => 6,
                    'kategori_produk_id'      => $kategori_produk_id->id ?? 12,
                    'pemasok_id'              => array_key_exists('pemasok_id', $value) ? $value['pemasok_id'] : null,
                    'min_jumlah_reorder'      => $value['min_jumlah_reorder'],
                    'kode_pajak_perse_id'     => 'NULL' == (int) $value['kode_pajak_perse_id'] ? null : $value['kode_pajak_perse_id'],
                    'catatan'                 => $value['catatan'],
                    'akun_beban_id'           => (null == $this->pureIt['akun_beban_id'] ? null : $this->pureIt['akun_beban_id']),
                    'akun_hpp_id'             => (null == $this->pureIt['akun_hpp_id'] ? null : $this->pureIt['akun_hpp_id']),
                    'akun_ret_penjualan_id'   => (null == $this->pureIt['akun_ret_penjualan_id'] ? null : $this->pureIt['akun_ret_penjualan_id']),
                    'akun_disk_penjualan_id'  => (null == $this->pureIt['akun_disk_penjualan_id'] ? null : $this->pureIt['akun_disk_penjualan_id']),
                    'akun_persedian_id'       => (null == $this->pureIt['akun_persedian_id'] ? null : $this->pureIt['akun_persedian_id']),
                    'akun_penjualan_id'       => (null == $this->pureIt['akun_penjualan_id'] ? null : $this->pureIt['akun_penjualan_id']),
                    'akun_barang_terkirim_id' => (null == $this->pureIt['akun_barang_terkirim_id'] ? null : $this->pureIt['akun_barang_terkirim_id']),
                    'akun_ret_pembelian_id'   => (null == $this->pureIt['akun_ret_pembelian_id'] ? null : $this->pureIt['akun_ret_pembelian_id']),
                    'akun_belum_tertagih_id'  => (null == $this->pureIt['akun_belum_tertagih_id'] ? null : $this->pureIt['akun_belum_tertagih_id']),
                    'deleted_at'              => null,
                    'diskon'                  => null,
                    'created_at'              => $this->pureIt['skr'],
                ];

                $produk = new Produk();
                $saveMasterProduk = $produk->fill($product_master_list);
                $saveMasterProduk->save();

                if ($value['kuantitas'] > 0) {
                    $x = 0;
                    $penyesuaian_persediaan = [
                        'no_penyesuaian'    => 'SALDO-AWAL-BARANG/IMPORT/ '.$this->pureIt['skr'].'/ '.$saveMasterProduk->id,
                        'tgl_penyesuaian'   => $this->pureIt['skr'],
                        'kode_akun_id'      => 21,
                        'keterangan_produk' => $value['catatan'],
                        'akun_penyesuaian'  => 75,
                        'keterangan'        => 'IMPORT CSV',
                        'created_at'        => $this->pureIt['skr'],
                        'updated_at'        => $this->pureIt['skr'],
                        'deleted_at'        => null,
                        'status'            => 0,
                    ];

                    $masterPenyesuaianPersediaan = new PenyesuaianPersediaan();
                    $masterPenyesuaianPersediaan->fill($penyesuaian_persediaan)->save();
                    $detail_penyesuaian = [
                        'penyesuaian_persediaan_id' => $masterPenyesuaianPersediaan->id, //detail dari $penyesuaian_persediaan
                        'produk_id'                 => $produk->id,
                        'keterangan_produk'         => $value['keterangan'],
                        'satuan_produk'             => $value['unit'],
                        'qty_produk'                => 0,
                        'qty_baru'                  => $value['kuantitas'],
                        'nilai_skrg'                => 0,
                        'nilai_baru'                => $value['harga_modal'],
                        'departemen'                => null,
                        'gudang_id'                 => 6,
                        'proyek'                    => null,
                        'serial_number'             => $value['no_barang'],
                        'created_at'                => $this->pureIt['skr'],
                        'updated_at'                => $this->pureIt['skr'],
                        'deleted_at'                => null,
                    ];
                    $detailPenyesuaian = new DetailPenyesuaianPersediaan();
                    $detailPenyesuaian->insert($detail_penyesuaian);
                    $transaksiKredit = [
                        //'id' => $i++,
                        'nominal'             => $value['harga_modal'] * $value['kuantitas'],
                        'tanggal'             => $this->pureIt['skr'],
                        'akun_id'             => 75,
                        'produk_id'           => $produk->id,
                        'item_id'             => $produk->id,
                        'item_type'           => Produk::class,
                        'status'              => 0,
                        'status_rekonsiliasi' => 0,
                        'sumber'              => 'SALDO AWAL BARANG',
                        'keterangan'          => 'SALDO-AWAL-BARANG/' . $produk->id,
                        'dari'                => 'Akun Penyesuaian',
                        'no_transaksi'        => 'SALDO-AWAL-BARANG/' . $produk->id,
                        'desc_dev'            => null,
                        'deleted_at'          => null,
                        'created_at'          => $this->pureIt['skr'],
                        'updated_at'          => $this->pureIt['skr'],
                        'created_by'          => 2, //krn dari console should be auth()->user()->id,
                        'updated_by'          => 2, //krn dari console should be auth()->user()->id,
                        'deleted_by'          => null,
                    ];

                    $transaksiKreditInsert = new Transaksi();
                    $transaksiKreditInsert->fill($transaksiKredit);
                    $transaksiKreditInsert->save();
                    $transaksiDebit = [
                        'nominal'             => $value['harga_modal'] * $value['kuantitas'],
                        'tanggal'             => $this->pureIt['skr'],
                        'akun_id'             => 21,
                        'produk_id'           => $produk->id,
                        'item_id'             => $produk->id,
                        'item_type'           => Produk::class,
                        'status'              => 1,
                        'status_rekonsiliasi' => 0,
                        'sumber'              => 'SALDO AWAL BARANG',
                        'keterangan'          => 'SALDO-AWAL-BARANG/' . $produk->id,
                        'dari'                => 'Akun Persediaan',
                        'no_transaksi'        => 'SALDO-AWAL-BARANG/' . $produk->id,
                        'desc_dev'            => null,
                        'deleted_at'          => null,
                        'created_at'          => $this->pureIt['skr'],
                        'updated_at'          => $this->pureIt['skr'],
                        'created_by'          => 2, //krn dari console
                        'updated_by'          => 2, //krn dari console
                        'deleted_by'          => null,
                    ];
                    $transaksiDebitInsert = new Transaksi();
                    $transaksiDebitInsert->fill($transaksiDebit);
                    $transaksiDebitInsert->save();

                    $product_detail_list = [
                        'no_faktur'      => 'POS-AWAL',
                        'tanggal'        => $this->pureIt['skr'],
                        'kuantitas'      => (null != $value['kuantitas'] ? $value['kuantitas'] : 0),
                        'harga_modal'    => (null != $value['harga_modal'] ? $value['harga_modal'] : 0),
                        'harga_terakhir' => (null != $value['harga_modal'] ? $value['harga_modal'] : 0),
                        'gudang_id'      => 6,
                        'transaksi_id'   => ($value['kuantitas'] > 0 ? $transaksiDebitInsert->id : null),
                        'produk_id'      => $produk->id,
                        'sn'             => $value['no_barang'],
                        'status'         => 0,
                        'item_type'      => get_class($masterPenyesuaianPersediaan), //jika
                        'item_id'        => $masterPenyesuaianPersediaan->id, //harus dari transasksi
                        'deleted_at'     => null,
                        'created_at'     => $this->pureIt['skr'],
                        'updated_at'     => $this->pureIt['skr'],
                    ];
                    SaldoAwalBarang::insert($product_detail_list);
                }
            endif;
        }
    }

    public function chunkSize() : int
    {
        return 500;
    }
}
