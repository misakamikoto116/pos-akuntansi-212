<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

use App\Modules\Akuntansi\Models\SyaratPembayaran;

class SyaratPembayaranImport implements ToCollection, WithHeadingRow, WithChunkReading
{
    public function __construct()
    {
        $this->skr                  =   date('Y-m-d H:i:s');
        $this->syaratPembayaran     =   new SyaratPembayaran ();
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $nama                   = [];
        foreach ($collection as $key => $value) {
            if ($value['nama'] !== null) {
                $nama[] = [
                    'nama'                          => $value['nama'],
                    'akan_dapat_diskon'             => $value['discount'],
                    'jika_membayar_antara'          => $value['disc_day'],
                    'jatuh_tempo'                   => $value['net_days'],
                    'created_at'                    => $this->skr,
                    'updated_at'                    => $this->skr,
                ];
            }
        }
        $this->syaratPembayaran->insert($nama);
    }

    public function chunkSize(): int
    {
        return 500;
    }
}
