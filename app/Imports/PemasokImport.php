<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\BarangFakturPembelian;
use App\Modules\Akuntansi\Models\DetailInformasiPemasok;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Transaksi;
use Carbon\Carbon;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;


class PemasokImport implements ToCollection, WithHeadingRow, WithChunkReading
{
    public function __construct()
    {
        $this->skr                     = date('Y-m-d H:i:s');
        $this->saldo_awal_produk       = Produk::where('tipe_barang', 5)->first();
        $this->akun_preferensi         = PreferensiMataUang::all();
        $this->akun_balance            = Akun::where('nama_akun', 'Opening Balance Equity')->pluck('id');
        $this->code_generator          = new CodeHelper();
        $this->transaksi_helper        = new TransaksiHelper();
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $arrSaldo                   = [];
        $arrItemSaldoAwalPurpose    = [];
        foreach ($collection as $key => $value) {
            if ($value['nama'] !== null && $value['no_pemasok'] !== null ) {
                $pemasok_master_list = [
                    'nama'             => $value['nama'],
                    'no_pemasok'       => $value['no_pemasok'],
                    'alamat'           => $value['alamat'],
                    'telepon'          => $value['telepon'],
                ];
                $pemasok                = new InformasiPemasok();
                $saveMasterPemasok      = $pemasok->fill($pemasok_master_list);
                $saveMasterPemasok->save();
                $saldo_awal         = str_replace(['Rp ', '.00', ','], '', $value['saldo_awal']);
                    if ($saldo_awal != 0) {
                        // overriding
                        $tanggal_saldo      = !empty($value['tanggal']) ? Carbon::parse($value['tanggal'])->format('Y-m-d H:i:s') : $this->skr;
                        $no_faktur          = $this->code_generator->generateNoFakturPemasokImport($value);
                        $arrSaldoAwalPurpose = [
                            'no_faktur'         => $no_faktur,
                            'pemasok_id'        => $saveMasterPemasok->id,
                            'form_no'           => '0',
                            'invoice_date'      => $tanggal_saldo,
                            'no_fp_std'         => '.',
                            'no_fp_std2'        => '.',
                            'no_fp_std_date'    => '.',
                            'keterangan'        => 'Saldo Awal',
                            'catatan'           => 'Saldo Awal',
                            'uang_muka'         => '0',
                            'total'             => $saldo_awal,
                            'term_id'           => $value['syarat_pembayaran_id'] ?? null,
                        ];
                        $fakturPembelian = FakturPembelian::create($arrSaldoAwalPurpose);
                        $arrSaldo[] = [
                            'no_faktur'                 => $no_faktur,
                            'tanggal'                   => $tanggal_saldo,
                            'saldo_awal'                => $saldo_awal,
                            'syarat_pembayaran_id'      => $value['syarat_pembayaran_id'] ?? null,
                            'informasi_pemasok_id'      => $saveMasterPemasok->id,
                            'created_at'                => $this->skr,
                            'updated_at'                => $this->skr,
                        ];
                        $arrItemSaldoAwalPurpose[] = [
                            'produk_id'             => $this->saldo_awal_produk->id,
                            'faktur_pembelian_id'   => $fakturPembelian->id,
                            'item_deskripsi'        => $this->saldo_awal_produk->keterangan,
                            'item_unit'             => $this->saldo_awal_produk->unit,
                            'jumlah'                => $this->saldo_awal_produk->kuantitas,
                            'unit_price'            => $saldo_awal,
                            'diskon'                => $this->saldo_awal_produk->diskon,
                            'harga_modal'           => $saldo_awal,
                            'harga_final'           => $saldo_awal,
                            'created_at'            => $this->skr,
                            'updated_at'            => $this->skr,
                        ];
                        $transaksiDebit = [
                            'nominal'       => $saldo_awal,
                            'tanggal'       => $tanggal_saldo,
                            'dari'          => 'Saldo Awal per Item '.$key,
                            'id'            => $no_faktur,
                            'akun'          => $this->akun_preferensi->first()->akun_hutang_id,
                            'item_id'       => $fakturPembelian->id,
                        ];
                        $transaksiKredit = [
                            'nominal'       => $saldo_awal,
                            'tanggal'       => $tanggal_saldo,
                            'dari'          => 'Saldo Awal per Item balance '.$key,
                            'id'            => $no_faktur,
                            'akun'          => $this->akun_balance[0],
                            'item_id'       => $fakturPembelian->id,
                        ];
                        $this->transaksi_helper->ExecuteTransactionSingle('insert', $fakturPembelian, $transaksiKredit); //kredit
                        $this->transaksi_helper->ExecuteTransactionSingle('insert', $fakturPembelian, $transaksiDebit, 1); //debit
                    }
            }
        }

        DetailInformasiPemasok::insert($arrSaldo);
        BarangFakturPembelian::insert($arrItemSaldoAwalPurpose);
    }

    public function chunkSize():int
    {
        return 500;
    }
}
