<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\PenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\DetailPenyesuaianPersediaan;

class ProdukWithAccountImport implements ToCollection, WithHeadingRow, WithChunkReading
{
    public function __construct()
    {
        $this->skr      = date('Y-m-d H:i:s');
        $this->akuns    = Akun::select(['kode_akun', 'id'])->get();
        $this->opening_balance = Akun::select(['kode_akun', 'id'])->where('kode_akun', '3300')->first();
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $i = 0;
        foreach ($collection as $key => $value) {
            if ($value['no_barang'] !== null && $value['keterangan'] !== null):
                $opening_balance			= $this->opening_balance->id;
                $akun_hpp_id 				= null == $value['kode_akun_hpp'] ? null : $this->akuns->where('kode_akun', $value['kode_akun_hpp'] )->first()->id ?? null;
                $akun_ret_pembelian_id 		= null == $value['kode_akun_ret_pembelian'] ? null : $this->akuns->where('kode_akun', $value['kode_akun_ret_pembelian'] )->first()->id ?? null;
                $akun_penjualan_id 			= null == $value['kode_akun_penjualan'] ? null : $this->akuns->where('kode_akun', $value['kode_akun_penjualan'] )->first()->id ?? null;
                $akun_ret_penjualan_id 		= null == $value['kode_akun_ret_penjualan'] ? null : $this->akuns->where('kode_akun', $value['kode_akun_ret_penjualan'] )->first()->id ?? null;
                $akun_persedian_id			= null == $value['kode_akun_persedian'] ? null : $this->akuns->where('kode_akun', $value['kode_akun_persedian'] )->first()->id ?? null;
                $akun_disk_penjualan_id 	= null == $value['kode_akun_disk_penjualan'] ? null : $this->akuns->where('kode_akun', $value['kode_akun_disk_penjualan'] )->first()->id ?? null;
                $akun_barang_terkirim_id 	= null == $value['kode_akun_barang_terkirim'] ? null : $this->akuns->where('kode_akun', $value['kode_akun_barang_terkirim'] )->first()->id ?? null;
                $akun_belum_tertagih_id 	= null == $value['kode_akun_belum_tertagih'] ? null : $this->akuns->where('kode_akun', $value['kode_akun_belum_tertagih'] )->first()->id ?? null;
                $product_master_list = [
                    'tipe_barang'             => $value['tipe_barang'],
                    'parent_id'               => ( null == $value['parent_id'] ? null : $value['parent_id'] ),
                    'no_barang'               => $value['no_barang'],
                    'status'                  => $value['status'],
                    'tipe_persedian'          => $value['tipe_persedian'] ?? null,
                    'keterangan'              => $value['keterangan'],
                    'harga_standar_def'       => $value['harga_standar_def'] ?? null,
                    'kuantitas'               => $value['kuantitas'],
                    'harga_jual'              => $value['harga_jual'],
                    'unit'                    => $value['unit'],
                    'status_umkm'             => array_key_exists('status_umkm', $value) ? $value['status_umkm'] : null,
                    'gudang_id'               => ( null == $value['gudang_id'] ? 6 : $value['gudang_id'] ),
                    'kategori_produk_id'      => ( null == $value['kategori_produk_id'] ? 12 : $value['kategori_produk_id'] ),
                    'pemasok_id'              => array_key_exists('pemasok_id', $value) ? $value['pemasok_id'] : null,
                    'min_jumlah_reorder'      => null == $value['min_jumlah_reorder'] ? null : $value['min_jumlah_reorder'],
                    'kode_pajak_perse_id'     => ( null == (int) $value['kode_pajak_perse_id'] ? null : $value['kode_pajak_perse_id'] ),
                    'catatan'                 => $value['catatan'],
                    'akun_beban_id'           => array_key_exists('akun_beban_id', $value) ? $value['akun_beban_id']: null,
                    'akun_hpp_id'             => (null == $value['kode_akun_hpp'] ? null : $akun_hpp_id),
                    'akun_ret_penjualan_id'   => (null == $value['kode_akun_ret_penjualan'] ? null : $akun_ret_penjualan_id),
                    'akun_disk_penjualan_id'  => (null == $value['kode_akun_disk_penjualan'] ? null : $akun_disk_penjualan_id),
                    'akun_persedian_id'       => (null == $value['kode_akun_persedian'] ? null : $akun_persedian_id),
                    'akun_penjualan_id'       => (null == $value['kode_akun_penjualan'] ? null : $akun_penjualan_id),
                    'akun_barang_terkirim_id' => (null == $value['kode_akun_barang_terkirim'] ? null : $akun_barang_terkirim_id),
                    'akun_ret_pembelian_id'   => (null == $value['kode_akun_ret_pembelian'] ? null : $akun_ret_pembelian_id),
                    'akun_belum_tertagih_id'  => (null == $value['kode_akun_belum_tertagih'] ? null : $akun_belum_tertagih_id),
                    'deleted_at'              => null,
                    'diskon'                  => null,
                    'created_at'              => $this->skr,
                ];
                $produk = new Produk();
                $saveMasterProduk = $produk->fill($product_master_list);
                $saveMasterProduk->save();
        // -------------------->
                // if ($value['kuantitas'] > 0) :
                    $x = 0;
                    $penyesuaian_persediaan = [
                        'no_penyesuaian'    => 'SALDO-AWAL-BARANG/IMPORT/ '.$this->skr.'/ '.$saveMasterProduk->id,
                        'tgl_penyesuaian'   => $this->skr,
                        'kode_akun_id'      => $akun_persedian_id,// 21,
                        'keterangan_produk' => $value['catatan'],
                        'akun_penyesuaian'  => $opening_balance, //75,
                        'keterangan'        => 'IMPORT XLSX',
                        'created_at'        => $this->skr,
                        'updated_at'        => $this->skr,
                        'deleted_at'        => null,
                        'status'            => 0,
                    ];
                    $masterPenyesuaianPersediaan = new PenyesuaianPersediaan();
                    $masterPenyesuaianPersediaan->fill($penyesuaian_persediaan)->save();
                    $detail_penyesuaian = [
                        'penyesuaian_persediaan_id' => $masterPenyesuaianPersediaan->id, //detail dari $penyesuaian_persediaan
                        'produk_id'                 => $produk->id,
                        'keterangan_produk'         => $value['keterangan'],
                        'satuan_produk'             => $value['unit'],
                        'qty_produk'                => 0,
                        'qty_baru'                  => $value['kuantitas'],
                        'nilai_skrg'                => 0,
                        'nilai_baru'                => $value['harga_modal'],
                        'departemen'                => null,
                        'gudang_id'                 => 6,
                        'proyek'                    => null,
                        'serial_number'             => $value['no_barang'],
                        'created_at'                => $this->skr,
                        'updated_at'                => $this->skr,
                        'deleted_at'                => null,
                    ];
                    $detailPenyesuaian = new DetailPenyesuaianPersediaan();
                    $detailPenyesuaian->insert($detail_penyesuaian);
                    $transaksiKredit = [
                        //'id' => $i++,
                        'nominal'             => $value['harga_modal'] * $value['kuantitas'],
                        'tanggal'             => $this->skr,
                        'akun_id'             => $opening_balance, //75,
                        'produk_id'           => $produk->id,
                        'item_id'             => $produk->id,
                        'item_type'           => Produk::class,
                        'status'              => 0,
                        'status_rekonsiliasi' => 0,
                        'sumber'              => 'SALDO AWAL BARANG',
                        'keterangan'          => 'SALDO-AWAL-BARANG/' . $produk->id,
                        'dari'                => 'Akun Penyesuaian',
                        'no_transaksi'        => 'SALDO-AWAL-BARANG/' . $produk->id,
                        'desc_dev'            => null,
                        'deleted_at'          => null,
                        'created_at'          => $this->skr,
                        'updated_at'          => $this->skr,
                        'created_by'          => 2,
                        'updated_by'          => 2,
                        'deleted_by'          => null,
                    ];
                    $transaksiKreditInsert = new Transaksi();
                    $transaksiKreditInsert->fill($transaksiKredit);
                    $transaksiKreditInsert->save();
                    $transaksiDebit = [
                        'nominal'             => $value['harga_modal'] * $value['kuantitas'],
                        'tanggal'             => $this->skr,
                        'akun_id'             => $akun_persedian_id, //21,
                        'produk_id'           => $produk->id,
                        'item_id'             => $produk->id,
                        'item_type'           => Produk::class,
                        'status'              => 1,
                        'status_rekonsiliasi' => 0,
                        'sumber'              => 'SALDO AWAL BARANG',
                        'keterangan'          => 'SALDO-AWAL-BARANG/' . $produk->id,
                        'dari'                => 'Akun Persediaan',
                        'no_transaksi'        => 'SALDO-AWAL-BARANG/' . $produk->id,
                        'desc_dev'            => null,
                        'deleted_at'          => null,
                        'created_at'          => $this->skr,
                        'updated_at'          => $this->skr,
                        'created_by'          => 2,
                        'updated_by'          => 2,
                        'deleted_by'          => null,
                    ];
                    $transaksiDebitInsert = new Transaksi();
                    $transaksiDebitInsert->fill($transaksiDebit);
                    $transaksiDebitInsert->save();
                // endif;
        // -------------------->
                $product_detail_list = [
                    'no_faktur'      => 'POS-AWAL',
                    'tanggal'        => $this->skr,
                    'kuantitas'      => (null != $value['kuantitas'] ? $value['kuantitas'] : 0),
                    'harga_modal'    => (null != $value['harga_modal'] ? $value['harga_modal'] : 0),
                    'harga_terakhir' => (null != $value['harga_modal'] ? $value['harga_modal'] : 0),
                    'gudang_id'      => null == $value['gudang_id'] ? 6 : $value['gudang_id'],
                    'transaksi_id'   => ($value['kuantitas'] > 0 ? $transaksiDebitInsert->id : null),
                    'produk_id'      => $produk->id,
                    'sn'             => $value['no_barang'],
                    'status'         => 0,
                    'item_type'      => get_class($masterPenyesuaianPersediaan),
                    'item_id'        => $masterPenyesuaianPersediaan->id,
                    'deleted_at'     => null,
                    'created_at'     => $this->skr,
                    'updated_at'     => $this->skr,
                ];
                SaldoAwalBarang::insert($product_detail_list);
            endif;
        }
    }

    public function chunkSize(): int
    {
        return 500;
    }
}
