<?php

namespace App\Http\Controllers\Api\KasirCheckOut;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\KasirCheckOut\KasirCheckOutRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KasirCheckOutController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(KasirCheckOutRepository $kasir_check_out_repository)
    {
        $this->kasir_check_out_repository = $kasir_check_out_repository;
    }
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // TODO CHECK OUT KASIR 
    // 1. membuat controller dan repository KasirCheckoutController & KasirCheckOutRepository
    // 2. Mengambil data dari body/request, dilempar ke repository untuk diolah
    // 3. Mengolah data seperti check out di pos. PenjualanRepositories => kasirCheckoutStore
    
    public function store(Request $request)
    {
       $data = $request->all();

       $response = $this->kasir_check_out_repository->insert($data);

       return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
