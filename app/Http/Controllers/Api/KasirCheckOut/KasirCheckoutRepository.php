<?php

namespace App\Http\Controllers\Api\KasirCheckOut; 


use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Pos\Models\KasirCek;
use App\Modules\Pos\Models\TransactionNonCash;
use Carbon\Carbon;
use DB;
use Generator\Interfaces\RepositoryInterface;

class KasirCheckOutRepository implements RepositoryInterface 
{
    public function __construct(FakturPenjualan $fakturPenjualan, 
                                KasirCek $kasirCek, 
                                TransactionNonCash $transactionNonCash,
                                Identitas $identitas) 
    {
        $this->fakturPenjualan              = $fakturPenjualan;
        $this->kasirCek                     = $kasirCek;
        $this->transactionNonCash           = $transactionNonCash;
        $this->identitas                    = $identitas;
    }

     /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        // TODO: Implement getItems() method.
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        // TODO: Implement findItem() method.
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id      [description]
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function update($id, $request)
    {
        // TODO: Implement update() method.
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($request)
    {
        DB::beginTransaction();

        try {
            
            $sum_total  = $this->getTransaksi()->where('created_by', auth()->user()->id)->whereDate('invoice_date', Carbon::now()->format('Y-m-d'))->sum('total');

            $last_modal = $this->kasirCek->whereDate('check_time', Carbon::now()->format('Y-m-d'))
                                         ->where(['check_state' => 'IN', 'kasir_mesin_id' => $request['kasir_id'], 'created_by' => auth()->user()->id])
                                         ->orderBy('id', 'desc')
                                         ->firstOrFail();

            $edc        = $this->transactionNonCash->whereDate('created_at', Carbon::now()->format('Y-m-d'))
                                                   ->where('created_by', auth()->user()->id)
                                                   ->sum('total');

            $hutang     = $this->getTransaksi()->doesntHave('invoice')->where('created_by', auth()->user()->id)->whereDate('invoice_date', Carbon::now()->format('Y-m-d'))->sum('total');

            $this->kasirCek->create([
                'check_state' => 'OUT',
                'check_time' => Carbon::now(),
                'modal' => $last_modal->modal,
                'kasir_mesin_id' => $request['kasir_id'],
                'created_by' => auth()->user()->id,
            ]);

            DB::commit();

            $nomor_report = sprintf('CheckOut-%s-%s-%s', auth()->user()->name, $last_modal->kasirMesin->nama, Carbon::now()->format('d-m-Y_H-i-s'));

            $user_name = auth()->user()->name ?? '-';

            $total_sistem = $sum_total + $last_modal->modal;

            $response =  [
                        'status'        => true,
                        'message'       => 'berhasil check out',
                        'data'          => [
                                'alamat'            => $this->identitas->first()->alamat,
                                'kasir'             => $user_name,
                                'kasir_mesin'       => $last_modal->kasirMesin->nama,
                                'tanggal_cek_out'   => Carbon::now('Asia/Makassar')->format('d/m/Y - H:i:s'),
                                'uang_fisik'        => number_format($request['uang_fisik']),
                                'uang_sistem'       => number_format($sum_total),
                                'modal_awal'        => number_format($last_modal->modal),
                                'total_sistem'      => number_format($total_sistem),
                                'edc'               => number_format($edc),
                                'selisih'           => number_format(($request['uang_fisik'] + $edc + $hutang) - $total_sistem),
                                'uang_setoran'      => number_format($request['uang_fisik'] - $last_modal->modal),
                                'nomor_report'      => $nomor_report,
                                'hutang'            => number_format($hutang),
                        ]
                    ];

            return $response;

        } catch (Exception $e) {
            
            DB::rollBack();
            
            $res = ['error' => $e->getMessage()];
            
            dd($e->getMessage());

            \Log::error('kasir check in',[$e->getMessage()]);

        }
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $request
     */
    public function filter($request)
    {
        // TODO: Implement filter() method.
    }

    public function getTransaksi()
    {
        return  $this->fakturPenjualan->with(['transaksi' => function ($query) {
            $query->where('dari', 'Penjualan dengan Tax & Disc');
        }, 'kasirMesin', 'barang.produk','invoice','riwayatPembayaran','returPenjualan.barang'])->has('transaksi')->where('status_modul', 1)->whereNotNull('kasir_mesin_id');

    }
}