<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

class ApiAuthController extends ApiController
{
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('uG1Ne4I2CPKuXQ/y42aM7WQY')->accessToken;
            return response()->json([
                'status' => true,
                'message' => 'sukses login dan mendapatkan token',
                'data' => [
                    'token'         => $token,
                    'role'          => auth()->user()->roles()->first()->name,
                    'kasir_id'      => auth()->user()->id
                ]
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'gagal login',
                'data' => [
                    'token' => null
                ],
            ], 401);
        }
    }

    public function testUser()
    {
        $user =  auth()->user();
        $resp = [
            'nama' => $user->name,
            'email' => $user->email,
            'role' => $user->roles()->first()->name,
        ];
        return response()->json(['user' => $resp], 200);
    }
}
