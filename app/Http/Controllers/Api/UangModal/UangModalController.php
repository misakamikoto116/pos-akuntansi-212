<?php

namespace App\Http\Controllers\Api\UangModal;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\UangModal\UangModalCollection;
use App\Modules\Pos\Models\PreferensiPos;
use Illuminate\Http\Request;

class UangModalController extends ApiController
{

    public function __construct(PreferensiPos $preferensi_pos)
    {
        $this->preferensi_pos = $preferensi_pos;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uang_modal = $this->preferensi_pos->get();

        if ($uang_modal->isNotEmpty()) {
            $response = [
                'status'        => true,
                'message'       => 'Data ditemukan',
                'uang_modal'    => $uang_modal->first()->uang_modal
            ];
        }else {
            $response = [
                'status'        => false,
                'message'       => 'Data tidak ditemukan',
                'uang_modal'    => null
            ];
        }

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
