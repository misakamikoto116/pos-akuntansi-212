<?php
/**
 * User: efendihariyadi
 * Date: 17/08/18
 * Time: 00.37
 */

namespace App\Http\Controllers\Api\Produk;

use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\Api\KategoriProdukCollection;
use App\Http\Resources\Api\ProdukCollection;
use App\Http\Resources\Api\ProdukResource;
use App\Modules\Akuntansi\Models\KategoriProduk;
use App\Modules\Akuntansi\Models\PenawaranPenjualan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use Helpers\CodeHelper;
use Illuminate\Http\Request;

class ProdukController extends ApiController
{
    protected $code;
    protected $produk;
    protected $produkRepository;
    public function __construct(CodeHelper $code, Produk $produk, ProdukRepository $produkRepository)
    {
        $this->code = $code;
        $this->produk = $produk;
        $this->produkRepository = $produkRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return ProdukCollection|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $produkDetail = SaldoAwalBarang::filter($request)
            ->with('produk')
            ->groupBy('produk_id')
            ->orderBy('id', 'desc')
            //->with('kategori')
            ->whereHas('produk',function($produk){
                $produk->where('tipe_barang', '!==', 3);
            })
            ->where('gudang_id','=',6)->paginate(25);
        //dd($produkDetail);
        //$produk = Produk::filter($request)
        //        ->with('kategori')
        //        ->where('tipe_barang','!==',3)
        //        ->where('gudang_id','=',6)
        //        ->paginate(25);
        if((new ProdukCollection($produkDetail))->isEmpty()){
            return response()->json([
                'status' => false,
                'message' => 'tidak ditemukan',
                'data' => null
            ], 404);
        }
        return new ProdukCollection($produkDetail);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk = Produk::where('id','=', $id)
            ->with('saldoAwalBarang')
            ->where('tipe_barang','!==',3)
            ->where('gudang_id','=',6)->first();
        if(is_null($produk)){
            return response()->json([
                'status' => false,
                'message' => 'tidak ditemukan',
                'data' => [null]
            ], 404);
        }
        $resp = response()->json([
            'status' => true,
            'message' => 'data ditemukan',
            'data' => new ProdukResource($produk)
        ], 200);
        return $resp;
    }

    /**
     * list kategori produk dipakai dalam api utk filter barang dgn kategori
     * @return KategoriProdukCollection|array
     */
    public function listKategori()
    {
        $kategoriProduk = KategoriProduk::get(['id','nama']);
        return new KategoriProdukCollection($kategoriProduk);
    }

    /**
     * Prosses simpan data penawaran dari pramuniaga lewat API/Android
     * @param Request $request | berupa form array
     * @param PenawaranPenjualan $penawaranPenjualan
     * @return \Illuminate\Http\JsonResponse|array
     */
    public function addPenawaranPost(Request $request,  PenawaranPenjualan $penawaranPenjualan)
    {
        $validator = \Validator::make($request->all(), [
            'alamat_pengiriman' => 'required',
            'total_potongan_rupiah' => 'numeric',
            'produk.*.id' => 'required|integer',
            'produk.*.nama' => 'required|string',
            'produk.*.qty' => 'required|numeric',
            'produk.*.harga' => 'required|numeric',
        ]);
        if ($validator->fails())
        {
            return response()->json([
                'status' => false,
                'message' => 'format tidak tepat',
                'data' => collect( $validator->messages())->flatten()->toArray()
            ], 400);
        }
        $no_penawaran_terakhir = $this->code->autoGenerate($penawaranPenjualan->orderBy('id', 'desc')->first()->toArray(), "no_penawaran", $request->no_penawaran);
        $items_produk = $request->get('produk');
        if(is_null($items_produk)){
            return response()->json([
                'status' => false,
                'message' => 'items tidak boleh kosong',
                'data' => [null]
            ], 400);
        }
        $items = [];
        $total = 0;
        $ongkir = floatval(str_replace(['.','Rp', 'Rp.'], '', $request->ongkir));
        $total_potongan_rupiah = floatval(str_replace(['.','Rp', 'Rp.'], '', $request->total_potongan_rupiah));
        foreach ($items_produk as $item):
            $produk_detail = $this->produk->find($item['id']);
            $total += $item['qty'] * floatval($produk_detail->harga_jual);
        endforeach;
        foreach ($items_produk as $item):
            $produk_detail = $this->produk->find($item['id']);
            $items[] = [
                'produk_id' => $item['id'],
                'item_deskripsi' => $item['nama'],
                'jumlah' => (int) $item['qty'],
                'satuan' => $produk_detail->unit,
                'harga' =>  $produk_detail->harga_jual, //floatval(str_replace('.', '', $item['harga'])), -> jika ambil harga dari request API
                'terproses' => 0,
                'created_by' => auth()->user()->id,
            ];
        endforeach;
        $barcode = $this->code->generateBarcode($no_penawaran_terakhir);
        $master_penawaran  = [
            'no_penawaran' => $no_penawaran_terakhir,
            'barcode_penawaran' => $barcode,
            'pelanggan_id' => 6, //id pelanggan POS sesuai backlog
            'alamat_pengiriman' => $request->alamat_pengiriman,
            'taxable' => $request->taxable,
            'in_tax' => $request->in_tax,
            'total_potongan_rupiah' => $total_potongan_rupiah,
            'ongkir' => $ongkir,
            'nilai_pajak' => null,
            'total' => ($total + $ongkir) - $total_potongan_rupiah,
            'status' => 0,
            'keterangan' => $request->keterangan,
            'catatan' => $request->catatan,
            'tanggal' => date('Y-m-d H:i:s'),
            'created_by' => auth()->user()->id,
            'produk' => $items
        ];
        $this->produkRepository->insert($master_penawaran);
        return response()->json([
            'status' => true,
            'message' => 'sukses',
            'data' => [
                'pesanan' => $master_penawaran,
            ]
        ], 200);
    }

    /**
     * Semua Produk di tampilkan
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function allProduk(Request $request)
    {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 300);
        $produkDetail = SaldoAwalBarang::filter($request)
            ->with('produk')
            ->groupBy('produk_id')
            ->orderBy('id', 'desc')
            ->whereHas('produk',function($produk){
                $produk->where('tipe_barang', '!==', 3);
            })
            ->where('gudang_id','=',6)
            ->get();
        // dd($produkDetail->count());
        $transform = $produkDetail->transform(function ($item) {

            /** @var array $res di format ulang response */
            $res = [
                'id' => $item->produk->produk_detail->id,
                'nama' => $item->produk->keterangan,
                'harga' => $item->produk->harga_jual,
                'unit' => $item->produk->unit,
                'kategori' => $item->produk->kategori->nama,
                'no_barang' => $item->produk->no_barang,
                'diskon' => $item->produk->diskon,
                'harga_terakhir' => $item->harga_terakhir,
                'harga_modal' => $item->harga_modal,
                'foto_produk' => $item->produk->foto_produk,
                'tersedia' => $item->produk->UpdatedQty,
                //'tipe_persediaan' => $item->produk->TipePersediaanApi,
                //'harga' => $item->harga_terakhir
            ];
            return $res;
        });
        return response()->json([
            'status' => true,
            'message' => 'Ditemukan data',
            'data' => $transform,
        ]);
    }
}
