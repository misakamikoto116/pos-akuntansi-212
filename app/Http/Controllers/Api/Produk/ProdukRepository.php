<?php
/**
 * Created by PhpStorm.
 * User: efendihariyadi
 * Date: 19/08/18
 * Time: 00.51
 */

namespace App\Http\Controllers\Api\Produk;


use App\Modules\Akuntansi\Models\BarangPenawaranPenjualan;
use App\Modules\Akuntansi\Models\PenawaranPenjualan;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class ProdukRepository implements RepositoryInterface
{

    protected $penawaranPenjualan;
    public function __construct(PenawaranPenjualan $penawaranPenjualan)
    {
        $this->penawaranPenjualan = $penawaranPenjualan;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        // TODO: Implement getItems() method.
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        // TODO: Implement findItem() method.
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id      [description]
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function update($id, $request)
    {
        // TODO: Implement update() method.
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return PenawaranPenjualan [type] [description]
     */
    public function insert($data)
    {
        $model = $this->penawaranPenjualan->fill($data);
        if($model->save()){
            $this->InsertBarangPenawaranPenjualan($data, $model);
        }
        return $model;
    }

    /**
     * @param $data array
     * @param $model Model
     * @return mixed
     */
    protected function InsertBarangPenawaranPenjualan($data, $model){
        $items = [];
        $jam_sekarang = date('Y-m-d H:i:s');
        foreach ($data['produk'] as $item):
            $items[] = [
                'produk_id' => $item['produk_id'],
                'penawaran_penjualan_id' => $model->id,
                'item_deskripsi' => $item['item_deskripsi'],
                'jumlah' => (int) $item['jumlah'],
                'satuan' => $item['satuan'], //$item['satuan'], -> jika ambil dari request
                'diskon' => null, //di Item padahal ada diskon knp null(sesuai backlog)
                'kode_pajak_id' => null,
                'kode_pajak_2_id' => null,
                'harga' =>  $item['harga'], //floatval(str_replace('.', '', $item['harga'])), -> jika ambil harga dari request API
                'harga_modal' => null,
                'terproses' => 0,
                'created_by' => $item['created_by'],
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];
        endforeach;
        return BarangPenawaranPenjualan::insert($items);
    }
    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $request
     */
    public function filter($request)
    {
        // TODO: Implement filter() method.
    }
}