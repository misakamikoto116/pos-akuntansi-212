<?php

namespace App\Http\Controllers\Api\KasirCheckIn;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\KasirCheckIn\KasirCheckInRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KasirCheckInController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // TODO Kasir Melakukan Check In
    // 1. membuat controller, repository, route
    // 2. proses pengecekan apakah kasir mesin ini tersedia atau tidak
    // 3. proses update status kasir mesin
    // 4. proses menyimpan history check in ke table kasir_cek
    
    protected $kasir_check_in_repository;

    public function __construct(KasirCheckInRepository $kasir_check_in_repository)
    {
        $this->kasir_check_in_repository        = $kasir_check_in_repository;
    }
    public function index()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $response = $this->kasir_check_in_repository->insert($data);

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
