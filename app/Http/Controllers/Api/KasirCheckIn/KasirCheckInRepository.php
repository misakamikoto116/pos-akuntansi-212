<?php

namespace App\Http\Controllers\Api\KasirCheckIn; 


use App\Modules\Pos\Models\KasirCek;
use App\Modules\Pos\Models\KasirMesin;
use App\Modules\Pos\Models\PreferensiPos;
use Carbon\Carbon;
use Generator\Interfaces\RepositoryInterface;
use DB;

class KasirCheckInRepository implements RepositoryInterface 
{
    public function __construct(
        KasirMesin $kasir_mesin,
        KasirCek $kasir_cek,
        PreferensiPos $preferensi_pos
    ) {
        $this->kasir_mesin                       = $kasir_mesin;
        $this->kasir_cek                         = $kasir_cek;
        $this->preferensi_pos                    = $preferensi_pos;
    }

     /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        // TODO: Implement getItems() method.
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        // TODO: Implement findItem() method.
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id      [description]
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function update($id, $request)
    {
        // TODO: Implement update() method.
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($request)
    {
        DB::beginTransaction();
        try {
            
            $kasir_mesin        = $this->kasir_mesin->findOrFail($request['kasir_id']);
            $preferensi_pos     = $this->preferensi_pos->firstOrFail();


            // if ($kasir_mesin->status === 0) {
                
                // $kasir_mesin->update([
                //     'status'        => 1,
                // ]);

                $this->kasir_cek->create([

                    'check_state'       => 'IN',
                    'check_time'        => Carbon::now(),
                    'modal'             => $preferensi_pos->modal ?? 0,
                    'kasir_mesin_id'    => $request['kasir_id'],
                    'created_by'        => auth()->user()->id

                ]);

                $response = [
                    'status'        => true,
                    'message'       => 'Kasir berhasil check in',
                ];

            // }else {

                // $response = [
                //     'status'        => false,
                //     'message'       => 'Kasir mesin sedang online'
                // ];

            // }


            DB::commit();

            return $response;

        } catch (Exception $e) {
            
            DB::rollBack();
            
            $res = ['error' => $e->getMessage()];
            
            dd($e->getMessage());

            \Log::error('kasir check in',[$e->getMessage()]);

        }
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $request
     */
    public function filter($request)
    {
        // TODO: Implement filter() method.
    }
}