<?php

namespace App\Http\Controllers\Api\Pelanggan;

use App\Http\Requests\Api\ApiPelangganRequest;
use App\Http\Resources\Api\Pelanggan\PelangganCollection;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use Generator\Interfaces\RepositoryInterface;
//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class Anggota extends Controller
{
    protected $informasiPelanggan;
    public function __construct(RepositoryInterface $model, InformasiPelanggan $informasiPelanggan)
    {
        $this->model = $model;
        $this->informasiPelanggan = $informasiPelanggan;
    }

    public function anggotaPost(ApiPelangganRequest $request)
    {
        $data = $request->get('anggota');
        $data['syarat_pembayaran_id'] = 1;
        $data['alamat_pajak'] = $data['alamat'];
        $data['batasan_piutang_hari'] = 10;
        $data['pajak_satu_id'] = null;
        $data['pajak_dua_id'] = null;
        $data['pajak_faktur'] = false;
        $data['nppkp'] = null;
        $data['tipe_pajak_id'] = null;
        $data['tipe_pelanggan_id'] = 5;
        $data['tingkatan_harga_jual'] = 0;
        $data['batasan_piutang_uang'] = null;
        $data['catatan'] = 'dari api kopsyah';

        
        DB::beginTransaction();
        $pelanggan = $this->informasiPelanggan;
        try {
            $location = storage_path().'/app/public/pelanggan/';
            $filenamePelanggan = null;
            $filenameNIKPelanggan = null;

            if(isset($request->anggota['foto_pelanggan'])){
                $filePelanggan = $request->anggota['foto_pelanggan'];
                $extensionPelanggan = $filePelanggan->getClientOriginalExtension();
                $filenamePelanggan = md5($filePelanggan->getFilename()).'.'.$extensionPelanggan;
                $locationImagePelanggan  =   $location.$filenamePelanggan;
                Image::make($filePelanggan)->resize(200, 150)->save($locationImagePelanggan);
                $data['foto_pelanggan'] = $filenamePelanggan;
            }
            if(isset($request->anggota['foto_nik_pelanggan'])){
                $fileNIKPelanggan = $request->anggota['foto_nik_pelanggan'];
                $extensionNIKPelanggan = $fileNIKPelanggan->getClientOriginalExtension();
                $filenameNIKPelanggan = md5($fileNIKPelanggan->getFilename()).'.'.$extensionNIKPelanggan;
                $locationImageNIKPelanggan  =   $location.$filenameNIKPelanggan;
                Image::make($fileNIKPelanggan)->resize(200, 150)->save($locationImageNIKPelanggan);
                $data['foto_nik_pelanggan'] = $filenameNIKPelanggan;
            }
            DB::commit();
            $pelanggan->fill($data)->save();
        }
        catch (\Exception $exception){
            DB::rollBack();
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage(),
                'data' => null,
            ], 500);
        }
        return new PelangganCollection(
            $this->informasiPelanggan
            ->where('no_pelanggan',  $pelanggan->no_pelanggan)
            ->get()
        );
    }


    // Update
    public function anggotaUpdate(ApiPelangganRequest $request, $no_pelanggan)
    {
        $data = $request->get('anggota');
        
        DB::beginTransaction();
        try {
            $pelanggan = $this->informasiPelanggan->where('no_pelanggan', $no_pelanggan)->first();
            if (!$pelanggan) {
                return response()->json([
                    'status' => false,
                    'message' => 'Pelanggan Not Found',
                    'data' => null,
                ], 404);    
            }

            $pelanggan->update($data);
            
            DB::commit();
        }
        catch (\Exception $exception){
            DB::rollBack();
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage(),
                'data' => null,
            ], 500);
        }
        return new PelangganCollection(
            $this->informasiPelanggan
            ->where('no_pelanggan',  $pelanggan->no_pelanggan)
            ->get()
        );
    }

    // Delete
    public function anggotaDelete($no_pelanggan)
    {
        DB::beginTransaction();
        try {
            $pelanggan = $this->informasiPelanggan->where('no_pelanggan', $no_pelanggan)->first();
            if (!$pelanggan) {
                return response()->json([
                    'status' => false,
                    'message' => 'Pelanggan Not Found',
                    'data' => null,
                ], 404);    
            }

            $pelanggan->delete();
            
            DB::commit();
        }
        catch (\Exception $exception){
            DB::rollBack();
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage(),
                'data' => null,
            ], 500);
        }
        return true;
    }
}
