<?php
/**
 * User: efendihariyadi
 * Date: 17/08/18
 * Time: 00.37
 */

namespace App\Http\Controllers\Api\Pelanggan;

use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\Api\Pelanggan\PelangganCollection;
//use App\Http\Resources\Api\Pelanggan\PelangganResources;
use App\Http\Resources\Api\ProdukResource;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Pos\Models\PreferensiPos;
use DB;
use Helpers\CodeHelper;
use Illuminate\Http\Request;

class PelangganController extends ApiController
{
    protected $code;

    public function __construct(CodeHelper $code)
    {
        $this->code = $code;
    }

    /**
     * Display a listing of the resource.
     *
     * @return PelangganCollection|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $pelanggan = InformasiPelanggan::filter($request)->paginate(25);

        if((new PelangganCollection($pelanggan))->isEmpty()){
            return response()->json([
                'status' => false,
                'message' => 'tidak ditemukan',
                'data' => null
            ], 404);
        }
        return new PelangganCollection($pelanggan);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $gudang_id = PreferensiPos::firstOrFail();

        $produk = Produk::where('id','=', $id)
            ->with('saldoAwalBarang')
            ->where('tipe_barang','!==',3)
            ->where('gudang_id','=', $gudang_id->gudang_id)->first();
        if(is_null($produk)){
            return response()->json([
                'status' => false,
                'message' => 'tidak ditemukan',
                'data' => null
            ], 404);
        }
        $resp = response()->json([
            'status' => true,
            'message' => 'data ditemukan',
            'data' => new ProdukResource($produk)
        ], 200);
        return $resp;
    }

    /**
     * APi Response grafik buat
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function topPelanggan(Request $request)
    {
        $headers = [
            'Access-Control-Allow-Origin' => '*',
        ];
        $bulan = date('m');
        $tahun = date('Y');
        $dataTopPelanggan = FakturPenjualan::with('pelanggan')
            ->addSelect('pelanggan_id')
            ->addSelect(DB::raw('sum(total) as total_belanja'))
            ->whereNotNull('pelanggan_id')
            ->orderBy(\DB::raw('sum(total)'),'DESC')
            ->whereRaw(DB::raw('month(created_at) = '.$bulan.' AND year(created_at) = '.$tahun))
            ->groupBy('pelanggan_id')
            ->limit($request->limit)
            ->get();

        $resp = [];
        $i = 0;
        if($dataTopPelanggan->isEmpty())
            return response()->json([
                'color' => '#fff',
                'nama' => 'no data',
                'nomer' => 'no data',
                'jml' => 0
            ], 404, $headers);
        foreach ($dataTopPelanggan as $row){
            $resp[] = [
                'color' => null,
                'nama' => $row->pelanggan->nama,
                'nomer' => $row->pelanggan->no_pelanggan,
                'jml' => $row->total_belanja,
            ];
            $i++;
        }
        return response()->json($resp, 200, $headers);
    }

    /**
     * Omzet Hariap Pos API
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function omzetHarian(Request $request)
    {
        $headers = [
            'Access-Control-Allow-Origin' => '*, http://pos.kelongtongku.com/',
        ];
        $penjualan = new BarangFakturPenjualan();
        $tgl_start = $request->tgl_start;
        $tgl_end   = $request->tgl_end;

        $grafikOmzet = $penjualan
            ->addSelect(DB::raw('sum(unit_price * jumlah) as omzet'))
            ->addSelect(DB::raw('DATE_FORMAT(date(created_at), "%d/%m/%Y") as tgl_transaksi'))
            ->groupBy(DB::raw('date(created_at)'))
            ->whereRaw('date(created_at) between ? AND ?', [$tgl_start, $tgl_end])
            ->where(DB::raw('year(created_at)'), date('Y'))
            ->get();

        $resp = [];

        foreach ($grafikOmzet as $row) {
            $resp[] = [
                'tanggal' => $row->tgl_transaksi,
                'omzet'   => $row->omzet,
            ];
        }

        return response()->json($resp, 200, $headers);
    }
}
