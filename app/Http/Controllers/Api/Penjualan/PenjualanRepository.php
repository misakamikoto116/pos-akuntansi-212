<?php

namespace App\Http\Controllers\Api\Penjualan; 

use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\BarangPengirimanPenjualan;
use App\Modules\Akuntansi\Models\BarangPesananPenjualan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\InvoicePenerimaanPenjualan;
use App\Modules\Akuntansi\Models\PenerimaanPenjualan;
use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use App\Modules\Akuntansi\Models\PesananPenjualan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Pos\Models\KasirCek;
use App\Modules\Pos\Models\KasirMesin;
use App\Modules\Pos\Models\PaymentHistory;
use App\Modules\Pos\Models\PreferensiPos;
use App\Modules\Pos\Models\TransactionNonCash;

//Additional
use Auth;
use Carbon\Carbon;
use Cart;
use DB;
use Generator\Interfaces\RepositoryInterface;
use Helpers\TransaksiHelper;
use PDF;
use Session;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class PenjualanRepository implements RepositoryInterface 
{
    public function __construct(
        Produk $produk,
        PreferensiPos $preferensiPos,
        PengirimanPenjualan $pengirimanPenjualan,
        BarangPengirimanPenjualan $barangPengirimanPenjualan,
        FakturPenjualan $fakturPenjualan,
        Transaksi $transaksi,
        SaldoAwalBarang $saldoAwalBarang,
        TransaksiHelper $transaksiHelper,
        Gudang $gudang,
        KasirMesin $kasirMesin,
        KasirCek $kasirCek,
        InformasiPelanggan $informasiPelanggan,
        PenerimaanPenjualan $penerimaanPenjualan,
        Identitas $identitas,
        InvoicePenerimaanPenjualan $invoicePenerimanaanPenjualan,
        TransactionNonCash $transactionNonCash,
        PesananPenjualan $pesanan_penjualan,
        BarangPesananPenjualan $barang_pesanan_penjualan
    ) {
        $this->produk                           = $produk;
        $this->preferensiPos                    = $preferensiPos;
        $this->pengirimanPenjualan              = $pengirimanPenjualan;
        $this->barangPengirimanPenjualan        = $barangPengirimanPenjualan;
        $this->fakturPenjualan                  = $fakturPenjualan;
        $this->transaksi                        = $transaksi;
        $this->saldoAwalBarang                  = $saldoAwalBarang;
        $this->transaksiHelper                  = $transaksiHelper;
        $this->gudang                           = $gudang;
        $this->kasirMesin                       = $kasirMesin;
        $this->kasirCek                         = $kasirCek;
        $this->informasiPelanggan               = $informasiPelanggan;
        $this->penerimaanPenjualan              = $penerimaanPenjualan;
        $this->identitas                        = $identitas;
        $this->invoicePenerimanaanPenjualan     = $invoicePenerimanaanPenjualan;
        $this->transactionNonCash               = $transactionNonCash;
        $this->pesanan_penjualan                = $pesanan_penjualan;
        $this->barang_pesanan_penjualan         = $barang_pesanan_penjualan;
    }

     /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        // TODO: Implement getItems() method.
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        // TODO: Implement findItem() method.
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id      [description]
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function update($id, $request)
    {
        // TODO: Implement update() method.
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($request)
    {
        // TODO: Implement insert() method.
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $request
     */
    public function filter($request)
    {
        // TODO: Implement filter() method.
    }
}