<?php

namespace App\Http\Controllers\Api\Penjualan;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\FakturPenjualan;

class ApiPenjualanController extends ApiController
{
    protected $fakturPenjualan; 
    public function __construct(Transaksi $transaksi, FakturPenjualan $fakturPenjualan)
    {
        $this->transaksi        = $transaksi;
        $this->fakturPenjualan  = $fakturPenjualan;
    }
    
    public function ListTransaksi(Request $request)
    {
        $items = [];
        $temp  = $this->getTransaksiModal()->orderBy('invoice_date','asc')->get()->map(function($item) use(&$items){
            if(!isset($items[Carbon::parse($item->invoice_date)->format('Y_m_d')])){
                $items[Carbon::parse($item->invoice_date)->format('Y_m_d')] = [
                    'tanggal'           => Carbon::parse($item->invoice_date)->format('d F Y'),
                    'total_transaksi'   => 0,
                    'harga_modal'       => 0,
                    'laba'              => 0,
                    'tanggal_formatted' => Carbon::parse($item->invoice_date)->format('Y-m-d')
                ];
            }

            $items[Carbon::parse($item->invoice_date)->format('Y_m_d')]['total_transaksi'] += $item->transaksi->where('dari', 'Penjualan dengan Tax & Disc')->sum('nominal');
            $items[Carbon::parse($item->invoice_date)->format('Y_m_d')]['harga_modal'] += $item->transaksi->where('dari', 'Penjualan HPP')->sum('nominal');
            $items[Carbon::parse($item->invoice_date)->format('Y_m_d')]['laba'] += $items[Carbon::parse($item->invoice_date)->format('Y_m_d')]['total_transaksi'] - $items[Carbon::parse($item->invoice_date)->format('Y_m_d')]['harga_modal'];

        });
        // dd($items);

        if($items != null){
            $status     = true;
            $message    = "Ok :)";
            $response   = 200;
        }else{
            $status     = false;
            $message    = "not found !";
            $response   = 404;
        }
        
        return response()->json([
            'status'    => $status,
            'message'   => $message,
            'data'      => $items
        ], $response);
    }

    public function getTransaksiModal()
    {
        return $this->fakturPenjualan->with(['transaksi', 'kasirMesin', 'barang.produk'])->has('transaksi')->where('status_modul', 1)->whereNotNull('kasir_mesin_id');   
    }


    public function detailTransaksi(Request $request)
    {
        $transaksi = $this->getTransaksiModal()->DateFilter($request)->get();
        $data      = $this->dataCekOut($transaksi);

        if($data->isEmpty()){
            $status     = true;
            $message    = "Ok :)";
            $response   = 200;
        }else{
            $status     = false;
            $message    = "not found !";
            $response   = 404;
        }
        
        return response()->json([
            'status'    => $status,
            'message'   => $message,
            'data'      => $data
        ], $response);         
    }

    public function dataCekOut($data)
    {
        return $data->map(function ($item_data)
        {
            $total_transaksi = $item_data->transaksi->where('dari', 'Penjualan dengan Tax & Disc')->sum('nominal');
            $harga_modal     = $item_data->transaksi->where('dari','Penjualan HPP')->sum('nominal');
            $laba            = $total_transaksi - $harga_modal;
            return [
                'id'                => $item_data->id,
                'no_transaksi'      => $item_data->no_faktur,
                'tanggal'           => Carbon::parse($item_data->invoice_date)->format('d F Y'),
                'jumlah_item'       => $item_data->barang->count(),
                'kasir'             => $item_data->user->name,
                'total_transaksi'   => $total_transaksi,
                'harga_modal'       => $harga_modal,
                'laba'              => $laba,
            ];
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $request->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
