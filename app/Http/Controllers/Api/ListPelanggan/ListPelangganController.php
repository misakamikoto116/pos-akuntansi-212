<?php

namespace App\Http\Controllers\Api\ListPelanggan;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\ListPelanggan\ListPelangganCollection;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use Illuminate\Http\Request;

class ListPelangganController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // TODO Api list Pelanggan
    // 1. membuat controller ListPelangganController
    // 2. membuat resource ListPelangganResource
    // 3. membuat collection ListPelangganCollection

    public function __construct(InformasiPelanggan $informasi_pelanggan)
    {
        $this->informasi_pelanggan      = $informasi_pelanggan;
    }

    public function index()
    {
        $informasi_pelanggan    = $this->informasi_pelanggan->where('tipe_pelanggan_id', 6)->get();

        if((new ListPelangganCollection($informasi_pelanggan))->isEmpty()) {
            return response()->json([
                'status'        => false,
                'message'       => 'data tidak ditemukan',
                'data'          => null,
            ], 404);
        }

        return new ListPelangganCollection($informasi_pelanggan);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
