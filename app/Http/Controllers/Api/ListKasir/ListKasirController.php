<?php

namespace App\Http\Controllers\Api\ListKasir;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Listkasir\ListKasirCollection;
use App\Modules\Pos\Models\KasirMesin;
use Illuminate\Http\Request;

class ListKasirController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // TODO Membuat Api List kasir
    // 1. membuat controller api ListKasirController
    // 2. membuat resource api ListKasirResource
    // 3. membuat collection api ListKasirCollection

    public function __construct(KasirMesin $kasir_mesin)
    {
        $this->kasir_mesin = $kasir_mesin;
    }

    public function index()
    {
        $list_kasir = $this->kasir_mesin->get();

        if ((new ListKasirCollection($list_kasir))->isEmpty()) {

            return response()->json([
                'status'        => false,
                'message'       => 'tidak ditemukan',
                'data'          => null
            ], 404);

        }

        return new ListKasirCollection($list_kasir);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
