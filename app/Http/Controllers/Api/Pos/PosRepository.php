<?php
/**
 * Created by PhpStorm.
 * User: efendihariyadi
 * Date: 19/08/18
 * Time: 00.51
 */

namespace App\Http\Controllers\Api\Pos;


use DB;
use Helpers\TransaksiHelper;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Pos\Models\PreferensiPos;
use App\Modules\Pos\Models\PaymentHistory;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Pos\Models\RiwayatPembayaran;
use Generator\Interfaces\RepositoryInterface;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\PesananPenjualan;
use App\Modules\Akuntansi\Models\PenerimaanPenjualan;
use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\BarangPesananPenjualan;
use App\Modules\Akuntansi\Models\BarangPengirimanPenjualan;
use App\Modules\Akuntansi\Models\InvoicePenerimaanPenjualan;

class PosRepository implements RepositoryInterface
{
    protected $pengirimanPenjualan;
    protected $barangPengirimanPenjualan;
    protected $fakturPenjualan;
    protected $barangFakturPenjualan;
    protected $penerimaanPenjualan;
    protected $produk;
    protected $transaksi;
    protected $preferensiPos;
    protected $saldoAwalBarang;
    protected $transaksiHelper;
    protected $pesananPenjualan;
    protected $paymentHistory;
    protected $invoice;
    protected $riwayatPembayaran;
    public function __construct(PengirimanPenjualan $pengirimanPenjualan,
                                BarangPengirimanPenjualan $barangPengirimanPenjualan,
                                FakturPenjualan $fakturPenjualan,
                                BarangFakturPenjualan $barangFakturPenjualan,
                                PenerimaanPenjualan $penerimaanPenjualan,
                                Produk $produk,
                                Transaksi $transaksi,
                                PreferensiPos $preferensiPos,
                                SaldoAwalBarang $saldoAwalBarang,
                                TransaksiHelper $transaksiHelper,
                                PesananPenjualan $pesananPenjualan,
                                PaymentHistory $paymentHistory,
                                InvoicePenerimaanPenjualan $invoice,
                                RiwayatPembayaran $riwayatPembayaran
    )
    {
        $this->pengirimanPenjualan          = $pengirimanPenjualan;
        $this->barangPengirimanPenjualan    = $barangPengirimanPenjualan;
        $this->fakturPenjualan              = $fakturPenjualan;
        $this->barangFakturPenjualan        = $barangFakturPenjualan;
        $this->penerimaanPenjualan          = $penerimaanPenjualan;
        $this->produk                       = $produk;
        $this->transaksi                    = $transaksi;
        $this->preferensiPos                = $preferensiPos;
        $this->saldoAwalBarang              = $saldoAwalBarang;
        $this->transaksiHelper              = $transaksiHelper;
        $this->pesananPenjualan             = $pesananPenjualan;
        $this->paymentHistory               = $paymentHistory;
        $this->invoice                      = $invoice;
        $this->riwayatPembayaran            = $riwayatPembayaran;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        // TODO: Implement getItems() method.
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        // TODO: Implement findItem() method.
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id      [description]
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function update($id, $request)
    {
        // TODO: Implement update() method.
    }

    /**
     * menambahkan data array ke masing-masing table. untuk pos penjualan
     * @param $data array
     * @return array diPakai utuk API Response
     */
    public function insert($data)
    {
        \DB::beginTransaction();

        try{
             // Pesanan Penjualan Master & Detail 
            $pesananPenjualan = $this->pesananPenjualan->fill($data['pesanan_penjualan']['master']);
            if($pesananPenjualan->save()){
                $this->InsertBarangPesananPenjualan($data['barang_pesanan_penjualan']['detail'], $pesananPenjualan);
            }

            // Pengiriman Penjualan Master & Detail
            $pengirimanPenjualan = $this->pengirimanPenjualan->fill($data['pengiriman_penjualan']['master']);
            if($pengirimanPenjualan->save()){
                $this->InsertBarangPengirimanPenjualan($data['barang_pengiriman_penjualan']['detail'], $pengirimanPenjualan);
            }
            
            // Transaksi Pengiriman Penjualan 
            $transaksi_terkirim = $this->InsertTransaksiTerkirim($data['transaksi_terkirim'], $pengirimanPenjualan, $data['produk_detail']);
            
            $transaksi_persediaan = $this->InsertTransaksiPersediaan($data['transaksi_persediaan'], $pengirimanPenjualan);
            
            // Faktur Penjualan Master & Detail
            $fakturPenjualan = $this->fakturPenjualan->fill($data['faktur_penjualan']['master']);
            if($fakturPenjualan->save()){
                    $this->InsertBarangFakturPenjualan($data['barang_faktur_penjualan']['detail'], $fakturPenjualan, $pengirimanPenjualan);
            }

            // Transaksi Pajak
            // $transaksi_pajak = $this->TransaksiPajak($data['transaksi_pajak'], $fakturPenjualan);

            // Transaksi Faktur Penjualan 
            $transaksi_debit = $this->TransaksiFakturDebit($data['transaksi_array_debit'], $fakturPenjualan);
            
            $transaksi_kredit = $this->TransaksiFakturKredit($data['transaksi_array_kredit'], $fakturPenjualan);

            // Penerimaan Penjualan 
            $penerimaanPenjualan = $this->penerimaanPenjualan->fill($data['penerimaan_penjualan']);
            $penerimaanPenjualan->save();
            
            // Invoice Penerimaan penjualan 
            $invoicePenerimaanPenjualan = $this->invoicePenerimaanPenjualan($data['invoice_penerimaan_penjualan'], $penerimaanPenjualan);
            
            // Transaksi Penerimaan Penjualan 
            $trans_penerimaanPenjualan_kredit = $this->TransaksiPenerimaanPenjualanKredit($data['penerimaan_penjualan_kredit'], $penerimaanPenjualan);
            
            $trans_penerimaanPenjualan_debit = $this->TransaksiPenerimaanPenjualanDebit($data['penerimaan_penjualan_debit'], $penerimaanPenjualan);
            
            // isi Payment History
            $paymentHistory = $this->paymentHistory->fill($data['payment_history']);
            $paymentHistory->save();

            // Riwayat Pembayaran 
            $data['riwayat_pembayaran']['faktur_penjualan_id'] = $fakturPenjualan->id;
            $riwayatPembayaran = $this->riwayatPembayaran->fill($data['riwayat_pembayaran']);
            $riwayatPembayaran->save();
            
            \DB::commit();
        }catch (\Exception $exc){
            DB::rollBack();
            $res = ['error' => $exc->getMessage()];
            dd($exc->getMessage());
            \Log::error('insert penjualan',[$exc->getMessage()]);
        }
        return true;
    }

    // Detail Pesanan Penjualan 
    protected function InsertBarangPesananPenjualan($data, $model){
        $items = [];
        $jam_sekarang = date('Y-m-d H:i:s');
        foreach ($data as $item):
            $items[] = [
                'produk_id'                     => $item['produk_id'],
                'pesanan_penjualan_id'          => $model->id,
                'barang_penawaran_penjualan_id' => null,
                'item_deskripsi'                => $item['item_deskripsi'],
                'jumlah'                        => $item['jumlah'],
                'harga'                         => $item['harga'],
                'diskon'                        => $item['diskon'],
                'terproses'                     => $item['terproses'],
                'ditutup'                       => $item['ditutup'],
                'satuan'                        => $item['satuan'],
                'kode_pajak_id'                 => $item['kode_pajak_id'],
                'harga_modal'                   => $item['harga_modal'],
                'created_by'                    => auth()->user()->id,
            ];
        endforeach;
        return BarangPesananPenjualan::insert($items);
    }

    // Detail Barang Pengiriman Penjualan 
    protected function InsertBarangPengirimanPenjualan($data, $model){
        $items = [];
        $jam_sekarang = date('Y-m-d H:i:s');
        foreach ($data as $item):
            $items[] = [
                'produk_id'                 => $item['produk_id'],
                'pengiriman_penjualan_id'   => $model->id,
                'item_deskripsi'            => $item['item_deskripsi'],
                'item_unit'                 => $item['item_unit'],
                'harga_modal'               => $item['harga_modal'],
                'status'                    => $item['status'],
                'jumlah'                    => $item['jumlah'],
                'created_by'                => auth()->user()->id,
                'created_at'                => $jam_sekarang
            ];
        endforeach;
        
        return BarangPengirimanPenjualan::insert($items);
    }

    // Transaksi Pengiriman Penjualan ['barang terkirim']
    protected function InsertTransaksiTerkirim($data, $model, $produk_detail){
        $items = [];
        $jam_sekarang = date('Y-m-d H:i:s');

        foreach($data as $key => $item) :
            $transaksi = Transaksi::create(
                [
                    'nominal'               => $item['nominal'],
                    'tanggal'               => $item['tanggal'],
                    'akun_id'               => $item['akun_id'],
                    'item_id'               => $model->id,
                    'item_type'             => get_class($model),
                    'status'                => 1,
                    'produk_id'             => $item['produk_id'],
                    'no_transaksi'          => $model->delivery_no,
                    'desc_dev'              => $item['desc_dev'],
                    'sumber'                => $item['sumber'],
                    'dari'                  => $item['dari'],
                    'keterangan'            => $item['keterangan'],
                    'status_rekonsiliasi'   => $item['status_rekonsiliasi'],
                    'created_by'            => auth()->user()->id,
                ]
            );

            $produk_detail_create = SaldoAwalBarang::create([
                'no_faktur'         => $transaksi->no_transaksi,
                'tanggal'           => $produk_detail[$key]['tanggal'],
                'kuantitas'         => $produk_detail[$key]['kuantitas'],
                'harga_modal'       => $produk_detail[$key]['harga_modal'],
                'harga_terakhir'    => $produk_detail[$key]['harga_terakhir'],
                'gudang_id'         => $produk_detail[$key]['gudang_id'],
                'produk_id'         => $produk_detail[$key]['produk_id'],
                'transaksi_id'      => $transaksi->id,
                'sn'                => $produk_detail[$key]['sn'],
                'status'            => $produk_detail[$key]['status'],
                'item_type'         => get_class($model),
                'item_id'           => $model->id,
                'created_by'        => auth()->user()->id,
            ]);

        endforeach;
        return true;
    }

    // Transaksi Pengiriman Penjualan ['persediaan']
    protected function InsertTransaksiPersediaan($data, $model){
        $items = [];
        $jam_sekarang = date('Y-m-d H:i:s');
        
        foreach($data as $item) :
            $items[] = $this->transaksi->create([
                'nominal'               => $item['nominal'],
                'tanggal'               => $item['tanggal'],
                'akun_id'               => $item['akun_id'],
                'item_id'               => $model->id,
                'item_type'             => get_class($this->pengirimanPenjualan),
                'status'                => 0,
                'produk_id'             => $item['produk_id'],
                'no_transaksi'          => $model->delivery_no,
                'desc_dev'              => $item['desc_dev'],
                'sumber'                => $item['sumber'],
                'dari'                  => $item['dari'],
                'keterangan'            => $item['keterangan'],
                'status_rekonsiliasi'   => $item['status_rekonsiliasi'],
                'created_by'            => auth()->user()->id,
            ]);
        endforeach;
        return true;
    }

    // Detail Faktur Penjualan 
    protected function InsertBarangFakturPenjualan($data, $model, $pengiriman){
        $items = [];
        $jam_sekarang = date('Y-m-d H:i:s');
        $total = 0;
        foreach ($data as $key => $item):
            $total += $item['jumlah'] * floatval($item['unit_price']);
            $bagian_diskon_master = ( $item['unit_price'] / $total ) * ( $item['diskon'] ?? 0 );
            $harga_final = number_format((float) ($item['unit_price'] - $bagian_diskon_master), 2, '.', '');
            $items[] = [
                'produk_id'                         => $item['produk_id'],
                'faktur_penjualan_id'               => $model->id,
                'barang_pengiriman_penjualan_id'    => $pengiriman->barang[$key]->id,
                'item_deskripsi'                    => $item['item_deskripsi'],
                'item_unit'                         => $item['item_unit'],
                'jumlah'                            => $item['jumlah'],
                'diskon'                            => $item['diskon'],
                'harga_modal'                       => $item['harga_modal'],
                'kode_pajak_id'                     => $item['kode_pajak_id'],
                'kode_pajak_2_id'                   => $item['kode_pajak_2_id'],
                'gudang_id'                         => $item['gudang_id'],
                'unit_price'                        => $item['unit_price'],
                'harga_modal'                       => $item['harga_modal'],
                'harga_final'                       => $harga_final,
                'bagian_diskon_master'              => $bagian_diskon_master,
                'created_at'                        => $jam_sekarang,
                'created_by'                        => auth()->user()->id
            ];
        endforeach;
        return BarangFakturPenjualan::insert($items);
    }

    // Transaksi Faktur Penjualan Debit 
    protected function TransaksiFakturDebit($data, $model){
        // $transaksi = [];
        $carbonNow = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        foreach ($data ?? [] as $debit) :

            $this->transaksi->create([
                'nominal'               => $debit['nominal'],
                'tanggal'               => $carbonNow,
                'akun_id'               => $debit['akun'],
                'produk_id'             => $debit['produk'],
                'item_id'               => $model->id,
                'item_type'             => get_class($this->fakturPenjualan),
                'status'                => 1,
                'no_transaksi'          => $model->no_faktur,
                'sumber'                => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->fakturPenjualan)))->getShortName(), ' '),
                'keterangan'            => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->fakturPenjualan)))->getShortName(), ' ').' '. 1,
                'dari'                  => $debit['dari'],
                'status_rekonsiliasi'   => null,
                'desc_dev'              => '',
                'created_by'            => auth()->user()->id,
                'created_at'            => $carbonNow,
            ]);
        endforeach;
        return true;
    }
    
    // Transaksi Faktur Penjualan Kredit 
    protected function TransaksiFakturKredit($data, $model){
        $carbonNow = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        // $in = [];

        foreach ($data ?? [] as $kredit) {
            $this->transaksi->create([
                'nominal'               => $kredit['nominal'],
                'tanggal'               => $carbonNow,
                'akun_id'               => $kredit['akun'],
                'produk_id'             => $kredit['produk'],
                'item_id'               => $model->id,
                'item_type'             => get_class($this->fakturPenjualan),
                'status'                => 0,
                'no_transaksi'          => $model->no_faktur,
                'sumber'                => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->fakturPenjualan)))->getShortName(), ' '),
                'keterangan'            => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->fakturPenjualan)))->getShortName(), ' ').' '. 1,
                'dari'                  => $kredit['dari'],
                'status_rekonsiliasi'   => null,
                'desc_dev'              => '',
                'created_by'            => auth()->user()->id,
                'created_at'            => $carbonNow,
            ]);
        }
        
        return true;
    }
    
    protected function InsertPersediaanBarang($data, $model)
    {
        $item = [];
        $jam_sekarang = date('Y-m-d H:i:s');
        foreach ($data as $item):
            $items[] = [
                'nominal'               => $item['nominal'],
                'tanggal'               => $item['tanggal'],
                'akun_id'               => $item['akun_id'],
                'item_id'               => $item['item_id'],
                'item_type'             => get_class($this->pengirimanPenjualan),
                'status'                => 1,
                'produk_id'             => $item['produk_id'],
                'no_transaksi'          => $item['no_transaksi'],
                'desc_dev'              => $item['desc_dev'],
                'sumber'                => $item['sumber'],
                'dari'                  => $item['dari'],
                'keterangan'            => $item['keterangan'],
                'status_rekonsiliasi'   => $item['status_rekonsiliasi'],
                'created_by'            => auth()->user()->id,
            ];
        endforeach;
        return Transaksi::insert($data);
    }

    protected function TransaksiPenerimaanPenjualanKredit($data, $model)
    {
        $items = [];
        $carbonNow = \Carbon\Carbon::now()->format('Y-m-d H:i:s');

        foreach ($data ?? [] as $item) {
            $items[] = $this->transaksi->create([
                'nominal'               => $data['nominal'],
                'tanggal'               => $carbonNow,
                'akun_id'               => $data['akun'],
                'item_id'               => $this->penerimaanPenjualan->id,
                'item_type'             => get_class($this->penerimaanPenjualan),
                'status'                => 0,
                'no_transaksi'          => $this->penerimaanPenjualan->form_no,
                'sumber'                => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->penerimaanPenjualan)))->getShortName(), ' '),
                'keterangan'            => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->penerimaanPenjualan)))->getShortName(), ' ').' '. 1,
                'dari'                  => $data['dari'],
                'status_rekonsiliasi'   => null,
                'desc_dev'              => '',
                'created_by'            => auth()->user()->id,
                'created_at'            => $carbonNow,
            ]);
        }

        return true;
    }

    protected function TransaksiPenerimaanPenjualanDebit($data, $model)
    {
        // $items = [];
        $carbonNow = \Carbon\Carbon::now()->format('Y-m-d H:i:s');

        foreach ($data as $item){
            $this->transaksi->create([
                'nominal'               => $data['nominal'],
                'tanggal'               => $carbonNow,
                'akun_id'               => $data['akun'],
                'item_id'               => $this->penerimaanPenjualan->id,
                'item_type'             => get_class($this->penerimaanPenjualan),
                'status'                => 1,
                // 'no_transaksi'          => $this->penerimaanPenjualan->form_no,
                'sumber'                => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->penerimaanPenjualan)))->getShortName(), ' '),
                'keterangan'            => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->penerimaanPenjualan)))->getShortName(), ' ').' '. 1,
                'dari'                  => $data['dari'],
                'status_rekonsiliasi'   => null,
                'desc_dev'              => '',
                'created_by'            => auth()->user()->id,
                'created_at'            => $carbonNow,
            ]);
        }
        
        return true;
    }

    protected function InsertPaymentHistory($data, $model){
        $item = [];
        $jam_sekarang = date('Y-m-d H:i:s');
        foreach ($data as $item):
            $items[] = [
                'penerimaan_penjualan_id'   => $item['penerimaan_penjualan_id'],
                'status'                    => $item['status'],
                'total'                     => $item['total'],
                'created_by'                => $jam_sekarang,
            ];
        endforeach;
        return PaymentHistory::insert($data);
    }

    protected function invoicePenerimaanPenjualan($data, $model)
    {
        $jam_sekarang = date('Y-m-d H:i:s');

        $items = [
            'faktur_id'                 => $this->fakturPenjualan->id,
            'penerimaan_penjualan_id'   => $this->penerimaanPenjualan->id,
            'tanggal'                   => $data['tanggal'],
            'payment_amount'            => $data['payment_amount'],
            'last_owing'                => $data['last_owing'],
            'diskon'                    => $data['diskon'],
            'created_at'                => $jam_sekarang,
        ];
        return InvoicePenerimaanPenjualan::create($items);
    }

    public function filter($request)
    {
        // TODO: Implement filter() method.
    }
}