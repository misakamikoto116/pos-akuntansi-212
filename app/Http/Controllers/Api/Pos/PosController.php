<?php
/**
 * User: efendihariyadi
 * Date: 17/08/18
 * Time: 00.37
 */

namespace App\Http\Controllers\Api\Pos;

use Carbon\Carbon;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Pos\Models\PreferensiPos;
use App\Http\Resources\Api\ProdukResource;
use App\Modules\Pos\Models\PaymentHistory;
use App\Http\Controllers\Api\ApiController;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Http\Resources\Api\ProdukCollection;
use App\Http\Resources\Api\Pos\PosCollection;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\PesananPenjualan;
use App\Http\Controllers\Api\Produk\ProdukRepository;
use App\Modules\Akuntansi\Models\PenerimaanPenjualan;
use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\BarangPesananPenjualan;
use App\Modules\Akuntansi\Models\BarangPengirimanPenjualan;

class PosController extends ApiController
{
    protected $code;
    protected $posRepository;
    protected $preferensiPos;
    protected $identitas;
    protected $pengirimanPenjualan;
    protected $transaksiHelpers;
    public function __construct(CodeHelper $code, Produk $produk, PosRepository $posRepository,
                                produkRepository $produkRepository, PreferensiPos $preferensiPos, Identitas $identitas, FakturPenjualan $fakturPenjualan, BarangFakturPenjualan $barangFaktur, BarangPesananPenjualan $barangPesanan, PesananPenjualan $pesananPenjualan, PengirimanPenjualan $pengirimanPenjualan, BarangPengirimanPenjualan $barangPengiriman, Transaksi $transaksi, PaymentHistory $paymentH, TransaksiHelper $transaksiHelpers, PenerimaanPenjualan $penerimaanPenjualan, SaldoAwalBarang $produk_detail)
    {
        $this->code                         = $code;
        $this->produk                       = $produk;
        $this->produkRepository             = $produkRepository;
        $this->posRepository                = $posRepository;
        $this->preferensiPos                = $preferensiPos;
        $this->identitas                    = $identitas;
        $this->fakturPenjualan              = $fakturPenjualan;
        $this->barangFaktur                 = $barangFaktur;
        $this->barangPesanan                = $barangPesanan;
        $this->barangPengiriman             = $barangPengiriman;
        $this->transaksi                    = $transaksi;
        $this->paymentH                     = $paymentH;
        $this->pengirimanPenjualan          = $pengirimanPenjualan;
        $this->transaksiHelpers             = $transaksiHelpers;
        $this->penerimaanPenjualan          = $penerimaanPenjualan;
        $this->produk_detail                = $produk_detail;
        $this->pesananPenjualan             = $pesananPenjualan;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return ProdukCollection|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function findByBarcode(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'barcode' => 'required|min:5|max:50|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'input tidak lengkap',
                'data' => null
            ], 400);
        }
        $produk = Produk::where('no_barang','like', $request->barcode)
            ->with('saldoAwalBarang')->first() ;
        if(is_null($produk)){
            return response()->json([
                'status' => false,
                'message' => 'tidak ditemukan',
                'data' => null
            ], 404);
        }
        $resp = response()->json([
            'status' => true,
            'message' => 'data ditemukan',
            'data' => new ProdukResource($produk)
        ], 200);
        return $resp;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($barcode)
    {
        $produk = Produk::where('no_barang','=', $barcode)
            ->with('saldoAwalBarang')
            ->where('tipe_barang','!==',3)
            ->where('gudang_id','=',6)->first();
        if(is_null($produk)){
            return response()->json([
                'status' => false,
                'message' => 'tidak ditemukan',
                'data' => [null]
            ], 404);
        }
        $resp = response()->json([
            'status' => true,
            'message' => 'data ditemukan',
            'data' => new ProdukResource($produk)
        ], 200);
        return $resp;
    }

    /**
     * Proses insert dta penjualan
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $items_array = json_decode($request->items, true);
        
        if(empty($items_array))
            return response()->json([
                'status' => false,
                'message' => 'tidak ada data barang/format request tidak benar',
                'data' => [null]
            ], 400);
        $total = 0;
        $dataToRepository = [];
        $carbonNow = \Carbon\Carbon::now()->format('Y-m-d H:i:s');

        // Pesanan Penjualan Master 
        $dataToRepository['pesanan_penjualan']['master'] = [
            'pelanggan_id'          => ($request->has('pelanggan_id') ? $request->pelanggan_id : 6),
            'pelanggan_kirim_ke_id' => $request->pelanggan_kirim_ke_id,
            'alamat_pengiriman'     => $request->alamat_pengiriman,
            'alamat_tagihan'        => $request->alamat_tagihan,
            'taxable'               => $request->taxable,
            'in_tax'                => $request->in_tax,
            'diskon'                => $request->diskon,
            'total_potongan_rupiah' => $request->total_potongan_rupiah,
            'ongkir'                => $request->ongkir,
            'total'                 => $request->total,
            'status'                => 1,
            'keterangan'            => $request->keterangan,
            'approved'              => 0,
            'po_number'             => $request->po_number,
            'so_number'             => $request->so_number,
            'so_date'               => date('Y-m-d H:i:s'),
            'ship_date'             => date('Y-m-d H:i:s'),
            'fob'                   => $request->fob,
            'term_id'               => $request->term_id,
            'akun_dp_id'            => $request->akun_dp_id,
            'ship_id'               => $request->ship_id,
            'nilai_pajak'           => $request->nilai_pajak,
            'created_by'            => auth()->user()->id,
        ];
        
        // Detail Pesanan Penjualan 
        foreach ($items_array as $item):
            
        $barangPesanan = $this->produk->where('id',$item['id'])->first();
        $dataToRepository['barang_pesanan_penjualan']['detail'][] = [
                'produk_id'                     => $barangPesanan['id'],
                // 'pesanan_penjualan_id'          => $this->pesananPenjualan->id,
                'item_deskripsi'                => $barangPesanan['keterangan'],
                'jumlah'                        => $item['qty'],
                'harga'                         => $item['harga'],
                'diskon'                        => $item['diskon'],
                'terproses'                     => 1,
                'ditutup'                       => 0,
                'satuan'                        => $barangPesanan['unit'],
                'kode_pajak_id'                 => $barangPesanan['kode_pajak_id'],
                'harga_modal'                   => $barangPesanan['produk_detail']['harga_modal'],
            ];  
        endforeach;
        
        // Pengiriman Penjualan Master 
        $dataToRepository['pengiriman_penjualan']['master'] = [
            'pelanggan_id'          => ($request->has('pelanggan_id') ? $request->pelanggan_id : 6),
            'pelanggan_kirim_ke_id' => $request->pelanggan_kirim_ke_id,
            'alamat_tagihan'        => $request->alamat_tagihan,
            'alamat_pengiriman'     => '-',
            'delivery_no'           => $this->generateNoPenjualan(),
            'po_no'                 => $this->code->autoGenerate($this->pengirimanPenjualan->orderBy('id', 'desc')->first()->toArray(), "po_no", $this->pengirimanPenjualan->orderBy('id', 'DESC')->first()->po_no ?? 1),
            'ship_id'               => $request->ship_id,
            'keterangan'            => $request->keterangan,    
            'cetak'                 => false,
            'status_modul'          => false,
            'kasir_mesin_id'        => $request->kasir_mesin_id,
            'created_by'            => auth()->user()->id,
        ];
        
        // { Detail Pengiriman Penjualan
        foreach ($items_array as $item):
            $barang = $this->produk->find($item['id']); 
            $dataToRepository['barang_pengiriman_penjualan']['detail'][] = [
                'produk_id'                 => $barang['id'],
                // 'pengiriman_penjualan_id'   => $barang['pengiriman_penjualan_id'],
                'item_deskripsi'            => $barang['keterangan'],
                'item_unit'                 => $barang['satuan'],
                'harga_modal'               => $barang['produk_detail']['harga_modal'],
                'status'                    => 0,
                'jumlah'                    => intval($item['qty']),
                'created_at'                => date('Y-m-d H:i:s')
            ];
            
            // Transaksi Pengiriman Penjualan ['barang terkirim']            
            $barangTerkirim = [
                'nominal'   => $barang['produk_detail']['harga_modal'] * $item['qty'],
                'tanggal'   => date('Y-m-d H:i:s'),
                'produk'    => $barang['produk_detail']['produk_id'],
                'akun'      => $barang['akun_barang_terkirim_id'],
                'dari'      => 'Barang Terkirim'
            ];
            
            $dataToRepository['transaksi_terkirim'][] = [
                'nominal'             => $barangTerkirim['nominal'],
                'tanggal'             => $barangTerkirim['tanggal'],
                'akun_id'             => $barangTerkirim['akun'],
                'item_id'             => $request->item_id,
                'item_type'           => get_class($this->pengirimanPenjualan),
                'status'              => 1,
                'produk_id'           => $barangTerkirim['produk'],
                // 'no_transaksi'        => $barang->delivery_no,
                'desc_dev'            => $request->desc_dev,
                'sumber'              => $this->transaksiHelpers->fromCamelCase((new \ReflectionClass(get_class($this->pengirimanPenjualan)))->getShortName(), ' '),
                'keterangan'          => $this->transaksiHelpers->fromCamelCase((new \ReflectionClass(get_class($this->pengirimanPenjualan)))->getShortName(), ' ').' '. 1,
                'dari'                => $barangTerkirim['dari'],
                'status_rekonsiliasi' => $request->status_rekonsiliasi,
                'created_by'          => auth()->user()->id,
            ];

            $dataToRepository['produk_detail'][] = [
                'tanggal'       => $carbonNow,
                'kuantitas'     => - $item['qty'],
                'harga_modal'   => $barang['produk_detail']['harga_modal'],
                'harga_terakhir'=> $barang['produk_detail']['harga_terakhir'],
                'gudang_id'     => ($request->has('gudang_id') ? $request->gudang_id : 6),
                // 'transaksi_id'  => $transaksi->id,
                'sn'            => null,
                'status'        => 1,
                // 'item_type'     => get_class($this->pengirimanPenjualan),
                'item_id'       => $this->pengirimanPenjualan->id,
                'produk_id'     => $barangTerkirim['produk'],
                'created_at'    => date('Y-m-d H:i:s'),
            ]; 

            // Transaksi Pengiriman Penjualan ['Persediaan']
            $persediaan = [
                'nominal' => $barang['produk_detail']['harga_modal'] * $item['qty'],
                'tanggal' => $carbonNow,
                'produk'  => $barang['produk_detail']['produk_id'],
                'akun'    => $barang['akun_persedian_id'],
                'dari'    => 'Persediaan',
            ];  
            
            $dataToRepository['transaksi_persediaan'][] = [
                'nominal'             => $persediaan['nominal'],
                'tanggal'             => $persediaan['tanggal'],
                'akun_id'             => $persediaan['akun'],
                'item_id'             => $request->item_id,
                'item_type'           => get_class($this->pengirimanPenjualan),
                'status'              => 0,
                'produk_id'           => $persediaan['produk'],
                // 'no_transaksi'        => $request->delivery_no,
                'desc_dev'            => $request->desc_dev,
                'sumber'              => $this->transaksiHelpers->fromCamelCase((new \ReflectionClass(get_class($this->pengirimanPenjualan)))->getShortName(), ' '),
                'keterangan'          => $this->transaksiHelpers->fromCamelCase((new \ReflectionClass(get_class($this->pengirimanPenjualan)))->getShortName(), ' ').' '. 0,
                'dari'                => $persediaan['dari'],
                'status_rekonsiliasi' => $request->status_rekonsiliasi,
                'created_by'          => date('Y-m-d H:i:s'),
            ];
        endforeach;
        
        // }
        
        // Faktur Penjualan Master 
        $total_bayar = $request->nilai_bayar - $request->nilai_kembalian;
        $nomerFaktur = 'POS/'.date('Y-m-d').'/'.$this->code->autoGenerate(with(new FakturPenjualan())->orderBy('id', 'desc')->first()->toArray(), 'id', 0);
        $dataToRepository['faktur_penjualan']['master'] = [
            'no_faktur'             => $nomerFaktur,
            'pelanggan_id'          => ($request->has('pelanggan_id') ? $request->pelanggan_id : 6),
            'alamat_pengiriman'     => '-',
            'diskon'                => 0, //dalam bentuk persen
            'po_no'                 => 1,
            'invoice_date'          => $carbonNow,
            'no_fp_std'             => $request->no_fp_std,
            'no_fp_std2'            => $request->no_fp_std2,
            'no_fp_std_date'        => $carbonNow,
            'ongkir'                => 0,
            'total'                 => $total_bayar,
            'keterangan'            => $request->keterangan,
            'status_modul'          => 1,
            'kasir_mesin_id'        => $request->kasir_mesin_id,
            'jumlah_diskon_faktur'  => 0, //dalam bentuk persen
            'created_by'            => auth()->user()->id, 
        ];
        
        // Detail Faktur Penjualan 
        $total_cart = 0;
        $harga_total_pajak = 0;
        foreach($items_array  as $item):
                    
        $barang = $this->produk->find($item['id']);
        
        // $subTotalDiskon = ($barang['harga_jual'] * ($barang['diskon'] ?? 0)) / 100;
        if($barang['diskon'] == 0 || $barang['diskon'] == null){
            $subTotalDiskon = $barang['harga_jual'] / 100;
        }else{
            $subTotalDiskon = ($barang['harga_jual'] * $barang['diskon']) / 100;
        }

        $priceReal      = $barang['harga_jual'] - $subTotalDiskon;
        $total_cart     += $priceReal * $item['qty'];
        
        if($total_cart == 0){
            $bagian_diskon_master   = ($priceReal / $total_cart);
        }else{
            $bagian_diskon_master   = ( $priceReal / $total_cart ) * ( $item['diskon'] ?? 0 );
        }

        $harga_final    = number_format((float) ($priceReal - $bagian_diskon_master), 2, '.', '');

        $barangFaktur = $this->produk->find($item['id']);
        $dataToRepository['barang_faktur_penjualan']['detail'][] = [
            'produk_id'                         => $barangFaktur['id'],
            // 'faktur_penjualan_id'               => $barangFaktur['faktur_penjualan_id'],
            // 'barang_pengiriman_penjualan_id'    => $barangFaktur['barang_pengiriman_penjualan_id'],
            'item_deskripsi'                    => $barangFaktur['keterangan'],
            'item_unit'                         => $barangFaktur['item_unit'],
            'jumlah'                            => $item['qty'],
            'diskon'                            => $item['diskon'],
            'kode_pajak_id'                     => $barangFaktur['kode_pajak_id'],
            'kode_pajak_2_id'                   => $barangFaktur['kode_pajak_2_id'],
            'gudang_id'                         => $barangFaktur['gudang_id'],
            'sn'                                => $barangFaktur['sn'],
            'unit_price'                        => $item['harga'], 
            'harga_modal'                       => $barangFaktur['produk_detail']['harga_modal'],
            // 'harga_final'                       => $barangFaktur['harga_final'],
            // 'bagian_diskon_master'              => $barangFaktur['bagian_diskon_master'],
        ];

        // Transaksi Faktur Penjualan 
        
        
        $preferensiPosPajak = $this->preferensiPos->first()->status == 1 ? $this->preferensiPos->first()->kodePajak->nilai : 0;        
        $total_pajak = ($harga_final *  $preferensiPosPajak) / 100;      
        $harga_total_pajak += ( $harga_final * $item['qty'] ) + $total_pajak ;

        if ($this->preferensiPos->first()->status == 1) {
            $arrayPajak = [
                'nominal'   => $total_pajak,
                'tanggal'   => $carbonNow,
                'produk'    => $barang['produk_detail']['produk_id'],
                'akun'      => $this->preferensiPos->first()->kodePajak->akun_pajak_penjualan_id,
                'dari'      => 'Pajak ' . $this->preferensiPos->first()->kode_pajak_id . ' per Item',
            ];

            $dataToRepository['pajak_preferensi'] = [
                'nominal'           => $arrayPajak['nominal'],
                'tanggal'           => $carbonNow,
                'akun_id'           => $arrayPajak['akun'],
                'produk_id'         => $arrayPajak['produk'] ?? null,
                'item_id'           => $this->fakturPenjualan->id,
                'item_type'         => get_class($this->fakturPenjualan),
                'status'            => 0,
                'no_transaksi'      => $this->fakturPenjualan->no_faktur,
                'sumber'            => $this->transaksiHelpers->fromCamelCase((new \ReflectionClass(get_class($this->fakturPenjualan)))->getShortName(), ' '),
                'keterangan'        => $this->transaksiHelpers->fromCamelCase((new \ReflectionClass(get_class($this->fakturPenjualan)))->getShortName(), ' ').' '. 1,
                'dari'              => $arrayPajak['dari'],
                'status_rekonsiliasi'   => null,
                'desc_dev'              => '',
                'created_at'            => $carbonNow,
            ];
        }

        $harga_jual_qty         = $barang['harga_jual'] * $item['qty'];
        $total_diskon           = ($harga_jual_qty * $item['diskon']) / 100;
        $total_barang           = $harga_jual_qty - $total_diskon;
        $total_harga_modal      = $barangFaktur['produk_detail']['harga_modal'] * $item['qty'];
        $akumulasi_diskon_item  = 0;
        $akumulasi_diskon_item += $total_diskon;

        $array_kredit[] = [
            'nominal'   => $harga_jual_qty,
            'tanggal'   => $carbonNow,
            'dari'      => 'Penjualan dengan Tax & Disc',
            'produk'    => $barang['produk_detail']['produk_id'],
            'akun'      => $barang['akun_penjualan_id']
        ];

        $array_kredit[] = [
            'nominal'   => $total_harga_modal,
            'tanggal'   => $carbonNow,
            'dari'      => 'Barang Terkirim',
            'produk'    => $barang['produk_detail']['produk_id'],
            'akun'      => $barang['akun_barang_terkirim_id']
        ];
        
        $array_debit[] = [
            'nominal'   => $total_harga_modal,
            'tanggal'   => $carbonNow,
            'dari'      => 'Penjualan HPP',
            'produk'    => $barang['produk_detail']['produk_id'],
            'akun'      => $barang->akun_hpp_id,
        ];
        
        $array_debit[] = [
            'nominal'   => $total_diskon,
            'tanggal'   => $carbonNow,
            'produk'    => $barang['produk_detail']['produk_id'],
            'dari'      => 'Selisih Diskon',
            'akun'      => $barang->akun_disk_penjualan_id,
        ];

        endforeach;

        $array_debit[] = [
            'nominal'   => $data['nilai_diskon'] ?? 0,
            'tanggal'   => $carbonNow,
            'produk'    => $barang['produk_detail']['produk_id'],
            'dari'      => 'Diskon Penjualan',
            'akun'      => $this->preferensiPos->first()->diskon_penjualan_id,
        ];

        $array_debit[] = [
            'nominal'   => $harga_total_pajak, 
            'tanggal'   => $carbonNow, 
            'produk'    => $barang['produk_detail']['produk_id'],
            'dari'      => 'Total Penjualan', 
            'akun'      => $this->preferensiPos->first()->akun_hutang_id
        ];

        $dataToRepository['transaksi_array_debit'] = $array_debit;
        $dataToRepository['transaksi_array_kredit'] = $array_kredit;
        

        // Penerimaan Penjualan

        foreach ($items_array as $item):
            $total += $item['qty'] * floatval($item['harga']);
        endforeach;

        $identitas = $this->identitas->first();

        if ($identitas !== null) {
            $rate = $identitas->mataUang->nilai_tukar ?? 1;
        } else {
            $rate = 1;
        }

        $dataToRepository['penerimaan_penjualan'] = [
            'pelanggan_id'          => ($request->has('pelanggan_id') ? $request->pelanggan_id : 6),
            'akun_bank_id'          => $this->preferensiPos->first()->akun_cash_id,
            'rate'                  => $rate,
            'cheque_no'             => null,
            'cheque_date'           => null,
            'memo'                  => 'Pembayaran POS',
            'form_no'               => $this->makeNoPos($this->penerimaanPenjualan->whereDate('created_at', Carbon::now()->format('Y-m-d'))->orderBy('id','desc')->first()->id ?? 0),
            'payment_date'          => $carbonNow,
            'kosong'                => 0,
            'fiscal_payment'        => null,
            'cheque_amount'         => 0,
            'existing_credit'       => null,
            'distribute_amount'     => 0,
            'created_by'            => auth()->user()->id
        ];

        // Invoice Penerimaan Penjualan 
        $dataToRepository['invoice_penerimaan_penjualan'] = [
            // 'faktur_id'                 => $this->fakturPenjualan,
            'payment_amount'            => $harga_total_pajak,
            'diskon'                    => 0,
            'last_owing'                => 0,
            'diskon_date'               => null,
            // 'penerimaan_penjualan_id'   => $this->penerimaanPenjualan->id,
            'tanggal'                   => $carbonNow
        ];

        // Transaksi Penerimaan Penjualan 
        $InvoicePenerimaanPenjualanKredit = [
            'produk'    => $barang['produk_detail']['produk_id'],
            'nominal'   => $barang['produk_detail']['harga_modal'] * $item['qty'],
            'dari'      => 'Payment Amount Kredit',
            'akun'      => $this->preferensiPos->first()->akun_hutang_id
        ];

        $InvoicePenerimaanPenjualanDebit = [
            'produk'    => $barang['produk_detail']['produk_id'],
            'nominal'   => $barang['produk_detail']['harga_modal'] * $item['qty'],
            'dari'      => 'Payment Amount Debit',
            'akun'      => $this->preferensiPos->first()->akun_cash_id
        ];

        $dataToRepository['penerimaan_penjualan_kredit'] = $InvoicePenerimaanPenjualanKredit;
        $dataToRepository['penerimaan_penjualan_debit'] = $InvoicePenerimaanPenjualanDebit;
        // dd($dataToRepository['penerimaan_penjualan_kredit']);
        
        // Payment History 
        $dataToRepository['payment_history'] = [
            'status'                    => 0,
            'total'                     => $total,
            'penerimaan_penjualan_id'   => $request->penerimaan_penjualan_id,
            'created_by'                => auth()->user()->id,
        ];
        
        // Riwayat Pembayaran 
        $dataToRepository['riwayat_pembayaran'] = [
            'kembalian'                 => $request->kembalian,
            'dibayar'                   => $request->dibayar,
            // 'faktur_penjualan_id'       => $this->fakturPenjualan,
            'created_at'                => $carbonNow,
        ];
        // dd($dataToRepository);
        $this->posRepository->insert($dataToRepository);

        $resp = response()->json([
            'status'    => true,
            'message'   => 'transaksi berhasil',
            'data'      => 'successfully save'
        ], 200);
        return $resp;
    }

    public function devPost(Request $request)
    {
        $resp = response()->json([
            'status'    => true,
            'message'   => 'transaksi sukses dilakukan',
             'data'     => [
                "no_faktur"     => "F-01212",
                "tgl_transaksi" => date('Y-m-d H:i:s'),
                "pelanggan"     => "default",
                "jumlah_items"  => 5,
                "total_rupiah"  => 1000000,
                "diskon"        => 0
             ]
        ], 200);
        return $resp;
    }

    public function devPosListPenjualan(Request $request)
    {
        $resp = response()->json([
            'status'    => true,
            'message'   => 'transaksi sukses dilakukan',
            'data'      => [
                [
                    "transaksi_id"      => 1,
                    "no_faktur"         => "F-01212",
                    "tgl_transaksi"     => date('Y-m-d H:i:s'),
                    "pelanggan"         => "default",
                    "jumlah_items"      => 5,
                    "total_rupiah"      => 1000000,
                    "diskon"            => 0
                ],
                [
                    "transaksi_id"      => 2,
                    "no_faktur"         => "F-01213",
                    "tgl_transaksi"     => date('Y-m-d H:i:s'),
                    "pelanggan"         => "default",
                    "jumlah_items"      => 5,
                    "total_rupiah"      => 1000000,
                    "diskon"            => 0
                ],
                [
                    "transaksi_id"      => 3,
                    "no_faktur"         => "F-01214",
                    "tgl_transaksi"     => date('Y-m-d H:i:s'),
                    "pelanggan"         => "default",
                    "jumlah_items"      => 5,
                    "total_rupiah"      => 1000000,
                    "diskon"            => 0
                ]
            ]
        ], 200);
        return $resp;
    }

    public function listPost(Request $request)
    {
        $data = FakturPenjualan::with(['barang.produk.saldoAwalBarang', 'invoice.penerimaanPenjualan.paymentHistory.transactionNonCash'])->paginate(10);

        if($data->isEmpty()) {
            return response()->json([
                'status'    => false,
                'message'   => 'Not Found !',
                'data'      => null
            ], 401);
        }

        return new PosCollection($data);
    }

    public function makeNoPos($id)
    {
        $id = $id + 1;
        $no_pos = 'POS/'.Carbon::now()->format('Y/m/d').'/'.$id;

        return $no_pos;
    }

    public function generateNoPenjualan()
    {
        $current_time = Carbon::now()->timestamp;
        $auth         = sprintf("%05s",Auth::user()->id); 
        return "POS/".$current_time."/".$auth;
    }

    public function getCartRealTotal()
    {
        $total = 0;

        $cart = Cart::instance('penjualan-diskon-'.auth()->user()->id);

        foreach ($cart->content() as $item) {
            $total += $item->options->price_real * $item->qty;
        }

        return $total;
    }
}
