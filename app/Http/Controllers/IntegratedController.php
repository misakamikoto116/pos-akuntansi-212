<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Modules\Akuntansi\Models\StikerHarga;
use Storage;
use Image;

class IntegratedController extends Controller
{
    /**
     * Force Download Last Active Header for Electron App
     *
     * TODO: JPEG TO PNG
     * NOTE: PNG FILE REQUIRED
     *
     * @method GET
     * @param null
     * @return response()stream
     */
    public function stickerHeader()
    {
        $sticker = StikerHarga::where('aktif', true)->firstOrFail();
        $name = 'sticker_header.png';
        $image = Image::make(public_path('storage/barang/stiker/'.$sticker->nama_stiker))->greyscale()->gamma(1.0)->resize(267, 43)->encode('png');

        // return response()->file(public_path('storage/barang/stiker/'.$sticker->nama_stiker));
        $headers = [
            'Content-Type' => 'image/png',
            'Content-Disposition' => 'attachment; filename='. $sticker->nama_stiker,
        ];
        return response()->stream(function () use ($image) {
            echo $image;
        }, 200, $headers);
    }
}
