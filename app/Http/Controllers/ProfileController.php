<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProfileRequest;
use App\Modules\Akuntansi\Models\User;
use Auth;
use Session;

use Helpers\PenjualanApi;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title_document' => 'Profile',
            'data' => Auth::user()
        ];
        return view('auth.profile')->with($data);
    }

    public function save(ProfileRequest $request)
    {
        $id = Auth::user()->id;
        $credentials = ['email' => $request->email, 'password' => $request->old_password];
        if(!Auth::attempt($credentials)){
            Session::flash('flash_notification', [
                'level'   => 'danger',
                'message' => '<i class="fa fa-close"></i> Gagal mengupdate profile. Password salah',
            ]);
            return back();
        }

        
        if($id){
            User::findOrFail($id)->update([
                'name' => $request->name,
                'email' => $request->email,
            ]);
            if($request->get('password') == $request->get('passwords') && $request->get('password') != '' && $request->get('password') != '' ){
                User::findOrFail($id)->update([
                    'password' => bcrypt($request->password)
                ]); 
            }
        }
        Session::flash('flash_notification', [
            'level'   => 'success',
            'message' => '<i class="fa fa-check-circle"></i> Berhasil mengupdate profile',
		]);
        return back();
    }

    public function testPulsa(Request $request)
    {
        $helper = new PenjualanApi;
        $helper = $helper->topUpPulsaRegularTest($request->get('code'), $request->get('nomor'), '1000');      
        return $helper;
        
    }

    public function testPulsaProses(Request $request)
    {
        
        $data = [
            "result" => "success",
            "message" => "S5 082228988857 Akan diproses"
        ];

        return $data;
            
    }

}
