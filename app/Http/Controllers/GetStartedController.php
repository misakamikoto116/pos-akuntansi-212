<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Akuntansi\Http\Controllers\BarangController;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\BarangFakturPembelian;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\Config;
use App\Modules\Akuntansi\Models\DetailInformasiPelanggan;
use App\Modules\Akuntansi\Models\DetailInformasiPemasok;
use App\Modules\Akuntansi\Models\DetailJurnalUmum;
use App\Modules\Akuntansi\Models\DetailPenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\Jasa;
use App\Modules\Akuntansi\Models\JurnalUmum;
use App\Modules\Akuntansi\Models\MataUang;
use App\Modules\Akuntansi\Models\PenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\PreferensiBarang;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\Transaksi;
use Carbon\Carbon;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class GetStartedController extends Controller
{
	public function __construct(Identitas $identitas, Akun $akun, JurnalUmum $jurnal_umum, DetailJurnalUmum $detail_jurnal_umum, 
								TransaksiHelper $transaksi, InformasiPelanggan $informasi_pelangan, CodeHelper $code, FakturPenjualan $faktur_penjualan,
								PreferensiMataUang $preferensi_mata_uang, Produk $produk, DetailInformasiPelanggan $detail_informasi_pelanggan,
								BarangFakturPenjualan $barang_faktur_penjualan, InformasiPemasok $informasi_pemasok, FakturPembelian $faktur_pembelian,
								DetailInformasiPemasok $detail_informasi_pemasok, BarangFakturPembelian $barang_faktur_pembelian, PenyesuaianPersediaan $penyesuaian_persediaan,
								Gudang $gudang, PreferensiBarang $preferensi_barang, DetailPenyesuaianPersediaan $detail_penyesuaian_persediaan, SaldoAwalBarang $saldo_awal_barang,
								Config $config, BarangController $barang_controller)
	{
		$this->identitas 						= $identitas;
		$this->akun 							= $akun;
		$this->jurnal_umum 						= $jurnal_umum;
		$this->detail_jurnal_umum 				= $detail_jurnal_umum;
		$this->transaksi 						= $transaksi;
		$this->informasi_pelangan 				= $informasi_pelangan;
		$this->code 							= $code;
		$this->faktur_penjualan 				= $faktur_penjualan;
		$this->preferensi_mata_uang				= $preferensi_mata_uang;
		$this->produk 							= $produk;
		$this->detail_informasi_pelanggan		= $detail_informasi_pelanggan;
		$this->barang_faktur_penjualan			= $barang_faktur_penjualan;
		$this->informasi_pemasok 				= $informasi_pemasok;
		$this->faktur_pembelian 				= $faktur_pembelian;
		$this->detail_informasi_pemasok 		= $detail_informasi_pemasok;
		$this->barang_faktur_pembelian			= $barang_faktur_pembelian;
		$this->penyesuaian_persediaan 			= $penyesuaian_persediaan;
		$this->gudang 							= $gudang;
		$this->preferensi_barang 				= $preferensi_barang;
		$this->detail_penyesuaian_persediaan 	= $detail_penyesuaian_persediaan; 
		$this->saldo_awal_barang				= $saldo_awal_barang;
		$this->config 							= $config;
		$this->barang_controller				= $barang_controller;
	}

	public function index()
	{
		$mata_uang = MataUang::pluck('nama','id');

		$akun_bank = Akun::with('tipeAkun')->whereHas('tipeAkun', function ($query)
		{
			$query->where('title','Kas/Bank');
		})->whereHas('childAkun')->get()->reduce(function ($output, $item) {
            $child = [];
            foreach ($item->childAkun as $key => $value) {
            	$child[$value->id] = $value->nama_akun;
            }
            $output[$item->nama_akun] =  $child;
            return $output;
        }, []);

		$identitas = Identitas::first();

		$gudang = $this->gudang->pluck('nama','id');

		$akun              = $this->listAkun();
		$preferensi_barang = $this->listPreferensiBarang();
		$data              = [
		                            'fileFormatUrl' => asset('assets/export-produkmaster-without-akun.csv')
		                     ];

		return view('chichi_theme.auth.starting', compact('mata_uang','akun_bank','identitas','gudang','akun','preferensi_barang','data'));
	}

	public function indexImportGetStarted()
	{
		$view = [
			'title_document'    => 'Upload Produk By Akun',
		    'akun'              => $this->listAkun(),
		    'preferensi_barang' => $this->listPreferensiBarang(),
		    'data'              => [
		                            'fileFormatUrl' => asset('assets/export-produkmaster-without-akun.csv')
		                           ],
		];

		return view('chichi_theme.auth.import_get_started')->with($view);
	}

	public function save(Request $request)
	{

		// dd($request->all());

		try {

			DB::beginTransaction();

					/*Fungsi save  Data Identitas*/
					$identitas = $this->saveIdentitas($request);

					/*Fungsi save  Data Akun*/
					if (!empty($request->akun_id)) {

						$save_transaksi_akun = $this->saveTransaksiAkun($request);
					
					}

					/*Fungsi save  Data Pelanggan*/
					$data_pelanggan = $request->only(['nama_pelanggan_umum','saldo_awal_pelanggan']);

					if (!empty($data_pelanggan['nama_pelanggan_umum'])) {
						
						$save_data_pelanggan = $this->saveDataPelanggan($data_pelanggan, $request);	

					}

			  		/*Fungsi save Data Pemasok*/
					$data_pemasok = $request->only(['nama_pemasok_umum','saldo_awal_pemasok']);
					
					$data_all 	  = $request->all();

					if (!empty($data_pemasok['nama_pemasok_umum'])) {
						
						$save_data_pemasok = $this->saveDataPemasok($data_pemasok, $data_all);

					}

					/*Fungsi save Barang*/
					$data_barang = $request->only(['no_barang','nama_barang','tanggal_stok_barang','kuantitas','satuan','harga_beli','harga_jual_barang']);

					if (!empty($data_barang['no_barang'])) {
						
						$save_data_barang = $this->saveDataBarang($data_barang, $data_all);

					}
					/*Fungsi Import Barang*/
					if ($request->hasFile('import_file')) {
						$save_import_barang = $this->barang_controller->postUploadProdukByAkun($request, 0);
					}

					/*Fungsi save Config*/
					$save_config = $this->saveConfig();

			DB::commit();

			return redirect('/login');

		} catch (Exception $e) {
			
			DB::rollBack();

			return redirect()->back()->withInput()->withErrors(['message' => $e]);

		}

	}

	public function validPerusahaan(Request $request)
	{
		$nama_perusahaan = $request->form_perusahaan[0]['value'];
	 	$alamat = $request->form_perusahaan[1]['value'];
	 	$tanggal_mulai = $request->form_perusahaan[2]['value'];
	 	$mata_uang_id = $request->form_perusahaan[3]['value'];

		$form = [
			'nama_perusahaan' => $nama_perusahaan,
			'alamat'					=> $alamat,
			'tanggal_mulai'		=> $tanggal_mulai,
			'mata_uang_id'		=> $mata_uang_id
		];

			$validator = Validator::make($form, [
				'nama_perusahaan'         => 'required',
				'alamat' 									=> 'required',
				'tanggal_mulai' 					=> 'required',
				'mata_uang_id' 						=> 'required',
			]);
			if ($validator->fails()) {
					return response()->json([
						'success' => false
					]);
			}
			return response()->json(['success' => true]);

	}

	// SAVE IDENTITAS PERUSAHAAN
	public function saveIdentitas($request)
	{
		$data_identitas = [
			'nama_perusahaan' 	=> $request->nama_perusahaan,
			'tanggal_mulai' 	=> $this->formatTanggal($request->tanggal_mulai),
			'mata_uang_id' 		=> $request->mata_uang_id,
			'alamat' 			=> $request->alamat,
		];


		return $this->identitas->create($data_identitas);
	}
	/////////////////////////////////////

	// PROSES SIMPAN TRANSAKSI AKUN
	public function saveTransaksiAkun($request)
	{
		$tanggal_sekarang 	= date("Y-m-d H:i:s");	

		$data_akun 			= $request->only(['akun_id','tanggal_akun','nominal_akun']);

		$modelJurnalUmum 	= $this->insertJurnalUmum($data_akun);

		$detailJurnalUmum 	= $this->insertDetailJurnalUmum($modelJurnalUmum, $data_akun, $tanggal_sekarang);

		$transaksiAkun 		= $this->insertTransaction($modelJurnalUmum, $data_akun);

		return $transaksiAkun;
	}

	public function insertJurnalUmum($data)
	{
		$jurnal_umum = [];

		foreach ($data['akun_id'] as $key => $data_akun) {
		
			$jurnal_umum[] = $this->jurnal_umum->create([
				    'no_faktur'   => $this->generateNoFaktur($data['tanggal_akun'][$key]),
				    'tanggal'     => $data['tanggal_akun'][$key],
				    'description' => 'Saldo Awal',
				    'status'      => 0,
			]);
		
		}

	    return $jurnal_umum;
	}

	public function insertDetailJurnalUmum($modelJurnalUmum, $data, $tanggal_sekarang)
	{
		$detail = [];

		foreach ($modelJurnalUmum as $key => $data_jurnal_umum) {
			$detail[] = [
                'akun_id'        => $data['akun_id'][$key],
                'jurnal_umum_id' => $data_jurnal_umum->id,
                'debit'          => $this->formatNominal($data['nominal_akun'][$key]),
                'kredit'         => 0,
                'created_at'     => $tanggal_sekarang,
                'updated_at'     => $tanggal_sekarang,
	        ];

	        $detail[] = [
	        	'akun_id'        => $this->akunOpening()->id,
	        	'jurnal_umum_id' => $data_jurnal_umum->id,
	        	'debit'          => 0,
	        	'kredit'         => $this->formatNominal($data['nominal_akun'][$key]),
	        	'created_at'     => $tanggal_sekarang,
	        	'updated_at'     => $tanggal_sekarang,
	        ];
		}	

		return $this->detail_jurnal_umum->insert($detail);
	}

	public function insertTransaction($modelJurnalUmum, $data)
	{
		foreach ($modelJurnalUmum as $key => $data_jurnal_umum) {
			
			$transaksi = [
			    'nominal' => $this->formatNominal($data['nominal_akun'][$key]),
			    'tanggal' => $this->formatTanggal($data['tanggal_akun'][$key]),
			];

			$debit  		= $transaksi;
			$kredit 		= $transaksi;
			$debit['akun']  = $data['akun_id'][$key];
			$kredit['akun'] = $this->akunOpening()->id;
			$debit['dari']  = 'Saldo Awal Akun per item '.$data_jurnal_umum->id;
			$kredit['dari'] = 'Saldo Awal Akun per item balance '.$data_jurnal_umum->id;

			$this->transaksi->ExecuteTransactionSingle('insert', $data_jurnal_umum, $debit, 1, $this->generateNoFaktur($data['tanggal_akun'][$key]));
			$this->transaksi->ExecuteTransactionSingle('insert', $data_jurnal_umum, $kredit, 0,$this->generateNoFaktur($data['tanggal_akun'][$key]));
		
		}

	    return true;
	}
	//////////////////////////////////////////

	// PROSES SIMPAN DATA PELANGGAN, FAKTUR, BARANG FAKTUR, DETAIL PELANGGAN, TRANSAKSI PELANGGAN
	public function saveDataPelanggan($data, $request)
	{	
		foreach ($data['nama_pelanggan_umum'] as $key => $pelanggan) {

			$pelanggans = $this->informasi_pelangan->create([
				'nama' 		=> $pelanggan,
			]);

			if (!empty($request->no_faktur_pelanggan[$key])) {

				$save_detail_pelanggan = $this->saveDetailPelanggan($pelanggans, $request, $key);

			}
			
		}

		return true;
	}

	public function saveDetailPelanggan($pelanggans, $request, $key)
	{
		$tanggal_sekarang 			= date("Y-m-d H:i:s");

		$data 						= $request->all();

		$arrDetailPelanggan			= [];

		$arrBarangFakturPelanggan 	= [];

		foreach ($request->no_faktur_pelanggan[$key] as $y =>  $faktur_pelanggan) {

			$data['tanggal_pelanggan'][$key][$y] 	= $this->formatTanggal($data['tanggal_pelanggan'][$key][$y]);
			$data['saldo_pelanggan'][$key][$y] 		= $this->formatNominal($data['saldo_pelanggan'][$key][$y]);

			$save_faktur_pelanggan = $this->saveFakturPelanggan($pelanggans, $data, $key, $y, $faktur_pelanggan);

			$arrDetailPelanggan[] = [
				'informasi_pelanggan_id'			=> $pelanggans->id,
			    'no_faktur' 						=> $faktur_pelanggan,
			    'tanggal' 							=> $data['tanggal_pelanggan'][$key][$y],
			    'saldo_awal' 						=> $data['saldo_pelanggan'][$key][$y],
			    'syarat_pembayaran_id' 				=> null,
			    'created_at' 						=> $tanggal_sekarang,
			    'updated_at' 						=> $tanggal_sekarang,
			];

			$arrBarangFakturPelanggan[] = $this->saveBarangFakturPelanggan($save_faktur_pelanggan, $data, $key, $y, $tanggal_sekarang);

			$save_transaksi_pelanggan	= $this->saveTransaksiPelanggan($save_faktur_pelanggan, $data, $key, $y, $faktur_pelanggan);
		
		}

		$this->detail_informasi_pelanggan->insert($arrDetailPelanggan);

		$this->barang_faktur_penjualan->insert($arrBarangFakturPelanggan);

		return true;
	}

	public function saveFakturPelanggan($pelanggans, $data, $key, $y, $faktur_pelanggan)
	{
		$arrFakturPelanggan = [
		    'no_faktur' 				=> $this->code->autoGenerate(new FakturPenjualan(), 'no_faktur', $faktur_pelanggan),
		    'pelanggan_id' 				=> $pelanggans->id,
		    'form_no' 					=> '0',
		    'invoice_date' 				=> $data['tanggal_pelanggan'][$key][$y],
		    'no_fp_std' 				=> '.',
		    'no_fp_std2' 				=> '.',
		    'no_fp_std_date' 			=> '.',
		    'keterangan' 				=> 'Saldo Awal',
		    'uang_muka' 				=> '0',
		    'total' 					=> $data['saldo_pelanggan'][$key][$y],
		];

		$fakturPenjualan = $this->faktur_penjualan->create($arrFakturPelanggan);

		return $fakturPenjualan;
	}

	public function saveBarangFakturPelanggan($save_faktur_pelanggan, $data, $key, $y, $tanggal_sekarang)
	{
		$saldo_awal = $this->barangSaldoAwal();

		$arrBarangFakturPelanggan = [
			'produk_id' 			=> $saldo_awal->id,
			'faktur_penjualan_id' 	=> $save_faktur_pelanggan->id,
			'item_deskripsi' 		=> $saldo_awal->keterangan,
			'item_unit' 			=> $saldo_awal->unit,
			'jumlah' 				=> $saldo_awal->kuantitas,
			'unit_price' 			=> $data['saldo_pelanggan'][$key][$y],
			'diskon' 				=> $saldo_awal->diskon,
			'harga_modal' 			=> $data['saldo_pelanggan'][$key][$y],
			'harga_final' 			=> $data['saldo_pelanggan'][$key][$y],
			'created_at' 			=> $tanggal_sekarang,
			'updated_at' 			=> $tanggal_sekarang,
		];

		return $arrBarangFakturPelanggan;
	}

	public function saveTransaksiPelanggan($save_faktur_pelanggan, $data, $key, $y, $faktur_pelanggan)
	{
		$transaksiDebit = [
		    'nominal' 		=> $data['saldo_pelanggan'][$key][$y],
		    'tanggal' 		=> $data['tanggal_pelanggan'][$key][$y],
		    'dari' 			=> 'Saldo Awal per Item '.$key.''.$y,
		    'id' 			=> $faktur_pelanggan,
		    'akun' 			=> $this->preferensiMataUangPiutang(),
		    'item_id' 		=> $save_faktur_pelanggan->id,
		];
		$transaksiKredit = [
		    'nominal' 		=> $data['saldo_pelanggan'][$key][$y],
		    'tanggal' 		=> $data['tanggal_pelanggan'][$key][$y],
		    'dari' 			=> 'Saldo Awal per Item balance '.$key.''.$y,
		    'id' 			=> $faktur_pelanggan,
		    'akun' 			=> $this->akunOpening()->id,
		    'item_id' 		=> $save_faktur_pelanggan->id,
		];

		$this->transaksi->ExecuteTransactionSingle('insert', $save_faktur_pelanggan, $transaksiKredit); //kredit
		$this->transaksi->ExecuteTransactionSingle('insert', $save_faktur_pelanggan, $transaksiDebit, 1); //debit

		return true;
	}
	//////////////////////////////////////////////////////////////////////

	// PROSES SIMPAN DATA PEMASOK, DETAIL PEMASOK, FAKTUR, BARANG FAKTUR, TRANSAKSI PEMASOK
	public function saveDataPemasok($data_pemasok, $data)
	{
		foreach ($data_pemasok['nama_pemasok_umum'] as $key => $pemasok) {

			$pemasoks = $this->informasi_pemasok->create([
				'nama' 		=> $pemasok,
			]);

			if (!empty($data['no_faktur_pemasok'][$key])) {

				$save_detail_pemasok = $this->saveDetailPemasok($pemasoks, $data, $key);

			}
			
		}

		return true;
	}

	public function saveDetailPemasok($pemasoks, $data, $key)
	{
		$tanggal_sekarang 			= date("Y-m-d H:i:s");

		$arrDetailPemasok			= [];

		$arrBarangFakturPemasok 	= [];

		foreach ($data['no_faktur_pemasok'][$key] as $y =>  $faktur_pemasok) {

			$data['tanggal_pemasok'][$key][$y] 		= $this->formatTanggal($data['tanggal_pemasok'][$key][$y]);
			$data['saldo_pemasok'][$key][$y] 		= $this->formatNominal($data['saldo_pemasok'][$key][$y]);

			$save_faktur_pemasok = $this->saveFakturPemasok($pemasoks, $data, $key, $y, $faktur_pemasok);

			$arrDetailPemasok[] = [
				'no_faktur' 				=> $faktur_pemasok,
				'tanggal' 					=> $data['tanggal_pemasok'][$key][$y],
				'saldo_awal' 				=> $data['saldo_pemasok'][$key][$y],
				'syarat_pembayaran_id' 		=> null,
				'informasi_pemasok_id' 		=> $pemasoks->id,
				'created_at' 				=> $tanggal_sekarang,
				'updated_at' 				=> $tanggal_sekarang,
			];

			$arrBarangFakturPemasok[] 	= $this->saveBarangFakturPemasok($save_faktur_pemasok, $data, $key, $y, $tanggal_sekarang);

			$save_transaksi_pemasok		= $this->saveTransaksiPemasok($save_faktur_pemasok, $data, $key, $y, $faktur_pemasok);
		
		}

		$this->detail_informasi_pemasok->insert($arrDetailPemasok);

		$this->barang_faktur_pembelian->insert($arrBarangFakturPemasok);

		return true;
	}

	public function saveFakturPemasok($pemasoks, $data, $key, $y, $faktur_pemasok)
	{
		$arrFakturPemasok = [
		    'no_faktur' 				=> $this->code->autoGenerate(new FakturPembelian(), 'no_faktur', $faktur_pemasok),
		    'pemasok_id' 				=> $pemasoks->id,
		    'form_no' 					=> '0',
		    'invoice_date' 				=> $data['tanggal_pemasok'][$key][$y],
		    'no_fp_std' 				=> '.',
		    'no_fp_std2' 				=> '.',
		    'no_fp_std_date' 			=> '.',
		    'keterangan' 				=> 'Saldo Awal',
		    'catatan' 					=> 'Saldo Awal',
		    'uang_muka' 				=> '0',
		    'total' 					=> $data['saldo_pemasok'][$key][$y],
		];

		$fakturPembelian = $this->faktur_pembelian->create($arrFakturPemasok);

		return $fakturPembelian;
	}

	public function saveBarangFakturPemasok($save_faktur_pemasok, $data, $key, $y, $tanggal_sekarang)
	{
		$saldo_awal = $this->barangSaldoAwal();

		$arrBarangFakturPemasok = [
			'produk_id' 			=> $saldo_awal->id,
			'faktur_pembelian_id' 	=> $save_faktur_pemasok->id,
			'item_deskripsi' 		=> $saldo_awal->keterangan,
			'item_unit' 			=> $saldo_awal->unit,
			'jumlah' 				=> $saldo_awal->kuantitas,
			'unit_price' 			=> $data['saldo_pemasok'][$key][$y],
			'diskon' 				=> $saldo_awal->diskon,
			'harga_modal' 			=> $data['saldo_pemasok'][$key][$y],
			'harga_final' 			=> $data['saldo_pemasok'][$key][$y],
			'created_at' 			=> $tanggal_sekarang,
			'updated_at' 			=> $tanggal_sekarang,
		];

		return $arrBarangFakturPemasok;
	}

	public function saveTransaksiPemasok($save_faktur_pemasok, $data, $key, $y, $faktur_pemasok)
	{
		$transaksiDebit = [
		    'nominal' 		=> $data['saldo_pemasok'][$key][$y],
		    'tanggal' 		=> $data['tanggal_pemasok'][$key][$y],
		    'dari' 			=> 'Saldo Awal per Item balance '.$key.''.$y,
		    'id' 			=> $faktur_pemasok,
		    'akun' 			=> $this->akunOpening()->id,
		    'item_id' 		=> $save_faktur_pemasok->id,
		];

		$transaksiKredit = [
		    'nominal' 		=> $data['saldo_pemasok'][$key][$y],
		    'tanggal' 		=> $data['tanggal_pemasok'][$key][$y],
		    'dari' 			=> 'Saldo Awal per Item '.$key.''.$y,
		    'id' 			=> $faktur_pemasok,
		    'akun' 			=> $this->preferensiMataUangHutang(),
		    'item_id' 		=> $save_faktur_pemasok->id,
		];

		$this->transaksi->ExecuteTransactionSingle('insert', $save_faktur_pemasok, $transaksiKredit); //kredit
		$this->transaksi->ExecuteTransactionSingle('insert', $save_faktur_pemasok, $transaksiDebit, 1); //debit

		return true;
	}
	//////////////////////////////////////////////////////////////////////////////////////

	// PROSES Simpan Data Barang
	public function saveDataBarang($data_barang, $data_all)
	{
		$akun_beban					= $this->akunBeban();
        $akun_persediaan			= $this->akunPersediaan();
        $akun_penjualan				= $this->akunPenjualan();
        $akun_retur_penjualan		= $this->akunReturPenjualan();
        $akun_potongan_penjualan	= $this->akunPotonganPenjualan();
        $akun_barang_terkirim		= $this->akunBarangTerkirim();
        $akun_hpp 					= $this->akunHPP();
        $akun_belum_tertagih 		= $this->akunBelumTertagih();

		$tanggal_sekarang 			= date("Y-m-d H:i:s");

		$arrayProdukDetail			= [];

		foreach ($data_barang['no_barang'] as $key => $barang) {

			$arrBarang = [
				'no_barang'					=> $barang,
				'keterangan'				=> $data_barang['nama_barang'][$key],
				'kuantitas'					=> $data_barang['kuantitas'][$key],
				'unit'						=> $data_barang['satuan'][$key],
				'akun_beban_id'				=> $akun_beban,
				'akun_persedian_id'			=> $akun_persediaan,
				'akun_penjualan_id'			=> $akun_penjualan,
				'akun_ret_penjualan_id'		=> $akun_retur_penjualan,
				'akun_disk_penjualan_id'	=> $akun_potongan_penjualan,
				'akun_barang_terkirim_id'	=> $akun_barang_terkirim,
				'akun_hpp_id'				=> $akun_hpp,
				'akun_ret_pembelian_id'		=> $akun_persediaan, // sesuai dengan seeder
				'akun_belum_tertagih_id'	=> $akun_belum_tertagih,
				'harga_jual'				=> $this->formatNominal($data_barang['harga_jual_barang'][$key]),
				'gudang_def_id'				=> $data_all['gudang_id'][$key],
			];

			$tempData = $this->produk->create($arrBarang);

			if ($data_barang['kuantitas'][$key] > 0) {

				$arrPenyesuaianPersediaan = [
					'no_penyesuaian'    => $this->makeNoPenyesuaian(Carbon::parse($data_barang['tanggal_stok_barang'][$key])->format('d-m-Y')),
	                'tgl_penyesuaian'   => $this->formatTanggal($data_barang['tanggal_stok_barang'][$key]),
	                'akun_penyesuaian'  => $this->preferensiPersediaan()->akun_persediaan_id,
	                'status'            => 0,
				];

				$save_penyesuaian 				= $this->penyesuaian_persediaan->create($arrPenyesuaianPersediaan);

				$save_detail_persediaan 		= $this->saveDetailPersediaan($save_penyesuaian, $tempData, $data_all, $key);
			
				$save_transaksi_persediaan		= $this->saveTransaksiPersediaan($save_penyesuaian, $data_all, $key);

				$arrayProdukDetail[] = [
				    'no_faktur'         => $this->makeNoFakturProdukDetail(Carbon::parse($data_all['tanggal_stok_barang'][$key])->format('d-m-Y'), $tempData->no_barang),
				    'tanggal'           => $this->formatTanggal($data_all['tanggal_stok_barang'][$key]),
				    'harga_modal'       => $this->formatNominal($data_all['harga_beli'][$key]),
				    'harga_terakhir'    => $this->formatNominal($data_all['harga_beli'][$key]),
				    'kuantitas'         => $data_all['kuantitas'][$key],
				    'transaksi_id'      => $save_transaksi_persediaan->id,
				    'status'            => 0,
				    'gudang_id'         => $data_all['gudang_id'][$key],
				    'produk_id'         => $tempData->id,
				    'created_at'        => $tanggal_sekarang,
				    'updated_at'        => $tanggal_sekarang,
				    'item_type'         => get_class($save_penyesuaian),
				    'item_id'           => $save_penyesuaian->id,
				];


			} 

		}

		$this->saldo_awal_barang->insert($arrayProdukDetail);

		return true;
	}

	public function saveDetailPersediaan($penyesuaian, $barang, $data, $key)
	{
		$detail_penyesuaian = $this->detail_penyesuaian_persediaan->create([
		    'penyesuaian_persediaan_id'     => $penyesuaian->id,
		    'produk_id'                     => $barang->id,
		    'keterangan_produk'             => $barang->keterangan,
		    'satuan_produk'                 => $barang->unit,
		    'qty_produk'                    => $data['kuantitas'][$key],
		    'qty_baru'                      => $data['kuantitas'][$key],
		    'nilai_skrg'                    => $data['kuantitas'][$key] * $this->formatNominal($data['harga_beli'][$key]),
		    'nilai_baru'                    => $this->formatNominal($data['harga_beli'][$key]),
		    'gudang_id'                     => $data['gudang_id'][$key],
		]);

		return $detail_penyesuaian;
	}

	public function saveTransaksiPersediaan($penyesuaian, $data, $key)
	{
		$arrDebitPersediaan = [
		    'nominal'       => $penyesuaian->detailPenyesuaian->sum('nilai_skrg'),
		    'tanggal'       => $penyesuaian->tgl_penyesuaian,
		    'dari'          => 'Akun Persediaan',$data['no_barang'][$key], 
		    'akun'          => $penyesuaian->akun_penyesuaian,
		];


		$arrKredit = [
		    'nominal'       => $penyesuaian->detailPenyesuaian->sum('nilai_skrg'),
		    'tanggal'       => $penyesuaian->tgl_penyesuaian,
		    'dari'          => 'Akun Penyesuaian',
		    'akun'          => $this->akunOpening()->id,
		];

		$first_transaksi_persediaan     = $this->transaksi->ExecuteTransactionSingle('insert', $penyesuaian, $arrDebitPersediaan, 1, $this->generateNoPersediaan($data['no_barang'][$key], $penyesuaian->tgl_penyesuaian));
		$second_transaksi_penyesuaian   = $this->transaksi->ExecuteTransactionSingle('insert', $penyesuaian, $arrKredit, 0,  $this->generateNoPersediaan($data['no_barang'][$key], $penyesuaian->tgl_penyesuaian));

		return $first_transaksi_persediaan;
	}
	////////////////////////////////////////////////////////////////////////////////////// 

	// PROSES SAVE CONFIG
	public function saveConfig()
	{
		$config = $this->config->create([
			'key' 		=> 'installed',
			'value' 	=> true,
			'deskripsi' => 'Get Started Telah Terselesaikan'
		]);

		return $config;
	}
	//////////////////////////////////////////////////////////////////////////////////////

	// LAIN-LAIN 
	public function generateNoFaktur($tanggal)
    {
        $last_jurnal = JurnalUmum::orderBy('id','DESC')->first();

        $tanggal_akun = Carbon::parse($tanggal)->format('d-m-Y');

        if ($last_jurnal === null) {
            $last_jurnal = $last_jurnal + 1;
        }else {
            $last_jurnal = $last_jurnal->id + 1;
        }
        $format_no_faktur = sprintf('saldo-awal/%s/%s', $tanggal_akun, $last_jurnal);

        return $format_no_faktur;
    }

    public function generateNoPersediaan($no_faktur, $tanggal)
    {
    	$tanggal_akun 		= Carbon::parse($tanggal)->format('d-m-Y');
    	$format_no_faktur 	= sprintf('saldo-awal/%s/%s', $tanggal_akun, $no_faktur);

    	return $format_no_faktur;
    }

    public function akunOpening()
    {
        return $this->akun->where('nama_akun','Opening Balance Equity')->first();
    }

    public function formatNominal($nominal)
    {
    	return str_replace(["Rp ",".00",","],"",$nominal);
    }

    public function formatTanggal($tanggal)
    {
    	return  Carbon::parse($tanggal)->format('Y-m-d H:i:s');
    }

    public function barangSaldoAwal()
    {
    	return $this->produk->where('tipe_barang', 5)->first();
    }

    public function preferensiMataUangPiutang()
    {
    	return $this->preferensi_mata_uang->pluck('akun_piutang_id')->first();
    }

    public function preferensiMataUangHutang()
    {
    	return $this->preferensi_mata_uang->pluck('akun_hutang_id')->first();
    }

    public function makeNoPenyesuaian($tanggal)
    {
        $penyesuaian = $this->penyesuaian_persediaan->get();

        if ($penyesuaian->isEmpty()) {
            $no_penyesuaian = sprintf('Saldo-Awal-Penyesuaian/%s/%s', $tanggal, '1');
        }else {
            $last_penyesuaian = PenyesuaianPersediaan::orderBy('id','DESC')->first();
            $no_penyesuaian = sprintf('Saldo-Awal-Penyesuaian/%s/%s', $tanggal, $last_penyesuaian->id + 1);
        }

        return $no_penyesuaian;
    }

    public function preferensiPersediaan()
    {
    	return $this->preferensi_barang->first();
    }

    public function makeNoFakturProdukDetail($tanggal, $no_barang)
    {
        $no_faktur = sprintf('Saldo-Awal-Produk/%s/%s', $tanggal, $no_barang);

        return $no_faktur;
    }

    public function listAkun()
    {
        return $this->akun->whereDoesntHave('childAkun')->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun.' - '.$item->kode_akun;

            return $output;
        }, []);
    }

    public function listPreferensiBarang()
    {
        return $this->preferensi_barang->first();
	}
	
	/*
	 *		Akun Produk
	 *		Harus Di ganti apabila akun nya berubah
	 */
	public function akunBeban(){
		return $this->akun->where('kode_akun', '1105-001')->pluck('id')->first();
	}

	public function akunPersediaan(){
		return $this->akun->where('kode_akun', '1105-001')->pluck('id')->first();
	}

	public function akunPenjualan(){
		return $this->akun->where('kode_akun', '4101-001')->pluck('id')->first();
	}

	public function akunReturPenjualan(){
		return $this->akun->where('kode_akun', '4201-001')->pluck('id')->first();
	}

	public function akunPotonganPenjualan(){
		return $this->akun->where('kode_akun', '4202')->pluck('id')->first();
	}

	public function akunBarangTerkirim(){
		return $this->akun->where('kode_akun', '1105-999')->pluck('id')->first();
	}

	public function akunHPP(){
		return $this->akun->where('kode_akun', '5100-001')->pluck('id')->first();
	}

	public function akunBelumTertagih(){
		return $this->akun->where('kode_akun', '2106')->pluck('id')->first();
	}
	/*
	 *-------------------------------------- 
	 */
    /////////////////////////////////////////////////////////////
}
