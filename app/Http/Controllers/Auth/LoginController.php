<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Modules\Pos\Models\KasirCek;
use App\Modules\Pos\Models\KasirMesin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo()
    {
        if (auth()->check()) {
            switch (Auth::user()->roles()->pluck('name')->first()) {
                case 'akuntansi':
                    $path = url('/akuntansi');
                break;

                case 'admin':
                    $path = url('/admin');
                break;

                case 'pos': 
                    $path = url('pos');
                break;

                case 'gudang': 
                    $path = url('gudang');
                break;
                case 'pengawas': 
                    $path = url('pengawas');
                break;
            }
            return $path;
        }else {
            return redirect(url('login'));
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('chichi_theme.auth.login');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => 'Email atau password anda tidak benar'];
        
        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function logout(Request $request)
    {
        if (auth()->user()->hasRole('pos')) {
            if (session()->has('kasir_mesin')) {
                $this->checkOutProses();
            }
        }

        $this->performLogout($request);

        session([
                'kasir_cek' => false,
                ]);

        return redirect()->route('login');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        if ($this->attemptLogin($request)) {
            $this->updateLoggedUser($request);
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function updateLoggedUser($request)
    {
        $data = $request->all();
        $user = \DB::table('users')->where('email', $data['email'])->update([
            'logged'    => 1,
        ]);

        return true;
    }

    public function checkOutProses()
    {
        $last_modal = KasirCek::whereDate('check_time', \Carbon\Carbon::now()->format('Y-m-d'))
                                     ->where(['check_state' => 'IN', 'kasir_mesin_id' => session()->get('kasir_mesin'), 'created_by' => auth()->user()->id])
                                     ->orderBy('id', 'desc')
                                     ->firstOrFail();

        $kasir_mesin = KasirMesin::findOrFail(session()->get('kasir_mesin'));

        try {

            \DB::beginTransaction();

            $kasir_mesin->update([
                'status'        => 0,
            ]);

            KasirCek::create([
                'check_state'       => 'OUT',
                'check_time'        => \Carbon\Carbon::now(),
                'modal'             => $last_modal->modal,
                'kasir_mesin_id'    => session()->get('kasir_mesin'),
                'created_by'        => auth()->user()->id,
            ]);

            \DB::commit();

        } catch (Exception $e) {
            
            \DB::rollback();

        }

        return true;
    }
}
