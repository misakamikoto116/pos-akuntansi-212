<?php
/**
 * Created by PhpStorm.
 * User: efendihariyadi
 * Date: 05/09/18
 * Time: 21.11
 */

namespace App\Http\Requests\Api;
use Illuminate\Foundation\Http\FormRequest as LaravelFormRequest;
use Illuminate\Http\JsonResponse;
abstract class ApiFormRequest extends LaravelFormRequest
{
    abstract public function rules();
    /**
     * Get the failed validation response for the request.
     *
     * @param array $errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
        $transformed = [];

        foreach ($errors as $field => $message) {
            $transformed[] = [
                'field' => $field,
                'message' => $message[0],
            ];
        }

        return response()->json([
            'errors' => $transformed
        ], JsonResponse::HTTP_BAD_REQUEST);
    }
}