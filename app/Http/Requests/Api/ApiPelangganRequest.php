<?php

namespace App\Http\Requests\Api;

class ApiPelangganRequest extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
        // Return Html?
        // return [
        //     'anggota.*nama' => 'required|min:3',
        //     'anggota.*alamat' => 'required|min:5',
        //     'anggota.*kota' => 'required|min:5',
        //     'anggota.*telepon' => 'required|min:5',
        //     'anggota.no_pelanggan' => 'required|unique:informasi_pelanggan,no_pelanggan'
        // ];
    }
}
