<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'          => 'sometimes|required|string',
            'email'         => 'required|email',
            'password'      => 'required_if:passwords,!=,null|same:passwords',
            'old_password'  => 'required'
        ];

        if ($this->isMethod('post')) {
            $rules['email'] .= '|unique:users';
        }

        return $rules;
    }
}
