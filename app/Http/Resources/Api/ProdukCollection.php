<?php
/**
 * User: efendihariyadi
 * Date: 17/08/18
 * Time: 00.37
 */

namespace App\Http\Resources\Api;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use Illuminate\Http\Resources\Json\ResourceCollection;
class ProdukCollection extends ResourceCollection
{

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection->transform(function (SaldoAwalBarang $produk) {
            $res = [
                'id' => $produk->produk->produk_detail->id,
                'nama' => $produk->produk->keterangan,
                'kategori' => $produk->produk->kategori->nama,
                'no_barang' => $produk->produk->no_barang,
                'diskon' => $produk->produk->diskon,
                'harga' => $produk->produk->harga_jual,
                'foto_produk' => $produk->produk->foto_produk,
                'tersedia' => $produk->produk->UpdatedQty,
                'harga' => $produk->harga_terakhir
            ];
            //dicomment krn masih bisa diPerlukan
            //return (new ProdukResource($produk));
            return $res;
         });
        return [
            'status' => true,
            'message' => 'Ditemukan data',
            'data' => $collection,
        ];
    }
}