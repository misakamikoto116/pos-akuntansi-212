<?php

namespace App\Http\Resources\Api\Pos;

use App\Modules\Akuntansi\Models\FakturPenjualan;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PosCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection->transform(function (FakturPenjualan $faktur) {
            
            return (new PosResource($faktur));
        });

        return [
            'status'    => true,
            'message'   => 'Ditemukan data',
            'data'      => $collection,
        ];
    }
}
