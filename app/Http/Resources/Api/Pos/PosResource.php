<?php

namespace App\Http\Resources\Api\Pos;

use Illuminate\Http\Resources\Json\JsonResource;

class PosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    
    public function toArray($request)
    {
        $product = [
            'id'            => $this->id,
            'keterangan'    => $this->keterangan,
            'harga'         => $this->harga_jual,
            'jumlah'        => $this->kuantitas,
            'diskon'        => $this->diskon,
            'harga_modal'   => $this->produk_detail['harga_modal'],
        ];

        $transaction = $this->invoice->map(function ($transaction){
                return [
                    'no_kartu_edc'      => $transaction->penerimaanPenjualan->paymentHistory->transactionNonCash->no_kartu ?? null,
                    'no_transaksi_edc'  => $transaction->penerimaanPenjualan->paymentHistory->transactionNonCash->no_transaksi_bank ?? null,
                    'total'             => $transaction->penerimaanPenjualan->paymentHistory->transactionNonCash->total ?? null,
                ];     
        });

        $transaction = $transaction->toArray();

        $data = [
            'no_faktur'             => $this->no_faktur,
            'pelanggan_id'          => $this->pelanggan_id, 
            'diskon'                => $this->diskon, 
            'jumlah_diskon_faktur'  => $this->jumlah_diskon_faktur, 
            'items'                 => $product,              
            'no_kartu_edc'          => $transaction[0]['no_kartu_edc'] ?? null, 
            'no_transaksi_edc'      => $transaction[0]['no_transaksi_edc'] ?? null, 
            'total_edc'             => $transaction[0]['total'] ?? null,              
            'tunai'                 => $this->tunai, 
            'nilai_bayar'           => $this->nilai_bayar, 
            'nilai_kembalian'       => $this->nilai_kembalian, 
            'harga_modal'           => $this->harga_modal, 
            'created_by'            => $this->created_by, 
            'kasir_mesin_id'        => $this->kasir_mesin_id
        ];
        
        return $data;
    }
}
