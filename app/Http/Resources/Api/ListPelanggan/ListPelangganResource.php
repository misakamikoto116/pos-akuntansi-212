<?php

namespace App\Http\Resources\Api\ListPelanggan;

use Illuminate\Http\Resources\Json\JsonResource;

class ListPelangganResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'        => $this->id,
            'nama'      => $this->nama
        ];

        return $data;
    }
}
