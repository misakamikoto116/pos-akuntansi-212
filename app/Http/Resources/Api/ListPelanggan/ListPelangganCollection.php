<?php

namespace App\Http\Resources\Api\ListPelanggan;

use App\Modules\Akuntansi\Models\InformasiPelanggan;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ListPelangganCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection->transform(function (InformasiPelanggan $informasi_pelanggan)
        {
            return (new ListPelangganResource($informasi_pelanggan));
        });

        return  [
            'status'        => true,
            'message'       => 'Data ditemukan',
            'data'          => $collection,
        ];
    }
}
