<?php
/**
 * User: efendihariyadi
 * Date: 17/08/18
 * Time: 00.37
 */

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class ProdukResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'nama' => $this->produk->keterangan,
            'unit' => $this->produk->unit,
            'kategori' => $this->produk->kategori->id,
            'harga' => $this->produk->harga_jual,
            'no_barang' => $this->produk->no_barang,
            'diskon' => (double) $this->produk->diskon,
            'tipe_persediaan' => $this->produk->TipePersediaanApi,
            'foto_produk' => ($this->produk->foto_produk == null ? null : asset('storage/produk/'.$this->produk->foto_produk)),
            'tersedia' => $this->produk->UpdatedQty
        ];
        return $data;
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param  \Illuminate\Http\Request
     * @param  \Illuminate\Http\Response
     * @return void
     */
    public function withResponse($request, $response)
    {
        //$response->;
    }

}
