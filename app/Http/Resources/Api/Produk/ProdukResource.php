<?php
/**
 * User: efendihariyadi
 * Date: 17/08/18
 * Time: 00.37
 */

namespace App\Http\Resources\Api\Produk;

use Illuminate\Http\Resources\Json\JsonResource;

class ProdukResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $data = [
            'id' => $this->id,
            'nama' => $this->keterangan,
            'unit' => $this->unit,
            'kategori' => $this->kategori->nama,
            'harga' => $this->harga_jual,
            'no_barang' => $this->no_barang,
            'tipe_persediaan' => $this->TipePersediaanApi,
            //'foto_produk' => ($this->foto_produk == null ? null : asset('storage/produk/'.$this->foto_produk))
        ];
        return $data;

    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param  \Illuminate\Http\Request
     * @param  \Illuminate\Http\Response
     * @return void
     */
    public function withResponse($request, $response)
    {
        //$response->;
    }

}
