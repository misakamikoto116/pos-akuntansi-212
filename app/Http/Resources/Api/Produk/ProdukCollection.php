<?php
/**
 * User: efendihariyadi
 * Date: 17/08/18
 * Time: 00.37
 */

namespace App\Http\Resources\Api\Produk;
use App\Modules\Akuntansi\Models\Produk;
use Illuminate\Http\Resources\Json\ResourceCollection;
class ProdukCollection extends ResourceCollection
{

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection->transform(function (Produk $produk) {
              return (new ProdukResource($produk));
         });
        return [
            'status' => true,
            'message' => 'Ditemukan data',
            'data' => $collection,
        ];
    }
}