<?php
/**
 * User: efendihariyadi
 * Date: 17/08/18
 * Time: 00.37
 */

namespace App\Http\Resources\Api;
use App\Modules\Akuntansi\Models\KategoriProduk;
use Illuminate\Http\Resources\Json\ResourceCollection;
class KategoriProdukCollection extends ResourceCollection
{

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection->transform(function (KategoriProduk $kategoriProduk) {
              return $kategoriProduk->toArray();
         });
        return [
            'status' => true,
            'message' => 'Ditemukan data',
            'data' => $collection,
        ];
    }
}