<?php

namespace App\Http\Resources\Api\Pelanggan;

use App\Modules\Akuntansi\Models\InformasiPelanggan;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PelangganCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection->transform(function (InformasiPelanggan $informasiPelanggan) {
            return (new PelangganResource($informasiPelanggan));
        });
        return [
            'status' => true,
            'message' => 'Pelanggan',
            'data' => $collection,
        ];
    }
}
