<?php

namespace App\Http\Resources\Api\Pelanggan;

use Illuminate\Http\Resources\Json\JsonResource;

class PelangganResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $data = [
            'id' => $this->id,
            'nama' => $this->nama,
            'no_pelanggan' => $this->no_pelanggan,
            'alamat' => $this->alamat,
        ];
        return $data;
    }
}
