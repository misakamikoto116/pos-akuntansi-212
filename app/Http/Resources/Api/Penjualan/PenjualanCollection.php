<?php

namespace App\Http\Resources\Api\Penjualan;

use App\Modules\Akuntansi\Models\Penjualan;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PenjualanCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection->transform(function (Penjualan $penjualan){
            return (new PenjualanResource($penjualan));
        });

        return [
            'status'    => true, 
            'message'   => 'Successfully get data transaction', 
            'data'      => $collection
        ];
    }
}
