<?php

namespace App\Http\Resources\Api\Penjualan;

use Illuminate\Http\Resources\Json\JsonResource;

class PenjualanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'                    => $this->id,
            'no_faktur'             => $this->no_faktur,
            'pelanggan_id'          => $this->pelanggan_id,
            'pelanggan_kirim_ke_id' => $this->pelanggan_kirim_ke_id,
            'alamat_pengiriman'     => $this->alamat_pengiriman,
            'alamat_tagihan'        => $this->alamat_tagihan,
            'taxable'               => $this->taxable,
            'in_tax'                => $this->in_tax,
            'diskon'                => $this->diskon,
            'jumlah_diskon_faktur'  => $this->jumlah_diskon_faktur,
            'po_no'                 => $this->po_no,
            'invoice_date'          => $this->invoice_date,
            'status'                => $this->status,
            'status_lunas'          => $this->status_lunas,
            'fob'                   => $this->fob,
            'term_id'               => $this->term_id,
            'ship_id'               => $this->ship_id,
            'ship_date'             => $this->ship_date,
            'no_fp_std'             => $this->no_fp_std,
            'no_fb_std2'            => $this->no_fb_std2,
            'no_fp_std_date'        => $this->no_fp_std_date,
            'ongkir'                => $this->ongkir,
            'total'                 => $this->total,
            'keterangan'            => $this->keterangan,
            'catatan'               => $this->catatan,
            'uang_muka'             => $this->uang_muka,
            'akun_piutang_id'       => $this->akun_piutang_id,
            'akun_dp_id'            => $this->akun_dp_id,
            'akun_ongkir_id'        => $this->akun_ongkir_id,
            'status_modul'          => $this->status_modul,
            'kasir_mesin_id'        => $this->kasir_mesin_id,
        ];
        return $data;
    }
}
