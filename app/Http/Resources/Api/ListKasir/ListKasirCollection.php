<?php

namespace App\Http\Resources\Api\Listkasir;

use App\Http\Resources\Api\ListKasir\ListKasirResource;
use App\Modules\Pos\Models\KasirMesin;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ListKasirCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        $collection = $this->collection->transform(function (KasirMesin $kasir_mesin) 
        {
            return (new ListKasirResource($kasir_mesin));
        });

        return [
            'status'        => true,
            'message'       => 'data ditemukan',
            'data'          => $collection,
        ];
    }
}
