<?php

namespace App\Http\Resources\Api\ListKasir;

use Illuminate\Http\Resources\Json\JsonResource;

class ListKasirResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'        => $this->id,
            'nama'      => $this->nama,
        ];

        return $data;
    }
}
