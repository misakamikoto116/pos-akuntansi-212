<?php
namespace App\Http\Middleware;

use Closure;
use Sentry\State\Scope;
use Illuminate\Http\Request;

class SentryContext
{
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure                 $next
	 *
	 * @return mixed
	 */
	public function handle(Request $request, Closure $next)
	{
		if (auth()->check() && app()->bound('sentry')) {
			\Sentry\configureScope(function (Scope $scope): void {
				$scope->setUser(auth()->user());
			});
		}

		return $next($request);
	}
}
