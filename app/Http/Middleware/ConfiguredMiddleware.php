<?php

namespace App\Http\Middleware;

use Closure;
use App\Modules\Akuntansi\Models\Config;

class ConfiguredMiddleware
{
    protected $model;

    public function __construct(Config $config)
    {
        $this->model = $config;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $configure)
    {
        $installed = $this->model->where('key', 'installed')
                                 ->where('value', true)
                                 ->first();

        if ($configure === 'true' && $installed === null) {
            return redirect()->route('chichi.starting');
        } else if($configure === 'false' && $installed !== null) {
            return abort(404);
        }

        return $next($request);
    }
}
