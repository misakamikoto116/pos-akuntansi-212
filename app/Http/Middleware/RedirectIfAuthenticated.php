<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $path = 'akuntansi';
        if (Auth::guard($guard)->check()) {
            switch (Auth::user()->roles()->pluck('name')->first()) {
                case 'akuntansi':
                    $path = url('akuntansi');
                break;
                case 'admin':
                    $path = url('admin');
                break;
                case 'pos':
                    $path = url('pos');
                break;
                case 'pengawas':
                    $path = url('pengawas');
                break;
            }

            return redirect($path);
        }

        return $next($request);
    }
}
