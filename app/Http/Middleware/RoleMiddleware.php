<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $role, $permission = null)
    {
        if (Auth::guest()) {
            return redirect('login');
        }

        $role = is_array($role)
            ? $role
            : explode('|', $role);

        if (!$request->user()->hasAnyRole($role)) {
            return redirect(url('/'.auth()->user()->roles()->pluck('name')->first()));
            abort(403);
        }

        if ($permission && !$request->user()->can($permission)) {
            abort(403);
        }

        return $next($request);
    }
}
