<?php

namespace App\Modules\Akuntansi\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Boot parsing data ke blade laporan.blade.php
        View::composer('akuntansi::laporan.index', 'App\Modules\Akuntansi\Http\ViewComposers\LaporanComposer');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
