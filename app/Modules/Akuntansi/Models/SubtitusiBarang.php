<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubtitusiBarang extends Model
{
    protected $table = 'subtitusi_barang';
	use SoftDeletes;

	protected $fillable = [
		'jika_membayar_antara',
		'akan_dapat_diskon',
		'jatuh_tempo',
		'keterangan',
		'status_cod',
	];
}
