<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;
class SerialNumber extends Model
{
    protected $table = 'serial_number';
    public $timestamps = true;
    
    protected $fillable = [
        'no_pengisian',
        'tgl_pengisian',
        'tipe_transaksi',
        'no_transaksi',
        'tgl_transaksi',
        'persiapan',
        'created_by',
        'updated_by',
        'deleted_by',
  ];


  public function detailSerialNumber()
  {
      return $this->hasMany(DetailSerialNumber::class);
  }
}
