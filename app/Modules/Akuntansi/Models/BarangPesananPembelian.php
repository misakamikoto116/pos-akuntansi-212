<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangPesananPembelian extends Model
{
	protected $table = 'barang_pesanan_pembelian';
  public $timestamps = true;
  protected $appends = ['harga_total'];
	use SoftDeletes;
  protected $with = [ 'pesananPembelian' ];
  protected $fillable = [
	    'produk_id',
      'pesanan_pembelian_id',
      'barang_permintaan_pembelian_id',
	    'satuan',
     	'item_deskripsi',
     	'item_unit',
    	'jumlah',
     	'harga',
      'diskon',
     	'kode_pajak_id',
     	'kode_pajak_2_id',
     ];

  public function produk()
  {
    return $this->belongsTo(Produk::class,'produk_id');
  }

  public function pesananPembelian()
  {
    return $this->belongsTo(PesananPembelian::class,'pesanan_pembelian_id');
  }
  public function barangPermintaanPembelian()
  {
    return $this->belongsTo(BarangPermintaanPembelian::class,'barang_permintaan_pembelian_id');
  }
  public function barangPenerimaanPembeliaan()
  {
    return $this->hasMany(BarangPenerimaanPembelian::class);
  }

  public function pajak()
  {
    return $this->belongsTo(KodePajak::class, 'kode_pajak_id');
  }

  public function pajak2()
  {
    return $this->belongsTo(KodePajak::class, 'kode_pajak_2_id');
  }

  public function getHargaTotalAttribute()
  {
    $total  = $this->getAttribute('harga') * $this->getAttribute('jumlah');
    // $tax  = $total * $this->getAttribute('pajak') / 100;
    $disc   = $total * $this->getAttribute('diskon') / 100;

    $grandtotal = $total - $disc;

    return $grandtotal;
  }

  public function getUpdatedQtyPenerimaanPembelianAttribute()
  {
      $qty_penerimaan     = 0;
      $qty_pesanan        = $this->getAttribute('jumlah');
      if ($this->barangPenerimaanPembeliaan) {
        $qty_penerimaan     = $this->barangPenerimaanPembeliaan->sum('jumlah');
      }
      return $qty_pesanan - $qty_penerimaan;
  }

  public function gudang()
	{
		return $this->belongsTo(Gudang::class, 'gudang_id', 'id');
	}
}
