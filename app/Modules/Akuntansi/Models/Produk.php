<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;
use Laravel\Scout\Searchable;

class Produk extends Model
{
    protected $table = 'produk';
    protected $appends = ['produk_detail'];
    protected $dates = ['tanggal_stok'];
    public $timestamps = true;
    use SoftDeletes;
    use Searchable;

    protected $fillable = [
        'tipe_barang', 
        'no_barang',
        'parent_id',
        'status',
        'keterangan',
        'tipe_persedian',
        'harga_standar_def', 
        'kategori_produk_id', 
        'gudang_def_id',
        'kuantitas', 
        'unit', 
        'harga_unit', 
        'harga_jual', 
        'status_umkm',
        'diskon', 
        'kode_pajak_penj_id',
        'pemasok_id', 
        'min_jumlah_reorder', 
        'kode_pajak_perse_id', 
        'kode_pajak_pemb_id',
        'akun_persedian_id', 
        'akun_beban_id', 
        'akun_penjualan_id', 
        'akun_ret_penjualan_id',
        'akun_disk_penjualan_id', 
        'akun_barang_terkirim_id', 
        'akun_hpp_id', 
        'akun_ret_pembelian_id',
        'akun_belum_tertagih_id', 
        'catatan', 
        'gudang_id', 
        'foto_produk',
        'barcode',
        'nama_singkat',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public static function boot()
    {
        parent::boot();

        self::deleting(function ($barang) {
            $layout = new LayoutHelper();

            // if ($barang->saldoAwalBarang->count() > 0) {
            //     $html = 'Produk tidak dapat dihapus, karena masih memiliki Detail Produk. ';

            //     return $layout->batalkanProses($html);
            // }

            if (!empty($barang->barangPengirimanPenjualan)) {
                $html = 'Produk tidak dapat dihapus, karena masih memiliki Pengiriman Penjualan. ';

                return $layout->batalkanProses($html);
            }

            if ($barang->barangPermintaanPembelian->count() > 0) {
                $html = 'Produk tidak dapat dihapus, karena masih memiliki Permintaan Pembelian. ';

                return $layout->batalkanProses($html);
            }

            // if (!empty($barang->tingkatHargaBarang)) {
            //     $html = 'Produk tidak dapat dihapus, karena masih memiliki Tingkat Harga Barang. ';

            //     return $layout->batalkanProses($html);
            // }

            if ($barang->barangPenawaranPenjualan->count() > 0) {
                $html = 'Produk tidak dapat dihapus, karena masih memiliki Tingkat Harga Barang. ';

                return $layout->batalkanProses($html);
            }
        });
    }

    public function kategori()
    {
        return $this->belongsTo(KategoriProduk::class, 'kategori_produk_id', 'id');
    }

    public function barangPesananPenjualan()
    {
        return $this->hasOne(BarangPesananPenjualan::class);
    }

    public function barangPesananPembelian()
    {
        return $this->hasOne(BarangPesananPembelian::class);
    }

    public function barangFakturPenjualan()
    {
        return $this->belongsTo(BarangFakturPenjualan::class);
    }

    public function kodePajak()
    {
        /*
         * @ask : has many apa has one?
        */
        return $this->belongsTo(KodePajak::class, 'kode_pajak_penj_id');
    }

    public function getKeteranganWithOutStringAttribute()
    {
        $nama_barang = $this->getAttribute('keterangan');

        $formatted_nama_barang = str_replace('"', '', $nama_barang);

        return $formatted_nama_barang;
    }

    public function saldoAwalBarang()
    {
        return $this->hasMany(SaldoAwalBarang::class, 'produk_id', 'id');
    }

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_def_id', 'id');
    }

    public function gudang_lain()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id', 'id');
    }

    public function barangPengirimanPenjualan()
    {
        return $this->hasOne(BarangPengirimanPenjualan::class);
    }

    public function barangFakturPembelian()
    {
        return $this->hasMany(BarangFakturPembelian::class);
    }

    public function barangPermintaanPembelian()
    {
        return $this->hasMany(BarangPermintaanPembelian::class);
    }

    public function tingkatHargaBarang()
    {
        return $this->hasOne(TingkatHargaBarang::class);
    }

    public function transaksiMany()
    {
        return  $this->hasMany(Transaksi::class);
    }

    public function barangPenawaranPenjualan()
    {
        return $this->hasMany(BarangPenawaranPenjualan::class);
    }

    public function fakturPembelian()
    {
        return $this->belongsToMany(FakturPembelian::class, 'barang_faktur_pembelian');
    }

    public function detailPenyelesaianPesanan()
    {
        return $this->hasOne(DetailPenyelesaianPesanan::class);
    }

    public function transaksi()
    {
        return $this->belongsTo(Transaksi::class, 'id', 'produk_id');
    }

    public function getTipeFormattedAttribute()
    {
        $helper = new LayoutHelper();

        $html = '';
        if (0 === $this->getAttribute('tipe_barang')) {
            $html = $helper->formLabel('success', 'Persediaan');
        } elseif (1 === $this->getAttribute('tipe_barang')) {
            $html = $helper->formLabel('info', 'Non Persediaan');
        } elseif (2 === $this->getAttribute('tipe_barang')) {
            $html = $helper->formLabel('primary', 'Servis');
        } elseif (3 === $this->getAttribute('tipe_barang')) {
            $html = $helper->formLabel('warning', 'Uang Muka');
        } elseif (4 === $this->getAttribute('tipe_barang')) {
            $html = $helper->formLabel('default', 'Grup');
        }

        return $html;
    }

    public function getTipePersediaanApiAttribute()
    {
        $tipe_field = $this->getAttribute('tipe_persedian');
        switch ($tipe_field){
            case 0:
                $tipe_persediaan = 'Bahan Baku';
                break;
            case 1:
                $tipe_persediaan = 'Bahan Baku Pembantu';
                break;
            case 2:
                $tipe_persediaan = 'Bahan Setengah Jadi';
                break;
            case 3:
                $tipe_persediaan = 'Barang Jadi';
                break;
            default: $tipe_persediaan = 'Barang lain-lain';
        }
        return $tipe_persediaan;
    }
    public function getTipePersediaanFormattedAttribute()
    {
        $helper = new LayoutHelper();

        $html = '';
        if (0 === $this->getAttribute('tipe_persedian')) {
            $html = $helper->formLabel('default', 'Bahan Baku');
        } elseif (1 === $this->getAttribute('tipe_persedian')) {
            $html = $helper->formLabel('default', 'Bahan Baku Pembantu');
        } elseif (2 === $this->getAttribute('tipe_persedian')) {
            $html = $helper->formLabel('default', 'Barang Setengah Jadi');
        } elseif (3 === $this->getAttribute('tipe_persedian')) {
            $html = $helper->formLabel('default', 'Barang Jadi');
        } else {
            $html = $helper->formLabel('default', 'Barang lain-lain');
        }

        return $html;
    }

    public function akunRetPenjualan()
    {
        return  $this->belongsTo(Akun::class, 'akun_ret_penjualan_id');
    }

    public function akunPenjualan()
    {
        return  $this->belongsTo(Akun::class, 'akun_penjualan_id');
    }

    public function akunDiskPenjualan()
    {
        return  $this->belongsTo(Akun::class, 'akun_disk_penjualan_id');
    }

    public function getProdukDetailAttribute()
    {
        $last = self::saldoAwalBarang()->where('produk_id', $this->getAttribute('id'))->latest()->first();
        if (!empty($last)) {
            return $last;
        }

        return null;
    }

    public function getUpdatedQtyAttribute()
    {
        // di comment dulu mungkin nanti berguna
        // $barang_transaksi_faktur_penjualan  = $SaldoAwalBarang->where('status',1)->where('status_transaksi',0)->sum('kuantitas');
        // $barang_transaksi_retur_penjualan   = $SaldoAwalBarang->where('status',1)->where('status_transaksi',1)->sum('kuantitas');
        // $barang_transaksi_faktur_pembelian  = $SaldoAwalBarang->where('status',1)->where('status_transaksi',2)->sum('kuantitas');
        // $barang_transaksi_retur_pembelian   = $SaldoAwalBarang->where('status',1)->where('status_transaksi',3)->sum('kuantitas');
        // if (!empty($barang_transaksi_faktur_penjualan)) {
        //     $sum_semua_barang = $sum_semua_barang - $barang_transaksi_faktur_penjualan;
        // }
        // if (!empty($barang_transaksi_retur_penjualan)) {
        //     $sum_semua_barang = $sum_semua_barang + $barang_transaksi_retur_penjualan;
        // }
        // if (!empty($barang_transaksi_faktur_pembelian)) {
        //     $sum_semua_barang = $sum_semua_barang + $barang_transaksi_faktur_pembelian;
        // }
        // if (!empty($barang_transaksi_retur_pembelian)) {
        //     $sum_semua_barang = $sum_semua_barang - $barang_transaksi_retur_pembelian;
        // }

        $SaldoAwalBarang = $this->saldoAwalBarang;
        $sum_semua_barang = $SaldoAwalBarang->sum('kuantitas');

        return $sum_semua_barang;
    }

    public function scopeReportFilter($query, $request)
    {
        if (null !== $request->get('gudang_id')) {
            return $query->where('gudang_id', $request->get('gudang_id'));
        }

        if ($request->get('one-date')) {
            return $query->whereDate('created_at', $request->get('one-date'));
        }

        if ($request->get('start-date') && $request->get('end-date')) {
            return $query->whereBetween('created_at', [$request->get('start-date'), $request->get('end-date')]);
        } else {
            $tgl_satu = date('Y-m').'-01';

            return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
        }
    }

    public function scopeFilter($query, $data)
    {
        if (!empty($data['search'])) {
            $query = $query->where('no_barang', 'like', '%'.$data['search'].'%')
                ->orWhere('keterangan', 'like', '%'.$data['search'].'%');
        }

        if (!empty($data['tipe_barang'])) {
            $query = $query->where('tipe_barang', 'like', '%'.$data['tipe_barang'].'%');
        }

        if (!empty($data['kategori'])) {
            $query = $query->whereHas(
                'kategori',
                function ($query) use ($data) {
                    $query->where('id', 'like', '%'.$data['kategori'].'%');
                }
            );
        }

        if (!empty($data['warehouse'])) {
            switch ($data['warehouse']) {
                case 'current':
                        $query->where('gudang_id', 6);
                    break;

                case 'other':
                        $query->whereNotIn('gudang_id', [6]);
                    break;
            }
        }

        return $query;
    }

    public function scopeReportLainLainFilter($query, $request)
    {
        if ($request->get('start-date')) {
            $query->whereBetween('created_at', ['0001-01-01', $request->get('start-date')]);
        }

        return $query;
    }

    public function scopeDateFilter($query, $request)
    {
        if ($request->get('start-date') && $request->get('end-date')) {
            return $query->whereBetween('created_at', [$request->get('start-date'), $request->get('end-date')]);
        } else {
            $tgl_satu = date('Y-m').'-01';

            return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
        }
    }

    public function getSumQtyAwalAttribute()
    {
        $id     = $this->getAttribute('id');
        $sum    = SaldoAwalBarang::where(['produk_id' => $id, 'status' => 0])->sum('kuantitas');

        return $sum;
    }

    public function getSumQtyProdukAttribute()
    {
        $id     = $this->getAttribute('id');
        $sum    = SaldoAwalBarang::where(['produk_id' => $id])->sum('kuantitas');

        return $sum;
    }

    protected $searchable = [
            'keterangan',
            'nama_singkat',
            'barcode',
            'no_barang',
        ];

    protected function fullTextWildcards($term)
    {
        // removing symbols used by MySQL
        $reservedSymbols = ['-', '+', '<', '>', '@', '(', ')', '~'];
        $term = str_replace($reservedSymbols, '', $term);
    
        $words = explode(' ', $term);
    
        foreach($words as $key => $word) {
            /*
             * applying + operator (required word) only big words
             * because smaller ones are not indexed by mysql
             */
            if(strlen($word) >= 3) {
                $words[$key] = '+' . $word . '*';
            }
        }
    
        $searchTerm = implode( ' ', $words);
    
        return $searchTerm;
    }

    public function scopeAdvancedSearch($query, $term)
    {
        $columns = implode(',',$this->searchable);

        $query->whereRaw("MATCH ({$columns}) AGAINST (? IN BOOLEAN MODE)" , $this->fullTextWildcards($term));
 
        return $query;
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = [];

        $array['id']            = $this->id;
        $array['keterangan']    = $this->keterangan;
        $array['no_barang']     = $this->no_barang;
        // $array['nama_singkat']  = $this->nama_singkat;
        // $array['barcode']       = $this->barcode;

        return $array;
    }

    public function searchableAs()
    {
       return 'produk';
    }
}
