<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PengeluaranAktiva extends Model
{
    protected $table = 'pengeluaran_aktiva';
    public $timestamps = true;
    use SoftDeletes;
    
    protected $fillable = [
        'akun_pengeluaran_aktiva_id',
        'daftar_aktiva_tetap_id',
        'tanggal',
        'keterangan',
        'jumlah'
	];

    public function daftarAktivaTetap()
    {
        return $this->belongsTo(DaftarAktivaTetap::class,'daftar_aktiva_tetap_id');
    }

    public function akun()
    {
        return $this->belongsTo(Akun::class, 'akun_pengeluaran_aktiva_id');
    }
}
