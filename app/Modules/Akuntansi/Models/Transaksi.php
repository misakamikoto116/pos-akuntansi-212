<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;
use Helpers\IndonesiaDate;
use Helpers\TransaksiHelper;

class Transaksi extends Model
{
	protected $table      = 'transaksi';
	protected $append     = ['tanggal_formatted', 'dk_formatted', 'priode'];
	protected $dates      = ['tanggal'];
	// protected $attributes = ['priode'];
	public $timestamps    = true;
	use SoftDeletes;

	protected $fillable = [
		'nominal',
		'tanggal',
		'akun_id',
		'item_id',
		'item_type',
		'status',
		'produk_id',
		'no_transaksi',
		'desc_dev',
		'sumber',
		'dari',
		'keterangan',
		'status_rekonsiliasi',
		'created_at',
		'updated_at',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function produkDetail()
	{
		return $this->hasMany(SaldoAwalBarang::class);
	}

	public function akun()
	{
		return $this->belongsTo(Akun::class);
	}

	public function historyRekonsiliasi()
	{
		return $this->belongsToMany(HistoryRekonsiliasi::class,'detail_history_rekonsiliasi');
	}

	public function item()
	{
		return $this->morphTo();
	}

	public function getPriodeAttribute()
	{
		return $this->tanggal->format('F Y');
	}

	public function getTanggalFormattedAttribute()
	{
		$helper = new IndonesiaDate();

		return $helper->blogDate($this->getAttribute('tanggal'));
	}

	public function getTahunAttribute()
	{
		$helper = new IndonesiaDate();

		return $helper->tahunDiff($this->getAttribute('tanggal'));
	}

	public function getNominalFormattedAttribute()
	{
		return 'Rp. ' . number_format($this->getAttribute('nominal'));
	}

	public function getDkFormattedAttribute()
	{
		$helper = new LayoutHelper();
		if (0 == $this->getAttribute('status')) {
			return $helper->formLabel('warning', 'Kredit');
		} else {
			return $helper->formLabel('success', 'Debit');
		}
	}

	public function getGenerateLinkAttribute()
	{
		$helper     = new TransaksiHelper();
		$nama_kelas = (new \ReflectionClass($this->getAttribute('item_type')))->getShortName();

		//khusus karena tidak seperti biasanya yang menggunakan anak bernama 'barang'
		if ('PenerimaanPenjualan' == $nama_kelas) {
			return '/akuntansi/penerimaan-penjualan/' . $this->getAttribute('item_id') . '/edit';
		}

		//khusus
		if ('TransaksiUangMuka' == $nama_kelas) {
			return '/akuntansi/uang-muka-pelanggan/' . $this->getAttribute('item_id') . '/edit';
		}

		//khusus
		if ('TransaksiUangMukaPemasok' == $nama_kelas) {
			return '/akuntansi/uang-muka-pemasok/' . $this->getAttribute('item_id') . '/edit';
		}

		$base_url = $helper->filterClass($this->getAttribute('item_type'), 'barang', $this->getAttribute('item_id'));
		// return $this->getAttribute('item_type');
		return $base_url;
	}

	public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			if($request->get('start-date') === $request->get('end-date')){
				return $query->whereDate('tanggal', $request->get('start-date'));
			}else{
				return $query->whereBetween('tanggal',[$request->get('start-date'), $request->get('end-date')]);
			}
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('tanggal', [$tgl_satu, date('Y-m-t')]);
		}
	}

	public function scopeDateBankFilter($query, $request)
	{
		if ($request->get('start-date') && $request->get('end-date')) {
			$query->whereBetween('tanggal', [$request->get('start-date'), $request->get('end-date')]);
		} else {
			$query = $query->whereBetween('tanggal', ['0001-01-01', date('Y-m-d', strtotime('+1 days'))]);
		}

		return $query;
	}

	public function getKeteranganFormattedAttribute()
	{
		if (null !== $this->item->keterangan) {
			return $this->item->keterangan;
		} elseif (null !== $this->item->catatan) {
			return $this->item->catatan;
		} elseif (null !== $this->item->memo) {
			return $this->item->memo;
		} else {
			return $this->keterangan;
		}
	}

	public function scopeFilter($query, $data)
	{
		// Filter Pelanggan
		if (!empty($data['pelanggan_select']) || !empty($data['filter_menu'])) {
			$query = $query->where(function ($query_find) use ($data)
					       {
	 	                        $query_find->whereIn('item_type', $data['filter_menu'] ?? [get_class(new PengirimanPenjualan)] )
	 				                       ->whereIn('item_id', PengirimanPenjualan::where('pelanggan_id', $data['pelanggan_select'])->pluck('id'));
					       })
						   ->orWhere(function ($query_find) use ($data)
						   {
						   		$query_find->whereIn('item_type', $data['filter_menu'] ?? [get_class(new FakturPenjualan)] )
	 				                       ->whereIn('item_id', FakturPenjualan::where('pelanggan_id', $data['pelanggan_select'])->pluck('id'));
						   })
						   ->orWhere(function ($query_find) use ($data)
						   {
						   		$query_find->whereIn('item_type', $data['filter_menu'] ?? [get_class(new PenerimaanPenjualan)] )
	 				                       ->whereIn('item_id', PenerimaanPenjualan::where('pelanggan_id', $data['pelanggan_select'])->pluck('id'));
						   })
						   ->orWhere(function ($query_find) use ($data)
						   {
						   		$query_find->whereIn('item_type', $data['filter_menu'] ?? [get_class(new ReturPenjualan)] )
	 				                       ->whereIn('item_id', ReturPenjualan::where('pelanggan_id', $data['pelanggan_select'])->pluck('id'));
						   });
		}

		// Filter Pemasok
		if (!empty($data['pemasok_select']) || !empty($data['filter_menu'])) {
			$query = $query->where(function ($query_find) use ($data)
					       {
	 	                        $query_find->whereIn('item_type', $data['filter_menu'] ?? [get_class(new PenerimaanPembelian)] )
	 				                       ->whereIn('item_id', PenerimaanPembelian::where('pemasok_id', $data['pemasok_select'])->pluck('id'));
					       })
						   ->orWhere(function ($query_find) use ($data)
						   {
						   		$query_find->whereIn('item_type', $data['filter_menu'] ?? [get_class(new FakturPembelian)] )
	 				                       ->whereIn('item_id', FakturPembelian::where('pemasok_id', $data['pemasok_select'])->pluck('id'));
						   })
						   ->orWhere(function ($query_find) use ($data)
						   {
						   		$query_find->whereIn('item_type', $data['filter_menu'] ?? [get_class(new PembayaranPembelian)] )
	 				                       ->whereIn('item_id', PembayaranPembelian::where('pemasok_id', $data['pemasok_select'])->pluck('id'));
						   })
						   ->orWhere(function ($query_find) use ($data)
						   {
						   		$query_find->whereIn('item_type', $data['filter_menu'] ?? [get_class(new ReturPembelian)] )
	 				                       ->whereIn('item_id', ReturPembelian::where('pemasok_id', $data['pemasok_select'])->pluck('id'));
						   });
		}

		// No Transaksi / No Sumber
		if (!empty($data['no_transaksi'])) {
			$query = $query->where('no_transaksi','LIKE','%'.$data['no_transaksi'].'%');
		}

		// No Akun
		if (!empty($data['no_akun'])) {
			$query = $query->whereHas('akun', function ($query) use ($data) {
						$query->where('kode_akun','LIKE','%'.$data['no_akun'].'%');
			});
		}

		// Select Akun
		if (!empty($data['akun_id'])) {
			$query = $query->whereHas('akun', function ($query) use ($data) {
						$query->where('akun_id', $data['akun_id']);
			});
		}

		// Select Tipe AKun
		if (!empty($data['tipe_akun_id'])) {
			$query = $query->whereHas('akun', function ($query) use ($data) {
						$query->where('tipe_akun_id', $data['tipe_akun_id']);
			});
		}

		// Tanggal
		if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
			$query = $query->whereBetween('tanggal',[$data['tanggal_dari'],$data['tanggal_sampai']]);
		}

		// Filter Menu / Name Space
		if (!empty($data['filter_menu'])) {
			$query = $query->whereIn('item_type', $data['filter_menu']);
		}

		if (!empty($data['filter_name_space'])) {
			$query = $query->whereIn('item_type', $data['filter_name_space']);
		}

		return $query;
	}
}
