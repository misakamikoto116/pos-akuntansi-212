<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NomorPajak extends Model
{
    protected $table = 'nomor_pajak';
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
		'dari',
		'sd',
		'tetap',
		'status',
	];
}
