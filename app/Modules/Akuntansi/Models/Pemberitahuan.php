<?php
/**
 * Created by PhpStorm.
 * User: efendihariyadi
 * Date: 14/08/18
 * Time: 14.16
 */

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Pemberitahuan extends Model
{
    protected $table = 'pemberitahuan';
    public $timestamps = true;
    use SoftDeletes;

    protected $fillable = [
        'tujuan',
        'is_read',
        'isi_pemberitahuan',
        'created_by'
    ];

    public function user(){
        return $this->belongsTo(User::class,'created_by');
    }
}