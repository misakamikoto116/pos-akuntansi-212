<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;
class PermintaanPembelian extends Model
{
    protected $table = 'permintaan_pembelian';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'request_no',
		'request_date',
		'status',
        'catatan',
        'tutup_buku',
        'tanggal_tutup_buku',
	];

	public function barangPermintaanPembelian()
	{
		return $this->hasMany(BarangPermintaanPembelian::class);
	}

	public function getStatusFormattedAttribute()
    {
        $helper = new LayoutHelper;
        return $kondisi = ($this->getAttribute('status')) ? $helper->formLabel('success','Ditutup') : $helper->formLabel('primary','Sedang Diproses') ;
	}

	public function scopeFilter($query, $data)
    {
    	// Seacrh By Keyword
        if(!empty($data['search'])){
            $query = $query->where('request_no','like','%'.$data['search'].'%')
				->orWhere('catatan','like','%'.$data['search'].'%');
		}

		// No Request
		if(!empty($data['no_faktur'])){
			$query = $query->where('request_no', $data['no_faktur']);
		}

		// Keterangan
		if(!empty($data['keterangan_pembayaran'])){
			$query = $query->where('catatan', $data['keterangan_pembayaran']);
		}

		// Status Penjualan
		if (!empty($data['status_penjualan'])) {
		    $query = $query->where('status','LIKE','%'.$data['status_penjualan'].'%');
		}else if ($data['status_penjualan'] == 0) {
		        $query = $query->where('status','LIKE','%'.$data['status_penjualan'].'%');
		}
		
		// Tanggal
		if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
			$query = $query->whereBetween('request_date',[$data['tanggal_dari'],$data['tanggal_sampai']]);
		}
		
        return $query;
	}
	public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
