<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TingkatHargaBarang extends Model
{
    protected $table = 'tingkat_harga_barang';
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
		'tingkat_1',
		'tingkat_2',
		'tingkat_3',
		'tingkat_4',
		'tingkat_5',
		'produk_id',
	];

	public function produk()
	{
		return $this->belongsTo(Produk::class,'id','produk_id');
	}
}
