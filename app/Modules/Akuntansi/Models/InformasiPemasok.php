<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;

class InformasiPemasok extends Model
{
    protected $table = 'informasi_pemasok';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'nama', 'foto_pemasok', 'nik', 'foto_nik_pemasok', 'no_pemasok','alamat_pajak','alamat','kota','prop','kode_pos','negara','telepon','personal_kontak','email','halaman_web','syarat_pembayaran_id','mata_uang_id','keterangan','pajak_satu_id','pajak_dua_id','pajak_faktur','npwp','no_pkp','tipe_pajak_id','catatan','created_by','updated_by','deleted_by',
	];

	public static function boot()
	{
		parent::boot();

		self::deleting(function ($info) {
			$layout = new LayoutHelper;
			if (
				$info->fakturPembelian->where('catatan','!=','Saldo Awal')->count() > 0 || 
				$info->pembayaranPembelian->count() > 0 ||
				$info->returPembelian->count() > 0 ||
				$info->pesananPembelian->count() > 0 
			) {
				$html = 'Pemasok ini telah memiliki transaksi.';
				return $layout->batalkanProses($html);				
			}
		});
	}

	public function detailInformasiPemasok()
	{
		return $this->hasMany(DetailInformasiPemasok::class);
	}

	public function kontakPemasok()
	{
		return $this->hasMany(KontakPelanggan::class,'pemasok_id','id');
	}

	public function getSumPemasokAttribute() // awalnya getSumAttribute()
	{
		return $this->detailInformasiPemasok->sum('saldo_awal');
	}

	public function fakturPembelian()
	{
		return $this->hasMany(FakturPembelian::class,'pemasok_id','id');
	}

	public function mataUang()
	{
		return $this->belongsTo(MataUang::class,'mata_uang_id');
	}
	
	public function getPembayaranPembelian(){
		$sumPembayaran	=	$this->hasMany(FakturPembelian::class,'pemasok_id','id')
							->whereHas('invoice')->first();
		if($sumPembayaran === null){
			return 0;
		}
		return $sumPembayaran->invoice->sum('payment_amount');
	}

	public function sumSaldoPemasok()
	{
		$sumPembayaran	= $this->getPembayaranPembelian();
		$sumFaktur		= $this->fakturPembelian()->sum('total');
		$sumSaldo		= $sumFaktur - $sumPembayaran;
		return $sumSaldo;
	}

	public function pembayaranPembelian()
	{
		return $this->hasMany(PembayaranPembelian::class,'pemasok_id','id');
	}

	public function pesananPembelian()
	{
		return $this->hasMany(PesananPembelian::class,'pemasok_id','id');
	}

	public function returPembelian()
	{
		return $this->hasMany(ReturPembelian::class,'pemasok_id','id');
	}

	public function syaratPembayaran()
	{
		return $this->belongsTo(SyaratPembayaran::class,'syarat_pembayaran_id');
	}

	public function scopeReportInvoiceFilter($query, $request)
    {
    	if($request->get('start-date')){
			$query = $query->whereHas('fakturPembelian', function ($query) use ($request) {
				$tgl_satu = date('Y-m')."-01";
				$query->whereBetween('invoice_date', [$tgl_satu." 00:00:00", $request->get('start-date')." 23:59:59"]);
			});
		}
		return $query;
    }

    public function scopeReportRincianFilter($query, $request)
    {
    	if($request->get('start-date')){
			$query = $query->whereHas('fakturPembelian', function ($query) use ($request) {
				$query->whereBetween('invoice_date', ['0001-01-01', $request->get('start-date')]);
			});
		}
		return $query;
    }

    public function scopeFilter($query,$data)
    {
    	// Keyword
		if(!empty($data['search'])){
            $query = $query->where('no_pemasok','like','%'.$data['search'].'%')
				           ->orWhere('nama','like','%'.$data['search'].'%')
				           ->orWhere('telepon','like','%'.$data['search'].'%');
		}

    	// No Pelanggan
		if (!empty($data['no_pemasok'])) {
			$query = $query->where('no_pemasok','LIKE','%'.$data['no_pemasok'].'%');
		}

		// Nama Pelanggan
		if (!empty($data['nama_pemasok'])) {
			$query = $query->where('nama','LIKE','%'.$data['nama_pemasok'].'%');
		}

		// Select Tipe Pelanggan
		if (!empty($data['mata_uang'])) {
			$query = $query->whereHas('mataUang', function ($query) use ($data) {
						$query->where('mata_uang_id', $data['mata_uang']);
			});
		}

		return $query;
    }

    public function scopeFilterAvailablePemasok($query, $data)
    {
    	if (array_key_exists('pemasok_transaksi', $data->all())) {
    		$query = $query->orWhereHas('fakturPembelian')->orWhereHas('pembayaranPembelian')->orWhereHas('returPembelian');
    	}
    }
}
