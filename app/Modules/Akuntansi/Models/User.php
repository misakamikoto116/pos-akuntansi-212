<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use HasApiTokens;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users';
    
    protected $fillable = [
        'name', 'email', 'password','logged',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeFilter($query, $data)
    {

        if(!empty($data['search'])){
            $query = $query->where('name','like','%'.$data['search'].'%')
                ->orWhere('email','like','%'.$data['search'].'%');
        }

        if(!empty($data['role'])){
            $query = $query->get()->reject(function($item) use ($data){
                return $item->role->name != $data['role'];
            });
            return $query;
        }
        
        return $query->get();
    }

    public function getRoleAttribute()
    {
        return $this->roles()->first();
    }

    public function getRoleNameFormatAttribute()
    {
        switch ($this->roles()->first()->name) {
            case 'akuntansi':
                return 'Akuntansi';
                break;
            
            case 'admin':
                return 'Admin';
                break;
            default:
                return ucfirst($this->roles()->first()->name);
                break;
        }
    }

    public function fakturPenjualan()
    {
        return $this->hasMany(FakturPenjualan::class,'created_by');
    }
}
