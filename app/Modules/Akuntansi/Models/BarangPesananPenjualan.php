<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangPesananPenjualan extends Model
{
    protected $table = 'barang_pesanan_penjualan';
	public $timestamps = true;
	protected $appends = ['harga_total'];
	use SoftDeletes;
	protected $fillable = [
	                'produk_id',
	                'pesanan_penjualan_id',
	                'barang_penawaran_penjualan_id',
                	'item_deskripsi',
                	'jumlah',
					'harga',
					'diskon',
                	'terproses',
                	'ditutup',
                	'satuan',
                	'kode_pajak_id',
                	'kode_pajak_2_id',
                	'harga_modal',
                	'created_by',
	];

	public function getHargaTotalAttribute()
	{
		$total 	= $this->getAttribute('harga') * $this->getAttribute('jumlah');
		$tax 	= $total * $this->getAttribute('pajak') / 100;
		$disc 	= $total * $this->getAttribute('diskon') / 100;

		$grandtotal = $total - $disc + $tax;

		return $grandtotal;
	}

	public function produk()
	{
		return $this->belongsTo(Produk::class,'produk_id');
	}

	public function pesananPenjualan()
    {
        return $this->belongsTo(PesananPenjualan::class,'pesanan_penjualan_id');
    }

    public function barangPenawaranPenjualan()
    {
    	return $this->belongsTo(BarangPenawaranPenjualan::class,'barang_penawaran_penjualan_id');
    }

    public function barangPengirimanPenjualan()
    {
    	return $this->hasOne(BarangPengirimanPenjualan::class);
	}
	public function gudang()
	{
		return $this->belongsTo(Gudang::class, 'gudang_id', 'id');
	}
    public function kodePajak()
    {
        return $this->belongsTo(KodePajak::class,'kode_pajak_id');
    }

    public function kodePajak2()
  {
    return $this->belongsTo(KodePajak::class, 'kode_pajak_2_id');
  }
}
