<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PemindahanBarang extends Model
{
	protected $table = 'pemindahan_barang';
	public $timestamps = true;
	protected $fillable = [
		'no_transfer',
		'tanggal',
		'dari_gudang_id',
		'alamat_dari_gudang',
		'ke_gudang_id',
		'alamat_ke_gudang',
		'keterangan',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function detailPemindahanBarang(){
		return $this->hasMany(DetailPemindahanBarang::class);
	}

	public function dataKeGudang(){
		return $this->belongsTo(Gudang::class, 'ke_gudang_id');
	}
	public function dataDariGudang(){
		return $this->belongsTo(Gudang::class, 'dari_gudang_id');
	}
	
	public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}

	public function scopeFilter($query, $data)
	{
		// Keyword
		if(!empty($data['search'])){
            $query = $query->where('no_transfer','like','%'.$data['search'].'%')
				           ->orWhere('keterangan','like','%'.$data['search'].'%');
		}

		// No Transfer
		if (!empty($data['no_transfer'])) {
			$query = $query->where('no_transfer','LIKE','%'.$data['no_transfer'].'%');
		}

		// Pindah Dari
		if (!empty($data['pindah_dari'])) {
			$query = $query->where('dari_gudang_id','LIKE','%'.$data['pindah_dari'].'%');
		}

		// Pindah Ke
		if (!empty($data['pindah_ke'])) {
			$query = $query->where('ke_gudang_id','LIKE','%'.$data['pindah_ke'].'%');
		}

		// Tanggal
		if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
			$query = $query->whereBetween('tanggal',[$data['tanggal_dari'],$data['tanggal_sampai']]);
		}

		return $query;
	}
}