<?php

namespace App\Modules\Akuntansi\Models;

use Helpers\LayoutHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KodePajak extends Model
{
    protected $table = 'kode_pajak';
    public $timestamps = true;
    use SoftDeletes;

    protected $fillable = [
        'nama',
        'nilai',
        'kode',
        'keterangan',
        'akun_pajak_penjualan_id',
        'akun_pajak_pembelian_id',
    ];

    public static function boot()
    {
        parent::boot();
        
        self::deleting(function ($kode_pajak) {
            $layout = new LayoutHelper;

            if (!empty($kode_pajak->produk)) {
                if ($kode_pajak->produk->count() > 0) {
                    $html = 'Kode Pajak tidak dapat dihapus, karena masih digunakan oleh Produk . ';
                    return $layout->batalkanProses($html);
                }
            }

            if (!empty($kode_pajak->barangPesananPenjualan)) {
                if ($kode_pajak->barangPesananPenjualan->count() > 0) {
                    $html = 'Kode Pajak tidak dapat dihapus, karena masih digunakan oleh Barang Pesanan Penjualan . ';
                    return $layout->batalkanProses($html);
                }
            }

            if (!empty($kode_pajak->akunpajakpenjualan)) {
                if ($kode_pajak->akunpajakpenjualan->count() > 0) {
                    $html = 'Kode Pajak tidak dapat dihapus, karena masih digunakan oleh Akun . ';
                    return $layout->batalkanProses($html);
                }
            }

            if (!empty($kode_pajak->akunpajakpembelian)) {
                if ($kode_pajak->akunpajakpembelian->count() > 0) {
                    $html = 'Kode Pajak tidak dapat dihapus, karena masih digunakan oleh Akun . ';
                    return $layout->batalkanProses($html);
                }
            }

            if (!empty($kode_pajak->barangFakturPembelian)) {
                if ($kode_pajak->barangFakturPembelian->count() > 0) {
                    $html = 'Kode Pajak tidak dapat dihapus, karena masih digunakan oleh Barang Faktur Pembelian . ';
                    return $layout->batalkanProses($html);
                }
            }

            if (!empty($kode_pajak->barangFakturPenjualan)) {
                if ($kode_pajak->barangFakturPenjualan->count() > 0) {
                    $html = 'Kode Pajak tidak dapat dihapus, karena masih digunakan oleh Barang Faktur Penjualan . ';
                    return $layout->batalkanProses($html);
                }
            }

            if (!empty($kode_pajak->barangPesananPembelian)) {
                if ($kode_pajak->barangPesananPembelian->count() > 0) {
                    $html = 'Kode Pajak tidak dapat dihapus, karena masih digunakan oleh Barang Pesanan Pembelian . ';
                    return $layout->batalkanProses($html);
                }
            }

            if (!empty($kode_pajak->barangReturPembelian)) {
                if ($kode_pajak->barangReturPembelian->count() > 0) {
                    $html = 'Kode Pajak tidak dapat dihapus, karena masih digunakan oleh Barang Retur Pembelian . ';
                    return $layout->batalkanProses($html);
                }
            }

            if (!empty($kode_pajak->barangReturPenjualan)) {
                if ($kode_pajak->barangReturPenjualan->count() > 0) {
                    $html = 'Kode Pajak tidak dapat dihapus, karena masih digunakan oleh Barang Retur Penjualan . ';
                    return $layout->batalkanProses($html);
                }
            }            

        });
    }

    public function produk()
    {
        return $this->hasMany(Produk::class, 'kode_pajak_penj_id', 'id');
    }

    public function scopePluckCode()
    {
        $data = ['0' => 'Pilih Pajak'];
        foreach (self::get() as $code) {
            $data += [$code->nilai => $code->nilai.' | '.$code->nama];
        }

        return $data;
    }

    public function getKodePajakFormattedAttribute()
    {
        return $this->getAttribute('nilai').'/'.$this->getAttribute('nama');
    }

    public function barangPesananPenjualan()
    {
        return $this->hasMany(BarangPesananPenjualan::class);
    }

    public function akunpajakpenjualan()
    {
        return $this->hasOne(Akun::class, 'id', 'akun_pajak_penjualan_id');
    }

    public function akunpajakpembelian()
    {
        return $this->hasOne(Akun::class, 'id', 'akun_pajak_pembelian_id');
    }

    public function barangFakturPembelian()
    {
        return $this->hasMany(BarangFakturPembelian::class);
    }

    public function barangFakturPenjualan()
    {
        return $this->hasMany(BarangFakturPenjualan::class);
    }

    public function barangPesananPembelian()
    {
        return $this->hasMany(BarangPesananPembelian::class);
    }

    public function barangReturPembelian()
    {
        return $this->hasMany(BarangReturPembelian::class);
    }

    public function barangReturPenjualan()
    {
        return $this->hasMany(BarangReturPenjualan::class);
    }
}
