<?php

namespace App\Modules\Akuntansi\Models;

use Helpers\LayoutHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PenyesuaianPersediaan extends Model
{
    protected $table = 'penyesuaian_persediaan';
    public $timestamps = true;
    use SoftDeletes;

    protected $fillable = [
        'no_penyesuaian',
        'tgl_penyesuaian',
        'akun_penyesuaian',
        'kode_akun_id',
        'keterangan',
        'status',
        'value_penyesuaian',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function detailPenyesuaian()
    {
        return $this->hasMany(DetailPenyesuaianPersediaan::class);
    }

    public function akun()
    {
        return $this->belongsTo(Akun::class, 'akun_penyesuaian');
    }

    public function getTipePenyesuaianFormattedAttribute()
    {
        $helper = new LayoutHelper;
        $tipe_penyesuaian = '';
        if ($this->getAttribute('value_penyesuaian') == 0) {
            $tipe_penyesuaian = $helper->formLabel('warning','Penyesuaian Kuantitas');
        }else {
            $tipe_penyesuaian = $helper->formLabel('success','Penyesuaian Nilai');
        }

        return $tipe_penyesuaian;

    }

    public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}

    public function scopeFilter($query, $data)
    {
        // Keyword
        if(!empty($data['search'])){
            $query = $query->where('no_penyesuaian','like','%'.$data['search'].'%')
                           ->orWhere('keterangan','like','%'.$data['search'].'%');
        }

        // No Faktur
        if (!empty($data['no_faktur'])) {
            $query = $query->where('no_penyesuaian','LIKE','%'.$data['no_faktur'].'%');
        }

        // Keterangan
        if (!empty($data['keterangan_pembayaran'])) {
            $query = $query->where('keterangan','LIKE','%'.$data['keterangan_pembayaran'].'%');
        }

        // Tipe Penyesuaian
        if (!empty($data['tipe_penyesuaian'])) {
            $query = $query->where('value_penyesuaian','LIKE','%'.$data['tipe_penyesuaian'].'%');
        }else if ($data['tipe_penyesuaian'] == 0) {
                $query = $query->where('value_penyesuaian','LIKE','%'.$data['tipe_penyesuaian'].'%');
        }

        // Tanggal
        if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
            $query = $query->whereBetween('tgl_penyesuaian',[$data['tanggal_dari'],$data['tanggal_sampai']]);
        }

        return $query;
    }
}
