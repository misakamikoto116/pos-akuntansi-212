<?php
namespace App\Modules\Akuntansi\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PesananPenjualan extends Model
{
	protected $table   = 'pesanan_penjualan';
	public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
		'pelanggan_id',
		'pelanggan_kirim_ke_id',
		'alamat_pengiriman',
		'alamat_tagihan',
		'taxable',
		'in_tax',
		'diskon',
		'total_potongan_rupiah',
		'ongkir',
		'total',
		'status',
		'keterangan',
		'approved',
		'catatan',
		'po_number',
		'so_number',
		'so_date',
		'ship_date',
		'fob',
		'term_id',
		'akun_dp_id',
		'approved_by',
		'ship_id',
		'nilai_pajak',
		'created_by',
		'tutup_buku',
        'tanggal_tutup_buku',
	];

	public function tokoKasir()
	{
		return $this->belongsTo(User::class, 'created_by', 'id');
	}

	public function gudangApproved(){
		return $this->belongsTo(User::class, 'approved_by', 'id');
	}

	public function pelanggan()
	{
		return $this->belongsTo(InformasiPelanggan::class, 'pelanggan_id', 'id');
	}

	public function barang()
	{
		return $this->hasMany(BarangPesananPenjualan::class, 'pesanan_penjualan_id', 'id');
	}

	public function syaratPembayaran()
	{
		return $this->belongsTo(SyaratPembayaran::class,'term_id','id');
	}

	public function scopeAvailableOnly($query)
	{
		return $query->where('status', 0);
	}

	public function getStatusFormattedAttribute()
	{
		switch ($this->getAttribute('status')) {
			case 0:
				return $this->formLabel('info', 'Mengantri');
			break;

			case 1:
				return $this->formLabel('primary', 'Menunggu');
			break;

			case 2:
				return $this->formLabel('success', 'Terproses');
			break;

			default:
				return $this->formLabel('danger', 'Ditutup');
			break;
		}
	}

	public function formLabel($status, $string)
	{
		return "<span class='label label-" . $status . "'>" . $string . '</span>';
	}

	public function getPajakAttribute()
	{
		if ($this->getAttribute('taxable')) {
			return 10;
		} else {
			return 0;
		}
	}

	public function getNilaiFakturAttribute()
	{
		$diskon       = $this->getAttribute('diskon');
		$get_barang   = self::barang()->get();
		$total_barang = 0;
		foreach ($get_barang as $barang) {
			$total_barang += $barang->harga_total;
		}

		$get_diskon = $total_barang * $diskon / 100;
		$get_ppn    = 0;
		if (1 == $this->getAttribute('taxable')) {
			$get_ppn = $total_barang * 10 / 100;
		}

		return $total_barang - $get_diskon + $get_ppn;
	}

	public function scopeFilter($query, $data)
    {
    	// Search By Keyword
    	if(!empty($data['search'])){
            $query = $query->where('po_number','like','%'.$data['search'].'%')
                           ->orWhere('so_number','like','%'.$data['search'].'%')
                           ->orWhere('alamat_pengiriman','like','%'.$data['search'].'%')
				           ->orWhere('keterangan','like','%'.$data['search'].'%');
		}

		// No Faktur
		if(!empty($data['no_faktur'])){
			$query = $query->where('so_number', $data['no_faktur']);
		}

		// Keterangan
		if(!empty($data['keterangan_pembayaran'])){
			$query = $query->where('keterangan', $data['keterangan_pembayaran']);
		}

		// Pelanggan Select
		if(!empty($data['pelanggan_select'])){
			$query = $query->where('pelanggan_id', $data['pelanggan_select']);
		}

		// Status Penjualan
		if (!empty($data['status_penjualan'])) {
		    $query = $query->where('status','LIKE','%'.$data['status_penjualan'].'%');
		}else if ($data['status_penjualan'] == 0) {
		        $query = $query->where('status','LIKE','%'.$data['status_penjualan'].'%');
		}
		
		// Tanggal
		if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
			$query = $query->whereBetween('so_date',[$data['tanggal_dari'],$data['tanggal_sampai']]);
		}

		// if (!empty($data['search_pelanggan'])) {
		// 	$query = $query->whereHas('pelanggan', function($query) use ($data){
		// 			 $query->where('nama', 'like', '%'.$data['search_pelanggan'].'%')
		// 			       ->orWhere('no_pelanggan', 'like', '%'.$data['search_pelanggan'].'%');
		// 		}
		// 	);
		// }
		
        return $query;
	}
	
	public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			if($request->get('start-date') === $request->get('end-date')){
				return $query->whereDate('so_date', $request->get('start-date'));
			}else{
				return $query->whereBetween('so_date',[$request->get('start-date'), $request->get('end-date')]);
			}
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('so_date', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
