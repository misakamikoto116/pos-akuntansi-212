<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MetodePenyusutan extends Model
{
    protected $table = 'metode_penyusutan';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'nama',
		'metode',
	];
	public function tipeAktivaTetapPajak()
	{
		return $this->hasOne(TipeAktivaTetapPajak::class);
	}

	public function daftarAktivaTetap()
	{
		return $this->hasMany(DaftarAktivaTetap::class);
	}
}
