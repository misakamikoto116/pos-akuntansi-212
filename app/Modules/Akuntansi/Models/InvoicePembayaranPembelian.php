<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoicePembayaranPembelian extends Model
{
    protected $table = 'invoice_pembayaran_pembelian';

    public $timestamps = true;
    use SoftDeletes;
    protected $fillable = [
        'faktur_id',
        'pembayaran_pembelian_id',
        'payment_amount',
        'last_owing',
        'tanggal',
        'created_at',
        'updated_at',                
        'deleted_at',                
    ];

    public function pembayaranPembelian()
    {
        return $this->belongsTo(PembayaranPembelian::class,'pembayaran_pembelian_id');
    }

    public function fakturPembelian()
    {
        return $this->belongsTo(FakturPembelian::class,'faktur_id');
    }

    public function scopeReportInvoiceFilter($query, $request)
    {
        if($request->get('start-date')){
                $tgl_satu = date('Y-m')."-01";
                $query->whereBetween('tanggal', [$tgl_satu." 00:00:00", $request->get('start-date')." 23:59:59"]);
        }
        return $query;
    }

    public function scopeReportRincianFilter($query, $request)
    {
        if($request->get('start-date')){
                $query->whereBetween('tanggal', ['0001-01-01', $request->get('start-date')]);
        }
        return $query;
    }
}
