<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;

class JasaPengiriman extends Model
{
    protected $table = 'jasa_pengiriman';
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
	  	'nama',
	  	'created_by',
		'updated_by',
		'deleted_by',
	];

	public static function boot()
	{
		parent::boot();

		self::deleting(function ($jasa) {
			$layout = new LayoutHelper;
			
			if ($jasa->pesananPenjualan->count() > 0) {
				$html = 'Jasa Pengiriman tidak dapat dihapus, karena masih memiliki pesanan Penjualan.';
				return $layout->batalkanProses($html);
			}
			
			if ($jasa->pesananPembelian->count() > 0) {
				$html = 'Jasa Pengiriman tidak dapat dihapus, karena masih memiliki pesanan Pembelian.';
				return $layout->batalkanProses($html);
			}

			if ($jasa->pengirimanPenjualan->count() > 0) {
				$html = 'Jasa Pengiriman tidak dapat dihapus, karena masih memiliki pengiriman Penjualan.';
				return $layout->batalkanProses($html);
			}

			if ($jasa->penerimaanPembelian->count() > 0) {
				$html = 'Jasa Pengiriman tidak dapat dihapus, karena masih memiliki penerimaan Pembelian.';
				return $layout->batalkanProses($html);
			}

			if ($jasa->fakturPenjualan->count() > 0) {
				$html = 'Jasa Pengiriman tidak dapat dihapus, karena masih memiliki faktur Penjualan.';
				return $layout->batalkanProses($html);
			}

			if ($jasa->fakturPembelian->count() > 0) {
				$html = 'Jasa Pengiriman tidak dapat dihapus, karena masih memiliki faktur Pembelian.';
				return $layout->batalkanProses($html);
			}
		});
	}

	public function pesananPenjualan()
	{
		return $this->hasMany(PesananPenjualan::class, 'ship_id', 'id');
	}

	public function pesananPembelian()
	{
		return $this->hasMany(PesananPembelian::class, 'ship_id', 'id');
	}

	public function pengirimanPenjualan()
	{
		return $this->hasMany(PengirimanPenjualan::class, 'ship_id', 'id');
	}

	public function penerimaanPembelian()
	{
		return $this->hasMany(PenerimaanPembelian::class, 'ship_id', 'id');
	}

	public function fakturPenjualan()
	{
		return $this->hasMany(FakturPenjualan::class, 'ship_id', 'id');
	}

	public function fakturPembelian()
	{
		return $this->hasMany(FakturPembelian::class, 'ship_id', 'id');
	}
}
