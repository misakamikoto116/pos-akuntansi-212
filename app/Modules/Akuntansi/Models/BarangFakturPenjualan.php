<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class BarangFakturPenjualan extends Model
{
    protected $table = "barang_faktur_penjualan";
    public $timestamps = true;
    protected $appends = ['harga_total'];
    use SoftDeletes;

    protected $fillable = [
   			'produk_id',
   			'faktur_penjualan_id',
   			'barang_pengiriman_penjualan_id',
   			'item_deskripsi',
   			'item_unit',
   			'jumlah',
   			'diskon',
   			'kode_pajak_id',
   			'kode_pajak_2_id',
   			'gudang_id',
            'sn',
            'unit_price',
            'harga_modal',
            'harga_final',
            'bagian_diskon_master',

    ];

  public function fakturPenjualan()
  {
    return $this->belongsTo(FakturPenjualan::class,'faktur_penjualan_id');
  }

  public function barangReturPenjualan()
  {
    return $this->hasOne(BarangReturPenjualan::class);
  }

  public function pajak()
  {
    return $this->belongsTo(KodePajak::class, 'kode_pajak_id');
  }

  public function pajak2()
  {
    return $this->belongsTo(KodePajak::class, 'kode_pajak_2_id');
  }
  
  public function getHargaTotalAttribute()
	{
		$total 	= $this->getAttribute('unit_price') * $this->getAttribute('jumlah');
		$disc 	= $total * $this->getAttribute('diskon') / 100;

		$grandtotal = $total - $disc;

		return $grandtotal; 
	}

	public function produk()
	{
		return $this->belongsTo(Produk::class, "produk_id");
	}

  public function barangPengirimanPenjualan()
  {
    return $this->belongsTo(BarangPengirimanPenjualan::class,'barang_pengiriman_penjualan_id');
  }
  public function gudang()
	{
		return $this->belongsTo(Gudang::class, 'gudang_id', 'id');
	}
  public function scopeCekDeliveryNo($query)
  {
    $query = '';
            if ($this->barangPengirimanPenjualan === null) {
                $query = '';
             }else {
                if ($this->barangPengirimanPenjualan->pengirimanPenjualan === null) {
                  $query = '';
                }else {
                  if ($this->barangPengirimanPenjualan->pengirimanPenjualan->delivery_no === null) {
                    $query = '';
                  }else {
                    $query = $this->barangPengirimanPenjualan->pengirimanPenjualan->delivery_no;
                  }
                }
             }
             return $query;
  }

  public function scopeCekSONo($query)
  {
    $query = '';
            if ($this->barangPengirimanPenjualan === null) {
                $query = '';
             }else {
                if ($this->barangPengirimanPenjualan->barangPesananPenjualan === null) {
                  $query = '';
                }else {
                  if ($this->barangPengirimanPenjualan->barangPesananPenjualan->pesananPenjualan === null) {
                    $query = '';
                  }else {
                    if ($this->barangPengirimanPenjualan->barangPesananPenjualan->pesananPenjualan->so_number === null) {
                      $query = '';
                    }else {
                      $query = $this->barangPengirimanPenjualan->barangPesananPenjualan->pesananPenjualan->so_number;
                    }
                  }
                }
             }
             return $query;
  }

  public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
