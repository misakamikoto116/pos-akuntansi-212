<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;

class SyaratPembayaran extends Model
{
    protected $table = 'syarat_pembayaran';
    public $timestamps = true;
	use SoftDeletes;
	
	protected $fillable = [
		'nama',
		'jika_membayar_antara',
		'akan_dapat_diskon',
		'jatuh_tempo',
		'keterangan',
		'status_cod',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public static function boot()
	{
		parent::boot();
		
		self::deleting(function ($syarat) {
			$layout = new LayoutHelper;

			// Pelanggan
			if (!empty($syarat->informasiPelanggan)) {
				if ($syarat->informasiPelanggan->count() > 0) {
					$html = 'Syarat pembayaran tidak dapat dihapus, karena masih memiliki Informasi Pelanggan. ';
					return $layout->batalkanProses($html);
				}
			}

			if (!empty($syarat->detailInformasiPelanggan)) {
				if ($syarat->detailInformasiPelanggan->count() > 0) {
					$html = 'Syarat pembayaran tidak dapat dihapus, karena masih memiliki Detail Informasi Pelanggan. ';
					return $layout->batalkanProses($html);
				}
			}
			///////////////////////////////////////

			// Pemasok
			if (!empty($syarat->detailInformasiPemasok)) {
				if ($syarat->detailInformasiPemasok->count() > 0) {
					$html = 'Syarat pembayaran tidak dapat dihapus, karena masih memiliki Detail Informasi Pemasok. ';
					return $layout->batalkanProses($html);
				}
			}

			if (!empty($syarat->informasiPemasok)) {
				if ($syarat->informasiPemasok->count() > 0) {
					$html = 'Syarat pembayaran tidak dapat dihapus, karena masih memiliki Informasi Pemasok. ';
					return $layout->batalkanProses($html);
				}
			}
			///////////////////////////////////////


			// Penjualan
			if (!empty($syarat->pesananPenjualan)) {
				if ($syarat->pesananPenjualan->count() > 0) {
					$html = 'Syarat pembayaran tidak dapat dihapus, karena masih memiliki Pesanan Penjualan. ';
					return $layout->batalkanProses($html);
				}
			}

			if (!empty($syarat->fakturPenjualan)) {
				if ($syarat->fakturPenjualan->count() > 0) {
					$html = 'Syarat pembayaran tidak dapat dihapus, karena masih memiliki Faktur Penjualan. ';
					return $layout->batalkanProses($html);
				}
			}
			///////////////////////////////////////////////// 

			// Pembelian
			if (!empty($syarat->pesananPembelian)) {
				if ($syarat->pesananPembelian->count() > 0) {
					$html = 'Syarat pembayaran tidak dapat dihapus, karena masih memiliki Pesanan Pembelian. ';
					return $layout->batalkanProses($html);
				}
			}

			if (!empty($syarat->fakturPembelian)) {
				if ($syarat->fakturPembelian->count() > 0) {
					$html = 'Syarat pembayaran tidak dapat dihapus, karena masih memiliki Faktur Pembelian. ';
					return $layout->batalkanProses($html);
				}
			}
			///////////////////////////////////////////////
		});
	}

	public function getNarationAttribute()
	{
		$formatted = '';

		if ($this->getAttribute('nama') !== null) {
			$formatted = $this->getAttribute('nama');
		}else {
			$formatted =  $this->getAttribute("akan_dapat_diskon").'/'.$this->getAttribute("jika_membayar_antara").'/n'.$this->getAttribute("jatuh_tempo");
		}

		return $formatted;
		
	}

	public function informasiPelanggan()
	{
		return $this->hasOne(InformasiPelanggan::class);
	}

	public function informasiPemasok()
	{
		return $this->hasOne(InformasiPemasok::class);
	}

	public function detailInformasiPelanggan()
	{
		return $this->hasMany(DetailInformasiPelanggan::class);
	}

	public function detailInformasiPemasok()
	{
		return $this->hasMany(DetailInformasiPemasok::class);
	}

	public function fakturPenjualan()
	{
		return $this->hasOne(FakturPenjualan::class,'term_id','id');
	}

	public function fakturPembelian()
	{
		return $this->hasOne(FakturPembelian::class,'term_id','id');
	}

	public function pesananPenjualan()
	{
		return $this->hasOne(PesananPenjualan::class,'term_id','id');
	}

	public function pesananPembelian()
	{
		return $this->hasOne(PesananPembelian::class,'term_id','id');
	}

	public function getUserCreatedAttribute()
	{
		$user_id	= $this->getAttribute('created_by');
		$user 		= User::find($user_id);
		$created_by = '-';

		if (!empty($user)) {
			$created_by = $user->name;	
		}

		return ucwords($created_by);
	}

	public function getUserUpdatedAttribute()
	{
		$user_id	= $this->getAttribute('updated_by');
		$user 		= User::find($user_id);
		$updated_by = '-';

		if (!empty($user)) {
			$updated_by = $user->name;	
		}

		return ucwords($updated_by);
	}
}
