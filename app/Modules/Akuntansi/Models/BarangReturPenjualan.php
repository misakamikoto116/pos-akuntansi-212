<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class BarangReturPenjualan extends Model
{
    protected $table = 'barang_retur_penjualan';
    public $timestamps = true;
	use SoftDeletes;
    protected $fillable = [
    	'produk_id',
    	'barang_faktur_penjualan_id',
    	'retur_penjualan_id',
    	'item_deskripsi',
    	'jumlah',
    	'harga',
    	'gudang_id',
    	'kode_pajak_id',
    	'diskon',
        'status',
        'harga_modal',
    	'satuan',
    ];

    public function returPenjualan()
    {
    	return $this->belongsTo(FakturPenjualan::class,'retur_penjualan_id');
	}
	
	public function produk()
	{
		return $this->belongsTo(Produk::class,'produk_id');
	}
	public function rPenjualan()
	{
		return $this->belongsTo(ReturPenjualan::class, 'retur_penjualan_id');
	}

	public function barangFakturPenjualan()
	{
		return $this->belongsTo(BarangFakturPenjualan::class,'barang_faktur_penjualan_id');
	}

	public function gudang()
	{
		return $this->belongsTo(Gudang::class, 'gudang_id', 'id');
	}

	public function pajak()
    {
      return $this->belongsTo(KodePajak::class, 'kode_pajak_id');
    }

    public function pajak2()
    {
      return $this->belongsTo(KodePajak::class, 'kode_pajak_2_id');
    }
	
	public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
