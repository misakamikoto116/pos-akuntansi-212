<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailPenyelesaianPesanan extends Model
{
    use SoftDeletes;
    public $timestamps = true;

    protected $table = "detail_penyelesaian_pesanan";
    protected $fillable = [
        'produk_id',
        'penyelesaian_pesanan_id',
        'kuantitas',
        'biaya',
        'harga_modal',
        'alokasi_nilai',
        'presentase',
        'gudang_id',
        'sn',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function pembiayaanPesanan()
	{
		return $this->belongsTo(PembiayaanPesanan::class, 'pembiayaan_pesanan_id', 'id');
	}

    public function produk()
    {
        return $this->belongsTo(Produk::class,'produk_id');
    }
}
