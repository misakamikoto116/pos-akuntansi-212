<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class PreferensiMataUang extends Model
{
    protected $table = 'preferensi_mata_uang';
    public $timestamps = true;
    // use SoftDeletes;
    
    protected $fillable = [
        'id',
        'akun_hutang_id',
        'akun_piutang_id',
        'uang_muka_pembelian_id',
        'uang_muka_penjualan_id',
        'diskon_penjualan_id',
        'laba_rugi_terealisir_id',
        'laba_rugi_tak_terealisir_id',
        'akun_penyesuaian_id'
    ];

    public function akunHutang()
    {
        return $this->belongsTo(Akun::class,'akun_hutang_id');
    }

    public function akunPiutang()
    {
        return $this->belongsTo(Akun::class,'akun_piutang_id');
    }

    public function akunUangMukaPembelian()
    {
        return $this->belongsTo(Akun::class,'uang_muka_pembelian_id');
    }

    public function akunUangMukaPenjualan()
    {
        return $this->belongsTo(Akun::class,'uang_muka_penjualan_id');
    }

    public function akunDiskonPenjualan()
    {
        return $this->belongsTo(Akun::class,'diskon_penjualan_id');
    }

    public function akunLabaRugiTerRealisir()
    {
        return $this->belongsTo(Akun::class,'laba_rugi_terealisir_id');
    }

    public function akunLabaRugiTakTerRealisir()
    {
        return $this->belongsTo(Akun::class,'laba_rugi_tak_terealisir_id');
    }

    public function akunPenyesuaian()
    {
        return $this->belongsTo(Akun::class,'akun_penyesuaian_id');
    }
}
