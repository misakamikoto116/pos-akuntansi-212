<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\Pos\Models\TransactionNonCash;
class InvoicePenerimaanPenjualan extends Model
{
    protected $table = "invoice_penerimaan_penjualan";
    protected $appends = ['sum_payment'];
    public $timestamps = true;
    use SoftDeletes;
    protected $fillable = [
        'faktur_id',
        'penerimaan_penjualan_id',
        'tanggal',
        'payment_amount',
        'last_owing',
        'diskon',
        'edc',
        'created_at',
        'updated_at',                
        'deleted_at',                
    ];

    public function penerimaanPenjualan()
    {
        return $this->belongsTo(PenerimaanPenjualan::class,'penerimaan_penjualan_id','id');
    }

    public function fakturPenjualan()
    {
        return $this->belongsTo(FakturPenjualan::class,'faktur_id','id');
    }

    public function getSumPaymentAttribute(){
        $data = self::where('faktur_id', $this->getAttribute('faktur_id'))->sum('payment_amount');
        return $data;
    }

    public function scopeReportInvoiceFilter($query, $request)
    {
        if($request->get('start-date')){
                $tgl_satu = date('Y-m')."-01";
                $query->whereBetween('tanggal', [$tgl_satu." 00:00:00", $request->get('start-date')." 23:59:59"]);
        }
        return $query;
    }

    public function scopeReportRincianFilter($query, $request)
    {
        if($request->get('start-date')){
                $query->whereBetween('tanggal', ["0001-01-01 00:00:00", $request->get('start-date')." 23:59:59"]);
        }
        return $query;
    }
}
