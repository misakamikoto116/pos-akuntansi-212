<?php

namespace App\Modules\Akuntansi\Models;

use Helpers\LayoutHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PenyelesaianPesanan extends Model
{
    use SoftDeletes;
    public $timestamps = true;

    protected $table = "penyelesaian_pesanan";
    protected $fillable = [
        'pembiayaan_pesanan_id',
        'tipe_penyelesaian',
        'tgl_selesai',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function item()
    {
        return $this->hasMany(DetailPenyelesaianPesanan::class);
    }

    public function akun()
    {
        return $this->hasOne(AkunPenyelesaianPesanan::class);
    }

    public function pembiayaanPesanan()
    {
        return $this->belongsTo(PembiayaanPesanan::class,'pembiayaan_pesanan_id');
    }
    
    public function getTipePenyelesaianFormattedAttribute()
    {
        $helper = new LayoutHelper;
        $status = '';
        if ($this->getAttribute('tipe_penyelesaian') == 0) {
            $status = $helper->formLabel('warning','To Account');
        }else if ($this->getAttribute('tipe_penyelesaian') == 1) {
            $status = $helper->formLabel('success','To Item');
        }

        return $status;        
    }

    public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
    }
    
}
