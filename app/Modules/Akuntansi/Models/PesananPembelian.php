<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;
class PesananPembelian extends Model
{
    protected $table = 'pesanan_pembelian';
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
	  	 			'no_penawaran',
	  	 			'pemasok_id',
                	'alamat_pengiriman',
                	'po_number',
                	'po_date',
                	'expected_date',
                	'fob',
                	'term_id',
                	'ship_id',
                	'diskon',
                	'total_potongan_rupiah',
                	'ongkir',
                	'total',
                	'keterangan',
					'akun_dp_id',
					'akun_ongkir_id',
					'no_fp_std',
					'no_fp_std2',
					'no_fp_std_date',
                	'taxable',
                	'in_tax',
                	'tutup_buku',
                	'tanggal_tutup_buku',
	];

	public function pemasok()
	{
		return $this->belongsTo(InformasiPemasok::class,'pemasok_id','id');
	}

	public function barang()
	{
		return $this->hasMany(BarangPesananPembelian::class,'pesanan_pembelian_id','id');
	}

	public function syaratPembayaran()
	{
		return $this->belongsTo(SyaratPembayaran::class,'term_id','id');
	}

	public function getStatusFormattedAttribute()
	{
		$layout = new LayoutHelper;
		switch ($this->getAttribute('status')) {
			case 0:
				return $layout->formLabel('info','Sedang Diproses'); 
			break;
			
			case 1:
				return $layout->formLabel('danger','Ditutup'); 
			break;

			case 2:
				return $layout->formLabel('success','Diterima'); 
			break;
			
		}
	}

	public function getSumUpdatedQtyAttribute()
	{
		return $this->barang->sum('updated_qty_penerimaan_pembelian');
	}

	public function scopeFilter($query, $data)
    {
    	// Search By Keyword
        if(!empty($data['search'])){
            $query = $query->where('po_number','like','%'.$data['search'].'%')
                ->orWhere('alamat_pengiriman','like','%'.$data['search'].'%')
				->orWhere('keterangan','like','%'.$data['search'].'%');
		}

		// No Penawaran
		if(!empty($data['no_faktur'])){
			$query = $query->where('po_number', $data['no_faktur']);
		}

		// Keterangan
		if(!empty($data['keterangan_pembayaran'])){
			$query = $query->where('keterangan', $data['keterangan_pembayaran']);
		}

		// Pemasok Select
		if(!empty($data['pemasok_select'])){
			$query = $query->where('pemasok_id', $data['pemasok_select']);
		}
		
		// Status Penjualan
		if (!empty($data['status_penjualan'])) {
		    $query = $query->where('status','LIKE','%'.$data['status_penjualan'].'%');
		}else if ($data['status_penjualan'] == 0) {
		        $query = $query->where('status','LIKE','%'.$data['status_penjualan'].'%');
		}

		// Tanggal
		if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
			$query = $query->whereBetween('po_date',[$data['tanggal_dari'],$data['tanggal_sampai']]);
		}
		
		// if (!empty($data['search_pemasok'])) {
		// 	$query = $query->whereHas('pemasok', function($query) use ($data){
		// 			 $query->where('nama', 'like', '%'.$data['search_pemasok'].'%')
		// 			       ->orWhere('no_pemasok', 'like', '%'.$data['search_pemasok'].'%');
		// 		}
		// 	);
		// }
		
        return $query;
	}
	
	public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
