<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailPembiayaanPesanan extends Model
{
    use SoftDeletes;
    public $timestamps = true;

    protected $table = "detail_pembiayaan_pesanan";
    protected $fillable = [
        'pembiayaan_pesanan_id',
        'produk_id',
        'tanggal',
        'qty',
        'unit',
        'cost',
        'sn'
    ];

    public function produk()
	{
		return $this->belongsTo(Produk::class, 'produk_id', 'id');
    }
    
    public function pembiayaanPesanan()
	{
		return $this->belongsTo(PembiayaanPesanan::class, 'pembiayaan_pesanan_id', 'id');
	}
}
