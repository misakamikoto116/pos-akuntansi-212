<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class BukuKeluar extends Model
{
    protected $table = 'buku_keluar';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
	'akun_id',
	'no_faktur',
	'no_cek',
	'tanggal',
	'keterangan',
	'peruntukan',
	'nominal',
	];
    public function detailBukuKeluar()
	{
		return $this->hasMany(DetailBukuKeluar::class);
	}
}
