<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DaftarAktivaTetap extends Model
{
    protected $table = 'daftar_aktiva_tetap';
    public $timestamps = true;
    use SoftDeletes;
    
	protected $fillable = [
        'kode_aktiva',
        'tipe_aktiva_id',
        'tgl_beli',
        'tgl_pakai',
        'keterangan',
        'qty',
        'departement_id',
        'tgl_jurnal',
        'tahun',
        'bulan',
        'metode_penyusutan_id',
        'rasio',
        'akun_aktiva_id',
        'akun_akum_penyusutan_id',
        'akun_beban_penyusutan_id',
        'aktiva_tidak_berwujud',
        'aktiva_tetap_fisikal',
        'catatan',
        'tutup_buku',
        'tanggal_tutup_buku',
        'created_by',
        'updated_by',
        'deleted_by',
	];

    public function daftarAktivaTetapDetail()
    {
        return $this->hasMany(DaftarAktivaTetapDetail::class, 'daftar_aktiva_tetap_id');
    }

    public function tipeAktivaTetap()
    {
        return $this->belongsTo(TipeAktivaTetap::class,'tipe_aktiva_id');
    }

    public function akunAktiva()
    {
        return $this->belongsTo(Akun::class,'akun_aktiva_id');
    }

    public function akunAkumPenyusutan()
    {
        return $this->belongsTo(Akun::class,'akun_akum_penyusutan_id');
    }

    public function akunBebanPenyusutan()
    {
        return $this->belongsTo(Akun::class,'akun_beban_penyusutan_id');
    }

    public function metodePenyusutan()
    {
        return $this->belongsTo(MetodePenyusutan::class,'metode_penyusutan_id');
    }

    public function pengeluaranAktiva()
    {
        return $this->hasMany(PengeluaranAktiva::class);
    }

    public function getSumPengeluaranAktivaAttribute()
    {
        $id     = $this->getAttribute('id');
        $sum    = $this->pengeluaranAktiva->where('daftar_aktiva_tetap_id',$id)->sum('jumlah');
        return $sum;
    }

    public function getTidakBerwujudAttribute()
    {
        $nilai = '';
        if ($this->getAttribute('aktiva_tidak_berwujud') === 1) {
            $nilai = 'Ya';
        }else {
            $nilai = 'Tidak';
        }
        return $nilai;
    }

    public function getTetapFisikalAttribute()
    {
        $nilai = '';
        if ($this->getAttribute('aktiva_tetap_fisikal') === 1) {
            $nilai = 'Ya';
        }else {
            $nilai = 'Tidak';
        }
        return $nilai;   
    }

    public function getSumMonthAttribute()
    {
        $per_bulan = 12;
        $sum       =  ($this->getAttribute('tahun') * $per_bulan) + $this->getAttribute('bulan');
        return $sum;
    }

    public function scopeFilter($query, $data)
    {
        // Search By Keyword
        if(!empty($data['search'])){
            $query = $query->where('kode_aktiva','like','%'.$data['search'].'%')
                           ->orWhere('keterangan','like','%'.$data['search'].'%');
        }

        // Kode Aktiva
        if(!empty($data['kode_aktiva'])){
            $query = $query->where('kode_aktiva','like','%'.$data['kode_aktiva'].'%');
        }

        // Keterangan Aktiva
        if(!empty($data['keterangan_aktiva'])){
            $query = $query->where('keterangan','like','%'.$data['keterangan_aktiva'].'%');
        }

        // Tipe Aktiva
        if(!empty($data['tipe_aktiva'])){
            $query = $query->where('tipe_aktiva_id', $data['tipe_aktiva']);
        }

        // Metode Penyesutan
        if(!empty($data['metode_penyusutan'])){
            $query = $query->where('metode_penyusutan_id', $data['metode_penyusutan']);
        }

        // Tidak Berwujud
        if (!empty($data['tidak_berwujud'])) {
            if ($data['tidak_berwujud'] == 2) {
                $query = $query;
            }else {
                $query = $query->where('aktiva_tidak_berwujud','LIKE','%'.$data['tidak_berwujud'].'%');
            }
        }else if ($data['tidak_berwujud'] == 0) {
                $query = $query->where('aktiva_tidak_berwujud','LIKE','%'.$data['tidak_berwujud'].'%');
        }

        // Pajak
        if (!empty($data['pajak'])) {
            if ($data['pajak'] == 2) {
                $query = $query;
            }else {
                $query = $query->where('aktiva_tetap_fisikal','LIKE','%'.$data['pajak'].'%');
            }
        }else if ($data['pajak'] == 0) {
                $query = $query->where('aktiva_tetap_fisikal','LIKE','%'.$data['pajak'].'%');
        }

        // Tanggal Pakai
        if (!empty($data['tanggal_dari_pakai']) || !empty($data['tanggal_sampai_pakai'])) {
            $query = $query->whereBetween('tgl_pakai',[$data['tanggal_dari_pakai'],$data['tanggal_sampai_pakai']]);
        }

        // Tanggal Beli
        if (!empty($data['tanggal_dari_perolehan']) || !empty($data['tanggal_sampai_perolehan'])) {
            $query = $query->whereBetween('tgl_beli',[$data['tanggal_dari_perolehan'],$data['tanggal_sampai_perolehan']]);
        }

        return $query;
    }

    public function scopeDateFilter($query, $request){
        if($request->get('start-date') && $request->get('end-date')){
            $request['start_date'] === null ? $request['start_date'] = date('Y-m-d') : $request['start_date'];
            $request['end_date']   === null ? $request['end_date']   = date('Y-m-t') : $request['end_date'];
            if($request->get('start-date') === $request->get('end-date')){
                return $query->whereDate('tgl_jurnal', $request['start_date']);
            }else{
                return $query->whereBetween('tgl_jurnal',[$request['start_date'], $request['end_date']]);
            }
        }else if ($request->get('end-date') === null ){
            $request['start_date'] === null ? $request['start_date'] = date('Y-m-d') : $request['start_date'];
            return $query->whereDate('tgl_jurnal', $request['start_date']);
        }else {
            $tgl_satu = date('Y-m')."-01";
            return $query->whereBetween('tgl_jurnal', [$tgl_satu, date('Y-m-t')]);
        }
    }
}
