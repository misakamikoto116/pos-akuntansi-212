<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;
use Session;
class PenawaranPenjualan extends Model
{
    protected $table = 'penawaran_penjualan';
    public $timestamps = true;
    protected $dates = ['tanggal'];
	use SoftDeletes;

	protected $fillable = [
	  	'no_penawaran',
        'barcode_penawaran',
	  	'pelanggan_id',
    	'alamat_pengiriman',
    	'taxable',
    	'in_tax',
    	'diskon',
    	'total_potongan_rupiah',
    	'ongkir',
    	'nilai_pajak',
		'total',
		'status',
		'tanggal',
    	'keterangan',
    	'catatan',
        'created_by',
        'tutup_buku',
        'tanggal_tutup_buku',
	];

	public static function boot()
	{
		parent::boot();
		self::deleting(function ($type) {
			if ($type->where('status', 2)->count() > 0) {
				$html = 'Penawaran Penjualan tidak dapat dihapus, karena sudah ada pesanan penjualan yang menggunakan penawaran penjualan ini atau penawaran penjualan telah terproses.';
				Session::flash('flash_notification', [
					'level'   => 'danger',
					'message' => $html,
				]);

				return false;
			}
		});
	}

	public function pelanggan()
	{
		return $this->belongsTo(InformasiPelanggan::class, 'pelanggan_id','id');
	}

	public function barang()
	{
		return $this->hasMany(BarangPenawaranPenjualan::class, 'penawaran_penjualan_id','id');
	}

	public function scopeAvailableOnly($query)
	{
		return $query->where('status',0);
	}

	public function getNilaiWithOutOngkirAttribute()
	{
		return $this->getAttribute('total') - $this->getAttribute('ongkir');
	}

	public function getNilaiFakturAttribute()
	{
		$diskon = $this->getAttribute('diskon');
		$get_barang = self::barang()->get();
		$total_barang = 0;
		foreach($get_barang as $barang){
			$total_barang += $barang->harga_total;
		}

		$get_diskon = $total_barang * $diskon / 100;
		$get_ppn	= 0;
		if($this->getAttribute('taxable') == 1){
			$get_ppn = $total_barang * 10 / 100;
		}

		return $total_barang - $get_diskon + $get_ppn;

	}

	public function getDiskonRupiahAttribute()
	{
		$diskon = $this->getAttribute('diskon');
		$get_barang = self::barang()->get();
		$total_barang = 0;
		foreach($get_barang as $barang){
			$total_barang += $barang->jumlah * $barang->harga;
		}

		$get_diskon = $total_barang * $diskon / 100;
		return $get_diskon;
	}

	public function getPajakAttribute()
	{
		if($this->getAttribute('taxable')){
			return 10;
		}else{
			return 0;
		}
	}

	public function getStatusFormattedAttribute()
	{
		$layout = new LayoutHelper;
		switch ($this->getAttribute('status')) {
			case 0:
				return $layout->formLabel('info','Mengantri');
			break;
			
			case 1:
				return $layout->formLabel('primary','Menunggu');
			break;

			case 2:
				return $layout->formLabel('success','Terproses');
			break;
			
			default:
				return $layout->formLabel('danger','Ditutup');
			break;
		}	
	}

	public function scopeFilter($query, $data)
    {
    	// Search By Keyword
        if(!empty($data['search'])){
            $query = $query->where('no_penawaran','like','%'.$data['search'].'%')
                           ->orWhere('alamat_pengiriman','like','%'.$data['search'].'%')
				           ->orWhere('keterangan','like','%'.$data['search'].'%');
		}

		// No Penawaran
		if(!empty($data['no_faktur'])){
			$query = $query->where('no_penawaran', $data['no_faktur']);
		}

		// Keterangan
		if(!empty($data['keterangan_pembayaran'])){
			$query = $query->where('keterangan', $data['keterangan_pembayaran']);
		}

		// Pelanggan Select
		if(!empty($data['pelanggan_select'])){
			$query = $query->where('pelanggan_id', $data['pelanggan_select']);
		}

		// Status Penjualan
		if (!empty($data['status_penjualan'])) {
		    $query = $query->where('status','LIKE','%'.$data['status_penjualan'].'%');
		}else if ($data['status_penjualan'] == 0) {
		        $query = $query->where('status','LIKE','%'.$data['status_penjualan'].'%');
		}
		
		// Tanggal
		if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
			$query = $query->whereBetween('tanggal',[$data['tanggal_dari'],$data['tanggal_sampai']]);
		}

		// if (!empty($data['search_pelanggan'])) {
		// 	$query = $query->whereHas('pelanggan', function($query) use ($data){
		// 			 $query->where('nama', 'like', '%'.$data['search_pelanggan'].'%')
		// 			       ->orWhere('no_pelanggan', 'like', '%'.$data['search_pelanggan'].'%');
		// 		}
		// 	);
		// }
		
        return $query;
	}

	public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			if($request->get('start-date') === $request->get('end-date')){
				return $query->whereDate('tanggal', $request->get('start-date'));
			}else{
				return $query->whereBetween('tanggal',[$request->get('start-date'), $request->get('end-date')]);
			}
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('tanggal', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
