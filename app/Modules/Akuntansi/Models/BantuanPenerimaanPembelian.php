<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BantuanPenerimaanPembelian extends Model
{
    use SoftDeletes;
    protected $table = 'bantu_penerimaan_pembelian';
    protected $fillable = [
                    'produk_id',
                    'brg_penerimaan_pembelian_id',
    ];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id', 'id');
    }

    public function barangPenerimaanPembelian()
    {
        return $this->belongsTo(BarangPenerimaanPembelian::class, 'brg_penerimaan_pembelian_id', 'id');
    }

    public function bantuPengirimanPenjualan()
    {
        return $this->hasMany(BantuanPengirimanPenjualan::class, 'bnt_penerimaan_pembelian_id', 'id');
    }
}
