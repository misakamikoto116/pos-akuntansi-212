<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = "permissions";
    public $timestamps = true;
    protected $fillable = ['name', 'guard_name'];
    protected $dates = ['created_at', 'updated_at'];

    public function scopeFilter($query, $data)
    {
        return $query->where('name','like','%'.$data['search'].'%');
    }
}
