<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;

class HargaJual extends Model
{
    protected $table = 'harga_jual';
    public $timestamps = true;
	
	protected $fillable = [
		'no_barang',
		'barcode',
		'keterangan',
		'harga_jual',
		'nama_singkat',
	];
}
