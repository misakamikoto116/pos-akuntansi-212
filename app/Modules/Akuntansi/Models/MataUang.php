<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MataUang extends Model
{
    protected $table = 'mata_uang';
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
		'negara',
		'kode',
	  	'kode_simbol',
	  	'nama',
	  	'nilai_tukar',
	  	'created_by',
		'updated_by',
		'deleted_by',
	];

	public function identitas()
	{
		return $this->hasOne(Identitas::class);
	}

	public function akun()
	{
		return $this->hasMany(Akun::class);
	}

	public function informasiPelanggan()
	{
		return $this->hasOne(InformasiPelanggan::class);
	}

	public function informasiPemasok()
	{
		return $this->hasOne(InformasiPemasok::class);
	}

	public function getUserCreatedAttribute()
	{
		$user_id	= $this->getAttribute('created_by');
		$user 		= User::find($user_id);
		$created_by = '-';

		if (!empty($user)) {
			$created_by = $user->name;	
		}

		return ucwords($created_by);
	}

	public function getUserUpdatedAttribute()
	{
		$user_id	= $this->getAttribute('updated_by');
		$user 		= User::find($user_id);
		$updated_by = '-';

		if (!empty($user)) {
			$updated_by = $user->name;	
		}

		return ucwords($updated_by);
	}
}
