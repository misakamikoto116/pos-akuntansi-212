<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pembayaran extends Model
{
    protected $table = 'buku_keluar';
    // protected $dates = ['tanggal'];
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
	  	'akun_id',
		'no_faktur',
		'no_cek',
		'tanggal',
		'peruntukan',
		'keterangan',
		'nominal',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function akun()
	{
		return $this->belongsTo(Akun::class);
	}
	public function detailBukuKeluar()
	{
		return $this->hasMany(DetailBukuKeluar::class,'buku_keluar_id');
	}

	public function barang()
	{
		return $this->hasMany(DetailBukuKeluar::class,'buku_keluar_id');
	}

	public function scopeReportPembayaran($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
            $query->whereBetween('tanggal',[$request->get('start-date'), $request->get('end-date')]);
        }else{
            $query = $query->whereBetween('tanggal', ['0001-01-01', date('Y-m-d', strtotime('+1 days'))]);
        }
        return $query;
	}

	public function scopeFilter($query, $data)
	{
		// Keyword
		if(!empty($data['search'])){
            $query = $query->where('no_faktur','like','%'.$data['search'].'%')
                ->orWhere('peruntukan','like','%'.$data['search'].'%')
				->orWhere('keterangan','like','%'.$data['search'].'%')
				->orWhere('no_cek','like','%'.$data['search'].'%');
		}

		// No Pembayaran
		if (!empty($data['no_pembayaran'])) {
			$query = $query->where('no_faktur','LIKE','%'.$data['no_pembayaran'].'%');
		}

		// No Pembayaran
		if (!empty($data['keterangan_pembayaran'])) {
			$query = $query->where('keterangan','LIKE','%'.$data['keterangan_pembayaran'].'%');
		}

		// Tanggal
		if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
			$query = $query->whereBetween('tanggal',[$data['tanggal_dari'],$data['tanggal_sampai']]);
		}

		return $query;
	}
}
