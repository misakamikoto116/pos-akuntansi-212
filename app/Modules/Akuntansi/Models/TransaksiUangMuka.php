<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransaksiUangMuka extends Model
{
    protected $table = "transaksi_uang_muka";
    use SoftDeletes;
    public $timestamps = true;
    protected $fillable = [
        'detail_informasi_pelanggan_id',
        'faktur_penjualan_id',
        'faktur_uang_muka_pelanggan_id',
        'pajak_satu_id',
        'pajak_dua_id',
        'total_pajak1',
        'total_pajak2',
        'jumlah',
    ];

    public function detailPelanggan()
	{
		return $this->belongsTo(DetailInformasiPelanggan::class,'detail_informasi_pelanggan_id');
    }
    
    public function fakturPenjualan()
    {
		return $this->belongsTo(FakturPenjualan::class,'faktur_penjualan_id');
    }

    public function fakturUangMukaPelanggan()
    {
        return $this->belongsTo(FakturPenjualan::class,'faktur_uang_muka_pelanggan_id');
    }

    public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
