<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Jasa extends Model
{
    protected $table = 'jasa';
    protected $dates = ['tanggal_jasa'];
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
	  	'kode',
		'tanggal_jasa',
		'harga_jual',
	];
}
