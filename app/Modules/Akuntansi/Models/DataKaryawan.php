<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataKaryawan extends Model
{
    protected $table = 'data_karyawan';
    public $timestamps = true;
    use SoftDeletes;

    protected $fillable = [
        'nik_karyawan',
        'nip_karyawan',
        'nama_karyawan',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'alamat_karyawan',
        'no_telp',
        'jabatan',
        'status',
  ];
}
