<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DetailInformasiPemasok extends Model
{
    protected $table = 'detail_informasi_pemasok';
	// protected $dates = ['tanggal'];
	protected $with = ['syaratPembayaran'];
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'informasi_pemasok_id',
		'syarat_pembayaran_id',
		'no_faktur',
		'tanggal',
		'taxable',
		'in_tax',
		'pajak',
		'saldo_awal',
		'status',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function informasiPemasok()
	{
		return $this->belongsTo(InformasiPemasok::class);
	}

	public function syaratPembayaran()
	{
		return $this->belongsTo(SyaratPembayaran::class,'syarat_pembayaran_id');
	}

	public function transaksiUangMukaPemasok()
	{
		return $this->hasMany(TransaksiUangMukaPemasok::class);
	}

	public function uangMukaSumPemasok()
	{
		return $this->transaksiUangMukaPemasok()->sum('jumlah');
	}
}
