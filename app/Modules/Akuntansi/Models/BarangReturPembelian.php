<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangReturPembelian extends Model
{
    protected $table = 'barang_retur_pembelian';
    use SoftDeletes;
    protected $fillable = [
    		'produk_id',
    		'barang_faktur_pembelian_id',
    		'retur_pembelian_id',
    		'item_deskripsi',
    		'satuan',
    		'jumlah',
    		'harga',
    		'gudang_id',
    		'kode_pajak_id',
    		'kode_pajak_2_id',
    		'sn',
    		'status',
    ];
    public function produk()
	{
		return $this->belongsTo(Produk::class,'produk_id');
	}
	public function gudang()
	{
		return $this->belongsTo(Gudang::class, 'gudang_id', 'id');
	}

    public function pajak()
    {
      return $this->belongsTo(KodePajak::class, 'kode_pajak_id');
    }

    public function pajak2()
    {
      return $this->belongsTo(KodePajak::class, 'kode_pajak_2_id');
    }
}
