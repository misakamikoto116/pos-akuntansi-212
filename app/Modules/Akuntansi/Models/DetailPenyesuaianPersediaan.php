<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailPenyesuaianPersediaan extends Model
{
    protected $table = 'detail_penyesuaian_persediaan';
    protected $appends = ['produk_detail'];
    public $timestamps = true;
    use SoftDeletes;

    protected $fillable = [
        'penyesuaian_persediaan_id',
        'produk_id',
        'keterangan_produk',
        'satuan_produk',
        'qty_produk',
        'qty_baru',
        'qty_diff',
        'nilai_skrg',
        'nilai_baru',
        'departemen',
        'gudang_id',
        'proyek',
        'serial_number',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function produkDetail()
    {
        return $this->hasMany(SaldoAwalBarang::class, 'produk_id', 'produk_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class);
    }

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id', 'id');
    }

    public function PenyesuaianPersediaan()
    {
        return $this->belongsTo(PenyesuaianPersediaan::class, 'penyesuaian_persediaan_id');
    }

    public function getProdukDetailAttribute()
    {
        return self::produkDetail()->orderBy('id', 'desc')->first();
    }
}
