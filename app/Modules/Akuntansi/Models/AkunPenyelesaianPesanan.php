<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AkunPenyelesaianPesanan extends Model
{
    use SoftDeletes;
    public $timestamps = true;

    protected $table = "akun_penyelesaian_pesanan";
    protected $fillable = [
        'penyelesaian_pesanan_id',
        'detail_akun_id',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function akun()
    {
    	return $this->belongsTo(Akun::class,'detail_akun_id');
    }
}
