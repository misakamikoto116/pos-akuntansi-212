<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;

class Gudang extends Model
{
    protected $table = 'gudang';
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
		'nama',
		'keterangan',
	  	'alamat',
	  	'penanggung_jawab',
	  	'created_by',
		'updated_by',
		'deleted_by',
	];

	public static function boot()
	{
		parent::boot();

		self::deleting(function ($gudang) {
			$layout = new LayoutHelper;
			if (!empty($gudang->produk)) {
				if ($gudang->produk->count() > 0) {
					$html = 'Gudang tidak dapat dihapus, karena masih memiliki Produk.';
					return $layout->batalkanProses($html);
				}
			}
			if (!empty($gudang->saldoAwalBarang)) {
				if ($gudang->saldoAwalBarang->count() > 0) {
					$html = 'Gudang tidak dapat dihapus, karena masih memiliki Detail Produk.';
					return $layout->batalkanProses($html);
				}
			}
		});
	}

	 public function produk()
	{
		return $this->hasOne(Produk::class);
	}
	public function produks()
	{
		return $this->hasMany(Produk::class);
	}
	 public function saldoAwalBarang()
	{
		return $this->hasMany(SaldoAwalBarang::class);
	}

	public function pemindahanBarang(){
		return $this->hasMany(PemindahanBarang::class, 'ke_gudang_id', 'dari_gudang_id');
	}
	
	public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
