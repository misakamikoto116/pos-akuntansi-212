<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangPenawaranPenjualan extends Model
{
    protected $table = 'barang_penawaran_penjualan';
	public $timestamps = true;
	protected $appends = ['harga_total'];
	use SoftDeletes;
	protected $fillable = [
	                'produk_id',
	                'penawaran_penjualan_id',
                	'item_deskripsi',
                	'jumlah',
                	'harga',
                	'terproses',
                	'ditutup',
                	'satuan',
                	'status',
                	'kode_pajak_id',
                	'kode_pajak_2_id',
                	'harga_modal',
	];

	public function penawaranPenjualan()
	{
		return $this->belongsTo(PenawaranPenjualan::class,'penawaran_penjualan_id');
	}

	public function produk()
	{
		return $this->belongsTo(Produk::class,'produk_id');
	}

	public function getHargaTotalAttribute()
	{
		$total 	= $this->getAttribute('harga') * $this->getAttribute('jumlah');
		$tax 	= $total * $this->getAttribute('pajak') / 100;
		$disc 	= $total * $this->getAttribute('diskon') / 100;

		$grandtotal = $total - $disc + $tax;

		return $grandtotal; 
	}

	public function barangPesananPenjualan()
	{
		return $this->hasOne(BarangPesananPenjualan::class);
	}
	public function gudang()
	{
		return $this->belongsTo(Gudang::class, 'gudang_id', 'id');
	}
}
