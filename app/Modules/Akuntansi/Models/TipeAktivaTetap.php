<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipeAktivaTetap extends Model
{
    protected $table = 'tipe_aktiva_tetap';
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
		'tipe_aktiva_tetap',
		'tipe_aktiva_tetap_pajak_id',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function tipeAktivaTetapPajak()
	{
		return $this->belongsTo(TipeAktivaTetapPajak::class);
	}

	public function daftarAktivaTetap()
	{
		return $this->hasOne(DaftarAktivaTetap::class);
	}
}
