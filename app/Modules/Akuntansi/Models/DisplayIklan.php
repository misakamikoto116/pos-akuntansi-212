<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DisplayIklan extends Model
{
    use SoftDeletes;
    protected $table   = 'display_iklan';
    public $timestamps = true;
    protected $fillable =  ['nama_iklan', 'image_path','deskripsi' ,'publish','urutan', 'publish', 'created_by'];

    public function user(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function getPublishAttribute($val)
    {
        return ($val ? 'Aktif' : 'Tidak Aktif');
    }

    public function ScopeFilterData($query, $request)
    {
        if ($request->has('search')) {
            $query->where('nama_iklan', 'like', '%'.$request->get('q').'%')
                ->orWhere('deskripsi', 'like', '%'.$request->get('q').'%' );
        }
        $query
            ->orderBy('id','DESC');
        return $query;
    }
}
