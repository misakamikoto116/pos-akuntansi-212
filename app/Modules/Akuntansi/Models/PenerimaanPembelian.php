<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class PenerimaanPembelian extends Model
{
    protected $table = 'penerimaan_pembelian';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'pemasok_id',
		'alamat_pengiriman',
		'form_no',
		'receipt_no',
		'receive_date',
		'ship_date',
		'fob',
		'ship_id',
        'keterangan',
        'tutup_buku',
        'tanggal_tutup_buku',
    ];
    
    public function barang()
    {
        return $this->hasMany(BarangPenerimaanPembelian::class);
    }

    public function pemasok()
    {
        return $this->belongsTo(InformasiPemasok::class,'pemasok_id','id');
    }

    public function scopeFilter($query, $data)
    {
    	// Search By Keyword
        if(!empty($data['search'])){
            $query = $query->where('form_no','like','%'.$data['search'].'%')
                ->orWhere('alamat_pengiriman','like','%'.$data['search'].'%')
                ->orWhere('keterangan','like','%'.$data['search'].'%')
				->orWhere('receipt_no','like','%'.$data['search'].'%');
		}
		
		// No Penawaran
		if(!empty($data['no_faktur'])){
			$query = $query->where('receipt_no', $data['no_faktur']);
		}

		// Keterangan
		if(!empty($data['keterangan_pembayaran'])){
			$query = $query->where('keterangan', $data['keterangan_pembayaran']);
		}

		// Pemasok Select
		if(!empty($data['pemasok_select'])){
			$query = $query->where('pemasok_id', $data['pemasok_select']);
		}

		// Tanggal
		if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
			$query = $query->whereBetween('receive_date',[$data['tanggal_dari'],$data['tanggal_sampai']]);
		}
		
		// if (!empty($data['search_pemasok'])) {
		// 	$query = $query->whereHas('pemasok', function($query) use ($data){
		// 			 $query->where('nama', 'like', '%'.$data['search_pemasok'].'%')
		// 			       ->orWhere('no_pemasok', 'like', '%'.$data['search_pemasok'].'%');
		// 		}
		// 	);
		// }
		
        return $query;
	}
	
	public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
