<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BebanFakturPembelian extends Model
{
    protected $table = 'beban_faktur_pembelian';
    public $timestamps = true;
    use SoftDeletes;
    protected $fillable = [
    		'akun_beban_id',
    		'faktur_pembelian_id',
    		'amount',
    		'alokasi_ke_barang',
    		'pemasok_id',
    ];

    public function fakturPembelian()
    {
        return $this->belongsTo(FakturPembelian::class,'faktur_pembelian_id');
    }

    public function akun()
    {
        return $this->belongsTo(Akun::class,'akun_beban_id');
    }
}
