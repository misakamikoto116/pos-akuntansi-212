<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Config extends Model
{
	protected $table = 'config';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'key',
		'value',
		'deskripsi',
	];
}
