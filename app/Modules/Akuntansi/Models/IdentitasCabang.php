<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IdentitasCabang extends Model
{
    protected $table = 'identitas_cabang';
    public $timestamps = true;
    use SoftDeletes;
    
    protected $fillable = [
        'identitas_id',
        'kode_cabang',
        'nama_cabang'
    ];

    public function identitas()
    {
        return $this->belongsTo(Identitas::class);
    }
}
