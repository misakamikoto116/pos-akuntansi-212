<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailJurnalUmum extends Model
{
    protected $table = 'detail_jurnal_umum';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		 		'akun_id',
		 		'jurnal_umum_id',
                'debit',
                'kredit',
                'memo',
                'subsidiary_ledger',
                'departement',
                'project',
                'created_by',
                'updated_by',
                'deleted_by',

	];
    public function jurnalUmum()
	{
		return $this->belongsTo(JurnalUmum::class);
	}
	public function akun()
	{
		return $this->belongsTo(Akun::class);
	}
}
