<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IdentitasPajak extends Model
{
    protected $table = 'identitas_pajak';
    public $timestamps = true;
    use SoftDeletes;
    
    protected $fillable = [
        'identitas_id',
        'no_seri_faktur_pajak',
        'npwp',
        'no_pengukuhan_pkp',
        'tanggal_pengukuhan_pkp',
        'kode_cabang_pajak',
        'jenis_usaha',
        'klu_spt',
    ];

    public function identitas()
    {
        return $this->belongsTo(Identitas::class);
    }
}
