<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailHistoryRekonsiliasi extends Pivot
{
    protected $table = 'detail_history_rekonsiliasi';
    public $timestamps = true;
    protected $dates = ['detail_tanggal_terakhir_rekonsil'];
    use SoftDeletes;

    protected $fillable = [
        'history_rekonsiliasi_id',
        'transaksi_id',
        'detail_tanggal_terakhir_rekonsil',
  	];
}
