<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailPemindahanBarang extends Model
{
	protected $table = 'detail_pemindahan_barang';
	public $timestamps = true;
	protected $fillable = [
		'produk_id',
        'keterangan',
		'jumlah',
		'satuan',
        'serial_number',
		'pemindahan_barang_id',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function PemindahanBarang(){
		return $this->belongsTo(PemindahanBarang::class,'pemindahan_barang_id');
	}
	
	public function produk()
	{
		return $this->belongsTo(Produk::class, 'produk_id');
	}
}