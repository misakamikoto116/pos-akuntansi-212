<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;
class PengirimanPenjualan extends Model
{
    protected $table = 'pengiriman_penjualan';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
        'pelanggan_id',
	  	'pelanggan_kirim_ke_id',
        'alamat_tagihan',
        'alamat_pengiriman',
    	'delivery_no',
    	'delivery_date',
    	'po_no',
        'ship_id',
        'keterangan',
        'cetak',
        'status_modul',
        'kasir_mesin_id',
		'created_by',
        'tutup_buku',
        'tanggal_tutup_buku',
	];

	public function barangPengirimanPenjualan()
	{
		return $this->hasMany(BarangPengirimanPenjualan::class);
    }

    public function barang()
	{
		return $this->hasMany(BarangPengirimanPenjualan::class);
    }
    
    public function pelanggan()
    {
        return $this->belongsTo(InformasiPelanggan::class, 'pelanggan_id','id');
    }

    public function getStatusFormattedAttribute()
    {
        $helper = new LayoutHelper;
        return $kondisi = ($this->getAttribute('status')) ? $helper->formLabel('success','Cetak') : $helper->formLabel('primary','Belum Cetak') ;
    }

    public function scopeFilter($query, $data)
    {

        if(!empty($data['search'])){
            $query = $query->where('delivery_no','like','%'.$data['search'].'%')
                ->orWhere('alamat_pengiriman','like','%'.$data['search'].'%')
                ->orWhere('alamat_tagihan','like','%'.$data['search'].'%')
				->orWhere('keterangan','like','%'.$data['search'].'%');
		}

        // No Faktur
        if(!empty($data['no_faktur'])){
            $query = $query->where('delivery_no', $data['no_faktur']);
        }

        // No Pelanggan
        if (!empty($data['no_pelanggan'])) {
            $query = $query->whereHas('pelanggan', function($query) use ($data) {
                     $query->where('no_pelanggan', 'like', '%'.$data['no_pelanggan'].'%');
            });
        }

        // Pelanggan Select
        if(!empty($data['pelanggan_select'])){
            $query = $query->where('pelanggan_id', $data['pelanggan_select']);
        }

		// Status Penjualan
        if (!empty($data['status_penjualan'])) {
            $query = $query->where('cetak','LIKE','%'.$data['status_penjualan'].'%');
        }else if ($data['status_penjualan'] == 0) {
                $query = $query->where('cetak','LIKE','%'.$data['status_penjualan'].'%');
        }
		
		// Tanggal
        if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
            $query = $query->whereBetween('delivery_date',[$data['tanggal_dari'],$data['tanggal_sampai']]);
        }
		
        return $query;
    }
    public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
            if($request->get('start-date') === $request->get('end-date')){
				return $query->whereDate('delivery_date', $request->get('start-date'));
			}else{
				return $query->whereBetween('delivery_date',[$request->get('start-date'), $request->get('end-date')]);
			}
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('delivery_date', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
