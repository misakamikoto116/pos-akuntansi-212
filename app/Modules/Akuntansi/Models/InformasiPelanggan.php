<?php

namespace App\Modules\Akuntansi\Models;

use Carbon\Carbon;
use Helpers\LayoutHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InformasiPelanggan extends Model
{
	protected $table = 'informasi_pelanggan';
	public $timestamps = true;
	
	use SoftDeletes;
	protected $fillable = [
		'nama','no_pelanggan','no_faktur_pelanggan','saldo_awal_pelanggan',
        'tanggal_pelanggan','alamat_pajak','alamat','kota','prop','kode_pos',
        'negara','telepon','personal_kontak','email','halaman_web','syarat_pembayaran_id',
        'batasan_piutang_uang','batasan_piutang_hari','mata_uang_id','pesan','pajak_satu_id',
        'pajak_dua_id','pajak_faktur','npwp','nppkp','tipe_pajak_id','tipe_pelanggan_id',
        'tingkatan_harga_jual','disk_default','catatan', 'nik', 'foto_pelanggan', 'foto_nik_pelanggan','created_by','updated_by','deleted_by',
	];

	public static function boot()
	{
		parent::boot();

		self::deleting(function ($info) {
			$layout = new LayoutHelper;
			if (
				$info->penawaranPenjualan->count() > 0 || 
				$info->pesananPenjualan->count() > 0 || 
				$info->pengirimanPenjualan->count() > 0 || 
				$info->fakturPenjualan()->where('catatan','!=','Saldo Awal')->count() > 0 || 
				$info->penerimaanPenjualan->count() > 0 ||
				$info->returPenjualan->count() > 0
			) {
				$html = 'Pelanggan ini telah memiliki transaksi.';
				return $layout->batalkanProses($html);				
			}
		});
	}

	public function detailInformasiPelanggan()
	{
		return $this->hasMany(DetailInformasiPelanggan::class);
	}

	public function penawaranPenjualan()
	{
		return $this->hasMany(PenawaranPenjualan::class,'pelanggan_id','id');
	}

	public function pesananPenjualan()
	{
		return $this->hasMany(PesananPenjualan::class,'pelanggan_id','id');
	}

	public function pengirimanPenjualan()
	{
		return $this->hasMany(PengirimanPenjualan::class,'pelanggan_id','id');
	}

	public function fakturPenjualan()
	{
		return $this->hasMany(FakturPenjualan::class,'pelanggan_id','id');
	}

	public function penerimaanPenjualan()
	{
		return $this->hasMany(PenerimaanPenjualan::class,'pelanggan_id','id');
	}

	public function getPenerimaanPenjualan(){
		$sumPenerimaan	=	$this->hasMany(FakturPenjualan::class,'pelanggan_id','id')
							->whereHas('invoice')->first();
		if($sumPenerimaan === null){
			return 0;
		}
		return $sumPenerimaan->invoice->sum('payment_amount');
	}

	public function getNamaFormattedAttribute()
	{
		return $this->getAttribute('no_pelanggan').' - '.$this->getAttribute('nama');
	}

	public function sumSaldoPelanggan()
	{
		$sumPenerimaan	= $this->getPenerimaanPenjualan();
		$sumFaktur		= $this->fakturPenjualan()->sum('total');
		$sumSaldo		= $sumFaktur - $sumPenerimaan;
		return $sumSaldo;
	}

	public function returPenjualan()
	{
		return $this->hasMany(ReturPenjualan::class,'pelanggan_id','id');
	}

	public function kontakPelanggan()
	{
		return $this->hasMany(KontakPelanggan::class,'pelanggan_id','id');
	}

	public function tipePelanggan()
	{
		return $this->belongsTo(TipePelanggan::class,'tipe_pelanggan_id');
	}

	public function syaratPembayaran()
	{
		return $this->belongsTo(SyaratPembayaran::class,'syarat_pembayaran_id');
	}

	public function mataUang()
	{
		return $this->belongsTo(MataUang::class,'mata_uang_id');
	}

	public function getSumPelangganAttribute() // awalnya getSumAttribute
	{
		return $this->detailInformasiPelanggan->sum('saldo_awal');
	}

	public function scopeReportInvoiceFilter($query, $request)
    {
    	if($request->get('start-date')){
			$query = $query->whereHas('fakturPenjualan', function ($query) use ($request) {
				$tgl_satu = date('Y-m')."-01";
				$query->whereBetween('invoice_date', [$tgl_satu." 00:00:00", $request->get('start-date')." 23:59:59"]);
			});
		}
		return $query;
    }

    public function scopeReportRincianFilter($query, $request)
    {
    	if($request->get('start-date')){
			$query = $query->whereHas('fakturPenjualan', function ($query) use ($request) {
				$query->whereBetween('invoice_date', ['0001-01-01  00:00:00', $request->get('start-date')." 23:59:59"]);
			});
		}
		return $query;
    }

    public function scopeReportPenagihanPelanggan($query, $request)
    {
    	$date = $this->formatingMonthYear($request);
    	$bulan_awal 	= $date['bulan_awal']->format('Y-m-d');
    	$bulan_akhir	= $date['bulan_akhir']->format('Y-m-d');
    	
    	if ($request->get('start-date-rincian') && $request->get('end-date-rincian')) {
    		$query = $query->whereHas('fakturPenjualan', function ($query) use ($bulan_awal, $bulan_akhir)
    		{
    			$query->whereBetween('invoice_date',[$bulan_awal, $bulan_akhir])
    			      ->whereHas('invoice', function ($query1) use ($bulan_awal, $bulan_akhir)
    			      {
    			      	$query1->whereBetween('tanggal',[$bulan_awal, $bulan_akhir]);
    			      });
    		});
    	}
    	return $query;
    }

    public function formatingMonthYear($request)
    {
    	$dateMonthArrayStart 	= explode('-', $request->get('start-date-rincian'));
        $dateMonthArrayEnd 		= explode('-', $request->get('end-date-rincian'));

        $month_start    = $dateMonthArrayStart[0];
        $year_start     = $dateMonthArrayStart[1];
        
        $month_end      = $dateMonthArrayEnd[0];
        $year_end       = $dateMonthArrayEnd[1];

         return [
            'bulan_awal'        => Carbon::createFromDate($year_start, $month_start, 1),
            'bulan_akhir'       => Carbon::createFromDate($year_end, $month_end, 1),
         ];
    }
    public function scopeFilter($query, $request)
    {
        if($request->has('s')){
            $query->where('nama','like','%'.$request->s.'%');
        }
        if($request->has('no_pelanggan'))
            $query->where('no_pelanggan', 'like', '%'.$request->no_pelanggan.'%');
        return $query;
    }

    public function scopeFilterPelanggan($query, $data)
    {
    	// Keyword
		if(!empty($data['search'])){
            $query = $query->where('no_pelanggan','like','%'.$data['search'].'%')
				           ->orWhere('nama','like','%'.$data['search'].'%')
				           ->orWhere('telepon','like','%'.$data['search'].'%');
		}

    	// No Pelanggan
		if (!empty($data['no_pelanggan'])) {
			$query = $query->where('no_pelanggan','LIKE','%'.$data['no_pelanggan'].'%');
		}

		// Nama Pelanggan
		if (!empty($data['nama_pelanggan'])) {
			$query = $query->where('nama','LIKE','%'.$data['nama_pelanggan'].'%');
		}

		// Select Tipe Pelanggan
		if (!empty($data['tipe_pelanggan'])) {
			$query = $query->whereHas('tipePelanggan', function ($query) use ($data) {
						$query->where('tipe_pelanggan_id', $data['tipe_pelanggan']);
			});
		}

		// Select Tipe Pelanggan
		if (!empty($data['mata_uang'])) {
			$query = $query->whereHas('mataUang', function ($query) use ($data) {
						$query->where('mata_uang_id', $data['mata_uang']);
			});
		}

		return $query;
    }

    public function scopeFilterAvailablePelanggan($query, $data)
    {
    	if (array_key_exists('pelanggan_transaksi', $data->all())) {
	    	$query = $query->orWhereHas('fakturPenjualan')->orWhereHas('penerimaanPenjualan')->orWhereHas('returPenjualan');
    	}

    	return $query;
    }
}
