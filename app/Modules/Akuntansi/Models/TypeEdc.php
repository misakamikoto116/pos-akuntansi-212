<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;

class TypeEdc extends Model
{
    protected $table = 'type_edc';
    protected $fillable = ['nama','akun_id'];

    public function akun()
    {
    	return $this->belongsTo(Akun::class,'akun_id');
    }

}
