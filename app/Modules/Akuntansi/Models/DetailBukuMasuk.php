<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DetailBukuMasuk extends Model
{
    protected $table = 'detail_buku_masuk';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'buku_masuk_id',
		'nominal',
		'akun_id',
		'catatan',
		'created_by',
		'updated_by',
		'deleted_by',

	];
    public function penerimaan()
	{
		return $this->belongsTo(Penerimaan::class,'buku_masuk_id');
	}
	public function akun()
	{
		return $this->belongsTo(Akun::class);
	}
}
