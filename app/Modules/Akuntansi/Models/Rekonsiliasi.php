<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rekonsiliasi extends Model
{
    protected $table = 'rekonsiliasi';
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
	  	'akun_id',
		'saldo_rekening_koran',
		'kalkulasi_saldo',
		'selisih_saldo',
		'tanggal_rekonsil',
	];

	public function akun()
	{
		return $this->belongsTo(Akun::class);
	}
}
