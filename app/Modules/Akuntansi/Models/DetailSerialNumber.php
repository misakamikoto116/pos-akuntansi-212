<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;
class DetailSerialNumber extends Model
{
    protected $table = 'detail_serial_number';
    public $timestamps = true;
    
    protected $fillable = [
        'item',
        'item_deskripsi',
        'jumlah',
        'satuan',
        'serial_number',
        'serial_number_id',
        'created_by',
        'updated_by',
        'deleted_by',
  ];

  public function serialNumber()
  {
      return $this->belongsTo(SerialNumber::class,'serial_number_id');
  }

  public function produk()
  {
      return $this->belongsTo(Produk::class, 'produk_id');
  }
}
