<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReturPembelian extends Model
{
    protected $table = 'retur_pembelian';
    use SoftDeletes;
    protected $fillable = [
        "pemasok_id",
        "faktur_pembelian_id",
        "alamat_asal",
        "invoice_no",
        "taxable",
        "in_tax",
        "return_no",
        "tanggal",
        "no_fiscal",
        "tgl_fiscal",
        "nilai_tukar",
        "nilai_tukar_pajak",
        "keterangan",
        "total",
        'tutup_buku',
        'tanggal_tutup_buku',
    ];

    public function pemasok()
    {
    	return $this->belongsTo(InformasiPemasok::class,'pemasok_id');
    }

    public function fakturPembelian()
    {
    	return $this->belongsTo(FakturPembelian::class,'faktur_pembelian_id');
    }

    public function barang()
	{
		return $this->hasMany(BarangReturPembelian::class);
	}

    public function getTotalFormattedAttribute()
	{
		// return $this->getAttribute('total') + $this->getAttribute('ongkir');
    }
    
    public function scopeFilter($query, $data)
    {
        // Search By Keyword
        if(!empty($data['search'])){
            $query = $query->where('return_no','like','%'.$data['search'].'%')
				->orWhere('keterangan','like','%'.$data['search'].'%');
		}

        // No Retur
        if(!empty($data['no_faktur'])){
            $query = $query->where('return_no', $data['no_faktur']);
        }

        // Pemasok Select
        if(!empty($data['pemasok_select'])){
            $query = $query->where('pemasok_id', $data['pemasok_select']);
        }
		
		// Tanggal
        if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
            $query = $query->whereBetween('tanggal',[$data['tanggal_dari'],$data['tanggal_sampai']]);
        }

		// if (!empty($data['search_pemasok'])) {
		// 	$query = $query->whereHas('pemasok', function($query) use ($data){
		// 			 $query->where('nama', 'like', '%'.$data['search_pemasok'].'%')
		// 			       ->orWhere('no_pemasok', 'like', '%'.$data['search_pemasok'].'%');
		// 		}
		// 	);
		// }
		
        return $query;
    }

    public function scopeReportInvoiceFilter($query, $request)
    {
        if($request->get('start-date')){
                $tgl_satu = date('Y-m')."-01";
                $query->whereBetween('tanggal', [$tgl_satu." 00:00:00", $request->get('start-date')." 23:59:59"]);
        }
        return $query;
    }

    public function scopeReportRincianFilter($query, $request)
    {
        if($request->get('start-date')){
                $query->whereBetween('tanggal', ['0001-01-01', $request->get('start-date')]);
        }
        return $query;
    }

    public function scopeReportPenerimaanFilter($query, $request)
    {
        if($request->get('start-date-rincian') && $request->get('end-date-rincian')){
            $query->whereBetween('tanggal',[$request->get('start-date-rincian')." 00:00:00", $request->get('end-date-rincian')." 23:59:59"]);
        }else{
            $query = $query->whereBetween('tanggal', ['0001-01-01', date('Y-m-d', strtotime('+1 days'))]);
        }
        return $query;
    }
    
    public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
