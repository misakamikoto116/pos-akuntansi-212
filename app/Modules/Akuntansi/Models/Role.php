<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role as RoleModel;

class Role extends RoleModel
{
    protected $table = 'roles';
    public $timestamps = true;
    protected $fillable = ['name', 'guard_name'];
    protected $dates = ['created_at', 'updated_at'];

    public function scopeFilter($query, $data)
    {
        return $query->where('name','like','%'.$data['search'].'%');
    }
   
}
