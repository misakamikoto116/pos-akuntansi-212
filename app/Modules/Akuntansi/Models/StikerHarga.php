<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;

class StikerHarga extends Model
{
    protected $table = 'stiker_harga';

    protected $fillable = [
        'aktif',
        'created_at',
        'updated_at',
        'nama_stiker'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public $timestamps = true;
}
