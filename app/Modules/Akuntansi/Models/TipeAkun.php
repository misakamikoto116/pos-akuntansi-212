<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
class TipeAkun extends Model
{
    protected $table = 'tipe_akun';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'title',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public static function boot()
	{
		parent::boot();

		self::deleting(function ($type) {
			if (!empty($type->akun)) {
				if ($type->akun->count() > 0) {
					if ($type->akun->count() > 0) {
						$html = 'Tipe akun tidak dapat dihapus karena sudah ada akun yang menggunakan tipe akun ini.';
					}

					Session::flash('flash_notification', [
						'level'   => 'danger',
						'message' => $html,
					]);

					return false;
				}
			}
		});
	}

    public function akun()
	{
		return $this->hasOne(Akun::class);
	}

	public function getUserCreatedAttribute()
	{
		$user_id	= $this->getAttribute('created_by');
		$user 		= User::find($user_id);
		$created_by = '-';

		if (!empty($user)) {
			$created_by = $user->name;	
		}

		return ucwords($created_by);
	}

	public function getUserUpdatedAttribute()
	{
		$user_id	= $this->getAttribute('updated_by');
		$user 		= User::find($user_id);
		$updated_by = '-';

		if (!empty($user)) {
			$updated_by = $user->name;	
		}

		return ucwords($updated_by);
	}
}
