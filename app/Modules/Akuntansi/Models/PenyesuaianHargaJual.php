<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PenyesuaianHargaJual extends Model
{
    protected $table = 'produk';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'request_no',
		'request_date',
		'status',
    	'catatan',
	];
}
