<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;

class TipeAktivaTetapPajak extends Model
{
    protected $table = 'tipe_aktiva_tetap_pajak';
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
		'tipe_aktiva_tetap_pajak',
		'metode_penyusutan_id',
		'estimasi_umur',
		'tarif_penyusutan',
		'created_by',
		'updated_by',
		'deleted_by',
	];
	public function metodePenyusutan()
	{
		return $this->belongsTo(MetodePenyusutan::class);
	}

	public function tipeAktivaTetap()
	{
		return $this->hasMany(TipeAktivaTetap::class);
	}

	public static function boot()
	{
		parent::boot();

		self::deleting(function ($type) {
			if ($type->tipeAktivaTetap->count() > 0) {
				if ($type->tipeAktivaTetap->count() > 0) {
					$html = 'Tipe aktiva tetap pajak tidak dapat dihapus, karena sudah ada tipe aktiva tetap yang menggunakan tipe aktiva tetap pajak ini.';
				}

				Session::flash('flash_notification', [
					'level'   => 'danger',
					'message' => $html,
				]);

				return false;
			}
		});
	}
}
