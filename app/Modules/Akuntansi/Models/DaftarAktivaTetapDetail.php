<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DaftarAktivaTetapDetail extends Model
{
    protected $table = 'daftar_aktiva_tetap_detail';
    public $timestamps = true;
    use SoftDeletes;
    
	protected $fillable = [
        'daftar_aktiva_tetap_id',
        'nama_beban',
        'nama_akumulasi',
        'periode',
        'tgl_jurnal',
        'keterangan',
        'harga_perolehan',
        'aktiva_dihentikan',
        'akumulasi_penyusutan',
        'nilai_buku_asuransi',
        'nilai_sisa',
        'nilai_penyusutan',
        'created_by',
        'updated_by',
        'deleted_by',
	];

    public function daftarAktivaTetap(){
        return $this->belongsTo(DaftarAktivaTetap::class, 'daftar_aktiva_tetap_id');
    }
}
