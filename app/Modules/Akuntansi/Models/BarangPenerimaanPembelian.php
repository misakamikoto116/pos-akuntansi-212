<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangPenerimaanPembelian extends Model
{
    protected $table = "barang_penerimaan_pembelian";
    public $timestamps = true;
    use SoftDeletes;
    protected $fillable = [
    		'penerimaan_pembelian_id',
    		'produk_id',
    		'barang_pesanan_pembelian_id',
    		'jumlah',
    		'satuan',
    		'gudang_id',
    		'harga_modal',
    		'sn',
	];
	protected $with = ['barangPesananPembelian'];
    public function produk()
	{
		return $this->belongsTo(Produk::class,'produk_id');
	}

	public function penerimaanPembelian()
	{
		return $this->belongsTo(PenerimaanPembelian::class,'penerimaan_pembelian_id');
	}

    public function barangPesananPembelian()
    {
        return $this->belongsTo(BarangPesananPembelian::class,'barang_pesanan_pembelian_id');
	}

	public function barangFakturPembelian()
	{
		return $this->hasMany(BarangFakturPembelian::class);
	}

	public function getUpdatedQtyAttribute()
    {
    	$qty_faktur 		= 0;
    	$qty_penerimaan		= $this->getAttribute('jumlah');
    	if ($this->barangFakturPembelian) {
    		$qty_faktur 		= $this->barangFakturPembelian->sum('jumlah');
    	}
    	return $qty_penerimaan - $qty_faktur;
    }
	
	public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
	public function gudang()
	{
		return $this->belongsTo(Gudang::class, 'gudang_id', 'id');
	}
}
