<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KontakPelanggan extends Model
{
    protected $table = 'kontak_pelanggan';
    use SoftDeletes;
    public $timestamps = true;
    protected $fillable = [
    		'pelanggan_id',
    		'pemasok_id',
    		'nama',
    		'nama_depan',
    		'jabatan',
    		'telepon_kontak',
    ];

    public function pelanggan()
    {
    	return $this->belongsTo(InformasiPelanggan::class,'pelanggan_id');
    }

    public function pemasok()
    {
    	return $this->belongsTo(InformasiPemasok::class,'pemasok_id');
    }
}
