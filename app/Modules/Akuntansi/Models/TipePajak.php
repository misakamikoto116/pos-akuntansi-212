<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
class TipePajak extends Model
{
    protected $table = 'tipe_pajak';
	protected $fillable = [
		'nama',
		'kode',
	];
}