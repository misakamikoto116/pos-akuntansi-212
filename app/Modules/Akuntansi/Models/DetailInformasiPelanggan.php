<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DetailInformasiPelanggan extends Model
{
    protected $table = 'detail_informasi_pelanggan';
    // protected $dates = ['tanggal'];
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'informasi_pelanggan_id',
		'syarat_pembayaran_id',
		'no_faktur',
		'tanggal',
		'taxable',
		'in_tax',
		'saldo_awal',
		'status',
		'no_pesanan',
		'pajak',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function informasiPelanggan()
	{
		return $this->belongsTo(InformasiPelanggan::class);
	}

	public function syaratPembayaran()
	{
		return $this->belongsTo(SyaratPembayaran::class,'syarat_pembayaran_id');
	}

	public function getInTaxTextedAttribute()
	{
		if($this->getAttribute('in_tax') == 0){
			return "Tidak";
		}else{
			return "Ya";			
		}
	}

	public function transaksiUangMuka()
	{
		return $this->hasMany(TransaksiUangMuka::class);
	}

	public function uangMukaSum()
	{
		return $this->transaksiUangMuka()->sum('jumlah');
	}
}
