<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistoryRekonsiliasi extends Model
{
    protected $table = 'history_rekonsiliasi';
    public $timestamps = true;
    use SoftDeletes;

    protected $fillable = [
        'akun_id',
        'saldo_rekening_koran',
        'kalkulasi_saldo',
        'selisih_saldo',
        'tanggal_terakhir_rekonsil',
  	];

  	public function transaksi()
  	{
  		return $this->belongsToMany(Transaksi::class,'detail_history_rekonsiliasi')->using(DetailHistoryRekonsiliasi::class);
  	}
}
