<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangPengirimanPenjualan extends Model
{
    protected $table = 'barang_pengiriman_penjualan';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
	                'produk_id',
	                'pengiriman_penjualan_id',
                    'barang_pesanan_penjualan_id',
                	'item_deskripsi',
                	'jumlah',
                	'item_unit',
                	'gudang_id',
                    'qty_used',
                	'harga_modal',
	];

    public function pengirimanPenjualan()
    {
        return $this->belongsTo(PengirimanPenjualan::class,'pengiriman_penjualan_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class,'produk_id');
    }

    public function barangPesananPenjualan()
    {
        return $this->belongsTo(BarangPesananPenjualan::class,'barang_pesanan_penjualan_id');
    }

    public function barangFakturPenjualan()
    {
        return $this->hasOne(BarangFakturPenjualan::class);
    }

    public function gudang()
	{
		return $this->belongsTo(Gudang::class, 'gudang_id', 'id');
	}

    public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
