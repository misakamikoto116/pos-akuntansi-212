<?php

namespace App\Modules\Akuntansi\Models;

use App\Modules\Pos\Models\KasirMesin;
use App\Modules\Pos\Models\RiwayatPembayaran;
use Helpers\LayoutHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class FakturPenjualan extends Model
{
    protected $table = 'faktur_penjualan';
    public $timestamps = true;
	use SoftDeletes;
	protected $appends = ['created_at_indo','all_amount','owing','sum_payment','nilai_faktur','invoice_date_indo','updated_uang_muka'];
	
	protected $fillable = [
	  	'no_faktur',
	  	'pelanggan_id',
	  	'pelanggan_kirim_ke_id',
    	'alamat_tagihan',
    	'alamat_pengiriman',
    	'taxable',
    	'in_tax',
		'diskon',
		'jumlah_diskon_faktur',
		'po_no',
		'invoice_date',
		'fob',
		'status',
		'status_lunas',
		'term_id',
		'ship_id',
		'ship_date',
		'no_fp_std',
		'no_fp_std2',
		'no_fp_std_date',
    	'ongkir',
    	'total',
    	'keterangan',
    	'catatan',
		'akun_piutang_id',
		'akun_ongkir_id',
    	'uang_muka',
		'akun_dp_id',
		'status_modul',
		'kasir_mesin_id',
		'created_by',
		'tutup_buku',
        'tanggal_tutup_buku',
	];

	public function user()
	{
		return $this->belongsTo(User::class, 'created_by');
	}

	public function pelanggan()
	{
		return $this->belongsTo(InformasiPelanggan::class, 'pelanggan_id','id');
	}

	public function transaksi()
	{
		return $this->morphMany(Transaksi::class, 'item');
	}

	public function uangMuka()
    {
		return $this->hasMany(TransaksiUangMuka::class, 'faktur_penjualan_id','id');        
    }

    public function transaksiUangMuka()
    {
    	return $this->hasMany(TransaksiUangMuka::class);
    }

	public function barang()
	{
		return $this->hasMany(BarangFakturPenjualan::class, 'faktur_penjualan_id','id');
	}

	public function invoice()
	{
		return $this->hasMany(InvoicePenerimaanPenjualan::class, 'faktur_id','id');
	}

	public function akunDp()
	{
		return $this->belongsTo(Akun::class,'akun_dp_id');
	}

	public function syaratPembayaran()
	{
		return $this->belongsTo(SyaratPembayaran::class,'term_id');
	}

	public function returPenjualan()
	{
		return $this->hasMany(ReturPenjualan::class);
	}

	public function kasirMesin()
	{
		return $this->belongsTo(KasirMesin::class,'kasir_mesin_id');
	}

	public function riwayatPembayaran()
	{
		return $this->hasOne(RiwayatPembayaran::class);
	}

	public function scopeAvailableOnly($query)
	{
		return $query->where('status',0);
	}

	public function getCreatedAtIndoAttribute()
	{
		$date = ($this->getAttribute('created_at')) ? date('d-m-Y', strtotime($this->getAttribute('created_at'))) : "-" ;
		return $date;
	}

	public function getInvoiceDateIndoAttribute()
	{
		$date = ($this->getAttribute('invoice_date')) ? date('d F Y', strtotime($this->getAttribute('invoice_date'))) : "-" ;
		return $date;
	}

	public function getSumPaymentAttribute(){
		$id = $this->getAttribute('id');
        $data = InvoicePenerimaanPenjualan::where('faktur_id', $id)->sum('payment_amount');
        return $data = ($data) ? $data : 0 ;
    }

	public function getAllAmountAttribute()
	{
		$goods = BarangFakturPenjualan::where('faktur_penjualan_id', $this->getAttribute('id'))->get();
		$total = 0;
		foreach($goods as $barang){
			$total += $barang->harga_total;
		}
		return $total;
	}

	public function getOwingAttribute(){
		$id = $this->getAttribute('id');
		$invoices = InvoicePenerimaanPenjualan::where('faktur_id', $id);
		$bayar = $invoices->sum('payment_amount') + $invoices->sum('diskon');
		$transaksiUangMuka = TransaksiUangMuka::where('faktur_penjualan_id', $id)->get();
		$uang_muka_dengan_pajak = $transaksiUangMuka->sum('jumlah') + $transaksiUangMuka->sum('total_pajak1') + $transaksiUangMuka->sum('total_pajak2');
		$total_barang = $this->getAttribute('total');

		return $total_barang - ($bayar + $uang_muka_dengan_pajak);	
	}

	public function getFakturKurangUangMukaAttribute()
	{
		$id = $this->getAttribute('id');
		// $transaksiUangMuka = TransaksiUangMuka::where('faktur_penjualan_id', $id)->get();
		$transaksiUangMuka = $this->transaksiUangMuka;
		$uang_muka_dengan_pajak = $transaksiUangMuka->sum('jumlah') + $transaksiUangMuka->sum('total_pajak1') + $transaksiUangMuka->sum('total_pajak2');
		$total_barang = $this->getAttribute('total');

		return $total_barang - $uang_muka_dengan_pajak;
	}

	public function getOwingAfterReturAttribute()
	{
		$id 		= $this->getAttribute('id');
		$retur 		= ReturPenjualan::where('faktur_penjualan_id',$id)->sum('total');
		return $this->fakturKurangUangMuka - ($this->sum_payment + $retur);
	}

	public function getUpdatedUangMukaAttribute()
	{
		$id 				= $this->getAttribute('id');
		$transaksiUangMuka 	= TransaksiUangMuka::where('faktur_uang_muka_pelanggan_id',$id)->sum('jumlah');
		$updated_uang_muka	= $this->uang_muka_with_out_tax - $transaksiUangMuka;

		return $updated_uang_muka; 
	}

	public function getNilaiFakturAttribute(){
		return $this->getAttribute('total');
	}

	public function getStatusFormattedAttribute()
	{
		$helper = new LayoutHelper;
		return $kondisi = ($this->getAttribute('status')) ? $helper->formLabel('success','Cetak') : $helper->formLabel('primary','Belum Cetak') ;
	}

	public function getStatusPenjualanPosAttribute()
	{
		$html = '';

		if ($this->invoice->isNotEmpty()) {
			$payment = $this->invoice->map(function ($item)   
			{

				return $item->penerimaanPenjualan->paymentHistory['status_payment'];
			
			});

			$html = "<label class='label label-success'>". array_first(array_flatten($payment)) ."</label>";

		}else {

			$html = "<label class='label label-warning'>Hutang</label>";

		}

		return $html;
	}

	public function getStatusPenjualanPosNoLabelAttribute()
	{
		$html = '';

		if ($this->invoice->isNotEmpty()) {
			$payment = $this->invoice->map(function ($item)   
			{

				return $item->penerimaanPenjualan->paymentHistory['status_payment'];
			
			});

			$html = array_first(array_flatten($payment));

		}else {

			$html = "Hutang";

		}

		return $html;
	}

	public function fakturPenjualan()
    {
    	return $this->hasOne(ReturPenjualan::class);
	}

	public function getInTaxTextedAttribute()
	{
		if($this->getAttribute('in_tax') == 0 || $this->getAttribute('in_tax') == null){
			return "Tidak";
		}else{
			return "Ya";			
		}
	}

	public function getUangMukaWithOutTaxAttribute()
	{
		return $this->barang()->where('faktur_penjualan_id',$this->getAttribute('id'))->pluck('unit_price')->first();
	}

	public function getSumHargaTotalAttribute()
	{
		return $this->barang->sum('harga_total');
	}

    public function scopeFilter($query, $data)
    {
    	// Search By Keyword
        if(!empty($data['search'])){
            $query = $query->where('no_faktur','like','%'.$data['search'].'%')
                ->orWhere('po_no','like','%'.$data['search'].'%')
				->orWhere('alamat_tagihan','like','%'.$data['search'].'%')
				->orWhere('alamat_pengiriman','like','%'.$data['search'].'%')
				->orWhere('catatan','like','%'.$data['search'].'%')
				->orWhere('keterangan','like','%'.$data['search'].'%');
		}

		// No Faktur
        if(!empty($data['no_faktur'])){
            $query = $query->where('no_faktur', $data['no_faktur']);
        }

		// No Pelanggan
        if (!empty($data['no_pelanggan'])) {
            $query = $query->whereHas('pelanggan', function($query) use ($data) {
                     $query->where('no_pelanggan', 'like', '%'.$data['no_pelanggan'].'%');
            });
        }

        // Pelanggan Select
		if(!empty($data['pelanggan_select'])){
			$query = $query->where('pelanggan_id', $data['pelanggan_select']);
		}
		
		// Tanggal
		if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
			$query = $query->whereBetween('invoice_date',[$data['tanggal_dari'], $data['tanggal_sampai']]);
		}

		// Tanggal rekap
		if (!empty($data['tanggal_dari_rekap']) || !empty($data['tanggal_sampai_rekap'])) {
			$query = $query->whereBetween('invoice_date',[$data['tanggal_dari_rekap']. " 00:00:00", $data['tanggal_sampai_rekap']. " 23:59:59"]);
		}

		if (!empty($data['search_pelanggan'])) {
            $query = $query->whereHas('pelanggan', function($query) use ($data){
                     $query->where('nama', 'like', '%'.$data['search_pelanggan'].'%')
                           ->orWhere('no_pelanggan', 'like', '%'.$data['search_pelanggan'].'%');
                }
            );
        }
		
        return $query;
    }

    public function scopeReportInvoiceFilter($query, $request)
    {
    	if($request->get('start-date')){
    			$tgl_satu = date('Y-m')."-01";
				$query->whereBetween('invoice_date', [$tgl_satu." 00:00:00", $request->get('start-date')." 23:59:59"]);
		}
		return $query;
	}

	public function scopeReportRincianFilter($query, $request)
    {
    	if($request->get('start-date')){
				$query->whereBetween('invoice_date', ['0001-01-01 00:00:00', $request->get('start-date')." 23:59:59"]);
		}
		return $query;
	}
	
	public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			if($request->get('start-date') === $request->get('end-date')){
				return $query->whereDate('invoice_date', $request->get('start-date'));
			}else{
				return $query->whereBetween('invoice_date',[$request->get('start-date'), $request->get('end-date')]);
			}
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('invoice_date', [$tgl_satu, date('Y-m-t')]);
		}
	}

	public function scopeReportPenerimaanFilter($query, $request)
    {
        if($request->get('start-date-rincian') && $request->get('end-date-rincian')){
            $query->whereBetween('invoice_date',[$request->get('start-date-rincian')." 00:00:00", $request->get('end-date-rincian')." 23:59:59"]);
        }else{
            $query = $query->whereBetween('invoice_date', ['0001-01-01', date('Y-m-d', strtotime('+1 days'))]);
        }
        return $query;
    }
}
