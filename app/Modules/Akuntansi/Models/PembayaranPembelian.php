<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;

class PembayaranPembelian extends Model
{
    protected $table = "pembayaran_pembelian";
    protected $fillable = [
        "pemasok_id",
        "akun_bank_id",
        "rate",
        "memo",
        "form_no",
        "payment_date",
        "void_cheque",
        "fiscal_payment",
        "cheque_amount",
        "cheque_no",
        "cheque_date",
        'tutup_buku',
        'tanggal_tutup_buku',
    ];

    public function akun()
    {
        return $this->belongsTo(Akun::class,'akun_bank_id');
    }

    public function pemasok()
    {
		return $this->belongsTo(InformasiPemasok::class, 'pemasok_id','id');
    }

    public function invoice()
    {
        return $this->hasMany(InvoicePembayaranPembelian::class);
    }

    public function scopeFilter($query, $data)
    {
        // Search By Keyword
        if(!empty($data['search'])){
            $query = $query->where('memo','like','%'.$data['search'].'%');
		}

        // No Penawaran
        if(!empty($data['no_cek'])){
            $query = $query->where('cheque_no', $data['no_cek']);
        }

        // Pemasok Select
        if(!empty($data['pemasok_select'])){
            $query = $query->where('pemasok_id', $data['pemasok_select']);
        }
		
		// Tanggal
        if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
            $query = $query->whereBetween('payment_date',[$data['tanggal_dari'],$data['tanggal_sampai']]);
        }

		// if (!empty($data['search_pemasok'])) {
		// 	$query = $query->whereHas('pemasok', function($query) use ($data){
		// 			 $query->where('nama', 'like', '%'.$data['search_pemasok'].'%')
		// 			       ->orWhere('no_pemasok', 'like', '%'.$data['search_pemasok'].'%');
		// 		}
		// 	);
		// }
		
        return $query;
    }

    public function scopeReportPenerimaanFilter($query, $request)
    {
        if($request->get('start-date-rincian') && $request->get('end-date-rincian')){
            $query->whereBetween('payment_date',[$request->get('start-date-rincian')." 00:00:00", $request->get('end-date-rincian')." 23:59:59"]);
        }else{
            $query = $query->whereBetween('payment_date', ['0001-01-01', date('Y-m-d', strtotime('+1 days'))]);
        }
        return $query;
    }
}
