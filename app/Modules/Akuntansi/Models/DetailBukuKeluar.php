<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DetailBukuKeluar extends Model
{
    protected $table = 'detail_buku_keluar';
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'buku_keluar_id',
		'nominal',
		'akun_id',
		'catatan',
		'created_by',
		'updated_by',
		'deleted_by',

	];
    public function pembayaran()
	{
		return $this->belongsTo(Pembayaran::class,'buku_keluar_id');
	}
	public function akun()
	{
		return $this->belongsTo(Akun::class);
	}
}
