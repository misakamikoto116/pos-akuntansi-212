<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Identitas extends Model
{
    protected $table = 'identitas';
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
		'nama_perusahaan',
		'alamat',
		'kepala_toko',
	  	'tanggal_mulai',
		'mata_uang_id',
		'kode_pos',
		'no_telepon',
	  	'negara',
	  	'footer_struk',
	  	'logo_perusahaan',
	];

	public function identitasPajak(){
		return $this->hasOne(IdentitasPajak::class);
	}

	public function identitasCabang(){
		return $this->hasMany(IdentitasCabang::class);
	}

	public function mataUang()
	{
		return $this->belongsTo(MataUang::class);
	}
}
