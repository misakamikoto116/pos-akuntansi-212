<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;

class TipePelanggan extends Model
{
    protected $table = 'tipe_pelanggan';
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
		'nama',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public static function boot()
	{
		parent::boot();
		
		self::deleting(function ($tipe) {
			$layout = new LayoutHelper;

			if ($tipe->pelanggan->count() > 0) {
				$html = 'Tipe Pelanggan tidak dapat dihapus, karena masih memiliki Informasi Pelanggan. ';
				return $layout->batalkanProses($html);
			}

		});
	}

	public function pelanggan()
	{
		return $this->hasMany(InformasiPelanggan::class,'tipe_pelanggan_id','id');
	}

	public function getUserCreatedAttribute()
	{
		$user_id	= $this->getAttribute('created_by');
		$user 		= User::find($user_id);
		$created_by = '-';

		if (!empty($user)) {
			$created_by = $user->name;	
		}

		return ucwords($created_by);
	}

	public function getUserUpdatedAttribute()
	{
		$user_id	= $this->getAttribute('updated_by');
		$user 		= User::find($user_id);
		$updated_by = '-';

		if (!empty($user)) {
			$updated_by = $user->name;	
		}

		return ucwords($updated_by);
	}
}
