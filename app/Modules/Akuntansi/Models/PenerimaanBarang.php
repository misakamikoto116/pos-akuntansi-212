<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PenerimaanBarang extends Model
{
    protected $table = 'permintaan_pembelian';
    public $timestamps = true;
	use SoftDeletes;
	// protected $fillable = [
	// 	'request_no',
	// 	'request_date',
	// 	'status',
 //        'catatan',
	// ];
}
