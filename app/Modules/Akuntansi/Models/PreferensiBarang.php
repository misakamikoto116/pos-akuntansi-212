<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PreferensiBarang extends Model
{
    protected $table = 'preferensi_barang';
    public $timestamps = true;
	use SoftDeletes;

	protected $fillable = [
		"akun_persediaan_id",
        "akun_penjualan_id",
        "akun_retur_penjualan_id",
        "akun_diskon_barang_id",
        "akun_barang_terkirim_id",
        "akun_hpp_id",
        "akun_retur_pembelian_id",
        "akun_beban_id",
        "akun_belum_tertagih_id",
        "minimal_stock",
        "estimasi_kada_luarsa",
	];

    public function akunPersediaan()
    {
        return $this->belongsTo(Akun::class,'akun_persediaan_id');
    }

    public function akunPenjualan()
    {
        return $this->belongsTo(Akun::class,'akun_penjualan_id');
    }

    public function akunReturPenjualan()
    {
        return $this->belongsTo(Akun::class,'akun_retur_penjualan_id');
    }

    public function akunDiskonBarang()
    {
        return $this->belongsTo(Akun::class,'akun_diskon_barang_id');
    }

    public function akunBarangTerkirim()
    {
        return $this->belongsTo(Akun::class,'akun_barang_terkirim_id');
    }

    public function akunHpp()
    {
        return $this->belongsTo(Akun::class,'akun_hpp_id');
    }

    public function akunReturPembelian()
    {
        return $this->belongsTo(Akun::class,'akun_retur_pembelian_id');
    }

    public function akunBeban()
    {
        return $this->belongsTo(Akun::class,'akun_beban_id');
    }

    public function akunBelumTertagih()
    {
        return $this->belongsTo(Akun::class,'akun_belum_tertagih_id');
    }
}
