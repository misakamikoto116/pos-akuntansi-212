<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class JurnalUmum extends Model
{
    protected $table = 'jurnal_umum';
    // protected $dates = ['tanggal'];
    public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'no_faktur',
		'tanggal',
		'description',
		'status',
		'tutup_buku',
        'tanggal_tutup_buku',
        'created_by',
		'updated_by',
		'deleted_by',
	];

	public function detailJurnalUmum()
	{
		return $this->hasMany(DetailJurnalUmum::class, 'jurnal_umum_id', 'id');
	}

	public function transaksi()
	{
		return $this->morphMany(Transaksi::class, 'item');
	}

	public function nominalJurnal()
	{
		return $this->detailJurnalUmum->where('debit', '<>', null)->sum('debit');
	}

	public function scopeReportJurnalUmum($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
            $query->whereBetween('tanggal',[$request->get('start-date'), $request->get('end-date')]);
        }else{
            $query = $query->whereBetween('tanggal', ['0001-01-01', date('Y-m-d', strtotime('+1 days'))]);
        }
        return $query;
	}

	public function scopeFilter($query, $data)
	{
		// Search By Keyword
    	if(!empty($data['search'])){
            $query = $query->where('no_faktur','like','%'.$data['search'].'%')
                           ->orWhere('description','like','%'.$data['search'].'%');
		}

		// No Faktur
		if(!empty($data['no_faktur'])){
			$query = $query->where('no_faktur', $data['no_faktur']);
		}

		// Keterangan
		if(!empty($data['keterangan_pembayaran'])){
			$query = $query->where('description', $data['keterangan_pembayaran']);
		}

		// Tanggal
		if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
			$query = $query->whereBetween('tanggal',[$data['tanggal_dari'],$data['tanggal_sampai']]);
		}
	}

	public function getUserCreatedAttribute()
	{
		$user_id	= $this->getAttribute('created_by');
		$user 		= User::find($user_id);
		$created_by = '-';

		if (!empty($user)) {
			$created_by = $user->name;	
		}

		return ucwords($created_by);
	}

	public function getUserUpdatedAttribute()
	{
		$user_id	= $this->getAttribute('updated_by');
		$user 		= User::find($user_id);
		$updated_by = '-';

		if (!empty($user)) {
			$updated_by = $user->name;	
		}

		return ucwords($updated_by);
	}
}
