<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ReturPenjualan extends Model
{
    protected $table = 'retur_penjualan';
    public $timestamps = true;
	use SoftDeletes;
    protected $fillable = [
    	'sr_no',
    	'tanggal',
    	'taxable',
    	'intax',
    	'no_fiscal',
    	'tgl_fiscal',
    	'pelanggan_id',
    	'faktur_penjualan_id',
    	'keterangan',
    	'diskon',
    	'total',
        'jumlah_diskon_retur',
        'tutup_buku',
        'tanggal_tutup_buku',
        'created_by',
    ];

    public function pelanggan()
    {
    	return $this->belongsTo(InformasiPelanggan::class,'pelanggan_id');
    }

    public function fakturPenjualan()
    {
    	return $this->belongsTo(FakturPenjualan::class,'faktur_penjualan_id');
	}

	public function barang()
	{
		return $this->hasMany(BarangReturPenjualan::class);
	}

    public function getSumAttribute()
    {
        return $this->barang->sum('harga');
    }

    public function scopeFilter($query, $data)
    {
        // Search By Keyword
        if(!empty($data['search'])){
            $query = $query->where('sr_no','like','%'.$data['search'].'%')
                ->orWhere('no_fiscal','like','%'.$data['search'].'%')
                ->orWhere('keterangan','like','%'.$data['search'].'%');
        }

        // No Faktur
        if(!empty($data['no_faktur'])){
            $query = $query->where('sr_no', $data['no_faktur']);
        }
        
        // Pelanggan Select
        if(!empty($data['pelanggan_select'])){
            $query = $query->where('pelanggan_id', $data['pelanggan_select']);
        }

        // Tanggal
        if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
            $query = $query->whereBetween('tanggal',[$data['tanggal_dari'],$data['tanggal_sampai']]);
        }

        // if (!empty($data['search_pelanggan'])) {
        //     $query = $query->whereHas('pelanggan', function($query) use ($data){
        //              $query->where('nama', 'like', '%'.$data['search_pelanggan'].'%')
        //                    ->orWhere('no_pelanggan', 'like', '%'.$data['search_pelanggan'].'%');
        //         }
        //     );
        // }
        
        return $query;
    }

    public function scopeReportInvoiceFilter($query, $request)
    {
        if($request->get('start-date')){
                $tgl_satu = date('Y-m')."-01";
                $query->whereBetween('tanggal', [$tgl_satu." 00:00:00", $request->get('start-date')." 23:59:59"]);
        }
        return $query;
    }

    public function scopeReportRincianFilter($query, $request)
    {
        if($request->get('start-date')){
                $query->whereBetween('tanggal', ['0001-01-01 00:00:00', $request->get('start-date')." 23:59:59"]);
        }
        return $query;
    }

    public function scopeReportPenerimaanFilter($query, $request)
    {
        if($request->get('start-date-rincian') && $request->get('end-date-rincian')){
            $query->whereBetween('tanggal',[$request->get('start-date-rincian')." 00:00:00", $request->get('end-date-rincian')." 23:59:59"]);
        }else{
            $query = $query->whereBetween('tanggal', ['0001-01-01', date('Y-m-d', strtotime('+1 days'))]);
        }
        return $query;
    }

    public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
            if($request->get('start-date') === $request->get('end-date')){
				return $query->whereDate('tanggal', $request->get('start-date'));
			}else{
				return $query->whereBetween('tanggal',[$request->get('start-date'), $request->get('end-date')]);
			}
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('tanggal', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
