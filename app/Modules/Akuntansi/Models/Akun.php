<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;

class Akun extends Model
{
    protected $table = 'akun';
    public $timestamps = true;
    use SoftDeletes;

    protected $fillable = [
        'kode_akun',
        'nama_akun',
        'money_function',
        'parent_id',
        'tipe_akun_id',
        'mata_uang_id',
        'fiksal',
        'alokasi_produksi',
        'tanggal_masuk',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    protected $with = [
        'transaksi',
    ];

    // public static function boot()
    // {
    //     parent::boot();
        
    //     self::deleting(function ($akun) {
    //         $layout = new LayoutHelper;

    //         // Transaksi
    //         if (!empty($akun->transaksi)) {
    //             if ($akun->transaksi->count() > 0) {
    //                 $html = 'Akun tidak dapat dihapus, karena masih memiliki Transaksi. ';
    //                 return $layout->batalkanProses($html);
    //             }
    //         }

    //         if (!empty($akun->childAkun)) {
    //             if ($akun->childAkun->count() > 0) {
    //                 $html = 'Akun tidak dapat dihapus, karena telah digunakan oleh Akun lain. ';
    //                 return $layout->batalkanProses($html);
    //             }
    //         }
    //         ///////////////////////////////////////////////
    //     });
    // }

    public function mataUang()
    {
        return $this->belongsTo(MataUang::class,'mata_uang_id');
    }

    public function tipeAkun()
    {
        return $this->belongsTo(TipeAkun::class);
    }

    public function parentAkun()
    {
        return $this->belongsTo(Akun::class, 'parent_id');
    }

    public function childAkun()
    {
        return $this->hasMany(Akun::class, 'parent_id', 'id');
    }

    public function pembayaran()
    {
        return $this->hasMany(Pembayaran::class);
    }

    public function penerimaan()
    {
        return $this->hasMany(Penerimaan::class);
    }

    public function detailBukuMasuk()
    {
        return $this->hasMany(DetailBukuMasuk::class);
    }

    public function detailBukuKeluar()
    {
        return $this->hasMany(DetailBukuKeluar::class);
    }

    public function detailJurnalUmum()
    {
        return $this->hasMany(DetailJurnalUmum::class);
    }

    public function transaksi()
    {
        return $this->hasMany(Transaksi::class);
    }

    public function fakturUangMuka()
    {
        return $this->hasMany(FakturPenjualan::class);
    }

    public function produkRetPenjualan()
    {
        return $this->hasMany(Produk::class, 'akun_ret_penjualan_id', 'id');
    }

    public function bebanFakturPembelian()
    {
        return $this->hasMany(BebanFakturPembelian::class);
    }

    public function prefBarang()
    {
        return $this->hasMany(PreferensiBarang::class);
    }

    public function daftarAktivaTetap()
    {
        return $this->hasOne(DaftarAktivaTetap::class);
    }

    public function penyesuaianPersediaan()
    {
        return $this->hasOne(PenyesuaianPersediaan::class);
    }

    public function akunPenyelesaianPesanan()
    {
        return $this->hasOne(AkunPenyelesaianPesanan::class);
    }

    public function typeEdc()
    {
        return $this->hasOne(typeEdc::class);
    }

    public function recrusiveAkun($akun, $saldo, $parent_stat)
    {
        foreach ($akun as $child) {

            $saldo_kredit   = $child->transaksi->where('status', 0)->sum('nominal');          

            $saldo_debit    = $child->transaksi->where('status', 1)->sum('nominal');

            $saldo += $saldo_debit - $saldo_kredit;

            // if (count($child->childAkun) > 0) {
            //     ++$parent_stat;
            //     $saldo = $this->recrusiveAkun($child->childAkun, $saldo, $parent_stat);
            // }
        }

        return $saldo;
    }

    public function scopeAkunChunk($query)
    {
        $data = [];
        $saldo = 0;
        $parent_stat = 0;
        $query = $query->orderBy('kode_akun')->chunk(100, function ($query) use (&$data, &$saldo, &$parent_stat) {
            foreach ($query as $akun) {
                $saldo = 0;
                $parent_stat = 0;

                $saldo = $akun->transaksi->sum('nominal');
                if (count($akun->childAkun) > 0) {
                    ++$parent_stat;
                    $saldo = $this->recrusiveAkun($akun->childAkun, $saldo, $parent_stat);
                }

                $data[] = [
                    'id' => $akun->id,
                    'parent_id' => $akun->parent_id,
                    'kode_akun' => $akun->kode_akun,
                    'nama_akun' => $akun->nama_akun,
                    // 'money_function' => $akun->money_function,
                    'tipe_akun' => $akun->tipeAkun->title,
                    'childAkun' => $akun->childAkun,
                    'saldo' => $saldo,
                    'parent_stat' => $parent_stat,
                ];
            }

            return $data;
        });

        return collect($data);
    }

    public function akunpajakpenjualan()
    {
        return $this->belongsTo(Akun::class, 'id');
    }


    public function scopeReportAkunPembayaran($query, $request)
    {
        if($request->get('start-date') && $request->get('end-date')){
            $query->whereHas('pembayaran', function ($query2) use ($request) {
                $query2->whereBetween('tanggal',[$request->get('start-date'), $request->get('end-date')]);
            });
        }else{
            $query = $query->whereHas('pembayaran', function ($query2) use ($request) {
                $query2->whereBetween('tanggal', ['0001-01-01', date('Y-m-d', strtotime('+1 days'))]);
            });
        }
        return $query;
    }

    public function scopeReportAkunPenerimaan($query, $request)
    {
        if($request->get('start-date') && $request->get('end-date')){
            $query->whereHas('penerimaan', function ($query2) use ($request) {
                $query2->whereBetween('tanggal',[$request->get('start-date'), $request->get('end-date')]);
            });
        }else{
            $query = $query->whereHas('penerimaan', function ($query2) use ($request) {
                $query2->whereBetween('tanggal', ['0001-01-01', date('Y-m-d', strtotime('+1 days'))]);
            });
        }
        return $query;
    }

    public function getMoneyFunctionFromTransactionAttribute()
    {
        $money_function = null;
        if (!empty($this->getAttribute('id'))) {
            $money_function = DetailJurnalUmum::with('jurnalUmum')->whereHas('jurnalUmum', function ($query)
            {
                $query->where('status', 0);
            })->where('akun_id', $this->getAttribute('id'))->first();
            
            if (!empty($money_function)) {
                $money_function = $money_function->debit > 0 ? $money_function->debit : $money_function->kredit;
            }else {
                $money_function = null;
            }
        }else {
            $money_function = null;
        }

        return $money_function;
    }

    public function scopeFilter($query, $data)
    {
        // Search By Keyword
        if(!empty($data['search'])){
            $query = $query->where('nama_akun','like','%'.$data['search'].'%')
                           ->orWhere('kode_akun','like','%'.$data['search'].'%');
        }

        // Nama Akun
        if(!empty($data['nama_akun'])){
            $query = $query->where('nama_akun','like','%'.$data['nama_akun'].'%');
        } 

        // Kode Akun
        if(!empty($data['no_akun'])){
            $query = $query->where('kode_akun','like','%'.$data['no_akun'].'%');
        }       

        // Tipe Akun
        if(!empty($data['tipe_akun_id'])){
            $query = $query->where('tipe_akun_id', $data['tipe_akun_id']);
        }

        return $query;
    }

    public function scopeLaporanKeuanganFilter($query, $data, $tanggal, $params)
    {
        // Search By Date 
        if ($tanggal) {
            if ($params === null) {
                $query = $query->with(['transaksi' => function ($subQuery) use ($tanggal) {
                                    if (!is_array($tanggal)) {
                                        $subQuery->whereDate('tanggal', '<=', $tanggal->toDateString());
                                    } else {
                                        $subQuery->whereDate('tanggal', '<=', array_last($tanggal)->toDateString());
                                    }
                                },'childAkun.transaksi' => function ($query) use ($tanggal) {
                                    if (!is_array($tanggal)) {
                                        $query->whereDate('tanggal', '<=', $tanggal->toDateString());
                                    } else {
                                        $query->whereDate('tanggal', '<=', array_last($tanggal)->toDateString());
                                    }
                                }, 'tipeAkun']);
            }else {
                $query = $query->with(['transaksi' => function ($subQuery) use ($tanggal) {
                                    $subQuery->whereBetween('tanggal', [$tanggal[0]->toDateString()." 00:00:00", $tanggal[1]->toDateString()." 23:59:59"]);
                                },'childAkun.transaksi' => function ($query) use ($tanggal) {
                                    $query->whereBetween('tanggal', [$tanggal[0]->toDateString()." 00:00:00", $tanggal[1]->toDateString()." 23:59:59"]);
                                }, 'tipeAkun']);
            }
        }

        return $query;
    }

    // public function getUserCreatedAttribute()
    // {
    //     $user_id    = $this->getAttribute('created_by');
    //     $user       = \DB::table('users')->find($user_id);
    //     $created_by = '-';

    //     if (!empty($user)) {
    //         $created_by = $user->name;  
    //     }

    //     return ucwords($created_by);
    // }

    // public function getUserUpdatedAttribute()
    // {
    //     $user_id    = $this->getAttribute('updated_by');
    //     $user       = User::find($user_id);
    //     $updated_by = '-';

    //     if (!empty($user)) {
    //         $updated_by = $user->name;  
    //     }

    //     return ucwords($updated_by);
    // }
}
