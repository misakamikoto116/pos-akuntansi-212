<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaldoAwalBarang extends Model
{
    protected $table = 'produk_detail';
    use SoftDeletes;

    protected $fillable = [
            'no_faktur',
            'tanggal',
            'kuantitas',
            // 'biaya',
            'harga_modal',
            'harga_terakhir',
            'gudang_id',
            'produk_id',
            'transaksi_id',
            'sn',
            'status',
            'item_type',
            'item_id',
            'created_by',
            'updated_by',
            'deleted_by',
    ];
    protected $dates = [
        'tanggal'
    ];

    public static function boot(){
    	parent::boot();

    	self::deleting(function ($detail){
    		return $detail->transaksi->delete();
    	});
    }

    public function gudang()
    {
        return $this->belongsTo(Gudang::class);
    }

    public function transaksi()
    {
        return $this->belongsTo(Transaksi::class, 'transaksi_id', 'id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class,'produk_id');
    }

    public function scopeDateFilter($query, $request)
    {
        if ($request->get('start-date') && $request->get('end-date')) {
            return $query->whereBetween('created_at', [$request->get('start-date'), $request->get('end-date')]);
        } else {
            $tgl_satu = date('Y-m').'-01';

            return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
        }
    }

    public function scopeFilter($query, $data)
    {
        if (!empty($data['search'])) {
            $query = $query->whereHas('produk', function ($produk) use($data) {
                $produk->where('no_barang', 'like', '%'.$data['search'].'%')
                    ->orWhere('keterangan', 'like', '%'.$data['search'].'%');
            });

            if (!empty($data['tipe_barang'])) {
                $query = $query->whereHas('produk', function ($produk) use($data){
                    $produk->where('tipe_barang', 'like', '%'.$data['tipe_barang'].'%');
                });
            }
            if(!empty($data['kategori'])){
                $query = $query->whereHas('produk', function ($produk) use($data){
                    $produk->whereHas('kategori', function ($kategori) use ($data){
                        $kategori->where('id', '=', $data['kategori']);
                    });
                });
            }
        }
        return $query;
    }
}
