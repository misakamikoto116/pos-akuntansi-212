<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BebanPembiayaanPesanan extends Model
{
    use SoftDeletes;
    public $timestamps = true;

    protected $table = "beban_pembiayaan_pesanan";
    protected $fillable = [
        'pembiayaan_pesanan_id',
        'akun_bbn_pembiayaan_pesanan_id',
        'tanggal',
        'catatan',
        'amount',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function akun()
	{
		return $this->belongsTo(Akun::class, 'akun_bbn_pembiayaan_pesanan_id', 'id');
    }
    
    public function pembiayaanPesanan()
	{
		return $this->belongsTo(PembiayaanPesanan::class, 'pembiayaan_pesanan_id', 'id');
	}
}
