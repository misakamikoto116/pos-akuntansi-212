<?php
namespace App\Modules\Akuntansi\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FakturPembelian extends Model
{
	protected $table   = 'faktur_pembelian';
	protected $appends = ['total_formatted', 'owing', 'payed', 'sum_tax', 'invoice_date_formatted', 'updated_uang_muka', 'owing_after_retur_pemasok', 'in_tax_texted', 'due_date_with_termin'];
	public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'no_faktur',
		'pemasok_id',
		'taxable',
		'in_tax',
		'diskon',
		'total_diskon_faktur',
		'form_no',
		'invoice_date',
		'fob',
		'term_id',
		'ship_id',
		'ship_date',
		'no_fp_std',
		'no_fp_std2',
		'no_fp_std_date',
		'bebankan_pemasok_id',
		'ongkir',
		'total',
		'keterangan',
		'catatan',
		'uang_muka',
		'akun_hutang_id',
		'akun_uang_muka_id',
		'tutup_buku',
		'tanggal_tutup_buku',
	];

	public function pemasok()
	{
		return $this->belongsTo(InformasiPemasok::class, 'pemasok_id');
	}

	public function barang()
	{
		return $this->hasMany(BarangFakturPembelian::class);
	}

	public function produk()
	{
		return $this->belongsToMany(Produk::class, 'barang_faktur_pembelian');
	}

	public function transaksi()
	{
		return $this->morphMany(Transaksi::class, 'item');
	}

	public function transaksiUangMukaPemasok()
	{
		return $this->hasMany(TransaksiUangMukaPemasok::class);
	}

	public function bebanFakturPembelian()
	{
		return $this->hasMany(bebanFakturPembelian::class);
	}

	public function invoice()
	{
		return $this->hasMany(InvoicePembayaranPembelian::class, 'faktur_id', 'id');
	}

	public function returPembelian()
	{
		return $this->hasMany(ReturPembelian::class);
	}

	public function syaratPembayaran()
	{
		return $this->belongsTo(SyaratPembayaran::class, 'term_id');
	}

	public function getTotalFormattedAttribute()
	{
		return $this->getAttribute('total') + $this->getAttribute('ongkir');
	}

	// public function getUangMukaFomattedAttribute() // awalnya getUangMukaAttribute()
	// {
	// 	return Transaksi::where([
	// 		'status'    => 1,
	// 		'item_id'   => $this->getAttribute('id'),
	// 		'item_type' => 'App\Modules\Akuntansi\Models\FakturPembelian', ])->sum('nominal');
	// }

	public function getOwingAttribute()
	{
		$id                     = $this->getAttribute('id');
		$invoice                = InvoicePembayaranPembelian::where('faktur_id', $this->getAttribute('id'))->sum('payment_amount');
		$transaksiUangMuka      = TransaksiUangMukaPemasok::where('faktur_pembelian_id', $id)->get();
		$uang_muka_dengan_pajak = $transaksiUangMuka->sum('jumlah') + $transaksiUangMuka->sum('total_pajak1') + $transaksiUangMuka->sum('total_pajak2');
		$total                  = $this->getAttribute('total') - $this->getAttribute('ongkir');

		return $total - $invoice - $uang_muka_dengan_pajak;
	}

	public function getFakturKurangUangMukaAttribute()
	{
		$id = $this->getAttribute('id');
		// $transaksiUangMuka = TransaksiUangMukaPemasok::where('faktur_pembelian_id', $id)->get();
		$transaksiUangMuka      = $this->transaksiUangMukaPemasok;
		$uang_muka_dengan_pajak = $transaksiUangMuka->sum('jumlah') + $transaksiUangMuka->sum('total_pajak1') + $transaksiUangMuka->sum('total_pajak2');
		$total                  = $this->getAttribute('total');

		return $total - $uang_muka_dengan_pajak;
	}

	public function getOwingAfterReturPemasokAttribute()
	{
		$id      = $this->getAttribute('id');
		$retur   = ReturPembelian::where('faktur_pembelian_id', $id)->sum('total');
		$invoice = InvoicePembayaranPembelian::where('faktur_id', $id)->sum('payment_amount');

		return $this->fakturKurangUangMuka - ($invoice + $retur);
	}

	public function getPayedAttribute()
	{
		$invoice = InvoicePembayaranPembelian::where('faktur_id', $this->getAttribute('id'))->sum('payment_amount');

		return $invoice;
	}

	public function getSumTaxAttribute()
	{
		$barang = BarangFakturPembelian::where('faktur_pembelian_id', $this->getAttribute('id'))->get();
		$total  = 0;
		foreach ($barang as $b) {
			$total += $b->tax_total;
		}

		return $total;
	}

	public function getInTaxTextedAttribute()
	{
		if (0 == $this->getAttribute('in_tax') || null == $this->getAttribute('in_tax')) {
			return 'Tidak';
		} else {
			return 'Ya';
		}
	}

	public function getInvoiceDateFormattedAttribute()
	{
		$date = ($this->getAttribute('invoice_date')) ? date('d F Y', strtotime($this->getAttribute('invoice_date'))) : '-';

		return $date;
	}

	public function getDueDateWIthTerminAttribute()
	{
		$date   = Carbon::parse($this->getAttribute('invoice_date'));
		$termin = $this->syaratPembayaran;
		if (!empty($termin)) {
			$due_date           = $date->addDays($termin->jatuh_tempo);
			$due_date_formatted = $due_date->format('d F Y');
		} else {
			$due_date_formatted = '';
		}

		return $due_date_formatted;
	}

	public function getUpdatedUangMukaAttribute()
	{
		$id                = $this->getAttribute('id');
		$transaksiUangMuka = TransaksiUangMukaPemasok::where('faktur_uang_muka_pemasok_id', $id)->sum('jumlah');
		$updated_uang_muka = $this->uang_muka_with_out_tax - $transaksiUangMuka;

		return $updated_uang_muka;
	}

	public function getUangMukaWithOutTaxAttribute()
	{
		return $this->barang()->where('faktur_pembelian_id', $this->getAttribute('id'))->pluck('unit_price')->first();
	}

	public function scopeFilter($query, $data)
	{
		// Serach By Keyword
		if (!empty($data['search'])) {
			$query = $query->where('no_faktur', 'like', '%' . $data['search'] . '%')
				->orWhere('form_no', 'like', '%' . $data['search'] . '%')
				->orWhere('catatan', 'like', '%' . $data['search'] . '%')
				->orWhere('keterangan', 'like', '%' . $data['search'] . '%');
		}

		// No Faktur
		if (!empty($data['no_faktur'])) {
			$query = $query->where('no_faktur', $data['no_faktur']);
		}

		// Keterangan
		if (!empty($data['keterangan_pembayaran'])) {
			$query = $query->where('keterangan', $data['keterangan_pembayaran']);
		}

		// Pemasok Select
		if (!empty($data['pemasok_select'])) {
			$query = $query->where('pemasok_id', $data['pemasok_select']);
		}

		// Tanggal
		if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
			$query = $query->whereBetween('invoice_date', [$data['tanggal_dari'], $data['tanggal_sampai']]);
		}

		// if (!empty($data['search_pemasok'])) {
		// 	$query = $query->whereHas('pemasok', function ($query) use ($data) {
		// 		$query->where('nama', 'like', '%' . $data['search_pemasok'] . '%')
		// 				   ->orWhere('no_pemasok', 'like', '%' . $data['search_pemasok'] . '%');
		// 	}
		// 	);
		// }

		return $query;
	}

	public function scopeReportInvoiceFilter($query, $request)
	{
		if ($request->get('start-date')) {
			$tgl_satu = date('Y-m') . '-01';
			$query->whereBetween('invoice_date', [$tgl_satu . ' 00:00:00', $request->get('start-date') . ' 23:59:59']);
		}

		return $query;
	}

	public function scopeReportRincianFilter($query, $request)
	{
		if ($request->get('start-date')) {
			$query->whereBetween('invoice_date', ['0001-01-01', $request->get('start-date')]);
		}

		return $query;
	}

	public function scopeDateFilter($query, $request)
	{
		if ($request->get('start-date') && $request->get('end-date')) {
			return $query->whereBetween('created_at', [$request->get('start-date'), $request->get('end-date')]);
		} else {
			$tgl_satu = date('Y-m') . '-01';

			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}

	public function scopeReportPenerimaanFilter($query, $request)
	{
		if ($request->get('start-date-rincian') && $request->get('end-date-rincian')) {
			$query->whereBetween('invoice_date', [$request->get('start-date-rincian') . ' 00:00:00', $request->get('end-date-rincian') . ' 23:59:59']);
		} else {
			$query = $query->whereBetween('invoice_date', ['0001-01-01', date('Y-m-d', strtotime('+1 days'))]);
		}

		return $query;
	}
}
