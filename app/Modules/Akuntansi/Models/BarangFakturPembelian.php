<?php
namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangFakturPembelian extends Model
{
	protected $table   = 'barang_faktur_pembelian';
	protected $appends = ['tax_total'];
	public $timestamps = true;
	use SoftDeletes;
	protected $fillable = [
		'produk_id',
		'faktur_pembelian_id',
		'barang_penerimaan_pembelian_id',
		'item_deskripsi',
		'item_unit',
		'jumlah',
		'unit_price',
		'harga_final',
		'bagian_diskon_master',
		'diskon',
		'kode_pajak_id',
		'kode_pajak_2_id',
		'gudang_id',
		'sn',
		'harga_modal',
		'expired_date',
	];

	public function fakturPembelian()
	{
		return $this->belongsTo(FakturPembelian::class);
	}

	public function barangPenerimaanPembelian()
	{
		return $this->belongsTo(BarangPenerimaanPembelian::class,'barang_penerimaan_pembelian_id');
	}

	public function pajak()
	{
		return $this->belongsTo(KodePajak::class, 'kode_pajak_id');
	}

	public function pajak2()
	{
		return $this->belongsTo(KodePajak::class, 'kode_pajak_2_id');
	}

	public function getHargaTotalAttribute()
	{
		$total = $this->getAttribute('unit_price') * $this->getAttribute('unit_price');
		$disc  = $total                            * $this->getAttribute('diskon') / 100;

		$grandtotal = $total - $disc;

		return $grandtotal;
	}

	public function produk()
	{
		return $this->belongsTo(Produk::class,'produk_id');
	}

	public function getTaxTotalAttribute()
	{
		$pajak1 = 0;
		$pajak2 = 0;
		if (!empty($this->getAttribute('kode_pajak_id'))) {
			$pajak1 = KodePajak::find($this->getAttribute('kode_pajak_id'))->first()->nilai;
		}

		if (!empty($this->getAttribute('kode_pajak_2_id'))) {
			$pajak2 = KodePajak::find($this->getAttribute('kode_pajak_2_id'))->first()->nilai;
		}

		return $pajak1 + $pajak2;
	}

	public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}

	public function gudang()
	{
		return $this->belongsTo(Gudang::class, 'gudang_id', 'id');
	}
}
