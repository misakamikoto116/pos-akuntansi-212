<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangPermintaanPembelian extends Model
{
    protected $table = 'barang_permintaan_pembelian';
    public $timestamps = true;
    protected $dates = ['required_date'];
	use SoftDeletes;
	protected $fillable = [
	                'produk_id',
	                'permintaan_pembelian_id',
                	'item_deskripsi',
                	'item_unit',
                	'jumlah',
                    'required_date',
                	'notes',
	];

    public function permintaanPembelian()
    {
        return $this->belongsTo(PermintaanPembelian::class,'permintaan_pembelian_id');
    }

    public function barangPesananPembelian()
    {
		return $this->hasMany(BarangPesananPembelian::class);
	}

    public function produk()
    {
        return $this->belongsTo(Produk::class,'produk_id');
    }

    public function getRequiredDateFormattedAttribute()
    {
        return $this->required_date->format('d F Y');
    }
    
    public function gudang()
	{
		return $this->belongsTo(Gudang::class, 'gudang_id', 'id');
	}
}
