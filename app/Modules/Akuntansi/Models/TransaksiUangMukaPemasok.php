<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransaksiUangMukaPemasok extends Model
{
    protected $table = "transaksi_uang_muka_pemasok";
    use SoftDeletes;
    public $timestamps = true;
    protected $fillable = [
        'faktur_uang_muka_pemasok_id',
        'faktur_pembelian_id',
        'jumlah',
        'pajak_satu_id',
        'pajak_dua_id',
        'total_pajak1',
        'total_pajak2',
    ];

    public function detailPemasok()
	{
		return $this->belongsTo(DetailInformasiPemasok::class,'detail_informasi_pemasok_id');
    }

    public function fakturPembelian()
    {
        return $this->belongsTo(FakturPembelian::class,'faktur_pembelian_id');
    }

    public function fakturUangMukaPemasok()
    {
        return $this->belongsTo(FakturPembelian::class,'faktur_uang_muka_pemasok_id');
    }

    public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}
}
