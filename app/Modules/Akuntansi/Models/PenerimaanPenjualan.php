<?php

namespace App\Modules\Akuntansi\Models;

use App\Modules\Pos\Models\PaymentHistory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class PenerimaanPenjualan extends Model
{
    protected $table = 'penerimaan_penjualan';
    public $timestamps = true;
    use SoftDeletes;
    protected $fillable = [
        "pelanggan_id",
        "akun_bank_id" ,
        "rate" ,
        "cheque_no" ,
        "cheque_date" ,
        "memo" ,
        "form_no" ,
        "payment_date" ,
        "kosong" ,
        "fiscal_payment" ,
        "cheque_amount" ,
        "existing_amount" ,
        "distribute_amount" ,
        "created_by",
        'tutup_buku',
        'tanggal_tutup_buku',
    ];

    public function pelanggan()
	{
		return $this->belongsTo(InformasiPelanggan::class, 'pelanggan_id','id');
	}

    public function invoice()
    {
        return $this->hasMany(InvoicePenerimaanPenjualan::class);
    }

    public function paymentHistory()
    {
        return $this->hasOne(PaymentHistory::class);
    }

    public function scopeFilter($query, $data)
    {
        // Search By Keyword
        if(!empty($data['search'])){
            $query = $query->where('cheque_no','like','%'.$data['search'].'%')
                ->orWhere('form_no','like','%'.$data['search'].'%')
                ->orWhere('memo','like','%'.$data['search'].'%');
        }

        // Pelanggan Select
        if(!empty($data['pelanggan_select'])){
            $query = $query->where('pelanggan_id', $data['pelanggan_select']);
        }
        
        // Tanggal
        if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
            $query = $query->whereBetween('payment_date',[$data['tanggal_dari'],$data['tanggal_sampai']]);
        }

        // if (!empty($data['search_pelanggan'])) {
        //     $query = $query->whereHas('pelanggan', function($query) use ($data){
        //              $query->where('nama', 'like', '%'.$data['search_pelanggan'].'%')
        //                    ->orWhere('no_pelanggan', 'like', '%'.$data['search_pelanggan'].'%');
        //         }
        //     );
        // }
        
        return $query;
    }

    public function scopeReportPenerimaanFilter($query, $request)
    {
        if($request->get('start-date-rincian') && $request->get('end-date-rincian')){
            $query->whereBetween('payment_date',[$request->get('start-date-rincian')." 00:00:00", $request->get('end-date-rincian')." 23:59:59"]);
        }else{
            $query = $query->whereBetween('payment_date', ['0001-01-01', date('Y-m-d', strtotime('+1 days'))]);
        }
        return $query;
    }
}
