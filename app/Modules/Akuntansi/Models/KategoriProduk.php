<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helpers\LayoutHelper;
class KategoriProduk extends Model
{
    protected $table = 'kategori_produk';
	use SoftDeletes;

	protected $fillable = [
		'nama',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public static function boot()
	{
		parent::boot();

		self::deleting(function ($kategori) {
			$layout = new LayoutHelper;
			if ($kategori->produk->count() > 0) {
				$html = 'Kategori tidak dapat dihapus, karena ada produk yang sudah menggunakan kategori ini.';
				return $layout->batalkanProses($html);
            }
		});
	}

	public function produk()
	{
		return $this->hasMany(Produk::class,'kategori_produk_id','id');
	}
}
