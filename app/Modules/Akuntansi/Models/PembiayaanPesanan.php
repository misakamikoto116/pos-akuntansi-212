<?php

namespace App\Modules\Akuntansi\Models;

use Helpers\IndonesiaDate;
use Helpers\LayoutHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PembiayaanPesanan extends Model
{
    use SoftDeletes;
    public $timestamps = true;

    protected $table = "pembiayaan_pesanan";
    protected $fillable = [
        'batch_no',
        'date',
        'akun_pembiayaan_pesanan_id',
        'deskripsi',
        'status',
        'created_by',
        'updated_by',
        'deleted_by',
    ];
	protected $appends = ['total_pembiayaan'];
    
    public function akun()
	{
		return $this->belongsTo(Akun::class, 'akun_pembiayaan_pesanan_id', 'id');
    }
    
    public function detail()
    {
        return $this->hasMany(DetailPembiayaanPesanan::class, 'pembiayaan_pesanan_id','id');
    }

    public function beban()
    {
        return $this->hasMany(BebanPembiayaanPesanan::class, 'pembiayaan_pesanan_id','id');
    }

    public function penyelesaianPesanan()
    {
        return $this->hasOne(PenyelesaianPesanan::class);
    }

    public function getTanggalFormattedAttribute()
	{
        $helper = new IndonesiaDate;
        if($this->getAttribute('date') == '0000-00-00 00:00:00'){
            return "-";
        }
		return $helper->blogDate($this->getAttribute('date'));
	}

    public function getTotalPembiayaanAttribute()
    {
        $beban = 0;
        if(!empty(self::beban()->get())){
            $beban = self::beban()->sum('amount');
        }
        return self::detail()->sum('cost') + $beban;
    }

    public function getTotalBarangAttribute()
    {
        $barang = 0;
        if (!empty(self::detail()->get())) {
            $barang = self::detail()->sum('cost');
        }

        return $barang;
    }

    public function getTotalBebanAttribute()
    {
        $beban = 0;
        if(!empty(self::beban()->get())){
            $beban = self::beban()->sum('amount');
        }
        return $beban;
    }

    public function getStatusFormattedAttribute()
    {
        $helper = new LayoutHelper;
        $status = '';
        if ($this->getAttribute('status') == 0) {
            $status = $helper->formLabel('warning','Sedang Diproses');
        }else {
            $status = $helper->formLabel('success','Selesai');
        }

        return $status;
    }

    public function scopeDateFilter($query, $request)
	{
		if($request->get('start-date') && $request->get('end-date')){
			return $query->whereBetween('created_at',[$request->get('start-date'), $request->get('end-date')]);
		}else{
			$tgl_satu = date('Y-m')."-01";
			return $query->whereBetween('created_at', [$tgl_satu, date('Y-m-d', strtotime('+1 days'))]);
		}
	}

    public function scopeFilter($query, $data)
    {
        // Keyword
        if(!empty($data['search'])){
            $query = $query->where('batch_no','like','%'.$data['search'].'%')
                           ->orWhere('deskripsi','like','%'.$data['search'].'%');
        }

        // No Batch
        if (!empty($data['no_batch'])) {
            $query = $query->where('batch_no','LIKE','%'.$data['no_batch'].'%');
        }

        // Keterangan
        if (!empty($data['status_pembiayaan'])) {
            if ($data['status_pembiayaan'] == 2) {
                $query = $query;
            }else {
                $query = $query->where('status','LIKE','%'.$data['status_pembiayaan'].'%');
            }
        }else if ($data['status_pembiayaan'] == 0) {
                $query = $query->where('status','LIKE','%'.$data['status_pembiayaan'].'%');
        }

        // Keterangan
        if (!empty($data['keterangan_pembayaran'])) {
            $query = $query->where('deskripsi','LIKE','%'.$data['keterangan_pembayaran'].'%');
        }

        // Tanggal
        if (!empty($data['tanggal_dari']) || !empty($data['tanggal_sampai'])) {
            $query = $query->whereBetween('date',[$data['tanggal_dari'],$data['tanggal_sampai']]);
        }

        return $query;
    }
}
