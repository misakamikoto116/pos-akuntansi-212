<?php

namespace App\Modules\Akuntansi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BantuanPengirimanPenjualan extends Model
{
    use SoftDeletes;
    protected $table = 'bantu_pengiriman_penjualan';

    protected $fillable = [
                    'brg_pengiriman_penjualan_id',
                    'bnt_penerimaan_pembelian_id',
    ];

    public function penerimaanPembelian()
    {
        return $this->belongsTo(PenerimaanPembelian::class, 'bnt_penerimaan_pembelian_id', 'id');
    }

    public function barangPenerimaanPembelian()
    {
        return $this->belongsTo(BarangPengirimanPenjualan::class, 'brg_pengiriman_penjualan_id', 'id');
    }
}
