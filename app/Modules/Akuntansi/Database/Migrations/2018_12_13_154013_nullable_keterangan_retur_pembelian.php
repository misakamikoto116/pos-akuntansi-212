<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullableKeteranganReturPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('retur_pembelian', 'keterangan')) {
            Schema::table('retur_pembelian', function (Blueprint $table) {
                $table->text('keterangan')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('retur_pembelian', function (Blueprint $table) {
            //
        });
    }
}
