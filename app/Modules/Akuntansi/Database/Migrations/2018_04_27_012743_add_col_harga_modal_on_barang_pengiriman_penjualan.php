<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColHargaModalOnBarangPengirimanPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('barang_pengiriman_penjualan', 'harga_modal')) {
            Schema::table('barang_pengiriman_penjualan', function (Blueprint $table) {
                $table->float('harga_modal',19,2)->after('status');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_pengiriman_penjualan', function (Blueprint $table) {
            //
        });
    }
}
