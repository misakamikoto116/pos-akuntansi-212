<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransaksiUangMuka extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('transaksi_uang_muka')) {
            Schema::create('transaksi_uang_muka', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('detail_informasi_pelanggan_id')->unsigned();
                $table->foreign('detail_informasi_pelanggan_id')
                ->references('id')
                ->on('detail_informasi_pelanggan')
                ->onUpdate('cascade');

                $table->integer('faktur_penjualan_id')->unsigned();
                $table->foreign('faktur_penjualan_id')
                ->references('id')
                ->on('faktur_penjualan')
                ->onUpdate('cascade');

                $table->float('jumlah',19,2);

                $table->softDeletes();
                $table->timestamps();
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_uang_muka');        
    }
}
