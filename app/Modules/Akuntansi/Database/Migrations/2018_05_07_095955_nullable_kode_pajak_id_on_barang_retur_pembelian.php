<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullableKodePajakIdOnBarangReturPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('barang_retur_pembelian', 'kode_pajak_id')) {
            Schema::table('barang_retur_pembelian', function (Blueprint $table) {
                $table->dropForeign('barang_retur_pembelian_kode_pajak_id_foreign');
                $table->dropColumn('kode_pajak_id');
            });
        }

        if (!Schema::hasColumn('barang_retur_pembelian', 'kode_pajak_id')) {
            Schema::table('barang_retur_pembelian', function (Blueprint $table) {
                $table->integer('kode_pajak_id')->unsigned()->after('gudang_id')->nullable();
                $table->foreign('kode_pajak_id')
                                ->references('id')
                                ->on('kode_pajak');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_retur_pembelian', function (Blueprint $table) {
            //
        });
    }
}
