<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstimasiKadaLuarsaOnPreferensiBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('preferensi_barang', 'estimasi_kada_luarsa')) {
            Schema::table('preferensi_barang', function (Blueprint $table) {
                $table->integer('estimasi_kada_luarsa')->after('minimal_stock')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preferensi_barang', function (Blueprint $table) {
            //
        });
    }
}
