<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyelesaianPesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penyelesaian_pesanan', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('tipe_penyelesaian')->comment('tipe_penyelesaian 0 = to_akun 1 = to_item')->default(0);
            $table->datetime('tgl_selesai');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penyelesaian_pesanan');
    }
}
