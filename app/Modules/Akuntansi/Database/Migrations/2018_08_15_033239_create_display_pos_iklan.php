<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisplayPosIklan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('display_iklan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_iklan', 100);
            $table->string('image_path', 100);
            $table->string('deskripsi', 200);
            $table->boolean('publish')->default(true)->comment('urutan iklan dalam tampilan');
            $table->integer('urutan', false, true);
            $table->integer('created_by', false, true);
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('display_iklan');
    }
}
