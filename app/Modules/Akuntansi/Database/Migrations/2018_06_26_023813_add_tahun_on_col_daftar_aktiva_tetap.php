<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTahunOnColDaftarAktivaTetap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('daftar_aktiva_tetap', 'tahun')) {
            Schema::table('daftar_aktiva_tetap', function (Blueprint $table) {
                $table->integer('tahun')->nullable()->after('umur_bulan_aktiva');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daftar_aktiva_tetap', function (Blueprint $table) {
            //
        });
    }
}
