<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteFieldBiayaOnProdukDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('produk_detail', 'biaya')) {
            Schema::table('produk_detail', function (Blueprint $table) {
                $table->dropColumn('biaya');
                $table->tinyInteger('status')->after('sn')->comment('0 = harga awal, 1 = harga transaksi')->default(0);
                
            });   
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('produk_detail', 'biaya')) {
            Schema::table('produk_detail', function (Blueprint $table) {
                $table->double('biaya')->after('kuantitas');
                $table->dropColumn('status');
            });   
        }
    }
}
