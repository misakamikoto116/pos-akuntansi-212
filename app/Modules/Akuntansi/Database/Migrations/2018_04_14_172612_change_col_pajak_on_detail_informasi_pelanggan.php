<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColPajakOnDetailInformasiPelanggan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('detail_informasi_pelanggan', 'pajak')) {
            Schema::table('detail_informasi_pelanggan', function (Blueprint $table) {
                $table->dropColumn('pajak');
            });
        }

        if (!Schema::hasColumn('detail_informasi_pelanggan', 'pajak')) {
            Schema::table('detail_informasi_pelanggan', function (Blueprint $table) {
                $table->float('pajak',19,2)->after('status');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_informasi_pelanggan', function (Blueprint $table) {
            //
        });
    }
}
