<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastOwingOnTableInvoicePembayaranPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('invoice_pembayaran_pembelian', 'last_owing')) {
            Schema::table('invoice_pembayaran_pembelian', function (Blueprint $table) {
                $table->float('last_owing',19,2)->after('payment_amount')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_pembayaran_pembelian', function (Blueprint $table) {
            //
        });
    }
}
