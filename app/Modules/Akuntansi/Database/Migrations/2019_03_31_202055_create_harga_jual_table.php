<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHargaJualTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('harga_jual')) {
            Schema::create('harga_jual', function (Blueprint $table) {
                $table->increments('id');
                $table->string('no_barang')->nullable();
                $table->string('barcode')->nullable();
                $table->string('keterangan')->nullable();
                $table->float('harga_jual', 19,2)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harga_jual');
    }
}
