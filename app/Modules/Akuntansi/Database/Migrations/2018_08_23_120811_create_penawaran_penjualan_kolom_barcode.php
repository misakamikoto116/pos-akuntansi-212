<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenawaranPenjualanKolomBarcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penawaran_penjualan', function (Blueprint $table) {
            $table->string('barcode_penawaran', 20)->after('no_penawaran')->nullable(true);
            $table->index(['barcode_penawaran']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penawaran_penjualan', function (Blueprint $table) {
            $table->dropIndex(['barcode_penawaran']);
            $table->dropColumn('barcode_penawaran');
        });
    }
}
