<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeGudangToUseIdOnDetailPenyesuaianPersediaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_penyesuaian_persediaan', function (Blueprint $table) {
            $table->dropColumn('gudang');
        }); 

        Schema::table('detail_penyesuaian_persediaan', function (Blueprint $table){
            $table->integer('gudang_id')->after('departemen')->nullable()->unsigned();
            $table->foreign('gudang_id')
                ->references('id')
                ->on('gudang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
