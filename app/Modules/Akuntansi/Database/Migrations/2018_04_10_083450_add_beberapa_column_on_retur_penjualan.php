<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBeberapaColumnOnReturPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('retur_penjualan', 'taxable')) {
            Schema::table('retur_penjualan', function (Blueprint $table) {
                $table->tinyInteger('taxable')->nullable()->comment('0 => false, 1 => true')->after('tanggal');
            });
        }
        if (!Schema::hasColumn('retur_penjualan', 'intax')) {
            Schema::table('retur_penjualan', function (Blueprint $table) {
                $table->tinyInteger('intax')->nullable()->comment('0 => false, 1 => true')->after('taxable');
            });
        }
        if (!Schema::hasColumn('retur_penjualan', 'no_fiscal')) {
            Schema::table('retur_penjualan', function (Blueprint $table) {
                $table->string('no_fiscal')->nullable()->after('intax');
            });
        }
        if (!Schema::hasColumn('retur_penjualan', 'tgl_fiscal')) {
            Schema::table('retur_penjualan', function (Blueprint $table) {
                $table->date('tgl_fiscal')->nullable()->after('no_fiscal');
            });
        }

        if (Schema::hasColumn('retur_penjualan', 'sr_no')) {
            Schema::table('retur_penjualan', function (Blueprint $table) {
                $table->dropColumn('sr_no');
            });
        }

        if (!Schema::hasColumn('retur_penjualan', 'sr_no')) {
            Schema::table('retur_penjualan', function (Blueprint $table) {
                $table->string('sr_no')->after('id');
            });
        }

        if (Schema::hasColumn('retur_penjualan', 'keterangan')) {
            Schema::table('retur_penjualan', function (Blueprint $table) {
                $table->dropColumn('keterangan');
            });
        }

        if (!Schema::hasColumn('retur_penjualan', 'keterangan')) {
            Schema::table('retur_penjualan', function (Blueprint $table) {
                $table->text('keterangan')->nullable()->after('faktur_penjualan_id');
            });
        }

        if (Schema::hasColumn('retur_penjualan', 'diskon')) {
            Schema::table('retur_penjualan', function (Blueprint $table) {
                $table->dropColumn('diskon');
            });
        }

        if (!Schema::hasColumn('retur_penjualan', 'diskon')) {
            Schema::table('retur_penjualan', function (Blueprint $table) {
                $table->double('diskon')->nullable()->after('keterangan');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('retur_penjualan', function (Blueprint $table) {
            //
        });
    }
}
