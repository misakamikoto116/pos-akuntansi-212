<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColKodePajakIdOnBarangPesananPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('barang_pesanan_penjualan', 'pajak')) {
            Schema::table('barang_pesanan_penjualan', function (Blueprint $table) {
                $table->dropColumn('pajak');
            });
        }

        if (!Schema::hasColumn('barang_pesanan_penjualan', 'kode_pajak_id')) {
            Schema::table('barang_pesanan_penjualan', function (Blueprint $table) {
                $table->integer('kode_pajak_id')->unsigned()->after('diskon')->nullable();
                $table->foreign('kode_pajak_id')
                    ->references('id')
                    ->on('kode_pajak');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_pesanan_penjualan', function (Blueprint $table) {
            //
        });
    }
}
