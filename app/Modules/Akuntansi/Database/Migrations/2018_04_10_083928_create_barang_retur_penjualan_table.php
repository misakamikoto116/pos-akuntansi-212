<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangReturPenjualanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_retur_penjualan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id')->unsigned();
                $table->foreign('produk_id')
                ->references('id')
                ->on('produk')
                ->onUpdate('cascade');
            $table->integer('barang_faktur_penjualan_id')->unsigned();
                $table->foreign('barang_faktur_penjualan_id')
                ->references('id')
                ->on('barang_faktur_penjualan')
                ->onUpdate('cascade');
            $table->integer('retur_penjualan_id')->unsigned();
                $table->foreign('retur_penjualan_id')
                ->references('id')
                ->on('retur_penjualan')
                ->onUpdate('cascade');
            $table->text('item_deskripsi');
            $table->integer('jumlah');
            $table->float('harga',19,2);
            $table->integer('gudang_id')->unsigned();
                $table->foreign('gudang_id')
                ->references('id')
                ->on('gudang')
                ->onUpdate('cascade');
            $table->integer('kode_pajak_id')->unsigned();
                $table->foreign('kode_pajak_id')
                ->references('id')
                ->on('kode_pajak')
                ->onUpdate('cascade');
            $table->double('diskon')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0 => Tetap, 1 => Berubah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_retur_penjualan');
    }
}
