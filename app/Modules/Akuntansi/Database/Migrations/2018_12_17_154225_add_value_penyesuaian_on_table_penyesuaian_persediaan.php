<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValuePenyesuaianOnTablePenyesuaianPersediaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('penyesuaian_persediaan', 'value_penyesuaian')) {
            Schema::table('penyesuaian_persediaan', function (Blueprint $table) {
                $table->tinyInteger('value_penyesuaian')->after('status')->nullable()->comment('0 = kuantitas 1 = nilai atau uang');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penyesuaian_persediaan', function (Blueprint $table) {
            //
        });
    }
}
