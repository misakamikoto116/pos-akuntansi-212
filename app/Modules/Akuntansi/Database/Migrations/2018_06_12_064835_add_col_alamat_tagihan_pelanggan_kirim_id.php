<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColAlamatTagihanPelangganKirimId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('pesanan_penjualan', 'alamat_tagihan')) {
            Schema::table('pesanan_penjualan', function (Blueprint $table) {
                $table->text('alamat_tagihan')->nullable()->after('alamat_pengiriman');
            });
        }

        if (!Schema::hasColumn('pesanan_penjualan', 'pelanggan_kirim_ke_id')) {
            Schema::table('pesanan_penjualan', function (Blueprint $table) {
                $table->integer('pelanggan_kirim_ke_id')->unsigned()->after('pelanggan_id')->nullable();
                $table->foreign('pelanggan_kirim_ke_id')
                      ->references('id')
                      ->on('informasi_pelanggan');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesanan_penjualan', function (Blueprint $table) {
            //
        });
    }
}
