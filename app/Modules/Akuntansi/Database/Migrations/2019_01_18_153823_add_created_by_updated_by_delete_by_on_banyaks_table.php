<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedByUpdatedByDeleteByOnBanyaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data =  [
            'syarat_pembayaran',
            'tipe_pelanggan',
            'mata_uang',
            'akun',
            'tipe_akun',
            'jurnal_umum',
            'buku_masuk',
            'buku_keluar',
            'serial_number',
            'produk',
            'gudang',
            'penyesuaian_persediaan',
            'kategori_produk',
            'tipe_aktiva_tetap_pajak',
            'tipe_aktiva_tetap',
            'pemindahan_barang',
            'daftar_aktiva_tetap',
            'penyelesaian_pesanan',
            'pembiayaan_pesanan',
            'jasa_pengiriman',
            'informasi_pelanggan',
            'informasi_pemasok',
            'produk_detail',
            'detail_buku_masuk',
            'detail_buku_keluar',
            'detail_serial_number',
            'detail_penyesuaian_persediaan',
            'detail_jurnal_umum',
            'detail_pemindahan_barang',
            'daftar_aktiva_tetap_detail',
            'detail_penyelesaian_pesanan',
            'akun_penyelesaian_pesanan',
            'beban_pembiayaan_pesanan',
            'detail_informasi_pelanggan',
            'detail_informasi_pemasok',
        ];

        foreach ($data as $table) {
            if (!Schema::hasColumn($table, 'created_by')) {
                Schema::table($table, function (Blueprint $table) {
                    $table->integer('created_by')->unsigned()->nullable();
                });
            }

            if (!Schema::hasColumn($table, 'updated_by')) {
                Schema::table($table, function (Blueprint $table) {
                    $table->integer('updated_by')->unsigned()->nullable();
                });
            }

            if (!Schema::hasColumn($table, 'deleted_by')) {
                Schema::table($table, function (Blueprint $table) {
                    $table->integer('deleted_by')->unsigned()->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
