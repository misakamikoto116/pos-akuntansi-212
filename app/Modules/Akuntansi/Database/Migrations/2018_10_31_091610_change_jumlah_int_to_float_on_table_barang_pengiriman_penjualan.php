<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeJumlahIntToFloatOnTableBarangPengirimanPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('barang_pengiriman_penjualan', 'jumlah')) {
            Schema::table('barang_pengiriman_penjualan', function (Blueprint $table) {
                $table->float('jumlah',19,2)->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_pengiriman_penjualan', function (Blueprint $table) {
            //
        });
    }
}
