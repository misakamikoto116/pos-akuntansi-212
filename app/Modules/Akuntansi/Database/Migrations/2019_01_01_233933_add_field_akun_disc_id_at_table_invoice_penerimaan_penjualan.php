<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAkunDiscIdAtTableInvoicePenerimaanPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_penerimaan_penjualan', function (Blueprint $table) {
            $table->integer('akun_disc_id')->unsigned()->nullable()->after('payment_amount');
            $table->foreign('akun_disc_id')->references('id')->on('akun');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_penerimaan_penjualan', function (Blueprint $table) {
            //
        });
    }
}
