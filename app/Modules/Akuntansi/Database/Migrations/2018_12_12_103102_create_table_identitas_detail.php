<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIdentitasDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identitas_pajak', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('identitas_id')->unsigned();
            $table->foreign('identitas_id')
                  ->references('id')
                  ->on('identitas');
            $table->string('no_seri_faktur_pajak')->nullable();
            $table->string('npwp')->nullable();
            $table->string('no_pengukuhan_pkp')->nullable();
            $table->string('tanggal_pengukuhan_pkp')->nullable();
            $table->string('kode_cabang_pajak')->nullable();
            $table->string('jenis_usaha')->nullable();
            $table->string('klu_spt')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identitas_pajak');
    }
}
