<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableCatatanOnDaftarAktivaTetap extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        if (!Schema::hasColumn('daftar_aktiva_tetap', 'catatan')) {
            Schema::table('daftar_aktiva_tetap', function (Blueprint $table) {
                $table->text('catatan')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('daftar_aktiva_tetap', function (Blueprint $table) {
        });
    }
}
