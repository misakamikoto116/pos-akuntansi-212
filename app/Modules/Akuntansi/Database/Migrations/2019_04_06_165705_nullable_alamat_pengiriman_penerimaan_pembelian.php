<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullableAlamatPengirimanPenerimaanPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('penerimaan_pembelian', 'alamat_pengiriman')) {
            Schema::table('penerimaan_pembelian', function (Blueprint $table) {
                $table->text('alamat_pengiriman')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penerimaan_pembelian', function (Blueprint $table) {
            //
        });
    }
}
