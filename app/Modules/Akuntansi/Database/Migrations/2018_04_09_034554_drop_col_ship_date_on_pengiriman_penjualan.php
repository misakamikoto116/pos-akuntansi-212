<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColShipDateOnPengirimanPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('pengiriman_penjualan', 'ship_date')) {
            Schema::table('pengiriman_penjualan', function (Blueprint $table) {
                $table->dropColumn('ship_date');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengiriman_penjualan', function (Blueprint $table) {
            //
        });
    }
}
