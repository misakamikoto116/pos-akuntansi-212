<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableNikDanFoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('informasi_pelanggan', 'nik'))
        {
            Schema::table('informasi_pelanggan', function (Blueprint $table) {
                $table->string('foto_pelanggan')->after('nama')->nullable();
                $table->string('nik')->after('foto_pelanggan');
                $table->string('foto_nik_pelanggan')->after('nik')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('informasi_pelanggan', function (Blueprint $table) {
            //
        });
    }
}
