<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableKeteranganShipOnTablePengirimanPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('pengiriman_penjualan', 'ship_id')) {
            Schema::table('pengiriman_penjualan', function (Blueprint $table) {
                $table->dropForeign('pengiriman_penjualan_ship_id_foreign');
                $table->dropColumn('ship_id');
            });
        }

        if (!Schema::hasColumn('pengiriman_penjualan', 'ship_id')) {
            Schema::table('pengiriman_penjualan', function (Blueprint $table) {
                $table->integer('ship_id')->unsigned()->nullable()->after('po_no');
                $table->foreign('ship_id')
                      ->references('id')
                      ->on('jasa_pengiriman')
                      ->onUpdate('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengiriman_penjualan', function (Blueprint $table) {
            //
        });
    }
}
