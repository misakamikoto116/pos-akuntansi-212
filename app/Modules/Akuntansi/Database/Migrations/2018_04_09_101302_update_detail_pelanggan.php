<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDetailPelanggan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('detail_informasi_pelanggan', 'taxable')) {
            Schema::table('detail_informasi_pelanggan', function (Blueprint $table) {
                $table->tinyInteger('taxable')->default(0)->after('tanggal');
                $table->tinyInteger('in_tax')->default(0)->after('taxable');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('detail_informasi_pelanggan', 'taxable')) {
            Schema::table('detail_informasi_pelanggan', function (Blueprint $table) {
                $table->dropColumn('taxable');
                $table->dropColumn('in_tax');
            });
        }
    }
}
