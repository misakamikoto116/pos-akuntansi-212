<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBantuanPenerimaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bantu_penerimaan_pembelian', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('produk_id')->unsigned();
            $table->foreign('produk_id')
                    ->references('id')
                    ->on('produk');

            $table->integer('brg_penerimaan_pembelian_id')->unsigned();
            $table->foreign('brg_penerimaan_pembelian_id')
                    ->references('id')
                    ->on('barang_penerimaan_pembelian');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bantu_penerimaan_pembelian');
    }
}
