<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKolomPajakOnDetailInfoPelanggan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('detail_informasi_pelanggan', 'pajak')) {
            Schema::table('detail_informasi_pelanggan', function (Blueprint $table) {
                $table->double('pajak')->default(0)->after('tanggal');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('detail_informasi_pelanggan', 'pajak')) {
            Schema::table('detail_informasi_pelanggan', function (Blueprint $table) {
                $table->dropColumn('pajak');
            });
        }
    }
}
