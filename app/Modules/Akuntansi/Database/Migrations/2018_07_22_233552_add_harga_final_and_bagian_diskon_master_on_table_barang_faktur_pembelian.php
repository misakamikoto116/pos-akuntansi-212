<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHargaFinalAndBagianDiskonMasterOnTableBarangFakturPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('barang_faktur_pembelian', 'harga_final')) {
            Schema::table('barang_faktur_pembelian', function (Blueprint $table) {
                $table->float('harga_final',19,2)->after('unit_price');
            });
        }

        if (!Schema::hasColumn('barang_faktur_pembelian', 'bagian_diskon_master')) {
            Schema::table('barang_faktur_pembelian', function (Blueprint $table) {
                $table->float('bagian_diskon_master',19,2)->after('harga_final');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_faktur_pembelian', function (Blueprint $table) {
            //
        });
    }
}
