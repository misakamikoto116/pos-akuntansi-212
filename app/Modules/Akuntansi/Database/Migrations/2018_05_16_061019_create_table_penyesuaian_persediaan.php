<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePenyesuaianPersediaan extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        if (!Schema::hasTable('penyesuaian_persediaan')) {
            Schema::create('penyesuaian_persediaan', function (Blueprint $table) {
                $table->increments('id');
                $table->string('no_penyesuaian');
                $table->string('tgl_penyesuaian');
                $table->integer('kode_akun_id')->unsigned()->nullable();
                $table->string('akun_penyesuaian')->nullable();
                $table->string('keterangan')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        if (Schema::hasTable('penyesuian_persediaan')) {
            Schema::dropIfExists('penyesuaian_persediaan');
        }
    }
}
