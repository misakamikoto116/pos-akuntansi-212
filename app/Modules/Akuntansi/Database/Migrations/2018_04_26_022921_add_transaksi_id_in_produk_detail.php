<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransaksiIdInProdukDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('produk_detail', 'transaksi_id')) {
            Schema::table('produk_detail', function (Blueprint $table) {
                $table->bigInteger('transaksi_id')->unsigned()->after('gudang_id')->nullable();
                $table->foreign('transaksi_id')
                                ->references('id')
                                ->on('transaksi');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produk_detail', function (Blueprint $table) {
            //
        });
    }
}
