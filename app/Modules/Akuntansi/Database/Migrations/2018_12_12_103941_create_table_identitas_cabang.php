<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIdentitasCabang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identitas_cabang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('identitas_id')->unsigned();
            $table->foreign('identitas_id')
                  ->references('id')
                  ->on('identitas');
            $table->string('kode_cabang');
            $table->string('nama_cabang');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identitas_cabang');
    }
}
