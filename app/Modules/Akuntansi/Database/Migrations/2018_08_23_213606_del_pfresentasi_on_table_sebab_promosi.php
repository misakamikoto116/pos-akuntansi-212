<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DelPfresentasiOnTableSebabPromosi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('sebab_promosi', 'presentase')) {
            Schema::table('sebab_promosi', function (Blueprint $table) {
                $table->dropColumn('presentase');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sebab_promosi', function (Blueprint $table) {
            //
        });
    }
}
