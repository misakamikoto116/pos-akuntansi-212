<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAkunPenyesuaianOnPreferensiMataUang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('preferensi_mata_uang', function (Blueprint $table) {
            $table->integer('akun_penyesuaian_id')->unsigned()->nullable()->after('laba_rugi_tak_terealisir_id');
            $table->foreign('akun_penyesuaian_id')->references('id')->on('akun');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
