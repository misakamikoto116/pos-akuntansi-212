<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableFobShipDateOnTableFakturPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('faktur_penjualan', 'fob')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->dropColumn('fob');
            });
        }

        if (!Schema::hasColumn('faktur_penjualan', 'fob')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->tinyInteger('fob')->after('status_lunas')->nullable();
            });
        }

        if (Schema::hasColumn('faktur_penjualan', 'ship_date')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->dropColumn('ship_date');
            });
        }

        if (!Schema::hasColumn('faktur_penjualan', 'ship_date')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->dateTime('ship_date')->after('ship_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faktur_penjualan', function (Blueprint $table) {
            //
        });
    }
}
