<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryRekonsiliasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('history_rekonsiliasi')) {
            Schema::create('history_rekonsiliasi', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('akun_id')->unsigned();
                $table->foreign('akun_id')
                      ->references('id')
                      ->on('akun')
                      ->onUpdate('cascade');
                $table->float('saldo_rekening_koran',19,2)->nullable();
                $table->float('kalkulasi_saldo',19,2)->nullable();
                $table->float('selisih_saldo',19,2)->nullable();
                $table->dateTime('tanggal_terakhir_rekonsil');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_rekonsiliasi');
    }
}
