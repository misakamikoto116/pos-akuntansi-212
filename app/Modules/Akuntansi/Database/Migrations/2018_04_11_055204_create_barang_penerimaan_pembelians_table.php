<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangPenerimaanPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('barang_penerimaan_pembelian')) {
            Schema::create('barang_penerimaan_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('produk_id')->unsigned();
                $table->foreign('produk_id')
                                ->references('id')
                                ->on('produk');
                $table->integer('jumlah');
                $table->string('satuan');
                $table->integer('gudang_id')->unsigned();
                $table->foreign('gudang_id')
                                ->references('id')
                                ->on('gudang');
                $table->string('sn')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_penerimaan_pembelian');
    }
}
