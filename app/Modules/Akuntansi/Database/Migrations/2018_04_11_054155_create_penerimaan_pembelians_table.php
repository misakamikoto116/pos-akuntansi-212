<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenerimaanPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('penerimaan_pembelian')) {       
            Schema::create('penerimaan_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('pemasok_id')->unsigned();
                $table->foreign('pemasok_id')
                                ->references('id')
                                ->on('informasi_pemasok');
                $table->text('alamat_pengiriman', 255);
                $table->string('form_no')->nullable();
                $table->string('receipt_no')->nullable();
                $table->date('receive_date')->nullable();
                $table->date('ship_date')->nullable();
                $table->tinyInteger('fob')->nullable();
                $table->integer('ship_id')->unsigned();
                $table->foreign('ship_id')
                    ->references('id')
                    ->on('jasa_pengiriman');
                $table->text('keterangan', 255)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penerimaan_pembelian');
    }
}
