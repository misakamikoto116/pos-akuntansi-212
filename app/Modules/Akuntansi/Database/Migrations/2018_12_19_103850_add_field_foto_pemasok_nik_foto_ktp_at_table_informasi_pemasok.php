<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldFotoPemasokNikFotoKtpAtTableInformasiPemasok extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('informasi_pemasok', 'foto_pemasok'))
        {
            Schema::table('informasi_pemasok', function (Blueprint $table) {
                $table->string('foto_pemasok')->after('nama')->nullable();
                $table->string('nik')->after('foto_pemasok');
                $table->string('foto_nik_pemasok')->after('nik')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('informasi_pemasok', function (Blueprint $table) {
            //
        });
    }
}
