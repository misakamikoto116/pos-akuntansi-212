<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTanggalCustomOnTablePreferensiPos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('preferensi_pos', 'tanggal_custom')) {
            Schema::table('preferensi_pos', function (Blueprint $table) {
                $table->tinyInteger('tanggal_custom')->after('status')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preferensi_pos', function (Blueprint $table) {
            //
        });
    }
}
