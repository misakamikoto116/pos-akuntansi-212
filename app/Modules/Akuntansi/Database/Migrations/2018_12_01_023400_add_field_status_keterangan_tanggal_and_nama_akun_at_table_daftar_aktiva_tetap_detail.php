<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldStatusKeteranganTanggalAndNamaAkunAtTableDaftarAktivaTetapDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('daftar_aktiva_tetap_detail', 'nama_beban')) {
            Schema::table('daftar_aktiva_tetap_detail', function (Blueprint $table) {
                $table->string('nama_beban')->nullable()->after('daftar_aktiva_tetap_id');
                $table->string('nama_akumulasi')->nullable()->after('nama_beban');
                $table->dateTime('tgl_jurnal')->nullable()->after('periode');
                $table->string('keterangan')->nullable()->after('tgl_jurnal');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daftar_aktiva_tetap_detail', function (Blueprint $table) {
            //
        });
    }
}
