<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInTaxTaxableOnPesananPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('pesanan_pembelian', 'in_tax')) {
            Schema::table('pesanan_pembelian', function (Blueprint $table) {
                $table->tinyInteger('in_tax')->nullable()->after('fob');
            });
        }

        if (!Schema::hasColumn('pesanan_pembelian', 'taxable')) {
            Schema::table('pesanan_pembelian', function (Blueprint $table) {
                $table->tinyInteger('taxable')->nullable()->after('in_tax');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesanan_pembelian', function (Blueprint $table) {
            //
        });
    }
}
