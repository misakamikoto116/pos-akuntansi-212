<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColHargaModalOnBarangFakturPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('barang_faktur_pembelian', 'harga_modal')) {
            Schema::table('barang_faktur_pembelian', function (Blueprint $table) {
                $table->float('harga_modal',19,2)->after('unit_price');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_faktur_pembelian', function (Blueprint $table) {
            //
        });
    }
}
