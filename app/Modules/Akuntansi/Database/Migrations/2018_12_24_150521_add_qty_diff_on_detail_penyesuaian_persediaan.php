<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQtyDiffOnDetailPenyesuaianPersediaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('detail_penyesuaian_persediaan', 'qty_diff')) {
            Schema::table('detail_penyesuaian_persediaan', function (Blueprint $table) {
                $table->float('qty_diff')->after('qty_baru');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_penyesuaian_persediaan', function (Blueprint $table) {
            //
        });
    }
}
