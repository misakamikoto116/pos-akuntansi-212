<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPelangganKirimAlamatTagihanOnPengirimanPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('pengiriman_penjualan', 'alamat_tagihan')) {
            Schema::table('pengiriman_penjualan', function (Blueprint $table) {
                $table->text('alamat_tagihan')->nullable()->after('alamat_pengiriman');
            });
        }

        if (!Schema::hasColumn('pengiriman_penjualan', 'pelanggan_kirim_ke_id')) {
            Schema::table('pengiriman_penjualan', function (Blueprint $table) {
                $table->integer('pelanggan_kirim_ke_id')->unsigned()->after('pelanggan_id')->nullable();
                $table->foreign('pelanggan_kirim_ke_id')
                      ->references('id')
                      ->on('informasi_pelanggan');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengiriman_penjualan', function (Blueprint $table) {
            //
        });
    }
}
