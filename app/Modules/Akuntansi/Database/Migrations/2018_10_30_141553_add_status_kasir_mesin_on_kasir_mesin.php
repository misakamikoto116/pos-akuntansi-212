<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusKasirMesinOnKasirMesin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('kasir_mesin', 'status')) {
            Schema::table('kasir_mesin', function (Blueprint $table) {
                $table->tinyInteger('status')->after('nama')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kasir_mesin', function (Blueprint $table) {
            //
        });
    }
}
