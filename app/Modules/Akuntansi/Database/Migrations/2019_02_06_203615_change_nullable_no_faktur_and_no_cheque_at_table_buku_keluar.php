<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNullableNoFakturAndNoChequeAtTableBukuKeluar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buku_keluar', function (Blueprint $table) {
            $table->string('no_faktur')->nullable()->change();
            $table->string('no_cek')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buku_keluar', function (Blueprint $table) {
            //
        });
    }
}
