<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNamaSingkatOnTableHargaJual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('harga_jual', 'nama_singkat')) {
            Schema::table('harga_jual', function (Blueprint $table) {
                $table->string('nama_singkat')->after('harga_jual')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('harga_jual', function (Blueprint $table) {
            //
        });
    }
}
