<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColHargaModalOnBarangPenawaranPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('barang_penawaran_penjualan', 'harga_modal')) {
            Schema::table('barang_penawaran_penjualan', function (Blueprint $table) {
                $table->float('harga_modal',19,2)->after('harga');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_penawaran_penjualan', function (Blueprint $table) {
            //
        });
    }
}
