<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNullableOnFakturPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('faktur_penjualan', 'term_id')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
            $table->dropForeign('faktur_penjualan_term_id_foreign');
            $table->dropColumn('term_id');
            });
        }
        if (!Schema::hasColumn('faktur_penjualan', 'term_id')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->integer('term_id')->unsigned()->nullable()->after('fob');
                $table->foreign('term_id')
                    ->references('id')
                    ->on('syarat_pembayaran');
            });
        }


        if (Schema::hasColumn('faktur_penjualan', 'ship_id')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
            $table->dropForeign('faktur_penjualan_ship_id_foreign');
            $table->dropColumn('ship_id');
            });
        }
        if (!Schema::hasColumn('faktur_penjualan', 'ship_id')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->integer('ship_id')->unsigned()->nullable()->after('term_id');
                $table->foreign('ship_id')
                    ->references('id')
                    ->on('jasa_pengiriman');
            });
        }


        if (Schema::hasColumn('faktur_penjualan', 'akun_piutang_id')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
            $table->dropForeign('faktur_penjualan_akun_piutang_id_foreign');
            $table->dropColumn('akun_piutang_id');
            });
        }
        if (!Schema::hasColumn('faktur_penjualan', 'akun_piutang_id')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->integer('akun_piutang_id')->unsigned()->nullable()->after('uang_muka');
                $table->foreign('akun_piutang_id')
                    ->references('id')
                    ->on('akun');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faktur_penjualan', function (Blueprint $table) {
            //
        });
    }
}
