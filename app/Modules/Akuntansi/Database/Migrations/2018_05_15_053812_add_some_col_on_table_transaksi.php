<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeColOnTableTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('transaksi', 'sumber')) {
            Schema::table('transaksi', function (Blueprint $table) {
                $table->text('sumber')->after('status_rekonsiliasi');
            });
        }

        if (!Schema::hasColumn('transaksi', 'keterangan')) {
            Schema::table('transaksi', function (Blueprint $table) {
                $table->text('keterangan')->after('sumber');
            });
        }

        if (!Schema::hasColumn('transaksi', 'no_transaksi')) {
            Schema::table('transaksi', function (Blueprint $table) {
                $table->string('no_transaksi')->after('keterangan');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi', function (Blueprint $table) {
            //
        });
    }
}
