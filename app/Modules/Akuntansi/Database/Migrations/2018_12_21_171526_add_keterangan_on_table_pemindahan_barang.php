<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeteranganOnTablePemindahanBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('pemindahan_barang', 'keterangan')) {
            Schema::table('pemindahan_barang', function (Blueprint $table) {
                $table->text('keterangan')->nullable()->after('alamat_ke_gudang');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pemindahan_barang', function (Blueprint $table) {
            //
        });
    }
}
