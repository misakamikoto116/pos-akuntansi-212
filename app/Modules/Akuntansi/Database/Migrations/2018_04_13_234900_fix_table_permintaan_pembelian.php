<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTablePermintaanPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permintaan_pembelian', function (Blueprint $table) {
            $table->integer('pemasok_id')->unsigned()->after('id');
            $table->foreign('pemasok_id')
            ->references('id')
            ->on('informasi_pemasok')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('permintaan_penjualan', function (Blueprint $table) {
        //     $table->dropColumn('pemasok_id');
        // });
    }
}
