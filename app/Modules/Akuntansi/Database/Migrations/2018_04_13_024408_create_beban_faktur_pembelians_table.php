<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBebanFakturPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('beban_faktur_pembelian')) {       
            Schema::create('beban_faktur_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('akun_beban_id')->unsigned();
                $table->foreign('akun_beban_id')
                    ->references('id')
                    ->on('akun');
                $table->float('amount',19,2);
                $table->text('notes')->nullable();
                $table->tinyInteger('alokasi_ke_barang')->default(0)->comment('0 => false, 1 => false');
                $table->tinyInteger('ke_pemasok_lain')->default(0)->comment('0 => false, 1 => false');
                
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
        
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beban_faktur_pembelian');
    }
}
