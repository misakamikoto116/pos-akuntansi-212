<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColDeletedAtOnTingkatHargaBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('tingkat_harga_barang', 'deleted_at')) {
            Schema::table('tingkat_harga_barang', function (Blueprint $table) {
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('tingkat_harga_barang', 'deleted_at')) {
            Schema::table('tingkat_harga_barang', function (Blueprint $table) {
                $table->dropColumn('deleted_at');
            });
        }
    }
}
