<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSerialNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('serial_number')) {
            Schema::create('serial_number', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('no_pengisian');
                $table->dateTime('tgl_pengisian');
                $table->integer('tipe_transaksi');
                $table->integer('no_transaksi');
                $table->dateTime('tgl_transaksi');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('serial_number')) {
            Schema::dropIfExists('serial_number');
        }
    }
}
