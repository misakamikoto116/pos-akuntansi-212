<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColOnDetailPelanggan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('detail_informasi_pelanggan', 'penjelasan')) {
            Schema::table('detail_informasi_pelanggan', function (Blueprint $table) {
                $table->string('penjelasan')->nullable()->after('id');
                $table->string('no_pesanan')->after('no_faktur');
                $table->dropColumn('pajak');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('detail_informasi_pelanggan', 'penjelasan')) {
            Schema::table('detail_informasi_pelanggan', function (Blueprint $table) {
                $table->dropColumn('penjelasan');
                $table->dropColumn('no_pesanan');
            });
        }
    }
}
