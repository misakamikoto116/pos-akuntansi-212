<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColQtyUseOnBarangPengirimanPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('barang_pengiriman_penjualan', 'qty_used')) {
            Schema::table('barang_pengiriman_penjualan', function (Blueprint $table) {
                $table->dropColumn('qty_used');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_pengiriman_penjualan', function (Blueprint $table) {
            //
        });
    }
}
