<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePemindahanBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pemindahan_barang')){
            Schema::create('pemindahan_barang', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('no_transfer');
                $table->dateTime('tanggal');
                $table->integer('dari_gudang_id')->unsigned();
                $table->integer('ke_gudang_id')->unsigned();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('pemindahan_barang')) {
            Schema::table('pemindahan_barang', function (Blueprint $table){
                Schema::dropIfExists('pemindahan_barang');
            });
        }
    }
}
