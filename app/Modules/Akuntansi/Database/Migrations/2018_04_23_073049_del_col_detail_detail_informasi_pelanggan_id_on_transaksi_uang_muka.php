<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DelColDetailDetailInformasiPelangganIdOnTransaksiUangMuka extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('transaksi_uang_muka', 'detail_informasi_pelanggan_id')) {
            Schema::table('transaksi_uang_muka', function (Blueprint $table) {
                $table->dropForeign('transaksi_uang_muka_detail_informasi_pelanggan_id_foreign');
                $table->dropColumn('detail_informasi_pelanggan_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi_uang_muka', function (Blueprint $table) {
            //
        });
    }
}
