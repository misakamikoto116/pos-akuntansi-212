<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangReturPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('barang_retur_pembelian')) {
            Schema::create('barang_retur_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('produk_id')->unsigned();
                $table->foreign('produk_id')
                ->references('id')
                ->on('produk')
                ->onUpdate('cascade');
                $table->integer('barang_faktur_pembelian_id')->unsigned()->nullable();
                $table->foreign('barang_faktur_pembelian_id')
                ->references('id')
                ->on('barang_faktur_pembelian')
                ->onUpdate('cascade');
                $table->integer('retur_pembelian_id')->unsigned();
                $table->foreign('retur_pembelian_id')
                ->references('id')
                ->on('retur_pembelian')
                ->onUpdate('cascade');
                $table->text('item_deskripsi');
                $table->integer('jumlah');
                $table->float('harga', 19, 2);
                $table->integer('gudang_id')->unsigned();
                $table->foreign('gudang_id')
                ->references('id')
                ->on('gudang')
                ->onUpdate('cascade');
                $table->integer('kode_pajak_id')->unsigned();
                $table->foreign('kode_pajak_id')
                ->references('id')
                ->on('kode_pajak')
                ->onUpdate('cascade');
                $table->string('sn')->nullable();
                $table->tinyInteger('status')->default(0)->comment('0 => Tetap, 1 => Berubah');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
