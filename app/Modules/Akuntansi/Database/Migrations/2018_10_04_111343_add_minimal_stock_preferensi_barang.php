<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinimalStockPreferensiBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('preferensi_barang', 'minimal_stock')) {
            Schema::table('preferensi_barang', function (Blueprint $table) {
                $table->integer('minimal_stock')->default(5)->after('akun_belum_tertagih_id')->nullable();;
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preferensi_barang', function (Blueprint $table) {
            //
        });
    }
}
