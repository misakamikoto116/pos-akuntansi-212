<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreferensiBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('preferensi_barang')) {
            Schema::create('preferensi_barang', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('akun_persediaan_id')->unsigned()->nullable();
                $table->integer('akun_penjualan_id')->unsigned()->nullable();
                $table->integer('akun_retur_penjualan_id')->unsigned()->nullable();
                $table->integer('akun_diskon_barang_id')->unsigned()->nullable();
                $table->integer('akun_barang_terkirim_id')->unsigned()->nullable();
                $table->integer('akun_hpp_id')->unsigned()->nullable();
                $table->integer('akun_retur_pembelian_id')->unsigned()->nullable();
                $table->integer('akun_beban_id')->unsigned()->nullable();
                $table->integer('akun_belum_tertagih_id')->unsigned()->nullable();

                $table->foreign('akun_persediaan_id')->references('id')->on('akun');
                $table->foreign('akun_penjualan_id')->references('id')->on('akun');
                $table->foreign('akun_retur_penjualan_id')->references('id')->on('akun');
                $table->foreign('akun_diskon_barang_id')->references('id')->on('akun');
                $table->foreign('akun_barang_terkirim_id')->references('id')->on('akun');
                $table->foreign('akun_hpp_id')->references('id')->on('akun');
                $table->foreign('akun_retur_pembelian_id')->references('id')->on('akun');
                $table->foreign('akun_beban_id')->references('id')->on('akun');
                $table->foreign('akun_belum_tertagih_id')->references('id')->on('akun');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferensi_barang');
    }
}
