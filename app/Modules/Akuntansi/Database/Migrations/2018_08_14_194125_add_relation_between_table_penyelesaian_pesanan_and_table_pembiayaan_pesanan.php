<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationBetweenTablePenyelesaianPesananAndTablePembiayaanPesanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_penyelesaian_pesanan', function (Blueprint $table) {
            $table->integer('pembiayaan_pesanan_id')->unsigned()->after('id')->nullable();
            $table->foreign('pembiayaan_pesanan_id')->references('id')
                    ->on('pembiayaan_pesanan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penyelesaian_pesanan', function (Blueprint $table) {
            //
        });
    }
}
