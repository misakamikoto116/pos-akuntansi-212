<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPembiayaanPesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pembiayaan_pesanan', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('produk_id')->unsigned();
            $table->foreign('produk_id')
                    ->references('id')
                    ->on('produk');

            $table->integer('pembiayaan_pesanan_id')->unsigned();
            $table->foreign('pembiayaan_pesanan_id')
                    ->references('id')
                    ->on('pembiayaan_pesanan');
            
            $table->datetime('tanggal');
            $table->integer('qty'); 
            $table->string('unit');
            $table->float('cost', 19,2);
            $table->string('sn')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pembiayaan_pesanan');
    }
}
