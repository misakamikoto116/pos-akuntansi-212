<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditDateOnPenerimaanPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('penerimaan_penjualan', 'cheque_date')) {
            Schema::table('penerimaan_penjualan', function (Blueprint $table) {
                $table->dropColumn('cheque_date');
            });
        }
        if (Schema::hasColumn('penerimaan_penjualan', 'payment_date')) {
            Schema::table('penerimaan_penjualan', function (Blueprint $table) {
                $table->dropColumn('payment_date');
            });
        }
        if (!Schema::hasColumn('penerimaan_penjualan', 'cheque_date')) {
            Schema::table('penerimaan_penjualan', function (Blueprint $table) {
                $table->dateTime('cheque_date')->after('cheque_no');
            });
        }
        if (!Schema::hasColumn('penerimaan_penjualan', 'payment_date')) {
            Schema::table('penerimaan_penjualan', function (Blueprint $table) {
                $table->dateTime('payment_date')->after('form_no');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penerimaan_penjualan', function (Blueprint $table) {
            //
        });
    }
}
