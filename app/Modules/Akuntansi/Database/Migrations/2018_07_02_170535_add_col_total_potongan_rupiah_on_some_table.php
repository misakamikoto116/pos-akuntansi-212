<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColTotalPotonganRupiahOnSomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Penjualan
        $data_penjualan = [
            'penawaran_penjualan',
            'pesanan_penjualan',
            'pesanan_pembelian',
        ];

        foreach($data_penjualan as $tb){
            if (!Schema::hasColumn($tb, 'total_potongan_rupiah')) {
                Schema::table($tb, function (Blueprint $table) {
                    $table->float('total_potongan_rupiah',19, 2)->nullable()->after('diskon');
                });
            }
        }

        if (!Schema::hasColumn('faktur_penjualan', 'jumlah_diskon_faktur')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->float('jumlah_diskon_faktur',19, 2)->nullable()->after('diskon');
            });
        }

        if (!Schema::hasColumn('retur_penjualan', 'jumlah_diskon_retur')) {
            Schema::table('retur_penjualan', function (Blueprint $table) {
                $table->float('jumlah_diskon_retur',19, 2)->nullable()->after('diskon');
            });
        }

        if (!Schema::hasColumn('faktur_pembelian', 'total_diskon_faktur')) {
            Schema::table('faktur_pembelian', function (Blueprint $table) {
                $table->float('total_diskon_faktur',19, 2)->nullable()->after('diskon');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
