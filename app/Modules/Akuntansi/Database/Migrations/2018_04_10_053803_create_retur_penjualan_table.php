<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturPenjualanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retur_penjualan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sr_no');
            $table->date('tanggal');
            $table->integer('pelanggan_id')->unsigned();
                $table->foreign('pelanggan_id')
                ->references('id')
                ->on('informasi_pelanggan')
                ->onUpdate('cascade');
            $table->integer('faktur_penjualan_id')->unsigned();
                $table->foreign('faktur_penjualan_id')
                ->references('id')
                ->on('faktur_penjualan')
                ->onUpdate('cascade');
            $table->text('keterangan');
            $table->double('diskon');
            $table->float('total',19,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retur_penjualan');
    }
}
