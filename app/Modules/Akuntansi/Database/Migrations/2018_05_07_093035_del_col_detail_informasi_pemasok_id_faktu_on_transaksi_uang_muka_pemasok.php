<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DelColDetailInformasiPemasokIdFaktuOnTransaksiUangMukaPemasok extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('transaksi_uang_muka_pemasok', 'faktur_uang_muka_pemasok_id')) {
            Schema::table('transaksi_uang_muka_pemasok', function (Blueprint $table) {
                $table->integer('faktur_uang_muka_pemasok_id')->unsigned()->after('id');
                $table->foreign('faktur_uang_muka_pemasok_id')
                                ->references('id')
                                ->on('faktur_pembelian');
            });
        }
        if (Schema::hasColumn('transaksi_uang_muka_pemasok', 'detail_informasi_pemasok_id')) {
            Schema::table('transaksi_uang_muka_pemasok', function (Blueprint $table) {
                $table->dropForeign('transaksi_uang_muka_pemasok_detail_informasi_pemasok_id_foreign');
                $table->dropColumn('detail_informasi_pemasok_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi_uang_muka_pemasok', function (Blueprint $table) {
            //
        });
    }
}
