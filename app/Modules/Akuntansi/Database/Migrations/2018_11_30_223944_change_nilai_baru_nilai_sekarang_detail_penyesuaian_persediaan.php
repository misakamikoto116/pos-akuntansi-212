<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNilaiBaruNilaiSekarangDetailPenyesuaianPersediaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('detail_penyesuaian_persediaan', 'nilai_skrg')) {
            Schema::table('detail_penyesuaian_persediaan', function (Blueprint $table) {
                $table->dropColumn('nilai_skrg');
            });
        }

        if (Schema::hasColumn('detail_penyesuaian_persediaan', 'nilai_baru')) {
            Schema::table('detail_penyesuaian_persediaan', function (Blueprint $table) {
                $table->dropColumn('nilai_baru');
            });
        }

        if (!Schema::hasColumn('detail_penyesuaian_persediaan', 'nilai_skrg')) {
            Schema::table('detail_penyesuaian_persediaan', function (Blueprint $table) {
                $table->float('nilai_skrg',19,2)->nullable()->after('qty_baru');
            });
        }

        if (!Schema::hasColumn('detail_penyesuaian_persediaan', 'nilai_baru')) {
            Schema::table('detail_penyesuaian_persediaan', function (Blueprint $table) {
                $table->float('nilai_baru',19,2)->nullable()->after('nilai_skrg');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_penyesuaian_persediaan', function (Blueprint $table) {
            //
        });
    }
}
