<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColFakturUangMukaPelangganIdOnTransaksiUangMuka extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('transaksi_uang_muka', 'faktur_uang_muka_pelanggan_id')) {
            Schema::table('transaksi_uang_muka', function (Blueprint $table) {
                $table->integer('faktur_uang_muka_pelanggan_id')->unsigned()->after('jumlah')->nullable();
                $table->foreign('faktur_uang_muka_pelanggan_id')
                    ->references('id')
                    ->on('faktur_penjualan');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi_uang_muka', function (Blueprint $table) {
            //
        });
    }
}
