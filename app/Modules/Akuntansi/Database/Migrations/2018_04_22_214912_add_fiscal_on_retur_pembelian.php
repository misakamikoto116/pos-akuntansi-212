<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiscalOnReturPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('retur_pembelian', 'no_fiscal')) {
            Schema::table('retur_pembelian', function (Blueprint $table) {
                $table->string('no_fiscal')->nullable()->after('tanggal');
                $table->datetime('tgl_fiscal')->nullable()->after('no_fiscal');
                $table->tinyInteger('taxable')->nullable()->after('keterangan');
                $table->tinyInteger('in_tax')->nullable()->after('taxable');
                $table->double('nilai_tukar')->nullable()->after('in_tax');
                $table->double('nilai_tukar_pajak')->nullable()->after('nilai_tukar');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
