<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableShipIdOnTablePenerimaanPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('penerimaan_pembelian', 'ship_id')) {
            Schema::table('penerimaan_pembelian', function (Blueprint $table) {
                $table->dropForeign('penerimaan_pembelian_ship_id_foreign');
                $table->dropColumn('ship_id');
            });
        }

        if (!Schema::hasColumn('penerimaan_pembelian', 'ship_id')) {
            Schema::table('penerimaan_pembelian', function (Blueprint $table) {
                $table->integer('ship_id')->unsigned()->nullable()->after('fob');
                $table->foreign('ship_id')
                      ->references('id')
                      ->on('jasa_pengiriman')
                      ->onUpdate('cascade');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penerimaan_pembelian', function (Blueprint $table) {
            //
        });
    }
}
