<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullableFpStdFieldsFakturPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('faktur_penjualan', 'no_fp_std')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->string('no_fp_std')->nullable()->change();
            });
        }

        if (Schema::hasColumn('faktur_penjualan', 'no_fp_std2')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->string('no_fp_std2')->nullable()->change();
            });
        }

        if (Schema::hasColumn('faktur_penjualan', 'no_fp_std_date')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->dateTime('no_fp_std_date')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faktur_penjualan', function (Blueprint $table) {
            //
        });
    }
}
