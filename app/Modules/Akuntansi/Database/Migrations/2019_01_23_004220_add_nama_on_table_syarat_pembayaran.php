<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNamaOnTableSyaratPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('syarat_pembayaran', 'nama')) {
            Schema::table('syarat_pembayaran', function (Blueprint $table) {
                $table->string('nama')->nullable()->after('id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('syarat_pembayaran', function (Blueprint $table) {
            //
        });
    }
}
