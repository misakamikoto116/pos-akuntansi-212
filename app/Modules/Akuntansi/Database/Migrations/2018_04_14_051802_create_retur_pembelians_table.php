<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('retur_pembelian')) {   
            Schema::create('retur_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->string('return_no');
                $table->date('tanggal');
                $table->integer('pemasok_id')->unsigned();
                $table->foreign('pemasok_id')
                ->references('id')
                ->on('informasi_pemasok')
                ->onUpdate('cascade');
                $table->integer('faktur_pembelian_id')->unsigned();
                $table->foreign('faktur_pembelian_id')
                ->references('id')
                ->on('faktur_pembelian')
                ->onUpdate('cascade');
                $table->text('keterangan');
                $table->double('diskon');
                $table->float('total', 19, 2);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('retur_pembelian');
    }
}
