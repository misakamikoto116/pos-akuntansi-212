<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailHistoryRekonsiliasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('detail_history_rekonsiliasi')) {
            Schema::create('detail_history_rekonsiliasi', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('history_rekonsiliasi_id')->unsigned();
                $table->foreign('history_rekonsiliasi_id')
                      ->references('id')
                      ->on('history_rekonsiliasi')
                      ->onUpdate('cascade');
                $table->bigInteger('transaksi_id')->unsigned();
                $table->foreign('transaksi_id')
                      ->references('id')
                      ->on('transaksi')
                      ->onUpdate('cascade');
                $table->dateTime('detail_tanggal_terakhir_rekonsil');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_history_rekonsiliasi');
    }
}
