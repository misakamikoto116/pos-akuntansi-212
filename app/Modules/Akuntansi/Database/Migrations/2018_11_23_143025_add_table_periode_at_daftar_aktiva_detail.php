<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePeriodeAtDaftarAktivaDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('daftar_aktiva_tetap_detail', 'periode')) {
            Schema::table('daftar_aktiva_tetap_detail', function (Blueprint $table) {
                $table->string('periode')->after('daftar_aktiva_tetap_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daftar_aktiva_tetap_detail', function (Blueprint $table) {
            //
        });
    }
}
