<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelDataKaryawan extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('data_karyawan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nik_karyawan')->unique();
            $table->integer('nip_karyawan')->unique();
            $table->string('nama_karyawan');
            $table->string('jenis_kelamin');
            $table->text('tempat_lahir');
            $table->date('tgl_lahir');
            $table->text('alamat_karyawan');
            $table->string('status');
            $table->string('no_telp');
            $table->string('jabatan');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('data_karyawan');
    }
}
