<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableColBarangPesananPenjualanIdOnBarangPengirimanPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('barang_pengiriman_penjualan', 'barang_pesanan_penjualan_id')) {
            Schema::table('barang_pengiriman_penjualan', function (Blueprint $table) {
            $table->dropForeign('barang_pengiriman_penjualan_barang_pesanan_penjualan_id_foreign');
            $table->dropColumn('barang_pesanan_penjualan_id');
            });
        }
        if (!Schema::hasColumn('barang_pengiriman_penjualan', 'barang_pesanan_penjualan_id')) {
            Schema::table('barang_pengiriman_penjualan', function (Blueprint $table) {
                $table->integer('barang_pesanan_penjualan_id')->unsigned()->nullable()->after('pengiriman_penjualan_id');
                $table->foreign('barang_pesanan_penjualan_id')
                    ->references('id')
                    ->on('barang_pesanan_penjualan');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_pengiriman_penjualan', function (Blueprint $table) {
            //
        });
    }
}
