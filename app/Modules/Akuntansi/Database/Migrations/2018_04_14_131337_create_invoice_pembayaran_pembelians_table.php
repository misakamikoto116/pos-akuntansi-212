<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicePembayaranPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('invoice_pembayaran_pembelian')) {
            Schema::create('invoice_pembayaran_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('faktur_id')->unsigned();
                $table->foreign('faktur_id')
                    ->references('id')
                    ->on('faktur_pembelian');
                $table->integer('pembayaran_pembelian_id')->unsigned();
                $table->foreign('pembayaran_pembelian_id')
                    ->references('id')
                    ->on('pembayaran_pembelian');
                $table->float('payment_amount', 19, 2);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_pembayaran_pembelian');
    }
}
