<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAkunUangMukaOnFakturPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('faktur_pembelian', 'akun_uang_muka_id')) {
            Schema::table('faktur_pembelian', function (Blueprint $table) {
                $table->integer('akun_uang_muka_id')->nullable()->unsigned()->after('akun_hutang_id');
                $table->foreign('akun_uang_muka_id')
                        ->references('id')
                        ->on('akun');
            });
        }   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
