<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewAmountOnDetailPenyesuaianPersediaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_penyesuaian_persediaan', function (Blueprint $table) {
            $table->float('nilai_skrg', 19,2)->nullable()->after('qty_baru');
            $table->float('nilai_baru', 19,2)->nullable()->after('nilai_skrg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
