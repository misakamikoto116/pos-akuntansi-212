<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DelColPemasokIdOnTablePermintaanPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('permintaan_pembelian', 'pemasok_id')) {
            Schema::table('permintaan_pembelian', function (Blueprint $table) {
                $table->dropForeign('permintaan_pembelian_pemasok_id_foreign');
                $table->dropColumn('pemasok_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permintaan_pembelian', function (Blueprint $table) {
            //
        });
    }
}
