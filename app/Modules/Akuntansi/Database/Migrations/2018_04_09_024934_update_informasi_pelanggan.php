<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInformasiPelanggan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('detail_informasi_pelanggan', 'status')) {
            Schema::table('detail_informasi_pelanggan', function (Blueprint $table) {
                $table->tinyInteger('status')->default(0)->after('saldo_awal');
                // 0 = DP
                // 1 = SALDO AWAL
            });   
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('detail_informasi_pelanggan', 'status')) {
            Schema::table('detail_informasi_pelanggan', function (Blueprint $table) {
                $table->dropColumn('status');                    
            });   
        }
    }
}
