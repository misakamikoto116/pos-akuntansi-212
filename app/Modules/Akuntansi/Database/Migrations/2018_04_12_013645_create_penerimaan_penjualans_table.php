<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenerimaanPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('penerimaan_penjualan')) {   
            Schema::create('penerimaan_penjualan', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('pelanggan_id')->unsigned();
                $table->foreign('pelanggan_id')
                                        ->references('id')
                                        ->on('informasi_pelanggan');
                $table->integer('akun_bank_id')->unsigned();
                $table->foreign('akun_bank_id')
                                        ->references('id')
                                        ->on('akun');
                $table->float('rate',19,2);
                $table->string('cheque_no');
                $table->date('cheque_date');
                $table->text('memo')->nullable();
                $table->string('form_no');
                $table->date('payment_date');
                $table->tinyInteger('kosong')->default(0)->comment('0 => false, 1 =>true');
                $table->tinyInteger('fiscal_payment')->nullable()->comment('0 => false, 1 =>true');
                $table->float('cheque_amount',19,2);
                $table->float('existing_credit',19,2)->nullable();
                $table->float('distribute_amount',19,2);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penerimaan_penjualan');
    }
}
