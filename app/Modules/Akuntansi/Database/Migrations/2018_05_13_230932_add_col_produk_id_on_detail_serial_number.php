<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColProdukIdOnDetailSerialNumber extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        if (!Schema::hasColumn('detail_serial_number', 'produk_id')) {
            Schema::table('detail_serial_number', function (Blueprint $table) {
                $table->integer('produk_id')->unsigned()->after('serial_number_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('detail_serial_number', function (Blueprint $table) {
        });
    }
}
