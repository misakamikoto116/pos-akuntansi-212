<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBantuanPengirimenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bantu_pengiriman_penjualan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brg_pengiriman_penjualan_id')->unsigned();
            $table->foreign('brg_pengiriman_penjualan_id')
                    ->references('id')
                    ->on('barang_pengiriman_penjualan');
            
            $table->integer('bnt_penerimaan_pembelian_id')->unsigned();
            $table->foreign('bnt_penerimaan_pembelian_id')
                    ->references('id')
                    ->on('bantu_penerimaan_pembelian');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bantu_pengiriman_penjualan');
    }
}
