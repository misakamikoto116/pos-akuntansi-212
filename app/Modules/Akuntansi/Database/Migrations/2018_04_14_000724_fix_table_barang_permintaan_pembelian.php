<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTableBarangPermintaanPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barang_permintaan_pembelian', function (Blueprint $table) {
            $table->double('unit_price')->after('item_unit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_permintaan_pembelian', function (Blueprint $table) {
            $table->dropColumn('unit_price');
        });
    }
}
