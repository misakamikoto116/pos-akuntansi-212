<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColBarangPesananPembelianIdOnBarangPenerimaanPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('barang_penerimaan_pembelian', 'barang_pesanan_pembelian_id')) {
            Schema::table('barang_penerimaan_pembelian', function (Blueprint $table) {
                $table->integer('barang_pesanan_pembelian_id')->unsigned()->after('produk_id')->nullable();
                $table->foreign('barang_pesanan_pembelian_id')
                                ->references('id')
                                ->on('barang_pesanan_pembelian');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_penerimaan_pembelian', function (Blueprint $table) {
            //
        });
    }
}
