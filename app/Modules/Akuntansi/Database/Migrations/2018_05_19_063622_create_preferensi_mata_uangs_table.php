<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreferensiMataUangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferensi_mata_uang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('akun_hutang_id')->unsigned()->nullable();
            $table->integer('akun_piutang_id')->unsigned()->nullable();
            $table->integer('uang_muka_pembelian_id')->unsigned()->nullable();
            $table->integer('uang_muka_penjualan_id')->unsigned()->nullable();
            $table->integer('diskon_penjualan_id')->unsigned()->nullable();
            $table->integer('laba_rugi_terealisir_id')->unsigned()->nullable();
            $table->integer('laba_rugi_tak_terealisir_id')->unsigned()->nullable();
            
            $table->foreign('akun_hutang_id')->references('id')->on('akun');
            $table->foreign('akun_piutang_id')->references('id')->on('akun');
            $table->foreign('uang_muka_pembelian_id')->references('id')->on('akun');
            $table->foreign('uang_muka_penjualan_id')->references('id')->on('akun');
            $table->foreign('diskon_penjualan_id')->references('id')->on('akun');
            $table->foreign('laba_rugi_terealisir_id')->references('id')->on('akun');
            $table->foreign('laba_rugi_tak_terealisir_id')->references('id')->on('akun');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferensi_mata_uang');
    }
}
