<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTutupBukuOnPenjualanDanPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
                    // Master Penjualan
                    'penawaran_penjualan',
                    'pesanan_penjualan',
                    'pengiriman_penjualan',
                    'faktur_penjualan',
                    'penerimaan_penjualan',
                    'retur_penjualan',
                    // Master Pembelian
                    'permintaan_pembelian',
                    'pesanan_pembelian',
                    'penerimaan_pembelian',
                    'faktur_pembelian',
                    'pembayaran_pembelian',
                    'retur_pembelian',
                    // Jurnal Umum
                    'jurnal_umum',
                    'daftar_aktiva_tetap',
        ];

        foreach ($data as $tb) {
            if (!Schema::hasColumn($tb, 'tutup_buku')) {
                Schema::table($tb, function (Blueprint $table) {
                    $table->tinyInteger('tutup_buku')->comment('0 = Belum Tutup Buku, 1 = Sudah Tutup Buku')->default(0);
                });
            }

            if (!Schema::hasColumn($tb, 'tanggal_tutup_buku')) {
                Schema::table($tb, function (Blueprint $table) {
                    $table->dateTime('tanggal_tutup_buku')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
