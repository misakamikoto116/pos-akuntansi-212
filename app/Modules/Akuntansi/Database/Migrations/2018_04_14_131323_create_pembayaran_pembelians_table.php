<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaranPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pembayaran_pembelian')) {
            Schema::create('pembayaran_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('pemasok_id')->unsigned();
                $table->foreign('pemasok_id')
                    ->references('id')
                    ->on('informasi_pemasok');
                $table->integer('akun_bank_id')->unsigned();
                $table->foreign('akun_bank_id')
                    ->references('id')
                    ->on('akun');
                $table->float('rate', 19, 2);
                $table->text('memo')->nullable();
                $table->string('form_no');
                $table->date('payment_date');
                $table->tinyInteger('void_cheque')->default(0)->comment('0 => false, 1 =>true');
                $table->tinyInteger('fiscal_payment')->nullable()->comment('0 => false, 1 =>true');
                $table->float('cheque_amount', 19, 2);
                $table->string('cheque_no');
                $table->dateTime('cheque_date');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayaran_pembelians');
    }
}
