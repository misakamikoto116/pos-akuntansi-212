<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColPajakTotalPajakOnTransaksiUangMukaPemasok extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('transaksi_uang_muka_pemasok', 'pajak_satu_id')) {
            Schema::table('transaksi_uang_muka_pemasok', function (Blueprint $table) {
                $table->integer('pajak_satu_id')->unsigned()->after('jumlah')->nullable();
                $table->foreign('pajak_satu_id')
                      ->references('id')
                      ->on('kode_pajak');
            });
        }

        if (!Schema::hasColumn('transaksi_uang_muka_pemasok', 'pajak_dua_id')) {
            Schema::table('transaksi_uang_muka_pemasok', function (Blueprint $table) {
                $table->integer('pajak_dua_id')->unsigned()->after('pajak_satu_id')->nullable();
                $table->foreign('pajak_dua_id')
                      ->references('id')
                      ->on('kode_pajak');
            });
        }
        if (!Schema::hasColumn('transaksi_uang_muka_pemasok', 'total_pajak1')) {
            Schema::table('transaksi_uang_muka_pemasok', function (Blueprint $table) {
                $table->float('total_pajak1',19,2)->after('pajak_dua_id')->nullable();
            });
        }

        if (!Schema::hasColumn('transaksi_uang_muka_pemasok', 'total_pajak2')) {
            Schema::table('transaksi_uang_muka_pemasok', function (Blueprint $table) {
                $table->float('total_pajak2',19,2)->after('total_pajak1')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi_uang_muka_pemasok', function (Blueprint $table) {
            //
        });
    }
}
