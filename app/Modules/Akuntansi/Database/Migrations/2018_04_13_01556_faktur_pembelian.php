<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FakturPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('faktur_pembelian')) {
            Schema::create('faktur_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->string('no_faktur');
                $table->integer('pemasok_id')->unsigned();
                $table->foreign('pemasok_id')
                    ->references('id')
                    ->on('informasi_pemasok');
                $table->tinyInteger('taxable')->nullable();
                $table->tinyInteger('in_tax')->nullable();
                $table->integer('diskon')->nullable();

                $table->string('form_no');
                $table->string('invoice_no');
                $table->datetime('invoice_date');
                $table->tinyInteger('fob');

                /** 
                 * 0 = Shopping Point
                 * 1 = Destination
                 */

                $table->integer('term_id')->unsigned();
                $table->integer('ship_id')->unsigned();
                $table->string('no_fp_std');
                $table->string('no_fp_std2');
                $table->datetime('no_fp_std_date');
                
                $table->foreign('ship_id')
                ->references('id')
                ->on('jasa_pengiriman')
                ->onUpdate('cascade');
                
                $table->foreign('term_id')
                ->references('id')
                ->on('syarat_pembayaran')
                ->onUpdate('cascade');
                $table->integer('bebankan_pemasok_id')->unsigned()->nullable();
                $table->foreign('bebankan_pemasok_id')
                    ->references('id')
                    ->on('informasi_pemasok')
                    ->onUpdate('cascade');

                $table->float('ongkir', 19, 2)->nullable();
                $table->float('total', 19, 2)->nullable();
                $table->text('keterangan', 255)->nullable();
                $table->text('catatan', 255)->nullable();
                $table->integer('akun_hutang_id')->unsigned();
                $table->foreign('akun_hutang_id')
                    ->references('id')
                    ->on('akun');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
