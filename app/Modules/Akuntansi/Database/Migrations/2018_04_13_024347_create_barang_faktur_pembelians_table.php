<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangFakturPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('barang_faktur_pembelian')) {   
            Schema::create('barang_faktur_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('produk_id')->unsigned();
                $table->foreign('produk_id')
                ->references('id')
                ->on('produk');
                $table->integer('faktur_pembelian_id')->unsigned();
                $table->foreign('faktur_pembelian_id')
                ->references('id')
                ->on('faktur_pembelian');
                $table->integer('barang_penerimaan_pembelian_id')->unsigned()->nullable();
                $table->foreign('barang_penerimaan_pembelian_id')
                ->references('id')
                ->on('barang_penerimaan_pembelian');
                $table->string('item_deskripsi');
                $table->string('item_unit')->nullable();
                $table->integer('jumlah');
                $table->float('unit_price',19,2);
                $table->float('diskon',19,2);
                $table->integer('kode_pajak_id')->unsigned()->nullable();
                $table->foreign('kode_pajak_id')
                ->references('id')
                ->on('kode_pajak');
                $table->integer('gudang_id')->unsigned()->nullable();
                $table->foreign('gudang_id')
                ->references('id')
                ->on('gudang');
                $table->float('sn')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('barang_faktur_pembelian');
    }
}
