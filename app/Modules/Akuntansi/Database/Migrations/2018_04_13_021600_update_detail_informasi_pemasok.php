<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDetailInformasiPemasok extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('detail_informasi_pemasok', 'pajak')) {
            Schema::table('detail_informasi_pemasok', function (Blueprint $table) {
                $table->tinyInteger('status')->default(0)->after('saldo_awal')->comment('0 => saldo awal, 1 => uang muka');
                /** 0 = Belum Cetak, 1 = Cetak */
                $table->float('pajak',19,2)->after('status');
                $table->tinyInteger('in_tax')->after('pajak');
                $table->tinyInteger('taxable')->after('in_tax');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
