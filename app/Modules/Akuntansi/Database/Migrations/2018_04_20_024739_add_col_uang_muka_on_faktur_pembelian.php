<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColUangMukaOnFakturPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('faktur_pembelian', 'uang_muka')) {
            Schema::table('faktur_pembelian', function (Blueprint $table) {
                $table->tinyInteger('uang_muka')->default(0)->after('catatan')->comment('0 => Bukan Uang Muka, 1 => Uang Muka');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faktur_pembelian', function (Blueprint $table) {
            //
        });
    }
}
