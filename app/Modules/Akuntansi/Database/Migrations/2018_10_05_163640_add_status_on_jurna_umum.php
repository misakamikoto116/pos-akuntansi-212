<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusOnJurnaUmum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('jurnal_umum', 'status')) {
            Schema::table('jurnal_umum', function (Blueprint $table) {
                $table->tinyInteger('status')->after('tanggal')->nullable()->comment('0 = saldo awal, 1 = data transaksi');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jurnal_umum', function (Blueprint $table) {
            //
        });
    }
}
