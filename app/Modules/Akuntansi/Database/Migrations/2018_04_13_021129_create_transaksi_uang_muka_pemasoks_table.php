<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiUangMukaPemasoksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('transaksi_uang_muka_pemasok')) {
            Schema::create('transaksi_uang_muka_pemasok', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('detail_informasi_pemasok_id')->unsigned();
                $table->foreign('detail_informasi_pemasok_id')
                ->references('id')
                ->on('detail_informasi_pemasok')
                ->onUpdate('cascade');
                $table->integer('faktur_pembelian_id')->unsigned();
                $table->foreign('faktur_pembelian_id')
                ->references('id')
                ->on('faktur_pembelian')
                ->onUpdate('cascade');
                $table->float('jumlah', 19, 2);
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_uang_muka_pemasok');
    }
}
