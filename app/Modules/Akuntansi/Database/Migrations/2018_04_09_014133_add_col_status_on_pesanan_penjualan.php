<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColStatusOnPesananPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('pesanan_penjualan', 'status')) {
            Schema::table('pesanan_penjualan', function (Blueprint $table) {
                $table->integer('status')->default(0)->after('total');
            });   
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesanan_penjualan', function (Blueprint $table) {
            $table->dropColumn('status');                        
        });
    }
}
