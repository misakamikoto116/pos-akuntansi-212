<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAkunPenyelesaianPesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akun_penyelesaian_pesanan', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('penyelesaian_pesanan_id')->unsigned();
            $table->foreign('penyelesaian_pesanan_id')
                    ->references('id')
                    ->on('penyelesaian_pesanan');

            $table->integer('detail_akun_id')->unsigned();
            $table->foreign('detail_akun_id')
                    ->references('id')
                    ->on('akun');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akun_penyelesaian_pesanan');
    }
}
