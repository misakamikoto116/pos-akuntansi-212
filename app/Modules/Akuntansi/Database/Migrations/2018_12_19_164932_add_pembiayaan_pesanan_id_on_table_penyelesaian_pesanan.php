<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPembiayaanPesananIdOnTablePenyelesaianPesanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('penyelesaian_pesanan', 'pembiayaan_pesanan_id')) {
            Schema::table('penyelesaian_pesanan', function (Blueprint $table) {
                $table->integer('pembiayaan_pesanan_id')->unsigned()->after('id');
                $table->foreign('pembiayaan_pesanan_id')
                        ->references('id')
                        ->on('penyelesaian_pesanan');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penyelesaian_pesanan', function (Blueprint $table) {
            //
        });
    }
}
