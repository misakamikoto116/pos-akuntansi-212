<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HapusNotNullChequeNoOnTablePembayaranPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('pembayaran_pembelian', 'cheque_no')) {
            Schema::table('pembayaran_pembelian', function (Blueprint $table) {
                $table->dropColumn('cheque_no');
            });
        }
        if (!Schema::hasColumn('pembayaran_pembelian', 'cheque_no')) {
            Schema::table('pembayaran_pembelian', function (Blueprint $table) {
                $table->string('cheque_no')->after('cheque_amount')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pembayaran_pembelian', function (Blueprint $table) {
            //
        });
    }
}
