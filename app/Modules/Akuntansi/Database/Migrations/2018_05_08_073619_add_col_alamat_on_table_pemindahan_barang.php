<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColAlamatOnTablePemindahanBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('pemindahan_barang', 'alamat_dari_gudang' && 'alamat_ke_gudang')) {
            Schema::table('pemindahan_barang', function (Blueprint $table) {
                $table->string('alamat_dari_gudang')->after('dari_gudang_id');
                $table->string('alamat_ke_gudang')->after('ke_gudang_id');
            });   
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pemindahan_barang', function (Blueprint $table) {
            $table->dropColumn('alamat_dari_gudang');
            $table->dropColumn('alamat_ke_gudang');
        });
    }
}
