<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembiayaanPesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembiayaan_pesanan', function (Blueprint $table) {
            $table->increments('id');

            $table->string('batch_no');
            $table->datetime('date');

            $table->integer('akun_pembiayaan_pesanan_id')->unsigned();
            $table->foreign('akun_pembiayaan_pesanan_id')
                    ->references('id')
                    ->on('akun');

            $table->text('deskripsi')->nullable(); 

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembiayaan_pesanan');
    }
}
