<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAkunOngkirOnPesananPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('pesanan_pembelian', 'akun_ongkir_id')) {
            Schema::table('pesanan_pembelian', function (Blueprint $table) {
                $table->integer('akun_ongkir_id')->nullable()->after('akun_dp_id')->unsigned();
                $table->foreign('akun_ongkir_id')
                      ->references('id')
                      ->on('akun');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
