<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShipDateOnFakturPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('faktur_penjualan', 'ship_date')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->tinyInteger('status')->default(0)->after('invoice_date');
                /** 0 = Belum Cetak, 1 = Cetak */
                $table->datetime('ship_date')->after('ship_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('faktur_penjualan', 'ship_date')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->dropColumn('status');
                $table->dropColumn('ship_date');
            });
        }
    }
}
