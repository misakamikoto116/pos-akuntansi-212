<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableFobOnTableFakturPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('faktur_pembelian', 'fob')) {
            Schema::table('faktur_pembelian', function (Blueprint $table) {
                $table->dropColumn('fob');
            });
        }

        if (!Schema::hasColumn('faktur_pembelian', 'fob')) {
            Schema::table('faktur_pembelian', function (Blueprint $table) {
                $table->tinyInteger('fob')->after('ship_date')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faktur_pembelian', function (Blueprint $table) {
            //
        });
    }
}
