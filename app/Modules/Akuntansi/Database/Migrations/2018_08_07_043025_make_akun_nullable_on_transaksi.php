<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeAkunNullableOnTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksi', function (Blueprint $table) {
            $table->dropForeign('transaksi_akun_id_foreign');
            $table->dropColumn('akun_id');
        }); 

        Schema::table('transaksi', function (Blueprint $table){
            $table->integer('akun_id')->after('tanggal')->nullable()->unsigned();
            $table->foreign('akun_id')
                ->references('id')
                ->on('akun');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
