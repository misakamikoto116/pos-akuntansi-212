<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogoPerusahaanOnIdentitas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('identitas', 'logo_perusahaan')) {
            Schema::table('identitas', function (Blueprint $table) {
                $table->text('logo_perusahaan')->nullable()->after('footer_struk');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('identitas', function (Blueprint $table) {
            //
        });
    }
}
