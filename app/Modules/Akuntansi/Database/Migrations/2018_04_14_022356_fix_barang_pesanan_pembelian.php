<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixBarangPesananPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barang_pesanan_pembelian', function (Blueprint $table) {
            $table->dropForeign(['barang_permintaan_pembelian_id']);
            $table->dropColumn('item_unit');

            $table->integer('barang_permintaan_pembelian_id')->nullable()->unsigned()->change();
            $table->foreign('barang_permintaan_pembelian_id')
                                ->references('id')
                                ->on('barang_permintaan_pembelian');
            $table->string('satuan')->after('barang_permintaan_pembelian_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
