<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicePenerimaanPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('invoice_penerimaan_penjualan')) {   
            Schema::create('invoice_penerimaan_penjualan', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('faktur_id')->unsigned();
                $table->foreign('faktur_id')
                                ->references('id')
                                ->on('faktur_penjualan');
                $table->float('payment_amount',19,2);
                $table->float('diskon',19,2)->nullable();
                $table->date('diskon_date')->nullable();
                $table->tinyInteger('status_pay')->comment('0 => false , 1 => true');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_penerimaan_penjualan');
    }
}
