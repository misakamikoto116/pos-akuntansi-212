<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColNoPesananOnPengirimanPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('pengiriman_penjualan', 'no_pesanan')) {
            Schema::table('pengiriman_penjualan', function (Blueprint $table) {
                $table->dropColumn('no_pesanan');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengiriman_penjualan', function (Blueprint $table) {
            //
        });
    }
}
