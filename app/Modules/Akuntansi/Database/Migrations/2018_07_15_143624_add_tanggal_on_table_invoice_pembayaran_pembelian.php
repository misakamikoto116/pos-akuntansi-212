<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTanggalOnTableInvoicePembayaranPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('invoice_pembayaran_pembelian', 'tanggal')) {
            Schema::table('invoice_pembayaran_pembelian', function (Blueprint $table) {
                $table->dateTime('tanggal')->after('last_owing');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_pembayaran_pembelian', function (Blueprint $table) {
            //
        });
    }
}
