<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DelColUnitPriceOnTableBarangPermintaanPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('barang_permintaan_pembelian', 'unit_price')) {
            Schema::table('barang_permintaan_pembelian', function (Blueprint $table) {
                $table->dropColumn('unit_price');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_permintaan_pembelian', function (Blueprint $table) {
            //
        });
    }
}
