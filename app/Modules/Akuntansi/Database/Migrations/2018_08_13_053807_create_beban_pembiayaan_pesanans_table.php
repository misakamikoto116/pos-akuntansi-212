<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBebanPembiayaanPesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beban_pembiayaan_pesanan', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('akun_bbn_pembiayaan_pesanan_id')->unsigned();
            $table->foreign('akun_bbn_pembiayaan_pesanan_id')
                    ->references('id')
                    ->on('akun');

            $table->datetime('tanggal');
            $table->text('catatan')->nullable(); 
            $table->float('amount', 19,2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beban_pembiayaan_pesanan');
    }
}
