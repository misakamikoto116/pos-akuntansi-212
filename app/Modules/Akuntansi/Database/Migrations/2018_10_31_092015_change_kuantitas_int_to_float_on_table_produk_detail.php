<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeKuantitasIntToFloatOnTableProdukDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('produk_detail', 'kuantitas')) {
            Schema::table('produk_detail', function (Blueprint $table) {
                $table->float('kuantitas', 19,2)->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produk_detail', function (Blueprint $table) {
            //
        });
    }
}
