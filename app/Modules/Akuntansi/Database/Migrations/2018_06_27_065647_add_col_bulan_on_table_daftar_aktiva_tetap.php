<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColBulanOnTableDaftarAktivaTetap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('daftar_aktiva_tetap', 'umur_bulan_aktiva')) {
            Schema::table('daftar_aktiva_tetap', function (Blueprint $table) {
                $table->dropColumn('umur_bulan_aktiva');
            });
        }

        if (!Schema::hasColumn('daftar_aktiva_tetap', 'bulan')) {
               Schema::table('daftar_aktiva_tetap', function (Blueprint $table) {
                    $table->integer('bulan')->nullable()->after('tahun');
               });
        }   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daftar_aktiva_tetap', function (Blueprint $table) {
            //
        });
    }
}
