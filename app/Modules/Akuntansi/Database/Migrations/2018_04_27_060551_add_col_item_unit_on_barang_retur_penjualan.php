<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColItemUnitOnBarangReturPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('barang_retur_penjualan', 'satuan')) {
            Schema::table('barang_retur_penjualan', function (Blueprint $table) {
                $table->string('satuan')->nullable()->after('jumlah');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_retur_penjualan', function (Blueprint $table) {
            //
        });
    }
}
