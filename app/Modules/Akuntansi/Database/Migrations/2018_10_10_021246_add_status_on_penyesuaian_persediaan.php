<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusOnPenyesuaianPersediaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('penyesuaian_persediaan', 'status')) {
            Schema::table('penyesuaian_persediaan', function (Blueprint $table) {
                $table->tinyInteger('status')->after('keterangan')->comment('0 = saldo awal, 1 = dari transaksi')->default(1);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penyesuaian_persediaan', function (Blueprint $table) {
            //
        });
    }
}
