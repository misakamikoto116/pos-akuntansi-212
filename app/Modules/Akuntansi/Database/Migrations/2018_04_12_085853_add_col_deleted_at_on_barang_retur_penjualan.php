<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColDeletedAtOnBarangReturPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('barang_retur_penjualan', 'deleted_at')) {
            Schema::table('barang_retur_penjualan', function (Blueprint $table) {
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('barang_retur_penjualan', 'deleted_at')) {
            Schema::table('barang_retur_penjualan', function (Blueprint $table) {
                $table->dropColumn('deleted_at');
            });
        }
    }
}
