<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyOnBebanPembiayaanPesanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beban_pembiayaan_pesanan', function (Blueprint $table) {
            $table->integer('pembiayaan_pesanan_id')->unsigned()->after('id');
            $table->foreign('pembiayaan_pesanan_id')
                    ->references('id')
                    ->on('pembiayaan_pesanan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
