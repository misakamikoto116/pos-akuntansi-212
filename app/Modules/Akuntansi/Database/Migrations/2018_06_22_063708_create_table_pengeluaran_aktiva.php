<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePengeluaranAktiva extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!schema::hasTable('pengeluaran_aktiva')){
            Schema::create('pengeluaran_aktiva', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('akun_pengeluaran_aktiva_id')->unsigned();
                $table->integer('daftar_aktiva_tetap_id')->unsigned();
                $table->datetime('tanggal');
                $table->text('keterangan');
                $table->float('jumlah', 19,2);
                $table->timestamps();

                $table->foreign('akun_pengeluaran_aktiva_id')
                        ->references('id')
                        ->on('akun');

                $table->foreign('daftar_aktiva_tetap_id')
                        ->references('id')
                        ->on('daftar_aktiva_tetap');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengeluaran_aktiva');        
    }
}
