<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDaftarAktivaTetapDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('daftar_aktiva_tetap_detail')) {
            Schema::create('daftar_aktiva_tetap_detail', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('daftar_aktiva_tetap_id')->unsigned();
                $table->foreign('daftar_aktiva_tetap_id')
                                ->references('id')
                                ->on('daftar_aktiva_tetap');
                // $table->integer('pengeluaran_aktiva_id')->unsigned();
                // $table->foreign('pengeluaran_aktiva_id')
                //                 ->references('id')
                //                 ->on('pengeluaran_aktiva');
                $table->float('harga_perolehan',19,2);
                $table->float('aktiva_dihentikan',19,2);
                $table->float('akumulasi_penyusutan',19,2);
                $table->float('nilai_buku_asuransi',19,2);
                $table->float('nilai_sisa',19,2);
                $table->float('nilai_penyusutan', 19,2);
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daftar_aktiva_tetap_detail');
    }
}
