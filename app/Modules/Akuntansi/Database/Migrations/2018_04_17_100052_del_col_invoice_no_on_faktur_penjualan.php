<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DelColInvoiceNoOnFakturPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('faktur_penjualan', 'invoice_no')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->dropColumn('invoice_no');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faktur_penjualan', function (Blueprint $table) {
            //
        });
    }
}
