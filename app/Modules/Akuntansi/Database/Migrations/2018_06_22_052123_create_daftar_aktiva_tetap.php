<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDaftarAktivaTetap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!schema::hasTable('daftar_aktiva_tetap')){
            Schema::create('daftar_aktiva_tetap', function (Blueprint $table) {
                $table->increments('id');
                $table->string('kode_aktiva');
                $table->integer('tipe_aktiva_id')->unsigned();
                $table->datetime('tgl_beli');
                $table->datetime('tgl_pakai');
                $table->text('keterangan');
                $table->integer('qty');
                $table->integer('departement_id')->nullable();
                $table->datetime('tgl_jurnal');
                $table->integer('umur_bulan_aktiva');
                $table->integer('metode_penyusutan_id')->unsigned();
                $table->float('rasio');
                $table->integer('akun_aktiva_id')->unsigned();
                $table->integer('akun_akum_penyusutan_id')->unsigned()->nullable();
                $table->integer('akun_beban_penyusutan_id')->unsigned()->nullable();
                $table->boolean('aktiva_tidak_berwujud')->nullable();
                $table->boolean('aktiva_tetap_fisikal')->nullable();
                $table->text('catatan')->nullable();
                $table->timestamps();

                $table->foreign('tipe_aktiva_id')
                        ->references('id')
                        ->on('tipe_aktiva_tetap');

                $table->foreign('metode_penyusutan_id')
                        ->references('id')
                        ->on('metode_penyusutan');

                $table->foreign('akun_aktiva_id')
                        ->references('id')
                        ->on('akun');

                $table->foreign('akun_akum_penyusutan_id')
                        ->references('id')
                        ->on('akun');

                $table->foreign('akun_beban_penyusutan_id')
                        ->references('id')
                        ->on('akun');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daftar_aktiva_tetap');        
    }
}
