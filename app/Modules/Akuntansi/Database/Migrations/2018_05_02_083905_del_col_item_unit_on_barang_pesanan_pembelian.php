<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DelColItemUnitOnBarangPesananPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('barang_pesanan_pembelian', 'item_unit')) {
            Schema::table('barang_pesanan_pembelian', function (Blueprint $table) {
                $table->dropColumn('item_unit');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_pesanan_pembelian', function (Blueprint $table) {
            //
        });
    }
}
