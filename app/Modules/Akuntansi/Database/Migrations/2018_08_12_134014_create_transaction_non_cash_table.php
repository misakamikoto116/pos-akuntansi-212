<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionNonCashTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('transaction_non_cash')) {
            Schema::create('transaction_non_cash', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('invoice_penerimaan_penjualan_id')->unsigned();

                $table->foreign('invoice_penerimaan_penjualan_id')->references('id')->on('invoice_penerimaan_penjualan');

                $table->string('no_transaksi_bank')->nullable();
                $table->string('no_kartu')->nullable();
                $table->string('total')->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_non_cash');
    }
}
