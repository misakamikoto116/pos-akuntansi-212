<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableTransaksiUangMuka extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('transaksi_uang_muka', 'total_pajak1')) {
            Schema::table('transaksi_uang_muka', function (Blueprint $table) {
                $table->float('total_pajak1',19,2)->after('pajak_dua_id');
            });
        }

        if (!Schema::hasColumn('transaksi_uang_muka', 'total_pajak2')) {
            Schema::table('transaksi_uang_muka', function (Blueprint $table) {
                $table->float('total_pajak2',19,2)->after('total_pajak1');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi_uang_muka', function (Blueprint $table) {
            //
        });
    }
}
