<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePayToDateOnInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_penerimaan_penjualan', function (Blueprint $table) {
            $table->dropColumn('status_pay');
            $table->datetime('tanggal');

            $table->integer('penerimaan_penjualan_id')->unsigned();
            $table->foreign('penerimaan_penjualan_id')
                ->references('id')
                ->on('penerimaan_penjualan');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
