<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColNilaiPajakPenawaranPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('penawaran_penjualan', 'nilai_pajak')) {
            Schema::table('penawaran_penjualan', function (Blueprint $table) {
                $table->float('nilai_pajak',19,2)->after('ongkir')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penawaran_penjualan', function (Blueprint $table) {
            //
        });
    }
}
