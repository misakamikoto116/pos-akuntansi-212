<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColNilaiPajakOnTablePesananPenjualan extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up()
	{
		if (!Schema::hasColumn('pesanan_penjualan', 'nilai_pajak')) {
			Schema::table('pesanan_penjualan', function (Blueprint $table) {
				$table->float('nilai_pajak', 19, 2)->after('ongkir')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 */
	public function down()
	{
		Schema::table('pesanan_penjualan', function (Blueprint $table) {
		});
	}
}
