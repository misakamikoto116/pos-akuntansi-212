<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetailPemindahanBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('detail_pemindahan_barang')) {
            Schema::create('detail_pemindahan_barang', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->integer('produk_id')->unsigned();
                $table->string('keterangan');
                $table->integer('jumlah');
                $table->string('satuan');
                $table->string('serial_number')->nullable();
                $table->integer('pemindahan_barang_id')->unsigned();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('detail_pemindahan_barang')) {
                Schema::dropIfExists('detail_pemindahan_barang');
        }
    }
}
