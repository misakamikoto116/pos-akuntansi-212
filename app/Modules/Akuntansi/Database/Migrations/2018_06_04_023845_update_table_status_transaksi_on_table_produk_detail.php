<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableStatusTransaksiOnTableProdukDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('produk_detail', 'status_transaksi')) {
            Schema::table('produk_detail', function (Blueprint $table) {
                $table->tinyInteger('status_transaksi')->comment('0 => faktur penjualan, 1 => retur penjualan, 2 => faktur pembelian, 3 =>  retur pembelian, 4 => pengiriman penjualan, 5 => penerimaan barang')->after('status')->nullable();
            });
        }
        if (!Schema::hasColumn('produk_detail', 'harga_terakhir')) {
            Schema::table('produk_detail', function (Blueprint $table) {
                $table->float('harga_terakhir',19,2)->after('harga_modal');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produk_detail', function (Blueprint $table) {
            //
        });
    }
}
