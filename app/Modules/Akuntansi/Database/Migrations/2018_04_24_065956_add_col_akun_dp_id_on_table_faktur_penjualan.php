<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColAkunDpIdOnTableFakturPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('faktur_penjualan', 'akun_dp_id')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->integer('akun_dp_id')->unsigned()->after('akun_piutang_id')->nullable();
                $table->foreign('akun_dp_id')
                    ->references('id')
                    ->on('akun');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faktur_penjualan', function (Blueprint $table) {
            //
        });
    }
}
