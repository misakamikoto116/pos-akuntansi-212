<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableTermShipOnTablePesananPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('pesanan_pembelian', 'term_id')) {
            Schema::table('pesanan_pembelian', function (Blueprint $table) {
                $table->dropForeign('pesanan_pembelian_term_id_foreign');
                $table->dropColumn('term_id');
            });
        }

        if (Schema::hasColumn('pesanan_pembelian', 'ship_id')) {
            Schema::table('pesanan_pembelian', function (Blueprint $table) {
                $table->dropForeign('pesanan_pembelian_ship_id_foreign');
                $table->dropColumn('ship_id');
            });
        }

        if (!Schema::hasColumn('pesanan_pembelian', 'term_id')) {
            Schema::table('pesanan_pembelian', function (Blueprint $table) {
                $table->integer('term_id')->unsigned()->nullable()->after('taxable');
                $table->foreign('term_id')
                      ->references('id')
                      ->on('syarat_pembayaran')
                      ->onUpdate('cascade');
            });
        }

        if (!Schema::hasColumn('pesanan_pembelian', 'ship_id')) {
            Schema::table('pesanan_pembelian', function (Blueprint $table) {
                $table->integer('ship_id')->unsigned()->nullable()->after('term_id');
                $table->foreign('ship_id')
                      ->references('id')
                      ->on('jasa_pengiriman')
                      ->onUpdate('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesanan_pembelian', function (Blueprint $table) {
            //
        });
    }
}
