<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailSerialNumber extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        if (!Schema::hasTable('detail_serial_number')) {
            Schema::create('detail_serial_number', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('serial_number_id')->unsigned();
                $table->string('item');
                $table->string('item_deskripsi');
                $table->integer('jumlah');
                $table->string('satuan');
                $table->string('serial_number')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('detail_serial_number', function (Blueprint $table) {
            Schema::dropIfExists('detail_serial_number');
        });
    }
}
