<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixBebanFakturPembelianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('beban_faktur_pembelian', 'ke_pemasok_lain')) {
            Schema::table('beban_faktur_pembelian', function (Blueprint $table) {
                $table->dropColumn('ke_pemasok_lain');
                $table->integer('pemasok_id')->nullable()->unsigned()->after('alokasi_ke_barang');
                $table->foreign('pemasok_id')
                        ->references('id')
                        ->on('informasi_pemasok');
            });
        }   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
