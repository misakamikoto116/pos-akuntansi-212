<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColProdukIdOnTableTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('transaksi', 'produk_id')) {
            Schema::table('transaksi', function (Blueprint $table) {
                $table->integer('produk_id')->unsigned()->after('akun_id')->nullable();
                $table->foreign('produk_id')
                                ->references('id')
                                ->on('produk');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi', function (Blueprint $table) {
            //
        });
    }
}
