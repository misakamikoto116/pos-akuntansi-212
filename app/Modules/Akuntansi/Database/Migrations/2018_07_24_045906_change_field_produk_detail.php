<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldProdukDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('produk_detail', 'status_transaksi')) {
            Schema::table('produk_detail', function (Blueprint $table) {
                $table->dropColumn('status_transaksi');
            });
        }
        if (!Schema::hasColumn('produk_detail', 'item_type')) {
            Schema::table('produk_detail', function (Blueprint $table) {
                $table->string('item_type')->after('status')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
