<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFpstdPesananPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('pesanan_pembelian', 'no_fp_std')) {
            Schema::table('pesanan_pembelian', function (Blueprint $table) {
                $table->string('no_fp_std')->nullable()->after('akun_ongkir_id');
                $table->string('no_fp_std2')->nullable()->after('no_fp_std');
                $table->datetime('no_fp_std_date')->nullable()->after('no_fp_std2');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
