<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColCetakOnPengirimanPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('pengiriman_penjualan', 'cetak')) {
            Schema::table('pengiriman_penjualan', function (Blueprint $table) {
                $table->tinyInteger('cetak')->after('keterangan')->default(0)->comment('0 => Belum Tercetak, 1 => Tercetak');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengiriman_penjualan', function (Blueprint $table) {
            //
        });
    }
}
