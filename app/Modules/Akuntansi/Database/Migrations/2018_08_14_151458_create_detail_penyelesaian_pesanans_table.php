<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPenyelesaianPesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_penyelesaian_pesanan', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('produk_id')->unsigned();
            $table->foreign('produk_id')
                    ->references('id')
                    ->on('produk');

            $table->integer('penyelesaian_pesanan_id')->unsigned();
            $table->foreign('penyelesaian_pesanan_id')
                    ->references('id')
                    ->on('penyelesaian_pesanan');

            $table->integer('kuantitas');            
            $table->float('biaya', 19,2);
            $table->float('harga_modal', 19,2);
            $table->float('alokasi_nilai', 19,2);
            $table->float('presentase', 19,2);
            
            $table->integer('gudang_id')->unsigned();
            $table->foreign('gudang_id')
                    ->references('id')
                    ->on('gudang');
            
            $table->string('sn')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_penyelesaian_pesanan');
    }
}
