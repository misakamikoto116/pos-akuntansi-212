<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastOwingOnInvoicePenerimaanPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('invoice_penerimaan_penjualan', 'last_owing')) {
            Schema::table('invoice_penerimaan_penjualan', function (Blueprint $table) {
                $table->float('last_owing',19,2)->after('diskon')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_penerimaan_penjualan', function (Blueprint $table) {
            //
        });
    }
}
