<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMataUangOnAkun extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('akun', 'mata_uang_id')) {
            Schema::table('akun', function (Blueprint $table) {
                $table->integer('mata_uang_id')->unsigned()->after('tipe_akun_id')->nullable();
                $table->foreign('mata_uang_id')->references('id')->on('mata_uang');

                $table->tinyInteger('fiksal')->default(0)->after('mata_uang_id');
                $table->tinyInteger('alokasi_produksi')->default(0)->after('fiksal');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
