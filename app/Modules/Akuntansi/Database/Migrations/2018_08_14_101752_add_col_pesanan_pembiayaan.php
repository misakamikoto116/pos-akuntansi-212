<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColPesananPembiayaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_pembiayaan_pesanan', function (Blueprint $table) {
            $table->float('harga_modal', 19,2)->after('sn');
            $table->boolean('status')->after('harga_modal')->default(0);
        });

        Schema::table('pembiayaan_pesanan', function (Blueprint $table) {
            $table->boolean('status')->after('deskripsi')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
