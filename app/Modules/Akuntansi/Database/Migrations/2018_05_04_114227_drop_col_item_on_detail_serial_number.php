<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColItemOnDetailSerialNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_serial_number', function (Blueprint $table) {
            $table->dropColumn('item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_serial_number', function (Blueprint $table) {
            $table->string('item')->after('serial_number_id')->nullable();
        });
    }
}
