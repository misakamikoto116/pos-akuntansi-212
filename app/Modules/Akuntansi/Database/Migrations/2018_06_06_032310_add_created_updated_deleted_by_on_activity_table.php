<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedUpdatedDeletedByOnActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
                    // Detail Penjualan
                    'barang_penawaran_penjualan',
                    'barang_pesanan_penjualan',
                    'barang_pengiriman_penjualan',
                    'barang_faktur_penjualan',
                    'invoice_penerimaan_penjualan',
                    'barang_retur_penjualan',
                    // Detail Pembelian
                    'barang_permintaan_pembelian',
                    'barang_pesanan_pembelian',
                    'barang_penerimaan_pembelian',
                    'barang_faktur_pembelian',
                    'invoice_pembayaran_pembelian',
                    'barang_retur_pembelian',
                    // Master Penjualan
                    'penawaran_penjualan',
                    'pesanan_penjualan',
                    'pengiriman_penjualan',
                    'faktur_penjualan',
                    'penerimaan_penjualan',
                    'retur_penjualan',
                    // Master Pembelian
                    'permintaan_pembelian',
                    'pesanan_pembelian',
                    'penerimaan_pembelian',
                    'faktur_pembelian',
                    'pembayaran_pembelian',
                    'retur_pembelian',
                    // Transaksi
                    'transaksi',
                    'transaksi_uang_muka',
                    'transaksi_uang_muka_pemasok'
            ];
        foreach($data as $tb){
            if (!Schema::hasColumn($tb, 'created_by')) {
                Schema::table($tb, function (Blueprint $table) {
                    $table->integer('created_by')->unsigned()->nullable();
                    $table->foreign('created_by')
                        ->references('id')
                        ->on('users');
                });
            }
            if (!Schema::hasColumn($tb, 'updated_by')) {
                Schema::table($tb, function (Blueprint $table) {
                    $table->integer('updated_by')->unsigned()->nullable();
                    $table->foreign('updated_by')
                        ->references('id')
                        ->on('users');
                });
            }
            if (!Schema::hasColumn($tb, 'deleted_by')) {
                Schema::table($tb, function (Blueprint $table) {
                    $table->integer('deleted_by')->unsigned()->nullable();
                    $table->foreign('deleted_by')
                        ->references('id')
                        ->on('users');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
