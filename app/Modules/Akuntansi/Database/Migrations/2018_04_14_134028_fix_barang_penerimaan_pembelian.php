<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixBarangPenerimaanPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barang_penerimaan_pembelian', function (Blueprint $table) {
            $table->integer('penerimaan_pembelian_id')->unsigned()->after('id');
            $table->foreign('penerimaan_pembelian_id')
                            ->references('id')
                            ->on('penerimaan_pembelian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
