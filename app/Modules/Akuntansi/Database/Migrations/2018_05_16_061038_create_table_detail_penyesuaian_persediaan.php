<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetailPenyesuaianPersediaan extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        if (!Schema::hasTable('detail_penyesuaian_persediaan')) {
            Schema::create('detail_penyesuaian_persediaan', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('penyesuaian_persediaan_id')->unsigned();
                $table->integer('produk_id')->unsigned();
                $table->string('keterangan_produk');
                $table->string('satuan_produk')->nullable();
                $table->string('qty_produk')->nullable();
                $table->string('qty_baru')->nullable();
                $table->string('departemen')->nullable();
                $table->string('gudang')->nullable();
                $table->string('proyek')->nullable();
                $table->string('serial_number')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        if (Schema::hasTable('detail_penyesuian_persediaan')) {
            Schema::dropIfExists('detail_penyesuaian_persediaan');
        }
    }
}
