<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColPajakIdOnEveryBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            'barang_faktur_pembelian',
            'barang_faktur_penjualan',
            'barang_penawaran_penjualan',
            'barang_pesanan_penjualan',
            'barang_pesanan_pembelian',
            'barang_retur_pembelian',
            'barang_retur_penjualan',
        ];
        foreach($data as $tb){
            if (!Schema::hasColumn($tb, 'kode_pajak_2_id')) {
                Schema::table($tb, function (Blueprint $table) {
                    $table->integer('kode_pajak_2_id')->unsigned()->after('kode_pajak_id')->nullable();
                    $table->foreign('kode_pajak_2_id')
                        ->references('id')
                        ->on('kode_pajak');
                });
            }
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
