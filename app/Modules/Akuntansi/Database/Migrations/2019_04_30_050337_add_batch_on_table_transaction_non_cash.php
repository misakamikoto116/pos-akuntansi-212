<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBatchOnTableTransactionNonCash extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('transaction_non_cash', 'bulan_tahun')) {
            Schema::table('transaction_non_cash', function (Blueprint $table) {
                $table->dropColumn('bulan_tahun');
            });
        }

        if (!Schema::hasColumn('transaction_non_cash', 'batch')) {
            Schema::table('transaction_non_cash', function (Blueprint $table) {
                $table->string('batch')->after('no_transaksi_bank')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_non_cash', function (Blueprint $table) {
            //
        });
    }
}
