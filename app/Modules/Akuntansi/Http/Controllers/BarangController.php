<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Models\Barang;
use App\Modules\Akuntansi\Http\Requests\BarangRequest;
use App\Modules\Akuntansi\Repositories\BarangRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

use App\Modules\Akuntansi\Models\BarangPenerimaanPembelian;
use App\Modules\Akuntansi\Models\BarangPengirimanPenjualan;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;

class BarangController extends Controller
{
    use ExportImportBarang;
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Barang';
        $this->request = BarangRequest::class;
        $this->requestField = [
            'tipe_barang',
            'no_barang',
            'barcode',
            'nama_singkat',
            'parent_id',
            'status',
            'keterangan',
            'tipe_persedian',
            'harga_standar_def',
            'kategori_produk_id',
            'gudang_def_id',
            'kuantitas',
            'unit',
            'harga_unit',
            'harga_jual',
            'status_umkm',
            'diskon',
            'kode_pajak_penj_id',
            'pemasok_id',
            'min_jumlah_reorder',
            'kode_pajak_perse_id',
            'kode_pajak_pemb_id',
            'akun_persedian_id',
            'akun_beban_id',
            'akun_penjualan_id',
            'akun_ret_penjualan_id',
            'akun_disk_penjualan_id',
            'akun_barang_terkirim_id',
            'akun_hpp_id',
            'akun_ret_pembelian_id',
            'akun_belum_tertagih_id',
            'catatan',
            'no_faktur_gudang',
            'tanggal_gudang',
            'gudang_id',
            'kuantitas_gudang',
            'biaya_gudang',
            'sn_gudang',
            'tingkat_1',
            'tingkat_2',
            'tingkat_3',
            'tingkat_4',
            'tingkat_5',
            'multi_gudang_id',
            'foto_produk'
        ];
    }

    public function formData()
    {
        return [
            'kategori' => $this->model->listKategoriProduk(),
            'gudang' => $this->model->listGudang(),
            'akun' => $this->model->listAkun(),
            'pemasok' => $this->model->listPemasok(),
            'kodePajak' => $this->model->listKodePajak(),
            'prefBarang' => $this->model->listPrefBarang(),
            'gudang' => $this->model->listGudang(),

            'tipe_barang' => collect(['0' => 'Persediaan','1' => 'Non Persediaan','2' => 'Servis','3' => 'Uang Muka','4' => 'Grup']),
            'tipe_persediaan' => collect(['0' => 'Bahan Baku','1' => 'Bahan Baku Pembantu','2' => 'Barang Setengah Jadi','3' => 'Barang Jadi','4' => 'Barang lain-lain']),
            'filter' => ['kategori','tipe_barang','tipe_persediaan'],
            // 'tipePelanggan' => $this->model->listTipePelanggan(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function embedDataToShow($id, $request)
    {
        $newCollection  = [];
        $pengurangan    = 0;
        $penambahan     = 0;
        
        $dataAwal    = SaldoAwalBarang::where('produk_id',$id)->dateFilter($request)->orderBy('id')->limit(0,1)->get(); 
        $dataSemua   = SaldoAwalBarang::with('produk')->where('produk_id',$id)->where('kuantitas','>',0)->dateFilter($request)->get();
        // $ruang_nama = [];
        foreach($dataSemua as $data){
            if($data->produk->transaksi != null){
                $newCollection[] = [
                    'produk_id'     => $data->produk_id,
                    'tanggal'       => $data->tanggal,
                    'jumlah'        => abs($data->kuantitas),
                    'harga_modal'   => $data->harga_terakhir,
                    'keterangan'    => $data->produk->transaksi()->first()->keterangan,
                    'invoice_no'    => $data->produk->transaksi()->first()->no_transaksi
                ];
                $ruang_nama = $data->produk->transaksi()->first() ;
                $nama_kelas = (new \ReflectionClass($ruang_nama))
                    ->getShortName();
            
                if($nama_kelas == 'PengirimanPenjualan' || $nama_kelas == 'FakturPenjualan' || $nama_kelas == 'ReturPembelian'){
                    $newCollection[count($newCollection)-1]['status'] = 'out';
                    $pengurangan += abs($data->kuantitas);
                }else{
                    $newCollection[count($newCollection)-1]['status'] = 'in';
                    $penambahan += abs($data->kuantitas);
                }
            }
        }
        // dd($ruang_nama);
        $getDate = $request->get('start-date');
        if(empty($getDate)){
            $getDate = "01-".date('m-Y');
        }else{
            $getDate = date('d-m-Y',strtotime($getDate));
        }

        $newCollection[] = [
            'produk_id'     => null,
            'tanggal'       => '0000-00-00',
            'jumlah'        => $dataAwal->where('produk_id',$id)->sum('kuantitas') - $penambahan + $pengurangan,
            'harga_modal'   => null,
            'status'        => null,
            'keterangan'    => 'On Hand As Of '.$getDate,
            'invoice_no'    => null
        ];

        $doneCollection = collect($newCollection)->sortBy('tanggal');
        return ['items' => $doneCollection, 'filter' => ['tanggal-jangka','module_url_show']];
    }

    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }

    public function marginHargaJual()
    {
        $request = request();
        $barang  = $this->model->marginHargaJual($request->all());

        if ($barang['type'] === "success") {
            return redirect()->back()->withMessage($barang['message']);
        }else {
            return redirect()->back()->withErrors($barang['message']);
        }
    }
}
