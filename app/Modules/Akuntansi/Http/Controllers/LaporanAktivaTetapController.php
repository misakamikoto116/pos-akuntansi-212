<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\DaftarAktivaTetap;
use App\Modules\Akuntansi\Models\DaftarAktivaTetapDetail;
use App\Modules\Akuntansi\Models\TipeAktivaTetapPajak;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;

class LaporanAktivaTetapController extends Controller
{
    use TraitLaporan;

    // Laporan Aktiva Tetap - Rincian Aktiva Tetap
    public function tipe_aktiva_tetap_pajak(Request $request)
    {
        $tipeAktivaTetapPajak       =   $this->tipeAktivaTetapPajak->with('metodePenyusutan')
                                        ->groupBy('id');

        $array = [];
        foreach($tipeAktivaTetapPajak->get() as $dataTipeAktivaTetapPajak){
            $array [] = [
                'tipeAktiva'        =>      $dataTipeAktivaTetapPajak->tipe_aktiva_tetap_pajak,
                'metode'            =>      $dataTipeAktivaTetapPajak->metodePenyusutan->metode,
                'estimasi'          =>      $dataTipeAktivaTetapPajak->estimasi_umur,
                'nilai_tukar'       =>      $dataTipeAktivaTetapPajak->tarif_penyusutan,
            ];
        }
        $newCollection = collect($array);

        $view = [
            'title'           =>      'Laporan Tipe Aktiva Tetap Pajak',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_aktiva_tetap.tipe_aktiva_tetap_pajak')->with($view);
    }

    public function daftar_aktiva_tetap_per_aktiva_tetap(Request $request)
    {
        $tipeAktivaTetap       =   $this->daftarAktivaTetap->with('tipeAktivaTetap','pengeluaranAktiva','daftarAktivaTetapDetail')
                                            ->DateFilter($request);
        
        $array = [];
        foreach($tipeAktivaTetap->get() as $dataTipeAktivaTetap){            
            $array [] = [
                'namaAktiva'                =>      $dataTipeAktivaTetap->tipeAktivaTetap->tipe_aktiva_tetap,
                'kodeAktiva'                =>      $dataTipeAktivaTetap->kode_aktiva,
                'keterangan'                =>      $dataTipeAktivaTetap->keterangan,
                'hargaPerolehan'            =>      $dataTipeAktivaTetap->daftarAktivaTetapDetail->last()['harga_perolehan'],
                'penyesuaianTahun'          =>      0,
                'akumulasiDep'              =>      $dataTipeAktivaTetap->daftarAktivaTetapDetail->last()['akumulasi_penyusutan'],
                'bookValue'                 =>      $dataTipeAktivaTetap->daftarAktivaTetapDetail->last()['nilai_buku_asuransi'], // harga perolehan dikurang dengan depresiasi
                'depresiasiTahunIni'        =>      0,
                'tglPemakaian'              =>      $this->dateCarbon($dataTipeAktivaTetap->tgl_beli),
                'tglPembelian'              =>      $this->dateCarbon($dataTipeAktivaTetap->tgl_pakai),
            ];
        }
        $newCollection = collect($array)->groupBy('namaAktiva');

        $view = [
            'title'           =>      'Laporan Daftar Aktiva Tetap per Aktiva Tetap',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_aktiva_tetap.daftar_aktiva_tetap_per_aktiva_tetap')->with($view);
    }

    public function daftar_aktiva_tetap_per_aktiva_pajak(Request $request)
    {
        $tipeAktivaTetap       =    $this->daftarAktivaTetap->with('tipeAktivaTetap',
                                    'metodePenyusutan.tipeAktivaTetapPajak','pengeluaranAktiva')
                                    ->DateFilter($request)->groupBy('id');
                                    
        $array = [];
        foreach($tipeAktivaTetap->get() as $dataTipeAktivaTetap){
                $array [] = [
                    'namaAktiva'                =>      $dataTipeAktivaTetap->tipeAktivaTetap->tipe_aktiva_tetap,
                    'tahunEs'                   =>      $dataTipeAktivaTetap->tahun.' Tahun',
                    'metode'                    =>      $dataTipeAktivaTetap->metodePenyusutan->metode,
                    'bulanEs'                   =>      $dataTipeAktivaTetap->bulan.' Bulan',
                    'nilaiTukar'                =>      $dataTipeAktivaTetap->metodePenyusutan->tipeAktivaTetapPajak->tarif_penyusutan,
                    'kodeAktiva'                =>      $dataTipeAktivaTetap->kode_aktiva,
                    'keterangan'                =>      $dataTipeAktivaTetap->keterangan,
                    'hargaPerolehan'            =>      $dataTipeAktivaTetap->daftarAktivaTetapDetail->last()['harga_perolehan'], // didapat dari harga penyesuaian 
                    'penyesuaianTahun'          =>      0,
                    'akumulasiDep'              =>      $dataTipeAktivaTetap->daftarAktivaTetapDetail->last()['akumulasi_penyusutan'], // 
                    'bookValue'                 =>      $dataTipeAktivaTetap->daftarAktivaTetapDetail->last()['nilai_buku_asuransi'], // harga perolehan dikurang dengan depresiasi
                    'depresiasiTahunIni'        =>      0, // masih belum tahu ?? nilai penyusutan per periode
                    'tglPemakaian'              =>      $this->dateCarbon($dataTipeAktivaTetap->tgl_beli),
                    'tglPembelian'              =>      $this->dateCarbon($dataTipeAktivaTetap->tgl_pakai),
                ];
        }
        $newCollection = collect($array)->groupBy('namaAktiva');

        $view = [
            'title'           =>      'Laporan Daftar Aktiva Tetap per Tipe Aktiva Pajak',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_aktiva_tetap.daftar_aktiva_tetap_per_aktiva_pajak')->with($view);
    }

    public function histori_aktiva_tetap(Request $request) // Masih Belum Yakin Dengan data Array Detailnya
    {
        $title          =   'Laporan Rincian Jurnal Aktiva Tetap';
        $aktivaDetail   =   $this->daftarAktivaTetapDetail->pluck('daftar_aktiva_tetap_id');
        $aktivaTetap    =   $this->daftarAktivaTetap
                            ->with('tipeAktivaTetap.tipeAktivaTetapPajak','daftarAktivaTetapDetail'
                                    ,'metodePenyusutan')->whereIn('id', $aktivaDetail)
                            ->get();
        $array = [];
        $arrayDetail = [];
        foreach($aktivaTetap as $key => $dataTipeAktivaTetap){
            $dataY = $dataTipeAktivaTetap->daftarAktivaTetapDetail->map(function($itemAktivaDetail) use(&$dataTipeAktivaTetap, &$arrayDetail){
                if ($itemAktivaDetail->periode == 0) {
                    $arrayDetail[] = [
                        'tanggal'         => $this->dateCarbon($itemAktivaDetail->tgl_jurnal),
                        'no_sumber'       => '',
                        'keterangan'      => 'Opening Balance',
                        'harga_perolehan' => $itemAktivaDetail->harga_perolehan,
                        'akumulasi_dep'   => $itemAktivaDetail->nilai_penyusutan,
                        'nilai_buku'      => $itemAktivaDetail->nilai_buku_asuransi,
                        'beban_dep'       => $itemAktivaDetail->nilai_penyusutan,
                        'fa_laba_rugi'    => 0, // Masih menjadi misteri
                    ];
                }else{
                    $arrayDetail[] = [
                        'tanggal'         => $this->dateCarbon($itemAktivaDetail->tgl_jurnal),
                        'no_sumber'       => '',
                        'keterangan'      => $itemAktivaDetail->keterangan,
                        'harga_perolehan' => 0,
                        'akumulasi_dep'   => $itemAktivaDetail->nilai_penyusutan,
                        'nilai_buku'      => $itemAktivaDetail->nilai_buku_asuransi,
                        'beban_dep'       => $itemAktivaDetail->nilai_penyusutan,
                        'fa_laba_rugi'    => 0, // Masih menjadi misteri
                    ];
                }                
                return array_last($arrayDetail);
            });
            $collectDataY = collect($dataY)->groupBy('tanggal');
            $array [] = [
                'kode_aktiva'                =>  $dataTipeAktivaTetap->kode_aktiva,
                'akun_aktiva'                =>  $dataTipeAktivaTetap->akunAktiva->nama_akun,
                'nama_aktiva'                =>  $dataTipeAktivaTetap->keterangan,
                'akun_akum_penyusutan'       =>  $dataTipeAktivaTetap->akunAkumPenyusutan->nama_akun,
                'nama_tipe_aktiva'           =>  $dataTipeAktivaTetap->tipeAktivaTetap->tipe_aktiva_tetap,
                'akun_beban_penyusutan'      =>  $dataTipeAktivaTetap->akunBebanPenyusutan->nama_akun,
                'nama_dep'                   =>  $dataTipeAktivaTetap->departement_id,
                'estimasi_umur'              =>  $dataTipeAktivaTetap->tahun.' Tahun '.$dataTipeAktivaTetap->bulan.' Bulan',
                'tgl_pembelian'              =>  $this->dateCarbon($dataTipeAktivaTetap->tgl_beli),
                'metode_penyusutan'          =>  $dataTipeAktivaTetap->metodePenyusutan->nama,
                'tgl_pemakaian'              =>  $this->dateCarbon($dataTipeAktivaTetap->tgl_pakai),
                'rasio_penyusutan'           =>  $dataTipeAktivaTetap->rasio.' %',
                'tgl_jurnal_akhir'           =>  $this->dateCarbon($dataTipeAktivaTetap->daftarAktivaTetapDetail->last()->tgl_jurnal) ? $this->dateCarbon($dataTipeAktivaTetap->daftarAktivaTetapDetail->last()->tgl_jurnal) : $this->dateCarbon($dataTipeAktivaTetap->tgl_jurnal),
                'nilai_residu'               =>  $dataTipeAktivaTetap->daftarAktivaTetapDetail->last()->nilai_sisa,
                'nama_gol_aktiva_tetap'      =>  $dataTipeAktivaTetap->tipeAktivaTetap->tipeAktivaTetapPajak->tipe_aktiva_tetap_pajak,
                'catatan'                    =>  $dataTipeAktivaTetap->catatan,
                'dihentikan'                 =>  '',
                'collectionY'                =>  $collectDataY,
            ];
        }
        $newCollection = collect($array)->groupBy('kode_aktiva');
        $view = $this->generateViewData($title, $request ,$newCollection);
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_aktiva_tetap.histori_aktiva_tetap')->with($view);
    }

    public function rincian_jurnal_aktiva_tetap(Request $request)
    {
        $title          =   'Laporan Rincian Jurnal Aktiva Tetap';
        $aktivaDetail   =   $this->daftarAktivaTetapDetail->pluck('daftar_aktiva_tetap_id');
        $aktivaTetap    =   $this->daftarAktivaTetap
                            ->with('tipeAktivaTetap.tipeAktivaTetapPajak','daftarAktivaTetapDetail'
                                    ,'metodePenyusutan')->whereIn('id', $aktivaDetail)
                            ->get();
        $array = [];
        $arrayDetail = [];
        foreach($aktivaTetap as $key => $dataTipeAktivaTetap){
            $dataX = $dataTipeAktivaTetap->daftarAktivaTetapDetail->map(function($itemAktivaDetail) use(&$dataTipeAktivaTetap, &$arrayDetail){
                if ($itemAktivaDetail->periode == 0) {
                    for ($i = 0; $i < count($dataTipeAktivaTetap->pengeluaranAktiva) ; $i++) { 
                        $arrayDetail[] = [
                            'tanggal'   => $this->dateCarbon($itemAktivaDetail->tgl_jurnal),
                            'no_sumber' => $dataTipeAktivaTetap->kode_aktiva,
                            'no_akun'   => $itemAktivaDetail->nama_beban !== null ? $dataTipeAktivaTetap->akunAktiva->kode_akun : $dataTipeAktivaTetap->pengeluaranAktiva[$i]->akun->kode_akun,
                            'nama_akun' => $itemAktivaDetail->nama_beban ?? $itemAktivaDetail->nama_akumulasi,
                            'debit'     => $itemAktivaDetail->nama_beban !== null ? $itemAktivaDetail->nilai_penyusutan != 0 ? $itemAktivaDetail->nilai_penyusutan : $itemAktivaDetail->harga_perolehan : 0,
                            'kredit'    => $itemAktivaDetail->nama_akumulasi !== null ? $itemAktivaDetail->nilai_penyusutan != 0 ? $itemAktivaDetail->nilai_penyusutan : $itemAktivaDetail->harga_perolehan : 0,
                        ];
                    }
                }else{
                    $arrayDetail[] = [
                        'tanggal'   => $this->dateCarbon($itemAktivaDetail->tgl_jurnal),
                        'no_sumber' => $itemAktivaDetail->periode == 0 ? $dataTipeAktivaTetap->kode_aktiva : $itemAktivaDetail->keterangan,
                        'no_akun'   => $itemAktivaDetail->nama_beban !== null ?  $dataTipeAktivaTetap->akunBebanPenyusutan->kode_akun : $dataTipeAktivaTetap->akunAkumPenyusutan->kode_akun,
                        'nama_akun' => $itemAktivaDetail->nama_beban ?? $itemAktivaDetail->nama_akumulasi,
                        'debit'     => $itemAktivaDetail->nama_beban !== null ? $itemAktivaDetail->nilai_penyusutan != 0 ? $itemAktivaDetail->nilai_penyusutan : $itemAktivaDetail->harga_perolehan : 0,
                        'kredit'    => $itemAktivaDetail->nama_akumulasi !== null ? $itemAktivaDetail->nilai_penyusutan != 0 ? $itemAktivaDetail->nilai_penyusutan : $itemAktivaDetail->harga_perolehan : 0,
                    ];
                }                
                return array_last($arrayDetail);
            });
            $collectDataX = collect($dataX)->groupBy('tanggal');
            $array[] = [
                'kode_aktiva'               => $dataTipeAktivaTetap->kode_aktiva,
                'akun_aktiva'               => $dataTipeAktivaTetap->akunAktiva->nama_akun,
                'nama_aktiva'               => $dataTipeAktivaTetap->keterangan,
                'akun_akum_penyusutan'      => $dataTipeAktivaTetap->akunAkumPenyusutan->nama_akun,
                'nama_tipe_aktiva'          => $dataTipeAktivaTetap->tipeAktivaTetap->tipe_aktiva_tetap,
                'akun_beban_penyusutan'     => $dataTipeAktivaTetap->akunBebanPenyusutan->nama_akun,
                'arrayDetail'               => $collectDataX,
            ];
        }
        $newCollection = collect($array)->groupBy('kode_aktiva');
        $view = $this->generateViewData($title, $request ,$newCollection);
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_aktiva_tetap.rincian_jurnal_aktiva_tetap')->with($view);
    }

    public function daftar_penyusutan_aktiva_tetap(Request $request)
    {
        $title          =   'Laporan Daftar Penyusutan Aktiva Tetap';
        $aktivaDetail   =   $this->daftarAktivaTetapDetail->pluck('daftar_aktiva_tetap_id');
        $aktivaTetap    =   $this->daftarAktivaTetap
                            ->with('tipeAktivaTetap.tipeAktivaTetapPajak','daftarAktivaTetapDetail'
                                    ,'metodePenyusutan')->whereIn('id', $aktivaDetail)
                            ->get();
        $array = [];
        $arrayDetail = [];
        foreach($aktivaTetap as $key => $dataTipeAktivaTetap){
            $dataW = $dataTipeAktivaTetap->daftarAktivaTetapDetail->map(function($itemAktivaDetail) use(&$dataTipeAktivaTetap, &$arrayDetail){
                if($itemAktivaDetail->nama_akumulasi !== null){
                    $arrayDetail[] = [
                        'tahun'         => Carbon::parse($itemAktivaDetail->tgl_jurnal)->format('Y'),
                        'penyusutan'    => $itemAktivaDetail->nilai_penyusutan,
                    ];
                    return array_last($arrayDetail);
                }
            })->filter();
            $collectDataW = collect($dataW)->groupBy('tahun');
            $lastDetailAktiva = $dataTipeAktivaTetap->daftarAktivaTetapDetail->first();
            $penyusutan = $this->kondisiPenyusutanBuTa($dataTipeAktivaTetap, $lastDetailAktiva);
            $array[] = [
                'kode_aktiva'       => $dataTipeAktivaTetap->kode_aktiva,
                'estimasi_umur'     => $dataTipeAktivaTetap->tahun.' Tahun '.$dataTipeAktivaTetap->bulan.' Bulan',
                'nama_tipe_aktiva'  => $dataTipeAktivaTetap->tipeAktivaTetap->tipe_aktiva_tetap,
                'metode_penyusutan' => $dataTipeAktivaTetap->metodePenyusutan->nama,
                'nama_aktiva'       => $dataTipeAktivaTetap->keterangan,
                'harga_perolehan'   => $dataTipeAktivaTetap->daftarAktivaTetapDetail->first()['harga_perolehan'],
                'tgl_pembelian'     => $this->dateCarbon($dataTipeAktivaTetap->tgl_beli),
                'penyusutan_tahun'  => $penyusutan['tahun'],
                'tgl_pemakaian'     => $this->dateCarbon($dataTipeAktivaTetap->tgl_pakai),
                'penyusutan_bulan'  => $penyusutan['bulan'],
                'collectionW'       => $collectDataW,
            ];
        }
        $newCollection = collect($array)->groupBy('kode_aktiva');
        $view = $this->generateViewData($title, $request ,$newCollection);
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_aktiva_tetap.daftar_penyusutan_aktiva_tetap')->with($view);
    }

    public function perbedaan_penyusutan_sementara(Request $request)
    {
        $title          =   'Laporan Perbedaan Penyusutan Sementara';
        $aktivaDetail   =   $this->daftarAktivaTetapDetail->pluck('daftar_aktiva_tetap_id');
        $aktivaTetap    =   $this->daftarAktivaTetap
                            ->with('tipeAktivaTetap.tipeAktivaTetapPajak','daftarAktivaTetapDetail'
                                    ,'metodePenyusutan')->whereIn('id', $aktivaDetail)
                            ->get();
        $array = [];
        $arrayDetail = [];
        foreach($aktivaTetap as $key => $dataTipeAktivaTetap){
            $dataZ = $dataTipeAktivaTetap->daftarAktivaTetapDetail->map(function($itemAktivaDetail) use(&$dataTipeAktivaTetap, &$arrayDetail){
                if($itemAktivaDetail->nama_akumulasi !== null){
                    $arrayDetail[] = [
                        'tahun'         => Carbon::parse($itemAktivaDetail->tgl_jurnal)->format('Y'),
                        'penyusutan'    => $itemAktivaDetail->nilai_penyusutan,
                        'koreksi_pajak' => 0,
                        'pph'           => $itemAktivaDetail->nilai_penyusutan,
                    ];
                    return array_last($arrayDetail);
                }
            })->filter();
            $collectDataZ = collect($dataZ)->groupBy('tahun');
            $lastDetailAktiva = $dataTipeAktivaTetap->daftarAktivaTetapDetail->first();
            $penyusutan = $this->kondisiPenyusutanBuTa($dataTipeAktivaTetap, $lastDetailAktiva);
            $array[] = [
                'kode_aktiva'       => $dataTipeAktivaTetap->kode_aktiva,
                'estimasi_umur'     => $dataTipeAktivaTetap->tahun.' Tahun '.$dataTipeAktivaTetap->bulan.' Bulan',
                'nama_tipe_aktiva'  => $dataTipeAktivaTetap->tipeAktivaTetap->tipe_aktiva_tetap,
                'metode_penyusutan' => $dataTipeAktivaTetap->metodePenyusutan->nama,
                'nama_aktiva'       => $dataTipeAktivaTetap->keterangan,
                'harga_perolehan'   => $dataTipeAktivaTetap->daftarAktivaTetapDetail->first()['harga_perolehan'],
                'tgl_pembelian'     => $this->dateCarbon($dataTipeAktivaTetap->tgl_beli),
                'penyusutan_tahun'  => $penyusutan['tahun'],
                'tgl_pemakaian'     => $this->dateCarbon($dataTipeAktivaTetap->tgl_pakai),
                'penyusutan_bulan'  => $penyusutan['bulan'],
                'collectionZ'       => $collectDataZ,
            ];
        }
        $newCollection = collect($array)->groupBy('kode_aktiva');
        $view = $this->generateViewData($title, $request ,$newCollection);
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_aktiva_tetap.perbedaan_penyusutan_sementara')->with($view);
    }

    public function histori_perubahan_asset_tetap()
    {
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_aktiva_tetap.histori_perubahan_asset_tetap');
    }

    public function kondisiPenyusutanBuTa($dataTipeAktivaTetap, $lastDetailAktiva){
        if ($dataTipeAktivaTetap['metode_penyusutan_id'] == 1) { // Metode Garis Lurus
            $penyusutan_tahun   = (($lastDetailAktiva['harga_perolehan'] * ($dataTipeAktivaTetap['rasio'] / 100)));
            $penyusutan_bulan   = $penyusutan_tahun / 12;
        } elseif ($dataTipeAktivaTetap['metode_penyusutan_id'] == 2) { // Metode Double Declining
            $penyusutan_tahun = (($lastDetailAktiva->nilai_buku_asuransi * ($dataTipeAktivaTetap['rasio'] / 100)));
            $penyusutan_bulan   = $penyusutan_tahun / 12;
        } elseif ($dataTipeAktivaTetap['metode_penyusutan_id'] == 3) { // Metode Tidak terdepresiasi
            $penyusutan_tahun = 0;
            $penyusutan_bulan = 0;
        } elseif ($dataTipeAktivaTetap['metode_penyusutan_id'] == 4) { // Metode Sum Of Year Digit
            $tahun_to_bulan = $dataTipeAktivaTetap['tahun'] * 12;
            $sumI = 0;
            for ($i = 1; $i <= $tahun_to_bulan; ++$i) {
                $sumI += $i;
            }
            $harga_perolehan = $lastDetailAktiva['harga_perolehan'] - $lastDetailAktiva['nilai_sisa'];
            $tahun = $dataTipeAktivaTetap['tahun'] * 12;
            $periode_tahun_ini = $tahun - $lastDetailAktiva['periode'];
            $bagi_tahun_ini   = $periode_tahun_ini / $sumI;
            $penyusutan_bulan = $harga_perolehan * $bagi_tahun_ini;
            $penyusutan_tahun = $penyusutan_bulan * 12;
        }
        return [
            'bulan'  => $penyusutan_bulan,
            'tahun'  => $penyusutan_tahun,
        ];
    }
}
