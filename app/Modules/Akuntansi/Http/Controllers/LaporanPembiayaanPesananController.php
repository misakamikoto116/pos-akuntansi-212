<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\PembiayaanPesanan;
use App\Modules\Akuntansi\Models\PenyelesaianPesanan;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class LaporanPembiayaanPesananController extends Controller
{
    public function __construct(
        Akun $akun,
        Identitas $identitas,
        PembiayaanPesanan $pembiayaanPesanan,
        PenyelesaianPesanan $penyelesaianPesanan
    )
    {
        $this->akun                 =   $akun;
        $this->identitas            =   $identitas;
        $this->pembiayaanPesanan    =   $pembiayaanPesanan;
        $this->penyelesaianPesanan  =   $penyelesaianPesanan;
    }

    protected function identitasPerushaan(){
        $nama_perusahaan        =       $this->identitas->first()->nama_perusahaan;
        return $nama_perusahaan;
    }
    public function dateCarbon($ubah){
        $format     =       Carbon::parse($ubah)->format('d F Y');
        return $format;
    }
    // Laporan Pembiayaan Pesanan - Rincian Pembiayaan Pesanan
    public function pembiayaan_pesanan_per_barang(Request $request){
        $dataPembiayaanPesanan  =   $this->pembiayaanPesanan->with('detail.produk')
                                    ->DateFilter($request)->get();

        $array = [];
        foreach($dataPembiayaanPesanan as $itemPembiayaanPesanan){
            foreach($itemPembiayaanPesanan->detail as $itemPembiayaan){
                if($itemPembiayaan->status == 0){
                    $status =   'Proses';
                }elseif($itemPembiayaan->status == 1){
                    $status =   'Selesai';
                }
                $array [] = [
                    'kode_barang'       =>      $itemPembiayaan->produk->no_barang,
                    'nama_barang'       =>      $itemPembiayaan->produk->keterangan,
                    'no_pembiayaan'     =>      $itemPembiayaanPesanan->batch_no,
                    'keterangan'        =>      $itemPembiayaanPesanan->deskripsi,
                    'tanggal'           =>      $this->dateCarbon($itemPembiayaanPesanan->date),
                    'status'            =>      $status,
                    'kuantitas'         =>      $itemPembiayaan->qty,
                    'satuan'            =>      $itemPembiayaan->produk->unit,
                    'biaya_rata'        =>      $itemPembiayaan->harga_modal,
                    'alokasi_biaya'     =>      $itemPembiayaan->cost,
                ];
            }
        }
        $newCollection    = collect($array)->groupBy('kode_barang');

        $view = [
            'title'           =>      'Laporan Pembiayaan Pesanan per Barang',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembiayaan_pesanan.pembiayaan_pesanan_per_barang')->with($view);
    }
    public function pembiayaan_pesanan_per_beban(Request $request){
        $dataPembiayaanPesanan  =   $this->pembiayaanPesanan->with('beban.akun')
                                    ->DateFilter($request)->get();

        $array = [];
        foreach($dataPembiayaanPesanan as $itemPembiayaanPesanan){
            $sumAmount = 0;
            foreach($itemPembiayaanPesanan->beban as $itemPembiayaan){
                if($itemPembiayaan->status == 0){
                    $status =   'Proses';
                }elseif($itemPembiayaan->status == 1){
                    $status =   'Selesai';
                }
                $sumAmount += $itemPembiayaan->amount;
                $array [] = [
                    'kode_beban'        =>      $itemPembiayaanPesanan->akun->kode_akun,
                    'nama_beban'        =>      $itemPembiayaanPesanan->akun->nama_akun,
                    'no_pembiayaan'     =>      $itemPembiayaanPesanan->batch_no,
                    'keterangan'        =>      $itemPembiayaanPesanan->deskripsi,
                    'tanggal'           =>      $this->dateCarbon($itemPembiayaanPesanan->date),
                    'status'            =>      $status,
                    'catatan'           =>      $itemPembiayaan->catatan,
                    'alokasi_biaya'     =>      $itemPembiayaan->amount,
                    'sum_alokasi_biaya' =>      $sumAmount,
                ];
            }
        }
        $newCollection    = collect($array)->groupBy('kode_beban');

        $view = [
            'title'           =>      'Laporan Pembiayaan Pesanan per Barang',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembiayaan_pesanan.pembiayaan_pesanan_per_beban')->with($view);
    }
    public function pembiayaan_pesanan_per_departemen(){
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembiayaan_pesanan.pembiayaan_pesanan_per_departemen');
    }
    public function pembiayaan_pesanan_per_proyek(){
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembiayaan_pesanan.pembiayaan_pesanan_per_proyek');
    }
    public function pembiayaan_pesanan_yang_telah_selesai(Request $request){
        $dataPenyelesaianPesanan    =   $this->penyelesaianPesanan->with('item.pembiayaanPesanan.detail','item.pembiayaanPesanan.beban','item.pembiayaanPesanan.akun')
                                        ->DateFilter($request)
                                        ->get();
        $array = [];
        foreach($dataPenyelesaianPesanan as $itemPenyelesaianPesanan){
            foreach($itemPenyelesaianPesanan->item as $dataPenyelesaianItem){
                if($dataPenyelesaianItem->pembiayaanPesanan){
                    foreach($dataPenyelesaianItem->pembiayaanPesanan->detail as $dataPenyelesaianDetail){
                        $cost       =   $dataPenyelesaianDetail->cost;
                        $kuantitas  =   $dataPenyelesaianDetail->qty;
                        $satuan     =   $dataPenyelesaianDetail->unit;
                        $hargaModal =   $dataPenyelesaianDetail->harga_modal;
                    }

                    foreach($dataPenyelesaianItem->pembiayaanPesanan->beban as $dataPenyelesaianBeban){
                        $amount  = $dataPenyelesaianBeban->amount;
                    }
                    
                    $total  =   $cost + $amount;

                    if($itemPenyelesaianPesanan->status == 0){
                        $status = 'Proses';
                    }elseif($itemPenyelesaianPesanan->status == 1){
                        $status = 'Selesai';
                    }
                    
                    $array [] = [
                        'no_pembiayaan'     =>      $dataPenyelesaianItem->pembiayaanPesanan->batch_no,
                        'tanggal'           =>      $this->dateCarbon($dataPenyelesaianItem->pembiayaanPesanan->date),
                        'keterangan'        =>      $dataPenyelesaianItem->pembiayaanPesanan->deskripsi,
                        'cost'              =>      $cost,
                        'amount'            =>      $amount,
                        'total_biaya'       =>      $total,
                        'tanggal_selesai'   =>      $this->dateCarbon($itemPenyelesaianPesanan->tgl_selesai),
                        'akun_pembiayaan'   =>      $dataPenyelesaianItem->pembiayaanPesanan->akun->kode_akun,
                        'nama_akun'         =>      $dataPenyelesaianItem->pembiayaanPesanan->akun->nama_akun,
                        'status'            =>      $status,
                        'kuantitas'         =>      $kuantitas,
                        'satuan'            =>      $satuan,
                        'biaya_rata'        =>      $hargaModal,
                        'alokasi_biaya'     =>      $cost,
                    ];
                }
            }
        }
        $newCollection    = collect($array)->groupBy('no_pembiayaan');

        $view = [
            'title'           =>      'Laporan Pembiayaan Pesanan yang Telah Selesai',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembiayaan_pesanan.pembiayaan_pesanan_yang_telah_selesai')->with($view);
    }
    // Laporan Pembiayaan Pesanan - Rincian Penawaran
    public function rincian_jurnal_pembiayaan_pesanan(){
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembiayaan_pesanan.rincian_jurnal_pembiayaan_pesanan');
    }
    public function ringkasan_histori_pembiayaan_pesanan(){
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembiayaan_pesanan.ringkasan_histori_pembiayaan_pesanan');
    }
}
