<?php
namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\PembiayaanPesananRequest;
use App\Modules\Akuntansi\Repositories\PembiayaanPesananRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
class PembiayaanPesananController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Pembiayaan Pesanan';
        $this->request = PembiayaanPesananRequest::class;
        $this->requestField = [
            'batch_no',
            'date',
            'akun_pembiayaan_pesanan_id',
            'deskripsi',
            'gudang_id',

            'produk_id',
            'tanggal_produk',
            'keterangan_produk',
            'qty_produk',
            'unit_produk',
            'amount_produk',
            'sn',

            'akun_beban_id',
            'tanggal_beban',
            'akun_beban_nama',
            'notes_beban',
            'amount_beban',

            'lanjutkan',
            'grandtotal'
        ];
    }
    
    public function formData()
    {
        $user = auth()->user();

        return [
            'gudang' => $this->model->listGudang(),
            'akun' => $this->model->listAkun(),
            'permission' => [
                'buat' => 'buat_pembiayaan_pesanan',
                'ubah' => 'ubah_pembiayaan_pesanan',
                'hapus' => 'hapus_pembiayaan_pesanan',
                'laporan' => 'laporan_pembiayaan_pesanan',
                'lihat' => 'lihat_pembiayaan_pesanan',
                'daftar' => $user->hasPermissionTo('daftar_pembiayaan_pesanan'),
            ],
            'filter'     => [
                                'no_batch',
                                'keterangan_pembayaran',
                                'status_pembiayaan',
                                'tanggal_dari_sampai'
                            ],
            'status_pembiayaan' => [
                0   => 'Sedang Diproses',
                1   => 'Selesai',
                2   => 'Semua',
            ],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function redirectSuccess(Request $request, $result = null)
	{
		if (!empty($result['continue_stat'])) {
			if (1 == $result['continue_stat']) {
				return redirect('/akuntansi/penyelesaian-pesanan/create?' . 'pembiayaan_pesanan_id=' . $result->id)
			->withMessage('Berhasil Menambah/Memperbarui data');
			} else {
				return redirect('/akuntansi/pembiayaan-pesanan/create')->withMessage('Berhasil Menambah/Memperbarui data');
			}
		} else {
			return redirect('/akuntansi/pembiayaan-pesanan')->withMessage('Berhasil Menambah/Memperbarui data');
		}
	}
}
