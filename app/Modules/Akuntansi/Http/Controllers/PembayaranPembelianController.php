<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Models\PembayaranPembelian;
use App\Modules\Akuntansi\Http\Requests\PembayaranPembelianRequest;
use App\Modules\Akuntansi\Repositories\PembayaranPembelianRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class PembayaranPembelianController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Pembayaran Pembelian';
        $this->request = PembayaranPembelianRequest::class;
        $this->requestField = [
            "no_pemasok",
            "pemasok_id",
            "alamat_asal",
            "form_no",
            "payment_date",
            "akun",
            "akun_bank_id",
            "rate",
            "memo",
            "void_cheque",
            "fiscal_payment",
            "cheque_no",
            "cheque_date",
            "cheque_amount",
            "faktur_id",
            "no_faktur",
            "date",
            "due",
            "amount",
            "owing",
            "payment_amount",
            "pay",
            "lanjutkan",                                              
        ];
    }
      public function formData()
    {
        return [
            // 'kategori' => $this->model->listKategoriProduk(),
            // 'gudang' => $this->model->listGudang(),
            'akun' => $this->model->listAkun(),
            'pemasok' => $this->model->listPemasok(),
            'idPrediction'   => $this->model->idPrediction(), 
            'filter' => ['tanggal_dari_sampai', 'pemasok_select','no_cek'],               
            // 'produk' => $this->model->listProduk(),
            //frm session
            // 'from_session' => session()->get('penawaran_penjualan'.auth()->user()->id),
            // 'tipePelanggan' => $this->model->listTipePelanggan(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function redirectSuccess(Request $request, $result = null)
    {
        if(!empty($result['continue_stat'])){
            return redirect('/akuntansi/pembayaran-pembelian/create')->withMessage('Berhasil Menambah/Memperbarui data');;
        }else{
            return redirect('/akuntansi/pembayaran-pembelian')->withMessage('Berhasil Menambah/Memperbarui data');;
        }
    }
}
