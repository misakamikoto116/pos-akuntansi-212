<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\GudangRequest;
use App\Modules\Akuntansi\Repositories\GudangRepository;
use Generator\Interfaces\RepositoryInterface;
class GudangController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Gudang';
        $this->request = GudangRequest::class;
        $this->requestField = ['nama','keterangan','alamat','penanggung_jawab'];
    }
}
