<?php
namespace App\Modules\Akuntansi\Http\Controllers;

use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
use App\Modules\Akuntansi\Models\Penerimaan;
use App\Modules\Akuntansi\Models\DetailBukuMasuk;
use App\Modules\Akuntansi\Http\Requests\PenerimaanRequest;
use Cart;

class PenerimaanController extends Controller
{
	public function __construct(RepositoryInterface $model)
	{
		$this->role         = 'akuntansi';
		$this->model        = $model;
		$this->title        = 'Data Penerimaan';
		$this->request      = PenerimaanRequest::class;
		$this->requestField = ['akun_id',
			'no_faktur',
			'no_cek',
			'tanggal',
			'peruntukan',
			'keterangan',
			'nominal',
		];
		// Cart::instance('penerimaan')->destroy(); masukkan di embded edit & tambah
	}

	public function formData()
	{
		$user = auth()->user();

		return [
			'listAkun'   => $this->model->listAkun(),
			'listAkunId' => $this->model->listAkunId(),
        	'permission' => [
				'buat' => 'buat_penerimaan_lain',
				'ubah' => 'ubah_penerimaan_lain',
				'hapus' => 'hapus_penerimaan_lain',
				'laporan' => 'laporan_penerimaan_lain',
				'lihat' => 'lihat_penerimaan_lain',
				'daftar' => $user->hasPermissionTo('daftar_penerimaan_lain'),
			],
			'filter'	 => [
								'no_pembayaran',
								'keterangan_pembayaran',
								'tanggal_dari_sampai'
    						],
		];
	}

	public function index()
    {
        return parent::index()->with($this->formData());
	}
	
	// ini sebelumnya di inject ke index
	public function embedDataToEdit($id)
	{
		$cart = Cart::instance('penerimaan');
		$cart->destroy();
		$data = Penerimaan::findOrFail($id);
		$anak = DetailBukuMasuk::where('buku_masuk_id', $data->id)->get();
		foreach ($anak as $child) {
			$uniqueId    = Cart::instance('penerimaan')->content()->pluck('id')->toArray();
			$newUniqueId = uniqid();
			while (in_array($newUniqueId, $uniqueId)) {
				$newUniqueId = uniqid();
			}
			$cc = [
				'name'    => $newUniqueId,
				'qty'     => 1,
				'id'      => $newUniqueId,
				'options' => [
					'akun_id'          => $child->akun->kode_akun,
					'nama_detail_akun' => $child->akun->nama_akun,
					'catatan_detail'   => $child->catatan,
				],
				'price' => $child->nominal,
			];
			$cart->add($cc);
		}

		return [$cart];
	}

	public function sessionApi(Request $request)
	{
		try {
			$uniqueId    = Cart::instance('penerimaan')->content()->pluck('id')->toArray();
			$newUniqueId = uniqid();
			while (in_array($newUniqueId, $uniqueId)) {
				$newUniqueId = uniqid();
			}

			$data = [
				'name'    => $newUniqueId,
				'qty'     => 1,
				'id'      => $newUniqueId,
				'options' => [
					'akun_id'          => $request->akun_detail_id,
					'nama_detail_akun' => $request->nama_detail_akun,
					'catatan_detail'   => $request->catatan_detail,
				],
				'price' => $request->nominal_detail,
			];

			$cart = Cart::instance('penerimaan')->add($data);

			$response = [
				'rowId'   => $cart->rowId,
				'name'    => $cart->name,
				'qty'     => $cart->qty,
				'id'      => $cart->id,
				'options' => [
					'akun_id'          => $cart->options->akun_id,
					'nama_detail_akun' => $request->nama_detail_akun,
					'catatan_detail'   => $request->catatan_detail,
				],
				'price' => $request->nominal_detail,
			];
		} catch (Exception $e) {
			$response = [];
		}

		return response()->json($response);
	}

	public function sessionUpdate(Request $request)
	{
		$data = [
			'options' => [
				'akun_id'          => $request->akun_detail_id,
				'nama_detail_akun' => $request->nama_detail_akun,
				'catatan_detail'   => $request->catatan_detail,
			],
			'price' => $request->nominal_detail,
		];

		$cart = Cart::instance('penerimaan')->update($request->rowId, $data);

		$response = [
			'rowId'   => $cart->rowId,
			'name'    => $cart->name,
			'qty'     => $cart->qty,
			'id'      => $cart->id,
			'options' => [
				'akun_id'          => $cart->options->akun_id,
				'nama_detail_akun' => $request->nama_detail_akun,
				'catatan_detail'   => $request->catatan_detail,
			],
			'price' => $request->nominal_detail,
		];

		return response()->json(['sukses' => true, 'data' => $response]);
	}

	public function sessionDelete(Request $request)
	{
		Cart::instance('penerimaan')->remove($request->id);

		return response()->json(['sukses' => true, 'data' => []]);
	}

	public function jurnal($id)
	{
		$buku_masuk        = Penerimaan::findOrFail($id);
		$detail_buku_masuk = DetailBukuMasuk::where('buku_masuk_id', $buku_masuk->id)->get();

		return view('akuntansi::penerimaan.jurnal', compact('buku_masuk', 'detail_buku_masuk'));
	}

	public function getLaporanPenerimaan(Request $request)
	{
		if(empty($request->all())){
			$request['start-date'] = date('Y-m-1');
			$request['end-date'] = date('Y-m-t');
		}

		return $this->model->laporanPenerimaan($request);
	}

	public function getCetakPenerimaan(Request $request)
    {
        return $this->model->cetakPenerimaan($request);
    }
}
