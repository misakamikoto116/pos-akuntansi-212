<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\PembayaranPembelian;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LaporanAkunHutangPemasokController extends Controller
{
    public function __construct(FakturPembelian $fakturPembelian, Identitas $identitas,
                                InformasiPemasok $informasiPemasok, PembayaranPembelian $pembayaranPembelian)
    {
        $this->fakturPembelian         =   $fakturPembelian;
        $this->identitas               =   $identitas;
        $this->informasiPemasok        =   $informasiPemasok;
        $this->pembayaranPembelian     =   $pembayaranPembelian;
    }
    // Laporan Akun Hutang dan Pemasok - Rincian Akun Hutang
    public function hutang_beredar(Request $request){
        $arrayItems                 = [];
        $nama_perusahaan            = $this->identitas->first();
        $grand_sum_total            = 0;    
        $grand_sum_hutang_asing     = 0;
        $pemasok                    = $this->informasiPemasok->whereHas('fakturPembelian')->with(['fakturPembelian' => function($query) use($request){
            $query->where('uang_muka', 0)->reportInvoiceFilter($request);
        }, 'fakturPembelian.invoice' => function ($query1) use ($request){
            $query1->reportInvoiceFilter($request);
        }, 'fakturPembelian.returPembelian' => function ($query2) use ($request){
            $query2->reportInvoiceFilter($request);
        }])->reportInvoiceFilter($request)->get();

        foreach ($pemasok as $key => $data_pemasok) {
            $arrayFaktur        = [];
            $sum_hutang_asing   = 0;
            $sum_total          = 0;
            if ($data_pemasok->fakturPembelian) {
                foreach ($data_pemasok->fakturPembelian as $key1 => $data_faktur) {
                    $tanggal_faktur             = Carbon::parse($data_faktur->invoice_date);

                    $temp_tanggal_faktur        = Carbon::parse($data_faktur->invoice_date);

                    $hutang_asing               = $data_faktur->faktur_kurang_uang_muka - $data_faktur->invoice->sum('payment_amount') - $data_faktur->returPembelian->sum('total');
                    $tgl_jatuh_tempo            = $tanggal_faktur->diff(Carbon::parse($request['start-date']))->days;

                    if (!empty($data_faktur->syaratPembayaran)) {
                        $jatuh_tempo = $temp_tanggal_faktur->addDays($data_faktur->syaratPembayaran->jatuh_tempo);
                    }else {
                        $jatuh_tempo = $temp_tanggal_faktur;
                    }

                    $sum_hutang_asing           += $hutang_asing;
                    $sum_total                  += $data_faktur->total;

                    $grand_sum_total            += $data_faktur->total;
                    $grand_sum_hutang_asing     += $hutang_asing;


                    $arrayFaktur[] = [
                        'no_faktur'         => $data_faktur->no_faktur,
                        'tanggal_faktur'    => $tanggal_faktur->format('d F Y'),
                        'jatuh_tempo'       => $jatuh_tempo->format('d F Y'),
                        'nilai_faktur'      => number_format($data_faktur->total),
                        'hutang_asing'      => number_format($hutang_asing),
                        'hutang_pajak'      => 0,
                        'tgl_jatuh_tempo'   => $tgl_jatuh_tempo,
                    ];
                }
            }
            $arrayItems[] = [
                'nomor_pemasok'         => $data_pemasok->no_pemasok,
                'nama_pemasok'          => $data_pemasok->nama,
                'faktur'                => $arrayFaktur,
                'sum_hutang_asing'      => number_format($sum_hutang_asing),
                'sum_total'             => number_format($sum_total),
            ];
        }

        $typeFilter = [
            'single_date',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_akun_hutang_pemasok_in_form_filter';

        $view = [
            'title'                     => 'Laporan Hutang Beredar',
            'perusahaan'                => $nama_perusahaan,
            'items'                     => $arrayItems,
            'tanggal'                   => Carbon::parse($request['start-date'])->format('d F Y'),
            'grand_sum_total'           => number_format($grand_sum_total),
            'grand_sum_hutang_asing'    => number_format($grand_sum_hutang_asing),
            'typeFilter'                => $typeFilter,
            'formFilter'                => $formFilter,
        ];

    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.hutang_beredar')->with($view);
    }

    public function ringkasan_umur_hutang(Request $request)
    {
        $arrayItems                         = [];
        $nama_perusahaan                    = $this->identitas->first();
        $tanggal                            = Carbon::parse($request['start-date']);
        $grand_sum_total                    = 0;
        $grand_sum_faktur_kurang_30         = 0;
        $grand_sum_faktur_kurang_60         = 0;
        $grand_sum_faktur_kurang_90         = 0;
        $grand_sum_faktur_kurang_120        = 0;
        $grand_sum_faktur_lebih_120         = 0;
        $grand_sum_faktur_belum             = 0;
        $pemasok                            = $this->informasiPemasok->whereHas('fakturPembelian')->with(['fakturPembelian' => function($query) use($request){
            $query->reportRincianFilter($request);
        }, 'fakturPembelian.invoice' => function ($query1) use ($request)
        {
            $query1->reportRincianFilter($request);
        }, 'fakturPembelian.returPembelian' => function ($query2) use ($request)
        {
            $query2->reportRincianFilter($request);
        }])->reportRincianFilter($request)->get();


        foreach ($pemasok as $key => $data_pemasok) {
            $sum_total                = 0;
            $sum_faktur_kurang_30     = 0;
            $sum_faktur_kurang_60     = 0;
            $sum_faktur_kurang_90     = 0;
            $sum_faktur_kurang_120    = 0;
            $sum_faktur_lebih_120     = 0;
            $sum_faktur_belum         = 0;
            if ($data_pemasok->fakturPembelian) {
                foreach ($data_pemasok->fakturPembelian as $key1 => $data_faktur) {
                    $tanggal_faktur             = Carbon::parse($data_faktur->invoice_date);
                    $nilai_faktur               = $data_faktur->faktur_kurang_uang_muka - $data_faktur->invoice->sum('payment_amount') - $data_faktur->returPembelian->sum('total');
                    $sum_total                 += $nilai_faktur;
                    $grand_sum_total           += $nilai_faktur;
                    $selisih_hari               = $tanggal->diff($tanggal_faktur)->days;

                    if ($nilai_faktur >= 1) {
                        if ($selisih_hari == 0) {
                            $sum_faktur_belum               += $nilai_faktur;
                            $grand_sum_faktur_belum         += $nilai_faktur;
                        }else if ($selisih_hari <= 30) {
                            $sum_faktur_kurang_30           += $nilai_faktur;
                            $grand_sum_faktur_kurang_30     += $nilai_faktur;
                        }else if ($selisih_hari <= 60 && $selisih_hari > 30) {
                            $sum_faktur_kurang_60           += $nilai_faktur;
                            $grand_sum_faktur_kurang_60     += $nilai_faktur;
                        }else if ($selisih_hari <= 90 && $selisih_hari > 60) {
                            $sum_faktur_kurang_90           += $nilai_faktur;
                            $grand_sum_faktur_kurang_90     += $nilai_faktur;
                        }else if ($selisih_hari <= 120 && $selisih_hari > 90) {
                            $sum_faktur_kurang_120          += $nilai_faktur;
                            $grand_sum_faktur_kurang_120    += $nilai_faktur;
                        }else if ($selisih_hari > 120) {
                            $sum_faktur_lebih_120           += $nilai_faktur;
                            $grand_sum_faktur_lebih_120     += $nilai_faktur;
                        }
                    }

                }
            }
            $arrayItems[] = [
                'nama_pemasok'          => $data_pemasok->nama,
                'sum_total'             => number_format($sum_total),
                'sum_faktur_kurang_30'  => number_format($sum_faktur_kurang_30),
                'sum_faktur_kurang_60'  => number_format($sum_faktur_kurang_60),
                'sum_faktur_kurang_90'  => number_format($sum_faktur_kurang_90),
                'sum_faktur_kurang_120' => number_format($sum_faktur_kurang_120),
                'sum_faktur_lebih_120'  => number_format($sum_faktur_lebih_120),
                'sum_faktur_belum'      => number_format($sum_faktur_belum),
            ];
        }

        $typeFilter = [
            'single_date',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_akun_hutang_pemasok_in_form_filter';

        $view = [
            'nama_perusahaan'               => $nama_perusahaan,
            'tanggal'                       => $tanggal->format('d F Y'),
            'items'                         => $arrayItems,
            'grand_sum_total'               => number_format($grand_sum_total),
            'grand_sum_faktur_kurang_30'    => number_format($grand_sum_faktur_kurang_30),
            'grand_sum_faktur_kurang_60'    => number_format($grand_sum_faktur_kurang_60),
            'grand_sum_faktur_kurang_90'    => number_format($grand_sum_faktur_kurang_90),
            'grand_sum_faktur_kurang_120'   => number_format($grand_sum_faktur_kurang_120),
            'grand_sum_faktur_lebih_120'    => number_format($grand_sum_faktur_lebih_120),
            'grand_sum_faktur_belum'        => number_format($grand_sum_faktur_belum),
            'typeFilter'                    => $typeFilter,
            'formFilter'                    => $formFilter,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.ringkasan_umur_hutang')->with($view);
    }

    public function rincian_umur_hutang(Request $request)
    {
        $arrayItems      = [];
        $nama_perusahaan = $this->identitas->first();
        $tanggal         = Carbon::parse($request['start-date']);  
        $pemasok         = $this->informasiPemasok->whereHas('fakturPembelian')->with(['fakturPembelian' => function($query) use($request){
            $query->reportRincianFilter($request);
            }, 'fakturPembelian.invoice' => function ($query1) use ($request)
            {
                $query1->reportRincianFilter($request);
            }, 'fakturPembelian.returPembelian' => function ($query2) use ($request)
            {
                $query2->reportRincianFilter($request);
            }])->reportRincianFilter($request)->get();

        foreach ($pemasok as $key => $data_pemasok) {
            $arrayFaktur        = [];
            $sum_hutang_asing   = 0;
            $sum_total          = 0;
            $sum_faktur_kurang_30     = 0;
            $sum_faktur_kurang_60     = 0;
            $sum_faktur_kurang_90     = 0;
            $sum_faktur_kurang_120    = 0;
            $sum_faktur_lebih_120     = 0;
            $sum_faktur_belum         = 0;
            if ($data_pemasok->fakturPembelian) {
                foreach ($data_pemasok->fakturPembelian as $key1 => $data_faktur) {
                    $tanggal_faktur             = Carbon::parse($data_faktur->invoice_date);
                    $nilai_faktur               = $data_faktur->faktur_kurang_uang_muka - $data_faktur->invoice->sum('payment_amount') - $data_faktur->returPembelian->sum('total');
                    $sum_total                  += $nilai_faktur;
                    $selisih_hari               = $tanggal->diff($tanggal_faktur)->days;

                    $nilai_faktur_belum         = 0;
                    $nilai_faktur_kurang_30     = 0;
                    $nilai_faktur_kurang_60     = 0;
                    $nilai_faktur_kurang_90     = 0;
                    $nilai_faktur_kurang_120    = 0;
                    $nilai_faktur_lebih_120     = 0;

                    if ($nilai_faktur >= 1) {
                        if ($selisih_hari == 0) {
                            $nilai_faktur_belum     = $nilai_faktur;
                            $sum_faktur_belum      += $nilai_faktur;
                        }else if ($selisih_hari <= 30) {
                            $nilai_faktur_kurang_30 = $nilai_faktur;
                            $sum_faktur_kurang_30  += $nilai_faktur;
                        }else if ($selisih_hari <= 60 && $selisih_hari > 30) {
                            $nilai_faktur_kurang_60 = $nilai_faktur;
                            $sum_faktur_kurang_60  += $nilai_faktur;
                        }else if ($selisih_hari <= 90 && $selisih_hari > 60) {
                            $nilai_faktur_kurang_90 = $nilai_faktur;
                            $sum_faktur_kurang_90  += $nilai_faktur;
                        }else if ($selisih_hari <= 120 && $selisih_hari > 90) {
                            $nilai_faktur_kurang_120 = $nilai_faktur;
                            $sum_faktur_kurang_120  += $nilai_faktur;
                        }else if ($selisih_hari > 120) {
                            $nilai_faktur_lebih_120 = $nilai_faktur;
                            $sum_faktur_lebih_120  += $nilai_faktur;
                        }

                        $arrayFaktur[] = [
                            'no_faktur'                     => $data_faktur->no_faktur,
                            'tanggal_faktur'                => $tanggal_faktur->format('d F Y'),
                            'nilai_faktur'                  => number_format($nilai_faktur),
                            'nilai_faktur_kurang_30'        => number_format($nilai_faktur_kurang_30),
                            'nilai_faktur_kurang_60'        => number_format($nilai_faktur_kurang_60),
                            'nilai_faktur_kurang_90'        => number_format($nilai_faktur_kurang_90),
                            'nilai_faktur_kurang_120'       => number_format($nilai_faktur_kurang_120),
                            'nilai_faktur_lebih_120'        => number_format($nilai_faktur_lebih_120),
                            'nilai_faktur_belum'            => number_format($nilai_faktur_belum),
                        ];
                    }

                }
            }
            $arrayItems[] = [
                'nomor_pemasok'         => $data_pemasok->no_pemasok,
                'nama_pemasok'          => $data_pemasok->nama,
                'faktur'                => $arrayFaktur,
                'sum_total'             => number_format($sum_total),
                'sum_faktur_kurang_30'  => number_format($sum_faktur_kurang_30),
                'sum_faktur_kurang_60'  => number_format($sum_faktur_kurang_60),
                'sum_faktur_kurang_90'  => number_format($sum_faktur_kurang_90),
                'sum_faktur_kurang_120' => number_format($sum_faktur_kurang_120),
                'sum_faktur_lebih_120'  => number_format($sum_faktur_lebih_120),
                'sum_faktur_belum'      => number_format($sum_faktur_belum),
            ];
        }

        $typeFilter = [
            'single_date',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_akun_hutang_pemasok_in_form_filter';

        $view = [
            'nama_perusahaan'   => $nama_perusahaan,
            'per_tanggal'       => $tanggal->format('d F Y'),
            'items'             => $arrayItems,
            'typeFilter'        => $typeFilter,
            'formFilter'        => $formFilter,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.rincian_umur_hutang')->with($view);
    }

    public function ringkasan_buku_besar_pembantu_hutang(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first();
        $tanggal_awal                       = Carbon::parse($request['start-date-rincian']);
        $tanggal_akhir                      = Carbon::parse($request['end-date-rincian']);
        $pemasok                            = $this->informasiPemasok->filterAvailablePemasok($request)->with(['fakturPembelian' => function($query) use($request){
            $query->reportPenerimaanFilter($request);
        }, 'pembayaranPembelian' => function ($query1) use ($request)
        {
            $query1->reportPenerimaanFilter($request);
        }, 'returPembelian' => function ($query2) use ($request)
        {
            $query2->reportPenerimaanFilter($request);
        }])->get();

        $dataPemasok = $pemasok->map(function ($item, $key)
        {
            $transaksi_uang_muka = $item->fakturPembelian->where('uang_muka','0')->map(function ($item_uang_muka)
            {
                $item_uang_muka->sum_uang_muka =  $item_uang_muka->transaksiUangMukaPemasok->sum('jumlah');
                return $item_uang_muka;
            });
            $saldo_pelanggan                    = $item->detailInformasiPemasok->sum('saldo_awal') + ($item->fakturPembelian->where('uang_muka','0')->sum('faktur_kurang_uang_muka') + $transaksi_uang_muka->sum('sum_uang_muka') + -$item->pembayaranPembelian->sum('cheque_amount') + -$item->returPembelian->sum('total'));

            $item->saldo_awal                   = $item->detailInformasiPemasok->sum('saldo_awal');
            $item->sum_faktur_pembelian         = $item->fakturPembelian->where('uang_muka','0')->sum('faktur_kurang_uang_muka') + $transaksi_uang_muka->sum('sum_uang_muka');
            $item->sum_pembayaran_pembelian     = $item->pembayaranPembelian->sum('cheque_amount');
            $item->sum_retur_pembelian          = $item->returPembelian->sum('total');
            $item->sum_uang_muka                = 0;
            $item->sum_bukti_jurnal             = 0;
            $item->saldo                        = $saldo_pelanggan;
            return $item;
        });

        $typeFilter = [
            'double_date_not_month',
            'pemasok_transaksi',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_akun_hutang_pemasok_in_form_filter';

        $view = [
            'nama_perusahaan'       => $nama_perusahaan,
            'tanggal_awal'          => $tanggal_awal->format('d F Y'),
            'tanggal_akhir'         => $tanggal_akhir->format('d F Y'),
            'items'                 => $dataPemasok,
            'typeFilter'            => $typeFilter,
            'formFilter'            => $formFilter,
        ];
        // dd($view);
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.ringkasan_buku_besar_pembantu_hutang')->with($view);
    }

    public function rincian_buku_besar_pembantu_hutang(Request $request)
    {
        $arrayItems = [];
        $nama_perusahaan                    = $this->identitas->first();
        $tanggal_awal                       = Carbon::parse($request['start-date-rincian']);
        $tanggal_akhir                      = Carbon::parse($request['end-date-rincian']);
        $pemasok                            = $this->informasiPemasok->filterAvailablePemasok($request)->with(['fakturPembelian' => function($query) use($request){
            $query->reportPenerimaanFilter($request);
        }, 'pembayaranPembelian' => function ($query1) use ($request)
        {
            $query1->reportPenerimaanFilter($request);
        }, 'returPembelian' => function ($query2) use ($request)
        {
            $query2->reportPenerimaanFilter($request);
        }])->get();

        $dataPemasok = $pemasok->map(function ($item, $key)
        {
            $arrayFaktur = [];
            foreach ($item->fakturPembelian as $key => $data_faktur) {
                $temp = [
                    'tanggal'               => $data_faktur->invoice_date,
                    'sumber'                => 'Faktur Pembelian',
                    'no_sumber'             => $data_faktur->no_faktur,
                    'keterangan_sumber'     => $data_faktur->keterangan !== null ? $data_faktur->keterangan : 'Faktur Pembelian : '.$data_faktur->no_faktur,
                    'nilai_sumber'          => $data_faktur->faktur_kurang_uang_muka,
                    'nilai_pajak'           => 0,
                    'saldo_asing'           => 0,
                    'saldo_pajak'           => 0,
                ];
                $arrayFaktur[] = $temp;
                if ($data_faktur->uang_muka) {
                    $temp['nilai_sumber'] = -$temp['nilai_sumber'];
                    $arrayFaktur[] = $temp;
                }
                if ($data_faktur->transaksiUangMukaPemasok->isNotEmpty()) {
                    foreach ($data_faktur->transaksiUangMukaPemasok as $key => $uang_muka) {
                        $temp['nilai_sumber'] = $uang_muka->jumlah;
                    }
                    $arrayFaktur[] = $temp;
                }
            }

            foreach ($item->pembayaranPembelian as $key => $data_pembayaran) {
                $arrayFaktur[] = [
                    'tanggal'               => $data_pembayaran->cheque_date,
                    'sumber'                => 'Pembayaran Pembelian',
                    'no_sumber'             => $data_pembayaran->form_no,
                    'keterangan_sumber'     => $data_pembayaran->memo !== null ? $data_pembayaran->memo : 'Pembayaran Pembelian : '.$data_pembayaran->form_no,
                    'nilai_sumber'          => -$data_pembayaran->cheque_amount,
                    'nilai_pajak'           => 0,
                    'saldo_asing'           => 0,
                    'saldo_pajak'           => 0,
                ];
            }

            foreach ($item->returPembelian as $key => $data_retur) {
                $arrayFaktur[] = [
                    'tanggal'               => $data_retur->tanggal,
                    'sumber'                => 'Retur Pembelian',
                    'no_sumber'             => $data_retur->return_no,
                    'keterangan_sumber'     => $data_retur->keterangan !== null ? $data_retur->keterangan : 'Retur Pembelian : '.$data_retur->return_no,
                    'nilai_sumber'          => -$data_retur->total,
                    'nilai_pajak'           => 0,
                    'saldo_asing'           => 0,
                    'saldo_pajak'           => 0,
                ];
            }
            $item->data = collect($arrayFaktur)->sortBy('tanggal');
            return $item;
        });

        $typeFilter = [
            'double_date_not_month',
            'pemasok_transaksi',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_akun_hutang_pemasok_in_form_filter';

        $view = [
            'nama_perusahaan'       => $nama_perusahaan,
            'tanggal_awal'          => $tanggal_awal->format('d F Y'),
            'tanggal_akhir'         => $tanggal_akhir->format('d F Y'),
            'items'                 => $dataPemasok,
            'typeFilter'            => $typeFilter,
            'formFilter'            => $formFilter
        ];
        // dd($view);
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.rincian_buku_besar_pembantu_hutang')->with($view);
    }

    public function histori_hutang_pelanggan(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first()->nama_perusahaan ?? null;
        $pemasok                            = $this->informasiPemasok->orWhereHas('fakturPembelian')->orWhereHas('pembayaranPembelian')->orWhereHas('returPembelian')->with(['fakturPembelian' => function($query) use($request){
            $query->reportPenerimaanFilter($request);
        }, 'pembayaranPembelian' => function ($query1) use ($request)
        {
            $query1->reportPenerimaanFilter($request);
        }, 'returPembelian' => function ($query2) use ($request)
        {
            $query2->reportPenerimaanFilter($request);
        }])->get();

        $typeFilter = [
            'double_date_not_month',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_akun_hutang_pemasok_in_form_filter';

        $view = [
            'title'                 => 'Histori Hutang Pemasok',
            'nama_perusahaan'       => $nama_perusahaan,
            'tanggal_awal'          => Carbon::parse($request['start-date-rincian'])->format('d F Y'),
            'tanggal_akhir'         => Carbon::parse($request['end-date-rincian'])->format('d F Y'),
            'items'                 => $this->historiHutangPemasok($pemasok),
            'total_all'             => $this->historiHutangPemasok($pemasok)->sum('total_perubahan'),
            'count_faktur'          => $this->historiHutangPemasok($pemasok)->sum('count_faktur'),
            'count_pembayaran'      => $this->historiHutangPemasok($pemasok)->sum('count_pembayaran'),
            'count_retur'           => $this->historiHutangPemasok($pemasok)->sum('count_retur'),
            'sum_faktur_all'        => $this->historiHutangPemasok($pemasok)->sum('sum_faktur'),
            'sum_pembayaran_all'    => $this->historiHutangPemasok($pemasok)->sum('sum_pembayaran'),
            'sum_retur_all'         => $this->historiHutangPemasok($pemasok)->sum('sum_retur'),
            'typeFilter'            => $typeFilter,
            'formFilter'            => $formFilter,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.histori_hutang_pelanggan')->with($view);
    }

    public function ringkasan_pembayaran_faktur(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first()->nama_perusahaan ?? null;
        $faktur                             = $this->fakturPembelian->where('uang_muka', 0)->with('invoice')->reportPenerimaanFilter($request)->get();

        $typeFilter = [
            'double_date_not_month',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_akun_hutang_pemasok_in_form_filter';

        $view = [
            'title'             => 'Rincian Pembayaran Faktur',
            'nama_perusahaan'   => $nama_perusahaan,
            'tanggal_awal'      => Carbon::parse($request['start-date-rincian'])->format('d F Y'),
            'tanggal_akhir'     => Carbon::parse($request['end-date-rincian'])->format('d F Y'),
            'items'             => $this->ringkasanPembayaranFaktur($faktur),
            'typeFilter'        => $typeFilter,
            'formFilter'        => $formFilter
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.ringkasan_pembayaran_faktur')->with($view);
    }

    public function rincian_pembayaran_faktur(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first()->nama_perusahaan ?? null;
        $pemasok                            = $this->informasiPemasok->whereHas('fakturPembelian')->with(['fakturPembelian' => function($query) use($request){
            $query->where('uang_muka',0)->reportPenerimaanFilter($request);
        }, 'fakturPembelian.invoice'])->get();

        $typeFilter = [
            'double_date_not_month',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_akun_hutang_pemasok_in_form_filter';

        $view = [
            'title'             => 'Rincian Pembayaran Faktur',
            'nama_perusahaan'   => $nama_perusahaan,
            'tanggal_awal'      => Carbon::parse($request['start-date-rincian'])->format('d F Y'),
            'tanggal_akhir'     => Carbon::parse($request['end-date-rincian'])->format('d F Y'),
            'items'             => $this->rincianPembayaranFaktur($pemasok),
            'typeFilter'        => $typeFilter,
            'formFilter'        => $formFilter
        ];
        // dd($view);
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.rincian_pembayaran_faktur')->with($view);
    }

    public function laporan_pemasok(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first()->nama_perusahaan ?? null;
        $pemasok                            = $this->informasiPemasok->filterAvailablePemasok($request)->with(['fakturPembelian' => function($query) use($request){
            $query->reportPenerimaanFilter($request);
        }, 'pembayaranPembelian' => function ($query1) use ($request)
        {
            $query1->reportPenerimaanFilter($request);
        }, 'returPembelian' => function ($query2) use ($request)
        {
            $query2->reportPenerimaanFilter($request);
        }, 'detailInformasiPemasok'])->get();

        $typeFilter = [
            'double_date_not_month',
            'pemasok_transaksi',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_akun_hutang_pemasok_in_form_filter';

        $view = [
            'title'                 => 'Laporan Pemasok',
            'nama_perusahaan'       => $nama_perusahaan,
            'tanggal_awal'          => Carbon::parse($request['start-date-rincian'])->format('d F Y'),
            'tanggal_akhir'         => Carbon::parse($request['end-date-rincian'])->format('d F Y'),
            'items'                 => $this->laporanPemasok($pemasok),
            'typeFilter'            => $typeFilter,
            'formFilter'            => $formFilter
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_pemasok')->with($view);
    }

    // Laporan Akun Hutang dan Pemasok - Rincian Pemasok
    public function daftar_pemasok(Request $request)
    {
        $arrayItems                         = [];
        $nama_perusahaan                    = $this->identitas->first();
        $tanggal                            = Carbon::parse($request['start-date']);
        $grand_sum_total                    = 0;
        $pemasok                            = $this->informasiPemasok->whereHas('fakturPembelian')->with(['fakturPembelian' => function($query) use($request){
            $query->reportRincianFilter($request);
        }, 'fakturPembelian.invoice' => function ($query1) use ($request)
        {
            $query1->reportRincianFilter($request);
        }, 'fakturPembelian.returPembelian' => function ($query2) use ($request)
        {
            $query2->reportRincianFilter($request);
        }])->reportRincianFilter($request)->get();


        foreach ($pemasok as $key => $data_pemasok) {
            $sum_total                = 0;
            if ($data_pemasok->fakturPembelian) {
                foreach ($data_pemasok->fakturPembelian as $key1 => $data_faktur) {
                    $nilai_faktur               = $data_faktur->faktur_kurang_uang_muka - $data_faktur->invoice->sum('payment_amount') - $data_faktur->returPembelian->sum('total');
                    $sum_total                  += $nilai_faktur;
                    $grand_sum_total            += $nilai_faktur;
                }
            }
            $arrayItems[] = [
                'nomor_pemasok'       => $data_pemasok->no_pemasok,
                'nama_pemasok'        => $data_pemasok->nama,
                'sum_total'           => number_format($sum_total),
            ];
        }

        $typeFilter = [
            'single_date',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_akun_hutang_pemasok_in_form_filter';

        $view = [
            'nama_perusahaan'               => $nama_perusahaan,
            'tanggal'                       => $tanggal->format('d F Y'),
            'items'                         => $arrayItems,
            'grand_sum_total'               => number_format($grand_sum_total),
            'typeFilter'                    => $typeFilter,
            'formFilter'                    => $formFilter,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.daftar_pemasok')->with($view);
    }

    public function ringkasan_pembayaran_pembelian(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first();
        $tanggal_awal                       = Carbon::parse($request['start-date-rincian']);
        $tanggal_akhir                      = Carbon::parse($request['end-date-rincian']);
        $pembayaran                         = $this->pembayaranPembelian->with('invoice.fakturPembelian')->reportPenerimaanFilter($request)->get();

        $typeFilter = [
            'double_date_not_month',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_akun_hutang_pemasok_in_form_filter';

        $view = [
            'nama_perusahaan'               => $nama_perusahaan,
            'items'                         => $pembayaran,
            'tanggal_awal'                  => $tanggal_awal->format('d F Y'),
            'tanggal_akhir'                 => $tanggal_akhir->format('d F Y'),
            'typeFilter'                    => $typeFilter,
            'formFilter'                    => $formFilter
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.ringkasan_pembayaran_pembelian')->with($view);
    }

    public function rincian_pembayaran_pembelian(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first();
        $tanggal_awal                       = Carbon::parse($request['start-date-rincian']);
        $tanggal_akhir                      = Carbon::parse($request['end-date-rincian']);
        $pembayaran                         = $this->pembayaranPembelian->with('invoice.fakturPembelian')->reportPenerimaanFilter($request)->get();

        $typeFilter = [
            'double_date_not_month',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.laporan_akun_hutang_pemasok_in_form_filter';

        $view = [
            'nama_perusahaan'               => $nama_perusahaan,
            'items'                         => $pembayaran,
            'tanggal_awal'                  => $tanggal_awal->format('d F Y'),
            'tanggal_akhir'                 => $tanggal_akhir->format('d F Y'),
            'typeFilter'                    => $typeFilter,
            'formFilter'                    => $formFilter
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_hutang_pemasok.rincian_pembayaran_pembelian')->with($view);
    }

    // Fungsi Fungsi
    public function rincianPembayaranFaktur($pemasok)
    {
        return $pemasok->map(function ($item_pemasok) 
        {
            $data_faktur = $item_pemasok->fakturPembelian->map(function ($item_faktur_pembelian)
            {
                $data_invoice_pembayaran = $item_faktur_pembelian->invoice->map(function ($item_invoice_pembelian)
                {
                    return [
                        'form_no'           => $item_invoice_pembelian->pembayaranPembelian->form_no,
                        'cheque_no'         => $item_invoice_pembelian->pembayaranPembelian->cheque_no,
                        'cheque_date'       => $item_invoice_pembelian->pembayaranPembelian->cheque_date,
                        'payment_amount'    => $item_invoice_pembelian->payment_amount,
                        'pajak'             => 0,
                        'diskon'            => 0,    
                    ];
                });
                return [
                    'no_faktur'                 => $item_faktur_pembelian->no_faktur,
                    'invoice_date'              => $item_faktur_pembelian->invoice_date,
                    'data_invoice_pembayaran'   => $data_invoice_pembayaran,
                    'sum_invoice_pembayaran'    => $data_invoice_pembayaran->sum('payment_amount'),
                    'sum_invoice_diskon'        => $data_invoice_pembayaran->sum('diskon'),
                    'total_faktur'              => $item_faktur_pembelian->total,
                    'last_owing'                => $item_faktur_pembelian->owing_after_retur_pemasok,
                ];
            });

            return [
                'nama_pemasok'          => $item_pemasok->nama,
                'data_faktur'           => $data_faktur,
            ];
        });
    }

    public function ringkasanPembayaranFaktur($faktur)
    {
        return $faktur->map(function ($item_faktur)
        {
            $data_pembayaran = $item_faktur->invoice->map(function ($item_pembayaran) use($item_faktur)
            {
                return [
                    'invoice_date'          => Carbon::parse($item_faktur->invoice_date)->format('d F Y'),
                    'no_faktur'             => $item_faktur->no_faktur,
                    'nama_pemasok'          => $item_faktur->pemasok->nama,
                    'payment_amount'        => $item_faktur->invoice->sum('payment_amount'),
                    'cheque_no'             => $item_pembayaran->pembayaranPembelian->cheque_no,
                    'cheque_date'           => Carbon::parse($item_pembayaran->pembayaranPembelian->cheque_date)->format('d F Y'),
                ];
            });
            return $data_pembayaran;
        });
    }

    public function historiHutangPemasok($pemasok)
    {
        return $pemasok->map(function ($item_pemasok)
        {
            $sum_faktur = 0;
            $data_faktur = [];
            foreach ($item_pemasok->fakturPembelian as $item_faktur) {
                $temp = [
                    'tanggal'           => Carbon::parse($item_faktur->invoice_date)->format('d F Y'),
                    'keterangan'        => $item_faktur->keterangan !== null ? $item_faktur->keterangan : 'Faktur Pembelian : '.$item_faktur->no_faktur,
                    'nilai_asing'       => $item_faktur->faktur_kurang_uang_muka,
                    'nilai_pajak'       => 0,
                    'css_child_faktur'  => $item_faktur->faktur_kurang_uang_muka < 0 ? 'red' : '#0066cc'
                ]; 
                $data_faktur[] = $temp;
                if ($item_faktur->uang_muka == 1) {
                    $temp['nilai_asing']        = -$temp['nilai_asing'];
                    $temp['css_child_faktur']   = 'red';
                    $data_faktur[] = $temp;
                }
            }

            $data_pembayaran = $item_pemasok->pembayaranPembelian->map(function ($item_pembayaran)
            {
                return [
                    'tanggal'               => Carbon::parse($item_pembayaran->cheque_date)->format('d F Y'),
                    'keterangan'            => $item_pembayaran->memo !== null ? $item_pembayaran->memo : 'Pembayaran Pembelian : '.$item_pembayaran->form_no,
                    'nilai_asing'           => -$item_pembayaran->cheque_amount,
                    'nilai_pajak'           => 0,
                    'css_child_pembayaran'  => -$item_pembayaran->cheque_amount < 0 ? 'red' : '#0066cc'
                ];
            });

            $data_retur = $item_pemasok->returPembelian->map(function ($item_retur)
            {
                return [
                    'tanggal'               => Carbon::parse($item_retur->tanggal)->format('d F Y') ,
                    'keterangan'            => $item_retur->keterangan !== null ? $item_retur->keterangan : 'Retur Pembelian : '.$item_retur->sr_no,
                    'nilai_asing'           => -$item_retur->total,
                    'nilai_pajak'           => 0,
                    'css_child_retur'       => -$item_retur->total < 0 ? 'red' : '#0066cc'
                ];
            });

            foreach ($data_faktur as $calc_faktur) {
                $sum_faktur += $calc_faktur['nilai_asing'];
            }

            // set plus minus
            $css_faktur     = '';
            $css_pembayaran = '';
            $css_retur      = '';

            if ($sum_faktur < 0) {
                $css = 'red';
            }else {
                $css_faktur = '#0066cc';
            }

            if ($data_pembayaran->sum('nilai_asing') < 0) {
                $css_pembayaran = 'red';
            }else {
                $css_pembayaran = '#0066cc';
            }

            if ($data_retur->sum('nilai_asing') < 0) {
                $css_retur = 'red';
            }else {
                $css_retur = '#0066cc';
            }

            // Hitung sum peneruman & penambahan
            $sum_penurunan_faktur   = 0;
            $sum_penambahan_faktur  = 0;

            $sum_penurunan_pembayaran   = 0;
            $sum_penambahan_pembayaran  = 0;

            $sum_penurunan_retur   = 0;
            $sum_penambahan_retur  = 0;

            foreach ($data_faktur as $faktur) {
                if ($faktur['nilai_asing'] < 0) {
                    $sum_penurunan_faktur += $faktur['nilai_asing'];
                }else {
                    $sum_penambahan_faktur += $faktur['nilai_asing'];
                }
            }

            foreach ($data_pembayaran as $pembayaran) {
                if ($pembayaran['nilai_asing'] < 0) {
                    $sum_penurunan_pembayaran += $pembayaran['nilai_asing'];
                }else {
                    $sum_penambahan_pembayaran += $pembayaran['nilai_asing'];
                }
            }

            foreach ($data_retur as $retur) {
                if ($retur['nilai_asing'] < 0) {
                    $sum_penurunan_retur += $retur['nilai_asing'];
                }else {
                    $sum_penambahan_retur += $retur['nilai_asing'];
                }
            }

            $sum_penurunan  = $sum_penurunan_faktur + $sum_penurunan_pembayaran + $sum_penurunan_retur;
            $sum_penambahan = $sum_penambahan_faktur + $sum_penambahan_pembayaran + $sum_penambahan_retur;

            $total_perubahan = $sum_penambahan + $sum_penurunan;

            return [
                'no_pemasok'                    => $item_pemasok->no_pemasok,
                'nama_pemasok'                  => $item_pemasok->nama,
                'data_faktur'                   => $data_faktur,
                'data_pembayaran'               => $data_pembayaran,
                'data_retur'                    => $data_retur,
                'sum_faktur'                    => $sum_faktur,
                'sum_pembayaran'                => $data_pembayaran->sum('nilai_asing'),
                'sum_retur'                     => $data_retur->sum('nilai_asing'),
                'css_faktur'                    => $css_faktur,
                'css_pembayaran'                => $css_pembayaran,
                'css_retur'                     => $css_retur,
                'sum_penurunan'                 => $sum_penurunan,
                'sum_penambahan'                => $sum_penambahan,
                'total_perubahan'               => $total_perubahan,
                'count_faktur'                  => count($data_faktur),
                'count_pembayaran'              => count($data_pembayaran),
                'count_retur'                   => count($data_retur),
            ];
        });
    }

    public function laporanPemasok($pemasok)
    {
        return $pemasok->map(function ($item, $key)
        {
            $perhitungan = $item->sum_pemasok;
            $arrayFaktur = [];
            foreach ($item->fakturPembelian as $key => $data_faktur) {
                $temp = [
                    'tanggal'               => Carbon::parse($data_faktur->invoice_date)->format('d F Y'),
                    'no_sumber'             => $data_faktur->no_faktur,
                    'keterangan_sumber'     => $data_faktur->keterangan !== null ? $data_faktur->keterangan : 'Faktur Pembelian : '.$data_faktur->no_faktur,
                    'penambahan'            => $data_faktur->faktur_kurang_uang_muka,
                    'pengurangan'           => 0,
                    'saldo'                 => $perhitungan = $perhitungan + $data_faktur->faktur_kurang_uang_muka,
                ];
                $arrayFaktur[] = $temp;
                if ($data_faktur->uang_muka) {
                    $temp['pengurangan'] = $data_faktur->faktur_kurang_uang_muka;
                    $temp['penambahan']  = 0;
                    $temp['saldo']       = $perhitungan = $perhitungan - $data_faktur->faktur_kurang_uang_muka;
                    $arrayFaktur[]       = $temp;
                }
                if ($data_faktur->transaksiUangMukaPemasok->isNotEmpty()) {
                    foreach ($data_faktur->transaksiUangMukaPemasok as $key => $uang_muka) {
                        $temp['penambahan'] = $uang_muka->jumlah;
                        $temp['saldo']      = $perhitungan = $perhitungan + $uang_muka->jumlah;
                    }
                    $arrayFaktur[] = $temp;
                }
            }


            foreach ($item->pembayaranPembelian as $key => $data_pembayaran) {
                $arrayFaktur[] = [
                    'tanggal'               => Carbon::parse($data_pembayaran->cheque_date)->format('d F Y') , 
                    'no_sumber'             => $data_pembayaran->form_no,
                    'keterangan_sumber'     => $data_pembayaran->memo !== null ? $data_pembayaran->memo : 'Pembayaran Pembelian : '.$data_pembayaran->form_no,
                    'penambahan'            => 0,
                    'pengurangan'           => $data_pembayaran->cheque_amount,
                    'saldo'                 => $perhitungan = $perhitungan - $data_pembayaran->cheque_amount,
                ];
            }

            foreach ($item->returPembelian as $key => $data_retur) {
                $arrayFaktur[] = [
                    'tanggal'               => Carbon::parse($data_retur->tanggal)->format('d F Y') ,
                    'no_sumber'             => $data_retur->sr_no,
                    'keterangan_sumber'     => $data_retur->keterangan !== null ? $data_retur->keterangan : 'Retur Pembelian : '.$data_retur->sr_no,
                    'penambahan'            => 0,
                    'pengurangan'           => $data_retur->total,
                    'saldo'                 => $perhitungan = $perhitungan - $data_retur->total,
                ];
            }
            $item->data             = collect($arrayFaktur)->sortBy('tanggal');
            $item->sum_penambahan   = $item->data->sum('penambahan');
            $item->sum_pengurangan  = $item->data->sum('pengurangan');
            $item->perubahan_bersih = $item->sum_pemasok + $item->data->sum('penambahan') - $item->data->sum('pengurangan'); 
            return $item;
        });
    }

}
