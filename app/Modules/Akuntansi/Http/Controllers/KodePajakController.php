<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\KodePajakRequest;
use App\Modules\Akuntansi\Repositories\KodePajakRepository;
use Generator\Interfaces\RepositoryInterface;
class KodePajakController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Kode Pajak';
        $this->request = KodePajakRequest::class;
        $this->requestField = ['nama','nilai','kode','keterangan','akun_pajak_penjualan_id','akun_pajak_pembelian_id'];
    }
    public function formData()
    {
        return [
            'listAkun' => $this->model->listAkun(),
        ];
    }
}
