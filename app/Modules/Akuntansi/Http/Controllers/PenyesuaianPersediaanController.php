<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Http\Requests\PenyesuaianPersediaanRequest;
use Generator\Interfaces\RepositoryInterface;

class PenyesuaianPersediaanController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Penyesuaian Persediaan';
        $this->request = PenyesuaianPersediaanRequest::class;
        $this->requestField = [
            'no_penyesuaian',
            'tgl_penyesuaian',
            'kode_akun_id',
            'akun_penyesuaian',
            'keterangan',
            'produk_id',
            'keterangan_produk',
            'satuan_produk',
            'qty_produk',
            'qty_baru',
            'qty_diff',
            'departemen',
            'gudang_id',
            'proyek',
            'serial_number',
            'produk_detail_id',
            'nilai_skrg',
            'nilai_baru',
            'value_penyesuaian'
        ];
    }

    public function formData()
    {
        $user = auth()->user();

        return [
            'produk'    => $this->model->listProduk(),
            'akun'      => $this->model->listAkun(),
            'tipeakun'  => $this->model->listTipeAkun(),
            'gudang'    => $this->model->listGudang(),
            'permission' => [
                'buat' => 'buat_penyesuaian_persediaan',
                'ubah' => 'ubah_penyesuaian_persediaan',
                'hapus' => 'hapus_penyesuaian_persediaan',
                'laporan' => 'laporan_penyesuaian_persediaan',
                'lihat' => 'lihat_penyesuaian_persediaan',
                'daftar' => $user->hasPermissionTo('daftar_penyesuaian_persediaan'),
            ],
            'filter'     => [
                                'no_faktur',
                                'keterangan_pembayaran',
                                'tanggal_dari_sampai',
                                'tipe_penyesuaian'
                            ],
            'tipe_penyesuaian'  => [
                '0'     => 'Kuantitas',
                '1'     => 'Nilai'
            ],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }
}
