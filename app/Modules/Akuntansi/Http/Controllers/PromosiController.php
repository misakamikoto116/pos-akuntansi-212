<?php


namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Http\Requests\PromosiRequest;
use Generator\Interfaces\RepositoryInterface;

class PromosiController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Promosi';
        $this->request = PromosiRequest::class;
        $this->requestField = [
            'title',
            'active_date',
            'expired_date',
            'produk_id',
            'qty',
            'presentase',
            'total',
            'type_sebab',
            'tipe_barang',
            'kelipatan',
            'produk_id_akibat',
            'qty_akibat',
            'presentase_akibat',
            'total_akibat',
            // Akibat
            'produk_id_akibat_promosi',
            'qty_akibat_promosi',
            'presentase_akibat_promosi',
            'total_akibat_promosi',
            'produk_id_sebab_promosi',
            'qty_sebab_promosi',
            'total_sebab_promosi',
            'type_sebab_promosi',
            'kelipatan_sebab_promosi',
        ];
    }
}
