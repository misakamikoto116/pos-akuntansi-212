<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\NomorPajakRequest;
use App\Modules\Akuntansi\Repositories\NomorPajakRepository;
use Generator\Interfaces\RepositoryInterface;
class NomorPajakController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Nomor Pajak';
        $this->request = NomorPajakRequest::class;
        $this->requestField = ['dari','sd','tetap','status'];
    }
}
