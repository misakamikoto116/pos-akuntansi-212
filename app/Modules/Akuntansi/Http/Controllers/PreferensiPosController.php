<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use Illuminate\Http\Request;
use Generator\Interfaces\RepositoryInterface;
use App\Modules\Akuntansi\Http\Requests\PreferensiPosRequest;

class PreferensiPosController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Preferensi';
        $this->request = PreferensiPosRequest::class;
        $this->requestField = [];
    }

    public function index()
    {
        return view('akuntansi::preferensi_pos.index')->with([
            'akun'           => $this->model->listAkun(),
            'preferensi_pos' => $this->model->listPreferensiPos(),
            'akunBank'       => $this->model->listAkunBank(),
            'pajak'          => $this->model->listPajak()
        ]);
    }

    public function storePos(Request $request)
    {
        return $this->model->storePos($request);
    }

    public function storePajak(Request $request)
    {
        return $this->model->storePajak($request);
    }

    public function storeModal(Request $request)
    {
        return $this->model->storeModal($request);
    }

    public function getAkun(Request $request)
    {
        return $this->model->getAkun($request);
    }
}
