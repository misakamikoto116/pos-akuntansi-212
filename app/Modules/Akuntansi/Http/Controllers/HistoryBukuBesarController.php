<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use Generator\Interfaces\RepositoryInterface;
use App\Modules\Akuntansi\Http\Requests\HistoryBukuBesarRequest;
use Illuminate\Http\Request;

class HistoryBukuBesarController extends Controller
{
    public function __construct(RepositoryInterface $model)
   	{ 
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'History Buku Besar';
        $this->request = HistoryBukuBesarRequest::class;
        $this->requestField = ['nama'];
    }

    public function formData()
    {
    	return [
    		'akun'			=> $this->model->listAkun(),
    		'tipe_akun'		=> $this->model->listTipeAkun(),
            'pelanggan'     => $this->model->listPelanggan(),
            'pemasok'       => $this->model->listPemasok(),
            'name_space'    => $this->model->listNameSpace(),
    		'filter'		=> [
    								'no_transaksi',
    								'no_akun',
    								'tanggal_dari_sampai',
    								'akun_id',
    								'tipe_akun_id',
                                    'pelanggan_select',
                                    'pemasok_select',
                                    'filter_menu',
    						   ]
    	];
    }

    public function index()
    {
    	return parent::index()->with($this->formData());
    }
}
