<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use Generator\Interfaces\RepositoryInterface;

class BarangPergudangController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Barang Pergudang';
    }
}
