<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Models\DetailInformasiPemasok;
use App\Modules\Akuntansi\Http\Requests\InformasiPemasokRequest;
use App\Modules\Akuntansi\Repositories\InformasiPemasokRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
class InformasiPemasokController extends Controller
{
    use ExportImportPemasok;
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Pemasok';
        $this->request = InformasiPemasokRequest::class;
        $this->requestField = [
        'nama',
        'no_pemasok',
        'foto_pemasok',
        'nik',
        'foto_nik_pemasok',
        'no_faktur_pemasok',
        'saldo_awal_pemasok',
        'tanggal_pemasok',
        'alamat_pajak',
        'alamat',
        'kota',
        'prop',
        'halaman_web',
        'kode_pos',
        'negara',
        'telepon',
        'personal_kontak',
        'email',
        'syarat_pembayaran_id',
        'batasan_piutang_uang',
        'batasan_piutang_hari',
        'mata_uang_id',
        'keterangan',
        'pajak_satu_id',
        'pajak_dua_id',
        'pajak_faktur',
        'npwp',
        'no_pkp',
        'tipe_pajak_id',
        'tingkatan_harga_jual',
        'disk_default',
        'catatan',
        'no_faktur_saldo',
        'tanggal_saldo',
        'saldo_awal',
        'syarat_pembayaran_saldo_id',
        'nama_kontak',
        'nama_depan_kontak',
        'jabatan_kontak',
        'telepon_kontak'];
    }
      public function formData()
    {
        return [
            'mataUang' => $this->model->listMataUang(),
            'termin' => $this->model->listTermin(),
            'kodePajak' => $this->model->listKodePajak(),
            'tipePajak' => $this->model->listTipePajak(),
            'permission' => [
                'buat' => 'buat_data_pemasok',
                'ubah' => 'ubah_data_pemasok',
                'hapus' => 'hapus_data_pemasok',
                'laporan' => 'laporan_data_pemasok',
                'lihat' => 'lihat_data_pemasok',
                'daftar' => auth()->user()->hasPermissionTo('daftar_data_pemasok'),
            ],
            'filter'     => [
                                'no_pemasok',
                                'nama_pemasok',
                                'mata_uang',
                            ],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function getHistoryBukuBesar(Request $request, $type,$id)
    {
        if (empty($id) || $id === null ) {
            return abort(404);
        }
        return $this->model->historyBukuBesar($request, $type,$id);
    }
}
