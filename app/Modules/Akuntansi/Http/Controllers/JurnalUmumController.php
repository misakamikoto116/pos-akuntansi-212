<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\JurnalUmumRequest;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
use Cart;

class JurnalUmumController extends Controller
{
	public function __construct(RepositoryInterface $model)
	{
		$this->role = 'akuntansi';
	    $this->model = $model;
	    $this->title = 'Data Jurnal Umum';
	    $this->request = JurnalUmumRequest::class;
	    $this->requestField = ['no_faktur_jurmum','tanggal_jurmum','description_jurmum'];
	}
	public function formData()
    {
        return [
			'listAkun' => $this->model->listAkun(),
			'totalCart' => $this->model->totalCart(),
			'permission' => [
				'buat' => 'buat_bukti_jurnal_umum',
				'ubah' => 'ubah_bukti_jurnal_umum',
				'hapus' => 'hapus_bukti_jurnal_umum',
				'laporan' => 'laporan_bukti_jurnal_umum',
				'lihat' => 'lihat_bukti_jurnal_umum',
				'daftar' => auth()->user()->hasPermissionTo('daftar_bukti_jurnal_umum'),
			],
			'filter'	=> ['tanggal_dari_sampai','no_faktur','keterangan_pembayaran'],
        ];
	}

	public function index()
	{
		return parent::index()->with($this->formData());
	}

	public function create()
	{	
		$this->model->removeSession('create', null);

		return parent::create();
	}

	public function sessionApi(Request $request)
	{
		try {
			$uniqueId = $this->model->getCart()
							  ->content()
							  ->pluck('id')
							  ->toArray();

			$newUniqueId = uniqid();
			while(in_array($newUniqueId, $uniqueId)) {
			  $newUniqueId = uniqid();
			}

			$data = [
				'id' => $newUniqueId,
				'name' => $request->nama_detail_akun,
				'qty' => 1,
				'price' => 1,
				'options' => [
					'akun' => $request->akun_detail_id,
					'type' => $request->type,
					'debet' => $request->debet_detail,
					'kredit' => $request->kredit_detail,
					'memo' => $request->memo_detail,
					'department' => $request->departement_detail,
					'ledger' => $request->ledger_detail,
					'project' => $request->project_detail
				]
			];

			$cart = $this->model->getCart()->add($data);

			$response = [
				'total' => $this->model->totalCart(),
				'view' => view('akuntansi::jurnal_umum.load', compact('cart'))->render()
			];

		} catch (Exception $e) {
			$response = [];
		}

		return response()->json($response);
	}

	public function getSession(Request $request)
	{
		try {
			$cart = $this->model->getCart()->get($request->id);

			$response = [
				'view' => $cart
			];

		} catch (Exception $e) {
			$response = [];
		}

		return response()->json($response);
	}

	public function sessionUpdate(Request $request)
	{
		try {
			$cart = $this->model->getCart()->get($request->rowId);

			$data = [
				'id' => $cart->id,
				'name' => $request->nama_detail_akun,
				'qty' => 1,
				'price' => 1,
				'options' => [
					'akun' => $request->akun_detail_id,
					'type' => $request->type,
					'debet' => $request->debet_detail,
					'kredit' => $request->kredit_detail,
					'memo' => $request->memo_detail,
					'department' => $request->departement_detail,
					'ledger' => $request->ledger_detail,
					'project' => $request->project_detail
				]
			];

			$cart = $this->model->getCart()->update($request->rowId, $data);

			$response = [
				'sukses' => true,
				'total' => $this->model->totalCart(),
				'view' => view('akuntansi::jurnal_umum.loadEdit', compact('cart'))->render(),
				'data' => [
					'rowId' => $cart->rowId
				]
			];

		} catch (Exception $e) {
			$response = [];
		}

		return response()->json($response);
	}

	public function sessionDelete(Request $request)
	{
		try {
			$this->model->getCart()->remove($request->id);

			$response = [
				'sukses' => true,
				'total' => $this->model->totalCart(),
			];
		} catch (Exception $e) {
			$response = [];
		}

		return response()->json($response);
	}

	public function getCetakJurnalUmum(Request $request)
	{
		return $this->model->cetakJurnalUmum($request);
	}
}
