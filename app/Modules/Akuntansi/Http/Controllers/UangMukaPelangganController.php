<?php
namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Http\Requests\UangMukaRequest;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class UangMukaPelangganController extends Controller
{
	public function __construct(RepositoryInterface $model)
	{
		$this->role         = 'akuntansi';
		$this->model        = $model;
		$this->title        = 'Uang Muka Pelanggan';
		$this->request      = UangMukaRequest::class;
		$this->requestField = [
			'no_faktur',
			'pelanggan_id',
			'alamat_pengiriman',
			'taxable',
			'in_tax',
			'diskon',
			'invoice_date',
			'status',
			'total',
			'catatan',
			'uang_muka',
			'produk_id',
			'faktur_penjualan',
			'item_deskripsi',
			'jumlah',
			'unit_price',
			'diskon_produk',
			'kode_pajak_id',
			'akun_piutang_id',
			'akun_dp_id',
			'lanjutkan',
		];
	}

	public function formData()
	{
		return [
			'pelanggan' => $this->model->listPelanggan(),
			'pajak'     => $this->model->listPajak(),
			'produk'    => $this->model->getDPFromProduk(),
			'akun'      => $this->model->listAkun(),
			'getAkunDp' => $this->model->getAkunDp(),
			'filter'    => ['tanggal_dari_sampai', 'pelanggan_select','no_faktur'],
		];
	}

	public function index()
	{
		return parent::index()->with($this->formData());
	}

	public function redirectSuccess(Request $request, $result = null)
	{
		if (!empty($result['continue_stat'])) {
			return redirect('/akuntansi/uang-muka-pelanggan/create')->withMessage('Berhasil Menambah/Memperbarui data');
		} else {
			return redirect('/akuntansi/uang-muka-pelanggan')->withMessage('Berhasil Menambah/Memperbarui data');
		}
	}
}
