<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Models\SerialNumber;
use App\Modules\Akuntansi\Http\Requests\SerialNumberRequest;
use App\Modules\Akuntansi\Repositories\SerialNumberRepository;
use Generator\Interfaces\RepositoryInterface;
class SerialNumberController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Pencatatan Nomor Serial';
        $this->request = SerialNumberRequest::class;
        $this->requestField = [
                                'no_pengisian',
                                'tgl_pengisian',
                                'tipe_transaksi',
                                'no_transaksi',
                                'tgl_transaksi',
                                'persiapan',
                                'produk_id',
                                'keterangan_produk',
                                'qty_produk',
                                'satuan_produk',
                                'nomor_serial',
                            ];
    }
    public function formData()
    {
        return [
            'produk' => $this->model->listProduk(),
            'transaksi' => $this->model->listTransaksi(),
     
        ];
    }
}