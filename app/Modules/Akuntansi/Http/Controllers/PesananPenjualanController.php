<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Http\Requests\PesananPenjualanRequest;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class PesananPenjualanController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data PesananPenjualan';
        $this->request = PesananPenjualanRequest::class;
        $this->requestField = ['no_pesanan',
            'pelanggan_id',
            'pelanggan_kirim_ke_id',
            'alamat_pengiriman',
            'alamat_tagihan',
            'taxable',
            'in_tax',
            'diskon',
            'ongkir',
            'total',
            'keterangan',
            'keterangan_produk',
            'produk_id',
            'barang_id',
            'qty_produk',
            'satuan_produk',
            'unit_harga_produk',
            'diskon_produk',
            'tax_produk',
            'amount_produk',
            'akun_dp_id',
            'po_number',
            'so_number',
            'so_date',
            'ship_date',
            'fob',
            'term_id',
            'ship_id',
            'terproses',
            'ditutup',
            'kode_pajak_id',
            'barang_id',
            'penawaran_id',
            'harga_modal',
            'harga_terakhir',
            'nilai_pajak',
            'lanjutkan',
            // Untuk keperluan cetak saja
            'tax_cetak0',
            'tax_cetak1',
            'grandtotal',
            'total_potongan_rupiah',
            'subTotal',
        ];
    }

    public function formData()
    {
        return [
            'kategori' => $this->model->listKategoriProduk(),
            'gudang' => $this->model->listGudang(),
            'akun' => $this->model->listAkun(),
            'pelanggan' => $this->model->listPelanggan(),
            // 'produk'       => $this->model->listProduk(),
            'termin' => $this->model->listTermin(),
            'pengiriman' => $this->model->listJasaPengiriman(),
            'kode_pajak' => $this->model->listKodePajak(),
            'idPrediction' => $this->model->idPrediction(),
            'getAkunDp' => $this->model->getAkunDp(),

            'status_penjualan' => collect(['0' => 'Mengantri', '1' => 'Menunggu', '2' => 'Terproses', '3' => 'Ditutup']),
            'filter' => ['status_penjualan', 'tanggal_dari_sampai', 'pelanggan_select','no_faktur','keterangan_pembayaran'],
            // 'tipePelanggan' => $this->model->listTipePelanggan(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function redirectSuccess(Request $request, $result = null)
    {
        if (!empty($result['continue_stat'])) {
            if ($result['continue_stat'] == 1) {
                return redirect('/akuntansi/pengiriman-penjualan/create?pelanggan_id='.$result->pelanggan_id.'&pesanan_id='.$result->id)
                ->withMessage('Berhasil Menambah/Memperbarui data');
            } else {
                return redirect('/akuntansi/pesanan-penjualan/create')->withMessage('Berhasil Menambah/Memperbarui data');
            }
        } else {
            return redirect('/akuntansi/pesanan-penjualan')->withMessage('Berhasil Menambah/Memperbarui data');
        }
    }
}
