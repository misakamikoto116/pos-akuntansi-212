<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LaporanBarangController extends Controller
{
    use TraitLaporan;
	
    public function daftar_barang(Request $request)
    {
        // $request['gudang_id'] === null ? $request['gudang_id'] = 6 : $request['gudang_id'];
        $title  = 'Laporan Daftar Barang';
        
        if(!$request->ajax()){
            $barang = $this->produk->reportFilter($request)->paginate(100);
            $barang->getCollection()->transform(function($query){
                return [
                    'noBarang'            =>     $query->no_barang,
                    'namaBarang'          =>     $query->keterangan,
                    'kuantitas'           =>     $query->updated_qty,
                    'biayaRata'           =>     !empty($query->saldoAwalBarang->last()->harga_modal) ? number_format($query->saldoAwalBarang->last()->harga_modal) : 0,
                    'biayaTerakhir'       =>     !empty($query->saldoAwalBarang->last()->harga_terakhir) ? number_format($query->saldoAwalBarang->last()->harga_terakhir) : 0,
                    'hargaSatuan'         =>     !empty($query->harga_jual) ? number_format($query->harga_jual) : 0,
                    'tipeBarang'          =>     $query->tipe_formatted,
                    'tipePersediaan'      =>     $query->tipe_persediaan_formatted,
                ];
            });
        }else{
            $barang = $this->produk->reportFilter($request)->get();
            $barang = $barang->map(function ($query) {
                return [
                    'noBarang'            =>     $query->no_barang,
                    'namaBarang'          =>     $query->keterangan,
                    'kuantitas'           =>     $query->updated_qty,
                    'biayaRata'           =>     !empty($query->saldoAwalBarang->last()->harga_modal) ? number_format($query->saldoAwalBarang->last()->harga_modal) : 0,
                    'biayaTerakhir'       =>     !empty($query->saldoAwalBarang->last()->harga_terakhir) ? number_format($query->saldoAwalBarang->last()->harga_terakhir) : 0,
                    'hargaSatuan'         =>     !empty($query->harga_jual) ? number_format($query->harga_jual) : 0,
                    'tipeBarang'          =>     $query->tipe_formatted,
                    'tipePersediaan'      =>     $query->tipe_persediaan_formatted,
                ];
            });
        }
        if($request->ajax()){
            if ( count($request->all()) === 2 ) { // Print Laporan
                return response()->json([$barang], 200);
            }elseif ( count($request->all()) === 3 ) { // Download Laporan
                $downloadPath   =   asset('storage/laporan/barang/daftar_barang/'.$titlePdf);
                return response()->json([
                    'success'   => true,
                    'path'      => $downloadPath,
                    'title'     => $titlePdf
                ], 200);
            }elseif( count($request->all()) === 4 ){ // infiniteScroll
                return response()->json([$barang->getCollection()], 200);
            }
        }else{
            $view = $this->generateViewData($title, $request ,$barang);
            $view['gudang'] = $request['gudang_id'];
            return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_barang.laporan_daftar_barang')->with($view);
        }
    }
}
