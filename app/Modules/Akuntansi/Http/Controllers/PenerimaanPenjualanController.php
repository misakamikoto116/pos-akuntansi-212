<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\PenerimaanPenjualanRequest;
use App\Modules\Akuntansi\Repositories\PenerimaanPenjualanRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class PenerimaanPenjualanController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Penerimaan Penjualan';
        $this->request = PenerimaanPenjualanRequest::class;
        $this->requestField = [
            "no_pelanggan",
            "pelanggan_id",
            "alamat_asal",
            "form_no",
            "payment_date",
            "akun",
            "akun_bank_id",
            "rate",
            "cheque_no",
            "cheque_date",
            "memo",
            "kosong",
            "fiscal_payment" , 
            "cheque_amount" , 
            "equiv_amount" , 
            "existing_credits" ,
            "distribute_amount" ,
            "faktur_id" ,
            // "no_faktur" ,
            "tanggal" ,
            "amount",
            "owing" ,
            "payment_amount",
            "diskon", 
            "akun_diskon_mdl",
            "status_pay",  
            "diskon_date",
            "id_bayar" ,
            "tgl_bayar",
            "jumlah",
            'lanjutkan'
            
        ];
    }
    public function formData()
    {
        return [
            'pelanggan'         => $this->model->listPelanggan(),
            'listAkunDiskon'    => $this->model->listAkunDiskon(),
            // 'gudang' => $this->model->listGudang(),
            // 'pemasok' => $this->model->listPemasok(),
            // 'produk' => $this->model->listProduk(),
            // 'pengiriman' => $this->model->listJasaPengiriman(),
            // 'kategori' => $this->model->listKategoriProduk(),
            // 'akun' => $this->model->listAkun(),
			'idPrediction'   => $this->model->idPrediction(),   
            'filter' => ['tanggal_dari_sampai', 'pelanggan_select'],         
            // 'termin' => $this->model->listTermin(),
            // 'tipePemasok' => $this->model->listTipePemasok(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function redirectSuccess(Request $request, $result = null)
    {
        if(!empty($result['continue_stat'])){
            return redirect('/akuntansi/penerimaan-penjualan/create')->withMessage('Berhasil Menambah/Memperbarui data');;
        }else{
            return redirect('/akuntansi/penerimaan-penjualan')->withMessage('Berhasil Menambah/Memperbarui data');;
        }
    }
}
