<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Http\Requests\PengirimanPenjualanRequest;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class PengirimanPenjualanController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Pengiriman Penjualan';
        $this->request = PengirimanPenjualanRequest::class;
        $this->requestField = [
                                'pelanggan_id',
                                'pelanggan_kirim_ke_id',
                                'barang_id',
                                'alamat_pengiriman',
                                'alamat_tagihan',
                                'delivery_no',
                                'delivery_date',
                                'po_no',
                                'ship_id',
                                'keterangan',
                                'keterangan_produk',
                                'produk_id',
                                'qty_produk',
                                'satuan_produk',
                                'gudang_id',
                                'no_so',
                                'nominal',
                                'pesanan',
                                'harga_modal',
                                'harga_terakhir',
                                'lanjutkan',
                                // Untuk Keperluan Cetak
                                'sn',
                            ];
    }

    public function formData()
    {
        return [
            'kategori' => $this->model->listKategoriProduk(),
            'gudang' => $this->model->listGudang(),
            'akun' => $this->model->listAkun(),
            'pelanggan' => $this->model->listPelanggan(),
            // 'produk' => $this->model->listProduk(),
            'termin' => $this->model->listTermin(),
            'pengiriman' => $this->model->listJasaPengiriman(),
            'idPrediction' => $this->model->idPrediction(),

            'status_penjualan' => collect(['0' => 'Belum Cetak', '1' => 'Tercetak']),
            'filter' => ['status_penjualan', 'tanggal_dari_sampai', 'pelanggan_select','no_faktur','no_pelanggan'],
            // 'tipePelanggan' => $this->model->listTipePelanggan(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function redirectSuccess(Request $request, $result = null)
    {
        if (!empty($result['continue_stat'])) {
            if ($result['continue_stat'] == 1) {
                return redirect('/akuntansi/faktur-penjualan/create?pelanggan_id='.$result->pelanggan_id.'&pengiriman_id='.$result->id)
            ->withMessage('Berhasil Menambah/Memperbarui data');
            } else {
                return redirect('/akuntansi/pengiriman-penjualan/create')->withMessage('Berhasil Menambah/Memperbarui data');
            }
        } else {
            return redirect('/akuntansi/pengiriman-penjualan')->withMessage('Berhasil Menambah/Memperbarui data');
        }
    }
}
