<?php
namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Http\Requests\FakturPembelianRequest;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class FakturPembelianController extends Controller
{
	/**
	 * model.
	 *
	 * @var App\Modules\Akuntansi\Repositories\FakturPembelianRepository
	 */
	protected $model;

	public function __construct(RepositoryInterface $model)
	{
		$this->role         = 'akuntansi';
		$this->model        = $model;
		$this->title        = 'Faktur Pembelian';
		$this->request      = FakturPembelianRequest::class;
		$this->requestField = [
			'no_pemasok',
			'pemasok_id',
			'uangmuka',
			'alamat_pengiriman',
			'form_no',
			'ship_date',
			'no_faktur',
			'invoice_date',
			'fob',
			'term_id',
			'ship_id',
			'ship_date',
			'receive_date',
			'amount',
			'produk_id',
			'keterangan_produk',
			'qty_produk',
			'satuan_produk',
			'unit_harga_produk',
			'diskon_produk',
			'tax_produk',
			'kode_pajak_id',
			'total_pajak',
			'amount_produk',
			'amount_modal_produk',
			'amount_old_modal_produk',
			'sn',
			'akun_beban_id',
			'akun_beban_nama',
			'amount_beban',
			'notes_beban',
			'dept_beban_id',
			'proyek_beban_id',
			'alokasi_ke_barang',
			'txt_alokasi_ke_barang',
			'pemasok_beban_id',
			'pemasok_beban_id_real',
			'faktur_dp_id',
			'gudang_id',
			'keterangan_dp',
			'pajak_dp',
			'total_dp',
			'no_faktur_dp',
			'no_po_dp',
			'include_tax_dp',
			'catatan',
			'akun_hutang_id',
			'akun_piutang_id',
			'akun_dp_id',
			'diskon',
			'ongkir',
			'total_diskon_faktur',
			'total',
			'subtotal',
			'taxable',
			'in_tax',
			'barang_penerimaan_pembelian_id',
			'lanjutkan',
			'subTotalWithBeban',
			'tax_cetak0',
			'tax_cetak1',
			'expired_date',
		];
	}

	public function embedData($id)
	{
		$prediction = $this->model->idPrediction();

		return [
			'gudang'               => $this->model->listGudang(),
			'pemasok'              => $this->model->listPemasok(),
			'produk'               => $this->model->listProduk($id),
			'pengiriman'           => $this->model->listJasaPengiriman(),
			'akun'                 => $this->model->listAkun(),
			'akunKode'             => $this->model->listAkunKode(),
			'termin'               => $this->model->listTermin(),
			'pajak'                => $this->model->listPajak(),
			'getAkunHutang'        => $this->model->getAkunHutang(),
			'filter'               => ['tanggal_dari_sampai', 'pemasok_select', 'no_faktur', 'keterangan_pembayaran'],
			'bebanFakturPembelian' => $this->getBebanFakturPembelian($id),
			'form_no'              => $prediction['form_no'],
			'no_faktur'            => $prediction['no_faktur'],
		];
	}

	public function getBebanFakturPembelian($id)
	{
		// dd(request()->old());
		if (request()->old()) {
			if (null !== $id) {
				return [];
			}
		}
		if (null !== $id) {
			$model = $this->model->findItem($id);

			return $model->bebanFakturPembelian;
		}

		return [];
	}

	public function index()
	{
		return parent::index()->with([
			'pemasok' => $this->model->listPemasok(),
			'filter'  => ['tanggal_dari_sampai', 'pemasok_select', 'no_faktur', 'keterangan_pembayaran'],
		]);
	}

	public function redirectSuccess(Request $request, $result = null)
	{
		if (!empty($result['continue_stat'])) {
			if (1 == $result['continue_stat']) {
				return redirect('/akuntansi/pembayaran-pembelian/create?pemasok_id=' . $request->pemasok_id)
				->withMessage('Berhasil Menambah/Memperbarui data');
			} else {
				return redirect('/akuntansi/faktur-pembelian/create')->withMessage('Berhasil Menambah/Memperbarui data');
			}
		} else {
			return redirect('/akuntansi/faktur-pembelian')->withMessage('Berhasil Menambah/Memperbarui data');
		}
	}
}
