<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LaporanBukuBesarController extends Controller
{
    use TraitLaporan;
    
    public function keseluruhan_jurnal(Request $request)
    {
        $title = 'Laporan Keseluruhan Jurnal';
        $arrayData = [];
        $x = [];
        $transaksi = $this->transaksi->with('akun')->dateFilter($request)
        ->orderBy('tanggal','desc')->orderBy('item_type')->get();

        if (!$transaksi->isEmpty()) {
            $x = $transaksi->groupBy('keterangan')->map(function ($query) {
                return $query->sortByDesc('status')->map(function($queries){
                    $queries->status === 0 ? $kredit = $queries->nominal ?? 0 : $kredit = 0 ;
                    $queries->status === 1 ? $debit = $queries->nominal ?? 0 : $debit = 0 ;
                    return [
                        'tanggal'               => $queries->tanggal_formatted ?? null,
                        'no_akun'               => $queries->akun->kode_akun ?? null,
                        'nama_akun'             => $queries->akun->nama_akun ?? null,
                        'debit'                 => $debit,
                        'kredit'                => $kredit,
                    ];
                })->groupBy('tanggal');
            });
            // for ($i = 0; $i <= count($transaksi); ++$i) {
            //     if ($i == 0) {
            //         $arrayData[] = [
            //             'item_type' => $transaksi[$i]->item_type,
            //             'item_id' => $transaksi[$i]->item_id,
            //             'keterangan' => $transaksi[$i]->keterangan,
            //             'tanggal' => $transaksi[$i]->tanggal_formatted,
            //             'subItems' => array(),
            //             'nominal' => array(),
            //         ];
            //     }
            //     if (!empty($transaksi[$i + 1])) {
            //         if ($transaksi[$i]->item_id == $transaksi[$i + 1]->item_id && $transaksi[$i]->item_type == $transaksi[$i + 1]->item_type) {
            //             // do nothing
            //         } else {
            //             $arrayData[] = [
            //                 'item_type' => $transaksi[$i + 1]->item_type,
            //                 'item_id' => $transaksi[$i + 1]->item_id,
            //                 'keterangan' => $transaksi[$i + 1]->keterangan,
            //                 'tanggal' => $transaksi[$i + 1]->tanggal_formatted,
            //                 'subItems' => array(),
            //                 'nominal' => array(),
            //             ];
            //         }
            //     }
            // }

            // foreach ($arrayData as $data => $key) {
            //     $subTransaction = $this->transaksi->with('akun')->dateFilter($request)->orderBy('id')->where(['item_type' => $key['item_type'], 'item_id' => $key['item_id']]);

            //     # Group by akun id
            //     $nominal = $subTransaction->get()->groupBy('dari')->map(function ($akun) {
            //         return $akun->groupBy('status')->map(function ($sum) {
            //             return $sum->sum('nominal');
            //         });
            //     });
            //     $brew = $subTransaction->groupBy('dari')->get();
            //     # End of Group by akun id

            //     foreach ($brew as $sub) {
            //         array_push($arrayData[$data]['subItems'], $sub);
            //     }
            //     foreach ($nominal as $nom) {
            //         array_push($arrayData[$data]['nominal'], $nom);
            //     }
            // }
        }else{
            $x = collect($x);
        }

        $view = $this->generateViewData($title, $request, $x);
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_buku_besar.keseluruhan_jurnal')->with($view);
    }

    public function daftar_histori_gl(Request $request)
    {
        $title  = 'Daftar Histori GL';
        $x = [];
        $transaksi = $this->transaksi->dateFilter($request)->whereNotNull('item_id')->get();
        if (!$transaksi->isEmpty()) {
            $collection = $transaksi->groupBy('keterangan')->map(function ($query) {
                return $query->sortByDesc('status')->map(function($queries){
                    return [
                        'tanggal'               => $this->dateCarbon($queries->tanggal) ?? null,
                        'tipe_sumber'           => ucwords($queries->sumber) ?? null,
                        'no_sumber'             => $queries->no_transaksi ?? null,
                        'no_akun'               => $queries->akun->kode_akun ?? null,
                        'nama_akun'             => $queries->akun->nama_akun ?? null,
                        'keterangan_transaksi'  => ucwords($queries->keterangan),
                        'nominal_kredit'        => $queries->status == 0 ? $queries->nominal : null,
                        'nominal_debet'         => $queries->status == 1 ? $queries->nominal : null,
                    ];
                });
            });
        }else{
            $collection = collect($x);
        }

        $view = $this->generateViewData($title, $request, $collection);
        $view['total_debet'] = $transaksi->where('status',1)->sum('nominal');
        $view['total_kredit'] = $transaksi->where('status',0)->sum('nominal');

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_buku_besar.daftar_histori_gl')->with($view);
    }

    public function ringkasan_buku_besar()
    {
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_buku_besar.ringkasan_buku_besar');
    }

    public function buku_besar_rinci(Request $request)
    {
        $title = 'Laporan Buku Besar Rinci';
        $x = [];
        $transaksi  =   $this->transaksi->dateFilter($request)
                        ->whereNotNull('item_id')->get();

        if (!$transaksi->isEmpty()) {
            $collection = $transaksi->groupBy('akun_id')->map(function ($query) {
                $dataTransaksi = $query->sortByDesc('status')->map(function($queries){
                    return [
                        'tanggal'               => $this->dateCarbon($queries->tanggal) ?? null,
                        'sumber'                => $queries->sumber,
                        'no_akun'               => $queries->akun->kode_akun ?? null,
                        'keterangan'            => ucwords($queries->keterangan),
                        'nominal_kredit'        => $queries->status == 0 ? $queries->nominal : 0,
                        'nominal_debet'         => $queries->status == 1 ? $queries->nominal : 0,
                    ];
                });

                return [
                    'no_akun_header'       => $query->first()->akun->kode_akun,
                    'nama_akun_header'     => $query->first()->akun->nama_akun,
                    'tipe_akun_header'     => $query->first()->akun->tipeAkun->title,
                    'saldo_awal_header'    => $query->first()->akun->money_function,
                    'dataTransaksi'        => $dataTransaksi,
                ];
            });
        }else{
            $collection = collect($x);
        }
        // $dataHeader = $this->transaksi->dateFilter($request)->groupBy('akun_id')
        //             ->whereNotNull('item_id')->get();

        // $newCollection = [];
        // foreach($dataHeader as $index => $rs){
        //     $newCollection[] = [
        //         'kode_akun' => $rs->akun->kode_akun,
        //         'nama_akun' => $rs->akun->nama_akun,
        //         'tipe_akun' => $rs->akun->tipeAkun->title,
        //         'saldo_awal' => $rs->akun->money_function,
        //         'subItem' => array(),
        //     ];

        //     $dataDetail = $this->transaksi->where('akun_id', $rs->akun_id)->groupBy('keterangan');
        //         # Group by akun id
        //         $nominal = $dataDetail->get()->groupBy('item_type')->mapWithKeys(function ($type) {
        //             $summed = $type->groupBy('item_id')->map(function ($summed){
        //                 return $summed->sum('nominal');
        //             });
                    
        //             return $type->transform(function($item) use ($summed){
        //                 $item['nominals'] = $summed->sum();
        //                 return $item;
        //             });
        //         });

        //     foreach ($nominal as $nom) {
        //         array_push($newCollection[$index]['subItem'], $nom);
        //     }
        // }
        $view = $this->generateViewData($title, $request, $collection);
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_buku_besar.buku_besar_rinci')->with($view);
    }

    public function neraca_saldo(Request $request)
    {
        $identitas      = $this->identitas->first()->nama_perusahaan ?? null;
        $data_arr       = [];
        $saldo          = 0;
        $parent_stat    = 0; 
        $akun           = $this->akun;

        $data = $this->akun->with('transaksi')->orderBy('kode_akun')->get();
        $data->transform(function($item, $key) use (&$data_arr, &$saldo, &$parent_stat, $akun){
            $parent_stat = 0; 

            if(count($item->childAkun) > 0){
                $parent_stat++;
            }
            $perubahan_debet    = $item->transaksi->where('status',1)->sum('nominal');
            $perubahan_kredit   = $item->transaksi->where('status',0)->sum('nominal');
            $data_arr = [
                'id'                => $item->id,
                'parent_id'         => $item->parent_id,
                'kode_akun'         => $item->kode_akun,
                'nama_akun'         => $item->nama_akun,
                'money_function'    => $item->money_function,
                'childAkun'         => $item->childAkun,
                'parent_stat'       => $parent_stat,
                'perubahan_debet'   => $perubahan_debet, 
                'perubahan_kredit'  => $perubahan_kredit 
            ];
            return $data_arr;
        });
        $view = [
            'nama_perusahaan'   => $identitas,
            'items'             => $data,
            'title'             => 'Neraca Saldo',
            'tanggal_awal'      => Carbon::parse($request['start-date'])->format('d F Y'),
            'tanggal_akhir'     => Carbon::parse($request['end-date'])->format('d F Y')
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_buku_besar.neraca_saldo')->with($view);
    }

    public function neraca_saldo_klasik(Request $request)
    {
        $identitas      = $this->identitas->first()->nama_perusahaan ?? null;
        $data_arr       = [];
        $saldo          = 0;
        $parent_stat    = 0; 
        $akun           = $this->akun;

        $data = $this->akun->with('transaksi')->orderBy('kode_akun')->get();
        $data->transform(function($item, $key) use (&$data_arr, &$saldo, &$parent_stat, $akun){
            $parent_stat = 0; 

            if(count($item->childAkun) > 0){
                $parent_stat++;
            }
            $perubahan_debet    = $item->transaksi->where('status',1)->sum('nominal');
            $perubahan_kredit   = $item->transaksi->where('status',0)->sum('nominal');
            $data_arr = [
                'id'                => $item->id,
                'parent_id'         => $item->parent_id,
                'kode_akun'         => $item->kode_akun,
                'nama_akun'         => $item->nama_akun,
                'money_function'    => $item->money_function,
                'childAkun'         => $item->childAkun,
                'parent_stat'       => $parent_stat,
                'perubahan_debet'   => $perubahan_debet, 
                'perubahan_kredit'  => $perubahan_kredit 
            ];
            return $data_arr;
        });
        $view = [
            'nama_perusahaan'   => $identitas,
            'items'             => $data,
            'title'             => 'Neraca Saldo',
            'tanggal_awal'      => Carbon::parse($request['start-date'])->format('d F Y'),
            'tanggal_akhir'     => Carbon::parse($request['end-date'])->format('d F Y')
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_buku_besar.neraca_saldo_klasik')->with($view);
    }

    public function untung_rugi_realisir()
    {
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_buku_besar.untung_rugi_realisir');
    }

    public function untung_rugi_nonrealisir()
    {
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_buku_besar.untung_rugi_nonrealisir');
    }

    public function bukti_jurnal_umum(Request $request)
    {
        $identitas      = $this->identitas->first()->nama_perusahaan ?? null;
        $jurnal_umum    = $this->jurnalUmum->with('detailJurnalUmum')->reportJurnalUmum($request)->get();

        $view = [
            'title'                 => 'Bukti Jurnal Umum',
            'nama_perusahaan'       => $identitas,
            'tanggal_awal'          => Carbon::parse($request['start-date'])->format('d F Y'),
            'tanggal_akhir'         => Carbon::parse($request['end-date'])->format('d F Y'),
            'items'                 => $jurnal_umum,
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_buku_besar.bukti_jurnal_umum')->with($view);
    }
}
