<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class SaldoAkunController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Saldo Akun';
    }

    public function formData()
    {
        return [
            'tahun' => $this->model->listTahun(),
            'thisYear' => date('Y'),
            'tipeAkun' => $this->model->tipeAkun()
        ];
    }

    public function index()
	{
		return parent::index()->with($this->formData());
	}
}
