<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Models\Barang;
use App\Modules\Akuntansi\Http\Requests\GroupBarangRequest;
use App\Modules\Akuntansi\Repositories\GroupBarangRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class GroupBarangController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Group Barang';
        $this->request = GroupBarangRequest::class;
        $this->requestField = [
            'tipe_barang',
            'no_barang',
            'parent_id',
            'status',
            'keterangan',
            'tipe_persedian',
            'harga_standar_def',
            'kategori_produk_id',
            'gudang_def_id',
            'kuantitas',
            'unit',
            'harga_unit',
            'harga_jual',
            'diskon',
            'kode_pajak_penj_id',
            'pemasok_id',
            'min_jumlah_reorder',
            'kode_pajak_perse_id',
            'kode_pajak_pemb_id',
            'akun_persedian_id',
            'akun_beban_id',
            'akun_penjualan_id',
            'akun_ret_penjualan_id',
            'akun_disk_penjualan_id',
            'akun_barang_terkirim_id',
            'akun_hpp_id',
            'akun_ret_pembelian_id',
            'akun_belum_tertagih_id',
            'catatan',
            'no_faktur_gudang',
            'tanggal_gudang',
            'gudang_id',
            'kuantitas_gudang',
            'biaya_gudang',
            'sn_gudang',
            'tingkat_1',
            'tingkat_2',
            'tingkat_3',
            'tingkat_4',
            'tingkat_5',
            'multi_gudang_id'
        ];
    }

    public function formData()
    {
        return [
            'akun' => $this->model->listAkun(),
            'kodePajak' => $this->model->listKodePajak(),
            'prefBarang' => $this->model->listPrefBarang(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }
}
