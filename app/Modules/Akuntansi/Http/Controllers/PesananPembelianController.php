<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Models\PesananPembelian;
use App\Modules\Akuntansi\Http\Requests\PesananPembelianRequest;
use App\Modules\Akuntansi\Repositories\PesananPembelianRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
class PesananPembelianController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Pesanan Pembelian';
        $this->request = PesananPembelianRequest::class;
        $this->requestField = [
                                'taxable',
                                'in_tax',
                                'keterangan_produk',
                                'produk_id',
                                'qty_produk',
                                'satuan_produk',
                                'unit_harga_produk',
                                'diskon_produk',
                                'tax_produk',
                                'amount_produk',
                                'pemasok_id',
                                'alamat_pengiriman',
                                'po_number',
                                'po_date',
                                'expected_date',
                                'ship_date',
                                'fob',
                                'term_id',
                                'ship_id',
                                'diskon',
                                'total_potongan_rupiah',
                                'ongkir',
                                'total',
                                'keterangan',
                                'kode_pajak_id',
                                'akun_dp_id',
                                'akun_ongkir_id',
                                'no_fp_std',
				            	'no_fp_std2',
				            	'no_fp_std_date',
                                'barang_permintaan_pembelian_id',
                                'harga_modal',
                                'lanjutkan',
                                'tax_cetak0',
                                'tax_cetak1'
                            ];
    }
      public function formData()
    {
        return [
            'kategori' => $this->model->listKategoriProduk(),
            'gudang' => $this->model->listGudang(),
            'akun' => $this->model->listAkun(),
            'pemasok' => $this->model->listPemasok(),
            // 'produk' => $this->model->listProduk(),
            'termin' => $this->model->listTermin(),
            'pengiriman' => $this->model->listJasaPengiriman(),
            'idPrediction'   => $this->model->idPrediction(),            
            'getAkunDp'   => $this->model->getAkunDp(),     
            'status_penjualan' => collect(['0' => 'Sedang Diproses','1' => 'Ditutup', '2' => 'Diterima Penuh']),
            'filter' => ['status_penjualan', 'tanggal_dari_sampai', 'pemasok_select','no_faktur','keterangan_pembayaran'],       
            'pajak' => $this->model->listPajak(),       
            // 'tipePemasok' => $this->model->listTipePemasok(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function redirectSuccess(Request $request, $result = null)
    {
        if(!empty($result['continue_stat'])){
            if($result['continue_stat'] == 1){
                return redirect('/akuntansi/penerimaan-barang/create?pemasok_id='.$result->pemasok_id.'&pesanan_id='.$result->id)
                ->withMessage('Berhasil Menambah/Memperbarui data');
            }else{
                return redirect('/akuntansi/pesanan-pembelian/create')->withMessage('Berhasil Menambah/Memperbarui data');;
            }
        }else{
            return redirect('/akuntansi/pesanan-pembelian')->withMessage('Berhasil Menambah/Memperbarui data');;
        }
    }
}
