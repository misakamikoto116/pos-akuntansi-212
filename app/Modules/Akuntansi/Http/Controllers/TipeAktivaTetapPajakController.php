<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\TipeAktivaTetapPajakRequest;
use App\Modules\Akuntansi\Repositories\TipeAktivaTetapPajakRepository;
use Generator\Interfaces\RepositoryInterface;
class TipeAktivaTetapPajakController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Tipe Aktiva Tetap Pajak';
        $this->request = TipeAktivaTetapPajakRequest::class;
        $this->requestField = ['tipe_aktiva_tetap_pajak','metode_penyusutan_id','estimasi_umur','tarif_penyusutan'];
    }
    public function formData()
    {
        return [
            'listMetodePenyusutan' => $this->model->listMetodePenyusutan(),
        ];
    }
}
