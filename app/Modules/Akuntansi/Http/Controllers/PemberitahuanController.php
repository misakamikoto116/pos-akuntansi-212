<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Models\Pemberitahuan;
use Generator\Interfaces\RepositoryInterface;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;

class PemberitahuanController extends Controller
{
    protected $pemberitahuan;
    public function __construct(RepositoryInterface $pemberitahuan)
    {
        $this->pemberitahuan = $pemberitahuan;
        $this->role = 'akuntansi';
        $this->title = 'Data Pemberitahuan';
    }
    /**
     * Index
     *
     * @method GET
     * @param null
     * @return view
     */
    public function index()
    {
        $tujuan = \Spatie\Permission\Models\Role::all();
        $data = $this->pemberitahuan->getItems();
        $view = [
            'tujuan' => $tujuan->pluck('name', 'name'),
            'data' => $data
        ];
        return view('akuntansi::pemberitahuan.index')->with($view);
    }

    public function kirimPesanPost(Request $request)
    {
        $data = [
            'tujuan' => $request->tujuan,
            'isi_pemberitahuan' =>  $request->pesan,
            'created_by' =>  auth()->user()->id,
        ];
        if(send_notif_socket($request->tujuan, $request->pesan)){
            $this->pemberitahuan->insert($data)->save();
            return response()->redirectToRoute('akuntansi.pemberitahuan.kirim')
                ->withMessage('Berhasil Mengirimkan notifikasi/pemberitahuan');
        }else{
            return response()->redirectToRoute('akuntansi.pemberitahuan.kirim',[], 302)
                ->withMessage('Gagal Mengirimkan notifikasi/pemberitahuan');
        }
    }

    public function kirimUlangAjax(Request $request)
    {

    }
}
