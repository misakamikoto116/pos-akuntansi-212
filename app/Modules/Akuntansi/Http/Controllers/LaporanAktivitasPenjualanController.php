<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\JasaPengiriman;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use Illuminate\Http\Request;

class LaporanAktivitasPenjualanController extends Controller
{
	public function __construct(
        Akun $akun, Identitas $identitas, InformasiPelanggan $informasiPelanggan, JasaPengiriman $jasaPengiriman,
        KodePajak $kodePajak, Produk $produk, SyaratPembayaran $syaratPembayaran
    )
    {
        $this->akun                 =   $akun;
        $this->identitas            =   $identitas;
        $this->informasiPelanggan   =   $informasiPelanggan;
        $this->jasaPengiriman       =   $jasaPengiriman;
        $this->kodePajak            =   $kodePajak;
        $this->produk               =   $produk;
        $this->syaratPembayaran     =   $syaratPembayaran;
	}
	
    public function penawaranPenjualan(Request $request)
    {
    	$items = $request->all();
    	$arrayDetailBarang = [];
    	$kode_pajak 	   = [];
    	$identitas = $this->identitas->first();
    	$pelanggan = $this->informasiPelanggan->findOrFail($items['pelanggan_id']);

    	if (!empty($items['produk_id'])) {
    	$count = count($items['produk_id']);
	    	for ($i=0; $i < $count; $i++) {

	    		// mencari kode pajak
	    		if (!empty($items['kode_pajak_id'])) {
		            $temp = $this->kodePajak->whereIn('id', $items['kode_pajak_id'][$i])->get();
		            $kode_pajak = $temp->map(function ($item, $key) use(&$all_pajak, $items, $i) 
		            {
		            	return $item->kode_pajak_formatted;
		            });
	    		 } 

	    		// mencari no barang
	            $no_barang = $this->produk->findOrFail($items['produk_id'][$i]);
	            // Detail Barang
	    		$arrayDetailBarang[] = [
	    			'no_item'					=> $no_barang->no_barang,
		    		'item_deskripsi'			=> $items['keterangan_produk'][$i],
		    		'qty_produk'				=> $items['qty_produk'][$i],
		    		'unit_harga_produk'			=> $items['unit_harga_produk'][$i],
		    		'diskon_produk'				=> $items['diskon_produk'][$i],
		    		'kode_pajak'				=> $kode_pajak,
		    		'amount_produk'				=> $items['amount_produk'][$i],
	    		];
	    	}
    	}
    	$view = [
    		'items' 			=> $items,
    		'pelanggan'			=> $pelanggan,
    		'identitas'			=> $identitas,
    		'arrayDetailBarang'	=> $arrayDetailBarang,
    	];

    	return view('akuntansi::laporan.cetak_laporan.cetak_barang')->with($view);
    }

    public function pesananPenjualan(Request $request)
    {
    	$items = $request->all();
    	$arrayDetailBarang = [];
    	$kode_pajak 	   = [];
    	$identitas         = $this->identitas->first();
    	$pelanggan 		   = $this->informasiPelanggan->findOrFail($items['pelanggan_id']);
        $pelanggan_kirim   = null;
        
        if (!empty($items['pelanggan_kirim_ke_id'])) {
            $pelanggan_kirim = $this->informasiPelanggan->findOrFail($items['pelanggan_kirim_ke_id']);
        }
    	if (!empty($items['term_id'])) {
	    	$termin 		   = $this->syaratPembayaran->findOrFail($items['term_id'])->naration;
    	}
    	if (!empty($items['ship_id'])) {
	    	$ship 			   = $this->jasaPengiriman->findOrFail($items['ship_id'])->nama;
    	}
    	if (!empty($items['produk_id'])) {
    	$count = count($items['produk_id']);
	    	for ($i=0; $i < $count; $i++) {
	    		// mencari kode pajak
	    		if (!empty($items['kode_pajak_id'][$i])) {
		            $temp = $this->kodePajak->whereIn('id', $items['kode_pajak_id'][$i])->get();
		            $kode_pajak = $temp->map(function ($item, $key) 
		            {
		            	return $item->kode_pajak_formatted;
		            });
	    		 }
	    		
	    		// mencari no barang
	            $no_barang = $this->produk->findOrFail($items['produk_id'][$i]);
	            // Detail Barang
	    		$arrayDetailBarang[] = [
	    			'no_item'					=> $no_barang->no_barang,
		    		'item_deskripsi'			=> $items['keterangan_produk'][$i],
		    		'qty_produk'				=> $items['qty_produk'][$i],
		    		'unit_harga_produk'			=> $items['unit_harga_produk'][$i],
		    		'diskon_produk'				=> $items['diskon_produk'][$i],
		    		'kode_pajak'				=> $kode_pajak,
		    		'amount_produk'				=> $items['amount_produk'][$i],
	    		];
	    	}
    	}
    	$view = [
    		'items' 			=> $items,
    		'pelanggan'			=> $pelanggan,
    		'identitas'			=> $identitas,
    		'arrayDetailBarang'	=> $arrayDetailBarang,
    		'termin'			=> $termin ?? null,
    		'ship'				=> $ship ?? null,
            'pelanggan_kirim'   => $pelanggan_kirim
    	];
    	// dd($view);
    	return view('akuntansi::laporan.cetak_laporan.cetak_pesanan_penjualan')->with($view);
    }

    public function pengirimanPenjualan(Request $request)
    {
    	$items = $request->all();
    	$arrayDetailBarang = [];
    	$identitas         = $this->identitas->first();
    	$pelanggan 		   = $this->informasiPelanggan->findOrFail($items['pelanggan_id']);
        $pelanggan_kirim   = null;
        
        if (!empty($items['pelanggan_kirim_ke_id'])) {
            $pelanggan_kirim = $this->informasiPelanggan->findOrFail($items['pelanggan_kirim_ke_id']);
        }
        if (!empty($items['ship_id'])) {
	    	$ship 			   = $this->jasaPengiriman->findOrFail($items['ship_id'])->nama;
    	}
    	if (!empty($items['produk_id'])) {
    	$count = count($items['produk_id']);
	    	for ($i=0; $i < $count; $i++) {
	    		// mencari no barang
	            $no_barang = $this->produk->findOrFail($items['produk_id'][$i]);
	            // Detail Barang
	    		$arrayDetailBarang[] = [
	    			'no_item'					=> $no_barang->no_barang,
		    		'item_deskripsi'			=> $items['keterangan_produk'][$i],
		    		'qty_produk'				=> $items['qty_produk'][$i],
		    		'sn'						=> $items['sn'][$i],
	    		];
	    	}
    	}
    	$view = [
    		'items' 			=> $items,
    		'pelanggan'			=> $pelanggan,
            'pelanggan_kirim'   => $pelanggan_kirim,
    		'identitas'			=> $identitas,
    		'arrayDetailBarang'	=> $arrayDetailBarang,
    		'ship'				=> $ship
    	];
    	// dd($view);
    	return view('akuntansi::laporan.cetak_laporan.cetak_pengiriman_barang')->with($view);
    }

    public function fakturPenjualan(Request $request)
    {
        $items = $request->all();
        $arrayDetailBarang = [];
        $kode_pajak        = [];
        $identitas         = $this->identitas->first();
        $pelanggan         = $this->informasiPelanggan->findOrFail($items['pelanggan_id']);
        $pelanggan_kirim   = null;
        
        if (!empty($items['pelanggan_kirim_ke_id'])) {
            $pelanggan_kirim = $this->informasiPelanggan->findOrFail($items['pelanggan_kirim_ke_id']);
        }
        if (!empty($items['term_id'])) {
            $termin            = $this->syaratPembayaran->findOrFail($items['term_id'])->naration;
        }
        if (!empty($items['ship_id'])) {
            $ship              = $this->jasaPengiriman->findOrFail($items['ship_id'])->nama;
        }
        if (!empty($items['produk_id'])) {
        $count = count($items['produk_id']);
            for ($i=0; $i < $count; $i++) {
                // mencari kode pajak
                if (!empty($items['kode_pajak_id'][$i])) {
                    $temp = $this->kodePajak->whereIn('id', $items['kode_pajak_id'][$i])->get();
                    $kode_pajak = $temp->map(function ($item, $key) 
                    {
                        return $item->kode_pajak_formatted;
                    });
                 }
                
                // mencari no barang
                $no_barang = $this->produk->findOrFail($items['produk_id'][$i]);
                // Detail Barang
                $arrayDetailBarang[] = [
                    'no_item'                   => $no_barang->no_barang,
                    'item_deskripsi'            => $items['keterangan_produk'][$i],
                    'qty_produk'                => $items['qty_produk'][$i],
                    'unit_harga_produk'         => $items['unit_harga_produk'][$i],
                    'diskon_produk'             => $items['diskon_produk'][$i],
                    'kode_pajak'                => $kode_pajak,
                    'amount_produk'             => $items['amount_produk'][$i],
                    'sn'                        => $items['sn'][$i],
                ];
            }
        }
        $view = [
            'items'             => $items,
            'pelanggan'         => $pelanggan,
            'pelanggan_kirim'   => $pelanggan_kirim,
            'identitas'         => $identitas,
            'arrayDetailBarang' => $arrayDetailBarang,
            'termin'            => $termin ?? null,
            'ship'              => $ship ?? null,
        ];
        // dd($view);
        return view('akuntansi::laporan.cetak_laporan.cetak_faktur')->with($view);
    }

    public function penerimaanPenjualan(Request $request)
    {
        $items             = $request->all();
        $arrayFaktur       = [];
        $identitas         = $this->identitas->first();
        $pelanggan         = $this->informasiPelanggan->findOrFail($items['pelanggan_id']);
        $akun              = null;
        if (!empty($items['akun_bank_id'])) {
            $akun = $this->akun->findOrFail($items['akun_bank_id']);
        }
        if (!empty($items['faktur_id'])) {
        $count = count($items['faktur_id']);
            for ($i=0; $i < $count; $i++) {
                // Detail Barang
                $arrayFaktur[] = [
                    'no_faktur'                 => $items['no_faktur'][$i],
                    'tanggal'                   => $items['tanggal'][$i],
                    'amount'                    => $items['amount'][$i],
                    'owing'                     => $items['owing'][$i],
                    'payment_amount'            => $items['payment_amount'][$i],
                    'diskon'                    => $items['diskon'][$i],
                ];
            }
        }
        $view = [
            'items'             => $items,
            'pelanggan'         => $pelanggan,
            'identitas'         => $identitas,
            'arrayFaktur'       => $arrayFaktur,
            'akun'              => $akun
        ];
        return view('akuntansi::laporan.cetak_laporan.cetak_penjualan_penerimaan_pelanggan')->with($view);
    }

}
