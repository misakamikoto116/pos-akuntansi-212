<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\PenyelesaianPesananRequest;
use App\Modules\Akuntansi\Repositories\PenyelesaianPesananRepository;
use Generator\Interfaces\RepositoryInterface;
class PenyelesaianPesananController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Penyelesaian Pesanan';
        $this->request = PenyelesaianPesananRequest::class;
        $this->requestField = [
            'pembiayaan_pesanan_id',
            'tipe_penyelesaian',
            'tgl_selesai',
            'produk_id',
            'keterangan_produk',
            'qty_produk',
            'amount_produk',
            'alokasi_nilai',
            'presentase_produk',
            'gudang_id',
            'sn',
            'nama_akun_item',
            'kode_akun_item'
        ];
    }

    public function formData()
    {
        return [
            'gudang'     => $this->model->listGudang(),
            'akun'       => $this->model->listAkun(),
            'pembiayaan' => $this->model->listPembiayaan()
        ];
    }
}
