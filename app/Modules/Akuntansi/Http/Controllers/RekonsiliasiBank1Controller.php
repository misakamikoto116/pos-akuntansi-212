<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use Illuminate\Http\Request;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\Rekonsiliasi;
use Carbon\Carbon;

class RekonsiliasiBank1Controller extends Controller
{
    public function __construct(Rekonsiliasi $model, Akun $akun, Transaksi $transaksi)
	{
        $this->roles = 'akuntansi';
        $this->model = $model;
        $this->akun = $akun;
        $this->transaksi = $transaksi;
	}

	public function index()
	{        
		$listAkunId = $this->akun->whereDoesntHave('childAkun')->whereHas('tipeAkun', function ($query)
        {
            $query->where('title','Kas/Bank');
        })->where('nama_akun','!=','Kas & Bank')->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
        
    	return view('akuntansi::rekonsiliasi_bank.index', with([
    		'listAkunId' => $listAkunId
    	]));
	}

    public function save(Request $request)
    {
        $model = $this->model;
        $model->akun_id              = $request->akun_id;
        $model->saldo_rekening_koran = str_replace(",","", $request->saldo_rekening_koran);
        $model->kalkulasi_saldo      = str_replace(",","", $request->kalkulasi_saldo);
        $model->selisih_saldo        = str_replace(",","", $request->selisih_saldo);
        $model->tanggal_rekonsil     = Carbon::parse($request->tanggal_rekonsil)->format('Y-m-d H:i:s');
        $model->save();

        return redirect()->back(); 
    }

	public function getAkun(Request $request)
    {
        $akun_id = $request->get('id');
        if ($akun_id !== null) {
            $akun = $this->akun->where('id',$akun_id)->first();

            $response = [
                'kode_akun' => $akun->kode_akun,
                'nama_akun' => $akun->nama_akun
            ];
            return response()->json($response);
        }else {
            $response = [
                'kode_akun' => '',
            ];
            return response()->json($response);
        }
    }

    public function getRekonsiliasiBankData(Request $request)
    {
        $akun_id = $request->get('id');
        if ($akun_id == $request->id) {
            $semua_rekonsiliasi = $this->model->where('akun_id', $akun_id)->orderBy('id', 'DESC')->first();;
            $response = [
                'rekonsiliasi' => $semua_rekonsiliasi,
            ];
            return response()->json($response);
        }else {
            $response = [
                'rekonsiliasi' => '',
            ];
            return response()->json($response);
        }

    }

    public function getRekonsiliasiBank(Request $request)
    {
        $semua_transaksi = $this->transaksi->whereHas('akun', function ($query) use($request)
        {
            $query->where('id', $request->id);
        })->whereNotNull("status")->get()->sortBy('tanggal');

        $view = view('akuntansi::rekonsiliasi_bank.content', compact('semua_transaksi','semua_rekonsiliasi'))->render();
        return response()->json($view);
    }
}
