<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Http\Requests\ReturPenjualanRequest;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class ReturPenjualanController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Retur Penjualan';
        $this->request = ReturPenjualanRequest::class;
        $this->requestField = [
            'no_pelanggan',
            'pelanggan_id',
            'alamat_asal',
            'list-sales-invoice',
            'taxable',
            'intax',
            'sr_no',
            'tanggal',
            'keterangan',
            'barang_faktur_penjualan_id',
            'produk_id',
            'keterangan_produk',
            'qty_produk',
            'satuan_produk',
            'unit_harga_produk',
            'diskon_produk',
            'tax_produk',
            'amount_produk',
            'gudang_id',
            'diskon',
            'faktur_penjualan_id',
            'no_fiscal',
            'tgl_fiscal',
            'total',
            'harga_modal',
            'harga_terakhir',
            'harga_dgn_pajak',
            'jumlah_diskon_retur',
            'lanjutkan',
            'tax_cetak0',
            'tax_cetak1'
        ];
    }

    public function formData()
    {
        return [
            'pelanggan' => $this->model->listPelanggan(),
            'idPrediction' => $this->model->idPrediction(),
            'kode_pajak' => $this->model->listKodePajak(),
            // 'produk' => $this->model->listProduk(),
            'gudang' => $this->model->listGudang(),
            'filter' => ['no_faktur', 'pelanggan_select','tanggal_dari_sampai'],
            // 'kategori' => $this->model->listKategoriProduk(),
            // 'akun' => $this->model->listAkun(),
            // 'termin' => $this->model->listTermin(),
            // 'pengiriman' => $this->model->listJasaPengiriman(),
            // 'tipePelanggan' => $this->model->listTipePelanggan(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function redirectSuccess(Request $request, $result = null)
    {
        if (!empty($result['continue_stat'])) {
            return redirect('/akuntansi/retur-penjualan/create')->withMessage('Berhasil Menambah/Memperbarui data');
        } else {
            return redirect('/akuntansi/retur-penjualan')->withMessage('Berhasil Menambah/Memperbarui data');
        }
    }
}
