<?php
namespace App\Modules\Akuntansi\Http\Controllers;

use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
use App\Modules\Akuntansi\Models\Pembayaran;
use App\Modules\Akuntansi\Models\DetailBukuKeluar;
use App\Modules\Akuntansi\Http\Requests\PembayaranRequest;
use Cart;

class PembayaranController extends Controller
{
	public function __construct(RepositoryInterface $model)
	{
		$this->role         = 'akuntansi';
		$this->model        = $model;
		$this->title        = 'Data Pembayaran';
		$this->request      = PembayaranRequest::class;
		$this->requestField = ['akun_id',
			'no_faktur',
			'no_cek',
			'tanggal',
			'peruntukan',
			'keterangan',
			'nominal',
		];
	}

	public function formData()
	{
		$user = auth()->user();

		return [
			'listAkun'   => $this->model->listAkun(),
			'listAkunId' => $this->model->listAkunId(),
			'filter'	 => [
								'no_pembayaran',
								'keterangan_pembayaran',
								'tanggal_dari_sampai'
    						],
    		'permission' => [
		            'buat' => 'buat_pembayaran_lain',
		            'ubah' => 'ubah_pembayaran_lain',
		            'hapus' => 'hapus_pembayaran_lain',
		            'laporan' => 'laporan_pembayaran_lain',
		            'lihat' => 'lihat_pembayaran_lain',
		            'daftar' => $user->hasPermissionTo('daftar_pembayaran_lain'),
		        ]
		];
	}

	public function index()
    {
        return parent::index()->with($this->formData());
    }

	public function sessionApi(Request $request)
	{
		try {
			$uniqueId    = Cart::instance('pembayaran')->content()->pluck('id')->toArray();
			$newUniqueId = uniqid();
			while (in_array($newUniqueId, $uniqueId)) {
				$newUniqueId = uniqid();
			}

			$data = [
				'name'    => $newUniqueId,
				'qty'     => 1,
				'id'      => $newUniqueId,
				'options' => [
					'akun_id'          => $request->akun_detail_id,
					'nama_detail_akun' => $request->nama_detail_akun,
					'catatan_detail'   => $request->catatan_detail,
				],
				'price' => $request->nominal_detail,
			];

			$cart = Cart::instance('pembayaran')->add($data);

			$response = [
				'rowId'   => $cart->rowId,
				'name'    => $cart->name,
				'qty'     => $cart->qty,
				'id'      => $cart->id,
				'options' => [
					'akun_id'          => $cart->options->akun_id,
					'nama_detail_akun' => $request->nama_detail_akun,
					'catatan_detail'   => $request->catatan_detail,
				],
				'price' => $request->nominal_detail,
			];
		} catch (Exception $e) {
			$response = [];
		}

		return response()->json($response);
	}

	public function sessionUpdate(Request $request)
	{
		$data = [
			'options' => [
				'akun_id'          => $request->akun_detail_id,
				'nama_detail_akun' => $request->nama_detail_akun,
				'catatan_detail'   => $request->catatan_detail,
			],
			'price' => $request->nominal_detail,
		];

		$cart = Cart::instance('pembayaran')->update($request->rowId, $data);

		$response = [
			'rowId'   => $cart->rowId,
			'name'    => $cart->name,
			'qty'     => $cart->qty,
			'id'      => $cart->id,
			'options' => [
				'akun_id'          => $cart->options->akun_id,
				'nama_detail_akun' => $request->nama_detail_akun,
				'catatan_detail'   => $request->catatan_detail,
			],
			'price' => $request->nominal_detail,
		];

		return response()->json(['sukses' => true, 'data' => $response]);
	}

	public function sessionDelete(Request $request)
	{
		Cart::instance('pembayaran')->remove($request->id);

		return response()->json(['sukses' => true, 'data' => []]);
	}

	public function jurnal($id)
	{
		$buku_keluar        = Pembayaran::findOrFail($id);
		$detail_buku_keluar = DetailBukuKeluar::where('buku_keluar_id', $buku_keluar->id)->get();

		return view('akuntansi::pembayaran.jurnal', compact('buku_keluar', 'detail_buku_keluar'));
	}

	// ini sebelumnya di inject ke index
	public function embedDataToEdit($id)
	{
		$cart = Cart::instance('pembayaran');
		$cart->destroy();
		$data = Pembayaran::findOrFail($id);
		$anak = DetailBukuKeluar::where('buku_keluar_id', $data->id)->get();
		foreach ($anak as $child) {
			$uniqueId    = Cart::instance('pembayaran')->content()->pluck('id')->toArray();
			$newUniqueId = uniqid();
			while (in_array($newUniqueId, $uniqueId)) {
				$newUniqueId = uniqid();
			}
			$cc = [
				'name'    => $newUniqueId,
				'qty'     => 1,
				'id'      => $newUniqueId,
				'options' => [
					'akun_id'          => $child->akun->kode_akun,
					'nama_detail_akun' => $child->akun->nama_akun,
					'catatan_detail'   => $child->catatan,
				],
				'price' => $child->nominal,
			];
			$cart->add($cc);
		}

		return [$cart];
	}

	public function getLaporanPembayaran(Request $request)
	{
		if(empty($request->all())){
			$request['start-date'] = date('Y-m-1');
			$request['end-date'] = date('Y-m-t');
		}

		return $this->model->laporanPembayaran($request);
	}

	public function getCetakPembayaran(Request $data)
	{
		return $this->model->cetakPembayaran($data);
	}
}
