<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\JasaPengirimanRequest;
use App\Modules\Akuntansi\Repositories\JasaPengirimanRepository;
use Generator\Interfaces\RepositoryInterface;
class JasaPengirimanController extends Controller
{
	use ExportImportJasaPengiriman;
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Daftar Jasa Pengiriman';
        $this->request = JasaPengirimanRequest::class;
        $this->requestField = ['nama'];
    }
}
