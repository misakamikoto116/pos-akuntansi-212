<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\UangMukaPemasokRequest;
use App\Modules\Akuntansi\Repositories\UangMukaPemasokRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
class UangMukaPemasokController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Uang Muka Pemasok';
        $this->request = UangMukaPemasokRequest::class;
        $this->requestField = [
                                'pemasok_id',
                                'no_faktur',
                                'taxable',
                                'in_tax',
                                'diskon',
                                'invoice_no',
                                'invoice_date',
                                'total',
                                'catatan',
                                'uang_muka',
                                'produk_id',
                                'item_deskripsi',
                                'jumlah',
                                'unit_price',
                                'diskon_produk',
                                'kode_pajak_id',
                                'akun_uang_muka_id',
                                'akun_hutang_id',
                                'lanjutkan'
                              ];
    }


    public function formData()
    {
        return [
            'pemasok' => $this->model->listPemasok(),
            'produk' => $this->model->getDPFromProduk(),
            'pajak' => $this->model->listPajak(),
            'akun' => $this->model->listAkun(),
            'filter' => ['tanggal_dari_sampai', 'pemasok_select','no_faktur','keterangan_pembayaran'],
            'idPrediction' => $this->model->idPrediction(),
            'getAkunDp' => $this->model->getAkunDp(),
            'getAkunHutang' => $this->model->getAkunHutang(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function redirectSuccess(Request $request, $result = null)
    {
        if(!empty($result['continue_stat'])){
            return redirect('/akuntansi/uang-muka-pemasok/create')->withMessage('Berhasil Menambah/Memperbarui data');;
        }else{
            return redirect('/akuntansi/uang-muka-pemasok')->withMessage('Berhasil Menambah/Memperbarui data');;
        }
    }
    
}
