<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\SyaratPembayaranRequest;
use App\Modules\Akuntansi\Repositories\SyaratPembayaranRepository;
use Generator\Interfaces\RepositoryInterface;
class SyaratPembayaranController extends Controller
{
	use ExportImportSyaratPembayaran;
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Informasi Diskon';
        $this->request = SyaratPembayaranRequest::class;
        $this->requestField = ['jika_membayar_antara','akan_dapat_diskon','jatuh_tempo','keterangan','status_cod','nama'];
    }

}
