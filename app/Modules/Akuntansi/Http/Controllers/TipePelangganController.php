<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\TipePelangganRequest;
use App\Modules\Akuntansi\Repositories\TipePelangganRepository;
use Generator\Interfaces\RepositoryInterface;
class TipePelangganController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Tipe Pelanggan';
        $this->request = TipePelangganRequest::class;
        $this->requestField = ['nama'];
    }
}
