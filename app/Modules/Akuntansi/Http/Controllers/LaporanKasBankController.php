<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\MataUang;
use App\Modules\Akuntansi\Models\Pembayaran;
use App\Modules\Akuntansi\Models\Penerimaan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LaporanKasBankController extends Controller
{
    public function __construct(Akun $akun, Identitas $identitas, MataUang $mataUang, Pembayaran $pembayaran, Penerimaan $penerimaan)
    {
        $this->akun         =   $akun;
        $this->identitas    =   $identitas;
        $this->mataUang     =   $mataUang;
        $this->pembayaran   =   $pembayaran;
        $this->penerimaan   =   $penerimaan;
    }
    // Laporan Kas & Bank - kas & bank
    public function daftar_akun_kas_dan_bank(Request $request)
    {
        $money = $this->mataUang->with(['akun' => function ($query) use ($request) {
            $query->where('tipe_akun_id', 1);
            $query->with(['transaksi' => function ($query) use ($request) {
                $query->dateFilter($request);
            }]);
        }])->get();

        $data = [
            'data' => $money,
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.daftar_akun_kas_dan_bank')->with($data);
    }

    public function buku_bank(Request $request)
    {
        $nama_perusahaan        = $this->identitas->first()->nama_perusahaan ?? null;
        $akuns                  = $this->akun->with(['transaksi' => function ($query) use($request)
        {
            $query->dateBankFilter($request);
        },'mataUang'])->where('tipe_akun_id', 1)->with('childAkun')->whereNotNull('parent_id')->get();

        $view = [
            'title'                 => 'Buku Bank',
            'nama_perusahaan'       => $nama_perusahaan,
            'tanggal_awal'          => Carbon::parse($request['start-date'])->format('d F Y'),
            'tanggal_akhir'         => Carbon::parse($request['end-date'])->format('d F Y'),
            'items'                 => $this->bukuBankAkun($akuns),
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.buku_bank')->with($view);
    }

    public function rekonsili_bank(Request $request)
    {
        $nama_perusahaan        = $this->identitas->first()->nama_perusahaan ?? null;

        $view = [
            'title'                 => 'Rekonsiliasi Bank',
            'nama_perusahaan'       => $nama_perusahaan,
        ];
        
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.rekonsili_bank')->with($view);
    }

    public function histori_rekonsili_bank()
    {
        // return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.rekonsili_bank');
    }

    public function ringkasan_proyeksi_arus_kas()
    {
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.ringkasan_proyeksi_arus_kas');
    }

    public function rincian_proyeksi_arus_kas()
    {
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.rincian_proyeksi_arus_kas');
    }

    public function arus_kas_per_akun()
    {
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.arus_kas_per_akun');
    }

    public function giro_mundur_belum_jatuh_tempo()
    {
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.giro_mundur_belum_jatuh_tempo');
    }

    // Laporan Kas & Bank - Rincian Pembayaran dan Penerimaan
    public function rincian_daftar_pembayaran(Request $request)
    {
        $identitas      = $this->identitas->first()->nama_perusahaan ?? null;
        $pembayaran     = $this->pembayaran->with('detailBukuKeluar')->reportPembayaran($request)->get();

        $view = [
            'title'                 => 'Rincian Daftar Pembayaran',
            'nama_perusahaan'       => $identitas,
            'tanggal_awal'          => Carbon::parse($request['start-date'])->format('d F Y'),
            'tanggal_akhir'         => Carbon::parse($request['end-date'])->format('d F Y'),
            'items'                 => $pembayaran,
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.rincian_daftar_pembayaran')->with($view);
    }

    public function ringkasan_pembayaran_per_bank(Request $request)
    {
        $identitas      = $this->identitas->first()->nama_perusahaan ?? null;
        $akun_bank      = $this->akun->where('tipe_akun_id', 1)->whereHas('pembayaran')->with(['pembayaran','pembayaran.detailBukuKeluar','mataUang'])->reportAkunPembayaran($request)->get();

        $view = [
            'title'                 => 'Ringkasan Pembayaran per Bank',
            'nama_perusahaan'       => $identitas,
            'tanggal_awal'          => Carbon::parse($request['start-date'])->format('d F Y'),
            'tanggal_akhir'         => Carbon::parse($request['end-date'])->format('d F Y'),
            'items'                 => $akun_bank,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.ringkasan_pembayaran_per_bank')->with($view);
    }

    public function rincian_pembayaran_per_bank(Request $request)
    {
        $identitas      = $this->identitas->first()->nama_perusahaan ?? null;
        $akun_bank      = $this->akun->where('tipe_akun_id', 1)->whereHas('pembayaran')->with(['pembayaran' => function ($query) use ($request)
        {
            $query->reportPembayaran($request);
        },'pembayaran.detailBukuKeluar','mataUang'])->reportAkunPembayaran($request)->get();


        $view = [
            'title'                 => 'Rincian Pembayaran per Bank',
            'nama_perusahaan'       => $identitas,
            'tanggal_awal'          => Carbon::parse($request['start-date'])->format('d F Y'),
            'tanggal_akhir'         => Carbon::parse($request['end-date'])->format('d F Y'),
            'items'                 => $this->akunBank($akun_bank),
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.rincian_pembayaran_per_bank')->with($view);
    }

    public function rincian_daftar_penerimaan(Request $request)
    {
        $identitas      = $this->identitas->first()->nama_perusahaan ?? null;
        $penerimaan     = $this->penerimaan->with('detailBukuMasuk')->reportPenerimaan($request)->get();

        $view = [
            'title'                 => 'Rincian Daftar Penerimaan',
            'nama_perusahaan'       => $identitas,
            'tanggal_awal'          => Carbon::parse($request['start-date'])->format('d F Y'),
            'tanggal_akhir'         => Carbon::parse($request['end-date'])->format('d F Y'),
            'items'                 => $penerimaan,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.rincian_daftar_penerimaan')->with($view);
    }

    public function ringkasan_penerimaan_per_bank(Request $request)
    {
        $identitas      = $this->identitas->first()->nama_perusahaan ?? null;
        $akun_bank      = $this->akun->where('tipe_akun_id', 1)->whereHas('penerimaan')->with(['penerimaan','mataUang'])->reportAkunPenerimaan($request)->get();

        $view = [
            'title'                 => 'Ringkasan Penerimaan per Bank',
            'nama_perusahaan'       => $identitas,
            'tanggal_awal'          => Carbon::parse($request['start-date'])->format('d F Y'),
            'tanggal_akhir'         => Carbon::parse($request['end-date'])->format('d F Y'),
            'items'                 => $akun_bank,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.ringkasan_penerimaan_per_bank')->with($view);
    }

    public function rincian_penerimaan_per_bank(Request $request)
    {
        $identitas      = $this->identitas->first()->nama_perusahaan ?? null;
        $akun_bank      = $this->akun->where('tipe_akun_id', 1)->whereHas('penerimaan')->with(['penerimaan' => function ($query) use ($request)
        {
            $query->reportPenerimaan($request);
        },'penerimaan.detailBukuMasuk','mataUang'])->reportAkunPenerimaan($request)->get();


        $view = [
            'title'                 => 'Rincian Penerimaan per Bank',
            'nama_perusahaan'       => $identitas,
            'tanggal_awal'          => Carbon::parse($request['start-date'])->format('d F Y'),
            'tanggal_akhir'         => Carbon::parse($request['end-date'])->format('d F Y'),
            'items'                 => $this->akunBankPenerimaan($akun_bank),
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_kas_dan_bank.rincian_penerimaan_per_bank')->with($view);
    }


    // Fungsi - Fungsi
    public function akunBank($akun_bank)
    {
        return $akun_bank->map(function ($item_bank)
        {
            $data_pembayaran = $item_bank->pembayaran->map(function ($item_pembayaran)
            {
                $detail_pembayaran = $item_pembayaran->detailBukuKeluar->map(function ($item_detail_pembayaran) use ($item_pembayaran)
                {
                    return [
                        'tanggal_transaksi'         => Carbon::parse($item_pembayaran->tanggal)->format('d F Y'),
                        'no_bukti'                  => $item_pembayaran->no_faktur,
                        'no_akun'                   => $item_detail_pembayaran->akun->kode_akun,
                        'nama_akun'                 => $item_detail_pembayaran->akun->nama_akun,
                        'catatan'                   => $item_detail_pembayaran->catatan,
                        'nilai_bayar'               => $item_detail_pembayaran->nominal
                    ];
                });
                return $detail_pembayaran;
            });
            return [
                'nama_bank'         => $item_bank->nama_akun,
                'kode_mata_uang'    => $item_bank->mataUang->kode,
                'sum_nominal'       => $item_bank->pembayaran->sum('nominal'),
                'data_pembayaran'   => $data_pembayaran,
            ];
        });
    }

    public function akunBankPenerimaan($akun_bank)
    {
        return $akun_bank->map(function ($item_bank)
        {
            $data_penerimaan = $item_bank->penerimaan->map(function ($item_penerimaan)
            {
                $detail_penerimaan = $item_penerimaan->detailBukuMasuk->map(function ($item_detail_penerimaan) use ($item_penerimaan)
                {
                    return [
                        'tanggal_transaksi'         => Carbon::parse($item_penerimaan->tanggal)->format('d F Y'),
                        'no_bukti'                  => $item_penerimaan->no_faktur,
                        'no_akun'                   => $item_detail_penerimaan->akun->kode_akun,
                        'nama_akun'                 => $item_detail_penerimaan->akun->nama_akun,
                        'catatan'                   => $item_detail_penerimaan->catatan,
                        'amount_paid'               => $item_detail_penerimaan->nominal
                    ];
                });
                return $detail_penerimaan;
            });
            return [
                'nama_bank'         => $item_bank->nama_akun,
                'kode_mata_uang'    => $item_bank->mataUang->kode,
                'sum_nominal'       => $item_bank->penerimaan->sum('nominal'),
                'data_penerimaan'   => $data_penerimaan,
            ];
        });
    }

    public function bukuBankAkun($akuns)
    {
        return $akuns->map(function ($item_akun)
        {
            $saldo = $item_akun->money_function;
            $data_transaksi = $item_akun->transaksi->map(function ($item_transaksi) use (&$saldo)
            { 
                $nominal    = $this->cekDebetKreditBukuBank($item_transaksi);
                $saldo      = $this->hitungSaldoBukuBank($saldo, $nominal);
                return  [
                    'tanggal'           => Carbon::parse($item_transaksi->tanggal)->format('d F Y'),
                    'no_sumber'         => $item_transaksi->no_transaksi,
                    'keterangan'        => $item_transaksi->keterangan,
                    'penambahan'        => $nominal['debet'],
                    'pengurangan'       => $nominal['kredit'],
                    'saldo'             => $saldo,
                    'css_saldo'         => $saldo < 0 ? 'red' : '',
                ];
            });
            return [
                'no_akun'           => $item_akun->kode_akun,
                'nama_akun'         => $item_akun->nama_akun,
                'mata_uang'         => $item_akun->mataUang->kode,
                'saldo_awal'        => $item_akun->money_function,
                'transaksi'         => $data_transaksi,
                'sum_penambahan'    => $data_transaksi->sum('penambahan'),
                'sum_pengurangan'   => $data_transaksi->sum('pengurangan'),
            ];
        });
    }

    public function cekDebetKreditBukuBank($item_transaksi)
    {
        $kredit     = 0;
        $debet      = 0;
        if ($item_transaksi->status == 0) {
            $kredit = $item_transaksi->nominal;
        }else if ($item_transaksi->status == 1) {
            $debet = $item_transaksi->nominal;
        }
        return [
            'kredit'        => $kredit,
            'debet'         => $debet,
        ];
    }

    public function hitungSaldoBukuBank($saldo, $nominal)
    {
        if ($nominal['debet'] !== 0) {
            $saldo = $saldo + $nominal['debet'];
        }else if ($nominal['kredit'] !== 0 ) {
            $saldo = $saldo - $nominal['kredit'];
        }
        return $saldo;
    }
}
