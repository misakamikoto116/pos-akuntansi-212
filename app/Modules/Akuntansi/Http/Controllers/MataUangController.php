<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\MataUangRequest;
use App\Modules\Akuntansi\Repositories\MataUangRepository;
use Generator\Interfaces\RepositoryInterface;
class MataUangController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Mata Uang';
        $this->request = MataUangRequest::class;
        $this->requestField = ['nama','negara','kode_simbol','kode','nilai_tukar'];
    }
}
