<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\PenerimaanPenjualan;
use App\Modules\Akuntansi\Models\TipePelanggan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LaporanAkunPiutangPelangganController extends Controller
{
    public function __construct(FakturPenjualan $fakturPenjualan, Identitas $identitas, TipePelanggan $tipePelanggan,
                                InformasiPelanggan $informasiPelanggan, PenerimaanPenjualan $penerimaanPenjualan)
    {
        $this->fakturPenjualan         =   $fakturPenjualan;
        $this->identitas               =   $identitas;
        $this->informasiPelanggan      =   $informasiPelanggan;
        $this->penerimaanPenjualan     =   $penerimaanPenjualan;
        $this->tipePelanggan           =   $tipePelanggan;
    }

    public function faktur_belum_lunas(Request $request)
    {
        $arrayItems                = [];
        $nama_perusahaan           = $this->identitas->first();
        $grand_sum_total           = 0;    
        $grand_sum_hutang_asing    = 0;	
        $pelanggan                 = $this->informasiPelanggan->whereHas('fakturPenjualan')->with(['fakturPenjualan' => function($query) use($request){
            $query->where('uang_muka', 0)->reportInvoiceFilter($request);
        }, 'fakturPenjualan.invoice' => function ($query1) use ($request)
        {
            $query1->reportInvoiceFilter($request);
        }, 'fakturPenjualan.returPenjualan' => function ($query2) use ($request)
        {
            $query2->reportInvoiceFilter($request);
        }])->reportInvoiceFilter($request)->get();

        foreach ($pelanggan as $key => $data_pelanggan) {
            $arrayFaktur        = [];
            $sum_hutang_asing   = 0;
            $sum_total          = 0;
            if ($data_pelanggan->fakturPenjualan) {
                foreach ($data_pelanggan->fakturPenjualan as $key1 => $data_faktur) {
                    $tanggal_faktur             = Carbon::parse($data_faktur->invoice_date);

                    $temp_tanggal_faktur        = Carbon::parse($data_faktur->invoice_date);

                    $hutang_asing               = $data_faktur->faktur_kurang_uang_muka - $data_faktur->invoice->sum('payment_amount') - $data_faktur->returPenjualan->sum('total');
                    $tgl_jatuh_tempo            = $tanggal_faktur->diff(Carbon::parse($request['start-date']))->days;

                    if (!empty($data_faktur->syaratPembayaran)) {
                        $jatuh_tempo = $temp_tanggal_faktur->addDays($data_faktur->syaratPembayaran->jatuh_tempo);
                    }else {
                        $jatuh_tempo = $temp_tanggal_faktur;
                    }

                    $sum_hutang_asing           += $hutang_asing;
                    $sum_total                  += $data_faktur->total;

                    $grand_sum_total            += $data_faktur->total;
                    $grand_sum_hutang_asing     += $hutang_asing;


                    $arrayFaktur[] = [
                        'no_faktur'         => $data_faktur->no_faktur,
                        'tanggal_faktur'    => $tanggal_faktur->format('d F Y'),
                        'jatuh_tempo'       => $jatuh_tempo->format('d F Y'),
                        'nilai_faktur'      => number_format($data_faktur->total),
                        'hutang_asing'      => number_format($hutang_asing),
                        'hutang_pajak'      => 0,
                        'tgl_jatuh_tempo'   => $tgl_jatuh_tempo,
                    ];
                }
            }
            $arrayItems[] = [
                'nomor_pelanggan'       => $data_pelanggan->no_pelanggan,
                'nama_pelanggan'        => $data_pelanggan->nama,
                'faktur'                => $arrayFaktur,
                'sum_hutang_asing'      => number_format($sum_hutang_asing),
                'sum_total'             => number_format($sum_total),
            ];
        }

        $typeFilter = [
            'single_date',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_akun_piutang_pelanggan_in_form_filer';

        $view = [
            'title'                     => 'Laporan Faktur Belum Lunas',
            'perusahaan'                => $nama_perusahaan,
            'items'                     => $arrayItems,
            'tanggal'                   => Carbon::parse($request['start-date'])->format('d F Y'),
            'grand_sum_total'           => number_format($grand_sum_total),
            'grand_sum_hutang_asing'    => number_format($grand_sum_hutang_asing),
            'formFilter'                => $formFilter,
            'typeFilter'                => $typeFilter,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.faktur_belum_lunas')->with($view);
    }

    public function ringkasan_umur_piutang(Request $request)
    {
        $arrayItems                         = [];
        $nama_perusahaan                    = $this->identitas->first();
        $tanggal                            = Carbon::parse($request['start-date']);
        $grand_sum_total                    = 0;
        $grand_sum_faktur_kurang_30         = 0;
        $grand_sum_faktur_kurang_60         = 0;
        $grand_sum_faktur_kurang_90         = 0;
        $grand_sum_faktur_kurang_120        = 0;
        $grand_sum_faktur_lebih_120         = 0;
        $grand_sum_faktur_belum             = 0;
        $pelanggan                          = $this->informasiPelanggan->whereHas('fakturPenjualan')->with(['fakturPenjualan' => function($query) use($request){
            $query->reportRincianFilter($request);
        }, 'fakturPenjualan.invoice' => function ($query1) use ($request)
        {
            $query1->reportRincianFilter($request);
        }, 'fakturPenjualan.returPenjualan' => function ($query2) use ($request)
        {
            $query2->reportRincianFilter($request);
        }])->reportRincianFilter($request)->get();


        foreach ($pelanggan as $key => $data_pelanggan) {
            $sum_total                = 0;
            $sum_faktur_kurang_30     = 0;
            $sum_faktur_kurang_60     = 0;
            $sum_faktur_kurang_90     = 0;
            $sum_faktur_kurang_120    = 0;
            $sum_faktur_lebih_120     = 0;
            $sum_faktur_belum         = 0;
            if ($data_pelanggan->fakturPenjualan) {
                foreach ($data_pelanggan->fakturPenjualan as $key1 => $data_faktur) {
                    $tanggal_faktur             = Carbon::parse($data_faktur->invoice_date);
                    $nilai_faktur               = $data_faktur->faktur_kurang_uang_muka - $data_faktur->invoice->sum('payment_amount') - $data_faktur->returPenjualan->sum('total');
                    $sum_total                  += $nilai_faktur;
                    $grand_sum_total           += $nilai_faktur;
                    $selisih_hari               = $tanggal->diff($tanggal_faktur)->days;

                    if ($nilai_faktur >= 1) {
                        if ($selisih_hari == 0) {
                            $sum_faktur_belum               += $nilai_faktur;
                            $grand_sum_faktur_belum         += $nilai_faktur;
                        }else if ($selisih_hari <= 30) {
                            $sum_faktur_kurang_30           += $nilai_faktur;
                            $grand_sum_faktur_kurang_30     += $nilai_faktur;
                        }else if ($selisih_hari <= 60 && $selisih_hari > 30) {
                            $sum_faktur_kurang_60           += $nilai_faktur;
                            $grand_sum_faktur_kurang_60     += $nilai_faktur;
                        }else if ($selisih_hari <= 90 && $selisih_hari > 60) {
                            $sum_faktur_kurang_90           += $nilai_faktur;
                            $grand_sum_faktur_kurang_90     += $nilai_faktur;
                        }else if ($selisih_hari <= 120 && $selisih_hari > 90) {
                            $sum_faktur_kurang_120          += $nilai_faktur;
                            $grand_sum_faktur_kurang_120    += $nilai_faktur;
                        }else if ($selisih_hari > 120) {
                            $sum_faktur_lebih_120           += $nilai_faktur;
                            $grand_sum_faktur_lebih_120     += $nilai_faktur;
                        }
                    }

                }
            }
            $arrayItems[] = [
                'nama_pelanggan'        => $data_pelanggan->nama,
                'sum_total'             => number_format($sum_total),
                'sum_faktur_kurang_30'  => number_format($sum_faktur_kurang_30),
                'sum_faktur_kurang_60'  => number_format($sum_faktur_kurang_60),
                'sum_faktur_kurang_90'  => number_format($sum_faktur_kurang_90),
                'sum_faktur_kurang_120' => number_format($sum_faktur_kurang_120),
                'sum_faktur_lebih_120'  => number_format($sum_faktur_lebih_120),
                'sum_faktur_belum'      => number_format($sum_faktur_belum),
            ];
        }

        $typeFilter = [
            'single_date',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_akun_piutang_pelanggan_in_form_filer';

        $view = [
            'nama_perusahaan'               => $nama_perusahaan,
            'tanggal'                       => $tanggal->format('d F Y'),
            'items'                         => $arrayItems,
            'grand_sum_total'               => number_format($grand_sum_total),
            'grand_sum_faktur_kurang_30'    => number_format($grand_sum_faktur_kurang_30),
            'grand_sum_faktur_kurang_60'    => number_format($grand_sum_faktur_kurang_60),
            'grand_sum_faktur_kurang_90'    => number_format($grand_sum_faktur_kurang_90),
            'grand_sum_faktur_kurang_120'   => number_format($grand_sum_faktur_kurang_120),
            'grand_sum_faktur_lebih_120'    => number_format($grand_sum_faktur_lebih_120),
            'grand_sum_faktur_belum'        => number_format($grand_sum_faktur_belum),
            'typeFilter'                    => $typeFilter,
            'formFilter'                    => $formFilter
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.ringkasan_umur_piutang')->with($view);
    }

    public function rincian_umur_piutang(Request $request)
    {
        $arrayItems      = [];
        $nama_perusahaan = $this->identitas->first();
        $tanggal         = Carbon::parse($request['start-date']);  
        $pelanggan       = $this->informasiPelanggan->whereHas('fakturPenjualan')->with(['fakturPenjualan' => function($query) use($request){
            $query->reportRincianFilter($request);
        }, 'fakturPenjualan.invoice' => function ($query1) use ($request)
        {
            $query1->reportRincianFilter($request);
        }, 'fakturPenjualan.returPenjualan' => function ($query2) use ($request)
        {
            $query2->reportRincianFilter($request);
        }])->reportRincianFilter($request)->get();

        foreach ($pelanggan as $key => $data_pelanggan) {
            $arrayFaktur        = [];
            $sum_hutang_asing   = 0;
            $sum_total          = 0;
            $sum_faktur_kurang_30     = 0;
            $sum_faktur_kurang_60     = 0;
            $sum_faktur_kurang_90     = 0;
            $sum_faktur_kurang_120    = 0;
            $sum_faktur_lebih_120     = 0;
            $sum_faktur_belum         = 0;
            if ($data_pelanggan->fakturPenjualan) {
                foreach ($data_pelanggan->fakturPenjualan as $key1 => $data_faktur) {
                    $tanggal_faktur             = Carbon::parse($data_faktur->invoice_date);
                    $nilai_faktur               = $data_faktur->faktur_kurang_uang_muka - $data_faktur->invoice->sum('payment_amount') - $data_faktur->returPenjualan->sum('total');
                    $sum_total                  += $nilai_faktur;
                    $selisih_hari               = $tanggal->diff($tanggal_faktur)->days;

                    $nilai_faktur_belum         = 0;
                    $nilai_faktur_kurang_30     = 0;
                    $nilai_faktur_kurang_60     = 0;
                    $nilai_faktur_kurang_90     = 0;
                    $nilai_faktur_kurang_120    = 0;
                    $nilai_faktur_lebih_120     = 0;

                    if ($nilai_faktur >= 1) {
                        if ($selisih_hari == 0) {
                            $nilai_faktur_belum     = $nilai_faktur;
                            $sum_faktur_belum      += $nilai_faktur;
                        }else if ($selisih_hari <= 30) {
                            $nilai_faktur_kurang_30 = $nilai_faktur;
                            $sum_faktur_kurang_30  += $nilai_faktur;
                        }else if ($selisih_hari <= 60 && $selisih_hari > 30) {
                            $nilai_faktur_kurang_60 = $nilai_faktur;
                            $sum_faktur_kurang_60  += $nilai_faktur;
                        }else if ($selisih_hari <= 90 && $selisih_hari > 60) {
                            $nilai_faktur_kurang_90 = $nilai_faktur;
                            $sum_faktur_kurang_90  += $nilai_faktur;
                        }else if ($selisih_hari <= 120 && $selisih_hari > 90) {
                            $nilai_faktur_kurang_120 = $nilai_faktur;
                            $sum_faktur_kurang_120  += $nilai_faktur;
                        }else if ($selisih_hari > 120) {
                            $nilai_faktur_lebih_120 = $nilai_faktur;
                            $sum_faktur_lebih_120  += $nilai_faktur;
                        }

                        $arrayFaktur[] = [
                            'no_faktur'                     => $data_faktur->no_faktur,
                            'tanggal_faktur'                => $tanggal_faktur->format('d F Y'),
                            'nilai_faktur'                  => number_format($nilai_faktur),
                            'nilai_faktur_kurang_30'        => number_format($nilai_faktur_kurang_30),
                            'nilai_faktur_kurang_60'        => number_format($nilai_faktur_kurang_60),
                            'nilai_faktur_kurang_90'        => number_format($nilai_faktur_kurang_90),
                            'nilai_faktur_kurang_120'       => number_format($nilai_faktur_kurang_120),
                            'nilai_faktur_lebih_120'        => number_format($nilai_faktur_lebih_120),
                            'nilai_faktur_belum'            => number_format($nilai_faktur_belum),
                        ];
                    }

                }
            }
            $arrayItems[] = [
                'nomor_pelanggan'       => $data_pelanggan->no_pelanggan,
                'nama_pelanggan'        => $data_pelanggan->nama,
                'faktur'                => $arrayFaktur,
                'sum_total'             => number_format($sum_total),
                'sum_faktur_kurang_30'  => number_format($sum_faktur_kurang_30),
                'sum_faktur_kurang_60'  => number_format($sum_faktur_kurang_60),
                'sum_faktur_kurang_90'  => number_format($sum_faktur_kurang_90),
                'sum_faktur_kurang_120' => number_format($sum_faktur_kurang_120),
                'sum_faktur_lebih_120'  => number_format($sum_faktur_lebih_120),
                'sum_faktur_belum'      => number_format($sum_faktur_belum),
            ];
        }

        $typeFilter = [
            'single_date',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_akun_piutang_pelanggan_in_form_filer';

        $view = [
            'nama_perusahaan'   => $nama_perusahaan,
            'per_tanggal'       => $tanggal->format('d F Y'),
            'items'             => $arrayItems,
            'typeFilter'        => $typeFilter,
            'formFilter'        => $formFilter
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.rincian_umur_piutang')->with($view);
    }

    public function ringkasan_buku_besar_pembantu_piutang(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first();
        $tanggal_awal                       = Carbon::parse($request['start-date-rincian']);
        $tanggal_akhir                      = Carbon::parse($request['end-date-rincian']);
        $pelanggan                          = $this->informasiPelanggan->filterAvailablePelanggan($request)->with(['fakturPenjualan' => function($query) use($request){
            $query->reportPenerimaanFilter($request);
        }, 'penerimaanPenjualan' => function ($query1) use ($request)
        {
            $query1->reportPenerimaanFilter($request);
        }, 'returPenjualan' => function ($query2) use ($request)
        {
            $query2->reportPenerimaanFilter($request);
        }])->get();

        $dataPelanggan = $pelanggan->map(function ($item, $key)
        {
            $transaksi_uang_muka = $item->fakturPenjualan->where('uang_muka','0')->map(function ($item_uang_muka)
            {
                $item_uang_muka->sum_uang_muka =  $item_uang_muka->transaksiUangMuka->sum('jumlah');
                return $item_uang_muka;
            });
            $saldo_pelanggan                    = $item->detailInformasiPelanggan->sum('saldo_awal') + ($item->fakturPenjualan->where('uang_muka','0')->sum('faktur_kurang_uang_muka') + $transaksi_uang_muka->sum('sum_uang_muka') + -$item->penerimaanPenjualan->sum('cheque_amount') + -$item->returPenjualan->sum('total'));

            $item->saldo_awal                   = $item->detailInformasiPelanggan->sum('saldo_awal');
            $item->sum_faktur_penjualan         = $item->fakturPenjualan->where('uang_muka','0')->sum('faktur_kurang_uang_muka') + $transaksi_uang_muka->sum('sum_uang_muka');
            $item->sum_penerimaan_penjualan     = $item->penerimaanPenjualan->sum('cheque_amount');
            $item->sum_retur_penjualan          = $item->returPenjualan->sum('total');
            $item->sum_uang_muka                = 0;
            $item->sum_bukti_jurnal             = 0;
            $item->saldo                        = $saldo_pelanggan;
            return $item;
        });

        $typeFilter = [
            'double_date_not_month',
            'pelanggan_transaksi',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_akun_piutang_pelanggan_in_form_filer';

        $view = [
            'nama_perusahaan'       => $nama_perusahaan,
            'tanggal_awal'          => $tanggal_awal->format('d F Y'),
            'tanggal_akhir'         => $tanggal_akhir->format('d F Y'),
            'items'                 => $dataPelanggan,
            'typeFilter'            => $typeFilter,
            'formFilter'            => $formFilter,
        ];

    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.ringkasan_buku_besar_pembantu_piutang')->with($view);
    }

    public function rincian_buku_besar_pembantu_piutang(Request $request)
    {
        $arrayItems = [];
        $nama_perusahaan                    = $this->identitas->first();
        $tanggal_awal                       = Carbon::parse($request['start-date-rincian']);
        $tanggal_akhir                      = Carbon::parse($request['end-date-rincian']);
        $pelanggan                          = $this->informasiPelanggan->filterAvailablePelanggan($request)->with(['fakturPenjualan' => function($query) use($request){
            $query->reportPenerimaanFilter($request);
        }, 'penerimaanPenjualan' => function ($query1) use ($request)
        {
            $query1->reportPenerimaanFilter($request);
        }, 'returPenjualan' => function ($query2) use ($request)
        {
            $query2->reportPenerimaanFilter($request);
        }])->get();

        $dataPelanggan = $pelanggan->map(function ($item, $key)
        {
            $arrayFaktur = [];
            foreach ($item->fakturPenjualan as $key => $data_faktur) {
                $temp = [
                    'tanggal'               => $data_faktur->invoice_date,
                    'sumber'                => 'Faktur Penjualan',
                    'no_sumber'             => $data_faktur->no_faktur,
                    'keterangan_sumber'     => $data_faktur->keterangan !== null ? $data_faktur->keterangan : 'Faktur Penjualan : '.$data_faktur->no_faktur,
                    'nilai_sumber'          => $data_faktur->faktur_kurang_uang_muka,
                    'nilai_pajak'           => 0,
                    'saldo_asing'           => 0,
                    'saldo_pajak'           => 0,
                ];
                $arrayFaktur[] = $temp;
                if ($data_faktur->uang_muka) {
                    $temp['nilai_sumber'] = -$temp['nilai_sumber'];
                    $arrayFaktur[] = $temp;
                }
                if ($data_faktur->transaksiUangMuka->isNotEmpty()) {
                    foreach ($data_faktur->transaksiUangMuka as $key => $uang_muka) {
                        $temp['nilai_sumber'] = $uang_muka->jumlah;
                    }
                    $arrayFaktur[] = $temp;
                }
            }

            foreach ($item->penerimaanPenjualan as $key => $data_penerimaan) {
                $arrayFaktur[] = [
                    'tanggal'               => $data_penerimaan->cheque_date,
                    'sumber'                => 'Penerimaan Penjualan',
                    'no_sumber'             => $data_penerimaan->form_no,
                    'keterangan_sumber'     => $data_penerimaan->memo !== null ? $data_penerimaan->memo : 'Penerimaan Penjualan : '.$data_penerimaan->form_no,
                    'nilai_sumber'          => -$data_penerimaan->cheque_amount,
                    'nilai_pajak'           => 0,
                    'saldo_asing'           => 0,
                    'saldo_pajak'           => 0,
                ];
            }

            foreach ($item->returPenjualan as $key => $data_retur) {
                $arrayFaktur[] = [
                    'tanggal'               => $data_retur->tanggal,
                    'sumber'                => 'Retur Penjualan',
                    'no_sumber'             => $data_retur->sr_no,
                    'keterangan_sumber'     => $data_retur->keterangan !== null ? $data_retur->keterangan : 'Retur Penjualan : '.$data_retur->sr_no,
                    'nilai_sumber'          => -$data_retur->total,
                    'nilai_pajak'           => 0,
                    'saldo_asing'           => 0,
                    'saldo_pajak'           => 0,
                ];
            }

            $item->data = collect($arrayFaktur)->sortBy('tanggal');
            return $item;
        });

        $typeFilter = [
            'double_date_not_month',
            'pelanggan_transaksi',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_akun_piutang_pelanggan_in_form_filer';

        $view = [
            'nama_perusahaan'       => $nama_perusahaan,
            'tanggal_awal'          => $tanggal_awal->format('d F Y'),
            'tanggal_akhir'         => $tanggal_akhir->format('d F Y'),
            'items'                 => $dataPelanggan,
            'typeFilter'            => $typeFilter,
            'formFilter'            => $formFilter,
        ];

    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.rincian_buku_besar_pembantu_piutang')->with($view);
    }

    public function histori_piutang_pelanggan(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first()->nama_perusahaan ?? null;
        $pelanggan                          = $this->informasiPelanggan->orWhereHas('fakturPenjualan')->orWhereHas('penerimaanPenjualan')->orWhereHas('returPenjualan')->with(['fakturPenjualan' => function($query) use($request){
            $query->reportPenerimaanFilter($request);
        }, 'penerimaanPenjualan' => function ($query1) use ($request)
        {
            $query1->reportPenerimaanFilter($request);
        }, 'returPenjualan' => function ($query2) use ($request)
        {
            $query2->reportPenerimaanFilter($request);
        }])->get();


        $typeFilter = [
            'double_date_not_month',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_akun_piutang_pelanggan_in_form_filer';

        $view = [
            'title'                 => 'Histori Piutang Pelanggan',
            'nama_perusahaan'       => $nama_perusahaan,
            'tanggal_awal'          => Carbon::parse($request['start-date-rincian'])->format('d F Y'),
            'tanggal_akhir'         => Carbon::parse($request['end-date-rincian'])->format('d F Y'),
            'items'                 => $this->historiPiutangPelanggan($pelanggan),
            'total_all'             => $this->historiPiutangPelanggan($pelanggan)->sum('total_perubahan'),
            'count_faktur'          => $this->historiPiutangPelanggan($pelanggan)->sum('count_faktur'),
            'count_penerimaan'      => $this->historiPiutangPelanggan($pelanggan)->sum('count_penerimaan'),
            'count_retur'           => $this->historiPiutangPelanggan($pelanggan)->sum('count_retur'),
            'sum_faktur_all'        => $this->historiPiutangPelanggan($pelanggan)->sum('sum_faktur'),
            'sum_penerimaan_all'    => $this->historiPiutangPelanggan($pelanggan)->sum('sum_penerimaan'),
            'sum_retur_all'         => $this->historiPiutangPelanggan($pelanggan)->sum('sum_retur'),
            'typeFilter'            => $typeFilter,
            'formFilter'            => $formFilter,
        ];

    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.histori_piutang_pelanggan')->with($view);
    }

    public function ringkasan_pembayaran_faktur(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first()->nama_perusahaan ?? null;
        $faktur                             = $this->fakturPenjualan->where('uang_muka', 0)->with('invoice')->reportPenerimaanFilter($request)->get();

        $typeFilter = [
            'double_date_not_month',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_akun_piutang_pelanggan_in_form_filer';

        $view = [
            'title'             => 'Ringkasan Pembayaran Faktur',
            'nama_perusahaan'   => $nama_perusahaan,
            'tanggal_awal'      => Carbon::parse($request['start-date-rincian'])->format('d F Y'),
            'tanggal_akhir'     => Carbon::parse($request['end-date-rincian'])->format('d F Y'),
            'items'             => $this->ringkasanPembayaranFaktur($faktur),
            'typeFilter'        => $typeFilter,
            'formFilter'        => $formFilter,
        ];

    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.ringkasan_pembayaran_faktur')->with($view);
    }

    public function rincian_pembayaran_faktur(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first()->nama_perusahaan ?? null;
        $pelanggan                          = $this->informasiPelanggan->whereHas('fakturPenjualan')->with(['fakturPenjualan' => function($query) use($request){
            $query->where('uang_muka',0)->reportPenerimaanFilter($request);
        }, 'fakturPenjualan.invoice','fakturPenjualan.returPenjualan'])->get();

        $typeFilter = [
            'double_date_not_month',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_akun_piutang_pelanggan_in_form_filer';

        $view = [
            'title'             => 'Rincian Pembayaran Faktur',
            'nama_perusahaan'   => $nama_perusahaan,
            'tanggal_awal'      => Carbon::parse($request['start-date-rincian'])->format('d F Y'),
            'tanggal_akhir'     => Carbon::parse($request['end-date-rincian'])->format('d F Y'),
            'items'             => $this->rincianPembayaranFaktur($pelanggan),
            'typeFilter'        => $typeFilter,
            'formFilter'        => $formFilter,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.rincian_pembayaran_faktur')->with($view);
    }

    public function laporan_pelanggan(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first()->nama_perusahaan ?? null;
        $pelanggan                          = $this->informasiPelanggan->filterAvailablePelanggan($request)->with(['fakturPenjualan' => function($query) use($request){
            $query->reportPenerimaanFilter($request);
        }, 'penerimaanPenjualan' => function ($query1) use ($request)
        {
            $query1->reportPenerimaanFilter($request);
        }, 'returPenjualan' => function ($query2) use ($request)
        {
            $query2->reportPenerimaanFilter($request);
        }, 'detailInformasiPelanggan'])->get();

        $typeFilter = [
            'double_date_not_month',
            'pelanggan_transaksi',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_akun_piutang_pelanggan_in_form_filer';

        $view = [
            'title'                 => 'Laporan Pelanggan',
            'nama_perusahaan'       => $nama_perusahaan,
            'tanggal_awal'          => Carbon::parse($request['start-date-rincian'])->format('d F Y'),
            'tanggal_akhir'         => Carbon::parse($request['end-date-rincian'])->format('d F Y'),
            'items'                 => $this->laporanPelanggan($pelanggan),
            'typeFilter'            => $typeFilter,
            'formFilter'            => $formFilter,
        ];        

    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_pelanggan')->with($view);
    }

    public function penagihan_per_pelanggan(Request $request)
    {
        $request['start-date-rincian'] === null ? $request['start-date-rincian'] = date('m-Y') : $request['start-date-rincian'];
        $request['end-date-rincian'] === null ? $request['end-date-rincian'] = date('12-Y') : $request['end-date-rincian'];

        $nama_perusahaan        = $this->identitas->first()->nama_perusahaan ?? null;
        $tanggal                = $this->formatMonthYear($request);
        $pelanggan              = $this->informasiPelanggan->whereHas('fakturPenjualan')->with(['fakturPenjualan.invoice','fakturPenjualan.syaratPembayaran'])->reportPenagihanPelanggan($request)->get();
        $penagihan              = $this->penagihanPerPelanggan($pelanggan);


        $view = [
            'title'             => 'Penagihan Per Pelanggan',
            'nama_perusahaan'   => $nama_perusahaan,
            'bulan_awal'        => $tanggal['bulan_awal'],
            'bulan_akhir'       => $tanggal['bulan_akhir'],
            'items'             => $penagihan,
            'sum_hari_bayar'    => $penagihan->sum('hari_bayar'),
            'sum_syarat_bayar'  => $penagihan->sum('syarat_bayar'),
            'sum_nunggak'       => $penagihan->sum('nunggak')
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.penagihan_per_pelanggan')->with($view);
    }

    public function penagihan_per_tipe_pelanggan(Request $request)
    {
        $request['start-date-rincian'] === null ? $request['start-date-rincian'] = date('m-Y') : $request['start-date-rincian'];
        $request['end-date-rincian'] === null ? $request['end-date-rincian'] = date('12-Y') : $request['end-date-rincian'];

        $nama_perusahaan        = $this->identitas->first()->nama_perusahaan ?? null;
        $tanggal                = $this->formatMonthYear($request);
        $pelanggan              = $this->tipePelanggan->whereHas('pelanggan.fakturPenjualan')->with(['pelanggan' => function ($query) use ($request)
        {
            $query->reportPenagihanPelanggan($request);
        },'pelanggan.fakturPenjualan.invoice','pelanggan.fakturPenjualan.syaratPembayaran'])->get();

        $penagihan              = $this->penagihanPerTipePelanggan($pelanggan);

        $view = [
            'title'             => 'Penagihan Per Tipe Pelanggan',
            'nama_perusahaan'   => $nama_perusahaan,
            'bulan_awal'        => $tanggal['bulan_awal'],
            'bulan_akhir'       => $tanggal['bulan_akhir'],
            'items'             => $penagihan,
            'sum_hari_bayar'    => $penagihan->sum('hari_bayar'),
            'sum_syarat_bayar'  => $penagihan->sum('syarat_bayar'),
            'sum_nunggak'       => $penagihan->sum('nunggak')
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.penagihan_per_tipe_pelanggan')->with($view);
    }

    // Laporan Akun Piutang dan Pelanggan - Data Pelanggan

    public function daftar_pelanggan(Request $request)
    {
        $arrayItems                         = [];
        $nama_perusahaan                    = $this->identitas->first();
        $tanggal                            = Carbon::parse($request['start-date']);
        $grand_sum_total                    = 0;
        $pelanggan                          = $this->informasiPelanggan->whereHas('fakturPenjualan')->with(['fakturPenjualan' => function($query) use($request){
            $query->reportRincianFilter($request);
        }, 'fakturPenjualan.invoice' => function ($query1) use ($request)
        {
            $query1->reportRincianFilter($request);
        }, 'fakturPenjualan.returPenjualan' => function ($query2) use ($request)
        {
            $query2->reportRincianFilter($request);
        }])->reportRincianFilter($request)->get();


        foreach ($pelanggan as $key => $data_pelanggan) {
            $sum_total                = 0;
            if ($data_pelanggan->fakturPenjualan) {
                foreach ($data_pelanggan->fakturPenjualan as $key1 => $data_faktur) {
                    $nilai_faktur               = $data_faktur->faktur_kurang_uang_muka - $data_faktur->invoice->sum('payment_amount') - $data_faktur->returPenjualan->sum('total');
                    $sum_total                  += $nilai_faktur;
                    $grand_sum_total            += $nilai_faktur;
                }
            }
            $arrayItems[] = [
                'nomor_pelanggan'       => $data_pelanggan->no_pelanggan,
                'nama_pelanggan'        => $data_pelanggan->nama,
                'sum_total'             => number_format($sum_total),
            ];
        }

        $typeFilter = [
            'single_date',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_akun_piutang_pelanggan_in_form_filer';


        $view = [
            'nama_perusahaan'               => $nama_perusahaan,
            'tanggal'                       => $tanggal->format('d F Y'),
            'items'                         => $arrayItems,
            'grand_sum_total'               => number_format($grand_sum_total),
            'typeFilter'                    => $typeFilter,
            'formFilter'                    => $formFilter
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.daftar_pelanggan')->with($view);
    }

    public function ringkasan_penerimaan_penjualan(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first();
        $tanggal_awal                       = Carbon::parse($request['start-date-rincian']);
        $tanggal_akhir                      = Carbon::parse($request['end-date-rincian']);
        $penerimaan                         = $this->penerimaanPenjualan->with('invoice.fakturPenjualan')->reportPenerimaanFilter($request)->get();

        $typeFilter = [
            'double_date_not_month',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_akun_piutang_pelanggan_in_form_filer';

        $view = [
            'nama_perusahaan'               => $nama_perusahaan,
            'items'                         => $penerimaan,
            'tanggal_awal'                  => $tanggal_awal->format('d F Y'),
            'tanggal_akhir'                 => $tanggal_akhir->format('d F Y'),
            'typeFilter'                    => $typeFilter,
            'formFilter'                    => $formFilter
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.ringkasan_penerimaan_penjualan')->with($view);
    }

    public function rincian_penerimaan_penjualan(Request $request)
    {
        $nama_perusahaan                    = $this->identitas->first();
        $tanggal_awal                       = Carbon::parse($request['start-date-rincian']);
        $tanggal_akhir                      = Carbon::parse($request['end-date-rincian']);
        $penerimaan                         = $this->penerimaanPenjualan->with('invoice.fakturPenjualan')->reportPenerimaanFilter($request)->get();

        $typeFilter = [
            'double_date_not_month',
        ];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.laporan_akun_piutang_pelanggan_in_form_filer';

        $view = [
            'nama_perusahaan'               => $nama_perusahaan,
            'items'                         => $penerimaan,
            'tanggal_awal'                  => $tanggal_awal->format('d F Y'),
            'tanggal_akhir'                 => $tanggal_akhir->format('d F Y'),
            'typeFilter'                    => $typeFilter,
            'formFilter'                    => $formFilter,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_akun_piutang_pelanggan.rincian_penerimaan_penjualan')->with($view);
    }

    // Fungsi Fungsi
    public function rincianPembayaranFaktur($pelanggan)
    {
        return $pelanggan->map(function ($item_pelanggan) 
        {
            $data_faktur = $item_pelanggan->fakturPenjualan->map(function ($item_faktur_penjualan)
            {
                $data_invoice_penerimaan = $item_faktur_penjualan->invoice->map(function ($item_invoice_penerimaan)
                {
                    return [
                        'form_no'           => $item_invoice_penerimaan->penerimaanPenjualan->form_no,
                        'cheque_no'         => $item_invoice_penerimaan->penerimaanPenjualan->cheque_no,
                        'cheque_date'       => $item_invoice_penerimaan->penerimaanPenjualan->cheque_date,
                        'payment_amount'    => $item_invoice_penerimaan->payment_amount,
                        'pajak'             => 0,
                        'diskon'            => $item_invoice_penerimaan->diskon,    
                    ];
                });
                return [
                    'no_faktur'                 => $item_faktur_penjualan->no_faktur,
                    'invoice_date'              => $item_faktur_penjualan->invoice_date,
                    'data_invoice_penerimaan'   => $data_invoice_penerimaan,
                    'sum_invoice_penerimaan'    => $data_invoice_penerimaan->sum('payment_amount'),
                    'sum_invoice_diskon'        => $data_invoice_penerimaan->sum('diskon'),
                    'total_faktur'              => $item_faktur_penjualan->total,
                    'last_owing'                => $item_faktur_penjualan->owing_after_retur,
                ];
            });

            return [
                'nama_pelanggan'        => $item_pelanggan->nama,
                'data_faktur'           => $data_faktur,
            ];
        });
    }

    public function ringkasanPembayaranFaktur($faktur)
    {
        return $faktur->map(function ($item_faktur)
        {
            $data_penerimaan = $item_faktur->invoice->map(function ($item_penerimaan) use($item_faktur)
            {
                return [
                    'invoice_date'          => Carbon::parse($item_faktur->invoice_date)->format('d F Y'),
                    'no_faktur'             => $item_faktur->no_faktur,
                    'nama_pelanggan'        => $item_faktur->pelanggan->nama,
                    'payment_amount'        => $item_faktur->invoice->sum('payment_amount'),
                    'cheque_no'             => $item_penerimaan->penerimaanPenjualan->cheque_no,
                    'cheque_date'           => Carbon::parse($item_penerimaan->penerimaanPenjualan->cheque_date)->format('d F Y'),
                ];
            });
            return $data_penerimaan;
        });
    }

    public function historiPiutangPelanggan($pelanggan)
    {
        return $pelanggan->map(function ($item_pelanggan)
        {
            $sum_faktur = 0;
            $data_faktur = [];
            foreach ($item_pelanggan->fakturPenjualan as $item_faktur) {
                $temp = [
                    'tanggal'           => Carbon::parse($item_faktur->invoice_date)->format('d F Y'),
                    'keterangan'        => $item_faktur->keterangan !== null ? $item_faktur->keterangan : 'Faktur Penjualan : '.$item_faktur->no_faktur,
                    'nilai_asing'       => $item_faktur->faktur_kurang_uang_muka,
                    'nilai_pajak'       => 0,
                    'css_child_faktur'  => $item_faktur->faktur_kurang_uang_muka < 0 ? 'red' : '#0066cc'
                ]; 
                $data_faktur[] = $temp;
                if ($item_faktur->uang_muka == 1) {
                    $temp['nilai_asing']        = -$temp['nilai_asing'];
                    $temp['css_child_faktur']   = 'red';
                    $data_faktur[] = $temp;
                }
            }
            $data_penerimaan = $item_pelanggan->penerimaanPenjualan->map(function ($item_penerimaan)
            {
                return [
                    'tanggal'               => Carbon::parse($item_penerimaan->cheque_date)->format('d F Y'),
                    'keterangan'            => $item_penerimaan->memo !== null ? $item_penerimaan->memo : 'Penerimaan Penjualan : '.$item_penerimaan->form_no,
                    'nilai_asing'           => -$item_penerimaan->cheque_amount + -$item_penerimaan->diskon,
                    'nilai_pajak'           => 0,
                    'css_child_penerimaan'  => -$item_penerimaan->cheque_amount + -$item_penerimaan->diskon < 0 ? 'red' : '#0066cc'
                ];
            });

            $data_retur = $item_pelanggan->returPenjualan->map(function ($item_retur)
            {
                return [
                    'tanggal'               => Carbon::parse($item_retur->tanggal)->format('d F Y') ,
                    'keterangan'            => $item_retur->keterangan !== null ? $item_retur->keterangan : 'Retur Penjualan : '.$item_retur->sr_no,
                    'nilai_asing'           => -$item_retur->total,
                    'nilai_pajak'           => 0,
                    'css_child_retur'       => -$item_retur->total < 0 ? 'red' : '#0066cc'
                ];
            });

            foreach ($data_faktur as $calc_faktur) {
                $sum_faktur += $calc_faktur['nilai_asing'];
            }

            // set plus minus
            $css_faktur     = '';
            $css_penerimaan = '';
            $css_retur      = '';

            if ($sum_faktur < 0) {
                $css = 'red';
            }else {
                $css_faktur = '#0066cc';
            }

            if ($data_penerimaan->sum('nilai_asing') < 0) {
                $css_penerimaan = 'red';
            }else {
                $css_penerimaan = '#0066cc';
            }

            if ($data_retur->sum('nilai_asing') < 0) {
                $css_retur = 'red';
            }else {
                $css_retur = '#0066cc';
            }

            // Hitung sum peneruman & penambahan
            $sum_penurunan_faktur   = 0;
            $sum_penambahan_faktur  = 0;

            $sum_penurunan_penerimaan   = 0;
            $sum_penambahan_penerimaan  = 0;

            $sum_penurunan_retur   = 0;
            $sum_penambahan_retur  = 0;

            foreach ($data_faktur as $faktur) {
                if ($faktur['nilai_asing'] < 0) {
                    $sum_penurunan_faktur += $faktur['nilai_asing'];
                }else {
                    $sum_penambahan_faktur += $faktur['nilai_asing'];
                }
            }

            foreach ($data_penerimaan as $penerimaan) {
                if ($penerimaan['nilai_asing'] < 0) {
                    $sum_penurunan_penerimaan += $penerimaan['nilai_asing'];
                }else {
                    $sum_penambahan_penerimaan += $penerimaan['nilai_asing'];
                }
            }

            foreach ($data_retur as $retur) {
                if ($retur['nilai_asing'] < 0) {
                    $sum_penurunan_retur += $retur['nilai_asing'];
                }else {
                    $sum_penambahan_retur += $retur['nilai_asing'];
                }
            }

            $sum_penurunan  = $sum_penurunan_faktur + $sum_penurunan_penerimaan + $sum_penurunan_retur;
            $sum_penambahan = $sum_penambahan_faktur + $sum_penambahan_penerimaan + $sum_penambahan_retur;

            $total_perubahan = $sum_penambahan + $sum_penurunan;

            return [
                'no_pelanggan'                  => $item_pelanggan->no_pelanggan,
                'nama_pelanggan'                => $item_pelanggan->nama,
                'data_faktur'                   => $data_faktur,
                'data_penerimaan'               => $data_penerimaan,
                'data_retur'                    => $data_retur,
                'sum_faktur'                    => $sum_faktur,
                'sum_penerimaan'                => $data_penerimaan->sum('nilai_asing'),
                'sum_retur'                     => $data_retur->sum('nilai_asing'),
                'css_faktur'                    => $css_faktur,
                'css_penerimaan'                => $css_penerimaan,
                'css_retur'                     => $css_retur,
                'sum_penurunan'                 => $sum_penurunan,
                'sum_penambahan'                => $sum_penambahan,
                'total_perubahan'               => $total_perubahan,
                'count_faktur'                  => count($data_faktur),
                'count_penerimaan'              => count($data_penerimaan),
                'count_retur'                   => count($data_retur),
            ];
        });
    }

    public function laporanPelanggan($pelanggan)
    {
        return $pelanggan->map(function ($item, $key)
        {
            $perhitungan = $item->sum_pelanggan;
            $arrayFaktur = [];
            foreach ($item->fakturPenjualan as $key => $data_faktur) {
                $temp = [
                    'tanggal'               => Carbon::parse($data_faktur->invoice_date)->format('d F Y'),
                    'no_sumber'             => $data_faktur->no_faktur,
                    'keterangan_sumber'     => $data_faktur->keterangan !== null ? $data_faktur->keterangan : 'Faktur Penjualan : '.$data_faktur->no_faktur,
                    'penambahan'            => $data_faktur->faktur_kurang_uang_muka,
                    'pengurangan'           => 0,
                    'saldo'                 => $perhitungan = $perhitungan + $data_faktur->faktur_kurang_uang_muka,
                ];
                $arrayFaktur[] = $temp;
                if ($data_faktur->uang_muka) {
                    $temp['pengurangan'] = $data_faktur->faktur_kurang_uang_muka;
                    $temp['penambahan']  = 0;
                    $temp['saldo']       = $perhitungan = $perhitungan - $data_faktur->faktur_kurang_uang_muka;
                    $arrayFaktur[]       = $temp;
                }
                if ($data_faktur->transaksiUangMuka->isNotEmpty()) {
                    foreach ($data_faktur->transaksiUangMuka as $key => $uang_muka) {
                        $temp['penambahan'] = $uang_muka->jumlah;
                        $temp['saldo']      = $perhitungan = $perhitungan + $uang_muka->jumlah;
                    }
                    $arrayFaktur[] = $temp;
                }
            }


            foreach ($item->penerimaanPenjualan as $key => $data_penerimaan) {
                $arrayFaktur[] = [
                    'tanggal'               => Carbon::parse($data_penerimaan->cheque_date)->format('d F Y') , 
                    'no_sumber'             => $data_penerimaan->form_no,
                    'keterangan_sumber'     => $data_penerimaan->memo !== null ? $data_penerimaan->memo : 'Penerimaan Penjualan : '.$data_penerimaan->form_no,
                    'penambahan'            => 0,
                    'pengurangan'           => $data_penerimaan->cheque_amount,
                    'saldo'                 => $perhitungan = $perhitungan - $data_penerimaan->cheque_amount,
                ];
            }

            foreach ($item->returPenjualan as $key => $data_retur) {
                $arrayFaktur[] = [
                    'tanggal'               => Carbon::parse($data_retur->tanggal)->format('d F Y') ,
                    'no_sumber'             => $data_retur->sr_no,
                    'keterangan_sumber'     => $data_retur->keterangan !== null ? $data_retur->keterangan : 'Retur Penjualan : '.$data_retur->sr_no,
                    'penambahan'            => 0,
                    'pengurangan'           => $data_retur->total,
                    'saldo'                 => $perhitungan = $perhitungan - $data_retur->total,
                ];
            }
            $item->data             = collect($arrayFaktur)->sortBy('tanggal');
            $item->sum_penambahan   = $item->data->sum('penambahan');
            $item->sum_pengurangan  = $item->data->sum('pengurangan');
            $item->perubahan_bersih = $item->sum_pelanggan + $item->data->sum('penambahan') - $item->data->sum('pengurangan'); 
            return $item;
        });
    }

    public function formatMonthYear($request)
    {
        $dateMonthArrayStart = explode('-', $request['start-date-rincian']);
        $dateMonthArrayEnd = explode('-', $request['end-date-rincian']);

        $month_start    = $dateMonthArrayStart[0];
        $year_start     = $dateMonthArrayStart[1];
        
        $month_end      = $dateMonthArrayEnd[0];
        $year_end       = $dateMonthArrayEnd[1];

         return [
            'bulan_awal'        => Carbon::createFromDate($year_start, $month_start, 1)->format('F Y'),
            'bulan_akhir'       => Carbon::createFromDate($year_end, $month_end, 1)->format('F Y'),
         ];
    }

    public function penagihanPerPelanggan($pelanggan)
    {
        return $pelanggan->map(function ($item_pelanggan)
        {
            $data_faktur = $item_pelanggan->fakturPenjualan->map(function ($item_faktur)
            {
                $penerimaan_date = !$item_faktur->invoice->isEmpty() ? Carbon::parse($item_faktur->invoice->last()->tanggal) : Carbon::parse($item_faktur->invoice_date);
                $faktur_date     = Carbon::parse($item_faktur->invoice_date);
                return [
                    'hari_bayar'    => $penerimaan_date->diffInDays($faktur_date),
                    'syarat_bayar'  => $item_faktur->syaratPembayaran->jatuh_tempo ?? 0,
                ];
            });
            $nunggak = $data_faktur->sum('hari_bayar') - $data_faktur->sum('syarat_bayar');
            return [
                'nama_pelanggan'        => $item_pelanggan->nama,
                'hari_bayar'            => $data_faktur->sum('hari_bayar'),
                'syarat_bayar'          => $data_faktur->sum('syarat_bayar'),
                'nunggak'               => $nunggak < 0 ? 0 : $nunggak,
            ];
        });
    }

    public function penagihanPerTipePelanggan($tipe_pelanggan)
    {
        return $tipe_pelanggan->map(function ($item_tipe_pelanggan)
        {
            $data_pelanggan = $item_tipe_pelanggan->pelanggan->map(function ($item_pelanggan)
            {
                $data_transaksi = $item_pelanggan->fakturPenjualan->map(function ($item_faktur)
                {
                    $penerimaan_date = Carbon::parse($item_faktur->invoice->last()->tanggal);
                    $faktur_date     = Carbon::parse($item_faktur->invoice_date);
                    return  [
                        'hari_bayar'    => $penerimaan_date->diffInDays($faktur_date),
                        'syarat_bayar'  => $item_faktur->syaratPembayaran->jatuh_tempo ?? 0,
                    ];
                });
                return [
                    'sum_hari_bayar'    => $data_transaksi->sum('hari_bayar'),
                    'sum_syarat_bayar'  => $data_transaksi->sum('syarat_bayar')
                ];
            });
            $rata_rata      = $data_pelanggan->count() !== 0 ? $data_pelanggan->sum('sum_hari_bayar') / $data_pelanggan->count() : $data_pelanggan->sum('sum_hari_bayar');
            $rata_syarat    = $data_pelanggan->count() !== 0 ? $data_pelanggan->sum('sum_syarat_bayar') / $data_pelanggan->count() : $data_pelanggan->sum('sum_syarat_bayar');
            return [
                'tipe_pelanggan'            => $item_tipe_pelanggan->nama,
                'hari_bayar'                => $rata_rata,
                'syarat_bayar'              => $rata_syarat,
                'nunggak'                   => $rata_rata - $rata_syarat 
            ];
        });
    }
}
