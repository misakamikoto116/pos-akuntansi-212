<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Http\Requests\FakturPenjualanRequest;
use App\Modules\Akuntansi\Repositories\FakturPenjualanRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
class FakturPenjualanController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Faktur Penjualan';
        $this->request = FakturPenjualanRequest::class;
        $this->requestField = ['no_faktur',
                                'pelanggan_id',
                                'pelanggan_kirim_ke_id',
                                'barang_id',
                                'alamat_tagihan',
                                'alamat_pengiriman',
                                'taxable',
                                'in_tax',
                                'diskon',
                                'ongkir',
                                'total',
                                'total_murni',
                                'keterangan',
                                'keterangan_produk',
                                'produk_id',
                                'qty_produk',
                                'satuan_produk',
                                'unit_harga_produk',
                                'diskon_produk',
                                'tax_produk',
                                'amount_produk',
                                'akun_id_dp',
                                'po_no',
                                'harga_terakhir',
                                'so_number',
                                'invoice_date',
                                'so_date',
                                'ship_date',
                                'delivery_date',
                                'fob',
                                'term_id',
                                'ship_id',
                                'catatan',
                                'akun_piutang_id',
                                'gudang_id',
                                'detail_informasi_pelanggan',
                                'faktur_uang_muka_pelanggan_id',
                                'syarat_pembayaran_dp',
                                'pajak_dp',
                                'total_uang_muka_dp',
                                'no_faktur_dp',
                                'in_tax_dp',
                                'penjelasan_dp',
                                'no_pesanan_dp',
                                'kode_pajak_id',
                                'harga_modal',
                                'selisih_diskon_amount',
                                'jumlah_diskon_faktur',
                                'barang_pengiriman_penjualan_id',
                                'pengiriman',
                                'lanjutkan',                      
                                // Untuk Keperluan Cetak langsung
                                'sn',                                
                                'subTotal',      
                                // new
                                'akun_ongkir_id',
                                'no_fp_std',
				            	'no_fp_std2',
				            	'no_fp_std_date',
                                'tax_cetak0',                          
                                'tax_cetak1'                          
                            ];
    }

    public function formData()
    {
        return [
            'kategori' => $this->model->listKategoriProduk(),
            'gudang' => $this->model->listGudang(),
            'akun' => $this->model->listAkun(),
            'pelanggan' => $this->model->listPelanggan(),
            // 'produk' => $this->model->listProduk(),
            'termin' => $this->model->listTermin(),
            'pengiriman' => $this->model->listJasaPengiriman(),
            'pajak' => $this->model->listPajak(),
            'idPrediction'   => $this->model->idPrediction(),
            'getAkunPiutang'   => $this->model->getAkunPiutang(),
            'filter' => ['tanggal_dari_sampai', 'pelanggan_select','no_faktur','no_pelanggan'],
            // 'tipePelanggan' => $this->model->listTipePelanggan(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function redirectSuccess(Request $request, $result = null)
    {
        if(!empty($result['continue_stat'])){
            if($result['continue_stat'] == 1){
                return redirect('/akuntansi/penerimaan-penjualan/create?pelanggan_id='.$result->pelanggan_id)
                ->withMessage('Berhasil Menambah/Memperbarui data');
            }else{
                return redirect('/akuntansi/faktur-penjualan/create')->withMessage('Berhasil Menambah/Memperbarui data');;
            }
        }else{
            return redirect('/akuntansi/faktur-penjualan')->withMessage('Berhasil Menambah/Memperbarui data');;
        }
    }

}
