<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LaporanPenjualanController extends Controller
{
    use TraitLaporan;

    // Laporan Penjualan - Rincian Penjualan
    public function penjualan_per_pelanggan(Request $request)
    {
        !empty($request['startDate']) ? $request['start-date'] = $request['startDate'] : null ;
        !empty($request['endDate'])   ? $request['end-date']   = $request['endDate']   : null ;
        
        $title          =   'Laporan Penjualan per Pelanggan';
        $viewBlade      =   'penjualan_per_pelanggan';
        $titlePdf       =   snake_case($title.$this->generateSubTitleDMY($request)).'.pdf';
        $fungsiUtama    =   __FUNCTION__;

        $dataPelanggan  =   $this->informasiPelanggan->pluck('id');
        $query          =   $this->fakturPenjualan->with('pelanggan')
                            ->addSelect(DB::Raw('sum(faktur_penjualan.total) as day_total'))
                            ->addSelect(DB::Raw('DATE(faktur_penjualan.created_at) as day'))
                            ->addSelect(DB::Raw('pelanggan_id'))
                            ->whereIn('pelanggan_id', $dataPelanggan)
                            ->where('uang_muka',0)
                            ->dateFilter($request)
                            ->groupBy('pelanggan_id')
                            // ->groupBy(DB::Raw('DATE(faktur_penjualan.created_at)'))
                            ->orderBy(DB::Raw('DATE(faktur_penjualan.created_at)'),'desc');
        
        return $this->switchAjaxRequest($query, $request, $title, $viewBlade, $titlePdf, $fungsiUtama, $posCheck = null);
    }

    public function rincian_penjualan_per_pelanggan(Request $request){
        $dataPelanggan          =       $this->informasiPelanggan->pluck('id');
        $totalPenjualan         =       $this->fakturPenjualan->with('pelanggan')
                                        ->select(array(
                                            DB::Raw('sum(faktur_penjualan.total) as day_total'),
                                            DB::Raw('DATE(faktur_penjualan.created_at) day'),
                                            DB::Raw('faktur_penjualan.no_faktur as nofaktur'),
                                            DB::Raw('DATE(faktur_penjualan.invoice_date) tgl_faktur'),
                                            DB::Raw('faktur_penjualan.keterangan as keterangan'),
                                            'pelanggan_id'))
                                        ->dateFilter($request)->whereIn('pelanggan_id', $dataPelanggan)->where('uang_muka',0)->groupBy('id')
                                        ->groupBy('pelanggan_id')->groupBy('day')->orderBy('day','desc');

        $array = [];
        foreach($totalPenjualan->get() as $sales){
            $array[] = [
                'nama'               =>     $sales->pelanggan->nama,
                'no_faktur'          =>     $sales->nofaktur,
                'tanggal_faktur'     =>     $this->dateCarbon($sales->tgl_faktur),
                'keterangan'         =>     $sales->keterangan,
                'total'              =>     $sales->day_total,
                'tanggal'            =>     $sales->day
            ];
        }
        $newCollection = collect($array)->groupBy('nama');

        $view = [
            'title'           =>      'Laporan Rincian Penjualan per Pelanggan',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.rincian_penjualan_per_pelanggan')->with($view);
    }

    public function rincian_penjualan_per_tipe_pelanggan(Request $request) 
    {
        $tipePelanggan          =       $request->tipe_pelanggan_id;
        $totalPenjualan         =       $this->fakturPenjualan->with('pelanggan')
                                        ->select(array(
                                            DB::Raw('sum(faktur_penjualan.total) as day_total'),
                                            DB::Raw('DATE(faktur_penjualan.created_at) day'),
                                            DB::Raw('faktur_penjualan.no_faktur as nofaktur'),
                                            DB::Raw('DATE(faktur_penjualan.invoice_date) tgl_faktur'),
                                            DB::Raw('faktur_penjualan.keterangan as keterangan'),
                                            'pelanggan_id'))
                                        ->dateFilter($request)->whereHas('pelanggan', function ($query) use ($tipePelanggan)
                                        {
                                            $query->where('tipe_pelanggan_id', $tipePelanggan);
                                        })
                                        ->where('uang_muka',0)->groupBy('id')
                                        ->groupBy('pelanggan_id')->groupBy('day')->orderBy('day','desc');

        $array = [];
        foreach($totalPenjualan->get() as $sales){
            $array[] = [
                'nama'               =>     $sales->pelanggan->nama,
                'no_faktur'          =>     $sales->nofaktur,
                'tanggal_faktur'     =>     $this->dateCarbon($sales->tgl_faktur),
                'keterangan'         =>     $sales->keterangan,
                'total'              =>     $sales->day_total,
                'tanggal'            =>     $sales->day
            ];
        }
        $newCollection = collect($array)->groupBy('nama');

        $view = [
            'title'           =>      'Laporan Rincian Penjualan per Tipe Pelanggan '.$this->tipePelanggan->findOrFail($request->tipe_pelanggan_id)->nama,
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.rincian_penjualan_per_pelanggan')->with($view);
    }

    public function penjualan_perbarang_omset(Request $request)
    {
        $fakturPenjualan    =   $this->fakturPenjualan->dateFilter($request)->pluck('id');
        $totalOmset         =   $this->barangFakturPenjualan->with('produk')
                                ->select(array(
                                    DB::Raw('sum(barang_faktur_penjualan.jumlah * barang_faktur_penjualan.unit_price) as day_total'),
                                    DB::Raw('DATE(barang_faktur_penjualan.created_at) day'),
                                    'produk_id',
                                    'faktur_penjualan_id',
                                ))
                                ->whereIn('faktur_penjualan_id', $fakturPenjualan)
                                ->groupBy('produk_id')->orderBy('day','desc');

        $array = [];
        foreach($totalOmset->get() as $sales){
            $array[] = [
                'nama'              =>  $sales->produk->keterangan,
                'total'             =>  $sales->day_total,
                'tanggal'           =>  $sales->day
            ];
        }
        $newCollection = collect($array)->groupBy('nama');

        $view = [
            'title'           =>      'Laporan Penjualan perbarang (Omset)',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.penjualan_perbarang_omset')->with($view);
    }

    public function penjualan_perbarang_kuantitas(Request $request)
    {
        $fakturPenjualan          =     $this->fakturPenjualan->dateFilter($request)->pluck('id');
        $KuantitasBarang          =     $this->barangFakturPenjualan->with('produk')
                                        ->select(array(
                                                DB::Raw('sum(barang_faktur_penjualan.jumlah) as jmlqty'),
                                                DB::Raw('barang_faktur_penjualan.item_unit as satuan'),
                                                DB::Raw('DATE(barang_faktur_penjualan.created_at) day'),
                                                'produk_id',
                                                'faktur_penjualan_id',
                                        ))
                                        ->whereIn('faktur_penjualan_id', $fakturPenjualan)
                                        ->groupBy('produk_id')->orderBy('day','desc');

        $array = [];
        foreach($KuantitasBarang->get() as $sales){
            $array[] = [
                'nama'          =>  $sales->produk->keterangan,
                'total'         =>  $sales->jmlqty,
                'unit'          =>  $sales->satuan,
                'tanggal'       =>  $sales->day,
            ];
        }
        $newCollection  =   collect($array)->groupBy('nama');

        $view = [
            'title'           =>      'Laporan Penjualan perbarang (Kuantitas)',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.penjualan_perbarang_kuantitas')->with($view);
    }

    public function penjualan_perbarang(Request $request)
    {
        !empty($request['startDate']) ? $request['start-date'] = $request['startDate'] : null ;
        !empty($request['endDate'])   ? $request['end-date']   = $request['endDate']   : null ;
        $title                  =   'Laporan Penjualan Perbarang';
        $viewBlade              =   'penjualan_perbarang';
        $titlePdf               =   snake_case($title.$this->generateSubTitleDMY($request)).'.pdf';
        $fungsiUtama            =   __FUNCTION__;
        $dataFaktur             =   $this->fakturPenjualan->where('uang_muka',0)
                                    ->dateFilter($request)
                                    ->pluck('id');
        $barangFaktur           =   $this->barangFakturPenjualan
                                    ->addSelect(DB::raw('barang_faktur_penjualan.item_deskripsi     as namaBarang'))
                                    ->addSelect(DB::raw('sum(barang_faktur_penjualan.jumlah)        as sumTotal'))
                                    ->addSelect(DB::raw('barang_faktur_penjualan.item_unit          as itemUnit'))
                                    ->addSelect(DB::raw('sum(barang_faktur_penjualan.harga_final * barang_faktur_penjualan.jumlah)   as sumHargaFinal'))
                                    ->whereIn('faktur_penjualan_id', $dataFaktur)
                                    ->groupBy('produk_id')
                                    ->orderBy('sumHargaFinal', 'desc');
        return $this->switchAjaxRequest($barangFaktur, $request, $title, $viewBlade, $titlePdf, $fungsiUtama, $posCheck = null);
    }

    public function penjualan_perbarang_per_kasir(Request $request)
    {
        !empty($request['startDate']) ? $request['start-date'] = $request['startDate'] : null ;
        !empty($request['endDate'])   ? $request['end-date']   = $request['endDate']   : null ;
        $posCheck               =   2;
        $title                  =   'Laporan Penjualan Perbarang Kasir '.$this->user->findOrFail($request->kasir_id)->name;
        $viewBlade              =   'penjualan_perbarang_perkasir';
        $titlePdf               =   snake_case($title.$this->generateSubTitleDMY($request)).'.pdf';
        $fungsiUtama            =   'penjualan_perbarang';
        $dataFaktur             =   $this->fakturPenjualan->where([
                                        'uang_muka'     => 0,
                                        'status_modul'  => 1,
                                        'created_by'    => $request->kasir_id,
                                    ])->dateFilter($request)
                                    ->pluck('id');
        $barangFaktur           =   $this->barangFakturPenjualan
                                    ->addSelect(DB::raw('barang_faktur_penjualan.item_deskripsi     as namaBarang'))
                                    ->addSelect(DB::raw('sum(barang_faktur_penjualan.jumlah)        as sumTotal'))
                                    ->addSelect(DB::raw('barang_faktur_penjualan.item_unit          as itemUnit'))
                                    ->addSelect(DB::raw('sum(barang_faktur_penjualan.harga_final * barang_faktur_penjualan.jumlah)   as sumHargaFinal'))
                                    ->whereIn('faktur_penjualan_id', $dataFaktur)
                                    ->groupBy('produk_id')
                                    ->orderBy('sumHargaFinal', 'desc');

        return $this->switchAjaxRequest($barangFaktur, $request, $title, $viewBlade, $titlePdf, $fungsiUtama, $posCheck);
    }

    public function penjualan_pertransaksi_per_kasir(Request $request)
    {
        !empty($request['startDate']) ? $request['start-date'] = $request['startDate'] : null ;
        !empty($request['endDate'])   ? $request['end-date']   = $request['endDate']   : null ;
        $kasir                  = $this->user->findOrFail($request->kasir_id);  
        $title                  =   'Laporan Penjualan Perbarang Kasir '.$kasir->name;

        $faktur                 = $this->fakturPenjualan
                                  ->with([
                                    'barang.produk',
                                  ])
                                  ->where([
                                    'uang_muka'         => 0,
                                    'status_modul'      => 1,
                                    'created_by'        => $request->kasir_id,
                                  ])
                                  ->dateFilter($request)
                                  ->orderBy('invoice_date')
                                  ->get();

        $data_faktur = $faktur->map(function ($item_faktur)
        {
            $barang = $item_faktur->barang->map(function ($item_barang)
            {
                return [
                    'barcode_barang'        => $item_barang->produk->no_barang,
                    'nama_barang'           => $item_barang->produk->keterangan,
                    'qty'                   => $item_barang->jumlah,
                    'harga'                 => number_format($item_barang->unit_price),
                    'total'                 => number_format($item_barang->unit_price * $item_barang->jumlah),
                ]; 
            });

            return [
                'tanggal'           => Carbon::parse($item_faktur->invoice_date)->format('d F Y H:i:s'),
                'no_faktur'         => $item_faktur->no_faktur,
                'status'            => $item_faktur->status_penjualan_pos_no_label,
                'total'             => number_format($item_faktur->sum_harga_total),
                'barang'            => $barang,
            ];
        });

        $view = [
            'items'             => $data_faktur,
            'title'             => $title,
            'perusahaan'        => $this->identitasPerushaan(),
            'subTitle'          => 'Laporan Penjualan Pertransaksi Perkasir',
            'nama'              => $kasir->name,
            'tanggal_awal'      => Carbon::parse($request['startDate'])->format('d F Y'),
            'tanggal_akhir'     => Carbon::parse($request['endDate'])->format('d F Y'),
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.penjualan_pertransaksi_perkasir')->with($view);

    }

    public function penjualan_perbarang_perpelanggan(Request $request)
    {
        !empty($request['startDate']) ? $request['start-date'] = $request['startDate'] : null ;
        !empty($request['endDate'])   ? $request['end-date']   = $request['endDate']   : null ;
        $posCheck               = 2;
        $title                  =   'Laporan Rincian Penjualan Perbarang Pelanggan ';
        $pelanggan              = $this->informasiPelanggan->findOrFail($request->pelanggan_id);

        $faktur                 = $this->fakturPenjualan->with(['pelanggan','barang.produk','invoice.penerimaanPenjualan'])
                                                        ->where([
                                                            'uang_muka'     => 0,
                                                            'status_modul'  => 1,
                                                            'pelanggan_id'  => $request->pelanggan_id,
                                                        ])->dateFilter($request)
                                                        ->get();


        $data_faktur            = $faktur->map(function ($item)
        {
            $barang = $item->barang->map(function ($item_barang)
            {
                return [
                    'barcode_barang'        => $item_barang->produk->no_barang,
                    'nama_barang'           => $item_barang->produk->keterangan,
                    'qty'                   => $item_barang->jumlah,
                    'harga'                 => number_format($item_barang->unit_price),
                    'total'                 => number_format($item_barang->unit_price * $item_barang->jumlah),
                ];
            });

            return [
                'no_faktur'         => $item->no_faktur,
                'tanggal_faktur'    => Carbon::parse($item->invoice_date)->format('d F Y'),
                'total'             => number_format($item->sum_harga_total),
                'status'            => $item->status_penjualan_pos_no_label,
                'barang'            => $barang,
            ];
        });

        $view = [
            'items'             => $data_faktur,
            'title'             => $title,
            'perusahaan'        => $this->identitasPerushaan(),
            'subTitle'          => 'Laporan Penjualan Perbarang Perpelanggan',
            'nama'              => $pelanggan->nama,
            'no_anggota'        => $pelanggan->no_pelanggan,
            'tanggal_awal'      => Carbon::parse($request['startDate'])->format('d F Y'),
            'tanggal_akhir'     => Carbon::parse($request['endDate'])->format('d F Y'),
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.penjualan_perbarang_perpelanggan')->with($view);
    }

    public function penjualan_perbarang_pos(Request $request)
    {
        !empty($request['startDate']) ? $request['start-date'] = $request['startDate'] : null ;
        !empty($request['endDate'])   ? $request['end-date']   = $request['endDate']   : null ;
        $posCheck               =   1;
        $title                  =   'Laporan Penjualan Perbarang POS';
        $viewBlade              =   'penjualan_perbarang_perkasir';
        $titlePdf               =   snake_case($title.$this->generateSubTitleDMY($request)).'.pdf';
        $fungsiUtama            =   'penjualan_perbarang';
        $dataFaktur             =   $this->fakturPenjualan->where([
                                        'uang_muka'     => 0,
                                        'status_modul'  => 1
                                    ])->dateFilter($request)
                                    ->pluck('id');
        $barangFaktur           =   $this->barangFakturPenjualan
                                    ->addSelect(DB::raw('barang_faktur_penjualan.item_deskripsi     as namaBarang'))
                                    ->addSelect(DB::raw('sum(barang_faktur_penjualan.jumlah)        as sumTotal'))
                                    ->addSelect(DB::raw('barang_faktur_penjualan.item_unit          as itemUnit'))
                                    ->addSelect(DB::raw('sum(barang_faktur_penjualan.harga_final * barang_faktur_penjualan.jumlah)   as sumHargaFinal'))
                                    ->whereIn('faktur_penjualan_id', $dataFaktur)
                                    ->groupBy('produk_id')
                                    ->orderBy('sumHargaFinal', 'desc');
        return $this->switchAjaxRequest($barangFaktur, $request, $title, $viewBlade, $titlePdf, $fungsiUtama, $posCheck);
    }

    public function rincian_penjualan_perbarang(Request $request){
        $dataFaktur             =   $this->fakturPenjualan->where('uang_muka',0)
                                    ->dateFilter($request)->pluck('id');
        $barangFaktur           =   $this->barangFakturPenjualan->with('fakturPenjualan')
                                    ->select(array(
                                        DB::Raw('barang_faktur_penjualan.item_deskripsi'),
                                        DB::Raw('sum(barang_faktur_penjualan.jumlah) as totals'),
                                        DB::Raw('barang_faktur_penjualan.item_unit'),
                                        DB::Raw('barang_faktur_penjualan.harga_final'),
                                        'faktur_penjualan_id',
                                    ))
                                    ->whereIn('faktur_penjualan_id', $dataFaktur)
                                    ->groupBy('id')->orderBy('created_at','desc');
       
        $array = [];
        foreach($barangFaktur->get() as $sales){
            $total = $sales->harga_final * $sales->totals;
            $array[] = [
                'nama_barang'       =>      $sales->item_deskripsi,
                'no_faktur'         =>      $sales->fakturPenjualan->no_faktur,
                'tgl_faktur'        =>      $this->dateCarbon($sales->fakturPenjualan->invoice_date),
                'keterangan'        =>      $sales->fakturPenjualan->catatan,
                'kuantitas'         =>      $sales->totals,
                'satuan'            =>      $sales->item_unit,
                'total'             =>      $total,
            ];
        }
        $newCollection = collect($array)->groupBy('nama_barang');

        $view = [
            'title'         =>  'Laporan Rincian Penjualan per Barang',
            'subTitle'      =>  $this->generateSubTitleDMY($request),
            'perusahaan'    =>  $this->identitasPerushaan(),
            'item'          =>  $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.rincian_penjualan_perbarang')->with($view);
    }

    public function penjualan_pelanggan_perbarang(Request $request)
    {
        $saldoAwalBarang        =   $this->saldoAwalBarang->pluck('transaksi_id');
        $transaksi              =   $this->transaksi->with('item.pelanggan','produkDetail.produk')
                                    ->where('item_type', 'like', '%penjualan%')
                                    ->whereIn('id', $saldoAwalBarang)->DateFilter($request)
                                    ->get();
        $array = [];
        
        $arrayTotal = [];
        foreach($transaksi as $dataTransaksi){
            foreach($dataTransaksi->produkDetail as $dataTransaksiProduk){
                $array [] = [
                    'nama_pelanggan'      =>  $dataTransaksi->item->pelanggan->nama,
                    'nama_produk'         =>  $dataTransaksiProduk->produk->keterangan,
                    'harga_terakhir'      =>  $dataTransaksiProduk->harga_terakhir,
                ];
            }
        }
        $newCollection = collect($array)->groupBy('nama_pelanggan');
        $view = [
            'title'           =>      'Laporan Penjualan Pelanggan Per Barang',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.penjualan_pelanggan_perbarang')->with($view);
    }

    public function penjualan_perbarang_pelanggan(Request $request)
    {
        $saldoAwalBarang        =   $this->saldoAwalBarang->pluck('transaksi_id');
        $transaksi              =   $this->transaksi->with('item.pelanggan','produkDetail.produk')
                                    ->where('item_type', 'like', '%penjualan%')
                                    ->whereIn('id', $saldoAwalBarang)->DateFilter($request)
                                    ->get();
        $array = [];
        
        $arrayTotal = [];
        foreach($transaksi as $dataTransaksi){
            foreach($dataTransaksi->produkDetail as $dataTransaksiProduk){
                $array [] = [
                    'nama_pelanggan'      =>  $dataTransaksi->item->pelanggan->nama,
                    'nama_produk'         =>  $dataTransaksiProduk->produk->keterangan,
                    'harga_terakhir'      =>  $dataTransaksiProduk->harga_terakhir,
                ];
            }
        }
        $newCollection = collect($array)->groupBy('nama_produk');
        $view = [
            'title'           =>      'Laporan Penjualan Barang Per Pelanggan',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.penjualan_perbarang_pelanggan')->with($view);
    }

    public function retur_penjualan_perbarang(Request $request)
    {
        // $fakturPenjualan            =       $this->fakturPenjualan->pluck('id');
        $returPenjualan             =       $this->returPenjualan->dateFilter($request)->pluck('id');
        $produk                     =       $this->produk->pluck('id');
        $query                      =       $this->barangReturPenjualan->with('returPenjualan')->with('produk')->with('rPenjualan')
                                            ->select(
                                                array(
                                                    DB::Raw('DATE(barang_retur_penjualan.created_at) day'),
                                                    DB::Raw('barang_retur_penjualan.jumlah qty'),
                                                    DB::Raw('barang_retur_penjualan.satuan unit'),
                                                    DB::Raw('sum(barang_retur_penjualan.jumlah * barang_retur_penjualan.harga) as totals'),
                                                    'retur_penjualan_id',
                                                    'produk_id',
                                                ))
                                            // ->whereIn('retur_penjualan_id', $fakturPenjualan)
                                            ->whereIn('retur_penjualan_id', $returPenjualan)->groupBy('id')
                                            ->whereIn('produk_id', $produk)
                                            ->orderBy('day','desc');

        $array = [];
        foreach($query->get() as $sales){
            if ($sales->returPenjualan) {
                $noFaktur  = $sales->returPenjualan->no_faktur;
                $tglFaktur = date('d-m-Y', strtotime($sales->returPenjualan->invoice_date));
            }else{
                $noFaktur = null ;
                $tglFaktur = null;
            }
            $array [] = [
                'produk'            =>    $sales->produk->keterangan,
                'nofaktur'          =>    $noFaktur,
                'tglfaktur'         =>    $tglFaktur,
                'keterangan'        =>    $sales->rPenjualan->keterangan,
                'kuantitas'         =>    $sales->qty,
                'satuan'            =>    $sales->unit,
                'total'             =>    $sales->totals,
            ];
        }
        $newCollection  =   collect($array)->groupBy('produk');

        $view = [
            'title'           =>      'Laporan Retur Penjualan perbarang',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.retur_penjualan_perbarang')->with($view);
    }

    public function retur_penjualan_per_pelanggan(Request $request)
    {
        $dataPelanggan          =       $this->informasiPelanggan->pluck('id');
        $dataFaktur             =       $this->fakturPenjualan->pluck('id');
        $dataRetur              =       $this->returPenjualan->with('fakturPenjualan')->with('pelanggan')
                                        ->select(array(
                                            DB::Raw('sum(retur_penjualan.total) as jumlah'),
                                            DB::Raw('retur_penjualan.keterangan as deskripsi'),
                                            DB::Raw('DATE(retur_penjualan.created_at) day'),
                                            'faktur_penjualan_id',
                                            'pelanggan_id'
                                        ))->dateFilter($request)
                                        ->whereIn('faktur_penjualan_id',$dataFaktur)->whereIn('pelanggan_id',$dataPelanggan)
                                        ->groupBy('pelanggan_id')->groupBy('faktur_penjualan_id')->orderBy('pelanggan_id');

        $array = [];
        foreach($dataRetur->get() as $sales){
            $array [] = [
                'pelanggan'      =>     $sales->pelanggan->nama,
                'nofaktur'       =>     $sales->fakturPenjualan->no_faktur,
                'total'          =>     $sales->jumlah,
                'keterangan'     =>     $sales->keterangan,
                'tanggal'        =>     $sales->day,
            ];
        }
        $newCollection = collect($array)->groupBy('pelanggan');
        
        $view = [
            'title'           =>      'Laporan Retur Penjualan per Pelanggan',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.retur_penjualan_per_pelanggan')->with($view);
    }

    public function penjualan_tahunan(Request $request)
    {
        $request['start-date'] === null ? $start_date = date("Y-m-01") : $start_date = $request['start-date'] ;
        $request['end-date']   === null ? $end_date   = date("Y-m-t") : $end_date   = $request['end-date'] ;
        
        $totalPenjualan     =   $this->transaksi->where('status', 1)
        ->where('item_type', get_class($this->fakturPenjualan))
        ->where('tanggal', '>=', $start_date)
        ->where('tanggal', '<=', $end_date)->get()
        ->groupBy(function($date){
            $dates = substr($date['tanggal'],0,7);
            return $this->yearMonthCarbon($dates);
        })
        ->map(function($itemTotalPenjualan)
        {
            $sumIT = 0;
            $items = $itemTotalPenjualan->map(function($itemTransaksi) use(&$sumIT){
                $sumIT += $itemTransaksi->nominal;
                return $sumIT;
            });
            return $items->last();
        });
        
        $view = [
            'title'           =>      'Laporan Penjualan Tahunan',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $totalPenjualan,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.penjualan_tahunan')->with($view);
    }

    public function grafik_prestasi_penjualan_barang(Request $request)
    {
        $request['start-date'] === null ? $start_date = date("Y-m-01") : $start_date = $request['start-date'] ;
        $request['end-date']   === null ? $end_date   = date("Y-m-t") : $end_date   = $request['end-date'] ;

        $validasi = $request->validate([
			'produk-item'  => 'required|array|max:5',
        ]);
        $totalPenjualan     =   $this->saldoAwalBarang->with('produk')->where('status', 1)
        ->where('item_type', get_class($this->fakturPenjualan))->whereIn('produk_id', $request['produk-item'])
        ->where('tanggal', '>=', $start_date)->where('tanggal', '<=', $end_date)->get()
        ->groupBy(function($date){
            $dates = substr($date['tanggal'],0,7);
            return $this->yearMonthCarbon($dates);
        })
        ->map(function($itemTotalPenjualan)
        {
            $items = $itemTotalPenjualan->groupBy('produk_id')->map(function($item){
                $sumI = 0;
                $sum = $item->map(function($sumItem) use(&$sumI){
                    $sumI += $sumItem->harga_terakhir;
                    return $sumI;
                });
                $name = $item->map(function($produkName){
                    return $produkName->produk->keterangan;
                });

                return [
                    $sum->last()    =>    $sum->last(),
                    $name->last()   =>    $name->last(),
                ];
            });
            return array_flatten($items);
        });
        
        $view = [
            'title'           =>      'Laporan Grafik Prestasi Penjualan Barang',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $totalPenjualan,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.grafik_prestasi_penjualan_barang')->with($view);
    }

    public function grafik_prestasi_penjualan_pelanggan(Request $request)
    {
        $request['start-date'] === null ? $start_date = date("Y-m-01") : $start_date = $request['start-date'] ;
        $request['end-date']   === null ? $end_date   = date("Y-m-t") : $end_date   = $request['end-date'] ;

        $validasi = $request->validate([
			'pelanggan-item'  => 'required|array|max:5',
        ]);

        $totalPenjualan = $this->fakturPenjualan->with('pelanggan')
        ->whereIn('pelanggan_id', $request['pelanggan-item'])->where('uang_muka',0)
        ->where('invoice_date', '>=', $start_date)
        ->where('invoice_date', '<=', $end_date)->get()
        ->groupBy(function($date){
            $dates = substr($date['invoice_date'],0,7);
            return $this->yearMonthCarbon($dates);
        })->map(function($itemTotalPenjualan) use(&$request){
            $datatotalPenjualan = $itemTotalPenjualan->groupBy('pelanggan_id')->map(function($a){
                $pelanggan = $a->map(function($d){
                    return $d->pelanggan->nama;
                });
                $sumP      = $a->sum('total');
                return [
                    'Total'     => $sumP,
                    'Pelanggan' => $pelanggan->last(),
                ];
            });
            return  array_flatten($datatotalPenjualan);
        });

        $view = [
            'title'           =>      'Laporan Grafik Prestasi Penjualan Pelanggan',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $totalPenjualan,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.grafik_prestasi_penjualan_pelanggan')->with($view);
    }

    // Laporan Penjualan - Rincian Penawaran
    public function penawaran_penjualan_belum_terproses(Request $request)
    {
        $pelanggan              =       $this->informasiPelanggan->pluck('id');
        $penawaranPenjualan     =       $this->penawaranPenjualan->with('pelanggan')
                                        ->select(array(
                                            DB::Raw('penawaran_penjualan.no_penawaran as nopem'),
                                            DB::Raw('penawaran_penjualan.keterangan as keterangans'),
                                            DB::Raw('DATE(penawaran_penjualan.tanggal) tgl_tawaran'),
                                            DB::Raw('sum(penawaran_penjualan.total) as jumlah'),
                                            DB::Raw('penawaran_penjualan.status as stat'),
                                            DB::Raw('penawaran_penjualan.created_at as day'),
                                            'pelanggan_id',
                                        ))->dateFilter($request)
                                        ->whereIn('pelanggan_id', $pelanggan)
                                        ->where('status', '0')->groupBy('pelanggan_id')->groupBy('no_penawaran')->orderBy('day','asc');
        
        $array = [];
        foreach($penawaranPenjualan->get() as $sales){
            $array [] = [
                'pelanggan'         =>      $sales->pelanggan->nama,
                'no_penawaran'      =>      $sales->nopem,
                'tgl_penawaran'     =>      $this->dateCarbon($sales->tgl_tawaran),
                'keterangan'        =>      $sales->keterangans,
                'total'             =>      $sales->jumlah,
                'status'            =>      $sales->stat,
            ];
        }
        $newCollection = collect($array)->groupBy('pelanggan');

        $view = [
            'title'           =>      'Laporan Penawaran Penjualan per Pelanggan belum Terproses',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.penawaran_penjualan_belum_terproses')->with($view);
    }

    public function penawaran_penjualan_per_barang_belum_terproses(Request $request)
    {
        $produkPenawaran        =       $this->barangPenawaranPenjualan->pluck('penawaran_penjualan_id');
        $penawaranPenjualan     =       $this->penawaranPenjualan->with('barang.produk')
                                        ->select(array(
                                            DB::Raw('penawaran_penjualan.no_penawaran as nopem'),
                                            DB::Raw('DATE(penawaran_penjualan.tanggal) tgl_tawaran'),
                                            DB::Raw('penawaran_penjualan.status as stat'),
                                            DB::Raw('penawaran_penjualan.created_at as day'),
                                            'id',
                                        ))->dateFilter($request)
                                        ->whereIn('id', $produkPenawaran)
                                        ->where('status', '0')->groupBy('id')->groupBy('no_penawaran')->orderBy('day','asc');

        $array = [];
        foreach($penawaranPenjualan->get() as $sale => $sales){
            foreach ($sales->barang as $dataBarang) {
                $array [] = [
                    'nopenawaran'       => $sales->nopem,
                    'tanggal'           => $this->dateCarbon($sales->tgl_tawaran),
                    'status'            => $sales->stat,
                    'produk'            => $dataBarang->item_deskripsi,
                    'qty'               => $dataBarang->jumlah,
                    'satuan'            => $dataBarang->satuan,
                    'qtyterima'         => 0,
                ];
            }
        }
        $newCollection = collect($array)->groupBy('produk');

        $view = [
            'title'           =>      'Laporan Penawaran Penjualan per Barang belum Terproses',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.penawaran_penjualan_per_barang_belum_terproses')->with($view);
    }

    public function pesanan_penjualan_per_pelanggan_belum_terprosesi(Request $request)
    {
        $pelanggan              =       $this->informasiPelanggan->pluck('id');
        $pesananPenjualan       =       $this->pesananPenjualan->with('pelanggan')
                                        ->select(array(
                                            DB::Raw('pesanan_penjualan.so_number as sonum'),
                                            DB::Raw('DATE(pesanan_penjualan.so_date) sodate'),
                                            DB::Raw('pesanan_penjualan.ship_date as sdate'),
                                            DB::Raw('pesanan_penjualan.status as stat'),
                                            DB::Raw('pesanan_penjualan.created_at as day'),
                                            'pelanggan_id',
                                        ))->dateFilter($request)
                                        ->whereIn('pelanggan_id', $pelanggan)
                                        ->where('status', 0)->orderBy('day', 'asc');
        $array = [];
        foreach($pesananPenjualan->get() as $sales){    
                $array [] = [
                    'pelanggan'      =>  $sales->pelanggan->nama,
                    'sonum'          =>  $sales->sonum,
                    'tanggal'        =>  $this->dateCarbon($sales->sodate),
                    'tanggalkirim'   =>  $sales->sdate,
                    'status'         =>  $sales->stat,
                    'total'          =>  $sales->nilai_faktur,
                ];
        }
        $newCollection  =   collect($array)->groupBy('pelanggan');
        $view = [
            'title'           =>      'Laporan Pesanan Penjualan per Pelanggan belum Terproses',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.pesanan_penjualan_per_pelanggan_belum_terprosesi')->with($view);
    }

    public function pesanan_penjualan_per_barang_belum_terprosesi(Request $request)
    {
        $barang                 =       $this->barangPesananPenjualan->groupBy('pesanan_penjualan_id')->pluck('pesanan_penjualan_id');
        $pesananPenjualan       =       $this->pesananPenjualan->with('barang')
                                        ->select(array(
                                            DB::Raw('pesanan_penjualan.so_number as sonum'),
                                            DB::Raw('DATE(pesanan_penjualan.so_date) sodate'),
                                            DB::Raw('pesanan_penjualan.status as stat'),
                                            DB::Raw('pesanan_penjualan.created_at as day'),
                                            'pelanggan_id',
                                            'id',
                                        ))->dateFilter($request)
                                        ->whereIn('id', $barang)
                                        ->where('status', 0)->orderBy('day', 'asc');

        $array = [];
        foreach($pesananPenjualan->get() as $sale => $sales){
            foreach ($sales->barang as $dataBarang) {
                $array [] = [
                    'sonum'          =>  $sales->sonum,
                    'tanggal'        =>  $this->dateCarbon($sales->sodate),
                    'status'         =>  $sales->stat,
                    'produk'         =>  $dataBarang->item_deskripsi,
                    'qty'            =>  $dataBarang->jumlah,
                    'satuan'         =>  $dataBarang->satuan,
                    'qtyterima'      =>  0,
                ];
            }
        }
        $newCollection  =   collect($array)->groupBy('produk');

        $view = [
            'title'           =>      'Laporan Pesanan Penjualan per Barang belum Terproses',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.pesanan_penjualan_per_barang_belum_terprosesi')->with($view);
    }

    public function uang_muka_pesanan_penjualan(Request $request)
    {
        $fakturUangMuka             =       $this->fakturPenjualan->with('barang.barangPengirimanPenjualan.barangPesananPenjualan.pesananPenjualan')
                                            ->select(array(
                                                DB::Raw('faktur_penjualan.no_faktur as nofak'),
                                                DB::Raw('DATE(faktur_penjualan.invoice_date) tgl_faktur'),
                                                DB::Raw('sum(faktur_penjualan.total) as totals'),
                                                DB::Raw('faktur_penjualan.catatan as note'),
                                                'uang_muka',
                                                'id'
                                            ))->dateFilter($request)->where('uang_muka', 1)
                                            // ->groupBy('no_faktur')
                                            ->orderBy('created_at', 'asc');
        
        $array = [];
        foreach($fakturUangMuka->get() as $sale => $sales){
            foreach($sales->barang as $data){
                // if($data->barangPengirimanPenjualan !== null){
                    $array [] = [
                        // 'noso'  =>  $data->barangPengirimanPenjualan->barangPesananPenjualan->pesananPenjualan->so_number,
                        // 'tglso'   =>  $data->barangPengirimanPenjualan->barangPesananPenjualan->pesananPenjualan->so_date,
                        'nofak'         =>    $sales->nofak,
                        'tglfak'        =>    $this->dateCarbon($sales->tgl_faktur),
                        'total'         =>    $sales->totals,
                        'catatan'       =>    $sales->note,
                    ];
            //     }
            }
        }
        $newCollection    = collect($array)->groupBy('nofak');

        $view = [
            'title'           =>      'Laporan Uang Muka Pesanan Penjualan',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.uang_muka_pesanan_penjualan')->with($view);
    }

    public function uang_muka_per_faktur(Request $request)
    {
        $fakturUangMuka         =       $this->fakturPenjualan->where('uang_muka', 0)
                                        ->dateFilter($request)->pluck('id');
        $transaksiUangMuka      =       $this->transaksiUangMuka->with('fakturPenjualan.barang.barangPengirimanPenjualan.barangPesananPenjualan.pesananPenjualan')
                                        ->select(array(
                                            DB::Raw('sum(transaksi_uang_muka.jumlah) as total'),
                                            'faktur_penjualan_id',
                                            'id',
                                        ))
                                        ->whereIn('faktur_penjualan_id', $fakturUangMuka)->groupBy('id');
        
        $array = [];
        foreach($transaksiUangMuka->get() as $sales){
            foreach($sales->fakturPenjualan->barang as $salesUangMuka){
                $array [] = [
                    'nofak'         =>  $sales->fakturPenjualan->no_faktur,
                    'noso'          =>  $salesUangMuka->barangPengirimanPenjualan->barangPesananPenjualan->pesananPenjualan->so_number,
                    'tglfak'        =>  $this->dateCarbon($sales->fakturPenjualan->invoice_date),
                    'tglso'         =>  $this->dateCarbon($salesUangMuka->barangPengirimanPenjualan->barangPesananPenjualan->pesananPenjualan->so_date),
                    'keterangan'    =>  $sales->fakturPenjualan->keterangan,
                    'total'         =>  $sales->total,
                ];
            }
        }
        
        $newCollection  = collect($array)->groupBy('nofak');
        $newCollect     = collect($array)->groupBy('nofak');
        $view = [
            'title'           =>      'Laporan Uang Muka per Faktur',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.uang_muka_per_faktur')->with($view);
    }

    public function histori_penawaran_penjualan(Request $request)
    {   
        $pelanggan              =   $this->informasiPelanggan->pluck('id');
        $barangPenawaran        =   $this->barangPenawaranPenjualan->pluck('penawaran_penjualan_id');
        $penawaranPenjualan     =   $this->penawaranPenjualan->with('pelanggan')->with('barang.produk')
                                    ->with('barang.barangPesananPenjualan.pesananPenjualan')
                                    ->dateFilter($request)
                                    ->whereIn('id', $barangPenawaran)->whereIn('pelanggan_id', $pelanggan)
                                    ->where('status', 2)->orderBy('created_at', 'asc');
        $array        = [];
        foreach($penawaranPenjualan->get() as $sale => $sales){
            
            foreach ($sales->barang as $sell) {
                
                if($sell->barangPesananPenjualan !== null){
                    $array [] = [
                        'pelanggan'     =>      $sales->pelanggan->nama,
                        'nopelanggan'   =>      $sales->pelanggan->no_pelanggan,
                        'nopenawaran'   =>      $sales->no_penawaran,
                        'tglpenawaran'  =>      $this->dateCarbon($sales->tanggal),
                        'nobarang'      =>      $sell->produk->no_barang,
                        'nmbarang'      =>      $sell->produk->keterangan,
                        'qty'           =>      $sell->jumlah,
                        'unit'          =>      $sell->satuan,
                        'tglfaktur'     =>      $this->dateCarbon($sales->invoice_date),
                        'status'        =>      $sales->status,
                        'nopesanan'     =>      $sell->barangPesananPenjualan->pesananPenjualan->so_number,
                        'tglpesanan'    =>      $this->dateCarbon($sell->barangPesananPenjualan->pesananPenjualan->so_date),                    
                    ];
                }                
            }
        }
        $newCollection  = collect($array)->groupBy('pelanggan');

        $view = [
            'title'           =>      'Laporan Histori Penawaran Penjualan',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.histori_penawaran_penjualan')->with($view);
    }

    public function histori_pesanan_penjualan(Request $request)
    {
        $pelanggan              =   $this->informasiPelanggan->pluck('id');
        $barangPesanan          =   $this->barangPesananPenjualan->pluck('pesanan_penjualan_id');
        $pesananPenjualan       =   $this->pesananPenjualan->with('pelanggan')->with('barang.produk')
                                    ->with('barang.barangPengirimanPenjualan.pengirimanPenjualan')
                                    ->dateFilter($request)
                                    ->whereIn('id', $barangPesanan)->whereIn('pelanggan_id', $pelanggan)
                                    ->where('status', 2)->orderBy('created_at', 'asc');
        $array        = [];
        foreach($pesananPenjualan->get() as $sale => $sales){
            foreach ($sales->barang as $sell) {
                if($sell->barangPengirimanPenjualan !== null){
                    $array [] = [
                        'pelanggan'     =>      $sales->pelanggan->nama,
                        'nopelanggan'   =>      $sales->pelanggan->no_pelanggan,
                        'nopesanan'     =>      $sales->so_number,
                        'tglpesanan'    =>      $this->dateCarbon($sales->tanggal),
                        'nobarang'      =>      $sell->produk->no_barang,
                        'nmbarang'      =>      $sell->produk->keterangan,
                        'qty'           =>      $sell->jumlah,
                        'unit'          =>      $sell->satuan,
                        'tglfaktur'     =>      $this->dateCarbon($sales->invoice_date),
                        'status'        =>      $sales->status,
                        'nopengiriman'  =>      $sell->barangPengirimanPenjualan->pengirimanPenjualan->delivery_no,
                        'tglpengiriman' =>      $this->dateCarbon($sell->barangPengirimanPenjualan->pengirimanPenjualan->delivery_date),                    
                    ];
                }                
            }
        }
        $newCollection  = collect($array)->groupBy('pelanggan');

        $view = [
            'title'           =>      'Laporan Histori Pesanan Penjualan',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.histori_pesanan_penjualan')->with($view);
    }

    public function histori_pengirim_pesanan(Request $request)
    {
        $pelanggan              =   $this->informasiPelanggan->pluck('id');
        $barangPengiriman       =   $this->barangPengirimanPenjualan->pluck('pengiriman_penjualan_id');
        $pengirimanPenjualan    =   $this->pengirimanPenjualan->with('pelanggan')->with('barang.produk')
                                    ->with('barang.barangFakturPenjualan.fakturPenjualan')
                                    ->dateFilter($request)
                                    ->whereIn('id', $barangPengiriman)->whereIn('pelanggan_id', $pelanggan)
                                    ->where('cetak', 0)->orderBy('created_at', 'asc');
        $array        = [];
        foreach($pengirimanPenjualan->get() as $sale => $sales){
            foreach ($sales->barang as $sell) {
                if($sell->barangFakturPenjualan !== null){
                    $array [] = [
                        'pelanggan'     =>      $sales->pelanggan->nama,
                        'nopelanggan'   =>      $sales->pelanggan->no_pelanggan,
                        'nopengiriman'  =>      $sales->so_number,
                        'tglpengiriman' =>      $this->dateCarbon($sales->tanggal),
                        'nobarang'      =>      $sell->produk->no_barang,
                        'nmbarang'      =>      $sell->produk->keterangan,
                        'qty'           =>      $sell->jumlah,
                        'unit'          =>      $sell->item_unit,
                        'status'        =>      $sales->cetak,
                        'nofaktur'      =>      $sell->barangFakturPenjualan->fakturPenjualan->no_faktur,
                        'tglfaktur'     =>      $this->dateCarbon($sell->barangFakturPenjualan->fakturPenjualan->invoice_date),
                    ];
                }                
            }
        }
        $newCollection  = collect($array)->groupBy('pelanggan');

        $view = [
            'title'           =>      'Laporan Histori Pengiriman Pesanan',
            'subTitle'        =>      $this->generateSubTitleDMY($request),
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.histori_pengirim_pesanan')->with($view);
    }

    // Laporan Penjualan - Rincian Penawaran Tagihan Dan Pendapatan RMA
    public function tagihan_rma()
    {
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.tagihan_rma');
    }

    public function pendapatan_rma()
    {
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.pendapatan_rma');
    }

    public function switchAjaxRequest($query, $request, $title, $viewBlade, $titlePdf, $fungsiUtama, $posCheck ) {
        if($request->ajax()){
            if ( $request['tipe'] == 'generatePDF' ) {
                $getQuery   =   $query->get();
                $mapQuery   = $this->modifyQuery($getQuery, $request['tipe'], $fungsiUtama, $request);
                $filePath = $this->PDFPath($titlePdf, $fungsiUtama);
                if ( $this->PDFGenerator($title, $request, $mapQuery, $filePath) == true ) {
                    return response()->json(
                        [
                            'code' => '200'
                        ]
                    );
                } else {
                    return response()->json(
                        [
                            'code' => '400'
                        ]
                    );
                }
            } else if ( $request['tipe'] == 'print' ) {
                $getQuery   =   $query->get();
                $mapQuery   =   $this->modifyQuery($getQuery, $request['tipe'], $fungsiUtama, $request);
                return response()->json($mapQuery, 200);
            }elseif ( $request['tipe'] == 'download' ) {
                $downloadPath   =   asset('storage/laporan/penjualan/'.$fungsiUtama.'/'.$titlePdf);
                return response()->json([
                    'success'   => true,
                    'path'      => $downloadPath,
                    'title'     => $titlePdf
                ], 200);
            }elseif( $request['tipe'] == 'scroll' ){
                $paginateQuery  =   $query->paginate(50);
                $paginateQuery = $this->modifyQuery($paginateQuery, $request['tipe'], $fungsiUtama, $request);
                return response()->json([$paginateQuery->toArray()], 200);
            } else if ( empty($request['tipe']) ) {
                return true;
            }
        }else{
            $paginateQuery  =   $query->paginate(50);
            $paginateQuery  =   $this->paginateFrontView( $paginateQuery, $fungsiUtama, $request);
            $view = $this->generateViewData($title, $request, $paginateQuery);
            $view['checkUs'] = $posCheck;
            $view['kasir_id'] = $request->kasir_id ?? null;
            return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_penjualan.'.$viewBlade)->with($view);
        }
    }

    public function modifyQuery($Query, $tipe, $fungsiUtama, $request)
    {
        $sumTotal = 0; $sumKuantitas = 0;
        if ( $tipe == 'generatePDF' ) {
            switch ( $fungsiUtama ) {
                case 'penjualan_perbarang':
                    $mapQuery   =   $Query->map(function($query) use(&$sumTotal, &$sumKuantitas, $request){
                        $arrRetur = [];
                        $returPenjualan = $this->returPenjualan->DateFilter($request)->get()->map( function ($query) use (&$arrRetur) {
                            $dataRetur = $query->barang->map(function ($query) use (&$arrRetur) {
                                return $arrRetur [] = [
                                    'namaBarang'    => $query->item_deskripsi,
                                    'jumlah'        => $query->jumlah,
                                    'harga'         => $query->jumlah * $query->harga
                                ];
                            });
                        });
                        $collectArr = collect($arrRetur)->groupBy('namaBarang');
                        if ( !$collectArr->isEmpty() ) {
                            foreach ($collectArr as $key => $value) {
                                if ( $query->namaBarang == $value[0]['namaBarang']) {
                                    $sumKuantitas   +=   $query->sumTotal - $value->sum('jumlah');
                                    $sumTotal       +=   $query->sumHargaFinal - $value->sum('harga');
                                    return [
                                        'nama_barang'       =>      $query->namaBarang,
                                        'kuantitas'         =>      $query->sumTotal - $value->sum('jumlah'),
                                        'satuan'            =>      $query->itemUnit,
                                        'total'             =>      $query->sumHargaFinal - $value->sum('harga'),
                                        'sumKuantitas'      =>      $sumKuantitas,
                                        'sumTotal'          =>      $sumTotal,
                                    ];
                                }
                            }
                            $sumKuantitas   +=   $query->sumTotal;
                            $sumTotal       +=   $query->sumHargaFinal;
                            return [
                                'nama_barang'       =>      $query->namaBarang,
                                'kuantitas'         =>      $query->sumTotal,
                                'satuan'            =>      $query->itemUnit,
                                'total'             =>      $query->sumHargaFinal,
                                'sumKuantitas'      =>      $sumKuantitas,
                                'sumTotal'          =>      $sumTotal,
                            ];
                        } else {
                            $sumKuantitas   +=   $query->sumTotal;
                            $sumTotal       +=   $query->sumHargaFinal;
                            return [
                                'nama_barang'       =>      $query->namaBarang,
                                'kuantitas'         =>      $query->sumTotal,
                                'satuan'            =>      $query->itemUnit,
                                'total'             =>      $query->sumHargaFinal,
                                'sumKuantitas'      =>      $sumKuantitas,
                                'sumTotal'          =>      $sumTotal,
                            ];
                        }
                    })->groupBy('nama_barang');
                break;
                case 'penjualan_per_pelanggan':
                    $mapQuery   =   $Query->map(function($query) use(&$sumTotal) {
                        $sumTotal       +=   $query->day_total;
                        return [
                            'nama'       =>      $query->pelanggan->nama,
                            'total'      =>      $query->day_total,
                            'tanggal'    =>      $query->day,
                            'sumTotal'   =>      $sumTotal,
                        ];
                    });
                break;
                default:
                    abort(404);
                break;
            }
            return $mapQuery;
        } else if ( $tipe == 'print' ) {
            switch ( $fungsiUtama ) {
                case 'penjualan_perbarang':
                    $mapQuery = $Query->map(function($query) use(&$sumTotal, &$sumKuantitas, &$request){
                        $arrRetur = [];
                        $returPenjualan = $this->returPenjualan->DateFilter($request)->get()->map( function ($query) use (&$arrRetur) {
                            $dataRetur = $query->barang->map(function ($query) use (&$arrRetur) {
                                return $arrRetur [] = [
                                    'namaBarang'    => $query->item_deskripsi,
                                    'jumlah'        => $query->jumlah,
                                    'harga'         => $query->jumlah * $query->harga
                                ];
                            });
                        });
                        $collectArr = collect($arrRetur)->groupBy('namaBarang');
                        if ( !$collectArr->isEmpty() ) {
                            foreach ($collectArr as $key => $value) {
                                if ( $query->namaBarang == $value[0]['namaBarang']) {
                                    $sumKuantitas   +=   $query->sumTotal - $value->sum('jumlah');
                                    $sumTotal       +=   $query->sumHargaFinal - $value->sum('harga');
                                    return [
                                        'nama_barang'       =>      $query->namaBarang,
                                        'kuantitas'         =>      $query->sumTotal - $value->sum('jumlah'),
                                        'satuan'            =>      $query->itemUnit,
                                        'total'             =>      $query->sumHargaFinal - $value->sum('harga'),
                                        'sumKuantitas'      =>      $sumKuantitas,
                                        'sumTotal'          =>      $sumTotal,
                                    ];
                                }
                            }
                            $sumKuantitas   +=   $query->sumTotal;
                            $sumTotal       +=   $query->sumHargaFinal;
                            return [
                                'nama_barang'       =>      $query->namaBarang,
                                'kuantitas'         =>      $query->sumTotal,
                                'satuan'            =>      $query->itemUnit,
                                'total'             =>      $query->sumHargaFinal,
                                'sumKuantitas'      =>      $sumKuantitas,
                                'sumTotal'          =>      $sumTotal,
                            ];
                        } else {
                            $sumKuantitas   +=   $query->sumTotal;
                            $sumTotal       +=   $query->sumHargaFinal;
                            return [
                                'nama_barang'       =>      $query->namaBarang,
                                'kuantitas'         =>      $query->sumTotal,
                                'satuan'            =>      $query->itemUnit,
                                'total'             =>      $query->sumHargaFinal,
                                'sumKuantitas'      =>      $sumKuantitas,
                                'sumTotal'          =>      $sumTotal,
                            ];
                        }
                    });
                break;
                case 'penjualan_per_pelanggan':
                    $mapQuery   =   $Query->map(function($query) use(&$sumTotal) {
                        $sumTotal       +=   $query->day_total;
                        return [
                            'nama'       =>      $query->pelanggan->nama,
                            'total'      =>      $query->day_total,
                            'tanggal'    =>      $query->day,
                            'sumTotal'   =>      $sumTotal,
                        ];
                    });
                break;
                // Case Lainnya ...
                default:
                    abort(404);
                break;
            }
            return $mapQuery;
        } else if ( $tipe == 'scroll' ) {
            switch ( $fungsiUtama ) {
                case 'penjualan_perbarang' :
                    $paginateQuery      =   $Query->getCollection()->transform(function($query) use(&$sumTotal, &$sumKuantitas, &$request){
                        $arrRetur = [];
                        $returPenjualan = $this->returPenjualan->DateFilter($request)->get()->map( function ($query) use (&$arrRetur) {
                            $dataRetur = $query->barang->map(function ($query) use (&$arrRetur) {
                                return $arrRetur [] = [
                                    'namaBarang'    => $query->item_deskripsi,
                                    'jumlah'        => $query->jumlah,
                                    'harga'         => $query->jumlah * $query->harga
                                ];
                            });
                        });
                        $collectArr = collect($arrRetur)->groupBy('namaBarang');
                        if ( !$collectArr->isEmpty() ) {
                            foreach ($collectArr as $key => $value) {
                                if ( $query->namaBarang == $value[0]['namaBarang']) {
                                    $sumKuantitas   +=   $query->sumTotal - $value->sum('jumlah');
                                    $sumTotal       +=   $query->sumHargaFinal - $value->sum('harga');
                                    return [
                                        'namaBarang'        =>      $query->namaBarang,
                                        'kuantitas'         =>      $query->sumTotal - $value->sum('jumlah'),
                                        'satuan'            =>      $query->itemUnit,
                                        'total'             =>      $query->sumHargaFinal - $value->sum('harga'),
                                        'sumKuantitas'      =>      $sumKuantitas,
                                        'sumTotal'          =>      $sumTotal,
                                    ];
                                }
                            }
                            $sumKuantitas   +=   $query->sumTotal;
                            $sumTotal       +=   $query->sumHargaFinal;
                            return [
                                'namaBarang'        =>      $query->namaBarang,
                                'kuantitas'         =>      $query->sumTotal,
                                'satuan'            =>      $query->itemUnit,
                                'total'             =>      $query->sumHargaFinal,
                                'sumKuantitas'      =>      $sumKuantitas,
                                'sumTotal'          =>      $sumTotal,
                            ];
                        } else {
                            $sumKuantitas   +=   $query->sumTotal;
                            $sumTotal       +=   $query->sumHargaFinal;
                            return [
                                'namaBarang'        =>      $query->namaBarang,
                                'kuantitas'         =>      $query->sumTotal,
                                'satuan'            =>      $query->itemUnit,
                                'total'             =>      $query->sumHargaFinal,
                                'sumKuantitas'      =>      $sumKuantitas,
                                'sumTotal'          =>      $sumTotal,
                            ];
                        }
                    });
                break;
                case 'penjualan_per_pelanggan':
                    $paginateQuery      =   $Query->getCollection()->transform(function($query) use(&$sumTotal) {
                        $sumTotal       +=   $query->day_total;
                        return [
                            'nama'       =>      $query->pelanggan->nama,
                            'total'      =>      $query->day_total,
                            'tanggal'    =>      $query->day,
                            'sumTotal'   =>      $sumTotal,
                        ];
                    });
                break;
                // Ketik Case Lainnya disini
                default:
                    abort(404);
                break;
            }
            return $paginateQuery;
        }
    }

    public function paginateFrontView ( $paginateQuery, $fungsiUtama, $request)
    {
        switch ( $fungsiUtama ) {
            case 'penjualan_perbarang':
                $sumTotal = 0; $sumKuantitas = 0;
                $paginateQuery->getCollection()->transform(function($query) use(&$sumTotal, &$sumKuantitas, &$request){
                    $arrRetur = [];
                    $returPenjualan = $this->returPenjualan->DateFilter($request)->get()->map( function ($query) use (&$arrRetur) {
                        $dataRetur = $query->barang->map(function ($query) use (&$arrRetur) {
                            return $arrRetur [] = [
                                'namaBarang'    => $query->item_deskripsi,
                                'jumlah'        => $query->jumlah,
                                'harga'         => $query->jumlah * $query->harga
                            ];
                        });
                    });
                    $collectArr = collect($arrRetur)->groupBy('namaBarang');
                    if ( !$collectArr->isEmpty() ) {
                        foreach ($collectArr as $key => $value) {
                            if ( $query->namaBarang == $value[0]['namaBarang']) {
                                $sumKuantitas   +=   $query->sumTotal - $value->sum('jumlah');
                                $sumTotal       +=   $query->sumHargaFinal - $value->sum('harga');
                                return [
                                    'nama_barang'       =>      $query->namaBarang,
                                    'kuantitas'         =>      $query->sumTotal - $value->sum('jumlah'),
                                    'satuan'            =>      $query->itemUnit,
                                    'total'             =>      $query->sumHargaFinal - $value->sum('harga'),
                                    'sumKuantitas'      =>      $sumKuantitas,
                                    'sumTotal'          =>      $sumTotal,
                                ];
                            }
                        }
                        $sumKuantitas   +=   $query->sumTotal;
                        $sumTotal       +=   $query->sumHargaFinal;
                        return [
                            'nama_barang'       =>      $query->namaBarang,
                            'kuantitas'         =>      $query->sumTotal,
                            'satuan'            =>      $query->itemUnit,
                            'total'             =>      $query->sumHargaFinal,
                            'sumKuantitas'      =>      $sumKuantitas,
                            'sumTotal'          =>      $sumTotal,
                        ];
                    } else {
                        $sumKuantitas   +=   $query->sumTotal;
                        $sumTotal       +=   $query->sumHargaFinal;
                        return [
                            'nama_barang'       =>      $query->namaBarang,
                            'kuantitas'         =>      $query->sumTotal,
                            'satuan'            =>      $query->itemUnit,
                            'total'             =>      $query->sumHargaFinal,
                            'sumKuantitas'      =>      $sumKuantitas,
                            'sumTotal'          =>      $sumTotal,
                        ];
                    }
                });
            break;
            case 'penjualan_per_pelanggan':
                $sumTotal = 0;
                $paginateQuery->getCollection()->transform(function($query) use(&$sumTotal) {
                    $sumTotal      +=   $query->day_total;
                    return [
                        'nama'          =>      $query->pelanggan->nama,
                        'total'         =>      $query->day_total,
                        'tanggal'       =>      $query->day,
                        'sumTotal'      =>      $sumTotal,
                    ];
                });
            break;
            // Case Lainnya ketik dibawah
            default:
                return abort(404);
            break;
        }

        return $paginateQuery;
    }
}