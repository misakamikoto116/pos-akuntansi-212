<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\TypeEdcRequest;
use App\Modules\Akuntansi\Repositories\TypeEdcRepository;
use Generator\Interfaces\RepositoryInterface;
class TypeEdcController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Type EDC';
        $this->request = TypeEdcRequest::class;
        $this->requestField = ['nama','akun_id'];
    }

    public function formData()
    {
        return [
            'akun' => $this->model->listAkun(),
        ];
    }

}
