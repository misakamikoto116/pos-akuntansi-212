<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\DisplayIklanRequest;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Image;

class DisplayIklanController extends Controller
{
    protected $model;
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Iklan';
        $this->request = DisplayIklanRequest::class;
        $this->requestField = [
            'nama_iklan',
            'keterangan',
        ];
    }

    public function formData()
    {
        return [
        ];
    }

    public function index()
    {
        $data = $this->model->getItems();
        $views = [
            'data' => $data,
           // 'route_add' => route('')
        ];
        return view('akuntansi::iklan.index')->with($views);
    }

    public function add()
    {
        $views = [
            'title_document' => 'Tambah Display Iklan',
        ];
        return view('akuntansi::iklan.create')->with($views);
    }

    public function update($iklan_id)
    {
        $views = [
            'title_document' => 'Update Display Iklan',
            'data' => $this->model->findItem($iklan_id)
        ];
        return view('akuntansi::iklan.edit')->with($views);
    }

    public function post(DisplayIklanRequest $request)
    {
        $lastUrutan = $this->model->getLastIndex() === null ? 1 : $this->model->getLastIndex()->urutan + 1;
        $data = [
            'nama_iklan'   => $request->nama_iklan,
            'deskripsi'    => $request->keterangan,
            'publish'      => ($request->publish == '1' ? true : false),
            'urutan'       => $lastUrutan,
            'created_by'   => auth()->user()->id,
        ];
        $file = $request->file('image_iklan');
        $extension = $file->getClientOriginalExtension();
        $filename = md5($file->getFilename()).'.'.$extension;
        $location = storage_path().'/app/public/iklan/'.$filename;
        Image::make($file)->resize(810, 450)->save($location);
        Image::make($file)->fit(200, 150)->save(storage_path().'/app/public/iklan/thumb_'.$filename);
        $data['image_path'] = $filename;
        $this->model->insert($data)->save();
        //DisplayIklan::insert($data);
        return response()->redirectToRoute('akuntansi.iklan-manage.get')->withMessage('Berhasil menambahkan data');
    }

    public function postUpdate(DisplayIklanRequest $request, $iklan_id)
    {
        $lastUrutan = $this->model->getLastIndex() === null ? 1 : $this->model->getLastIndex()->urutan + 1;
        $data = [
            'nama_iklan'   => $request->nama_iklan,
            'deskripsi'    => $request->keterangan,
            'publish'      => ($request->publish == '1' ? true : false),
            'urutan'       => $this->model->getLastIndex()->first()->urutan + 1,
            'created_by'   => auth()->user()->id,
        ];
        if($request->file('image_iklan')){
            $file = $request->file('image_iklan');
            $extension = $file->getClientOriginalExtension();
            $filename = md5($file->getFilename()).'.'.$extension;
            $location = storage_path().'/app/public/iklan/'.$filename;
            Image::make($file)->resize(810, 450)->save($location);
            Image::make($file)->fit(200, 150)->save(storage_path().'/app/public/iklan/thumb_'.$filename);
            $data['image_path'] = $filename;
        }else{
            $data['image'] = $request->last_image;
        }
        $this->model->update($iklan_id, $data)->save();
        return response()->redirectToRoute('akuntansi.iklan-manage.get')->withMessage('Berhasil merubah data');
    }

    public function deleteIklan(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'iklan_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'sukses' => false,
                'msg' => 'input tidak lengkap'
            ], 400);
        }
        $this->model->delete($request->iklan_id);
        return response()->json([
            'sukses' => true,
            'msg' => 'data sukses di hapus',
            'id' => $request->iklan_id
        ], 200);
    }
    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
}
