<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Akuntansi\Http\Requests\DaftarAktivaTetapRequest;
use Generator\Interfaces\RepositoryInterface;

class DaftarAktivaTetapController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Aktiva Tetap';
        $this->request = DaftarAktivaTetapRequest::class;
        $this->requestField = [
            'kode_aktiva',
            'tipe_aktiva_id',
            'tgl_beli',
            'tgl_pakai',
            'keterangan',
            'qty',
            'departement_id',
            'tgl_jurnal',
            'tahun',
            'bulan',
            'metode_penyusutan_id',
            'rasio',
            'akun_aktiva_id',
            'akun_akum_penyusutan_id',
            'akun_beban_penyusutan_id',
            'aktiva_tidak_berwujud',
            'aktiva_tetap_fisikal',
            'akun_pengeluaran_aktiva_id',
            'tanggal_pengeluaran',
            'keterangan_pengeluaran',
            'jumlah_pengeluaran',
            'harga_perolehan',
            'aktiva_dihentikan',
            'akumulasi_penyusutan',
            'nilai_buku',
            'sisa',
            'catatan',
        ];
    }

    public function formData()
    {
        return [
            'listTipeAktivaTetap'   => $this->model->listTipeAktivaTetap(),
            'listAkun'              => $this->model->listAkun(),
            'listMetodePenyusutan'  => $this->model->listMetodePenyusutan(),
            'listAkunAktivaTetap'   => $this->model->listAkunAktivaTetap(),
            'listAkunPenyusutan'    => $this->model->listAkunPenyusutan(),
            'permission' => [
                'buat' => 'buat_aktiva_tetap',
                'ubah' => 'ubah_aktiva_tetap',
                'hapus' => 'hapus_aktiva_tetap',
                'laporan' => 'laporan_aktiva_tetap',
                'lihat' => 'lihat_aktiva_tetap',
                'daftar' => auth()->user()->hasPermissionTo('daftar_aktiva_tetap'),
            ],
            'status_aktiva' => ['0' => 'Tidak','1' => 'Ya','2' => 'Semua'],
            'filter' => ['kode_aktiva', 'keterangan_aktiva', 'tipe_aktiva','tidak_berwujud','pajak','tanggal_pakai','tanggal_perolehan','metode_penyusutan'],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function reqKodeAktiva(Request $request)
    {
        $data = $this->model->getKodeAktiva($request->kode_aktiva);
        $data->isEmpty() ? $response['response'] = null : $response['response'] = 'ada' ;
        return response()->json($response);
    }
}
