<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\KategoriProdukRequest;
use App\Modules\Akuntansi\Repositories\KategoriProdukRepository;
use Generator\Interfaces\RepositoryInterface;
class KategoriProdukController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Kategori Produk';
        $this->request = KategoriProdukRequest::class;
        $this->requestField = ['nama'];
    }
}
