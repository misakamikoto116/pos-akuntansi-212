<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\TipeAkunRequest;
use App\Modules\Akuntansi\Repositories\TipeAkunRepository;
use Generator\Interfaces\RepositoryInterface;

class TipeAkunController extends Controller
{
	public function __construct(RepositoryInterface $model)
	{
		$this->role = 'akuntansi';	
	    $this->model = $model;
	    $this->title = 'Data Tipe Akun';
	    $this->request = TipeAkunRequest::class;
	    $this->requestField = ['title'];
	}
}
