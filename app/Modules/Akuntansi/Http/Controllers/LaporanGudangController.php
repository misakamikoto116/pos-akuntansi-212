<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\PemindahanBarang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class LaporanGudangController extends Controller
{
    public function __construct(
        Identitas   $identitas,
        Gudang      $gudang,
        PemindahanBarang    $pemindahanBarang,
        Produk      $produk,
        SaldoAwalBarang $saldoAwalBarang
    )
    {
        $this->identitas        =   $identitas;
        $this->gudang           =   $gudang;
        $this->pemindahanBarang =   $pemindahanBarang;
        $this->produk           =   $produk;
        $this->saldoAwalBarang  =   $saldoAwalBarang;
    }

    protected function identitasPerushaan(){
        $nama_perusahaan        =       $this->identitas->first()->nama_perusahaan;
        return $nama_perusahaan;
    }
    public function dateCarbon($ubah){
        $format     =       Carbon::parse($ubah)->format('d F Y');
        return $format;
    }

    public function daftar_gudang(){
        $dataGudang         =   $this->gudang->groupBy('id')->get();
        $array = [];
        foreach($dataGudang as $data){
            $array[] = [
                'nama'          =>  $data->nama,
                'keterangan'    =>  $data->keterangan,
                'pj'            =>  $data->penanggung_jawab,
                'alamat'        =>  $data->alamat,
            ];
        }
        $newCollection = collect($array);

        $view = [
            'title'           =>      'Laporan Daftar Gudang',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_gudang.daftar_gudang')->with($view);
    }

    public function rincian_daftar_perpindahan_barang(Request $request){
        $dataPindahBarang         =   $this->pemindahanBarang->with('detailPemindahanBarang.produk')->with('dataDariGudang')->with('dataKeGudang')
                                      ->dateFilter($request)->groupBy('id');
        $array = [];
        foreach($dataPindahBarang->get() as $dataPindahBarang){
            foreach($dataPindahBarang->detailPemindahanBarang as $dataPindah){
                $array[] = [
                    'no_transfer'   =>  $dataPindahBarang->no_transfer,
                    'tgl_transfer'  =>  $this->dateCarbon($dataPindahBarang->tanggal),
                    'keterangan'    =>  $dataPindahBarang->keterangan,
                    'dari_gudang'   =>  $dataPindahBarang->dataDariGudang->nama,
                    'ke_gudang'     =>  $dataPindahBarang->dataKeGudang->nama,
                    'kode_barang'   =>  $dataPindah->produk->no_barang,
                    'nama_barang'   =>  $dataPindah->keterangan,
                    'qtydefault'    =>  $dataPindah->jumlah,
                    'qty'           =>  $dataPindah->jumlah,
                    'satuan'        =>  $dataPindah->satuan,
                ];
            }
        }
        $newCollection = collect($array)->groupBy('no_transfer');

        $view = [
            'title'           =>      'Laporan Rincian Daftar Perpindahan Gudang',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_gudang.rincian_daftar_perpindahan_barang')->with($view);
    }

    public function mutasi_perbarang_pergudang(Request $request){
        $dataBarang     =   $this->produk->with(['saldoAwalBarang.transaksi' =>function ($query){
                            $query->withTrashed()->with('item');},'gudang'])
                            ->where('tipe_barang', 0)->groupBy('no_barang')->get();

        $view = [
            'title'           =>      'Laporan Mutasi per Barang per Gudang',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $this->barangGudangArray($dataBarang),
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_gudang.mutasi_perbarang_pergudang')->with($view);
    }

    public function mutasi_pergudang_perbarang(Request $request){
        $gudang         =   $this->gudang->with(['produks.saldoAwalBarang.transaksi' =>function ($query){
                                $query->withTrashed()->with('item');
                            }])->get();
        $view = [
            'title'           =>      'Laporan Mutasi per Gudang per Barang',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'items'           =>      $this->gudangArray($gudang),
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_gudang.mutasi_pergudang_perbarang')->with($view);
    }

    public function ringkasan_mutasi_gudang_perbarang(Request $request){
        $dataGudang         =   $this->gudang->with('produks.saldoAwalBarang.transaksi.item')
                                ->DateFilter($request)->get();
        $view = [
        'title'           =>      'Laporan Ringkasan Mutasi Gudang per Barang',
        'perusahaan'      =>      $this->identitasPerushaan(),
        'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
        'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
        'item'            =>      $this->RingkasanMutasiGudangBarang($dataGudang),
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_gudang.ringkasan_mutasi_gudang_perbarang')->with($view);
    }
    public function kuantitas_barang_per_daftar_gudang(Request $request){
        $produk         =   $this->produk->get(['id','no_barang','keterangan']);
        $gudang         =   $this->gudang->pluck('nama','id');

        $dataSaldo      =   $this->saldoAwalBarang->with('gudang','produk')
                            ->whereIn('produk_id',$produk->pluck('id'))
                            ->whereIn('gudang_id',$gudang->keys())
                            ->get();
        $view = [
            'title'           =>      'Laporan Kuantitas Barang per Daftar Barang',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $this->KuantitasBarangGudang($dataSaldo, $gudang, $produk),
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_gudang.kuantitas_barang_per_daftar_gudang')->with($view);
    }

    public function lembar_perhitungan_stok_fisik_barang(Request $request){
        $produk         =   $this->produk->where('tipe_barang', '!=', 3)->pluck('id');
        $saldoAwal      =   $this->saldoAwalBarang->with('produk')->whereIn('produk_id',$produk)
                            ->select(array(
                                DB::Raw('sum(produk_detail.kuantitas) as kuantitas'),
                                DB::Raw('produk_detail.produk_id as produk_id'),
                                'id'
                            ))
                            ->groupBy('produk_id')->get();
        $view = [
            'title'           =>      'Laporan Lembar Perhitungan Stok Fisik Barang',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $this->LembarPerhitunganStokFisikBarang($saldoAwal),
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_gudang.lembar_perhitungan_stok_fisik_barang')->with($view);
    }

    //  Fungsi-fungsi

    public Function LembarPerhitunganStokFisikBarang($saldoAwal){
        $totalKuantitas = 0;
        return $saldoAwal->map(function($itemSaldo)use(&$totalKuantitas){
            return [
                'kode_barang'       =>      $itemSaldo->produk->no_barang,
                'nama_barang'       =>      $itemSaldo->produk->keterangan,
                'kuantitas'         =>      $itemSaldo->kuantitas,
                'satuan'            =>      $itemSaldo->produk->unit,
                'kontrol_qty'       =>      0, // masih belum tau
                'total_kuantitas'   =>      $itemSaldo->sum('kuantitas'),
                'total_kontrol_qty' =>      0, // masih belum tau
            ];
        });
    }

    public function KuantitasBarangGudang($dataSaldo, $gudang, $produk){
        $output = collect()->push($gudang->values()->prepend('Nama Barang')->prepend('No Barang'));
        $items = $dataSaldo->groupBy('produk_id')->map(function ($produk){
            return $produk->groupBy('gudang_id')->map(function ($gudang)
            {
                return $gudang->sum('kuantitas');
            });
        });
        $noBarang = $produk->pluck('no_barang','id');
        $pordukBarang = $produk->pluck('keterangan','id');
        foreach ($items as $produkID => $gudangItem) {
            $temp = collect();
            $temp->push($noBarang[$produkID]);
            $temp->push($pordukBarang[$produkID]);
            foreach ($gudang->keys() as $gudangID) {
                $temp->push($gudangItem[$gudangID] ?? 0);
            }
            $output->push($temp);
        }
        return $output;
    }

    public function barangGudangArray($dataBarang)
    {
        return $dataBarang->map(function($itemBarang){
            $saldo = $itemBarang->kuantitas;
            $dataGudang = $itemBarang->saldoAwalBarang->where('status', 1)->where('kuantitas', '!=', 0)
            ->map(function($itemGudang) use(&$saldo){
                if($itemGudang->kuantitas >= 0){
                    $kts_masuk           =   $itemGudang->kuantitas;
                    $kts_keluar          =   0;
                }elseif($itemGudang->kuantitas < 0){
                    $kts_masuk           =   0;
                    $kts_keluar          =   $itemGudang->kuantitas;
                }
                if($itemGudang->transaksi){
                    $dataTransaksi [] = [
                        'tanggal'           => $this->dateCarbon($itemGudang->transaksi->tanggal ?? null),
                        'no_transaksi'      => $itemGudang->transaksi->no_transaksi ?? null,
                        'tipe'              => $itemGudang->transaksi->sumber ?? null,
                        'keterangan'        => $itemGudang->transaksi->keterangan ?? null,
                        'kts_masuk'         => $kts_masuk ?? 0,
                        'kts_keluar'        => $kts_keluar ?? 0,
                        'saldo'             => $saldo = $saldo + (($kts_masuk ?? 0) + ($kts_keluar ?? 0)),
                    ];
                }
                return [
                    'nama_gudang'   =>  $itemGudang->gudang->nama,
                    'transaksi'     =>  $dataTransaksi,
                ];
            })->groupBy('nama_gudang');
            return [
                'kode_barang'   =>  $itemBarang->no_barang,
                'nama_barang'   =>  $itemBarang->keterangan,
                'saldo_awal'    =>  $itemBarang->kuantitas,
                'gudang'        =>  $dataGudang,
            ];
        });
    }

    public function gudangArray($gudang)
    {
        return $gudang->map(function ($item_gudang)
        {
            $data_produk = $item_gudang->produks->map(function ($item_produk)
            {
                $saldo = 0;
                $data_saldo_awal  = $item_produk->saldoAwalBarang
                ->where('status', 1)->where('kuantitas', '!=', 0)
                ->map(function ($item_saldo_awal) use(&$saldo)
                {
                    if($item_saldo_awal->kuantitas >= 0){
                        $kts_masuk           =   $item_saldo_awal->kuantitas;
                        $kts_keluar          =   0;
                        $qty                 =   ($kts_masuk   ?? 0);
                    }elseif($item_saldo_awal->kuantitas < 0){
                        $kts_masuk           =   0;
                        $kts_keluar          =   $item_saldo_awal->kuantitas;
                        $qty                 =   ($kts_keluar   ?? 0);
                    }
                    
                    if($item_saldo_awal->transaksi) {
                        return [
                            'tanggal'           => $this->dateCarbon($item_saldo_awal->transaksi->tanggal ?? null),
                            'no_transaksi'      => $item_saldo_awal->transaksi->no_transaksi ?? null,
                            'tipe'              => $item_saldo_awal->transaksi->sumber ?? null,
                            'keterangan'        => $item_saldo_awal->transaksi->keterangan ?? null,
                            'kts_masuk'         => $kts_masuk,
                            'kts_keluar'        => $kts_keluar,
                            'saldo'             => $saldo = $saldo + ($kts_masuk + $kts_keluar),
                        ];
                    }
                })->reject(function ($item)
                {
                    return $item == null;
                })->values();
                // dd($data_saldo_awal);
                return [
                    'nama_barang'   => $item_produk->keterangan,   
                    'kode_barang'   => $item_produk->no_barang,
                    'kuantitas'     => $item_produk->kuantitas,
                    'saldo_awal'    => $data_saldo_awal,
                    'sum_kts_masuk' => $data_saldo_awal->sum('kts_masuk'),
                    'sum_kts_keluar' => $data_saldo_awal->sum('kts_keluar'),
                ];
            });
            return [
                'nama_gudang'       => $item_gudang->nama,
                'produk'            => $data_produk,
            ];
        });
    }
    public function RingkasanMutasiGudangBarang($dataGudang){
        $totalSaldoAwal = 0;    $totalKtsMasuk = 0;
        $totalKtsKeluar = 0;    $totalBalance  = 0;
        return $dataGudang->map(function($itemGudang)use(&$totalSaldoAwal,&$totalKtsMasuk,&$totalKtsKeluar,&$totalBalance){
            $subTotalSaldoAwal = 0;    $subTotalKtsMasuk = 0;
            $subTotalKtsKeluar = 0;    $subTotalBalance  = 0;
            $dataBarang = $itemGudang->produks->map(function($itemBarang)use(&$subTotalSaldoAwal,&$subTotalKtsMasuk,&$subTotalKtsKeluar,&$subTotalBalance){
                $dataSaldoAwal  =   $itemBarang->saldoAwalBarang->where('status', 1)->where('kuantitas', '!=', 0)->map(function($itemSaldoAwal){
                    if($itemSaldoAwal->kuantitas > 0){
                        $ktsMasuk           =   $itemSaldoAwal->kuantitas ?? 0;
                        $ktsKeluar          =   0;
                    }elseif($itemSaldoAwal->kuantitas < 0){
                        $ktsMasuk           =   0;
                        $ktsKeluar          =   $itemSaldoAwal->kuantitas ?? 0;
                    }
                        return [
                            'kts_masuk'  => $ktsMasuk,
                            'kts_keluar' => $ktsKeluar,
                        ];
                });
                return [
                    'kode_barang'       =>      $itemBarang->no_barang,
                    'nama_barang'       =>      $itemBarang->keterangan,
                    'saldo_awal'        =>      $itemBarang->kuantitas,
                    'sum_kts_masuk'     =>      $dataSaldoAwal->sum('kts_masuk'),
                    'sum_kts_keluar'    =>      $dataSaldoAwal->sum('kts_keluar'),
                    'balance'           =>      $itemBarang->kuantitas + ($dataSaldoAwal->sum('kts_masuk') + $dataSaldoAwal->sum('kts_keluar')),
                    'data_saldo_awal'   =>      $dataSaldoAwal,
                ];
            });
            return [
                'nama_gudang'               =>  $itemGudang->nama,
                'sum_total_saldo_awal'      =>  $totalSaldoAwal += $dataBarang->sum('saldo_awal'),
                'sum_total_kts_masuk'       =>  $totalKtsMasuk  += $dataBarang->sum('sum_kts_masuk'),
                'sum_total_kts_keluar'      =>  $totalKtsKeluar += $dataBarang->sum('sum_kts_keluar'),
                'sum_total_balance'         =>  $totalBalance   += $dataBarang->sum('balance'),
                'data_barang'               =>  $dataBarang,
            ];
        });
    }
}