<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\PermintaanPembelian;
use App\Modules\Akuntansi\Models\BarangPermintaanPembelian;
use App\Modules\Akuntansi\Models\PesananPembelian;
use App\Modules\Akuntansi\Models\BarangPesananPembelian;
use App\Modules\Akuntansi\Models\PenerimaanPembelian;
use App\Modules\Akuntansi\Models\BarangPenerimaanPembelian;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\BarangFakturPembelian;
use App\Modules\Akuntansi\Models\BebanFakturPembelian;
use App\Modules\Akuntansi\Models\ReturPembelian;
use App\Modules\Akuntansi\Models\BarangReturPembelian;
use App\Modules\Akuntansi\Models\TransaksiUangMukaPemasok;
use Carbon\Carbon;
use DB;

class LaporanPembelianController extends Controller
{
    public function __construct(
        Identitas $identitas,
        InformasiPemasok $informasiPemasok,
        PermintaanPembelian $permintaanPembelian,
        BarangPermintaanPembelian $barangPermintaanPembelian,
        PesananPembelian $pesananPembelian,
        BarangPesananPembelian $barangPesananPembelian,
        PenerimaanPembelian $penerimaanPembelian,
        BarangPenerimaanPembelian $barangPenerimaanPembelian,
        FakturPembelian $fakturPembelian,
        BarangFakturPembelian $barangFakturPembelian,
        BebanFakturPembelian $bebanFakturPembelian,
        ReturPembelian $returPembelian,
        BarangReturPembelian $barangReturPembelian,
        TransaksiUangMukaPemasok $transaksiUangPemasok
    )
    {
        $this->identitas                   =   $identitas;
        $this->informasiPemasok            =   $informasiPemasok;
        $this->permintaanPembelian         =   $permintaanPembelian;
        $this->barangPermintaanPembelian   =   $barangPermintaanPembelian;
        $this->pesananPembelian            =   $pesananPembelian;
        $this->barangPesananPembelian      =   $barangPesananPembelian;
        $this->penerimaanPembelian         =   $penerimaanPembelian;
        $this->barangPenerimaanPembelian   =   $barangPenerimaanPembelian;
        $this->fakturPembelian             =   $fakturPembelian;
        $this->barangFakturPembelian       =   $barangFakturPembelian;
        $this->bebanFakturPembelian        =   $bebanFakturPembelian;
        $this->returPembelian              =   $returPembelian;
        $this->barangReturPembelian        =   $barangReturPembelian;
        $this->transaksiUangMukaPemasok    =   $transaksiUangPemasok;
    }

    use TraitLaporan;
    
    // Laporan Pembelian - Rincian Pembelian
    public function pembelian_per_pemasok(Request $request){
        $title = 'Laporan Pembelian per Pemasok';

        $request['start-date'] === null ? $request['start-date'] = date('Y-m-1') : $request['start-date'] ;
        $request['end-date']   === null ? $request['end-date']   = date('Y-m-t') : $request['end-date'] ;
        //Ambil tgl dulu
        //start-date=2018-12-01&end-date=2018-12-31
        $startDate = Carbon::createFromFormat('Y-m-d', $request->get('start-date'));
        $endDate = Carbon::createFromFormat('Y-m-d', $request->get('end-date'));

        $dates = []; //-> berupa range tanggal dlm bentuk aray

        for($date = $startDate; $date->lte($endDate); $date->addWeek()) {
            //isi berupa range tanggal
            $dates[] = ['tgl' => $date->format('Y-m-d') . ' - ' . $date->addDay(6)->format('Y-m-d')];
        }
        /*
         * Range tanggal jika di pilih mingguan maka di dapat di ambil per 7 hari jika filter bulan
         * maka dapat di ambil by month dalam range filter tersebut
         * */
        app('debugbar')->info($dates);
        $bagiNilaiFilter = $request->bagi_nilai;
        //$nilai = 'day';
        $view = null;
        switch ($bagiNilaiFilter){
            case 'hanya_total' :
                $nilai = 'day';
                $view = 'pembelian_per_pemasok_hanya_total';
                break;
            case 'harian' :
                $nilai = 'day';
                $view = 'pembelian_per_pemasok';
                break;
            case 'mingguan' :
                $nilai = DB::Raw('WEEK(day)');
                $view = 'pembelian_per_pemasok_weekly';
                break;
            case 'kwartal':
                $nilai = DB::Raw('QUARTER(day)');
                $view = 'pembelian_per_pemasok_kwartal';
                break;
            case 'tahunan':
                $nilai = DB::Raw('YEAR(day)');
                $view = 'pembelian_per_pemasok_yearly';
                break;
            default : $nilai = 'day';
        }
//        app('debugbar')->info("nama");
        $dataPemasok = $this->informasiPemasok->pluck('id');
        $totalPenjualan = $this->fakturPembelian
            ->with('pemasok')
            ->select(
                    [   DB::Raw('sum(faktur_pembelian.total) as day_total'),
                        DB::Raw('DATE(faktur_pembelian.created_at) day'),'pemasok_id'
                        ]
            )
        ->dateFilter($request)->whereIn('pemasok_id', $dataPemasok)->where('uang_muka', 0)
        ->groupBy('pemasok_id')->groupBy($nilai)->orderBy('day','desc');
        $array = [];
        foreach($totalPenjualan->get() as $buy){
            $array[] = [
                'nama' => $buy->pemasok->nama,
                'total' => $buy->day_total,
                'tanggal' => $buy->day,
            ];
        }
        $newCollection = collect($array)->groupBy('tanggal');
        $viewData = $this->generateViewData($title, $request ,$newCollection);
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.'.$view)->with(['dates' => $dates])->with($viewData);
    }

    public function rincian_pembelian_per_pemasok(Request $request){
        $dataPemasok            =       $this->informasiPemasok->pluck('id');
        $totalPembelian         =       $this->fakturPembelian->with('pemasok')
                                        ->select(array(
                                            DB::Raw('sum(faktur_pembelian.total) as day_total'),
                                            DB::Raw('DATE(faktur_pembelian.created_at) day'),
                                            DB::Raw('faktur_pembelian.no_faktur as nofaktur'),
                                            DB::Raw('DATE(faktur_pembelian.invoice_date) tgl_faktur'),
                                            DB::Raw('faktur_pembelian.keterangan as keterangan'),
                                            'pemasok_id'))
                                        ->dateFilter($request)->whereIn('pemasok_id', $dataPemasok)->where('uang_muka',0)->groupBy('id')
                                        ->groupBy('pemasok_id')->groupBy('day')->orderBy('day','desc');

        $array = [];
        foreach($totalPembelian->get() as $buys){
            $array[] = [
                'nopemasok'          =>     $buys->pemasok->no_pemasok,
                'nama'               =>     $buys->pemasok->nama,
                'no_faktur'          =>     $buys->nofaktur,
                'tanggal_faktur'     =>     Carbon::parse($buys->tgl_faktur)->format('d F Y'),
                'keterangan'         =>     $buys->keterangan,
                'total'              =>     $buys->day_total,
                'tanggal'            =>     $buys->day
            ];
        }
        $newCollection = collect($array)->groupBy('nopemasok');

        $view = [
            'title'           =>      'Laporan Rincian Pembelian per Pelanggan',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.rincian_pembelian_per_pemasok')->with($view);
    }

    public function pembelian_per_barang_total(Request $request){
        $fakturPembelian    =   $this->fakturPembelian->pluck('id');
        $totalBarang        =   $this->barangFakturPembelian->with('produk')
                                ->select(array(
                                    DB::Raw('sum(barang_faktur_pembelian.jumlah * barang_faktur_pembelian.unit_price) as day_total'),
                                    DB::Raw('DATE(barang_faktur_pembelian.created_at) day'),
                                    'produk_id'))
                                ->dateFilter($request)
                                ->groupBy('produk_id')->orderBy('day','desc');

        $array = [];
        foreach($totalBarang->get() as $buys){
            $array[] = [
                'nama'              =>  $buys->produk->keterangan,
                'total'             =>  $buys->day_total,
                'tanggal'           =>  $buys->day
            ];
        }
        $newCollection = collect($array)->groupBy('nama');

        $view = [
            'title'           =>      'Laporan Pembelian perbarang Total',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.pembelian_per_barang_total')->with($view);
    }

    public function pembelian_per_barang_kuantitas(Request $request){
        $fakturPembelian          =     $this->fakturPembelian->pluck('id');
        $KuantitasBarang          =     $this->barangFakturPembelian->with('produk')
                                        ->select(array(
                                                DB::Raw('sum(barang_faktur_pembelian.jumlah) as jmlqty'),
                                                DB::Raw('barang_faktur_pembelian.item_unit as satuan'),
                                                DB::Raw('DATE(barang_faktur_pembelian.created_at) day'),
                                                'produk_id'))
                                        ->dateFilter($request)
                                        ->groupBy('produk_id')
                                        ->orderBy('day','desc');

        $array = [];
        foreach($KuantitasBarang->get() as $buys){
            $array[] = [
                'nama'          =>  $buys->produk->keterangan,
                'total'         =>  $buys->jmlqty,
                'unit'          =>  $buys->satuan,
                'tanggal'       =>  $buys->day,
            ];
        }
        $newCollection  =   collect($array)->groupBy('nama');

        $view = [
            'title'           =>      'Laporan Pembelian perbarang (Kuantitas)',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.pembelian_per_barang_kuantitas')->with($view);
    }

    public function pembelian_per_barang(Request $request){
        $fakturPembelian    =   $this->fakturPembelian->pluck('id');
        $totalBarang        =   $this->barangFakturPembelian->with('produk')
                                ->select(array(
                                    DB::Raw('sum(barang_faktur_pembelian.jumlah * barang_faktur_pembelian.unit_price) as totalharga'),
                                    DB::Raw('sum(barang_faktur_pembelian.jumlah) as totalqty'),
                                    DB::Raw('barang_faktur_pembelian.item_unit as satuan'),
                                    DB::Raw('DATE(barang_faktur_pembelian.created_at) day'),
                                    'faktur_pembelian_id',
                                    'produk_id'))
                                ->dateFilter($request)->whereIn('faktur_pembelian_id', $fakturPembelian)
                                ->groupBy('produk_id')->orderBy('day','desc');

        $array = [];
        foreach($totalBarang->get() as $buys){
            $array[] = [
                'nama'              =>  $buys->produk->keterangan,
                'total'             =>  $buys->totalharga,
                'qty'               =>  $buys->totalqty,
                'unit'              =>  $buys->satuan,
                'tanggal'           =>  $buys->day
            ];
        }
        $newCollection = collect($array)->groupBy('nama');
        
        $view = [
            'title'           =>      'Laporan Pembelian perbarang',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.pembelian_per_barang')->with($view);
    }

    public function rincian_pembelian_per_barang(Request $request){
        $dataBarang      =  $this->barangFakturPembelian->pluck('faktur_pembelian_id');
        $totalPembelian  =  $this->fakturPembelian->with('barang.produk')
                               ->select(array(
                                   DB::Raw('sum(faktur_pembelian.total) as total'),
                                   DB::Raw('DATE(faktur_pembelian.created_at) day'),
                                   DB::Raw('faktur_pembelian.no_faktur as nofaktur'),
                                   DB::Raw('DATE(faktur_pembelian.invoice_date) tgl_faktur'),
                                   DB::Raw('faktur_pembelian.keterangan as keterangan'),
                                   'id'))
                               ->dateFilter($request)->whereIn('id', $dataBarang)->where('uang_muka',0)->groupBy('id')
                               ->groupBy('day')->orderBy('day','desc');

        $array = [];
        foreach($totalPembelian->get() as $buys){
            foreach($buys->barang as $buy){
                $array[] = [
                    'noproduk'           =>     $buy->produk->no_barang,
                    'namaproduk'         =>     $buy->item_deskripsi,
                    'no_faktur'          =>     $buys->nofaktur,
                    'tanggal_faktur'     =>     Carbon::parse($buys->tgl_faktur)->format('d F Y'),
                    'keterangan'         =>     $buys->keterangan,
                    'qty'                =>     $buy->jumlah,
                    'unit'               =>     $buy->item_unit,
                    'total'              =>     number_format(($buy->jumlah * $buys->total) / $buy->unit_price),
                    'tanggal'            =>     $buys->day
                ];
            }
        }
        $newCollection = collect($array)->groupBy('noproduk');

        $view = [
            'title'           =>      'Laporan Rincian Pembelian perbarang',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.rincian_pembelian_per_barang')->with($view);
    }

    public function pembelian_per_biaya(Request $request){
        $dataBebanFaktur        =       $this->bebanFakturPembelian->pluck('faktur_pembelian_id');
        $totalPembelian         =       $this->fakturPembelian->with('bebanFakturPembelian.akun')
                                        ->select(array(
                                            DB::Raw('sum(faktur_pembelian.total) as total'), 
                                            DB::Raw('DATE(faktur_pembelian.created_at) day'),
                                            DB::Raw('faktur_pembelian.no_faktur as nofaktur'),
                                            DB::Raw('DATE(faktur_pembelian.invoice_date) tgl_faktur'),
                                            DB::Raw('faktur_pembelian.catatan as catatan'),
                                            'id'
                                        ))
                                        ->dateFilter($request)->whereIn('id', $dataBebanFaktur)
                                        ->where('uang_muka',0)->groupBy('id')
                                        ->groupBy('day')->orderBy('day','desc');

        $array = [] ;
        foreach($totalPembelian->get() as $buys){
            foreach($buys->bebanFakturPembelian as $buy){
                $array[] = [
                    'nobeban'            =>     $buy->akun->kode_akun,
                    'namabeban'          =>     $buy->akun->nama_akun,
                    'no_faktur'          =>     $buys->nofaktur,
                    'tanggal_faktur'     =>     Carbon::parse($buys->tgl_faktur)->format('d F Y'),
                    'keterangan'         =>     $buys->catatan,
                    'total'              =>     $buy->amount,
                ];
            }
        }
        $newCollection = collect($array)->groupBy('nobeban');

        $view = [
            'title'           =>      'Laporan Pembelian Perbiaya',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.pembelian_per_biaya')->with($view);
    }

    public function pengembalian_pembelian_per_pemasok(Request $request){
        $pemasok                =       $this->identitas->pluck('id');
        $totalPembelian         =       $this->returPembelian->with('pemasok')
                                        ->select(array(
                                            DB::Raw('retur_pembelian.return_no as noretur'),
                                            DB::Raw('DATE(retur_pembelian.tanggal) tgl_retur'),
                                            DB::Raw('retur_pembelian.keterangan as catatan'),
                                            DB::Raw('sum(retur_pembelian.total) as total'), 
                                            DB::Raw('DATE(retur_pembelian.created_at) day'),
                                            'pemasok_id',
                                            'id'
                                        ))
                                        ->dateFilter($request)
                                        ->groupBy('id')->groupBy('day')->orderBy('day','desc');

        $array = [] ;
        foreach($totalPembelian->get() as $buys){
            $array[] = [
                'nopemasok'          =>     $buys->pemasok->no_pemasok,
                'namapemasok'        =>     $buys->pemasok->nama,
                'no_retur'           =>     $buys->noretur,
                'tanggal_retur'      =>     Carbon::parse($buys->tgl_retur)->format('d F Y'),
                'keterangan'         =>     $buys->catatan,
                'total'              =>     $buys->total,
            ];
        }
        $newCollection = collect($array)->groupBy('nopemasok');

        $view = [
            'title'           =>      'Laporan Pengembalian Pembelian Perpemasok',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.pengembalian_pembelian_per_pemasok')->with($view);
    }

    public function pengembalian_pembelian_per_barang(Request $request){
        $barang                 =       $this->barangReturPembelian->pluck('id');
        $totalPembelian         =       $this->returPembelian->with('barang.produk')
                                        ->select(array(
                                            DB::Raw('retur_pembelian.return_no as noretur'),
                                            DB::Raw('DATE(retur_pembelian.tanggal) tgl_retur'),
                                            DB::Raw('retur_pembelian.keterangan as catatan'),
                                            DB::Raw('sum(retur_pembelian.total) as total'), 
                                            DB::Raw('DATE(retur_pembelian.created_at) day'),
                                            'pemasok_id',
                                            'id'
                                        ))
                                        ->dateFilter($request)
                                        ->groupBy('id')->groupBy('day')->orderBy('day','desc');

        $array = [] ;
        foreach($totalPembelian->get() as $buys){
            foreach($buys->barang as $buy){
                $array[] = [
                    'nobarang'           =>     $buy->produk->no_barang,
                    'namabarang'         =>     $buy->item_deskripsi,
                    'no_retur'           =>     $buys->noretur,
                    'tanggal_retur'      =>     Carbon::parse($buys->tgl_retur)->format('d F Y'),
                    'keterangan'         =>     $buys->catatan,
                    'unit'               =>     $buy->satuan,
                    'qty'                =>     $buy->jumlah,
                    'total'              =>     $buys->total,
                ];
            }
        }
        $newCollection = collect($array)->groupBy('nobarang');

        $view = [
            'title'           =>      'Laporan Pengembalian Pembelian Perbarang',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.pengembalian_pembelian_per_barang')->with($view);
    }

    // Laporan Pembelian - Rincian Pesanan dan Pembelian
    public function permintaan_pembelian_belum_proses(Request $request){
        $barangPermintaan          =    $this->barangPermintaanPembelian->pluck('permintaan_pembelian_id');
        $permintaanPembelian       =    $this->permintaanPembelian->with('barangPermintaanPembelian.produk')->with('barangPermintaanPembelian.barangPesananPembelian')
                                        ->select(array(
                                            DB::Raw('permintaan_pembelian.request_no as reqnum'),
                                            DB::Raw('DATE(permintaan_pembelian.request_date) reqdate'),
                                            DB::Raw('permintaan_pembelian.status as stat'),
                                            DB::Raw('permintaan_pembelian.created_at as day'),
                                            'id',
                                        ))->dateFilter($request)->whereIn('id', $barangPermintaan)->groupBy('id')
                                        ->whereBetween('status', [0, 1])->orderBy('day', 'asc');

        $array = [];
        foreach($permintaanPembelian->get() as $buy => $buys){
            foreach($buys->barangPermintaanPembelian as $dataBuys){
                $array [] = [
                    'nobarang'       =>  $dataBuys->produk->no_barang,
                    'nmbarang'       =>  $dataBuys->item_deskripsi,
                    'qty'            =>  $dataBuys->jumlah,
                    'unit'           =>  $dataBuys->item_unit,
                    'qtydipesan'     =>  $dataBuys->barangPesananPembelian->sum('jumlah') ?? null,
                    'reqnum'         =>  $buys->reqnum,
                    'tanggal'        =>  $this->dateCarbon($buys->reqdate),
                    'status'         =>  $buys->stat,
                ];
            }
        }
        $newCollection  =   collect($array)->groupBy('nobarang');

        $view = [
            'title'           =>      'Laporan Permintaan Pembelian Belum Terproses',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.permintaan_pembelian_belum_proses')->with ($view);
    }

    public function pesanan_pembelian_per_pemasok_belum_proses(Request $request){
        $barangPesanan             =    $this->barangPesananPembelian->pluck('pesanan_pembelian_id');
        $pesananPembelian          =    $this->pesananPembelian->with('pemasok')->with('barang.produk')
                                        ->select(array(
                                            DB::Raw('pesanan_pembelian.po_number as ponum'),
                                            DB::Raw('DATE(pesanan_pembelian.po_date) podate'),
                                            DB::Raw('DATE(pesanan_pembelian.expected_date) exdate'),
                                            DB::Raw('pesanan_pembelian.status as stat'),
                                            DB::Raw('pesanan_pembelian.created_at as day'),
                                            'pemasok_id',
                                            'id',
                                        ))->dateFilter($request)->whereIn('id', $barangPesanan)->groupBy('id')
                                        ->whereBetween('status', [0, 1])->orderBy('day', 'asc');

        $array = [];
        foreach($pesananPembelian->get() as $buy => $buys){
            foreach($buys->barang as $dataBarang){
                $array [] = [
                    'nopemasok'       =>  $buys->pemasok->no_pemasok,
                    'nmpemasok'       =>  $buys->pemasok->nama,
                    'ponum'           =>  $buys->ponum,
                    'tanggal'         =>  $this->dateCarbon($buys->podate),
                    'tanggalpesan'    =>  $this->dateCarbon($buys->exdate),
                    'total'           =>  $dataBarang->harga * $dataBarang->jumlah,
                    'status'          =>  $buys->stat,
                ];
            }
        }
        $newCollection  =   collect($array)->groupBy('nopemasok');

        $view = [
            'title'           =>      'Laporan Pesanan Pembelian per Pemasok (Belum Terproses)',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.pesanan_pembelian_per_pemasok_belum_proses')->with($view);
    }

    public function pesanan_pembelian_per_barang(Request $request){
        $barangPesanan             =    $this->barangPesananPembelian->pluck('pesanan_pembelian_id');
        $pesananPembelian          =    $this->pesananPembelian->with('barang.produk')->with('barang.barangPenerimaanPembeliaan')
                                        ->select(array(
                                            DB::Raw('pesanan_pembelian.po_number as ponum'),
                                            DB::Raw('DATE(pesanan_pembelian.po_date) podate'),
                                            DB::Raw('DATE(pesanan_pembelian.expected_date) exdate'),
                                            DB::Raw('pesanan_pembelian.status as stat'),
                                            DB::Raw('pesanan_pembelian.created_at as day'),
                                            'pemasok_id',
                                            'id',
                                        ))->dateFilter($request)->whereIn('id', $barangPesanan)->groupBy('id')
                                        ->whereBetween('status', [0, 1])->orderBy('day', 'asc');

        $array = [];
        foreach($pesananPembelian->get() as $buy => $buys){
            foreach($buys->barang as $dataBarang){
                foreach ($dataBarang->barangPenerimaanPembeliaan as $keyBarang => $itemBarangPenerimaanPembelian) {
                    $array [] = [
                        'nobarang'        =>  $dataBarang->produk->no_barang,
                        'nmbarang'        =>  $dataBarang->item_deskripsi,
                        'ponum'           =>  $buys->ponum,
                        'tanggal'         =>  $this->dateCarbon($buys->podate),
                        'qty'             =>  $dataBarang->jumlah,
                        'qtydikirm'       =>  $itemBarangPenerimaanPembelian->jumlah ?? 0,
                        'total'           =>  $dataBarang->harga * $dataBarang->jumlah,
                        'satuan'          =>  $dataBarang->satuan,
                        'status'          =>  $buys->stat,
                    ];
                }
            }
        }
        $newCollection  =   collect($array)->groupBy('nobarang');

        $view = [
            'title'           =>      'Laporan Pesanan Pembelian per Barang',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.pesanan_pembelian_per_barang')->with($view);
    }

    public function histori_permintaan_pembelian(Request $request){
        $barangPermintaan          =    $this->barangPermintaanPembelian->pluck('permintaan_pembelian_id');
        $permintaanPembelian       =    $this->permintaanPembelian->with('barangPermintaanPembelian.produk')
                                        ->with('barangPermintaanPembelian.barangPesananPembelian.pesananPembelian.pemasok')
                                        ->with('barangPermintaanPembelian.barangPesananPembelian.barangPermintaanPembelian')
                                        ->select(array(
                                            DB::Raw('permintaan_pembelian.request_no as reqnum'),
                                            DB::Raw('DATE(permintaan_pembelian.request_date) reqdate'),
                                            DB::Raw('permintaan_pembelian.status as stat'),
                                            DB::Raw('permintaan_pembelian.created_at as day'),
                                            'id',
                                        ))->dateFilter($request)->whereIn('id', $barangPermintaan)->groupBy('id')
                                        ->whereBetween('status', [0, 1])->orderBy('day', 'asc');

        $array = [];
        foreach($permintaanPembelian->get() as $buy => $buys){
            if($buys->stat == 0){
                $stringStatus = 'Sedang Proses';
            }elseif($buys->stat == 1){
                $stringStatus = 'Menunggu';
            }elseif($buys->stat == 2){
                $stringStatus = 'Ditutup';
            }
            foreach($buys->barangPermintaanPembelian as $dataBuys){                
                $qytpesnan  = $dataBuys->barangPesananPembelian[0]->jumlah ?? 0;
                $qty        = $dataBuys->jumlah;

                $array [] = [
                    'reqnum'         =>  $buys->reqnum,
                    'tanggal'        =>  $this->dateCarbon($buys->reqdate),
                    'status'         =>  $stringStatus ?? null,
                    'nobarang'       =>  $dataBuys->produk->no_barang,
                    'qty'            =>  $dataBuys->jumlah,
                    'satuan'         =>  $dataBuys->item_unit,
                    'valqty'         =>  $qty - $qytpesnan,
                    'nopo'           =>  $dataBuys->barangPesananPembelian[0]->pesananPembelian->po_number ?? null,
                    'tglpesan'       =>  $this->dateCarbon($dataBuys->barangPesananPembelian[0]->pesananPembelian->po_date ?? null),
                    'qtypesan'       =>  $dataBuys->barangPesananPembelian[0]->jumlah ?? 0,
                    'nmpemasok'      =>  $dataBuys->barangPesananPembelian[0]->pesananPembelian->pemasok->nama ?? null,
                ];
            }
        }
        $newCollection  =   collect($array)->groupBy('reqnum');
        $view = [
            'title'           =>      'Laporan Histori Permintaan Pembelian',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
    	return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.histori_permintaan_pembelian')->with($view);
    }

    public function histori_pesanan_pembelian(Request $request){
        $barangPesanan          =   $this->barangPesananPembelian->pluck('pesanan_pembelian_id');
        $pesananPembelian       =   $this->pesananPembelian->with('barang.produk')->with('pemasok')
                                        ->with('barang.barangPenerimaanPembeliaan.penerimaanPembelian')
                                        ->select(array(
                                            DB::Raw('pesanan_pembelian.po_number as ponum'),
                                            DB::Raw('DATE(pesanan_pembelian.po_date) podate'),
                                            DB::Raw('pesanan_pembelian.status as stat'),
                                            DB::Raw('pesanan_pembelian.created_at as day'),
                                            'pemasok_id',
                                            'id',
                                        ))->dateFilter($request)->whereIn('id', $barangPesanan)
                                        ->groupBy('id')->orderBy('day', 'asc');

        $array = [];
        foreach($pesananPembelian->get() as $buy => $buys){
            if($buys->stat == 0){
                $stringStatus = 'Sedang Proses';
            }elseif($buys->stat == 1){
                $stringStatus = 'Ditutup';
            }elseif($buys->stat == 2){
                $stringStatus = 'Diterima Penuh';
            }
            foreach($buys->barang as $dataBuys){
                foreach ($dataBuys->barangPenerimaanPembeliaan as $keyDataBuys => $itemBarangPenerimaanPembelian) {
                    $qytreceipt   = $itemBarangPenerimaanPembelian->jumlah ?? 0;
                    $qty          = $dataBuys->jumlah;
                    $array [] = [
                        'ponum'          =>  $buys->ponum,
                        'tanggal'        =>  $this->dateCarbon($buys->reqdate),
                        'nmpemasok'      =>  $buys->pemasok->nama ?? null,
                        'status'         =>  $stringStatus ?? null,
                        'nobarang'       =>  $dataBuys->produk->no_barang,
                        'nmbarang'       =>  $dataBuys->item_deskripsi,
                        'qty'            =>  $dataBuys->jumlah,
                        'satuan'         =>  $dataBuys->produk->unit,
                        'valqty'         =>  $qty - $qytreceipt,
                        'receiptnum'     =>  $itemBarangPenerimaanPembelian->penerimaanPembelian->receipt_no ?? null,
                        'receipttgl'     =>  $this->dateCarbon($itemBarangPenerimaanPembelian->penerimaanPembelian->receive_date ?? null),
                        'qtyreceipt'     =>  $qytreceipt ?? 0,
                    ];
                }
            }
        }
        $newCollection  =   collect($array)->groupBy('ponum');
        $view = [
            'title'           =>      'Laporan Histori Pesanan Pembelian',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.histori_pesanan_pembelian')->with($view);
    }

    public function histori_penerimaan_barang(Request $request){
        $barangPenerimaan       =   $this->barangPenerimaanPembelian->pluck('penerimaan_pembelian_id');
        $penerimaanPembelian    =   $this->penerimaanPembelian->with('barang.produk')->with('pemasok')
                                        ->with('barang.barangFakturPembelian.fakturPembelian')
                                        ->select(array(
                                            DB::Raw('penerimaan_pembelian.receipt_no as recnum'),
                                            DB::Raw('DATE(penerimaan_pembelian.receive_date) recdate'),
                                            // DB::Raw('penerimaan_pembelian.status as stat'),
                                            DB::Raw('penerimaan_pembelian.created_at as day'),
                                            'pemasok_id',
                                            'id',
                                        ))->dateFilter($request)->whereIn('id', $barangPenerimaan)
                                        ->groupBy('id')->orderBy('day', 'asc');

        $array = [];
        foreach($penerimaanPembelian->get() as $buy => $buys){
            // if($buys->stat == 0){
            //     $stringStatus = 'Sedang Proses';
            // }elseif($buys->stat == 1){
            //     $stringStatus = 'Menunggu';
            // }elseif($buys->stat == 2){
            //     $stringStatus = 'Tertagih';
            // }
            foreach($buys->barang as $dataBuys){
                $qytfaktur    = $dataBuys->barangFakturPembelian[0]->jumlah ?? 0;
                $qty          = $dataBuys->jumlah;
                $array [] = [
                    'recnum'         =>  $buys->recnum,
                    'recdate'        =>  $this->dateCarbon($buys->reqdate),
                    'status'         =>  $stringStatus ?? null,
                    'nmpemasok'      =>  $buys->pemasok->nama ?? null,
                    'nobarang'       =>  $dataBuys->produk->no_barang,
                    'nmbarang'       =>  $dataBuys->produk->keterangan,
                    'qty'            =>  $dataBuys->jumlah,
                    'valqty'         =>  $qty - $qytfaktur,
                    'faktnum'        =>  $dataBuys->barangFakturPembelian[0]->fakturPembelian->no_faktur ?? null,
                    'fakttgl'        =>  $this->dateCarbon($dataBuys->barangFakturPembelian[0]->fakturPembelian->invoice_date ?? null),
                    'qtyfakt'        =>  $dataBuys->barangFakturPembelian[0]->jumlah ?? 0,
                ];
            }
        }
        $newCollection  =   collect($array)->groupBy('recnum');
        $view = [
            'title'        => 'Laporan Histori Penerimaan Pembelian',
            'perusahaan'   => $this->identitasPerushaan(),
            'dari_tanggal' => $this->dateCarbon($request['start-date']),
            'ke_tanggal'   => $this->dateCarbon($request['end-date']),
            'item'         => $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.histori_penerimaan_barang')->with($view);
    }

    // Laporan Pembelian - Rincian Uang Muka Faktur dan Pesanan
    public function uang_muka_faktur_pembelian(Request $request){
        $fakturUangMuka         =       $this->fakturPembelian->where('uang_muka', 0)->pluck('id');
        $transaksiUangMuka      =       $this->transaksiUangMukaPemasok->with('fakturPembelian.barang.barangPenerimaanPembelian.barangPesananPembelian.pesananPembelian')
                                        ->select(array(
                                            DB::Raw('sum(transaksi_uang_muka_pemasok.jumlah) as total'),
                                            'faktur_pembelian_id',
                                            'id',
                                        ))
                                        ->dateFilter($request)
                                        ->whereIn('faktur_pembelian_id', $fakturUangMuka)->groupBy('id');
        
        $array = [];
        foreach($transaksiUangMuka->get() as $buys){
            foreach($buys->fakturPembelian->barang as $buysUangMuka){
                $array [] = [
                    'nofak'         =>  $buys->fakturPembelian->no_faktur,
                    'nopo'          =>  $buysUangMuka->barangPenerimaanPembelian->barangPesananPembelian->pesananPembelian->po_number,
                    'tglfak'        =>  $this->dateCarbon($buys->fakturPembelian->invoice_date),
                    'tglpo'         =>  $this->dateCarbon($buysUangMuka->barangPenerimaanPembelian->barangPesananPembelian->pesananPembelian->po_date),
                    'keterangan'    =>  $buys->fakturPembelian->keterangan,
                    'total'         =>  $buys->total,
                ];
            }
        }
        
        $newCollection  = collect($array)->groupBy('nofak');
        $view = [
            'title'           =>      'Laporan Uang Muka Faktur Pembelian',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.uang_muka_faktur_pembelian')->with($view);
    }

    public function uang_muka_pesanan_pembelian(Request $request){
        $fakturUangMuka             =       $this->fakturPembelian->with('barang.barangPenerimaanPembelian.barangPesananPembelian.pesananPembelian')
                                            ->select(array(
                                                DB::Raw('faktur_pembelian.no_faktur as nofak'),
                                                DB::Raw('DATE(faktur_pembelian.invoice_date) tgl_faktur'),
                                                DB::Raw('sum(faktur_pembelian.total) as totals'),
                                                DB::Raw('faktur_pembelian.catatan as note'),
                                                'uang_muka',
                                                'id'
                                            ))->dateFilter($request)->where('uang_muka', 1)
                                            ->orderBy('created_at', 'asc');
        
        $array = [];
        foreach($fakturUangMuka->get() as $buy => $buys){
            foreach($buys->barang as $data){                
                    $array [] = [
                        'nofak'         =>    $buys->nofak,
                        'tglfak'        =>    $this->dateCarbon($buys->tgl_faktur),
                        'total'         =>    $buys->totals,
                        'catatan'       =>    $buys->note,
                    ];
            }
        }
        $newCollection    = collect($array)->groupBy('nofak');

        $view = [
            'title'           =>      'Laporan Uang Muka Pesanan Penjualan',
            'perusahaan'      =>      $this->identitasPerushaan(),
            'dari_tanggal'    =>      $this->dateCarbon($request['start-date']),
            'ke_tanggal'      =>      $this->dateCarbon($request['end-date']),
            'item'            =>      $newCollection,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_pembelian.uang_muka_pesanan_pembelian')->with($view);
    }

}
