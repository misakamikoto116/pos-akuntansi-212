<?php
namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\JurnalUmum;
use App\Modules\Akuntansi\Models\Pembayaran;
use App\Modules\Akuntansi\Models\Penerimaan;
use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LaporanKeuanganController extends Controller
{
	public function __construct(Akun $akun, Identitas $identitas, JurnalUmum $jurnalUmum, Pembayaran $pembayaran, Penerimaan $penerimaan,
							    PengirimanPenjualan $pengirimanPenjualan)
	{
		$this->akun							=	$akun;
		$this->identitas					=	$identitas;
		$this->jurnalUmum					=	$jurnalUmum;
		$this->pembayaran   				= 	$pembayaran;
		$this->penerimaan   				= 	$penerimaan;
		$this->pengirimanPenjualan          =   $pengirimanPenjualan;
	}

	public function requestDate($data)
	{
		$data->date_start === null ? $start_date = date("m-Y")  : $start_date = $data->date_start ;
		$data->date_end   === null ? $end_date   = date("m-Y")  : $end_date   = $data->date_end   ;
		$startDate 	  = $start_date;
		$endDate 	  = $end_date;
		$startDateNew = '01-'.$startDate;
		$endDateNew	  = date('d-'.$endDate);
		$start 		  = date('Y-m-d', strtotime($startDateNew));
		$end   		  = date('Y-m-t', strtotime($endDateNew));
		return [
			'awal'  => $start,
			'akhir'	=> $end
		];
	}

	/**  
	 * get akun filter by date.
	 *
	 * @param Carbon|array $tanggal
	 */
	private function getAkuns($tanggal,$data, $params_laba_rugi)
	{
		$akuns = $this->akun->laporanKeuanganFilter($data, $tanggal, $params_laba_rugi)->orderBy('kode_akun')->with('childAkun')->whereNull('parent_id')->get();

		return $akuns;
	}

	/**
	 * parse date from string or array to carbon or carbon in array.
	 *
	 * @param string|array $date
	 */
	private function parseTanggal($date)
	{
		if (is_string($date)) {
			return Carbon::parse($date);
		} elseif (is_array($date)) {
			$listMont = array_map(function ($tanggal) {
				return (null !== $tanggal && is_string($tanggal)) ? Carbon::parse($tanggal) : Carbon::now();
			}, $date);

			return [array_first($listMont)->startOfMonth(), array_last($listMont)->lastOfMonth()];
		}

		return Carbon::now();
	}

	// Laporan Keuangan - Neraca
	public function laporan_neraca(Request $request)
	{
		$data = $request->all();

		$request->validate([
			'date_start' => 'date_format:Y-m-d',
		]);
		$dates = [$this->parseTanggal($request->get('start-date'))];
		$typeFilter = [
			'single_date',
			'tampil_total',
			'tampil_induk',
			'tampil_anak',
			'tampil_saldo_nol',
			'tampil_jumlah_induk',
		];
        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_in_form_filter';
		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.neraca')->with($this->makeNeraca($dates,$data, 'Neraca (Standar)'))->with(['formFilter' => $formFilter,'typeFilter' => $typeFilter]);
	}

	public function laporan_neraca_induk_skontro(Request $request)
	{
		// $arrayKasBank    = [];
		// $akuns           = $this->getAkuns($this->parseTanggal($request->get('start-date')));
		// $arrayAllAkun    = [
		// 	'aktiva_lancar' => $this->aktivaLancar($akuns),
		// 	'aktiva_tetap'  => $this->aktivaTetap($akuns),
		// 	'kewajiban'     => $this->kewajiban($akuns),
		// 	'ekuitas'       => $this->ekuitas($akuns),
		// ];

		// // Laba Rugi
		// $laba_rugi = $this->labaRugiSum($akuns);

		// // Neraca
        // $neraca = $this->neracaStandarSum($akuns, $laba_rugi);
        // $dataArray = $data['items']->toArray();
		// $view = [
		// 	'title'                 => 'Neraca (Induk Skontro)',
		// 	'nama_perusahaan'       => $nama_perusahaan,
  //           'tanggal'               => Carbon::parse($request['start-date'])->format('d F Y'),
  //           'items'                 => $dataArray,
		// 	'items'                 => $arrayAllAkun,
		// 	'sum_aktiva_lancar'     => $neraca['sum_aktiva_lancar'],
		// 	'sum_aktiva_tetap'      => $neraca['sum_aktiva_tetap'],
		// 	'sum_kewajiban'         => $neraca['sum_kewajiban'],
		// 	'sum_ekuitas'           => $neraca['sum_ekuitas'],
		// 	'sum_all_aktiva'        => $neraca['sum_aktiva_lancar'] + $neraca['sum_aktiva_tetap'],
		// 	'laba_tahun_ini'        => $laba_rugi['laba_rugi_sebelum_pajak'],
		// 	'sum_kewajiban_ekuitas' => $neraca['sum_kewajiban'] + $neraca['sum_ekuitas'],
		// ];
		
		$nama_perusahaan = $this->identitas->first()->nama_perusahaan ?? null;
		$data = $request->all();
        $request->validate([
			'date_start' => 'date_format:Y-m-d',
		]);
		$dates = [$this->parseTanggal($request->get('start-date'))];
        
        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_in_form_filter';
		$typeFilter = [
			'single_date',
			'tampil_total',
			'tampil_induk',
			'tampil_anak',
			'tampil_saldo_nol',
			'tampil_jumlah_induk',
		];

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.neraca_induk_skontro')
            ->with(['formFilter' => $formFilter,'typeFilter' => $typeFilter])
            ->with($this->makeNeraca($dates, $data,'Neraca (Induk Skontro)'));
	}

	public function laporan_neraca_multi_periode(Request $request)
	{
		$request->date_start = $this->memecahTanggalStart($request);
		$request->date_end	 = $this->memecahTanggalEnd($request);

		$data = $request->all();
        $tanggal = $this->requestDate($request);

        $dates = $this->parseTanggal([
			$tanggal['awal'],
			$tanggal['akhir'],
		]);
        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_in_form_filter';
        $typeFilter = [
			'double_date',
			'tampil_total',
			'tampil_induk',
			'tampil_anak',
			'tampil_saldo_nol',
			'tampil_jumlah_induk',
		];
		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.neraca_multi_periode', $this->makeNeraca($dates,$data, 'Neraca (Standar)'))
            ->with(['formFilter' => $formFilter,'typeFilter' => $typeFilter]);
	}

	/**
	 * membuat nearaca baik satu priode maupun multi priode.
	 *
	 * @param array  $dates
	 * @param string $title
	 *
	 * @return array
	 */
	private function makeNeraca($dates, $data,$title, $anggaran = null, $type = null)
	{
		$params_laba_rugi = null;
		if (2 == count($dates)) {
			if ($type == 'perbandingan_bulan') {
				$bulan_akhir = $dates[1];
				$priode = $dates[0]->diffInMonths($bulan_akhir->addDay(-3));
			}else {
				$priode = $dates[0]->diffInMonths($dates[1]);
				if ($priode < 2) {
					$priode = 2;
				}
			}
		} else {
			$priode = 0;
		}

		$nama_perusahaan = $this->identitas->first()->nama_perusahaan ?? null;
		$akuns           = $this->getAkuns($dates, $data, $params_laba_rugi = null);
		$priodes         = collect();

		if ($type === 'perbandingan_bulan') {
				if ($dates[0]->format('F Y') === $dates[1]->format('F Y')) {
					$priodes->push($dates[0]->format('F Y'));
				}else {
					$priodes->push($dates[0]->format('F Y'));
					$priodes->push($dates[1]->format('F Y'));
				}
		}else {
			for ($i = 0; $i <= $priode; ++$i) {
				$priodes->push($dates[0]->copy()->addMonths($i)->lastOfMonth()->format('F Y'));
			}
		}

		$arrayAllAkun = [
			'Aktiva Lancar' => $anggaran === null ? $this->aktivaLancar($data, $akuns, $dates[0], $priode, $params_laba_rugi, $type) :
													$this->aktivaLancar($data, $akuns, $dates[0], $priode, $params_laba_rugi , $anggaran, $type),
			'Aktiva Tetap'  => $anggaran === null ? $this->aktivaTetap($data, $akuns, $dates[0], $priode, $params_laba_rugi, $type)  :
													$this->aktivaTetap($data, $akuns, $dates[0], $priode, $params_laba_rugi , $anggaran, $type) ,
			'Kewajiban'     => $anggaran === null ? $this->kewajiban($data, $akuns, $dates[0], $priode, $params_laba_rugi, $type)    :
													$this->kewajiban($data, $akuns, $dates[0], $priode, $params_laba_rugi , $anggaran, $type)   ,
			'Ekuitas'       => $anggaran === null ? $this->ekuitas($data, $akuns, $dates[0], $priode, $params_laba_rugi, $type)      :
													$this->ekuitas($data, $akuns, $dates[0], $priode, $params_laba_rugi , $anggaran, $type)     ,
		];

		$arrayAllAkun['Ekuitas'] = $arrayAllAkun['Ekuitas']['Ekuitas']['child_and_parent'];

		$arrayAllAkun['Ekuitas'] = collect($arrayAllAkun['Ekuitas']);

		$laba_rugi = $this->labaRugiSum($data, $akuns, $dates[0], $priode,$params_laba_rugi, $type);

		$laba_tahun_ini = [
				'nama_akun_parent' => 'Laba tahun ini',
				'tipe_akun_parent' => 'Ekuitas',
				'sum_akun_parent'  => $laba_rugi['laba_rugi_sebelum_pajak'],
				'akun_child'       => collect([]),
		];
		
		$arrayAllAkun['Ekuitas']->push($laba_tahun_ini);; 

		$neraca_sum       = collect($this->neracaStandarSum($arrayAllAkun, $dates[0], $priode, $type));

		$sum_neraca_group = $neraca_sum->chunk(2)->map(function ($group) {
			return [
				'title'                 => ucfirst(strtolower('Jumlah ' . $group->keys()->implode(' dan '))),
				'jumlah'                => $group->values()->reduce(function ($out, $item) {
					foreach ($item as $key => $value) {
						if (!isset($out[$key])) {
							$out[$key] = $value;
						} else {
							$out[$key] += $value;
						}
					}

					return $out;
				}, []),
			];
		});

		return [
			'title'            => $title,
			'sub_title'        => $this->generateSubTitle($priodes),
			'nama_perusahaan'  => $nama_perusahaan,
			'tanggal'          => $dates[0]->format('d F Y'),
			'items'            => collect($arrayAllAkun),
			'neraca_sum'       => $neraca_sum,
			'sum_neraca_group' => $sum_neraca_group,
			'priodes'          => $priodes,
		];
	}

	public function sumPerTipeAkun($data, $accounts, $tipeAkuns, $inOutMethod, $out, $params_laba_rugi)
	{
		$group_tipe_akun = $accounts->whereIn('tipe_akun_id',$tipeAkuns)->groupBy('tipeAkun.title');		

		return $this->calculateAccount($data, $group_tipe_akun, $inOutMethod, $out, $params_laba_rugi);
	}

	public function laporan_neraca_perbandingan_bulan(Request $request)
	{
		$data = $request->all();

		$tanggal = $this->requestDate($request);

        $dates = $this->parseTanggal([
			$tanggal['awal'],
			$tanggal['akhir'],
		]);

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_form_filter';

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.neraca_perbandingan_bulan')
            ->with(['formFilter' => $formFilter])
            ->with($this->makeNeraca($dates, $data,'Laporan Neraca Perbadingan Bulan', null, 'perbandingan_bulan'));
	}

	public function laporan_neraca_anggaran_periode(Request $request)
	{
		$tanggal = $this->requestDate($request);

        $dates = $this->parseTanggal([
			$tanggal['awal'],
			$tanggal['akhir'],
		]);
        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_form_filter';

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.neraca_anggaran_periode')
            ->with(['formFilter' => $formFilter])
            ->with($this->makeNeraca($dates, 'Neraca (Anggaran Periode)', 1));
	}

	public function laporan_neraca_perbandingan_anggaran()
	{
        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_form_filter';
		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.neraca_perbandingan_anggaran')->with(['formFilter' => $formFilter]);
	}

	public function laporan_neraca_perbandingan_anggaran_periode()
	{
        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_form_filter';
		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.neraca_perbandingan_anggaran_periode')->with(['formFilter' => $formFilter]);
	}

	public function laporan_neraca_ukuran_umum(Request $request)
	{
		$data = $request->all();

		$request->validate([
			'date_start' => 'date_format:Y-m-d',
		]);
		$dates = [$this->parseTanggal($request->get('start-date'))];

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_in_form_filter';
        $typeFilter = [
        	'single_date',
        	'tampil_total',
        	'tampil_induk',
        	'tampil_anak',
        	'tampil_saldo_nol',
        	'tampil_jumlah_induk',
        ];
		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.neraca_ukuran_umum')->with($this->makeNeraca($dates,$data, 'Neraca (Ukuran Umum)'))->with(['formFilter' => $formFilter,'typeFilter' => $typeFilter]);
	}

	public function laporan_neraca_konsolidasi()
	{
        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_form_filter';
		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.neraca_konsolidasi')->with(['formFilter' => $formFilter]);
	}

	// Laporan Keuangan - Laba Rugi
	public function laporan_laba_rugi_standar(Request $request)
	{
		$data = $request->all();

		$request->validate([
			'start_date' 	=> 'date_format:Y-m-d',
			'end_date' 		=> 'date_format:Y-m-d',
		]);

		// $dates = $this->requestDate($request);

		$dates['awal'] 	= Carbon::parse($request->start_date);
		$dates['akhir'] = Carbon::parse($request->end_date);

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_in_form_filter';
		$typeFilter = [
        	'single_date',
        	'tampil_total',
        	'tampil_induk',
        	'tampil_anak',
        	'tampil_saldo_nol',
        	'tampil_jumlah_induk',
        ];
		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laba_rugi_standar')
            ->with(['formFilter' => $formFilter,'typeFilter' => $typeFilter])
            ->with($this->makeLabaRugi($data, $dates,'Laba Rugi Standar'));
	}

	public function laporan_laba_rugi_multi_periode(Request $request)
	{
		$request->date_start = $this->memecahTanggalStart($request);
		$request->date_end	 = $this->memecahTanggalEnd($request);

		$data = $request->all();

		$tanggal = $this->requestDate($request);
        
        $dates = $this->parseTanggal([
			$tanggal['awal'],
			$tanggal['akhir'],
		]);

        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_in_form_filter';
		$typeFilter = [
			'double_date',
			'tampil_total',
			'tampil_induk',
			'tampil_anak',
			'tampil_saldo_nol',
			'tampil_jumlah_induk',
		];
		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laba_rugi_multi_periode', $this->makeLabaRugi($data, $dates, 'Laba Rugi Multi Priode'))
            ->with(['formFilter' => $formFilter,'typeFilter' => $typeFilter]);
	}

	
	public function makePriodes($dates)
	{
		if (2 == count($dates)) {
			$priode = $dates[0]->diffInMonths($dates[1]);
			if ($priode < 2) {
				$priode = 2;
			}
		} else {
			$priode = 0;
		}
		$priodes = collect();
		for ($i = 0; $i <= $priode; ++$i) {
			$priodes->push($dates[0]->copy()->addMonths($i)->lastOfMonth()->format('F Y'));
		}
		$priodes->push('Selisih');

		return compact('priode', 'priodes');
	}

	private function makeLabaRugi($data, $dates, $title, $anggaran = null)
	{
		$params_laba_rugi = null;

		if (2 == count($dates)) {
			if (array_key_exists('awal', $dates)) {
				$priode = 0;
			}else {
				$priode = $dates[0]->diffInMonths($dates[1]);
				if ($priode < 2) {
					$priode = 2;
				}
			}
		} else {
			$priode = 0;
		}

		if (array_key_exists('awal', $dates)) {
			$dates 										 = [$dates['awal'], $dates['akhir']];
			$priodes 									 = collect(['0' => 'Maret']);
			$params_laba_rugi 							 = true;
		}else {
			['priode' => $priode, 'priodes' => $priodes] = $this->makePriodes($dates);
		}

		$nama_perusahaan                             = $this->identitas->first()->nama_perusahaan ?? null;
		$akuns                                       = $this->getAkuns($dates,$data, $params_laba_rugi);

		$arrayAllAkun                                = [
			'Pendapatan'      		=> $anggaran === null ? $this->pendapatan($data ,$akuns, $dates[0], $priode, $params_laba_rugi) :
															$this->pendapatan($data ,$akuns, $dates[0], $priode, $anggaran, $params_laba_rugi),
			'Harga Pokok Penjualan' => $anggaran === null ? $this->hargaPokokPenjualan($data ,$akuns, $dates[0], $priode, $params_laba_rugi) :
															$this->hargaPokokPenjualan($data ,$akuns, $dates[0], $priode, $anggaran, $params_laba_rugi),
			'Beban Operasi'         => $anggaran === null ? $this->beban($data ,$akuns, $dates[0], $priode, $params_laba_rugi) :
															$this->beban($data ,$akuns, $dates[0], $priode, $anggaran, $params_laba_rugi),
			'Pendapatan dan Beban Lain' => [
				'pendapatan_lain'	=> $anggaran === null ? $this->pendapatanLain($data ,$akuns, $dates[0], $priode, $params_laba_rugi) :
															$this->pendapatanLain($data ,$akuns, $dates[0], $priode, $anggaran, $params_laba_rugi),
				'beban_lain'      	=> $anggaran === null ? $this->bebanLain($data ,$akuns, $dates[0], $priode, $params_laba_rugi)	:
															$this->bebanLain($data ,$akuns, $dates[0], $priode, $anggaran, $params_laba_rugi),
			],
		];

		$laba_rugi = $this->labaRugiSum($data, $akuns, $dates[0], $priode, $params_laba_rugi);

		return [
			'title'           => $title,
			'sub_title'       => $this->generateSubTitle($priodes),
			'nama_perusahaan' => $nama_perusahaan,
			'tanggal'         => $dates[0]->format('d F Y'),
			'items'           => collect($arrayAllAkun),
			'priodes'         => $priodes,
			'laba_rugi'       => $laba_rugi,
		];
	}

	public function laporan_laba_rugi_perbandingan_periode(Request $request)
	{
		$tanggal = $this->requestDate($request);

        $dates = $this->parseTanggal([
			$tanggal['awal'],
			$tanggal['akhir'],
		]);

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laba_rugi_perbandingan_periode')->with($this->makeLabaRugi($dates, 'Laporan Laba Rugi Perbandingan Periode'));
	}

	public function laporan_laba_rugi_anggaran_periode(Request $request)
	{
		$tanggal = $this->requestDate($request);

        $dates = $this->parseTanggal([
			$tanggal['awal'],
			$tanggal['akhir'],
		]);
        $formFilter = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_form_filter';
		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laba_rugi_anggaran_periode')
            ->with(['formFilter' => $formFilter])
            ->with($this->makeLabaRugi($dates, 'Laporan Laba/Rugi (Anggaran Periode)', 1));
	}

	public function laporan_laba_rugi_perbandingan_anggaran(Request $request)
	{
		

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laba_rugi_perbandingan_anggaran');
	}

	public function laporan_laba_rugi_perbandingan_anggaran_periode()
	{
		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laba_rugi_perbandingan_anggaran_periode');
	}

	public function laporan_laba_rugi_konsolidasi()
	{
		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laba_rugi_konsolidasi');
	}

	public function laporan_grafik_perbandingan_nilai_akun(Request $request)
	{
		$request['akun'] === null ? $request['akun'] = [1, 5, 10, 16, 76] : $request['akun'];
		$request['start-date'] === null ? $start_date = date("Y-m-01") : $start_date = $request['start-date'];
		$request['end-date']   === null ? $end_date   = date("Y-m-t") : $end_date   = $request['end-date'];
		$nama_perusahaan = $this->identitas->first()->nama_perusahaan ?? null;

		$data_arr = [];
        $saldo = 0; $parent_stat = 0; 
		$akun = new Akun();

		$data = $this->akun->with(['parentAkun','childAkun','tipeAkun'])
		->whereIn('id', $request['akun'])->orderBy('kode_akun')
		->get();
        $data->transform(function($item, $key) use (&$data_arr, &$saldo, &$parent_stat, $akun){
			$saldo = 0;
			$saldo = $item->transaksi->sum('nominal');
            if(count($item->childAkun) > 0){
                $parent_stat++;
                $saldo = $akun->recrusiveAkun($item->childAkun, $saldo, $parent_stat);
			}
            $data_arr = [
                'nama_akun'      => $item->nama_akun,
                'saldo'          => $saldo,
            ];
			return $data_arr;
		});
		$view = [
			'title'                 => 'Grafik Perbandingan Akun',
			'nama_perusahaan'       => $nama_perusahaan,
			'dari_tanggal'          => Carbon::parse($request['start-date'])->format('d F Y'),
			'ke_tanggal'            => Carbon::parse($request['end-date'])->format('d F Y'),
			'item'                  => $data,
		];
		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.grafik_perbandingan_nilai_akun')->with($view);
	}

	/**
	 * untuk menggenerate sub title (priode) dari laporan.
	 *
	 * @param \Illuminate\Support\Collection $priodes
	 *
	 * @return string
	 */
	private function generateSubTitle($priodes)
	{
		return 1 == $priodes->count() ? "Per tanggal {$priodes->last()}" : "Periode {$priodes->first()} sampai {$priodes->last()}";
	}

	/**
	 * calculate all account.
	 *
	 * @param array  $accounts
	 * @param array  $accountType
	 * @param string $inOut
	 * @param Carbon $priode
	 */
	private function calculateAccount($data, $accounts, $inOutMethod, $priode, $params_laba_rugi, $anggaran = null)
	{
		return $accounts->map(function ($account, $key_tipe_akun) use ($inOutMethod, $priode, $anggaran, $data, $params_laba_rugi) {
			$check_data_parent = [];
			$temp_sum_tipe_akun = [];
			foreach ($account as  $accountInType) {
				$sumAccountCurrentPriode = collect();
				$akunChild 				 = collect();
				foreach ($accountInType->childAkun as $childAkun) {
					$transaksiChild = $childAkun->transaksi->groupBy('priode');
					$sumDK = [];
					foreach ($priode as $currentPriode) {
						$sumDK[$currentPriode] = 0.0;
						$ccp = Carbon::createFromFormat('F Y', $currentPriode)->lastOfMonth();
						foreach ($transaksiChild as $priodeTransaksi => $transaksiChildValue) {
							$cpt = Carbon::createFromFormat('F Y', $priodeTransaksi)->lastOfMonth();
							$diff = $ccp->diffInDays($cpt, false);
							if ($params_laba_rugi === true) {
								if($anggaran === null){
									$inChildSum[$currentPriode] 	= $transaksiChildValue->where('status', 1)->sum('nominal');
									$outChildSum[$currentPriode] 	= $transaksiChildValue->where('status', 0)->sum('nominal');
									$sumDK[$currentPriode] 			= 'in' === $inOutMethod ? ($inChildSum[$currentPriode] - $outChildSum[$currentPriode]) : ($outChildSum[$currentPriode] - $inChildSum[$currentPriode]);
								}else{
									$inChildSum[$currentPriode] 	= $transaksiChildValue->where('status', 1)->sum('nominal');
									$outChildSum[$currentPriode] 	= $transaksiChildValue->where('status', 0)->sum('nominal');
									$sumDK[$currentPriode] 			= 'in' === $inOutMethod ? ($inChildSum[$currentPriode] - $outChildSum[$currentPriode]) : ($outChildSum[$currentPriode] - $inChildSum[$currentPriode]);
								}
							}else {
								if ($diff <= 0) {
									if($anggaran === null){
										$inChildSum[$currentPriode] 	= $transaksiChildValue->where('status', 1)->sum('nominal');
										$outChildSum[$currentPriode] 	= $transaksiChildValue->where('status', 0)->sum('nominal');
										$sumDK[$currentPriode] 			= 'in' === $inOutMethod ? ($inChildSum[$currentPriode] - $outChildSum[$currentPriode]) : ($outChildSum[$currentPriode] - $inChildSum[$currentPriode]);
									}else{
										$inChildSum[$currentPriode] 	= $transaksiChildValue->where('status', 1)->sum('nominal');
										$outChildSum[$currentPriode] 	= $transaksiChildValue->where('status', 0)->sum('nominal');
										$sumDK[$currentPriode] 			= 'in' === $inOutMethod ? ($inChildSum[$currentPriode] - $outChildSum[$currentPriode]) : ($outChildSum[$currentPriode] - $inChildSum[$currentPriode]);
									}
								}
							}
						}
						if (!$sumAccountCurrentPriode->has($currentPriode)) {
							$sumAccountCurrentPriode->put($currentPriode, collect());
						}
						$sumAccountCurrentPriode->get($currentPriode)->push($sumDK[$currentPriode]);
					}

					$checked_data_child = $this->checkDataChild($data, $childAkun, $sumDK);
					$akunChild->push($checked_data_child);
				}

				$transaksi = $accountInType->transaksi->groupBy('priode');

				$sumAkunParent = [];
				foreach ($priode as $currentPriode) {
					$ccp 							= Carbon::createFromFormat('F Y', $currentPriode)->lastOfMonth();
					$sumAkunParent[$currentPriode] 	= $sumAccountCurrentPriode->has($currentPriode) ? $sumAccountCurrentPriode->get($currentPriode)->sum() : 0.0;
					if ($transaksi->isNotEmpty()) {
						foreach ($transaksi as $priodeTransaksiMaster => $transaksiMaster) {
							$cpt = Carbon::createFromFormat('F Y', $priodeTransaksiMaster)->lastOfMonth();
							if ($params_laba_rugi === true) {
								if($anggaran === null){
									$inSum[$currentPriode]  = $transaksiMaster->where('status', 1)->sum('nominal');
									$outSum[$currentPriode] = $transaksiMaster->where('status', 0)->sum('nominal');
									$sumAkunParent[$currentPriode] += 'in' === $inOutMethod ? ($inSum[$currentPriode] - $outSum[$currentPriode]) : ($outSum[$currentPriode] - $inSum[$currentPriode]);
								}else{
									$inSum[$currentPriode] 	= $transaksiMaster->where('status', 1)->sum('nominal');
									$outSum[$currentPriode] = $transaksiMaster->where('status', 0)->sum('nominal');
									$sumAkunParent[$currentPriode] += 'in' === $inOutMethod ? ($inSum[$currentPriode] - $outSum[$currentPriode]) : ($outSum[$currentPriode] - $inSum[$currentPriode]);
								}
							}else {
								if ($ccp->diffInDays($cpt, false) <= 0) {
									if($anggaran === null){
										$inSum[$currentPriode]  = $transaksiMaster->where('status', 1)->sum('nominal');
										$outSum[$currentPriode] = $transaksiMaster->where('status', 0)->sum('nominal');
										$sumAkunParent[$currentPriode] += 'in' === $inOutMethod ? ($inSum[$currentPriode] - $outSum[$currentPriode]) : ($outSum[$currentPriode] - $inSum[$currentPriode]);
									}else{
										$inSum[$currentPriode] 	= $transaksiMaster->where('status', 1)->sum('nominal');
										$outSum[$currentPriode] = $transaksiMaster->where('status', 0)->sum('nominal');
										$sumAkunParent[$currentPriode] += 'in' === $inOutMethod ? ($inSum[$currentPriode] - $outSum[$currentPriode]) : ($outSum[$currentPriode] - $inSum[$currentPriode]);
									}
								}
							}
						}
					}

				}
				$akunChild = $akunChild->filter(function ($value)
				{
					return $value !== null;
				});

				$check_data_parent[]  = $this->checkDataParent($data, $accountInType, $sumAkunParent, $akunChild);

				$temp_sum_tipe_akun[] = $sumAkunParent;
			}

			$check_data_parent = array_filter($check_data_parent, function ($value)
			{
				return $value !== null;
			});

			$sum_tipe_akun = array_shift($temp_sum_tipe_akun);

			foreach ($temp_sum_tipe_akun as $key => $sum_priode) {
				foreach ($sum_priode as $id => $sum) {
					$sum_tipe_akun[$id] += $sum;
				}
			}

			$all_data = [
				'sum_tipe_akun'		=> $sum_tipe_akun,
				'child_and_parent'	=> $check_data_parent
			];

			return $all_data;
		});
	}

	/**
	 * generator for make output multiple priode or single priode.
	 *
	 * @param \Illuminate\Support\Collection $accounts
	 * @param array                          $tipeAkuns
	 * @param string                         $inOutMethod
	 * @param \Carbon\Carbon                 $dateStart
	 * @param string                         $priode
	 */
	private function multipleOrSingleAccount($data, $accounts, $tipeAkuns, $inOutMethod, $dateStart = null, $priode = null, $params_laba_rugi, $type, $anggaran = null)
	{
		$out = collect();
		if ($type === 'perbandingan_bulan') {
			for ($i=0; $i <= $priode; $i++) {
				$priodeSekarang = $dateStart->copy()->addMonths($i);
				$formatedPriod  = $priodeSekarang->format('F Y'); 
				if ($i == 0) {
					$out->push($formatedPriod);
				}else if($i == $priode) {
					$out->push($formatedPriod);
				}
			}
		}else if(null !== $dateStart && null !== $priode) {
			for ($i = 0; $i <= $priode; ++$i) {
				$priodeSekarang = $dateStart->copy()->addMonths($i);
				$formatedPriod  = $priodeSekarang->format('F Y');
				$out->push($formatedPriod);
			}
		}

		if($anggaran === null){
			return $this->sumPerTipeAkun($data, $accounts, $tipeAkuns, $inOutMethod, $out, $params_laba_rugi);
		}else{
			return $this->calculateAccount($data, $accounts, $tipeAkuns, $inOutMethod, $out, $anggaran);
		}
	}

	// Fungsi-fungsi
	public function aktivaLancar($data, $akuns, $dateStart = null, $priode = null, $params_laba_rugi, $type, $anggaran = null)
	{
		return $this->multipleOrSingleAccount($data, $akuns, [1, 2, 4, 5, 6], 'in', $dateStart, $priode, $params_laba_rugi, $type,$anggaran);
	}

	public function aktivaTetap($data, $akuns, $dateStart = null, $priode = null, $params_laba_rugi, $anggaran = null)
	{
		return $this->multipleOrSingleAccount($data, $akuns, [3, 7], 'in', $dateStart, $priode, $params_laba_rugi ,$anggaran);
	}

	public function kewajiban($data, $akuns, $dateStart = null, $priode = null, $params_laba_rugi, $anggaran = null)
	{
		return $this->multipleOrSingleAccount($data, $akuns, [8, 9], 'out', $dateStart, $priode, $params_laba_rugi ,$anggaran);
	}

	public function ekuitas($data, $akuns, $dateStart = null, $priode = null, $params_laba_rugi, $anggaran = null)
	{
		return $this->multipleOrSingleAccount($data, $akuns, [11], 'out', $dateStart, $priode, $params_laba_rugi ,$anggaran);
	}

	public function pendapatan($data, $akuns, $dateStart = null, $priode = null, $params_laba_rugi, $anggaran = null)
	{
		return $this->multipleOrSingleAccount($data, $akuns, [12], 'out', $dateStart, $priode, $params_laba_rugi,$anggaran);
	}

	public function hargaPokokPenjualan($data, $akuns, $dateStart = null, $priode = null, $params_laba_rugi, $anggaran = null)
	{
		return $this->multipleOrSingleAccount($data, $akuns, [13], 'in', $dateStart, $priode, $params_laba_rugi,$anggaran);
	}

	public function beban($data, $akuns, $dateStart = null, $priode = null, $params_laba_rugi, $anggaran = null)
	{
		return $this->multipleOrSingleAccount($data, $akuns, [14], 'in', $dateStart, $priode, $params_laba_rugi,$anggaran);
	}

	public function pendapatanLain($data, $akuns, $dateStart = null, $priode = null, $params_laba_rugi, $anggaran = null)
	{
		return $this->multipleOrSingleAccount($data, $akuns, [15], 'out', $dateStart, $priode, $params_laba_rugi,$anggaran);
	}

	public function bebanLain($data, $akuns, $dateStart = null, $priode = null, $params_laba_rugi, $anggaran = null)
	{
		return $this->multipleOrSingleAccount($data, $akuns, [16], 'in', $dateStart, $priode, $params_laba_rugi,$anggaran);
	}

	private function calculateSum($collection, $key, $priode)
	{
		return $collection->reduce(function ($sum, $item) use ($key, $priode) {
			$value = $item[$key][$priode] ?? 0.0;

			return $sum + $value;
		}, 0.0);
	}

	public function labaRugiSum($data, $akuns, $dateStart = null, $priode = null, $params_laba_rugi, $type = null)
	{
		$temp                 = [];
		$akun_pendapatan      = $this->pendapatan($data, $akuns, $dateStart, $priode, $params_laba_rugi);
		$akun_hpp             = $this->hargaPokokPenjualan($data, $akuns, $dateStart, $priode, $params_laba_rugi);
		$akun_beban           = $this->beban($data, $akuns, $dateStart, $priode, $params_laba_rugi);
		$akun_pendapatan_lain = $this->pendapatanLain($data, $akuns, $dateStart, $priode, $params_laba_rugi);
		$akun_beban_lain      = $this->bebanLain($data, $akuns, $dateStart, $priode, $params_laba_rugi);
		if (null !== $dateStart && null !== $priode) {
			if ($type === 'perbandingan_bulan') {
				for ($i = 0; $i <= $priode; ++$i) {
					$priodeSekarang                                       = $dateStart->copy()->addMonths($i);
					$formatedPriod                                        = $priodeSekarang->format('F Y');
					if ($i == 0) {
						$temp['sum_pendapatan'][$formatedPriod]               = $this->calculateSum($akun_pendapatan, 'sum_tipe_akun', $formatedPriod);
						$temp['sum_hpp'][$formatedPriod]                      = $this->calculateSum($akun_hpp, 'sum_tipe_akun', $formatedPriod);
						$temp['sum_beban'][$formatedPriod]                    = $this->calculateSum($akun_beban, 'sum_tipe_akun', $formatedPriod);
						$temp['sum_pendapatan_lain'][$formatedPriod]          = $this->calculateSum($akun_pendapatan_lain, 'sum_tipe_akun', $formatedPriod);
						$temp['sum_beban_lain'][$formatedPriod]               = $this->calculateSum($akun_beban_lain, 'sum_tipe_akun', $formatedPriod);
						$temp['laba_kotor'][$formatedPriod]                   = $temp['sum_pendapatan'][$formatedPriod]      - $temp['sum_hpp'][$formatedPriod];
						$temp['pendapatan_operasi'][$formatedPriod]           = $temp['laba_kotor'][$formatedPriod]          - $temp['sum_beban'][$formatedPriod];
						$temp['jumlah_pendapatan_beban_lain'][$formatedPriod] = $temp['sum_pendapatan_lain'][$formatedPriod] - $temp['sum_beban_lain'][$formatedPriod];
						$temp['laba_rugi_sebelum_pajak'][$formatedPriod]      = $temp['jumlah_pendapatan_beban_lain'][$formatedPriod] + $temp['pendapatan_operasi'][$formatedPriod];
					}else if ($i == $priode) {
						$temp['sum_pendapatan'][$formatedPriod]               = $this->calculateSum($akun_pendapatan, 'sum_tipe_akun', $formatedPriod);
						$temp['sum_hpp'][$formatedPriod]                      = $this->calculateSum($akun_hpp, 'sum_tipe_akun', $formatedPriod);
						$temp['sum_beban'][$formatedPriod]                    = $this->calculateSum($akun_beban, 'sum_tipe_akun', $formatedPriod);
						$temp['sum_pendapatan_lain'][$formatedPriod]          = $this->calculateSum($akun_pendapatan_lain, 'sum_tipe_akun', $formatedPriod);
						$temp['sum_beban_lain'][$formatedPriod]               = $this->calculateSum($akun_beban_lain, 'sum_tipe_akun', $formatedPriod);
						$temp['laba_kotor'][$formatedPriod]                   = $temp['sum_pendapatan'][$formatedPriod]      - $temp['sum_hpp'][$formatedPriod];
						$temp['pendapatan_operasi'][$formatedPriod]           = $temp['laba_kotor'][$formatedPriod]          - $temp['sum_beban'][$formatedPriod];
						$temp['jumlah_pendapatan_beban_lain'][$formatedPriod] = $temp['sum_pendapatan_lain'][$formatedPriod] - $temp['sum_beban_lain'][$formatedPriod];
						$temp['laba_rugi_sebelum_pajak'][$formatedPriod]      = $temp['jumlah_pendapatan_beban_lain'][$formatedPriod] + $temp['pendapatan_operasi'][$formatedPriod];
					}
				}
			}else {
				for ($i = 0; $i <= $priode; ++$i) {
					$priodeSekarang                                       = $dateStart->copy()->addMonths($i);
					$formatedPriod                                        = $priodeSekarang->format('F Y');
					$temp['sum_pendapatan'][$formatedPriod]               = $this->calculateSum($akun_pendapatan, 'sum_tipe_akun', $formatedPriod);
					$temp['sum_hpp'][$formatedPriod]                      = $this->calculateSum($akun_hpp, 'sum_tipe_akun', $formatedPriod);
					$temp['sum_beban'][$formatedPriod]                    = $this->calculateSum($akun_beban, 'sum_tipe_akun', $formatedPriod);
					$temp['sum_pendapatan_lain'][$formatedPriod]          = $this->calculateSum($akun_pendapatan_lain, 'sum_tipe_akun', $formatedPriod);
					$temp['sum_beban_lain'][$formatedPriod]               = $this->calculateSum($akun_beban_lain, 'sum_tipe_akun', $formatedPriod);
					$temp['laba_kotor'][$formatedPriod]                   = $temp['sum_pendapatan'][$formatedPriod]      - $temp['sum_hpp'][$formatedPriod];
					$temp['pendapatan_operasi'][$formatedPriod]           = $temp['laba_kotor'][$formatedPriod]          - $temp['sum_beban'][$formatedPriod];
					$temp['jumlah_pendapatan_beban_lain'][$formatedPriod] = $temp['sum_pendapatan_lain'][$formatedPriod] - $temp['sum_beban_lain'][$formatedPriod];
					$temp['laba_rugi_sebelum_pajak'][$formatedPriod]      = $temp['jumlah_pendapatan_beban_lain'][$formatedPriod] + $temp['pendapatan_operasi'][$formatedPriod];
				}	
			}
		}

		return $temp;
	}

	public function neracaStandarSum($arrayAllAkun, $dateStart = null, $priode = null, $type = null)
	{
		$out = [];
		if (null !== $dateStart && null !== $priode) {
			if ($type === 'perbandingan_bulan') {
				for ($i=0; $i <= $priode; $i++) {
					$priodeSekarang = $dateStart->copy()->addMonths($i);
					$formatedPriod  = $priodeSekarang->format('F Y'); 
					if ($i == 0) {
						$out['Aktiva Lancar'][$formatedPriod] = $this->calculateSum($arrayAllAkun['Aktiva Lancar'], 'sum_tipe_akun',$formatedPriod);
						$out['Aktiva Tetap'][$formatedPriod]  = $this->calculateSum($arrayAllAkun['Aktiva Tetap'], 'sum_tipe_akun',$formatedPriod);
						$out['Kewajiban'][$formatedPriod]     = $this->calculateSum($arrayAllAkun['Kewajiban'], 'sum_tipe_akun',$formatedPriod);
						$out['Ekuitas'][$formatedPriod]       = $this->calculateSum($arrayAllAkun['Ekuitas'], 'sum_akun_parent',$formatedPriod);
					}else if($i == $priode) {
						$out['Aktiva Lancar'][$formatedPriod] = $this->calculateSum($arrayAllAkun['Aktiva Lancar'], 'sum_tipe_akun',$formatedPriod);
						$out['Aktiva Tetap'][$formatedPriod]  = $this->calculateSum($arrayAllAkun['Aktiva Tetap'], 'sum_tipe_akun',$formatedPriod);
						$out['Kewajiban'][$formatedPriod]     = $this->calculateSum($arrayAllAkun['Kewajiban'], 'sum_tipe_akun',$formatedPriod);
						$out['Ekuitas'][$formatedPriod]       = $this->calculateSum($arrayAllAkun['Ekuitas'], 'sum_akun_parent',$formatedPriod);
					}
				}
			}else if(null !== $dateStart && null !== $priode) {
				for ($i = 0; $i <= $priode; ++$i) {
					$priodeSekarang = $dateStart->copy()->addMonths($i);
					$formatedPriod  = $priodeSekarang->format('F Y');

					$out['Aktiva Lancar'][$formatedPriod] = $this->calculateSum($arrayAllAkun['Aktiva Lancar'], 'sum_tipe_akun',$formatedPriod);
					$out['Aktiva Tetap'][$formatedPriod]  = $this->calculateSum($arrayAllAkun['Aktiva Tetap'], 'sum_tipe_akun',$formatedPriod);
					$out['Kewajiban'][$formatedPriod]     = $this->calculateSum($arrayAllAkun['Kewajiban'], 'sum_tipe_akun',$formatedPriod);
					$out['Ekuitas'][$formatedPriod]       = $this->calculateSum($arrayAllAkun['Ekuitas'], 'sum_akun_parent',$formatedPriod);
				}
			}
		}

		return $out;
	}

	public function nameSpaceLabaRugi()
	{
		$name_space = [
			get_class($this->jurnalUmum),
			get_class($this->pembayaran),
			get_class($this->penerimaan),
			get_class($this->pengirimanPenjualan)
		];

		return $name_space;
	}

	public function checkDataChild($data, $childAkun, $sumDK)
	{
		$filtered =  [
			'nama_akun_child' => $childAkun->nama_akun,
			'sum_akun_child'  => $sumDK,
		];
		if (count($sumDK) <= 1) {
			if (empty($data['saldo_nol'])) {
				if (reset($sumDK) != 0) {
					$filtered =  [
						'nama_akun_child' => $childAkun->nama_akun,
						'sum_akun_child'  => $sumDK,
					];
				}else {
					$filtered = null;
				}
			}
		}else {
			$tmp = array_filter($sumDK);
			if (empty($data['saldo_nol'])) {
				if (!empty($tmp)) {
					$filtered =  [
						'nama_akun_child' => $childAkun->nama_akun,
						'sum_akun_child'  => $sumDK,
					];
				}else {
					$filtered = null;
				}
			}
		}

		if (!array_key_exists('tampil_anak', $data)) {
			$filtered = null;
		}

		if (array_key_exists('tampil_total', $data)) {
			$filtered = null;
		}

		return $filtered;
	}

	public function checkDataParent($data, $accountInType, $sumAkunParent, $akunChild)
	{
		$filtered = [
			'nama_akun_parent' => $accountInType->nama_akun,
			'tipe_akun_parent' => $accountInType->tipeAkun->title,
			'sum_akun_parent'  => $sumAkunParent,
			'akun_child'       => $akunChild,
		];

		if (count($sumAkunParent) <= 1) {
			if (empty($data['saldo_nol'])) {
				if (reset($sumAkunParent) != 0) {
					$filtered =  [
						'nama_akun_parent' => $accountInType->nama_akun,
						'tipe_akun_parent' => $accountInType->tipeAkun->title,
						'sum_akun_parent'  => $sumAkunParent,
						'akun_child'       => $akunChild,
					];
				}else {
					$filtered =  [
						'sum_akun_parent'  => $sumAkunParent,
						'akun_child'       => $akunChild,
					];
				}
			}
		}else {
			$tmp = array_filter($sumAkunParent);
			if (empty($data['saldo_nol'])) {
				if (!empty($tmp)) {
					$filtered =  [
						'nama_akun_parent' => $accountInType->nama_akun,
						'tipe_akun_parent' => $accountInType->tipeAkun->title,
						'sum_akun_parent'  => $sumAkunParent,
						'akun_child'       => $akunChild,
					];
				}else {
					$filtered =  [
						'sum_akun_parent'  => $sumAkunParent,
						'akun_child'       => $akunChild,
					];
				}
			}
		}	

		if (!array_key_exists('tampil_induk', $data)) {
				$filtered =  [
					'sum_akun_parent'  => $sumAkunParent,
					'akun_child'       => $akunChild,
				];
		}

		if (array_key_exists('tampil_total', $data)) {
				$filtered =  [
					'sum_akun_parent'  => $sumAkunParent,
					'akun_child'       => $akunChild,
				];
		}	

		return $filtered;
	}

	public function memecahTanggalStart($request)
	{
		$pecah_tanggal_start 	= explode('-', $request->date_start);

		if (!empty($pecah_tanggal_start)) {
			if (strlen($pecah_tanggal_start[0]) > 3) {
				$request->date_start = $pecah_tanggal_start[1].'-'.$pecah_tanggal_start[0];
			}
		}

		return $request->date_start;
	}

	public function memecahTanggalEnd($request)
	{
		$pecah_tanggal_end 		= explode('-', $request->date_end);

		if (!empty($pecah_tanggal_end)) {
			if (strlen($pecah_tanggal_end[0]) > 3) {
				$request->date_end = $pecah_tanggal_end[1].'-'.$pecah_tanggal_end[0];
			}
		}

		return $request->date_end;
	}
}