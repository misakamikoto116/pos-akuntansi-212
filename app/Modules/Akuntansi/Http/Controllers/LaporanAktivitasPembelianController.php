<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\JasaPengiriman;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use Illuminate\Http\Request;

class LaporanAktivitasPembelianController extends Controller
{
    public function __construct(
        Akun $akun, Identitas $identitas, InformasiPemasok $informasiPemasok, JasaPengiriman $jasaPengiriman,
        KodePajak $kodePajak, Produk $produk, SyaratPembayaran $syaratPembayaran
    )
    {
        $this->akun                 =   $akun;
        $this->identitas            =   $identitas;
        $this->informasiPemasok     =   $informasiPemasok;
        $this->jasaPengiriman       =   $jasaPengiriman;
        $this->kodePajak            =   $kodePajak;
        $this->produk               =   $produk;
        $this->syaratPembayaran     =   $syaratPembayaran;
    }

    public function permintaanPembelian(Request $request)
    {
    	$items 				= $request->all();
    	$arrayDetailBarang 	= [];
    	$identitas 			= $this->identitas->first();

    	if (!empty($items['produk_id'])) {
    	$count = count($items['produk_id']);
	    	for ($i=0; $i < $count; $i++) { 

	    		// mencari no barang
	            $no_barang = $this->produk->findOrFail($items['produk_id'][$i]);
	            // Detail Barang
	    		$arrayDetailBarang[] = [
	    			'no_item'					=> $no_barang->no_barang,
		    		'item_deskripsi'			=> $items['keterangan_produk'][$i],
		    		'qty_produk'				=> $items['qty_produk'][$i],
		    		'required_date'				=> $items['required_date'][$i],
		    		'notes'						=> $items['notes'][$i],
		    		'qty_ordered'				=> $items['qty_ordered'][$i],
		    		'qty_received'				=> $items['qty_received'][$i],
	    		];
	    	}
    	}
    	$view = [
    		'items' 			=> $items,
    		'identitas'			=> $identitas,
    		'arrayDetailBarang'	=> $arrayDetailBarang,
    	];

    	return view('akuntansi::laporan.cetak_laporan.aktivitas_pembelian.cetak_permintaan_barang')->with($view);
    }

    public function pesananPembelian(Request $request)
    {
        $items = $request->all();
        // dd($items);
        $arrayDetailBarang = [];
        $kode_pajak        = [];
        $identitas         = $this->identitas->first();
        $pemasok           = $this->informasiPemasok->findOrFail($items['pemasok_id']);
        $taxable           = '';
        if (!empty($items['term_id'])) {
            $termin            = $this->syaratPembayaran->findOrFail($items['term_id'])->naration;
        }
        if (!empty($items['ship_id'])) {
            $ship              = $this->jasaPengiriman->findOrFail($items['ship_id'])->nama;
        }
        $items['taxable'] = $this->checkTaxable($items, $request);
        if (!empty($items['produk_id'])) {
        $count = count($items['produk_id']);
            for ($i=0; $i < $count; $i++) {
                // mencari kode pajak
                if (!empty($items['kode_pajak_id'][$i])) {
                    $temp = $this->kodePajak->whereIn('id', $items['kode_pajak_id'][$i])->get();
                    $kode_pajak = $temp->map(function ($item, $key) 
                    {
                        return $item->kode_pajak_formatted;
                    });
                 }
                
                // mencari no barang
                $no_barang = $this->produk->findOrFail($items['produk_id'][$i]);
                // Detail Barang
                $arrayDetailBarang[] = [
                    'no_item'                   => $no_barang->no_barang,
                    'item_deskripsi'            => $items['keterangan_produk'][$i],
                    'qty_produk'                => $items['qty_produk'][$i],
                    'unit_harga_produk'         => $items['unit_harga_produk'][$i],
                    'diskon_produk'             => $items['diskon_produk'][$i],
                    'kode_pajak'                => $kode_pajak,
                    'amount_produk'             => $items['amount_produk'][$i],
                ];
            }
        }
        $view = [
            'items'             => $items,
            'pemasok'           => $pemasok,
            'identitas'         => $identitas,
            'arrayDetailBarang' => $arrayDetailBarang,
            'termin'            => $termin ?? null,
            'ship'              => $ship ?? null,
        ];
        // dd($view);

        return view('akuntansi::laporan.cetak_laporan.aktivitas_pembelian.cetak_pesanan_pembelian')->with($view);
    }

    public function penerimaanPembelian(Request $request)
    {
        $items = $request->all();
        $arrayDetailBarang = [];
        $identitas         = $this->identitas->first();
        $pemasok           = $this->informasiPemasok->findOrFail($items['pemasok_id']);
        if (!empty($items['ship_id'])) {
            $ship              = $this->jasaPengiriman->findOrFail($items['ship_id'])->nama;
        }
        if (!empty($items['produk_id'])) {
        $count = count($items['produk_id']);
            for ($i=0; $i < $count; $i++) {
                // mencari no barang
                $no_barang = $this->produk->findOrFail($items['produk_id'][$i]);
                // Detail Barang
                $arrayDetailBarang[] = [
                    'no_item'                   => $no_barang->no_barang,
                    'item_deskripsi'            => $items['keterangan_produk'][$i],
                    'qty_produk'                => $items['qty_produk'][$i],
                    'sn'                        => $items['sn'][$i],
                ];
            }
        }
        $view = [
            'items'             => $items,
            'pemasok'           => $pemasok,
            'identitas'         => $identitas,
            'arrayDetailBarang' => $arrayDetailBarang,
            'ship'              => $ship
        ];
        // dd($view);
        return view('akuntansi::laporan.cetak_laporan.aktivitas_pembelian.cetak_penerimaan_barang')->with($view);
    }

    public function fakturPembelian(Request $request)
    {
        $items = $request->all();

        $arrayDetailBarang = [];
        $kode_pajak        = [];
        $identitas         = $this->identitas->first();
        $pemasok           = $this->informasiPemasok->findOrFail($items['pemasok_id']);
        $taxable           = '';
        if (!empty($items['term_id'])) {
            $termin            = $this->syaratPembayaran->findOrFail($items['term_id'])->naration;
        }
        if (!empty($items['ship_id'])) {
            $ship              = $this->jasaPengiriman->findOrFail($items['ship_id'])->nama;
        }
        $items['taxable'] = $this->checkTaxable($items, $request);
        if (!empty($items['produk_id'])) {
        $count = count($items['produk_id']);
            for ($i=0; $i < $count; $i++) {
                // mencari kode pajak
                if (!empty($items['kode_pajak_id'][$i])) {
                    $temp = $this->kodePajak->whereIn('id', $items['kode_pajak_id'][$i])->get();
                    $kode_pajak = $temp->map(function ($item, $key) 
                    {
                        return $item->kode_pajak_formatted;
                    });
                 }
                
                // mencari no barang
                $no_barang = $this->produk->findOrFail($items['produk_id'][$i]);
                // Detail Barang
                $arrayDetailBarang[] = [
                    'no_item'                   => $no_barang->no_barang,
                    'item_deskripsi'            => $items['keterangan_produk'][$i],
                    'qty_produk'                => $items['qty_produk'][$i],
                    'unit_harga_produk'         => $items['unit_harga_produk'][$i],
                    'diskon_produk'             => $items['diskon_produk'][$i],
                    'kode_pajak'                => $kode_pajak,
                    'amount_produk'             => $items['amount_produk'][$i],
                ];
            }
        }
        $view = [
            'items'             => $items,
            'pemasok'           => $pemasok,
            'identitas'         => $identitas,
            'arrayDetailBarang' => $arrayDetailBarang,
            'termin'            => $termin ?? null,
            'ship'              => $ship ?? null,
        ];
        // dd($view);

        return view('akuntansi::laporan.cetak_laporan.aktivitas_pembelian.cetak_faktur_pembelian')->with($view);
    }

    public function pembayaranPembelian(Request $request)
    {
        $items             = $request->all();
        $arrayFaktur       = [];
        $identitas         = $this->identitas->first();
        $pemasok         = $this->informasiPemasok->findOrFail($items['pemasok_id']);
        $akun              = null;
        if (!empty($items['akun_bank_id'])) {
            $akun = $this->akun->findOrFail($items['akun_bank_id']);
        }
        if (!empty($items['faktur_id'])) {
        $count = count($items['faktur_id']);
            for ($i=0; $i < $count; $i++) {
                // Detail Barang
                $arrayFaktur[] = [
                    'no_faktur'                 => $items['no_faktur'][$i],
                    'tanggal'                   => $items['date'][$i],
                    'due'                       => $items['due'][$i],
                    'amount'                    => $items['amount'][$i],
                    'owing'                     => $items['owing'][$i],
                    'payment_amount'            => $items['payment_amount'][$i],
                ];
            }
        }
        $view = [
            'items'             => $items,
            'pemasok'           => $pemasok,
            'identitas'         => $identitas,
            'arrayFaktur'       => $arrayFaktur,
            'akun'              => $akun
        ];
        // dd($view);
        return view('akuntansi::laporan.cetak_laporan.aktivitas_pembelian.cetak_pembayaran_pembelian')->with($view);
    }

    // Fungsi - fungsi
    public function checkTaxable($items, $request)
    {
        if ($request->has('taxable')) {
            if ($items['taxable'] == 1) {
                $items['taxable'] = 'Yes';
            }else {
                $items['taxable'] = 'No';
            }
        }else {
            $items['taxable'] = 'No';
        }
        return $items['taxable'];
    }
}
