<?php
namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Http\Requests\PenawaranPenjualanRequest;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class PenawaranPenjualanController extends Controller
{
	public function __construct(RepositoryInterface $model)
	{
		$this->role         = 'akuntansi';
		$this->model        = $model;
		$this->title        = 'Data Penawaran Penjualan';
		$this->request      = PenawaranPenjualanRequest::class;
		$this->requestField = ['no_penawaran',
			'pelanggan_id',
			'no_pelanggan',
			'alamat_asal',
			'alamat_pengiriman',
			'taxable',
			'in_tax',
			'diskon',
			'ongkir',
			'nilai_pajak',
			'total',
			'tanggal',
			'keterangan',
			'catatan',
			'keterangan_produk',
			'produk_id',
			'qty_produk',
			'satuan_produk',
			'unit_harga_produk',
			'diskon_produk',
			'tax_produk',
			'amount_produk',
			'terproses',
			'ditutup',
			'kode_pajak_id',
			'harga_modal',
			'lanjutkan',
			// Untuk keperluan cetak langsung
			'subTotal',
			'total_potongan_rupiah',
			'grandtotal',
			'tax_cetak0',
			'tax_cetak1',
		];
	}

	public function formData()
	{
		return [
			'kategori'  => $this->model->listKategoriProduk(),
			'gudang'    => $this->model->listGudang(),
			'akun'      => $this->model->listAkun(),
			'pelanggan' => $this->model->listPelanggan(),
			// 'produk' => $this->model->listProduk(),
			'kode_pajak'       => $this->model->listKodePajak(),
			'status_penjualan' => collect(['0' => 'Mengantri', '1' => 'Menunggu', '2' => 'Terproses', '3' => 'Ditutup']),
			'filter'           => ['status_penjualan', 'tanggal_dari_sampai', 'pelanggan_select','no_faktur','keterangan_pembayaran'],
			//frm session
			'idPrediction' => $this->model->idPrediction(),
			'from_session' => session()->get('penawaran_penjualan' . auth()->user()->id),
			// 'tipePelanggan' => $this->model->listTipePelanggan(),
		];
	}

	public function index()
	{
		return parent::index()->with($this->formData());
	}

	public function redirectSuccess(Request $request, $result = null)
	{
		if (!empty($result['continue_stat'])) {
			if (1 == $result['continue_stat']) {
				return redirect('/akuntansi/pesanan-penjualan/create?pelanggan_id=' . $result->pelanggan_id . '&penawaran_id=' . $result->id)
			->withMessage('Berhasil Menambah/Memperbarui data');
			} else {
				return redirect('/akuntansi/penawaran-penjualan/create')->withMessage('Berhasil Menambah/Memperbarui data');
			}
		} else {
			return redirect('/akuntansi/penawaran-penjualan')->withMessage('Berhasil Menambah/Memperbarui data');
		}
	}
}
