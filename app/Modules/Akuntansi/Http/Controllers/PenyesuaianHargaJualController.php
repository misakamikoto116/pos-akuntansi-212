<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Models\PenyesuaianHargaJual;
use App\Modules\Akuntansi\Http\Requests\PenyesuaianHargaJualRequest;
use App\Modules\Akuntansi\Repositories\PenyesuaianHargaJualRepository;
use Generator\Interfaces\RepositoryInterface;
class PenyesuaianHargaJualController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Penyesuaian Harga Jual';
        $this->request = PenyesuaianHargaJualRequest::class;
        $this->requestField = [
            'no_penyesuaian',
            'tgl_efektif',
            'tgl_penyesuaian',
            'keterangan',
            'produk_id',
        ];
    }
    public function formData()
    {
        return [
            'produk' => $this->model->listProduk(),
        ];
    }
}
