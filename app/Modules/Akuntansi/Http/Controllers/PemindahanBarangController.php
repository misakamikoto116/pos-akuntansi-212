<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\PemindahanBarangRequest;
use App\Modules\Akuntansi\Repositories\PemindahanBarangRepository;
use Generator\Interfaces\RepositoryInterface;
class PemindahanBarangController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Pemindahan Barang';
        $this->request = PemindahanBarangRequest::class;
        $this->requestField = [
            'no_transfer',
            'tanggal',
            'dari_gudang_id',
            'alamat_dari_gudang',
            'ke_gudang_id',
            'alamat_ke_gudang',
            'produk_id',
            'keterangan',
            'keterangan_produk',
            'jumlah',
            'satuan',
            'serial_number',
        ];
    }
    public function formData()
    {
        return [
            'gudang' => $this->model->listGudang(),
            'produk' => $this->model->listProduk(),
            'filter'     => [
                                'no_transfer',
                                'pindah_dari',
                                'pindah_ke',
                                'tanggal_dari_sampai',
                            ],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }
}
