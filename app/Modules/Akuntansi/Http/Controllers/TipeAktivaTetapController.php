<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\TipeAktivaTetapRequest;
use App\Modules\Akuntansi\Repositories\TipeAktivaTetapRepository;
use Generator\Interfaces\RepositoryInterface;
class TipeAktivaTetapController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Tipe Aktiva Tetap';
        $this->request = TipeAktivaTetapRequest::class;
        $this->requestField = ['tipe_aktiva_tetap','tipe_aktiva_tetap_pajak_id'];
    }
    public function formData()
    {
        return [
            'listTipeAktivaTetapPajak' => $this->model->listTipeAktivaTetapPajak(),
        ];
    }
}
