<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use Generator\Interfaces\RepositoryInterface;
use App\Modules\Akuntansi\Http\Requests\PreferensiRequest;

class PreferensiController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Preferensi';
        $this->request = PreferensiRequest::class;
    }

    // public function searchData()
    // {
    //     return [
    //         'akun' => $this->model->listAkun()
    //     ];
    // }

    public function index()
    {
        $var = parent::index()->with([
                    'akun'          => $this->model->listAkun(),
                    'dataMataUang'  => $this->model->dataMataUang(),
                    'dataBarang'    => $this->model->dataBarang(),
               ]);
        return $var;
    }
}
