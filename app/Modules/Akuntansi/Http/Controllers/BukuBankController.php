<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Pembayaran;
use App\Modules\Akuntansi\Models\Penerimaan;
use App\Modules\Akuntansi\Models\Transaksi;
use Carbon\Carbon;

class BukuBankController extends Controller
{
	public function __construct(Akun $akun, Penerimaan $penerimaan, Pembayaran $pembayaran, Transaksi $transaksi)
	{
        $this->akun = $akun;
        $this->penerimaan = $penerimaan;
		$this->pembayaran = $pembayaran;
        $this->transaksi = $transaksi;
	}
	public function index()
	{        
		$listAkun = $this->akun->whereDoesntHave('childAkun')->whereHas('tipeAkun', function ($query)
        {
            $query->where('title','Kas/Bank');
        })->where('nama_akun','!=','Kas & Bank')->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);

        
    	return view('akuntansi::buku_bank.index', with([
    		'listAkun' => $listAkun
    	]));
	}

    public function getBukuBank(Request $request)
    {
        $tanggal_dari = Carbon::parse($request->get('tanggal_dari'))->format('Y-m-d H:i:s');
        $tanggal_sampai = Carbon::parse($request->get('tanggal_sampai'))->format('Y-m-d H:i:s');
        $akun = $this->akun->where('id', $request->id)->pluck('money_function')->first();

        $semua_transaksi = $this->transaksi->whereHas('akun', function ($query) use ($request, $tanggal_dari, $tanggal_sampai)
        {
            $query->where('id', $request->id);
        })->whereBetween('tanggal', [$tanggal_dari, $tanggal_sampai])->get()->sortBy('tanggal');
        
        $view = view('akuntansi::buku_bank.content', compact('semua_transaksi','akun'))->render();
        return response()->json($view);
    }
}
