<?php
/**
 * User: efendihariyadi
 * Date: 30/08/18
 * Time: 12.03.
 */
namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Config;
use App\Modules\Akuntansi\Models\PreferensiBarang;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

trait ExportImportBarang
{
	public function getUploadProduk(Request $request)
	{
		$view = [
			'title'          => 'Data Pemberitahuan',
			'title_document' => 'Upload Produk',
			'data'           => [
				'fileFormatUrl' => asset('assets/sample-export-produk-master-form.xlsx'),
			],
		];
		//app('debugbar')->info(asset('assets/export-produkmaster.csv'));
		return view('akuntansi::barang.upload-produk')->with($view);
	}

	public function postUploadProduk(Request $request)
	{
		if ($request->hasFile('import_file')) {
			$file     = $request->file('import_file');
			// $filePath = $file->getRealPath();
			$destinationPath = storage_path('');
			$file->move($destinationPath, $file->getClientOriginalName());
			$original_name           = $file->getClientOriginalName();
			// $destinationPath = storage_path('');
			// $file->move($destinationPath, $file->getClientOriginalName());

			// Jika menggunankan fungsi ini oversized memory, waktu eksekusi time out habis dluan
			// ini_set('max_execution_time', 300);
			// Artisan::call('import:produk', [
			//     'nama' => $file->getClientOriginalName()
			// ]);

			// native php exec command

			exec('echo ' . base_path('artisan import:produk ' . $original_name) . ' --env=' . env('APP_ENV') . ' >> ' . storage_path('logs/import.log'));
			$message = exec('php ' . base_path('artisan import:produk ' . $original_name) . ' --env=' . env('APP_ENV') . ' >> ' . storage_path('logs/import.log'));
		}

		return redirect()->route('akuntansi.get.produk-upload')
			->withMessage('Berhasil import data' . $message);
	}

	public function postUploadProdukByAkun(Request $request, $type)
	{
		if ($request->hasFile('import_file')) {
			$file            = $request->file('import_file');
			$destinationPath = storage_path('');
			$file->move($destinationPath, $file->getClientOriginalName());

			// Jika menggunankan fungsi ini oversized memory, waktu eksekusi time out habis dluan
			//ini_set('max_execution_time', 300);
			// Artisan::call('import:produk', [
			//     'nama' => $file->getClientOriginalName()
			// ]);

			// Akun Akun
			$akun_persedian_id       = $request->get('akun_persediaan_id');
			$akun_penjualan_id       = $request->get('akun_penjualan_id');
			$akun_ret_penjualan_id   = $request->get('akun_retur_penjualan_id');
			$akun_disk_penjualan_id  = $request->get('akun_diskon_barang_id');
			$akun_barang_terkirim_id = $request->get('akun_barang_terkirim_id');
			$akun_hpp_id             = $request->get('akun_hpp_id');
			$akun_ret_pembelian_id   = $request->get('akun_retur_pembelian_id');
			$akun_beban_id           = $request->get('akun_beban_id');
			$akun_belum_tertagih_id  = $request->get('akun_belum_tertagih_id');
			$base_log                = base_path('storage/logs/import.log');
			$base_command            = base_path('artisan import_produk_by_akun:nama ');
			$original_name           = $file->getClientOriginalName();

			// native php exec command
			$message = exec("php $base_command $original_name $akun_persedian_id $akun_penjualan_id $akun_ret_penjualan_id $akun_disk_penjualan_id $akun_barang_terkirim_id $akun_hpp_id $akun_ret_pembelian_id $akun_beban_id $akun_belum_tertagih_id  > $base_log");
		}

		if (0 == $type) {
			// $save_config = $this->saveConfig();

			return redirect('/login');
		} else {
			return redirect()->route('akuntansi.get.produk-upload-by-akun')
			->withMessage('Berhasil import data' . $message);
		}
	}

	public function getUploadProdukByAkun(Request $request)
	{
		$view = [
			'title'             => 'Data Pemberitahuan',
			'title_document'    => 'Upload Produk By Akun',
			'akun'              => $this->listAkun(),
			'preferensi_barang' => $this->listPreferensiBarang(),
			'data'              => [
				'fileFormatUrl' => asset('assets/export-produkmaster-without-akun.xlsx'),
			],
		];

		return view('akuntansi::barang.upload_produk_by_akun')->with($view);
	}

	/**
	 * @param $csvFile File
	 * @param string $separator
	 *
	 * @return array
	 */
	protected function readCsv($csvFile, $separator = ',')
	{
		$file   = fopen($csvFile, 'r');
		$data   = [];
		$topRow = true;
		while (false !== ($getData = fgetcsv($file, 10000, $separator))) {
			if ($topRow) {
				$topRow = false;
				continue;
			}
			$data[] = $getData;
		}
		fclose($file);

		return $data;
	}

	public function listAkun()
	{
		return Akun::whereDoesntHave('childAkun')->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama_akun . ' - ' . $item->kode_akun;

			return $output;
		}, []);
	}

	public function listPreferensiBarang()
	{
		return PreferensiBarang::firstOrFail();
	}

	// PROSES SAVE CONFIG
	public function saveConfig()
	{
		$config = Config::create([
			'key'       => 'installed',
			'value'     => true,
			'deskripsi' => 'Get Started Telah Terselesaikan',
		]);

		return $config;
	}
}
