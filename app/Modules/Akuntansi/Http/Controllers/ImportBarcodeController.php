<?php
/**
 * User: efendihariyadi
 * Date: 30/08/18
 * Time: 12.03
 */

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Models\Config;
use Excel;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
class ImportBarcodeController extends Controller
{
    public function postUploadBarcode(Request $request)
    {

        if($request->hasFile('import_file')){

            $file = $request->file('import_file');
            $destinationPath = storage_path('');
            $file->move($destinationPath,$file->getClientOriginalName());

            // Jika menggunankan fungsi ini oversized memory, waktu eksekusi time out habis dluan 
            //ini_set('max_execution_time', 300);
            // Artisan::call('import:produk', [
            //     'nama' => $file->getClientOriginalName()
            // ]);

            // Akun Akun
            $base_log                = base_path("storage/logs/import.log");
            $base_command            = base_path("artisan import_barcode:nama "); 
            $original_name           = $file->getClientOriginalName();

            // native php exec command
            $message = exec("php $base_command $original_name > $base_log");

        }

        return redirect()->route("akuntansi.get.import-barcode")
        ->withMessage("Berhasil import data". $message);    
    }


    public function getUploadBarcode(Request $request)
    {
        $view = [
            'title'             => 'Data Pemberitahuan',
            'title_document'    => 'Upload Barcode',
            'data'              => [
                                    'fileFormatUrl' => asset('assets/export-syaratpembayarannmaster.xlsx')
                                   ],
        ];

        return view('akuntansi::barcode.upload_barcode')->with($view);
    }


    /**
     * @param $csvFile File
     * @param string $separator
     * @return array
     */
    protected function readCsv($csvFile, $separator = ',')
    {
        $file = fopen($csvFile, "r");
        $data = [];
        $topRow = true;
        while (($getData = fgetcsv($file, 10000, $separator)) !== FALSE)
        {
            if($topRow) {
                $topRow = false;
                continue;
            }
            $data[] = $getData;
        }
        fclose($file);
        return $data;
    }
}