<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\PenerimaanBarangRequest;
use App\Modules\Akuntansi\Repositories\PenerimaanBarangRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class PenerimaanBarangController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Penerimaan Barang';
        $this->request = PenerimaanBarangRequest::class;
        $this->requestField = [
            "no_pemasok",
            "pemasok_id",
            "pesanan",
            "alamat_pengiriman",
            "receipt_no",
            "receive_date",
            "form_no",
            "fob",
            "ship_date",
            "ship_id",
            "produk_id",
            "keterangan_produk",
            "qty_produk",
            "satuan_produk",
            "gudang_id",
            "sn",
            "keterangan",
            "total_produk", 
            "harga_modal", 
            'harga_terakhir',
            'unit_price',
            'diskon_produk',  
            "barang_pesanan_pembelian_id", 
            'lanjutkan',
        ];
    }
    public function formData()
    {
        return [
            'gudang' => $this->model->listGudang(),
            'pemasok' => $this->model->listPemasok(),
            // 'produk' => $this->model->listProduk(),
            'pengiriman' => $this->model->listJasaPengiriman(),
            'idPrediction'   => $this->model->idPrediction(),       
            'filter' => ['tanggal_dari_sampai', 'pemasok_select','no_faktur','keterangan_pembayaran'],        
            // 'kategori' => $this->model->listKategoriProduk(),
            // 'akun' => $this->model->listAkun(),
            // 'termin' => $this->model->listTermin(),
            // 'tipePemasok' => $this->model->listTipePemasok(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function redirectSuccess(Request $request, $result = null)
    {
        if(!empty($result['continue_stat'])){
            if($result['continue_stat'] == 1){
                return redirect('/akuntansi/faktur-pembelian/create?pemasok_id='.$result->pemasok_id.'&penerimaan_id='.$result->id)
                ->withMessage('Berhasil Menambah/Memperbarui data');
            }else{
                return redirect('/akuntansi/penerimaan-barang/create')->withMessage('Berhasil Menambah/Memperbarui data');;
            }
        }else{
            return redirect('/akuntansi/penerimaan-barang')->withMessage('Berhasil Menambah/Memperbarui data');;
        }
    }
}
