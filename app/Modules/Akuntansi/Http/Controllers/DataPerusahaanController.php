<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Models\User;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class DataPerusahaanController extends Controller
{
	public function showPassword()
	{
		$show = User::all()->last();
		$title = 'Data Perusahaan';
		return view('user/edit',compact('show','title'));

	}
	public function editPassword(Request $request)
	{
		$title = 'Data Perusahaan';
		$show = User::all()->last();
		$show->email = $request->email;
		$show->password = bcrypt($request->passwordbaru);
		$show->save();
		return view('user/edit',compact('show','title'));
	}
	public function validatePasslama(Request $request)
	{
		$a = $request->passwordlama;
		$b = User::all()->last()->password;
		if (Hash::check($a, $b)) {
			return response()->json('benar');
		}else{
			return response()->json('salah');
		}
	}
}
