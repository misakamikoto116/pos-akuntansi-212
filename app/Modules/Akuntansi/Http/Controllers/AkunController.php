<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Http\Requests\AkunRequest;
use App\Modules\Akuntansi\Models\Transaksi;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class AkunController extends Controller
{
    use ExportImportAkun;
    public function __construct(RepositoryInterface $model, Akun $akun)
    {
        $this->akun = $akun;
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Akun';
        $this->request = AkunRequest::class;
        $this->requestField = [
                'kode_akun',
                'nama_akun',
                'tipe_akun_id',
                'tipe_akun_nama',
                'parent_id',
                'mata_uang_id',
                'fiksal',
                'alokasi_ke_produksi',
                'money_function',
                'tanggal_masuk',
                'use_parent'
        ];
    }

    public function formData()
    {
        return [
            'listTipeAkun' => $this->model->listTipeAkun(),
            'tipe_akun' => $this->model->listTipeAkun(),
            // 'listAkun' => $this->model->listAkun(),
            'listMataUang' => $this->model->listMataUang(),
            'permission' => [
                'buat' => 'buat_daftar_akun',
                'ubah' => 'ubah_daftar_akun',
                'hapus' => 'hapus_daftar_akun',
                'laporan' => 'laporan_daftar_akun',
            ],
            'filter' => ['no_akun', 'nama_akun', 'tipe_akun_id'],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function embedDataToShow($id)
    {
        $data['transaksi'] = Transaksi::where('akun_id',$id)->paginate(10);
        return $data;   
    }

    public function getBukuBank(Request $request)
    {
        if ($request->ajax()) {
            $request['akun'] = $request['id'];
            unset($request['id']);
        }else{
            if (empty($request->all()) || $request['akun'] === null || array_key_exists('akun', $request->all()) === false) {
                return abort(404);
            }
            $request['tanggal_dari'] = date('Y-m-1');
            $request['tanggal_sampai'] = date('Y-m-d');   
        }
        return $this->model->bukuBank($request);
    }

    public function destroy($id)
    {
        $model = $this->akun->findOrFail($id);

        $html  = '';

        if ($model->transaksi->count() > 0) {
            $html = 'Akun tidak dapat dihapus, karena masih memiliki Transaksi. ';
            session()->flash('error_message', $html); 
            session()->flash('icon', 'error');

            return redirect()->back();

        }else if ($model->childAkun->count() > 0) {
            $html = 'Akun tidak dapat dihapus, karena telah digunakan oleh Akun lain. ';
            session()->flash('error_message', $html); 
            session()->flash('icon', 'error');

            return redirect()->back();

        }else {
            $this->model->delete($id);
            return redirect()->back()->withMessage('Berhasil Menambah/Memperbarui data');            
        }

    }
}
