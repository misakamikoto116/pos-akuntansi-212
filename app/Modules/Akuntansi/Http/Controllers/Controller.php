<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use  Generator\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
   protected $viewNamespace = 'akuntansi';
}