<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\BarangFakturPembelian;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\BarangPenawaranPenjualan;
use App\Modules\Akuntansi\Models\BarangPenerimaanPembelian;
use App\Modules\Akuntansi\Models\BarangPengirimanPenjualan;
use App\Modules\Akuntansi\Models\BarangPermintaanPembelian;
use App\Modules\Akuntansi\Models\BarangPesananPembelian;
use App\Modules\Akuntansi\Models\BarangPesananPenjualan;
use App\Modules\Akuntansi\Models\BarangReturPembelian;
use App\Modules\Akuntansi\Models\BarangReturPenjualan;
use App\Modules\Akuntansi\Models\BebanFakturPembelian;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\HistoryRekonsiliasi;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\KategoriProduk;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\PembiayaanPesanan;
use App\Modules\Akuntansi\Models\PenawaranPenjualan;
use App\Modules\Akuntansi\Models\PenerimaanPembelian;
use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use App\Modules\Akuntansi\Models\PermintaanPembelian;
use App\Modules\Akuntansi\Models\PesananPembelian;
use App\Modules\Akuntansi\Models\PesananPenjualan;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\ReturPembelian;
use App\Modules\Akuntansi\Models\ReturPenjualan;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use App\Modules\Akuntansi\Models\TipeAktivaTetapPajak;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\TransaksiUangMuka;
use App\Modules\Akuntansi\Models\TransaksiUangMukaPemasok;
use App\Modules\Akuntansi\Models\JasaPengiriman;
use Carbon\Carbon;
use Cart;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function __construct(PreferensiMataUang $preferensi_mata_uang, SaldoAwalBarang $saldoAwalBarang, FakturPenjualan $fakturPenjualan, BarangFakturPenjualan $barangFakturPenjualan, FakturPembelian $fakturPembelian, BarangFakturPembelian $barangFakturPembelian, JasaPengiriman $jasaPengiriman, SyaratPembayaran $syaratPembayaran)
    {
        $this->preferensi_mata_uang = $preferensi_mata_uang;
        $this->saldoAwalBarang = $saldoAwalBarang;
        $this->fakturPenjualan = $fakturPenjualan;
        $this->barangFakturPenjualan = $barangFakturPenjualan;
        $this->fakturPembelian = $fakturPembelian;
        $this->barangFakturPembelian = $barangFakturPembelian;
        $this->jasaPengiriman = $jasaPengiriman;
        $this->syaratPembayaran = $syaratPembayaran;
    }

    public function master(Request $request)
    {
        if ($request->has('akun_id')) {
            $request->session()->put('akun_id', $request->akun_id);
        }
        if ($request->has('nama_akun')) {
            $request->session()->put('nama_akun', $request->nama_akun);
        }
        if ($request->has('keterangan')) {
            $request->session()->put('keterangan', $request->keterangan);
        }
        if ($request->has('no_faktur')) {
            $request->session()->put('no_faktur', $request->no_faktur);
        }
        if ($request->has('no_cek')) {
            $request->session()->put('no_cek', $request->no_cek);
        }
        if ($request->has('tanggal')) {
            $request->session()->put('tanggal', $request->tanggal);
        }
        if ($request->has('peruntukan')) {
            $request->session()->put('peruntukan', $request->peruntukan);
        }
        if ($request->has('nominal')) {
            $request->session()->put('nominal', $request->nominal);
        }
        if ($request->has('terbilang')) {
            $request->session()->put('terbilang', $request->terbilang);
        }
    }

    public function masterJurnalUmum(Request $request)
    {
        if ($request->has('description_jurmum')) {
            $request->session()->put('description_jurmum', $request->description_jurmum);
        }
        if ($request->has('no_faktur_jurmum')) {
            $request->session()->put('no_faktur_jurmum', $request->no_faktur_jurmum);
        }
        if ($request->has('tanggal_jurmum')) {
            $request->session()->put('tanggal_jurmum', $request->tanggal_jurmum);
        }
    }

    public function getAkun(Request $request)
    {
        $akun_id = $request->get('id');
        if (null !== $akun_id) {
            $akun = Akun::where('id', $akun_id)->first();

            $response = [
                'id' => $akun->id,
                'kode_akun' => $akun->kode_akun,
                'nama_akun' => $akun->nama_akun,
            ];

            return response()->json($response);
        } else {
            $response = [
                'kode_akun' => '',
            ];

            return response()->json($response);
        }
    }

    public function getAktiva(Request $request)
    {
        $aktiva_tetap = $request->get('id');
        if (null !== $aktiva_tetap) {
            $aktiva = TipeAktivaTetapPajak::where('id', $aktiva_tetap)->first();

            $response = [
                'metode' => $aktiva->metodePenyusutan->nama,
                'estimasi_umur' => $aktiva->estimasi_umur,
                'tarif_penyusutan' => $aktiva->tarif_penyusutan,
            ];

            return response()->json($response);
        } else {
            $response = [
                'metode' => '',
                'estimasi_umur' => '',
                'tarif_penyusutan' => '',
            ];

            return response()->json($response);
        }
    }

    /*generate kode berdasarkan akun */
    public function getAkunKode(Request $request)
    {
        $akun_id = $request->get('id');

        if (null !== $akun_id) {
            $akun = Akun::where('id', $akun_id)->first();

            $response = [
                'kode_akun' => $akun->kode_akun,
            ];

            return response()->json($response);
        } else {
            $response = [
                'kode_akun' => '',
            ];

            return response()->json($response);
        }
    }

    //get akun berdasarkan tipe
    public function getAkunTipe(Request $request)
    {
        $tipe_akun_id = $request->get('id');
        $akun_id      = $request->get('akun_id');

        // return response()->json($tipe_akun_id);
        if (null !== $tipe_akun_id) {
            if (!empty($akun_id)) {

                $akun = Akun::where('id','!=', $akun_id)->whereHas('tipeAkun', function ($query) use ($tipe_akun_id) {
                    $query->where('id', $tipe_akun_id);
                })->get()->reduce(function ($output, $item) {
                    $output[$item->kode_akun][$item->id] = $item->nama_akun;

                    return $output;
                }, []);

            }else {

                $akun = Akun::whereHas('tipeAkun', function ($query) use ($tipe_akun_id) {
                    $query->where('id', $tipe_akun_id);
                })->get()->reduce(function ($output, $item) {
                    $output[$item->kode_akun][$item->id] = $item->nama_akun;

                    return $output;
                }, []);

            }

            $response = [
                'listAkun' => $akun,
            ];

            return response()->json($response);
        } else {
            $response = [
                'kode_akun' => '',
            ];

            return response()->json($response);
        }
    }

    public function getAkunInCart(Request $request)
    {
        $akun_id = $request->get('id');
        if (null !== $akun_id) {
            $akun = Akun::where('nama_akun', $akun_id)->first();

            $response = [
                'kode_akun' => $akun->kode_akun,
            ];

            return response()->json($response);
        } else {
            $response = [
                'kode_akun' => '',
            ];

            return response()->json($response);
        }
    }

    public function validateNominal(Request $request)
    {
        $type = null === $request->type ? 'default' : $request->type;
        $cart = str_replace(['Rp ', '.00', ','], '', Cart::instance($type)->total());
        $nominal = str_replace(['Rp ', '.00', ','], '', $request->nominal);
        if ($cart == $nominal) {
            return response()->json(['success' => 'Apakah Anda Yakin Ingin Menyimpan?']);
        }else if($cart > $nominal){
            return response()->json(['error' => 'Total '. ucfirst($type) .' Tidak Boleh Lebih dari Jumlah Nominal']);
        }else if($cart < $nominal){
            return response()->json(['error' => 'Total '. ucfirst($type) .' Todak Boleh Kurang dari Jumlah Nominal']);
        }
    }

    public function validateNominalJurnalUmum(Request $request)
    {
        $type = null === $request->type ? 'default' : $request->type;
        $test = cart::instance($type)->content();
        $kredit = 0;
        $debet = 0;
        foreach ($test as $key => $value) {
            $kredit += $value->options->kredit;
            $debet += $value->options->debet;
        }
        // $kredit = str_replace(["Rp ",".00",","],"", $request->kredit);
        // $nominal = str_replace(["Rp ",".00",","],"", $request->debet);
        if ($kredit == $debet) {
            return response()->json(['success' => 'Apakah Anda Yakin Ingin Menyimpan?']);
        }

        return response()->json(['error' => 'Nominal Tidak Seimbang']);
    }

    public function getTotal(Request $request)
    {
        try {
            $type = null === $request->type ? 'default' : $request->type;
            $total = str_replace(['Rp ', '.00', ','], '', Cart::instance($type)->total());

            $response = [
                'success' => true,
                'data' => [
                    'total' => $total,
                ],
            ];
        } catch (Exception $e) {
            $response = [
                'success' => false,
                'data' => [],
            ];
        }

        return response()->json($response);
    }

    public function getAlamatPelanggan(Request $request)
    {
        ini_set('memory_limit', '4095M'); 
        $pelanggan_id = $request->get('id');
        $type = $request->get('type');
        if (null !== $pelanggan_id) {
            if ($type === 'edit') {
                $pelanggan = InformasiPelanggan::where('id', $pelanggan_id)->first();
                $penawaran = PenawaranPenjualan::with('barang')->where(['pelanggan_id' => $pelanggan_id])->get();
                $pesanan = PesananPenjualan::with('barang')->where('pelanggan_id', $pelanggan_id)->get();
                $pengiriman = PengirimanPenjualan::where('pelanggan_id', $pelanggan_id)->get();
                $uang_muka = FakturPenjualan::where('pelanggan_id', $pelanggan->id)->where('uang_muka', 1)->get();
                $fakturPelanggan = FakturPenjualan::with('barang', 'invoice', 'transaksiUangMuka')->where('pelanggan_id', $pelanggan_id)->where('uang_muka', '!=', 1)->get();
            } else {
                $pelanggan = InformasiPelanggan::where('id', $pelanggan_id)->first();
                $penawaran = PenawaranPenjualan::availableOnly()->with('barang')->where(['pelanggan_id' => $pelanggan_id])->get();
                $pesanan = PesananPenjualan::availableOnly()->with('barang')->where('pelanggan_id', $pelanggan_id)->get();
                $pengiriman = PengirimanPenjualan::where('pelanggan_id', $pelanggan_id)->get();
                $uang_muka = FakturPenjualan::availableOnly()->where('pelanggan_id', $pelanggan->id)->where('uang_muka', 1)->get();
                $fakturPelanggan = FakturPenjualan::with('barang', 'invoice', 'transaksiUangMuka')->where('pelanggan_id', $pelanggan_id)->where('uang_muka', '!=', 1)->get();
            }
            $response = [
                'alamat' => $pelanggan->alamat,
                'no_pelanggan' => $pelanggan->no_pelanggan,
                'penawaran' => $penawaran,
                'pesanan' => $pesanan,
                'pengiriman' => $pengiriman,
                'uang_muka' => $uang_muka,
                'fakturPelanggan' => $fakturPelanggan,
            ];
            ini_set('memory_limit', '512M'); 
            return response()->json($response);
        } else {
            $response = [
                'kode_pelanggan' => '',
                'code' => '404',
            ];
            ini_set('memory_limit', '512M'); 
            return response()->json($response);
        }
    }

    public function getAlamatPelangganKirimKe(Request $request)
    {
        $pelanggan_id = $request->get('id');
        if (null !== $pelanggan_id) {
            $pelanggan = InformasiPelanggan::where('id', $pelanggan_id)->first();
            $response = [
                'alamat' => $pelanggan->alamat,
            ];

            return response()->json($response);
        } else {
            $response = [
                'kode_pelanggan' => '',
            ];

            return response()->json($response);
        }
    }

    public function getProduk(Request $request)
    {
        $pelanggan_id = $request->get('pelanggan_id');
        $pemasok_id = $request->get('pemasok_id');
        $produk_id = $request->get('id');
        $barang_faktur_id = $request->get('barang_faktur_id');
        $saldoAwalBarang = new SaldoAwalBarang();
        if (null !== $produk_id) {
            $data = [];
            $informasi_pelanggan = InformasiPelanggan::where('id', $pelanggan_id)->first();
            $produk = Produk::with('tingkatHargaBarang')->where('id', $produk_id)->first();
            // $sum_qty = SaldoAwalBarang::where('produk_id', $produk->id)->groupBy('gudang_id')->sum('kuantitas');
            // Mencari Pajak Pelanggan & Pajak Pemasok
            if (!empty($informasi_pelanggan)) {
                $kode_pajak = KodePajak::whereIn('id', [$informasi_pelanggan->pajak_satu_id, $informasi_pelanggan->pajak_dua_id])->orderBy('nilai', 'ASC')->get();
            } elseif (!empty($pemasok_id) && is_numeric($pemasok_id)) {
                $informasi_pemasok = InformasiPemasok::where('id', $pemasok_id)->first();
                $kode_pajak = KodePajak::whereIn('id', [$informasi_pemasok->pajak_satu_id, $informasi_pemasok->pajak_dua_id])->orderBy('nilai', 'ASC')->get();
            } else {
                $kode_pajak = KodePajak::get();
            }

            $produk_gudang = Produk::where('id', $produk->id)->get()
                ->map(function ($item) use ($data, $saldoAwalBarang) {

                    if (!empty($item->gudang)) {
                        $data['gudang'] = [0 => ['id' => $item->gudang->id, 'nama' => $item->gudang->nama]];
                        $i = 1;
                        $cek_gudang = $saldoAwalBarang->where('produk_id', $item->id)->groupBy('gudang_id')->get();
                        foreach ($cek_gudang as $saldo_awal) {
                            if ($item->gudang->id != $saldo_awal->gudang_id) {
                                $data['gudang'] += [$i => ['id' => $saldo_awal->gudang_id,
                                    'nama' => $saldo_awal->gudang->nama, ]];
                                ++$i;
                            }
                        }
                    }else {
                        $data['gudang'] = [0 => ['id' => $item->gudang_lain->id, 'nama' => $item->gudang_lain->nama]];
                        $i = 1;
                        $cek_gudang = $saldoAwalBarang->where('produk_id', $item->id)->groupBy('gudang_id')->get();
                        foreach ($cek_gudang as $saldo_awal) {
                            if ($item->gudang_lain->id != $saldo_awal->gudang_id) {
                                $data['gudang'] += [$i => ['id' => $saldo_awal->gudang_id,
                                    'nama' => $saldo_awal->gudang->nama, ]];
                                ++$i;
                            }
                        }
                    }

                    return $data;
                });

            $response = [
                'keterangan' => $produk->keterangan,
                'diskon' => $produk->diskon,
                'jumlah' => 1,
                'tax' => $kode_pajak,
                'satuan' => $produk->unit,
                'unitPrice' => !empty($pelanggan_id) ? $this->getHargaJual($produk, $informasi_pelanggan) : $produk->harga_jual,
                'multiGudang' => $produk_gudang,
                'harga_modal' => $produk->produk_detail,
                'sum_current_qty' => $produk->updated_qty,
            ];

            if (!empty($barang_faktur_id)) {
                $response['kode_pajak'] = $this->getSelectedPajak($barang_faktur_id);
            }
        } else {
            $response = [
                'code' => '404',
            ];
        }

        return response()->json($response);
    }

    public function getAlamatPemasok(Request $request)
    {
        $pemasok_id = $request->get('id');
        if (null !== $pemasok_id) {
            $pemasok = InformasiPemasok::where('id', $pemasok_id)->first();
            $permintaan = PermintaanPembelian::get();
            $pesanan = PesananPembelian::with('barang')->where('pemasok_id', $pemasok_id)->get();
            $penerimaan = PenerimaanPembelian::with('barang')->where('pemasok_id', $pemasok_id)->get();
            $faktur = FakturPembelian::with('barang', 'invoice')->where('pemasok_id', $pemasok_id)->where('uang_muka', '!=', 1)->get();
            $uangmuka = FakturPembelian::where([
                'pemasok_id' => $pemasok_id,
                'uang_muka' => 1,
            ])->get();

            $retur = ReturPembelian::with(['barang' => function ($retur) {
                $retur->with('produk');
            }])->where('pemasok_id', $pemasok_id)->get();

            if ($request->has('retur_id')) {
                $retur->where('id', $request->get('retur_id'));
            }

            $pesanan = $this->pesananPembelianWithQty($pesanan);

            $response = [
                'alamat' => $pemasok->alamat,
                'no_pemasok' => $pemasok->no_pemasok,
                'permintaan' => $permintaan,
                'pesanan' => $pesanan,
                'faktur' => $faktur,
                'penerimaan' => $penerimaan,
                'retur' => $retur,
                'uangmuka' => $uangmuka,
            ];

            return response()->json($response);
        } else {
            $response = [
                'kode_pemasok' => '',
            ];

            return response()->json($response);
        }
    }

    public function getDetailBarang(Request $request)
    {
        $id = $request->get('id');
        $barang = $request->get('type');
        $pelanggan_id = $request->get('pelanggan_id');
        $pemasok_id = $request->get('pemasok_id');
        if (!empty($id)) {
            if (!empty($pelanggan_id) && is_numeric($pelanggan_id)) {
                $informasi_pelanggan = InformasiPelanggan::where('id', $pelanggan_id)->first();
                $kode_pajak = KodePajak::whereIn('id', [$informasi_pelanggan->pajak_satu_id, $informasi_pelanggan->pajak_dua_id])->orderBy('nilai', 'ASC')->get();
            } elseif (!empty($pemasok_id) && is_numeric($pemasok_id)) {
                $informasi_pemasok = InformasiPemasok::where('id', $pemasok_id)->first();
                $kode_pajak = KodePajak::whereIn('id', [$informasi_pemasok->pajak_satu_id, $informasi_pemasok->pajak_dua_id])->orderBy('nilai', 'ASC')->get();
            } else {
                $kode_pajak = KodePajak::get();
            }
            switch ($barang) {
                // Bagian Penjualan
                case 'penawaran':
                    $data = BarangPenawaranPenjualan::with(['produk' => function ($item) {
                        $item->with('kodePajak');
                    }])->where(['penawaran_penjualan_id' => $id])->get();
                break;

                case 'pesanan':
                    $data = BarangPesananPenjualan::with('produk')->where(['pesanan_penjualan_id' => $id, 'pesanan_penjualan_id' => $id]);
                    $getBarangPesananId = $data->pluck('id');
                    $dataPengiriman = BarangPengirimanPenjualan::whereIn('barang_pesanan_penjualan_id', $getBarangPesananId)
                                    ->groupBy('produk_id')
                                    ->selectRaw('*, sum(jumlah) as sum')->get();
                    if ($dataPengiriman->count() <= 0) {
                        $data = $data->get();
                    } else {
                        $data = $data->get()->each(function ($item, $key) use ($dataPengiriman) {
                            // $item->jumlah -= $dataPengiriman[$key]->sum;

                            return $item;
                        });
                    }
                break;

                case 'pengiriman':
                    $data = BarangPengirimanPenjualan::with('produk')->where('pengiriman_penjualan_id', $id)->get();
                break;

                case 'faktur':
                    $data = BarangFakturPenjualan::with('produk')->where('faktur_penjualan_id', $id)->get();
                break;

                case 'retur':
                    $data = BarangReturPenjualan::with('produk')->where('retur_penjualan_id', $id)->get();
                break;

                case 'uang-muka':
                $data = TransaksiUangMuka::with('fakturUangMukaPelanggan')->where('faktur_penjualan_id', $id)->get();
                break;

                // Bagian Pembelian
                case 'permintaan-pembelian':
                    $data = BarangPermintaanPembelian::where('permintaan_pembelian_id', $id)->get();
                break;

                case 'pesanan-pembelian':
                    $data = BarangPesananPembelian::with(['produk', 'pesananPembelian'])->where('pesanan_pembelian_id', $id)->get();
                break;

                case 'penerimaan-pembelian':
                    $data = BarangPenerimaanPembelian::with('produk')->where('penerimaan_pembelian_id', $id)->get();
                break;

                case 'faktur-pembelian':
                    $data = BarangFakturPembelian::with('produk')->where('faktur_pembelian_id', $id)->get();
                break;

                case 'barang':
                    $data = SaldoAwalBarang::where(['produk_id' => $id, 'status' => 0])->get();
                break;
            }
            if ($barang == 'barang') {
                $data = $this->formattingTanggalNominalBarang($data);
            }
            if ($barang == 'faktur-pembelian') {
                $data = $this->formattingTanggalNominalFaktur($data);
            }
            if ($barang == 'pesanan-pembelian') {
                $data = $data->map(function ($item) {
                    $item->updated_qty = $item->updated_qty_penerimaan_pembelian;

                    return $item;
                });
            }
            $response = [
                'code' => 200,
                'barang' => $data,
                'kode_pajak' => $kode_pajak,
            ];
            if ($barang == 'pesanan') {
                $response['penawaran_id'] = $this->getPenawaranId($data);
            }

            if ($barang == 'faktur-pembelian') {
                $barangFaktur = BarangFakturPembelian::where('faktur_pembelian_id', '=', $id)->firstOrFail();
                $produk = Produk::where('id', $barangFaktur->produk_id)->first();
                $saldoAwalBarang = new SaldoAwalBarang();
                $array_gudang = [];
                $produk_gudang = Produk::where('id', $barangFaktur->produk_id)->get()
                    ->map(function ($item) use ($array_gudang, $saldoAwalBarang) {
                        if (!empty($item->gudang->id)) {
                            $array_gudang['gudang'] = [0 => ['id' => $item->gudang->id, 'nama' => $item->gudang->nama]];
                            $i = 1;
                            $cek_gudang = $saldoAwalBarang->where('produk_id', $item->id)->groupBy('gudang_id')->get();
                            foreach ($cek_gudang as $saldo_awal) {
                                if ($item->gudang->id != $saldo_awal->gudang_id) {
                                    $array_gudang['gudang'] += [$i => ['id' => $saldo_awal->gudang_id,
                                        'nama' => $saldo_awal->gudang->nama, ]];
                                    ++$i;
                                }
                            }
                        }else if(!empty($item->gudang_lain->id)) {
                            $array_gudang['gudang'] = [0 => ['id' => $item->gudang_lain->id, 'nama' => $item->gudang_lain->nama]];
                            $i = 1;
                            $cek_gudang = $saldoAwalBarang->where('produk_id', $item->id)->groupBy('gudang_id')->get();
                            foreach ($cek_gudang as $saldo_awal) {
                                if ($item->gudang_lain->id != $saldo_awal->gudang_id) {
                                    $array_gudang['gudang'] += [$i => ['id' => $saldo_awal->gudang_id,
                                        'nama' => $saldo_awal->gudang->nama, ]];
                                    ++$i;
                                }
                            }
                        }
                        return $array_gudang;
                    });
                $response['multiGudang'] = $produk_gudang;
                $response['beban'] = BebanFakturPembelian::where('faktur_pembelian_id', $id)->get();
                $response['dp'] = TransaksiUangMukaPemasok::with('fakturUangMukaPemasok.barang')->where('faktur_pembelian_id', $id)->get();
            }
        } else {
            $response = [
                'code' => 404,
                'barang' => null,
            ];
        }

        return response()->json($response);
    }

    public function getProdukByVal(Request $request)
    {
        $id = $request->get('id');
        $pelanggan_id = $request->get('pelanggan_id');
        $pemasok_id = $request->get('pemasok_id');
        $type = $request->get('type');
        if (null !== $id) {
            switch ($type) {
                case 'penawaran':
                    $data = BarangPenawaranPenjualan::findOrFail($id);
                break;

                case 'pesanan':
                    $data = BarangPesananPenjualan::findOrFail($id);
                break;

                case 'pengiriman':
                    $data = BarangPengirimanPenjualan::findOrFail($id);
                break;

                case 'faktur':
                    $data = BarangFakturPenjualan::findOrFail($id);
                break;

                case 'retur':
                    $data = BarangReturPenjualan::where('barang_faktur_penjualan_id', $id)->first();
                break;

                // Bagian Pembelian
                case 'permintaan-pembelian':
                    $data = BarangPermintaanPembelian::findOrFail($id);
                break;

                case 'pesanan-pembelian':
                    $data = BarangPesananPembelian::findOrFail($id);
                break;

                case 'penerimaan-pembelian':
                    $data = BarangPenerimaanPembelian::findOrFail($id);
                break;

                case 'faktur-pembelian':
                    $data = BarangFakturPembelian::findOrFail($id);
                break;

                case 'retur-pembelian':
                    $data = BarangReturPembelian::findOrFail($id);
                break;
            }
            // Produk hanya digunakan guna mencari pajak / tax
            $produk = Produk::where('id', $data->produk_id)->first();
            $saldoAwalBarang = new SaldoAwalBarang();
            $array_gudang = [];
            $produk_gudang = Produk::where('id', $data->produk_id)->get()
                ->map(function ($item) use ($array_gudang, $saldoAwalBarang) {
                    if (!empty($item->gudang->id)) {
                        $array_gudang['gudang'] = [0 => ['id' => $item->gudang->id, 'nama' => $item->gudang->nama]];
                        $i = 1;
                        $cek_gudang = $saldoAwalBarang->where('produk_id', $item->id)->groupBy('gudang_id')->get();
                        foreach ($cek_gudang as $saldo_awal) {
                            if ($item->gudang->id != $saldo_awal->gudang_id) {
                                $array_gudang['gudang'] += [$i => ['id' => $saldo_awal->gudang_id,
                                    'nama' => $saldo_awal->gudang->nama, ]];
                                ++$i;
                            }
                        }
                    }else {
                        $array_gudang['gudang'] = [0 => ['id' => $item->gudang_lain->id, 'nama' => $item->gudang_lain->nama]];
                        $i = 1;
                        $cek_gudang = $saldoAwalBarang->where('produk_id', $item->id)->groupBy('gudang_id')->get();
                        foreach ($cek_gudang as $saldo_awal) {
                            if ($item->gudang_lain->id != $saldo_awal->gudang_id) {
                                $array_gudang['gudang'] += [$i => ['id' => $saldo_awal->gudang_id,
                                    'nama' => $saldo_awal->gudang->nama, ]];
                                ++$i;
                            }
                        }
                    }

                    return $array_gudang;
                });

            // Ambil Semua Pajak Pelanggan & Pemasok
            if (!empty($pelanggan_id) && is_numeric($pelanggan_id)) {
                $informasi_pelanggan = InformasiPelanggan::where('id', $pelanggan_id)->first();
                $kode_pajak = KodePajak::whereIn('id', [$informasi_pelanggan->pajak_satu_id, $informasi_pelanggan->pajak_dua_id])->orderBy('nilai', 'ASC')->get();
            } elseif (!empty($pemasok_id) && is_numeric($pemasok_id)) {
                $informasi_pemasok = InformasiPemasok::where('id', $pemasok_id)->first();
                $kode_pajak = KodePajak::whereIn('id', [$informasi_pemasok->pajak_satu_id, $informasi_pemasok->pajak_dua_id])->orderBy('nilai', 'ASC')->get();
            } else {
                $kode_pajak = KodePajak::get();
            }
            // Apabila type yang dipanggil pesanan
            if ((!empty($data))) {
                $response = [
                    // 'produk' =>$produk->kodePajak,
                    'id' => $data->id,
                    'text' => $data->produk->no_barang.' | '.$data->item_deskripsi,
                    'keterangan' => $data->item_deskripsi,
                    'jumlah' => $data->jumlah,
                    'diskon' => $data->diskon,
                    'tax' => $kode_pajak,
                    'satuan' => $data->satuan,
                    'unitPrice' => $data->harga,
                    'barang_id' => $data->id,
                    'terproses' => $data->terproses,
                    'ditutup' => $data->ditutup,
                    'multiGudang' => $produk_gudang,
                    'harga_modal' => $produk->produk_detail,
                    'all' => $data,
                ];
                if (!empty($data->kode_pajak_id)) {
                    $response['kode_pajak_id'] = $data->kode_pajak_id;
                    $response['kode_pajak_2_id'] = $data->kode_pajak_2_id;
                }

                if ('retur-pembelian' == $type) {
                    $response['barang_faktur_pembelian_id'] = $data->barang_faktur_pembelian_id;
                    $response['taxable'] = ReturPembelian::findOrFail($data->retur_pembelian_id);
                }
                if ('faktur-pembelian' == $type) {
                    $response['satuan'] = $data->item_unit;
                    $response['unitPrice'] = $data->unit_price;
                    $response['taxable'] = FakturPembelian::where('id', $data->faktur_pembelian_id)->first();
                }
                if (
                    // $type == 'faktur' or
                    'permintaan-pembelian' == $type
                ) {
                    $response['satuan'] = $data->item_unit;
                    $response['unitPrice'] = $data->unit_price;
                }

                if ('pesanan-pembelian' == $type) {
                    $pesanan_pembelian = PesananPembelian::where('id', $data->pesanan_pembelian_id)->first();
                    $response['fob'] = $pesanan_pembelian->fob;
                    $response['ship_id'] = $pesanan_pembelian->ship_id;
                    $response['term_id'] = $pesanan_pembelian->term_id;
                    $response['satuan'] = $data->satuan;
                    $response['taxable'] = $pesanan_pembelian;
                    $response['updated_qty'] = $data->updated_qty_penerimaan_pembelian;
                }

                if ('faktur' == $type) {
                    $response['satuan'] = $data->item_unit;
                    $response['unitPrice'] = $data->unit_price;
                    $response['taxable'] = FakturPenjualan::where('id', $data->faktur_penjualan_id)->first();
                }

                if ('retur' == $type) {
                    $response['barang_faktur_penjualan_id'] = $data->barang_faktur_penjualan_id;
                }

                if ('penawaran' == $type) {
                    $response['penawaran_id'] = $data->penawaran_penjualan_id;
                    $response['taxable'] = PenawaranPenjualan::where('id', $data->penawaran_penjualan_id)->first();
                }

                if ('pengiriman' == $type) {
                    $pengiriman_penjualan = PengirimanPenjualan::where('id', $data->pengiriman_penjualan_id)->first();
                    $response['satuan'] = $data->item_unit;
                    $response['delivery_date'] = $pengiriman_penjualan->delivery_date;
                    // Cari Harga sampai dapat
                    if (null !== $data->barang_pesanan_penjualan_id) {
                        $cari_harga_ke_pesanan = BarangPesananPenjualan::where('id', $data->barang_pesanan_penjualan_id)->first();
                        if (null !== $cari_harga_ke_pesanan->harga) {
                            $response['unitPrice'] = $cari_harga_ke_pesanan->harga;
                        } else {
                            $response['unitPrice'] = 0;
                        }
                    } else {
                        $response['unitPrice'] = 0;
                    }
                    // Cari Diskon sampai dapat
                    if (null !== $data->barang_pesanan_penjualan_id) {
                        $cari_diskon_ke_pesanan = BarangPesananPenjualan::where('id', $data->barang_pesanan_penjualan_id)->first();
                        if (null !== $cari_diskon_ke_pesanan->diskon) {
                            $response['diskon'] = $cari_diskon_ke_pesanan->diskon;
                        } else {
                            $response['diskon'] = 0;
                        }
                    } else {
                        $response['diskon'] = 0;
                    }
                    // Cari Pajak Ke Pesanan
                    if (null === $data->kode_pajak_id || null === $data->kode_pajak_2_id) {
                        $data_pajak = BarangPesananPenjualan::where('id', $data->barang_pesanan_penjualan_id)->first();
                        $response['kode_pajak_id'] = $data_pajak->kode_pajak_id;
                        $response['kode_pajak_2_id'] = $data_pajak->kode_pajak_2_id;
                    }
                    // Cari So Number & Term ID & FOB
                    $cari_no_so = PesananPenjualan::whereHas('barang.barangPengirimanPenjualan', function ($query) use ($data) {
                        $query->where('id', $data->id);
                    })->first();
                    $response['diskon_faktur'] = $cari_no_so->diskon;
                    $response['jumlah_diskon_faktur'] = $cari_no_so->jumlah_diskon_faktur;
                    $response['keterangan_faktur'] = $cari_no_so->keterangan;
                    $response['ongkir_faktur'] = $cari_no_so->ongkir;
                    $response['no_so'] = $cari_no_so->so_number;
                    $response['term_id'] = $cari_no_so->term_id;
                    $response['fob'] = $cari_no_so->fob;
                    $response['no_do'] = $pengiriman_penjualan->delivery_no;
                    $response['po_no'] = $pengiriman_penjualan->po_no;
                    $response['ship_id'] = $pengiriman_penjualan->ship_id;
                    $response['barang_pengiriman_penjualan_id'] = $data->id;
                    $response['taxable'] = $cari_no_so;
                }
                if ('penerimaan-pembelian' == $type) {
                    $response['keterangan'] = $data->produk->keterangan;
                    // Cari value ke penerimaan-pembelian
                    $data_penerimaan = PenerimaanPembelian::where('id', $data->penerimaan_pembelian_id)->first();
                    $response['form_no'] = $data_penerimaan->form_no;
                    $response['ship_date'] = Carbon::parse($data_penerimaan->ship_date)->format('d F Y');
                    $response['receive_date'] = $data_penerimaan->receive_date;
                    $response['fob'] = $data_penerimaan->fob;
                    $response['ship_id'] = $data_penerimaan->ship_id;

                    // Cari Value dari penerimaan pembelian ke pesanan pembelian
                    $data_pesanan = BarangPesananPembelian::where('id', $data->barang_pesanan_pembelian_id)->first();

                    $response['unitPrice'] = 0;
                    $response['diskon'] = 0;
                    $response['kode_pajak_id'] = null;
                    $response['kode_pajak_2_id'] = null;
                    $response['updated_qty'] = $data->updated_qty;

                    if (!empty($data_pesanan)) {
                        $response['unitPrice'] = $data_pesanan->harga;
                        $response['diskon'] = $data_pesanan->diskon;
                        $response['kode_pajak_id'] = $data_pesanan->kode_pajak_id;
                        $response['kode_pajak_2_id'] = $data_pesanan->kode_pajak_2_id;

                        // Cari value ke pesanan pembelian
                        $data_pesanan_pembelian = PesananPembelian::where('id', $data_pesanan->pesanan_pembelian_id)->first();
                        $response['term_id'] = $data_pesanan_pembelian->term_id;
                        $response['taxable'] = $data_pesanan_pembelian;
                    }
                }
                if ('pesanan' == $type) {
                    $pesanan_penjualan = PesananPenjualan::where('id', $data->pesanan_penjualan_id)->first();
                    $response['diskon_faktur'] = $pesanan_penjualan->diskon;
                    $response['keterangan_faktur'] = $pesanan_penjualan->keterangan;
                    $response['ongkir_faktur'] = $pesanan_penjualan->ongkir;
                    $response['no_so'] = $pesanan_penjualan->so_number;
                    $response['po_no'] = $pesanan_penjualan->po_number;
                    $response['ship_id'] = $pesanan_penjualan->ship_id;
                    $response['term_id'] = $pesanan_penjualan->term_id;
                    $response['fob'] = $pesanan_penjualan->fob;
                    $response['taxable'] = $pesanan_penjualan;
                    $response['nominal'] = $data->harga_total;
                    // $response['harga_modal'] 					= $data->harga_modal;
                }
            }
        } else {
            $response = [
                'code' => '404',
            ];
        }

        return response()->json($response);
    }

    public function getDetailPelanggan(Request $request)
    {
        $id = $request->get('id');
        if (null !== $id) {
            $syarat_pembayaran = SyaratPembayaran::get();
            $data = FakturPenjualan::findOrFail($id);
            $detail_faktur = BarangFakturPenjualan::where('faktur_penjualan_id', $data->id)->first();
            if (!empty($data->pelanggan_id) && is_numeric($data->pelanggan_id)) {
                $informasi_pelanggan = InformasiPelanggan::where('id', $data->pelanggan_id)->first();
                $arrayTaxPelanggan = KodePajak::whereIn('id', [$informasi_pelanggan->pajak_satu_id, $informasi_pelanggan->pajak_dua_id])->get();
            } else {
                $arrayTaxPelanggan = KodePajak::get();
            }
            $response = [
                'id' => $data->id,
                'penjelasan' => $detail_faktur->item_deskripsi,
                'saldo_awal' => $data->updated_uang_muka,
                'no_faktur' => $data->no_faktur,
                'no_pesanan' => $data->po_no,
                'in_tax_texted' => $data->in_tax_texted,
                'syarat_pembayaran' => $syarat_pembayaran,
                'dp' => $data->akun_dp_id,
                'tax' => $arrayTaxPelanggan,
                'code' => '200',
            ];
        } else {
            $response = [
                'code' => '404',
            ];
        }

        return response()->json($response);
    }

    public function getFakturPembelian(Request $request)
    {
        $id_faktur = $request->get('id');
        if (null != $id_faktur) {
            $data = FakturPembelian::findOrFail($id_faktur);
            $detail_faktur = BarangFakturPembelian::where('faktur_pembelian_id', $data->id)->first();
            if (!empty($data->pemasok_id) && is_numeric($data->pemasok_id)) {
                $informasi_pemasok = InformasiPemasok::where('id', $data->pemasok_id)->first();
                $arrayTaxPemasok = KodePajak::whereIn('id', [$informasi_pemasok->pajak_satu_id, $informasi_pemasok->pajak_dua_id])->get();
            } else {
                $arrayTaxPemasok = KodePajak::get();
            }
            $response = [
                'data' => $data,
                'penjelasan' => $detail_faktur->item_deskripsi,
                'termasuk_pajak' => $data->in_tax_texted,
                'tax' => $arrayTaxPemasok,
                'code' => '200',
            ];
        } else {
            $response = [
                'code' => '404',
            ];
        }

        return response()->json($response);
    }

    public function setMasterDetailSession(Request $request)
    {
        $respNum = 200;
        $all = $request->all();
        $count = count($request['produk_id']);
        $items = [];
        $jam_sekarang = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        if ($count > 0) {
            for ($i = 0; $i < $count; ++$i) {
                $ditutup = (empty($request['ditutup'][$i])) ? 0 : $request['ditutup'][$i];
                $terproses = (empty($request['terproses'][$i])) ? 0 : $request['terproses'][$i];
                $items[] = [
                    'produk_id' => $request['produk_id'][$i],
                    'item_deskripsi' => $request['keterangan_produk'][$i],
                    'jumlah' => $request['qty_produk'][$i],
                    'harga' => $request['unit_harga_produk'][$i],
                    'satuan' => $request['satuan_produk'][$i],
                    'diskon' => $request['diskon_produk'][$i],
                    'pajak' => (empty($request['tax_produk'][$i])) ? 0 : $request['tax_produk'][$i],
                    'terproses' => $terproses,
                    'ditutup' => $ditutup,
                    'created_at' => $jam_sekarang,
                    'updated_at' => $jam_sekarang,
                ];
            }
        }

        $dataSession = [
            'master_pelanggan_id' => $request['pelanggan_id'],
            'alamat_pengiriman' => $request['alamat_pengiriman'],
            'no_pelanggan' => $request['no_pelanggan'],
            'items' => $items,
        ];
        custom_session('penawaran_penjualan'.auth()->user()->id, $dataSession);

        return response()->json($dataSession, $respNum);
    }

    public function checkDp(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->get('id');
            $data = TransaksiUangMuka::where('faktur_uang_muka_pelanggan_id', $id)->sum('jumlah');

            return response()->json($data);
        }
    }

    public function checkDpPembelian(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->get('id');
            $data = Transaksi::where([
                'status' => 1,
                'item_id' => $this->getAttribute('id'),
                'item_type' => 'App\Modules\Akuntansi\Models\FakturPembelian',
            ])->sum('nominal');

            return response()->json($data);
        }
    }

    public function checkTermin(Request $request)
    {
        $termin_id = $request->get('id');
        $faktur_id = $request->get('faktur_id');
        if (!empty($termin_id)) {
            $termin = SyaratPembayaran::findOrFail($termin_id);
            $faktur = FakturPenjualan::findOrFail($faktur_id);
            $tanggal_sekarang = Carbon::now();
            $parse_invoice_date = Carbon::parse($faktur->invoice_date);
            $discountLimit = $parse_invoice_date->diffInDays($tanggal_sekarang);
            if ($discountLimit <= $termin->jika_membayar_antara) {
                $hasil_diskon = $termin->akan_dapat_diskon / 100 * $faktur->nilai_faktur;
            } else {
                $hasil_diskon = 0;
            }
            $response = [
                'termin'    => $termin->naration,
                'diskon'    => $hasil_diskon,
                'code'      => '200',
            ];
        } else {
            $response = [
                'termin'    => '',
                'diskon'    => 0,
                'code'      => '404',
            ];
        }

        return response()->json($response);
    }

    public function sortPajak(Request $request)
    {
        $id = $request->get('id');
        $type = $request->get('type');
        $barang_faktur_id = $request->get('barang_faktur_id');
        if (null !== $id) {
            switch ($type) {
                case 'pemasok':
                    $data = InformasiPemasok::findOrFail($id);
                break;
                case 'pelanggan':
                    $data = InformasiPelanggan::findOrFail($id);
                break;
            }
            if (!empty($data)) {
                $kode_pajak = KodePajak::whereIn('id', [$data->pajak_satu_id, $data->pajak_dua_id])->orderBy('nilai', 'ASC')->get();
            } else {
                $kode_pajak = KodePajak::orderBy('nilai', 'ASC')->get();
            }
            $response = [
                'kode_pajak' => $kode_pajak,
                'code' => '200',
            ];
            if (null !== $barang_faktur_id) {
                $barang = BarangFakturPembelian::findOrFail($barang_faktur_id);
                $pajak_terpilih = KodePajak::whereIn('id', [$barang->kode_pajak_id, $barang->kode_pajak_2_id])->get();
                $response['pajak_terpilih'] = $pajak_terpilih;
            }
        } else {
            $response = [
                'kode_pajak' => null,
                'code' => '404',
            ];
        }

        return response()->json($response);
    }

    public function getDetailPemasok(Request $request)
    {
        $id = $request->get('id');
        $data = InformasiPemasok::with(['kontakPemasok', 'detailInformasiPemasok'])->findOrFail($id);

        // Merubah data tanggal dan data saldo awal
        $data->detailInformasiPemasok = $data->detailInformasiPemasok->map(function ($item, $key) {
            $item->tanggal = Carbon::parse($item->tanggal)->format('d F Y');
            $item->saldo_awal = number_format($item->saldo_awal);

            return $item;
        });

        $response = [
            'data' => $data,
        ];

        return response()->json($response);
    }

    public function getEditInformasiPelanggan(Request $request)
    {
        $id = $request->get('id');
        $data = InformasiPelanggan::with(['kontakPelanggan', 'detailInformasiPelanggan'])->findOrFail($id);

        // Merubah data tanggal dan data saldo awal
        $data->detailInformasiPelanggan = $data->detailInformasiPelanggan->map(function ($item, $key) {
            $item->tanggal = Carbon::parse($item->tanggal)->format('d F Y');
            $item->saldo_awal = number_format($item->saldo_awal);

            return $item;
        });
        $response = [
            'data' => $data,
        ];

        return response()->json($response);
    }

    public function getAlamatGudang(Request $request)
    {
        $gudang_id = $request->get('id');
        if (null !== $gudang_id) {
            $gudang = Gudang::where('id', $gudang_id)->first();

            $listBarang = Produk::whereHas('saldoAwalBarang', function ($query) use ($gudang) {
                $query->where('gudang_id', $gudang->id);
            })->get()->reduce(function ($output, $item) {
                $output[$item->id] = $item->no_barang.' | '.$item->keterangan;

                return $output;
            }, []);

            $response = [
                'id' => $gudang->id,
                'alamat' => $gudang->alamat,
                'produk' => $listBarang,
            ];

            return response()->json($response);
        } else {
            $response = [
                'code' => '404',
            ];

            return response()->json($response);
        }
    }

    public function getPenawaranId($data)
    {
        $penawaran = PenawaranPenjualan::whereHas('barang', function ($query) use ($data) {
            $query->whereIn('id', $data->pluck('barang_penawaran_penjualan_id'));
        })->first();

        return $penawaran;
    }

    public function formattingTanggalNominalBarang($data)
    {
        $data = $data->map(function ($item, $key) {
            $item->tanggal_formatted    = Carbon::parse($item->tanggal)->format('d F Y');
            $item->harga_modal          = number_format($item->harga_modal);

            return $item;
        });

        return $data;
    }

    public function formattingTanggalNominalFaktur($data)
    {
        $data = $data->map(function ($item, $key) {
            $item->expired_date    = Carbon::parse($item->expired_date)->format('d F Y');
            return $item;
        });

        return $data;
    }

    public function getSelectedPajak($barang_faktur_id)
    {
        return BarangFakturPenjualan::where('id', $barang_faktur_id)->get()->map(function ($item, $key) {
            $kode_pajak = [
                'pajak_satu' => $item->kode_pajak_id,
                'pajak_dua' => $item->kode_pajak_2_id,
            ];

            return $kode_pajak;
        });
    }

    public function cekGudangStock(Request $request)
    {
        $gudang_id = $request->gudang_id;
        $produk_id = $request->produk_id;
        $qty_jumlah = $request->qty_id;
        $status = 0;
        if (null !== $gudang_id && null !== $produk_id) {
            $kuantitas = SaldoAwalBarang::where(['produk_id' => $produk_id, 'gudang_id' => $gudang_id])->sum('kuantitas');
            $gudang = Gudang::findOrFail($gudang_id);
            if ($qty_jumlah <= $kuantitas) {
                $status = 1;
            } else {
                $status = 0;
            }
            $response = [
                'status' => $status,
                'gudang' => $gudang->nama,
                'kuantitas' => $kuantitas,
                'code' => '200',
            ];

            return response()->json($response);
        } else {
            $response = [
                'code' => '404',
            ];

            return response()->json($response);
        }
    }

    public function getReturnedItem(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->get('id'); //faktur penjualannya
            $pelanggan_id = $request->get('pelanggan_id');
            $data = ReturPenjualan::where(['pelanggan_id' => $pelanggan_id, 'faktur_penjualan_id' => $id])->pluck('id');

            if (!empty($data)) {
                $barang = BarangReturPenjualan::whereIn('retur_penjualan_id', $data)->get();
                $arrayBarang = [];
                foreach ($barang as $key => $data_barang) {
                    $arrayBarang[] = [
                        'produk_id' => $data_barang->produk_id,
                        'qty' => $data_barang->where('produk_id', $data_barang->produk_id)->sum('jumlah'),
                    ];
                }
                $response = [
                    'code' => '200',
                    'data' => $arrayBarang,
                ];
            } else {
                $response = [
                    'code' => '404',
                    'data' => '',
                ];
            }

            return $response;
        }
    }

    public function getDaftarNamaProduk(Request $request)
    {
        if ($request->ajax()) {
            if(!empty($request['keterangan']) === true){ // buat pencarian keterangan
                $key = $request->get('keterangan');
                $data = Produk::where('tipe_barang', '!=', 3)->Where('keterangan', 'like', '%'.$key.'%');
            }else if(!empty($request['no_barang'])  === true){ // buat pencarian no_barang
                $key = $request->get('no_barang');
                $data = Produk::where('tipe_barang', '!=', 3)->Where('no_barang', 'like', '%'.$key.'%');
            }else{
                $key = $request->get('search');
                $data = Produk::where('tipe_barang', '!=', 3)->Where('keterangan', 'like', '%'.$key.'%');
            }
            if(!empty($request['kategoriBySearch'])){
                $data->Where('kategori_produk_id', $request['kategoriBySearch']);
            }else{
                // nothing to do ...
            }
            $data = $data->paginate(10)->getCollection()->transform(function ($item) use ($request) {
                return ['id' => $item['id'], 'text' => $item['no_barang'].' | '.$item['keterangan'], 'no_barang' => $item['no_barang'], 'keterangan' => $item['keterangan'] ];
            });
            // 
            if ($key === null || $data->isEmpty()) {
                return response()->json('null');
            }else{
                return response()->json($data);
            }
        }
    }

    public function getDaftarNamaProdukDetail(Request $request)
    {
        if ($request->ajax()) {
            $key = $request->get('search');
            $data = Produk::where('tipe_barang', '!=', 3)->Where('keterangan', 'like', '%'.$key.'%')
                ->has('saldoAwalBarang')
                ->get()->map(function ($item) use ($request) {
                    return ['id' => $item['id'], 'text' => $item['no_barang'].' | '.$item['keterangan']];
                });

            return response()->json($data);
        }
    }

    public function getDaftarIdProduk(Request $request)
    {
        if ($request->ajax()) {
            ini_set('memory_limit', '4095M');
            $key = $request->get('search');
            if($key === 'undefined'){
                $item = [
                    'item' => null,
                    'harga' => null,
                ];
            }else{
                $data = Produk::where('id', $key)
                    ->get()->map(function ($item) use ($request) {
                        return ['id' => $item['id'], 'text' => $item['no_barang'].' | '.$item['keterangan'], 'no_barang' => $item['no_barang']];
                    });
    
                $harga = Produk::where('id', $key)->first()->harga_jual;
    
                $item = [
                    'item' => $data,
                    'harga' => $harga
                ];
            }
            ini_set('memory_limit', '512M'); 
            return response()->json($item);
        }
    }

    public function getDaftarNamaAkun(Request $request)
    {
        if ($request->ajax()) {
            $key = $request->get('search');
            $data = Akun::whereDoesntHave('childAkun')->where('nama_akun', 'like', '%'.$key.'%');
            if ($request->get('kas') == 1) {
                $data = $data->whereHas('tipeAkun', function ($item) {
                    $item->where('title', 'Kas/Bank');
                });
            }
            $data = $data->get()->map(function ($item) use ($request) {
                return ['id' => $item['id'], 'text' => $item['nama_akun']];
            });

            return response()->json($data);
        }
    }

    public function getDaftarIdAkun(Request $request)
    {
        if ($request->ajax()) {
            $key = $request->get('search');
            $data = Akun::where('id', $key)
                ->get()->map(function ($item) use ($request) {
                    return ['id' => $item['id'], 'text' => $item['kode_akun'].' | '.$item['nama_akun']];
                });

            return response()->json($data);
        }
    }

    public function getTanggalTransaksi(Request $request)
    {
        $transaksi_id = $request->get('id');
        if (null !== $transaksi_id) {
            $transaksi = Transaksi::where('id', $transaksi_id)->first();
            $response = [
                'id' => $transaksi->id,
                'tanggal' => $transaksi->tanggal,
            ];

            return response()->json($response);
        } else {
            $response = [
                'code' => '404',
            ];

            return response()->json($response);
        }
    }

    public function cekRetur(Request $request)
    {
        if ($request->ajax()) {
            if ($request->get('pemasok_id') === null) {
                return response()->json(['total' => 0]);
            } else {
                $data = ReturPembelian::where('pemasok_id', $request->get('pemasok_id'))->get();

                $array = [];
                foreach ($data as $retur) {
                    $array[] = [
                        'total' => $retur->total,
                        'faktur_pembelian_id' => $retur->faktur_pembelian_id,
                    ];
                }
                if ($array === null) {
                    return response()->json(['total' => 0]);
                }
                $newCollection = collect($array);
                $newCollection->groupBy('faktur_pembelian_id')->map(function ($row) {
                    return $row->sum('total');
                });
                $response = [
                    'total' => $data->sum('total'),
                    'perFaktur' => $newCollection,
                ];

                return response()->json($response);
            }
        }
    }

    public function pesananPembelianWithQty($pesanan)
    {
        return $pesanan->map(function ($item_pesanan) {
            $item_pesanan->sum_updated_qty = $item_pesanan->sum_updated_qty;

            return $item_pesanan;
        });
    }

    public function getRekonsiliasiBankData(Request $request)
    {
        $akun_id = $request->get('id');
        if ($akun_id == $request->id) {
            $semua_rekonsiliasi = Transaksi::where('akun_id', $akun_id)->orderBy('id', 'DESC')->first();
            $response = [
                'rekonsiliasi' => $semua_rekonsiliasi,
            ];

            return response()->json($response);
        } else {
            $response = [
                'rekonsiliasi' => '',
            ];

            return response()->json($response);
        }
    }

    public function getRekonsiliasiBank(Request $request)
    {
        $view = null;
        $tanggal = Carbon::parse($request->tanggal)->format('Y-m-d');
        $akun_id = $request->id;

        if ($akun_id !== null) {
            $semua_transaksi = Transaksi::with(['akun', 'historyRekonsiliasi'])->whereHas('akun', function ($query) use ($request) {
                $query->where('id', $request->id);
            })->whereNotNull('status')->get()->sortBy('tanggal');

            $semua_transaksi = $this->semuaTransaksi($semua_transaksi);

            $view = view('akuntansi::rekonsiliasi_bank.content', compact('semua_transaksi'))->render();
        }

        return response()->json($view);
    }

    public function getAkunRekonsiliasi(Request $request)
    {
        $akun_id = $request->get('id');
        if ($akun_id !== null) {
            $akun = Akun::where('id', $akun_id)->first();
            $history_rekonsiliasi = HistoryRekonsiliasi::with('transaksi')->whereHas('transaksi')->where('akun_id', $akun_id)->latest()->first();

            if ($history_rekonsiliasi !== null) {
                $response = [
                    'kode_akun' => $akun->kode_akun,
                    'nama_akun' => $akun->nama_akun,
                    'saldo_rekening_koran' => $history_rekonsiliasi->saldo_rekening_koran,
                    'kalkulasi_saldo' => $history_rekonsiliasi->kalkulasi_saldo,
                    'selisih_saldo' => $history_rekonsiliasi->selisih_saldo,
                    'tanggal_terakhir_rekonsil' => Carbon::parse($history_rekonsiliasi->tanggal_terakhir_rekonsil)->format('d F Y'),
                ];
            } else {
                $response = [
                    'kode_akun' => $akun->kode_akun,
                    'nama_akun' => $akun->nama_akun,
                    'saldo_rekening_koran' => null,
                    'kalkulasi_saldo' => null,
                    'selisih_saldo' => null,
                    'tanggal_terakhir_rekonsil' => null,
                ];
            }

            return response()->json($response);
        } else {
            $response = [
                'kode_akun' => '',
            ];

            return response()->json($response);
        }
    }

    public function semuaTransaksi($semua_transaksi)
    {
        return $semua_transaksi->map(function ($item_semua_transaksi) {
            // status checked
            if (!empty($item_semua_transaksi->historyRekonsiliasi)) {
                $data_history = !empty($item_semua_transaksi->historyRekonsiliasi->last()->pivot) ? 'checked' : 'unchecked';
                $rekonsil_pada = $item_semua_transaksi->historyRekonsiliasi->last()->tanggal_terakhir_rekonsil ?? null;
            }else {
                $data_history = 'unchecked';
                $rekonsil_pada = null;
            }

            // debet - kredit
            if ($item_semua_transaksi->status == 1) {
                $debet = $item_semua_transaksi->nominal;
                $kredit = 0;
            } elseif ($item_semua_transaksi->status == 0) {
                $debet = 0;
                $kredit = $item_semua_transaksi->nominal;
            }

            return [
                'id' => $item_semua_transaksi->id,
                'status' => $item_semua_transaksi->status,
                'tanggal' => Carbon::parse($item_semua_transaksi->tanggal)->format('d F Y'),
                'no_faktur' => $item_semua_transaksi->no_transaksi,
                'no_cek' => $item_semua_transaksi->item->no_cek,
                'keterangan' => $item_semua_transaksi->keterangan,
                'rate' => 1,
                'debet' => $debet,
                'kredit' => $kredit,
                'status_checked' => $data_history,
                'rekonsil_pada' => Carbon::parse($rekonsil_pada)->format('d F Y'),
            ];
        });
    }

    public function getPembiayaan(Request $request)
    {
        if ($request->ajax()) {
            $pembiayaan = PembiayaanPesanan::find($request->get('id'));

            $pembiayaan->date = Carbon::parse($pembiayaan->date)->format('d F Y');

            return response()->json($pembiayaan);
        }
    }

    public function getHargaJual($produk, $informasi_pelanggan)
    {
        $tingkat_harga_pelanggan = $informasi_pelanggan->tingkatan_harga_jual ?? 0;

        if (!empty($produk->tingkatHargaBarang)) {
            if ($tingkat_harga_pelanggan === 1) {
                $harga_jual = $produk->tingkatHargaBarang->tingkat_1 ?? 0;
            }else if ($tingkat_harga_pelanggan === 2) {
                $harga_jual = $produk->tingkatHargaBarang->tingkat_2 ?? 0;
            }else if ($tingkat_harga_pelanggan === 3) {
                $harga_jual = $produk->tingkatHargaBarang->tingkat_3 ?? 0;
            }else if ($tingkat_harga_pelanggan === 4) {
                $harga_jual = $produk->tingkatHargaBarang->tingkat_4 ?? 0;
            }else if ($tingkat_harga_pelanggan === 5) {
                $harga_jual = $produk->tingkatHargaBarang->tingkat_5 ?? 0;
            }else {
                $harga_jual = $produk->harga_jual ?? 0;
            }
        }else {
            $harga_jual = $produk->harga_jual ?? 0;
        }

        return $harga_jual;
    }

    public function cekPreferensiPenyesuaian(Request $request)
    {
        $preferensi_mata_uang = $this->preferensi_mata_uang->first();

        $kuantitas = $request->kuantitas;

        $status = null;

        if (!empty($kuantitas) && $kuantitas > 0) {

            if (empty($preferensi_mata_uang->akun_penyesuaian_id)) {
                $status = 'false';
            }else {
                $status = 'true';
            }

        }else {

            $status = 'true';

        }

        return $status;
    }
    public function getCategoryProduk(Request $request)
    {
        if ($request->ajax()) {
            $key = $request->get('search');
            $data = KategoriProduk::where('id', $key)
                ->get()->map(function ($item) use ($request) {
                    return ['id' => $item['id'], 'text' => $item['nama']];
                });

            $item = [
                'item' => $data
            ];
            return response()->json($item);
        }
    }

    public function getListKategoriProduk(Request $request)
    {
        $kategori = KategoriProduk::get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
        return response()->json($kategori);
    }

    public function getListGudang(Request $request)
    {
        $gudang  = Gudang::get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
        return response()->json($gudang);
    }

    public function getSumQtyGudangProdukDetail(Request $request)
    {
        if ($request->ajax()) {
            if ($request['gudang'] > 0 ) {
                $data = $this->saldoAwalBarang
                        ->select([
                            \DB::Raw('sum(kuantitas) as qty'),
                        ])
                        ->Where(['gudang_id' => $request['gudang'], 'produk_id' => $request['produk']])
                        ->groupBy('gudang_id')->get()->sum('qty');
            }else{
                $data = $this->saldoAwalBarang
                        ->select([
                            \DB::Raw('sum(kuantitas) as qty'),
                        ])
                        ->Where('produk_id', '=', $request['produk'])
                        ->groupBy('gudang_id')->get()->sum('qty');
            }

            return response()->json($data);
        }
    }

    public function getListHistoryPriceItem(Request $data) {
        $getPriceHistory = null;
        if ( $data['type'] == 'penjualan' ) {
            $getPriceHistory = $this->fakturPenjualan->where('pelanggan_id', $data['sov'])->whereHas('barang', function ( $queryPrice ) use ( $data ) {
                $queryPrice->where('produk_id', $data['item']);
            })->orderBy('created_at', 'desc')->limit(5)->get();
        } elseif ( $data['type'] == 'pembelian' ) {
            $getPriceHistory = $this->fakturPembelian->where('pemasok_id', $data['sov'])->whereHas('barang', function ( $queryPrice ) use ( $data ) {
                $queryPrice->where('produk_id', $data['item']);
            })->orderBy('created_at', 'desc')->limit(5)->get();
        }
        
        $dataPriceHistory = $getPriceHistory->map( function ( $itemPriceHistory ) use ( $data ) {
            return $itemPriceHistory->barang->map( function ( $dataPrice ) use ( $data ) {
                if ( $dataPrice->produk_id == $data['item'] ) {
                    return [
                        'tanggal' => Carbon::parse($dataPrice->created_at)->format('d F Y'),
                        'kuantitas'=> $dataPrice->jumlah,
                        'unit' => $dataPrice->item_unit,
                        'harga'=> $dataPrice->unit_price,
                        'diskon' => $dataPrice->diskon
                    ];
                }
            })->filter()->values();
        })->flatten(1);
        
        return response()->json($dataPriceHistory);
    }

    public function getListTerms (Request $data) {
        if ( !empty($data['search']) ) {
            $terms = $this->syaratPembayaran->where('nama', 'like', '%'.$data['search'].'%')
                        ->orWhere('akan_dapat_diskon', 'like', '%'.$data['search'].'%');
        } else {
            $terms = $this->syaratPembayaran;
        }
        $getTerms = $terms->get()->map(function ($item) {
            $dapat_diskon = $item['akan_dapat_diskon'] ?? '-';
            $membayar_antara = $item['jika_membayar_antara'] ?? '-';
            $jatuh_tempo = $item['jatuh_tempo'] ?? '-' ;
            $data = $item['nama'] != null ? $item['nama'] : $dapat_diskon.'/'.$membayar_antara.'/'.$jatuh_tempo;
            return ['id' => $item['id'], 'nama' => $data ];
        });
        return response()->json($getTerms);
    }
    
    public function getListShips (Request $data) {
        if ( !empty($data['search']) ) {
            $ship = $this->jasaPengiriman->where('nama', 'like', '%'.$data['search'].'%');
        } else {
            $ship = $this->jasaPengiriman;
        }

        $getShip = $ship->get()->map(function ($item) {
            return ['id' => $item['id'], 'nama' => $item['nama'] ];
        });

        return response()->json($getShip);
    }
}
