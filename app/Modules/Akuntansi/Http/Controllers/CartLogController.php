<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use Generator\Interfaces\RepositoryInterface;

class CartLogController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Cart Logs';
    }

    public function formData()
	{
		return [
			'filter' => [''],
		];
    }    
    
    public function index()
	{
		return parent::index()->with($this->formData());
	}
}
