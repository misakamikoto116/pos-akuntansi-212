<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Repositories\DataKaryawanRepository;
use App\Modules\Akuntansi\Http\Requests\DataKaryawanRequest;
use Generator\Interfaces\RepositoryInterface;

class DataKaryawanController extends Controller
{
    /**
     * @var DataKaryawanRepository
     */
    protected $model;

    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Karyawan';
        $this->request = DataKaryawanRequest::class;
        $this->requestField = [
            'nik_karyawan',
            'nip_karyawan',
            'nama_karyawan',
            'jenis_kelamin',
            'tempat_lahir',
            'tgl_lahir',
            'alamat_karyawan',
            'no_telp',
            'jabatan',
            'status',
        ];
    }
}
