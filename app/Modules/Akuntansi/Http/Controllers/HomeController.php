<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\PreferensiBarang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Produk $produk, PreferensiBarang $preferensi_barang, SaldoAwalBarang $saldo_awal_barang)
    {
        $this->middleware('auth');
        $this->saldo_awal_barang    = $saldo_awal_barang;
        $this->produk               = $produk;
        $this->preferensi_barang    = $preferensi_barang;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $count_penjualan = FakturPenjualan::where('uang_muka', 0)->count();
        $penjualan = new BarangFakturPenjualan();
        $grafikPenjualan = $penjualan
            ->addSelect(\DB::raw('sum(harga_final * jumlah) as total_penjualan')) //->
            ->addSelect(\DB::raw('sum(harga_final * jumlah - (harga_modal * jumlah)) as laba_rugi')) //->
            ->addSelect(\DB::raw('sum(harga_modal * jumlah) as modal')) //->
            ->addSelect(\DB::raw('month(created_at) as bulan_int'))
            ->groupBy(\DB::raw('month(created_at)'))
            ->addSelect(\DB::raw('CASE
                WHEN month(created_at) = "1" THEN "Jan"
                WHEN month(created_at) = "2" THEN "Feb"
                WHEN month(created_at) = "3" THEN "Mar"
                WHEN month(created_at) = "4" THEN "Apr"
                WHEN month(created_at) = "5" THEN "Mei"
                WHEN month(created_at) = "6" THEN "Jun"
                WHEN month(created_at) = "7" THEN "Jul"
                WHEN month(created_at) = "8" THEN "Agu"
                WHEN month(created_at) = "9" THEN "Sept"
                WHEN month(created_at) = "10" THEN "Okt"
                WHEN month(created_at) = "11" THEN "Nop"
                WHEN month(created_at) = "12" THEN "Des"
            END as bulan_text'))
            ->where(\DB::raw('year(created_at)'), date('Y'))
            ->get();
        $grafikPie = $penjualan
            ->with('produk')
            ->addSelect(\DB::raw('sum(jumlah) as jml_penjualan'))
            ->addSelect('produk_id')
            ->groupBy(\DB::raw('produk_id'))
            ->where(\DB::raw('year(created_at)'), date('Y'))
            ->orderBy(\DB::raw('sum(jumlah)'), 'DESC')
            ->limit(3)
            ->get();
        // $badge_harga_jual = 0;
        // $badge_barang_expired = 0 ;
        // if($this->getHargaJualBarang())
        //     $badge_harga_jual = $this->getHargaJualBarang()->total();
        // if($this->getBarangExpired())
        //     $badge_barang_expired = $this->getBarangExpired()->total();

        $view = [
            'total_penjualan'       => $grafikPenjualan->sum('total_penjualan'),
            'total_transaksi'       => $count_penjualan,
            'laba_rugi'             => $grafikPenjualan->sum('laba_rugi'),
            'grafik_penjualan'      => $grafikPenjualan,
            'grafik_pie'            => $grafikPie,
            'harga_jual_barang'     => $this->getHargaJualBarang() ,
            'qty_minimal_stock'     => $this->getQtyBarangMinimalStock(),
            'harga_modal'           => $this->getHargajualKurangDariHargaModal(),
            'barang_expired'        => $this->getBarangExpired(),
            'badge_harga_jual'      => $this->cekDataNull($this->getHargaJualBarang()),
            'badge_minimal_stock'   => $this->cekDataNull($this->getQtyBarangMinimalStock()),
            'badge_harga_modal'     => $this->cekDataNull($this->getHargajualKurangDariHargaModal()),
            'badge_barang_expired'  => $this->cekDataNull($this->getBarangExpired()),

        ];

        return view('chichi_theme.dashboard.home.dashboard')
            ->with($view);
    }

    public function indexHargaJualNol(){
        return view('chichi_theme.dashboard.home.components_page.harga_jual_nol')->with(['item' => $this->getHargaJualBarang()]);
    }

    public function indexMinimalStock(){
        return view('chichi_theme.dashboard.home.components_page.minimal_stock')->with(['item' => $this->getQtyBarangMinimalStock()]);
    }

    public function indexHargaModal(){
        return view('chichi_theme.dashboard.home.components_page.harga_modal')->with(['item' => $this->getHargajualKurangDariHargaModal()]);
    }

    public function indexKadaLuarsa(){
        return view('chichi_theme.dashboard.home.components_page.barang_kadaluarsa')->with(['item' => $this->getBarangExpired()]);
    }

    public function getHargaJualBarang()
    {
        $harga_jual_barang = $this->produk->with('saldoAwalBarang')
                                          ->where('harga_jual','<=', 0)
                                          ->orWhere('harga_jual', null)
                                          ->whereNotIn('tipe_barang', [3,4,5])
                                          ->paginate(10)->appends('aktif_tab','harga_jual');
        if($harga_jual_barang->isEmpty())
            return null;

        return $harga_jual_barang;
    }

    public function getQtyBarangMinimalStock()
    {
        $minimal_stock = $this->preferensi_barang->firstOrFail()->minimal_stock;

        $barang_minimal_stock = $this->produk->with('saldoAwalBarang')
                                             ->whereNotIn('tipe_barang',[3,4,5])
                                             ->whereHas('saldoAwalBarang', function ($query) use ($minimal_stock)
                                             {
                                                $query->select(DB::raw('sum(kuantitas) qty'))
                                                      ->having('qty','<=', $minimal_stock); 
                                             })
                                             ->paginate(10)->appends('aktif_tab','minimal_stock');
         

        $barang_minimal_stock->getCollection()->transform(function ($item) use ($minimal_stock)
        {
            return [
                'id'            => $item->id,
                'no_barang'     => $item->no_barang,
                'nama_barang'   => $item->keterangan,
                'satuan'        => $item->unit,
                'minimal_stock' => $minimal_stock,
                'qty_sekarang'  => $item->updated_qty,
            ];
        });
        if($barang_minimal_stock->isEmpty())
            return null;

        return $barang_minimal_stock;
    }

    public function getHargajualKurangDariHargaModal()
    {
        $saldo_awal_barang = $this->saldo_awal_barang
                                  ->selectRaw('produk_id ,MAX(produk_detail.id), harga_modal')
                                  ->groupBy('produk_id')
                                  ->with('produk')
                                  ->whereHas('produk', function ($query)
                                  {
                                      $query->whereNotIn('tipe_barang', [3,4,5])
                                            ->whereRaw('produk.harga_jual <= produk_detail.harga_modal');
                                  })
                                  ->paginate(10)->appends('aktif_tab','saldo_awal');

        $saldo_awal_barang->getCollection()->transform(function ($item)
        {
          return [
                    'id'                    => $item->produk->id,
                    'no_barang'             => $item->produk->no_barang,
                    'nama_barang'           => $item->produk->keterangan,
                    'satuan'                => $item->produk->unit,
                    'harga_jual_sekarang'   => $item->produk->harga_jual,
                    'harga_modal_terakhir'  => $item->harga_modal,
                ];  
        });
        if(empty($saldo_awal_barang))
            return null;
        return $saldo_awal_barang;
    }
    // 1. Cari barang yang relasi barang faktur pembelian nya nda kosong
    // 2. cari selisih antar tanggal sekarang dan tanggal expired nya
    public function getBarangExpired()
    {

        $barang_expired = $this->produk->with('barangFakturPembelian')->has('barangFakturPembelian')->paginate(10)->appends('aktif_tab','barang_expired');

        if ($barang_expired->isNotEmpty()) {

            $estimasi_kada_luarsa  = $this->preferensi_barang->firstOrFail()->estimasi_kada_luarsa;

            $tanggal_sekarang   = Carbon::today();

            $barang_expired->getCollection()->transform(function ($item) use ($tanggal_sekarang, $estimasi_kada_luarsa)
            {
                $data_faktur_pembelian = $item->barangFakturPembelian->map(function ($item_faktur_pembelian) use ($tanggal_sekarang)
                {
                    $selisih_hari = Carbon::parse($tanggal_sekarang)->diffInDays($item_faktur_pembelian->expired_date, false);

                    $view = [
                        'selisih_hari'    => $selisih_hari,
                        'expired_date'    => $item_faktur_pembelian->expired_date,
                    ];

                    return $view;
                });

                $data = array_flatten($data_faktur_pembelian);

                if ($data[0] >= 0) {

                    if ($data[0] <= $estimasi_kada_luarsa) {
                        return [
                                  'id'              => $item->id,
                                  'no_barang'       => $item->no_barang,
                                  'nama_barang'     => $item->keterangan,
                                  'satuan'          => $item->unit,
                                  'expired_date'    => Carbon::parse($data[1])->format('d F Y'),
                                  'estimasi'        => $estimasi_kada_luarsa.' Hari',
                        ];
                    }
                
                }

            });
        }
        if($barang_expired->isEmpty())
            return null;
        return $barang_expired;
    }

    public function cekDataNull($data)
    {
        $html = '';
        if (!empty($data)) {
            $html = 'success';
        }else {
            $html = 'danger';
        }

        return $html;
    }
}
