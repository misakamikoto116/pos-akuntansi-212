<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\BarangPenawaranPenjualan;
use App\Modules\Akuntansi\Models\BarangPengirimanPenjualan;
use App\Modules\Akuntansi\Models\BarangPesananPenjualan;
use App\Modules\Akuntansi\Models\BarangReturPenjualan;
use App\Modules\Akuntansi\Models\DaftarAktivaTetap;
use App\Modules\Akuntansi\Models\DaftarAktivaTetapDetail;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\JurnalUmum;
use App\Modules\Akuntansi\Models\PenawaranPenjualan;
use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use App\Modules\Akuntansi\Models\PesananPenjualan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\ReturPenjualan;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\TipeAktivaTetapPajak;
use App\Modules\Akuntansi\Models\TipePelanggan;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\TransaksiUangMuka;
use App\Modules\Akuntansi\Models\User;
use Carbon\Carbon;

trait TraitLaporan
{
    public function __construct(
        DaftarAktivaTetap $daftarAktivaTetap,       DaftarAktivaTetapDetail $daftarAktivaTetapdetail,
        PenawaranPenjualan $penawaranPenjualan,     BarangPenawaranPenjualan $barangPenawaranPenjualan,
        PesananPenjualan $pesananPenjualan,         BarangPesananPenjualan $barangPesananPenjualan,
        PengirimanPenjualan $pengirimanPenjualan,   BarangPengirimanPenjualan $barangPengirimanPenjualan,
        FakturPenjualan $fakturPenjualan,           BarangFakturPenjualan $barangFakturPenjualan,
        ReturPenjualan $returPenjualan,             BarangReturPenjualan $barangReturPenjualan,
        Akun $akun,                                 Identitas $identitas,
        InformasiPelanggan $informasiPelanggan,     JurnalUmum $jurnalUmum,
        Produk $produk,                             SaldoAwalBarang $saldoAwalBarang,
        Transaksi $transaksi,                       TransaksiUangMuka $transaksiUangMuka,
        TipeAktivaTetapPajak $tipeAktivaTetapPajak, TipePelanggan $tipePelanggan,
        User $user

    ){
        $this->akun                         =   $akun;
        $this->identitas                    =   $identitas;
        $this->informasiPelanggan           =   $informasiPelanggan;
        $this->jurnalUmum                   =   $jurnalUmum;
        $this->produk                       =   $produk;
        $this->saldoAwalBarang              =   $saldoAwalBarang;
        $this->transaksi                    =   $transaksi;
        $this->penawaranPenjualan           =   $penawaranPenjualan;
        $this->barangPenawaranPenjualan     =   $barangPenawaranPenjualan;
        $this->pesananPenjualan             =   $pesananPenjualan;
        $this->barangPesananPenjualan       =   $barangPesananPenjualan;
        $this->fakturPenjualan              =   $fakturPenjualan;
        $this->barangFakturPenjualan        =   $barangFakturPenjualan;
        $this->returPenjualan               =   $returPenjualan;
        $this->barangReturPenjualan         =   $barangReturPenjualan;
        $this->transaksiUangMuka            =   $transaksiUangMuka;
        $this->pengirimanPenjualan          =   $pengirimanPenjualan;
        $this->barangPengirimanPenjualan    =   $barangPengirimanPenjualan;
        $this->daftarAktivaTetap            =   $daftarAktivaTetap;
        $this->daftarAktivaTetapDetail      =   $daftarAktivaTetapdetail;
        $this->tipeAktivaTetapPajak         =   $tipeAktivaTetapPajak;
        $this->tipePelanggan                =   $tipePelanggan;
        $this->user                         =   $user;
    }

    protected function identitasPerushaan()
    {
        $nama_perusahaan        =       $this->identitas->first()->nama_perusahaan ?? null;
        return $nama_perusahaan;
    }
    public function dateCarbon($ubah)
    {
        $format     =       Carbon::parse($ubah)->format('d F Y');
        return $format;
    }

    public function yearMonthCarbon($ubah)
    {
        $format     =       Carbon::parse($ubah)->format('F Y');
        return $format;
    }

    protected function requestDateCondition($requestStart, $requestEnd){
        $requestStart === null ? $startDate = date("Y-m-01") : $startDate = $requestStart ;
        $requestEnd   === null ? $endDate   = date("Y-m-t")  : $endDate   = $requestEnd ;
        return [
            'start-date'    => $startDate,
            'end-date'      => $endDate
        ];
    }

    public function generateSubTitleDMY($date){
        if($date['start-date'] && $date['end-date']){
            if($date['start-date'] === $date['end-date']){
                return 'Per Tanggal '.$this->dateCarbon($date['start-date']);
            }else{
                return 'Dari Tanggal '.$this->dateCarbon($date['start-date']).' Ke Tanggal '.$this->dateCarbon($date['end-date']);
            }
        }else{
            return 'Dari Tanggal '.$this->dateCarbon(date('Y-m')."-01").' Ke Tanggal '.$this->dateCarbon(date('Y-m-t'));
        }
    }

    public function generateViewData($title, $requestDate, $collection){
        $date = $this->requestDateCondition($requestDate['start-date'], $requestDate['end-date']);
        return [
            'title'         =>  $title,
            'subTitle'      =>  $this->generateSubTitleDMY($requestDate),
            'perusahaan'    =>  $this->identitasPerushaan(),
            'dari_tanggal'  =>  $date['start-date'],
            'ke_tanggal'    =>  $date['end-date'],
            'item'          =>  $collection,
        ];
    }

    protected function PDFPath($titlePdf, $functionName){
        if(class_basename($this) === 'LaporanPenjualanController'){
            $pathName = 'penjualan';
        }
        $path   =   storage_path('app/public/laporan/'.$pathName.'/'.$functionName.'/'. $titlePdf);
        return $path;
    }

    protected function PDFGenerator($title, $request, $arrayToCollection, $filePath){
        $viewPDF    = $this->generateViewData($title, $request, $arrayToCollection);
        switch ($title) {
            case 'Laporan Penjualan Perbarang':
                $html       = $this->html_penjualan_perbarang($viewPDF);
            break;
            case 'Laporan Penjualan per Pelanggan':
                $html       = $this->html_penjualan_per_pelanggan($viewPDF);
            break;
            default:
                abort(404);
            break;
        }
        $pdf        = App::make('dompdf');
        $pdf->loadHTML($html);
        $pdf->render();
        $output = $pdf->output();
        file_put_contents($filePath, $output);
        return true;
    }
        
    // -----------------------------------------------------------

    // Generate CSS Untuk HTML Ke PDF
    protected function css_generate_pdf(){
        $css = '<head>
                    <style>
                        .table {
                            width: 100%;
                            max-width: 100%;
                            margin-bottom: 1rem;
                            background-color: transparent;
                        }
                        .table th,
                        .table td {
                            padding: 0.75rem;
                            vertical-align: top;
                            border-top: 1px solid #e9ecef;
                        }
                        .table tbody + tbody {
                            border-top: 2px solid #e9ecef;
                        }
                        .table .table {
                            background-color: #fff;
                        }
                        .row {
                            display: -ms-flexbox;
                            display: flex;
                            -ms-flex-wrap: wrap;
                                flex-wrap: wrap;
                            margin-right: -15px;
                            margin-left: -15px;
                        }
                        .no-gutters {
                            margin-right: 0;
                            margin-left: 0;
                        }
                        .no-gutters > .col,
                        .no-gutters > [class*="col-"] {
                            padding-right: 0;
                            padding-left: 0;
                        }
                        .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,
                        .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,
                        .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,
                        .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,
                        .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,
                        .col-xl-auto {
                            position: relative;
                            width: 100%;
                            min-height: 1px;
                            padding-right: 15px;
                            padding-left: 15px;
                        }
                        .col {
                            -ms-flex-preferred-size: 0;
                                flex-basis: 0;
                            -ms-flex-positive: 1;
                                flex-grow: 1;
                            max-width: 100%;
                        }
                        .col-auto {
                            -ms-flex: 0 0 auto;
                                flex: 0 0 auto;
                            width: auto;
                            max-width: none;
                        }
                        .col-1 {
                            -ms-flex: 0 0 8.333333%;
                                flex: 0 0 8.333333%;
                            max-width: 8.333333%;
                        }
                        .col-2 {
                            -ms-flex: 0 0 16.666667%;
                                flex: 0 0 16.666667%;
                            max-width: 16.666667%;
                        }
                        .col-3 {
                            -ms-flex: 0 0 25%;
                                flex: 0 0 25%;
                            max-width: 25%;
                        }
                        .col-4 {
                            -ms-flex: 0 0 33.333333%;
                                flex: 0 0 33.333333%;
                            max-width: 33.333333%;
                        }
                        .col-5 {
                            -ms-flex: 0 0 41.666667%;
                                flex: 0 0 41.666667%;
                            max-width: 41.666667%;
                        }
                        .col-6 {
                            -ms-flex: 0 0 50%;
                                flex: 0 0 50%;
                            max-width: 50%;
                        }
                        .col-7 {
                            -ms-flex: 0 0 58.333333%;
                                flex: 0 0 58.333333%;
                            max-width: 58.333333%;
                        }
                        .col-8 {
                            -ms-flex: 0 0 66.666667%;
                                flex: 0 0 66.666667%;
                            max-width: 66.666667%;
                        }
                        .col-9 {
                            -ms-flex: 0 0 75%;
                                flex: 0 0 75%;
                            max-width: 75%;
                        }
                        .col-10 {
                            -ms-flex: 0 0 83.333333%;
                                flex: 0 0 83.333333%;
                            max-width: 83.333333%;
                        }
                        .col-11 {
                            -ms-flex: 0 0 91.666667%;
                                flex: 0 0 91.666667%;
                            max-width: 91.666667%;
                        }
                        .col-12 {
                            -ms-flex: 0 0 100%;
                                flex: 0 0 100%;
                            max-width: 100%;
                        }
                        .alert {
                            position: relative;
                            padding: 0.75rem 1.25rem;
                            margin-bottom: 1rem;
                            border: 1px solid transparent;
                            border-radius: 0.25rem;
                        }
                        .alert-warning {
                            color: #856404;
                            background-color: #fff3cd;
                            border-color: #ffeeba;
                        }
                        .border-top{
                            border-top: 2px solid black !important;
                        }
                        .border-bottom{
                            border-bottom: 2px solid black !important;
                        }
                        .align-mid{
                            text-align: center;
                        }
                        header, footer, .watermark{
                            display: none;
                        }
                        thead > tr > td {
                            border: none !important;
                            text-align: center;
                        }
                        thead > tr > th {
                            font-size: 16px;
                            color: #0066cc;
                        }
                        thead > tr > th:hover {
                            color: #cc0000;
                        }
                        tr td {
                            font-size: 12px;
                        }
                        .text-kanan {
                            text-align: right;
                        }
                        h3{
                            color: crimson;
                        }
                        tbody > tr > td{
                            padding: 4px 20px !important;
                        }
                        .text-left {
                            text-align: left !important;
                        }
                        .text-right {
                            text-align: right !important;
                        }
                        .text-center {
                            text-align: center !important;
                        }
                    </style>
                </head>';
        return $css;
    }

    // -----------------------------------------------------------
    
    // HTML to PDF
    protected function html_penjualan_perbarang($view){
        $html = '<html>'.
                    $this->css_generate_pdf().'
                    <body>
                        <div class="row">
                            <div class="col-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td colspan=4>
                                                <p><h1>'. $view['perusahaan'] .'</h1></p>
                                                <p><h3>'. $view['title'] .'</h3></p>
                                                <p><b>'.  $view['subTitle'] .'</b></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Keterangan Barang</th>
                                            <th>Kuantitas</th>
                                            <th>Unit 1</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                                        if($view['item']->isEmpty()){
                                            $html .='<tr>
                                                <td colspan=4 align="center">
                                                    <div class="alert alert-warning">
                                                        Tidak ada data '. $view['title'] .' yang tersedia <br>
                                                        '. $view['subTitle'] .'.
                                                    </div>
                                                </td>
                                            </tr>';
                                        }else{
                                            foreach($view['item'] as $item_key => $dataPenjualanPerbarang){
                                                foreach($dataPenjualanPerbarang as $item_array => $itemPenjualanPerbarang){
                                                    $html .='<tr class="post-original">
                                                        <td>'. $itemPenjualanPerbarang['nama_barang']          .'</td>
                                                        <td>'. $itemPenjualanPerbarang['kuantitas']            .'</td>
                                                        <td>'. $itemPenjualanPerbarang['satuan']               .'</td>
                                                        <td>'. number_format($itemPenjualanPerbarang['total']) .'</td>
                                                    </tr>';
                                                }
                                            }
                                            $html .='<tr class="post-data">
                                                <td>Total</td>
                                                <td class="border-top sumDataKuantitas">'.   number_format($itemPenjualanPerbarang['sumKuantitas'])     .'</td>
                                                <td></td>
                                                <td class="border-top sumDataTotal">'.   number_format($itemPenjualanPerbarang['sumTotal'])          .'</td>
                                            </tr>';
                                        }
                                    $html .='</tbody>
                                </table>
                            </div>
                        </div>
                    </body>
                </html>';
        return $html;
    }

    protected function html_penjualan_per_pelanggan($view){
        $html = '<html>'.
                    $this->css_generate_pdf().'
                    <body>
                        <div class="row">
                            <div class="col-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td colspan=4>
                                                <p><h1>'. $view['perusahaan'] .'</h1></p>
                                                <p><h3>'. $view['title'] .'</h3></p>
                                                <p><b>'.  $view['subTitle'] .'</b></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center" colspan=2>Nama Pelanggan</th>
                                            <th class="text-center" colspan=2>'. date('d F Y',strtotime($view['dari_tanggal'])).' - '. date('d F Y',strtotime($view['ke_tanggal'])) .'</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                                        if($view['item']->isEmpty()){
                                            $html .='<tr>
                                                <td colspan=4 align="center">
                                                    <div class="alert alert-warning">
                                                        Tidak ada data '. $view['title'] .' yang tersedia <br>
                                                        '. $view['subTitle'] .'.
                                                    </div>
                                                </td>
                                            </tr>';
                                        }else{
                                            foreach($view['item'] as $item_key => $data){
                                                $html .='<tr class="post-original">
                                                    <td class="text-left" colspan=2>'. $data['nama'] .'</td>
                                                    <td class="text-right" colspan=2>'. number_format($data['total'], 0) .'</td>
                                                </tr>';
                                            }
                                            $html .='<tr class="post-data">
                                                <td colspan=2>Total</td>
                                                <td class="border-top text-right" colspan=2>'.   number_format($data['sumTotal'])  .'</td>
                                            </tr>';
                                        }
                                    $html .='</tbody>
                                </table>
                            </div>
                        </div>
                    </body>
                </html>';
        return $html;
    }
}