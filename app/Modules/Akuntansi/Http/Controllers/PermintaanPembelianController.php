<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Models\PermintaanPembelian;
use App\Modules\Akuntansi\Http\Requests\PermintaanPembelianRequest;
use App\Modules\Akuntansi\Repositories\PermintaanPembelianRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
class PermintaanPembelianController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Permintaan Pembelian';
        $this->request = PermintaanPembelianRequest::class;
        $this->requestField = [
                    'request_no',
                    'request_date',
                    'status',
                    'catatan',
                    'produk_id',
                    'permintaan_pembelian_id',
                    'keterangan_produk',
                    'satuan_produk',
                    'qty_produk',
                    'required_date',
                    'lanjutkan',                                  
                    'notes',
        ];
    }
    public function formData()
    {
        return [
            'kategori' => $this->model->listKategoriProduk(),
            'gudang' => $this->model->listGudang(),
            'akun' => $this->model->listAkun(),
            'pelanggan' => $this->model->listPelanggan(),
            // 'produk' => $this->model->listProduk(),
            'idPrediction'   => $this->model->idPrediction(),     
            'status_penjualan' => collect(['0' => 'Sedang Di Proses', '1' => 'Ditutup','2' => 'Menunggu']),
            'filter' => ['status_penjualan', 'tanggal_dari_sampai','no_faktur','keterangan_pembayaran'],       
            // 'tipePelanggan' => $this->model->listTipePelanggan(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function redirectSuccess(Request $request, $result = null)
    {
        if(!empty($result['continue_stat'])){
            return redirect('/akuntansi/permintaan-pembelian/create')->withMessage('Berhasil Menambah/Memperbarui data');;
        }else{
            return redirect('/akuntansi/permintaan-pembelian')->withMessage('Berhasil Menambah/Memperbarui data');;
        }
    }
}
