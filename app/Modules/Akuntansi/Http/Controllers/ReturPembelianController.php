<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Http\Requests\ReturPembelianRequest;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class ReturPembelianController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Retur Pembelian';
        $this->request = ReturPembelianRequest::class;
        $this->requestField = [
            'pemasok_id',
            'faktur_pembelian_id',
            'invoice_no',
            'taxable',
            'in_tax',
            'return_no',
            'tanggal',
            'no_fiscal',
            'tgl_fiscal',
            'nilai_tukar',
            'nilai_tukar_pajak',
            'keterangan',
            'barang_faktur_pembelian_id',
            'produk_id',
            'keterangan_produk',
            'qty_produk',
            'satuan_produk',
            'unit_harga_produk',
            'tax_produk',
            'amount_produk',
            'gudang_id',
            'total',
            'harga_modal',
            'harga_terakhir',
            'lanjutkan',
            'tax_cetak0',
            'tax_cetak1'
        ];
    }

    public function formData()
    {
        return [
            'pemasok' => $this->model->listPemasok(),
            'idPrediction' => $this->model->idPrediction(),
            'kode_pajak' => $this->model->listKodePajak(),
            'filter' => ['tanggal_dari_sampai', 'pemasok_select','no_retur'],
            'gudang' => $this->model->listGudang(),
            // 'kategori' => $this->model->listKategoriProduk(),
            // 'akun' => $this->model->listAkun(),
            // 'produk' => $this->model->listProduk(),
            // 'termin' => $this->model->listTermin(),
            // 'pengiriman' => $this->model->listJasaPengiriman(),
            // 'tipePelanggan' => $this->model->listTipePelanggan(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function redirectSuccess(Request $request, $result = null)
    {
        if (!empty($result['continue_stat'])) {
            return redirect('/akuntansi/retur-pembelian/create')->withMessage('Berhasil Menambah/Memperbarui data');
        } else {
            return redirect('/akuntansi/retur-pembelian')->withMessage('Berhasil Menambah/Memperbarui data');
        }
    }
}
