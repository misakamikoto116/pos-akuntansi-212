<?php

namespace App\Modules\Akuntansi\Http\Controllers;
// use App\Modules\Akuntansi\Http\Requests\IdentitasRequest;
use App\Modules\Akuntansi\Repositories\RekapTransaksiRepository;
use Generator\Interfaces\RepositoryInterface;
class RekapTransaksiController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Rekap Transaksi';
        // $this->request = IdentitasRequest::class;
        // $this->requestField = ['nama_perusahaan','tanggal_mulai','alamat','mata_uang_id'];
    }


    public function index()
    {
        $data = $this->model->getRekapTransaksi();

        return view('akuntansi::rekap_transaksi.index')->with($data);
    }

    public function detailTransaksi($tanggal)
    {
        return $this->model->getDetailTransaksi($tanggal);
    }

    public function detailBarang($id)
    {
        return $this->model->getDetailBarang($id);
    }

    public function rekapKasir()
    {
        $request = request();
        return $this->model->getRekapKasir($request);
    }
}
