<?php
namespace App\Modules\Akuntansi\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\PenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\DetailPenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\PenerimaanPenjualan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\FakturPembelian;
use Carbon\Carbon;
use DB;

class LaporanPersediaanController extends Controller
{
	public function __construct(
		Akun $akun,
		DetailPenyesuaianPersediaan $detailPenyesuaianPersediaan,
		Identitas $identitas,
		PenerimaanPenjualan $penerimaanPenjualan,
		PenyesuaianPersediaan $penyesuaianPersediaan,
		Produk $produk,
		SaldoAwalBarang $saldoAwalBarang,
		Transaksi $transaksi,
		FakturPembelian $fakturPembelian,
		FakturPenjualan $fakturPenjualan
	)
	{
		$this->akun							=	$akun;
		$this->detailPenyesuaianPersediaan	=	$detailPenyesuaianPersediaan;
		$this->identitas 					=	$identitas;
		$this->penerimaanPenjualan 			=	$penerimaanPenjualan;
		$this->penyesuaianPersediaan 		=	$penyesuaianPersediaan;
		$this->produk 						=	$produk;
		$this->saldoAwalBarang 				=	$saldoAwalBarang;
		$this->transaksi 					=	$transaksi;
		$this->fakturPembelian = $fakturPembelian;
		$this->fakturPenjualan = $fakturPenjualan;
	}

	protected function identitasPerushaan()
	{
		$nama_perusahaan = $this->identitas->first()->nama_perusahaan;

		return $nama_perusahaan;
	}

	public function dateCarbon($ubah)
	{
		$format = Carbon::parse($ubah)->format('d F Y');

		return $format;
	}

	public function ringkasan_valuasi_persediaan(Request $request)
	{
		$produkDetail = $this->saldoAwalBarang->pluck('produk_id');
		$transaksi    = $this->produk->with(['saldoAwalBarang.transaksi' => function($query){
									$query->where('item_type', '!=', get_class($this->akun))
									->where('item_type', '!=', get_class($this->produk))
									->where('item_type', '!=', get_class($this->penerimaanPenjualan));
								}, 'saldoAwalBarang.produk'])
								->where('tipe_barang', '=', '0')
								->whereIn('id', $produkDetail)
								->DateFilter($request);
		$array = [];
		foreach ($transaksi->get() as $dataTransaksi) {
			$sumKtsK    = 0; $sumKtsM    = 0; $dataQty = 0;
			$saldoAwalKuantitas = 0;
			foreach ($dataTransaksi->saldoAwalBarang as $data) {
				$saldoAwalKuantitas += $data->kuantitas;
				$nilaiSaldo = $dataTransaksi->harga_standar_def * $dataTransaksi->kuantitas;
				if ($data->kuantitas > 0) {
					$qtyMasuk         = $data->kuantitas;
					$sumKtsM		  += $qtyMasuk;
					$biayaNilaiMasuk  = ($data->harga_terakhir ?? 0);
					$nilaiMasuk       = $biayaNilaiMasuk * $sumKtsM;
					$qtyKeluar        = 0;
					$biayaNilaiKeluar = 0;
					$nilaiKeluar      = 0;
				} elseif ($data->kuantitas < 0) {
					$qtyKeluar        = $data->kuantitas;
					$sumKtsK    	  += $qtyKeluar;
					$biayaNilaiKeluar = ($data->harga_terakhir ?? 0);
					$nilaiKeluar      = $biayaNilaiKeluar * $sumKtsK;
				}
				if ($data->transaksi) {
					$nilai = $nilaiMasuk - $nilaiKeluar;
					$array[] = [
						'no_barang'    => $dataTransaksi->no_barang,
						'nm_barang'    => $dataTransaksi->keterangan,
						'kts_saldo'    => $saldoAwalKuantitas,
						'nilai_saldo'  => $dataTransaksi->harga_standar_def * $dataTransaksi->kuantitas,
						'kts_masuk'    => $sumKtsM ?? 0,
						'nilai_masuk'  => $nilaiMasuk ?? 0,
						'kts_keluar'   => abs($sumKtsK) ?? 0,
						'nilai_keluar' => abs($nilaiKeluar) ?? 0,
						'kuantitas'    => $saldoAwalKuantitas,
						'nilai_akhir'  => abs($nilai),
					];
				}
			}
		}
		$newCollection = collect($array)->groupBy('no_barang');

		$view = [
			'title'        => 'Laporan Ringkasan Valuasi Persediaan',
			'perusahaan'   => $this->identitasPerushaan(),
			'dari_tanggal' => $this->dateCarbon($request['start-date']),
			'ke_tanggal'   => $this->dateCarbon($request['end-date']),
			'item'         => $newCollection,
		];

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_persediaan.ringkasan_valuasi_persediaan')->with($view);
	}

	public function rincian_valuasi_persediaan(Request $request)
	{
		$produkDetail = $this->saldoAwalBarang->pluck('produk_id');
		$transaksi    = $this->produk->with(['saldoAwalBarang.transaksi' => function($query){
									$query->where('item_type', '!=', get_class($this->akun))
									->where('item_type', '!=', get_class($this->produk))
									->where('item_type', '!=', get_class($this->penerimaanPenjualan));
								}, 'saldoAwalBarang.produk'])
								->where('tipe_barang', '=', '0')
								->whereIn('id', $produkDetail)
								->DateFilter($request);
		$array = [];
		foreach ($transaksi->get() as $dataTransaksi) {
			foreach ($dataTransaksi->saldoAwalBarang as $data) {
				$nilaiSaldo = $dataTransaksi->harga_standar_def * $dataTransaksi->kuantitas;
				$tempQty    = 0;
				if ($data->kuantitas > 0) {
					$qtyMasuk         = $data->kuantitas;
					$biayaNilaiMasuk  = ($data->harga_terakhir ?? 0);
					$nilaiMasuk       = $biayaNilaiMasuk * $qtyMasuk;
					$qtyKeluar        = 0;
					$biayaNilaiKeluar = 0;
					$nilaiKeluar      = 0;
					$qty              = ($qtyMasuk ?? 0);
					$nilai            = ($nilaiMasuk ?? 0);
				} elseif ($data->kuantitas < 0) {
					$qtyMasuk         = 0;
					$biayaNilaiMasuk  = 0;
					$nilaiMasuk       = 0;
					$qtyKeluar        = $data->kuantitas;
					$biayaNilaiKeluar = ($data->harga_terakhir ?? 0);
					$nilaiKeluar      = $biayaNilaiKeluar * $qtyKeluar;
					$qty              = ($data->kuantitas) - ($qtyKeluar ?? 0);
					$nilai            = ($nilaiKeluar ?? 0);
				}
				if ($data->transaksi) {
					$array[] = [
						'no_barang'          => $dataTransaksi->no_barang,
						'kts_saldo'          => $dataTransaksi->kuantitas,
						'nm_barang'          => $dataTransaksi->keterangan,
						'nilai_saldo'        => $nilaiSaldo,
						'tanggal_transaksi'  => $this->dateCarbon($data->tanggal) ?? null,
						'tipe_transaksi'     => $data->transaksi->sumber ?? null,
						'no_faktur'          => $data->transaksi->no_transaksi ?? 0,
						'kts_masuk'          => $qtyMasuk ?? 0,
						'biaya_nilai_masuk'  => $biayaNilaiMasuk ?? 0,
						'nilai_masuk'        => $nilaiMasuk ?? 0,
						'kts_keluar'         => $qtyKeluar ?? 0,
						'biaya_nilai_keluar' => $biayaNilaiKeluar ?? 0,
						'nilai_keluar'       => $nilaiKeluar ?? 0,
						'kuantitas'          => $qty ?? 0,
						'nilai_akhir'        => $nilai ?? 0,
					];
				}
			}
		}
		$newCollection = collect($array)->groupBy('no_barang');

		$view = [
			'title'        => 'Laporan Ringkasan Valuasi Persediaan',
			'perusahaan'   => $this->identitasPerushaan(),
			'dari_tanggal' => $this->dateCarbon($request['start-date']),
			'ke_tanggal'   => $this->dateCarbon($request['end-date']),
			'item'         => $newCollection,
		];

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_persediaan.rincian_valuasi_persediaan')->with($view);
	}

	public function rekap_umur_persediaan(Request $request)
	{
		$requestTanggal = $request['start-date'] === null ? date('Y-m-d') : $request['start-date'];

		$dataProduk    = $this->produk->pluck('id');
		$dataSaldoAwal = $this->saldoAwalBarang->with('produk')
								->select([
									DB::Raw('sum(produk_detail.kuantitas) as sumKuantitas'),
									DB::Raw('produk_detail.produk_id'),
									'id',
								])->whereIn('produk_id', $dataProduk)->groupBy('produk_id')->get();
		$dataSaldo = $dataSaldoAwal->transform(function ($querySaldo) use ($requestTanggal) {
			$saldoAwal  = $querySaldo->where('item_type', get_class($this->penyesuaianPersediaan))->where('produk_id', $querySaldo->produk_id)->first()['kuantitas'];

			$umurSaldo  = $querySaldo->produk->transaksiMany->sortBy('tanggal')->map(function ($transaksi) use (&$requestTanggal, &$querySaldo, &$saldoAwal) {
				$umurProduk = $transaksi->tanggal->diffInDays($requestTanggal);
				$sum = $querySaldo->sumKuantitas;
				
				return [
					'30'   			=> ($umurProduk >= 0 && $umurProduk <= 30) ? $sum ?? 0 : 0,
					'60'   			=> ($umurProduk >= 31 && $umurProduk <= 60) ? $sum ?? 0 : 0,
					'90'   			=> ($umurProduk >= 61 && $umurProduk <= 90) ? $sum ?? 0 : 0,
					'120'  			=> ($umurProduk >= 91 && $umurProduk <= 120) ? $sum ?? 0 : 0,
					'120+' 			=> ($umurProduk >= 121) ? $sum ?? 0 : 0,
				];
			});
			if($umurSaldo->isEmpty()) {
				$umurSaldo = [
					'30'   			=> 0,
					'60'   			=> 0,
					'90'   			=> 0,
					'120'  			=> 0,
					'120+' 			=> 0,
				];
				$umurSaldo = collect($umurSaldo);
			}else{
				$umurSaldo = $umurSaldo->last();
			}

			return [
				'kode_barang'   => $querySaldo->produk->no_barang,
				'nama_barang'   => $querySaldo->produk->keterangan,
				'kuantitas'     => $saldoAwal ?? 0,
				'umur_saldo'	=> $umurSaldo,
			];
		});
		
		$view = [
			'title'        => 'Laporan Rekap Umur Persediaan',
			'perusahaan'   => $this->identitasPerushaan(),
			'dari_tanggal' => $this->dateCarbon($request['start-date']),
			'ke_tanggal'   => $this->dateCarbon($request['end-date']),
			'item'         => $dataSaldo,
		];

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_persediaan.rekap_umur_persediaan')->with($view);
	}

	public function rincian_umur_persediaan(Request $request)
	{
		$tanggalRequest = is_string($request->get('start-date')) ? Carbon::parse($request->get('start-date')) : Carbon::now();
		if( $request->get('barang_rincian_umur') != 0 )
		{
			$queryFiter = $this->produk->where('id', $request['barang_rincian_umur']);
		} else {
			$queryFiter = $this->produk;
		}
		$produks = $queryFiter->whereNotIn('tipe_barang', [3])->get()->map(function ($produk) use ($tanggalRequest) {
			$temp = [
				'nama_produk' => $produk->keterangan,
				'no_produk'   => $produk->no_barang,
				'sum'         => $produk->SumQtyProduk,
				'transaksi'   => $produk->saldoAwalBarang->sortBy('tanggal')->map(function ($saldoAwal) use ($tanggalRequest) {
					$umurProduk = $saldoAwal->tanggal->diffInDays($tanggalRequest);
					$sum = $saldoAwal->kuantitas;

					return [
						'no_transaksi'    => $saldoAwal->transaksi['no_transaksi'],
						'tanggal'         => $saldoAwal->tanggal->toDateString(),
						'jenis_transaksi' => ucwords($saldoAwal->transaksi['sumber']),
						'kuantitas'       => [
							'30'   => ($umurProduk >= 0 && $umurProduk <= 30) ? $sum : 0,
							'60'   => ($umurProduk >= 31 && $umurProduk <= 60) ? $sum : 0,
							'90'   => ($umurProduk >= 61 && $umurProduk <= 90) ? $sum : 0,
							'120'  => ($umurProduk >= 91 && $umurProduk <= 120) ? $sum : 0,
							'120+' => ($umurProduk >= 121) ? $sum : 0,
						],
					];
				}),
			];
			foreach ([30, 60, 90, 120, '120+'] as $group) {
				$temp['sumUmur'][$group] = $temp['transaksi']->reduce(function ($sum, $saldoAwal) use ($group) {
					return $sum + $saldoAwal['kuantitas'][$group];
				}, 0);
			}

			return $temp;
		});

		$view = [
			'title'        => 'Laporan Rincian Umur Persediaan',
			'perusahaan'   => $this->identitasPerushaan(),
			'dari_tanggal' => $this->dateCarbon($request['start-date']),
			'ke_tanggal'   => $this->dateCarbon($request['end-date']),
			'item'         => $produks,
		];

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_persediaan.rincian_umur_persediaan')->with($view);
	}

	// Mapping
	public function RincianUmurPersediaan($dataSaldoAwal)
	{
	}

	public function kartu_stok_persediaan(Request $request)
	{
		$produkDetail = $this->saldoAwalBarang->pluck('produk_id');
		$transaksi    = $this->produk->with(['saldoAwalBarang.transaksi' => function($query){
											$query->where('item_type', '!=', get_class($this->akun))
												  ->where('item_type', '!=', get_class($this->produk)
											);
										},'saldoAwalBarang.produk', 'gudang'])
							->where('tipe_barang', '=', '0')
							->whereIn('id', $produkDetail)
							->DateFilter($request)->get();
		$Collection = $transaksi->map(function ( $query ) {
			$item = $query->saldoAwalBarang->map(function ( $querySaldoAwal ) use ( $query ) {
				$data = $querySaldoAwal->transaksi;
				if($data) {
					$allInOne = collect();
					if($data->item->getTable() === "penerimaan_pembelian") {
						$allInOne = $data->item->barang;
					} elseif($data->item->getTable() === "faktur_pembelian") {
						$allInOne = $data->item->barang;
					} elseif($data->item->getTable() === "retur_pembelian") {
						$allInOne = $data->item->barang;
					} elseif($data->item->getTable() === "pengiriman_penjualan") {
						$allInOne = $data->item->barang;
					} elseif($data->item->getTable() === "retur_penjualan") {
						$allInOne = $data->item->barang;
					} elseif($data->item->getTable() === "penyesuaian_persediaan") {
						$allInOne = $data->item->detailPenyesuaian;
					} elseif($data->item->getTable() === "pembiayaan_pesanan") {
						$allInOne = $data->item->detail;
					} elseif($data->item->getTable() === "penyelesaian_pesanan") {
						$allInOne = $data->item->item;
					}
					if( !$allInOne->isEmpty() ){
						return $allInOne->map( function ( $queryAllInOne ) use ( &$query, &$querySaldoAwal, &$data ) {
							if ($querySaldoAwal->kuantitas > 0) {
								$qtyMasuk  = $querySaldoAwal->kuantitas;
								$qtyKeluar = 0;
								$qty       = ($qtyMasuk ?? 0);
							} elseif ($querySaldoAwal->kuantitas < 0) {
								$qtyMasuk  = 0;
								$qtyKeluar = $querySaldoAwal->kuantitas;
								$qty       = ($qtyKeluar ?? 0);
							}
							return [
								'tanggal'    => $this->dateCarbon($querySaldoAwal->tanggal) ?? null,
								'tipe'       => $data->sumber ?? null,
								'no_faktur'  => $data->no_transaksi ?? 0,
								'keterangan' => $data->keterangan,
								'kts_masuk'  => $qtyMasuk ?? 0,
								'kts_keluar' => $qtyKeluar ?? 0,
								'kuantitas'  => $qty ?? 0,
								'gudang'     => $queryAllInOne->gudang->nama ?? 0,
							];
						});
					}
				}
			})->filter(function ($key) {
				return $key;
			})->values();

			return [
				'no_barang'  => $query->no_barang,
				'nm_barang'  => $query->keterangan,
				'kts_saldo'  => $query->SumQtyAwal,
				'item'		 => $item,
			];
		});

		$view = [
			'title'        => 'Laporan Kartu Stok Persediaan',
			'perusahaan'   => $this->identitasPerushaan(),
			'dari_tanggal' => $this->dateCarbon($request['start-date']),
			'ke_tanggal'   => $this->dateCarbon($request['end-date']),
			'item'         => $Collection,
		];

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_persediaan.kartu_stok_persediaan')->with($view);
	}

	public function histori_penyesuaian_harga_jual_barang(Request $request)
	{
		$produk = $this->produk->with('tingkatHargaBarang')
							->groupBy('no_barang')
							->orderBy('created_at', 'asc');
		$array = [];
		foreach ($produk->get() as $dataProduk) {
			if ($dataProduk->tingkatHargaBarang) {
				$satu  = $dataProduk->tingkatHargaBarang->tingkat_1 ?? 0;
				$dua   = $dataProduk->tingkatHargaBarang->tingkat_2 ?? 0;
				$tiga  = $dataProduk->tingkatHargaBarang->tingkat_3 ?? 0;
				$empat = $dataProduk->tingkatHargaBarang->tingkat_4 ?? 0;
				$lima  = $dataProduk->tingkatHargaBarang->tingkat_5 ?? 0;
			} else {
				$satu  = 0;
				$dua   = 0;
				$tiga  = 0;
				$empat = 0;
				$lima  = 0;
			}
			$array[] = [
				'no_barang'     => $dataProduk->no_barang,
				'nm_barang'     => $dataProduk->keterangan,
				'tingkat_satu'  => $satu ?? 0,
				'tingkat_dua'   => $dua ?? 0,
				'tingkat_tiga'  => $tiga ?? 0,
				'tingkat_empat' => $empat ?? 0,
				'tingkat_lima'  => $lima ?? 0,
			];
		}
		$newCollection = collect($array)->groupBy('no_barang');

		$view = [
			'title'        => 'Laporan Histori Penyesuaian Harga Jual',
			'perusahaan'   => $this->identitasPerushaan(),
			'dari_tanggal' => $this->dateCarbon($request['start-date']),
			'ke_tanggal'   => $this->dateCarbon($request['end-date']),
			'item'         => $newCollection,
		];

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_persediaan.histori_penyesuaian_harga_jual_barang')->with($view);
	}

	public function ringkasan_analisis_persediaan(Request $request)
	{ // masih belum selesai sisa penjualan perminggu
		$dataProduk          = $this->produk->where('tipe_barang', '!=', 3)->pluck('id');
		$dataSaldoAwalBarang = $this->saldoAwalBarang->with('produk.barangPesananPenjualan.pesananPenjualan', 'produk.barangPesananPembelian.pesananPembelian')
									->select([
										DB::Raw('sum(produk_detail.kuantitas) as sumKuantitas'),
										DB::Raw('produk_detail.kuantitas as kuantitas'),
										'produk_id',
									])
									->whereIn('produk_id', $dataProduk)->groupBy('produk_id')->get();
		$array       = [];
		$sumDiTangan = 0;
		$sumDiPesan  = 0;
		$sumDiJual   = 0;
		$sumKtsStok  = 0;
		foreach ($dataSaldoAwalBarang as $itemSaldoAwalBarang) {
			if ($itemSaldoAwalBarang->produk->barangPesananPembelian) {
				$itemDiPesan = $itemSaldoAwalBarang->produk->barangPesananPembelian
								->whereHas('pesananPembelian', function ($query) {
									$query->where('status', 0);
								})->where('produk_id', $itemSaldoAwalBarang->produk_id)->sum('jumlah');
			} elseif ($itemSaldoAwalBarang->produk->barangPesananPenjualan) {
				$itemDiJual = $itemSaldoAwalBarang->produk->barangPesananPenjualan
								->whereHas('pesananPenjualan', function ($query) {
									$query->where('status', 0);
								})->where('produk_id', $itemSaldoAwalBarang->produk_id)->sum('jumlah');
			}
			$diTangan = $itemSaldoAwalBarang->sumKuantitas;
			$sumDiTangan += $diTangan;
			$diPesan = $itemDiPesan ?? 0;
			$sumDiPesan += $diPesan ?? 0;
			$diJual = $itemDiJual   ?? 0;
			$sumDiJual += $diJual   ?? 0;
			$ktsStok = ($diTangan + $diPesan) - $diJual;
			$sumKtsStok += $ktsStok;

			$array[] = [
				'nama_barang'   => $itemSaldoAwalBarang->produk->keterangan,
				'ditangan'      => $diTangan,
				'dipesan'       => $diPesan,
				'dijual'        => $diJual,
				'akumulasi'     => 0, // masih belum tau rumusnya
				'kts_stok'      => $ktsStok,
				'satuan'        => $itemSaldoAwalBarang->produk->unit,
				'sum_ditangan'  => $sumDiTangan,
				'sum_dipesan'   => $sumDiPesan,
				'sum_dijual'    => $sumDiJual,
				'sum_akumulasi' => 0, // masih belum tau rumusnya
				'sum_kts_stok'  => $sumKtsStok,
			];
		}
		$newCollection = collect($array)->groupBy('nama_barang');
		$view          = [
			'title'        => 'Laporan Ringkasan Analisis Persediaan',
			'perusahaan'   => $this->identitasPerushaan(),
			'dari_tanggal' => $this->dateCarbon($request['start-date']),
			'item'         => $newCollection,
		];

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_persediaan.ringkasan_analisis_persediaan')->with($view);
	}

	public function histori_barang(Request $request)
	{
		$dataSaldoAwalBarang = $this->saldoAwalBarang->pluck('produk_id');
		$dataproduk          = $this->produk->with(['saldoAwalBarang.transaksi' => function($query){
												$query->where('item_type', '!=', get_class($this->akun))
												  	  ->where('item_type', '!=', get_class($this->produk))
												  	  ->where('item_type', '!=', get_class($this->penerimaanPenjualan));
											}])
									->whereIn('id', $dataSaldoAwalBarang)->DateFilter($request)
									->groupBy('no_barang')->orderBy('created_at', 'asc');
		$array = [];
		foreach ($dataproduk->get() as $itemProduk) {
			$saldo        = $itemProduk->kuantitas;
			$sumQtyMasuk  = 0;
			$sumQtyKeluar = 0;
			foreach ($itemProduk->saldoAwalBarang as $itemSaldoAwal) {
				if ($itemSaldoAwal->transaksi) {
					if ($itemSaldoAwal->transaksi->item->getTable() === "faktur_pembelian"){
						$allInOne = $itemSaldoAwal->transaksi->item->barang;
					} elseif($itemSaldoAwal->transaksi->item->getTable() === "retur_pembelian") {
						$allInOne = $itemSaldoAwal->transaksi->item->barang;
					} elseif($itemSaldoAwal->transaksi->item->getTable() === "pengiriman_penjualan") {
						$allInOne = $itemSaldoAwal->transaksi->item->barang;
					} elseif($itemSaldoAwal->transaksi->item->getTable() === "retur_penjualan") {
						$allInOne = $itemSaldoAwal->transaksi->item->barang;
					} elseif($itemSaldoAwal->transaksi->item->getTable() === "penyesuaian_persediaan") {
						$allInOne = $itemSaldoAwal->transaksi->item->detailPenyesuaian;
					} elseif($itemSaldoAwal->transaksi->item->getTable() === "pembiayaan_pesanan") {
						$allInOne = $itemSaldoAwal->transaksi->item->detail;
					} elseif($itemSaldoAwal->transaksi->item->getTable() === "penyelesaian_pesanan") {
						$allInOne = $itemSaldoAwal->transaksi->item->item;
					}
					foreach ($allInOne as $itemAktivitas) {
						if (strpos($itemAktivitas->getTable(), 'pembelian')) {
							$qtyMasuk  = $itemAktivitas->jumlah;
							$qtyKeluar = 0;
							$saldo       += $qtyMasuk;
							$sumQtyMasuk += $qtyMasuk;
						} elseif (strpos($itemAktivitas->getTable(), 'penjualan')) {
							$qtyMasuk  = 0;
							$qtyKeluar = $itemAktivitas->jumlah;
							$saldo -= $qtyKeluar;
							$sumQtyKeluar += $qtyKeluar;
						} elseif (strpos($itemAktivitas->getTable(), 'penyesuaian')) {
							$qtyMasuk  = 0;
							$qtyKeluar = $itemAktivitas->jumlah;
							$saldo -= $qtyKeluar;
							$sumQtyKeluar += $qtyKeluar;
						} elseif (strpos($itemAktivitas->getTable(), 'pembiayaan')) {
							$qtyMasuk  = 0;
							$qtyKeluar = $itemAktivitas->jumlah;
							$saldo -= $qtyKeluar;
							$sumQtyKeluar += $qtyKeluar;
						} elseif (strpos($itemAktivitas->getTable(), 'penyelesaian')) {
							$qtyMasuk  = 0;
							$qtyKeluar = $itemAktivitas->jumlah;
							$saldo -= $qtyKeluar;
							$sumQtyKeluar += $qtyKeluar;
						}
						$array[] = [
							'kode_barang'      => $itemProduk->no_barang,
							'nama_barang'      => $itemProduk->keterangan,
							'kuantitas'        => $itemProduk->kuantitas,
							'tanggal'          => $itemSaldoAwal->transaksi->tanggal,
							'no_transaksi'     => $itemSaldoAwal->transaksi->no_transaksi,
							'catatan'          => $itemSaldoAwal->transaksi->sumber . ' : ' . $itemSaldoAwal->transaksi->dari,
							'harga'            => $itemSaldoAwal->harga_terakhir,
							'kuantitas_masuk'  => $qtyMasuk  ?? 0,
							'kuantitas_keluar' => $qtyKeluar  ?? 0,
							'kuantitas_saldo'  => $saldo  ?? 0,
							'sum_kty_masuk'    => $sumQtyMasuk ?? 0,
							'sum_kty_keluar'   => $sumQtyKeluar ?? 0,
						];
					}
				}
			}
		}
		$newCollection = collect($array)->groupBy('kode_barang');

		$view = [
			'title'        => 'Laporan Histori Barang',
			'perusahaan'   => $this->identitasPerushaan(),
			'dari_tanggal' => $this->dateCarbon($request['start-date']),
			'ke_tanggal'   => $this->dateCarbon($request['end-date']),
			'item'         => $newCollection,
		];

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_persediaan.histori_barang')->with($view);
	}

	public function jurnal_persediaan(Request $request)
	{
		$dataAkun      = $this->akun->pluck('id');
		$dataTransaksi = $this->transaksi->with('akun')
								->DateFilter($request)
								->whereNotNull('item_type')
								->whereIn('akun_id', $dataAkun);
		$array = [];
		foreach ($dataTransaksi->get() as $dataTransaksiPersediaan) {
			$array[] = [
				'no_transaksi'      => $dataTransaksiPersediaan->no_transaksi,
				'tanggal_transaksi' => $this->dateCarbon($dataTransaksiPersediaan->tanggal),
				'sumber'            => $dataTransaksiPersediaan->sumber,
				'keterangan'        => $dataTransaksiPersediaan->keterangan ?? null,
				'kode_akun'         => $dataTransaksiPersediaan->akun->kode_akun,
				'nama_akun'         => $dataTransaksiPersediaan->akun->nama_akun,
				'status'            => $dataTransaksiPersediaan->status,
				'nominal'           => $dataTransaksiPersediaan->nominal ?? 0,
			];
		}
		$newCollection = collect($array)->groupBy('keterangan');

		$view = [
			'title'        => 'Laporan Jurnal Persediaan',
			'perusahaan'   => $this->identitasPerushaan(),
			'dari_tanggal' => $this->dateCarbon($request['start-date']),
			'ke_tanggal'   => $this->dateCarbon($request['end-date']),
			'item'         => $newCollection,
		];

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_persediaan.jurnal_persediaan')->with($view);
	}

	public function rincian_daftar_penyesuaian_persediaan(Request $request)
	{
		$dataAkun                  = $this->akun->pluck('id');
		$dataDetailPenyesuaian     = $this->detailPenyesuaianPersediaan->pluck('penyesuaian_persediaan_id');
		$dataPenyesuaianPersediaan = $this->penyesuaianPersediaan->with('detailPenyesuaian.produk')->with('akun')
											->DateFilter($request)
											->whereIn('id', $dataDetailPenyesuaian)
											->whereIn('kode_akun_id', $dataAkun);
		$array = [];
		foreach ($dataPenyesuaianPersediaan->get() as $dataPenyesuaianPersediaanBarang) {
			foreach ($dataPenyesuaianPersediaanBarang->detailPenyesuaian as $dataPenyesuaianPersediaan) {
				if ($dataPenyesuaianPersediaan->qty_produk >= $dataPenyesuaianPersediaan->qty_baru) {
					$qty = $dataPenyesuaianPersediaan->qty_produk - $dataPenyesuaianPersediaan->qty_baru;
				} elseif ($dataPenyesuaianPersediaan->qty_produk < $dataPenyesuaianPersediaan->qty_baru) {
					$qty = $dataPenyesuaianPersediaan->qty_baru - $dataPenyesuaianPersediaan->qty_produk;
				}
				$nilai1 = $dataPenyesuaianPersediaan->qty_produk * $dataPenyesuaianPersediaan->produk->harga_jual;
				$nilai2 = $dataPenyesuaianPersediaan->qty_baru   * $dataPenyesuaianPersediaan->produk->harga_jual;
				$biaya  = (($nilai1 + $nilai2) / 2);
				if ($dataPenyesuaianPersediaan->qty_produk >= $dataPenyesuaianPersediaan->qty_baru) {
					$qty = $dataPenyesuaianPersediaan->qty_produk - $dataPenyesuaianPersediaan->qty_baru;
				} elseif ($dataPenyesuaianPersediaan->qty_produk < $dataPenyesuaianPersediaan->qty_baru) {
					$qty = $dataPenyesuaianPersediaan->qty_baru - $dataPenyesuaianPersediaan->qty_produk;
				}
				if ($dataPenyesuaianPersediaan->produk->harga_jual >= $biaya) {
					$selisih = $dataPenyesuaianPersediaan->produk->harga_jual - $biaya;
				} elseif ($dataPenyesuaianPersediaan->produk->harga_jual < $biaya) {
					$selisih = $biaya - $dataPenyesuaianPersediaan->produk->harga_jual;
				}
				$array[] = [
					'tanggal'        => $this->dateCarbon($dataPenyesuaianPersediaanBarang->tgl_penyesuaian),
					'no_penyesuaian' => $dataPenyesuaianPersediaanBarang->no_penyesuaian,
					'tipe_akun'      => $dataPenyesuaianPersediaanBarang->akun->nama_akun,
					'keterangan'     => $dataPenyesuaianPersediaanBarang->keterangan ?? null,
					'no_barang'      => $dataPenyesuaianPersediaan->produk->no_barang,
					'nm_barang'      => $dataPenyesuaianPersediaan->produk->keterangan,
					'satuan'         => $dataPenyesuaianPersediaan->produk->unit,
					'qty_diff'       => $qty,
					'biaya_rata'     => $biaya,
					'nilai_selisih'  => $selisih,
				];
			}
		}
		$newCollection = collect($array)->groupBy('keterangan');

		$view = [
			'title'        => 'Laporan Rincian Daftar Penyesuaian Persediaan',
			'perusahaan'   => $this->identitasPerushaan(),
			'dari_tanggal' => $this->dateCarbon($request['start-date']),
			'ke_tanggal'   => $this->dateCarbon($request['end-date']),
			'item'         => $newCollection,
		];

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_persediaan.rincian_daftar_penyesuaian_persediaan')->with($view);
	}

	public function group_barang(Request $request)
	{
		$dataGrop = $this->produk->with('tingkatHargaBarang')->with('akunRetPenjualan')->with('akunPenjualan')
											->with('akunDiskPenjualan')->with('kodePajak')
											->where('tipe_barang', 4);
		$array = [];
		foreach ($dataGrop->get() as $dataGropBarang) {
			$array[] = [
				'no_grup'         => $dataGropBarang->no_barang ?? null,
				'keterangan'      => $dataGropBarang->keterangan ?? null,
				'cetak'           => 'Tidak' ?? null,
				'status'          => $status ?? null,
				'akun_pajak_jual' => $dataGropBarang->kodePajak->keterangan ?? null,
				'akun_penjualan'  => $dataGropBarang->akunPenjualan->nama_akun ?? null,
				'akun_diskon'     => $dataGropBarang->akunDiskPenjualan->nama_akun ?? null,
				'akun_retur'      => $dataGropBarang->akunRetPenjualan->nama_akun ?? null,
				'no_barang'       => $dataGropBarang->no_barang ?? null,
				'nm_barang'       => $dataGropBarang->keterangan ?? null,
				'kuantitas'       => $dataGropBarang->kuantitas ?? null,
				'satuan'          => $dataGropBarang->unit ?? null,
				'harga_jual_1'    => $dataGropBarang->tingkatHargaBarang->tingkat_1 ?? null,
				'harga_jual_2'    => $dataGropBarang->tingkatHargaBarang->tingkat_2 ?? null,
				'harga_jual_3'    => $dataGropBarang->tingkatHargaBarang->tingkat_3 ?? null,
				'harga_jual_4'    => $dataGropBarang->tingkatHargaBarang->tingkat_4 ?? null,
				'harga_jual_5'    => $dataGropBarang->tingkatHargaBarang->tingkat_5 ?? null,
			];
		}
		$newCollection = collect($array)->groupBy('no_grup');

		$view = [
			'title'        => 'Laporan Group Barang',
			'perusahaan'   => $this->identitasPerushaan(),
			'dari_tanggal' => $this->dateCarbon($request['start-date']),
			'ke_tanggal'   => $this->dateCarbon($request['end-date']),
			'item'         => $newCollection,
		];

		return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_persediaan.group_barang')->with($view);
	}
}
