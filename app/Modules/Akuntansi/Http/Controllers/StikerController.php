<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Akuntansi\Models\StikerHarga;
use Image;

class StikerController extends Controller
{
    /**
    * Index Upload Stiker
    *
    * @method GET
    * @param null
    * @return view
    */
    public function index()
    {
        $view = [
            'stickers' => StikerHarga::paginate(10),
            'active' => StikerHarga::where('aktif', true)->first()
        ];

        return view('akuntansi::stiker.index')->with($view);
    }

    /**
     * Upload Stiker Harga
     *
     * @method POST
     * @param Request
     * @return void
     */
    public function upload(Request $request)
    {
        $file = $request->header;
        $photo = time() . str_slug($file->getClientOriginalName(), '_') . '.' . $file->getClientOriginalExtension();
        $location = storage_path().'/app/public/sticker/header/';

        $locationImage  =   $location.$photo;
            
        // simpan foto produk yg baru
        Image::make($file)->resize(200, 150)->save($locationImage);

        // $file->move(public_path('storage/sticker/header/'), $photo);

        StikerHarga::create([
            'nama_stiker' => $photo
        ]);

        return redirect()->route('akuntansi.upload.stiker.index')->withMessage('Berhasil Mengupload Stiker');
    }

    /**
     * Update Status Stiker
     *
     * @method POST
     * @param $id
     * @return void
     */
    public function status($id)
    {
        $sticker = StikerHarga::where('id', $id)->first();
        $active = StikerHarga::where('aktif', true)->first();

        if ($sticker->aktif) {
            $sticker->update([
                'aktif' => !$sticker->aktif
            ]);
        } else {
            if ($active != null) {
                /**TODO: Return Message */
                return redirect()->route('akuntansi.upload.stiker.index');
            } else {
                $sticker->update([
                    'aktif' => !$sticker->aktif
                ]);
            }
        }
        
        return redirect()->route('akuntansi.upload.stiker.index')->withMessage('Berhasil Mengubah Status');
    }

    /**
     * Delete Stiker
     *
     * @method POST
     * @param id $id
     * @return void
     */
    public function delete($id)
    {
        $sticker = StikerHarga::where('id', $id)->first();
        $sticker->delete();

        /** TODO: Message Berhasil */
        return redirect()->route('akuntansi.upload.stiker.index')->withMessage('Berhasil Menghapus Stiker');
    }
}
