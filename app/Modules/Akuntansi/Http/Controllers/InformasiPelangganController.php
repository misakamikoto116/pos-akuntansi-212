<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use App\Modules\Akuntansi\Models\DetailInformasiPelanggan;
use App\Modules\Akuntansi\Http\Requests\InformasiPelangganRequest;
use App\Modules\Akuntansi\Repositories\InformasiPelangganRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
class InformasiPelangganController extends Controller
{
    use ExportImportPelanggan;
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Pelanggan';
        $this->request = InformasiPelangganRequest::class;
        $this->requestField = ['nama','no_pelanggan','no_faktur_pelanggan','saldo_awal_pelanggan','tanggal_pelanggan','alamat_pajak','alamat','kota','prop','halaman_web','kode_pos','negara','telepon','personal_kontak','email','syarat_pembayaran_id','batasan_piutang_uang','batasan_piutang_hari','mata_uang_id','pesan','pajak_satu_id','pajak_dua_id','pajak_faktur','npwp','nppkp','tipe_pajak_id','tipe_pelanggan_id','tingkatan_harga_jual','disk_default','catatan','no_faktur_saldo','tanggal_saldo','saldo_awal','syarat_pembayaran_saldo_id','nama_kontak','nama_depan_kontak','jabatan_kontak','telepon_kontak','nik','foto_pelanggan','foto_nik_pelanggan'];
    }
      public function formData()
    {
        return [
            'mataUang' => $this->model->listMataUang(),
            'termin' => $this->model->listTermin(),
            'kodePajak' => $this->model->listKodePajak(),
            'tipePajak' => $this->model->listTipePajak(),
            'tipePelanggan' => $this->model->listTipePelanggan(),
            'permission' => [
                'buat' => 'buat_data_pelanggan',
                'ubah' => 'ubah_data_pelanggan',
                'hapus' => 'hapus_data_pelanggan',
                'laporan' => 'laporan_data_pelanggan',
            ],
            'filter'     => [
                                'no_pelanggan',
                                'nama_pelanggan',
                                'mata_uang',
                                'tipe_pelanggan',
                            ],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

    public function getHistoryBukuBesar(Request $request, $type,$id)
    {
        if (empty($id || $id === null)) {
            return abort(404);
        }
        return $this->model->historyBukuBesar($request, $type,$id);
    }
}
