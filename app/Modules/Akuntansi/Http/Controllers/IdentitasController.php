<?php

namespace App\Modules\Akuntansi\Http\Controllers;
use Illuminate\Http\Request;
use App\Modules\Akuntansi\Http\Requests\IdentitasRequest;
use App\Modules\Akuntansi\Repositories\IdentitasRepository;
use Generator\Interfaces\RepositoryInterface;
class IdentitasController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Data Perusahaan';
        $this->request = IdentitasRequest::class;
        $this->requestField = [
            // Tab Umum
            'nama_perusahaan', 'tanggal_mulai', 'alamat', 'kepala_toko', 'mata_uang_id',
            'kode_pos', 'no_telepon', 'negara','footer_1','footer_2','logo_perusahaan',
            // Tab Pajak
            'no_seri_faktur_pajak',
            'npwp',
            'no_pengukuhan_pkp',
            'tanggal_pengukuhan_pkp',
            'kode_cabang_pajak',
            'jenis_usaha',
            'klu_spt',
            // Tab Cabang
            'kode_cabang',
            'nama_cabang',
        ];
    }

    public function formData()
    {
        return [
            'mataUang' => $this->model->listMataUang(),
        ];
    }

    public function redirectToIndex(Request $request)
    {
        return redirect()->back()->withMessage('Berhasil Menambah/Memperbarui data');
    }
}
