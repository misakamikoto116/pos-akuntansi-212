<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Modules\Akuntansi\Repositories\RekonsiliasiBankRepository;
use App\Modules\Akuntansi\Http\Requests\RekonsiliasiBankRequest;
use Generator\Interfaces\RepositoryInterface;

class RekonsiliasiBankController extends Controller
{
    /**
     * @var DataKaryawanRepository
     */
    protected $model;

    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'akuntansi';
        $this->model = $model;
        $this->title = 'Rekonsiliasi Bank';
        $this->request = RekonsiliasiBankRequest::class;
        $this->requestField = [
            'akun_id',
            'saldo_rekening_koran',
            'kalkulasi_saldo',
            'selisih_saldo',
            'tanggal_terakhir_rekonsil',
            'transaksi_id',
        ];
    }

    public function formData()
    {
        return [
            'akun' => $this->model->listAkun(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }
}
