<?php

namespace App\Modules\Akuntansi\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\JasaPengiriman;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\MataUang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use App\Modules\Akuntansi\Models\TipePelanggan;
use App\Modules\Akuntansi\Models\User;
use Illuminate\Http\Request;

class LaporanLainLainController extends Controller
{
    public function __construct(Identitas   $identitas, JasaPengiriman $jasaPengiriman, KodePajak $kodePajak, User $user,
                                MataUang $mataUang, Produk $produk, SyaratPembayaran $syaratPembayaran, TipePelanggan $tipePelanggan)
    {
        $this->identitas        =   $identitas;
        $this->jasaPengiriman   =   $jasaPengiriman;
        $this->kodePajak        =   $kodePajak;
        $this->mataUang         =   $mataUang;
        $this->produk           =   $produk;
        $this->syaratPembayaran =   $syaratPembayaran;
        $this->tipePelanggan    =   $tipePelanggan;
        $this->user             =   $user;
    }

    public function daftar_barang(Request $request)
    {
    	$barang = $this->produk->reportLainLainFilter($request)->get();
        $nama_perusahaan = $this->identitas->first();
        $view = [
            'datas'             => $barang,
            'title'             => 'Laporan Daftar Barang',
            'nama_perusahaan'   => $nama_perusahaan->nama_perusahaan ?? null,
        ];
        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_lain_lain.daftar_barang')->with($view);
    }

    public function daftar_mata_uang()
    {
        $mata_uang          = $this->mataUang->get();
        $nama_perusahaan    = $this->identitas->first();

        $view = [
            'items'             => $mata_uang,
            'title'             => 'Daftar Mata Uang',
            'nama_perusahaan'   => $nama_perusahaan->nama_perusahaan ?? null
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_lain_lain.daftar_mata_uang')->with($view);
    }

    public function rincian_daftar_mata_uang()
    {
        $mata_uang          = $this->mataUang->get();
        $nama_perusahaan    = $this->identitas->first();

        $view = [
            'items'             => $mata_uang,
            'title'             => 'Rincian Daftar Mata Uang',
            'nama_perusahaan'   => $nama_perusahaan->nama_perusahaan ?? null
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_lain_lain.daftar_rincian_mata_uang')->with($view);
    }

    public function daftar_pajak()
    {
        $pajak              = $this->kodePajak->get();
        $nama_perusahaan    = $this->identitas->first();

        $view = [
            'items'             => $pajak,
            'title'             => 'Daftar Pajak',
            'nama_perusahaan'   => $nama_perusahaan->nama_perusahaan ?? null
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_lain_lain.daftar_pajak')->with($view);
    }

    public function daftar_syarat_pembayaran()
    {
        $syarat_pembayaran      = $this->syaratPembayaran->get();
        $nama_perusahaan        = $this->identitas->first();

        $view = [
            'items'             => $syarat_pembayaran,
            'title'             => 'Daftar Syarat Pembayaran',
            'nama_perusahaan'   => $nama_perusahaan->nama_perusahaan ?? null
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_lain_lain.daftar_syarat_pembayaran')->with($view);
    }

    public function daftar_pengiriman()
    {
        $jasa_pengiriman        = $this->jasaPengiriman->get();
        $nama_perusahaan        = $this->identitas->first();

        $view = [
            'items'             => $jasa_pengiriman,
            'title'             => 'Daftar Pengiriman',
            'nama_perusahaan'   => $nama_perusahaan->nama_perusahaan ?? null
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_lain_lain.daftar_pengiriman')->with($view);
    }

    public function daftar_tipe_pelanggan()
    {
        $tipe_pelanggan         = $this->tipePelanggan->get();
        $nama_perusahaan        = $this->identitas->first();

        $view = [
            'items'             => $tipe_pelanggan,
            'title'             => 'Daftar Tipe Pelanggan',
            'nama_perusahaan'   => $nama_perusahaan->nama_perusahaan ?? null
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_lain_lain.daftar_tipe_pelanggan')->with($view);
    }

    public function ringkasan_daftar_pengguna()
    {
        $daftar_pengguna        = $this->user->get();
        $nama_perusahaan        = $this->identitas->first();

        $view = [
            'items'             => $daftar_pengguna,
            'title'             => 'Ringkasan Daftar Pengguna',
            'nama_perusahaan'   => $nama_perusahaan->nama_perusahaan ?? null
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_lain_lain.ringkasan_daftar_pengguna')->with($view);   
    }

    public function daftar_batasan_komisi()
    {
        $nama_perusahaan        = $this->identitas->first();

        $view = [
            'title'             => 'Daftar Batasan Komisi',
            'nama_perusahaan'   => $nama_perusahaan->nama_perusahaan ?? null
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_lain_lain.daftar_batasan_komisi')->with($view);   
    }
}
