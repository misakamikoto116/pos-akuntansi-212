<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DataKaryawanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nik_karyawan' => 'required',
            'nip_karyawan' => 'required',
            'nama_karyawan' => 'required',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat_karyawan' => 'required',
            'no_telp' => 'required',
            'jabatan' => 'required',
            'status' => 'required',
        ];
        if ($this->isMethod('post')) {
            $rules['nik_karyawan'] .= '|unique:data_karyawan';
            $rules['nip_karyawan'] .= '|unique:data_karyawan';
        }

        return $rules;
    }
}
