<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReturPembelianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'return_no' => 'nullable',
            'pemasok_id' => 'required',
            // 'keterangan' => 'required',
            'total' => 'required',
        ];

        if ($this->isMethod('post')) {
            $rules['return_no'] .= '|unique:retur_pembelian';
        }

        return $rules;
    }
}
