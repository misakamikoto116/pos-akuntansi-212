<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenyesuaianPersediaanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'no_penyesuaian' => 'required',
            'tgl_penyesuaian' => 'required',
            'akun_penyesuaian' => 'required',
            // 'persiapan' => 'required',
            'produk_id' => 'required',
        ];

        if ($this->isMethod('post')) {
        }

        return $rules;
    }
}
