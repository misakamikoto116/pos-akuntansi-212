<?php
namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\Akuntansi\Repositories\FakturPembelianRepository;

class FakturPembelianRequest extends FormRequest
{
	/**
	 * repo.
	 *
	 * @var App\Modules\Akuntansi\Repositories\FakturPembelianRepository
	 */
	protected $repo;

	public function __construct()
	{
		$this->repo = app(FakturPembelianRepository::class);
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [
			// 'form_no'           => 'required',
			// 'no_faktur'         => 'required',
			'pemasok_id'        => 'required',
			// 'alamat_pengiriman' => 'required',
			'unit_harga_produk' => 'required|min:1',
			'total'             => 'numeric|required',
			// 'fob'               => 'required',
			'akun_hutang_id'    => 'required',
			// 'gudang_id' => 'required'
		];

		if ($this->isMethod('post')) {
			if ( empty($rules['form_no']) ) {
				// Kosongan
			} else {
				$rules['form_no'] .= '|unique:faktur_pembelian';
			}
		}

		return $rules;
	}

	public function withValidation($validation)
	{
		$produkTipeBarang = $this->repo->getProdukTipeBarang($this->produk_id);
		foreach ($this->produk_id as $key => $produkID) {
			if (in_array($produkTipeBarang[$produkID], [0, 1, 2])) {
				$validation->sometimes("gudang_id.$key", 'required', function () {
					return true;
				});
			}
		}
	}
}
