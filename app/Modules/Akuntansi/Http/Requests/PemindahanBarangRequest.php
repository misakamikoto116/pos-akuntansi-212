<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PemindahanBarangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'no_transfer' => 'required',
            'tanggal' => 'required',
            'val_dari' => 'required',
            'val_ke' => 'required',
            'produk_id' => 'required',
        ];
        
        if ($this->isMethod('post')) {
            
        }

        return $rules;
    }
    public function messages()
    {
        return [
            'val_dari.required'        => 'transfer dari gudang harus terisi.',
            'val_ke.required'          => 'transfer ke gudang harus terisi.',
        ];
    }
}
