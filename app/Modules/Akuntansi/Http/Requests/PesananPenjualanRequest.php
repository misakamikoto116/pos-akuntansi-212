<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PesananPenjualanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'po_number' => 'nullable',
            'pelanggan_id' => 'required',
            'akun_dp_id' => 'required',
        // 'term_id' => 'required',
        // 'ship_id' => 'required',
            // 'alamat_pengiriman' => 'required',
            'produk_id' => 'required',
        ];

        if ($this->isMethod('post')) {
            $rules['po_number'] .= '|unique:pesanan_penjualan,po_number,NULL,id,deleted_at,NULL';
        }

        return $rules;
    }
}
