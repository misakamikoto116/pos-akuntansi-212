<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FakturPenjualanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'no_faktur'         => 'nullable',
            'po_no'             => 'nullable',
            'pelanggan_id'      => 'required',
            'alamat_pengiriman' => 'nullable',
            'total'             => 'numeric|required',
            // 'fob' => 'required',
            // 'akun_ongkir_id' => 'required_unless:ongkir,0,1',
            // 'ongkir' => 'numeric'
        ];

        if ($this->isMethod('post')) {
            $rules['no_faktur'] .= '|unique:faktur_penjualan,no_faktur,NULL,id,deleted_at,NULL';
        }

        return $rules;
    }
    
    public function withValidator($validator)
    {
        $validator->sometimes('akun_ongkir_id','required',function ($input)
        {
            return $input->ongkir > 0;
        });
    }
}
