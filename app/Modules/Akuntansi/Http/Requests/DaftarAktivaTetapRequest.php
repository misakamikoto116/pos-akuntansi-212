<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DaftarAktivaTetapRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kode_aktiva' => 'required',
            'tipe_aktiva_id' => 'required',
            'tgl_beli' => 'required',
            'tgl_pakai' => 'required',
            'keterangan' => 'required',
            'qty' => 'required| min: 1',
            'metode_penyusutan_id' => 'required',
            'akun_aktiva_id' => 'required',
            // 'akun_akum_penyusutan_id' => 'required',
            'akun_beban_penyusutan_id' => 'required',
            'akun_id.*' => 'required',
        ];
    }
}
