<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IdentitasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'nama_perusahaan'           => 'sometimes|required',
        'tanggal_mulai'             => 'required',
        'alamat'                    => 'required',
        'mata_uang_id'              => 'required',
        ];
    }
    public function messages()
    {
        return [
            'required'        => 'Field :attribute harus di isi.',
            'mata_uang_id.required'   =>  'Field mata uang harus di isi',
        ];
    }
}
