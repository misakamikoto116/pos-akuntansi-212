<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SyaratPembayaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'jika_membayar_antara'        => 'required',
        'akan_dapat_diskon'          => 'required',
        'jatuh_tempo'   => 'required',
        // 'keterangan'          => 'required',
        ];
    }
    public function messages()
    {
        return [
            'required'        => 'Field :attribute harus di isi.',
        ];
    }
}
