<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReturPenjualanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'sr_no' => 'nullable',
            'pelanggan_id' => 'required',
            'alamat_pengiriman',
            'total' => 'required',
            'produk_id' => 'required',
        ];

        if ($this->isMethod('post')) {
            $rules['sr_no'] .= '|unique:retur_penjualan';
        }

        return $rules;
    }
}
