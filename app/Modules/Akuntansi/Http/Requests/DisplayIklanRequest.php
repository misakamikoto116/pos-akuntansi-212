<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DisplayIklanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'image_iklan' => 'required_without:last_image',
            'keterangan'  => 'required',
            'nama_iklan'  => 'required',
            'publish'     => 'required',
        ];


        return $rules;
    }
}
