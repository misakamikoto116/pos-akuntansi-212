<?php
namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AkunRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
			$rules =  [
				'kode_akun'      => 'required',
				'nama_akun'      => 'required',
				'tipe_akun_id' 	 => 'required',
			];

			if ($this->isMethod('post')) {
				$rules['kode_akun'] .= '|unique:akun,kode_akun';
			}

			return $rules;

			// 'parent_id' => 'required',
			// 'money_function' => 'numeric|required_if:tipe_akun_id,1|required_if:tipe_akun_id,4|required_if:tipe_akun_id,5|required_if:tipe_akun_id,6|required_if:tipe_akun_id,9|required_if:tipe_akun_id,11|required_if:tipe_akun_id,12|required_if:tipe_akun_id,13|required_if:tipe_akun_id,15',
			// 'mata_uang_id' => 'numeric|required_if:tipe_akun_id,1'
	}
}
