<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PembayaranPembelianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'no_form' => 'nullable',
            'pemasok_id' => 'required',
            'akun_bank_id' => 'required',
        ];

        if ($this->isMethod('post')) {
            $rules['no_form'] .= '|unique:pembayaran_pembelian';
        }

        return $rules;
    }
}
