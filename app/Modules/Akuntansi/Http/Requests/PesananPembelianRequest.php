<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PesananPembelianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            // 'po_number' => 'nullable',
            'pemasok_id' => 'required',
            // 'alamat_pengiriman' => 'required',
            'akun_dp_id' => 'required',
            'produk_id' => 'required',
        ];

        if ($this->isMethod('post')) {
            if ( empty($rules['po_number']) ) {
                // Nothing To Do, so...
            } else {
                $rules['po_number'] .= '|unique:pesanan_pembelian';
            }
        }

        return $rules;
    }
}
