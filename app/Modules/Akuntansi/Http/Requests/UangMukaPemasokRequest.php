<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UangMukaPemasokRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'no_faktur' => 'nullable',
            'invoice_date' => 'required',
            'pemasok_id' => 'required',
            'akun_uang_muka_id' => 'required',
        ];

        if ($this->isMethod('post')) {
            $rules['no_faktur'] .= '|unique:faktur_pembelian';
        }

        return $rules;
    }
}
