<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TipeAktivaTetapPajakRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'tipe_aktiva_tetap_pajak'        => 'required',
        'metode_penyusutan_id'          => 'required',
        'estimasi_umur'   => 'required',
        'tarif_penyusutan'          => 'required',
        ];
    }
}
