<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PengirimanPenjualanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $rules = [
            'delivery_no' => 'nullable',
            'pelanggan_id' => 'required',
            // 'alamat_pengiriman' => 'required',
            'produk_id' => 'required',
            'pesanan' => 'required',
            'gudang_id' => 'required',
        ];

        if ($this->isMethod('post')) {
            $rules['delivery_no'] .= '|unique:pengiriman_penjualan,delivery_no,NULL,id,deleted_at,NULL';
        }

        return $rules;
    }
}
