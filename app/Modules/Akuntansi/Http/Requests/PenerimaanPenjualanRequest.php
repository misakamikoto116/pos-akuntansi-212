<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenerimaanPenjualanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'akun_bank_id' => 'required',
            'no_pelanggan' => 'required',
            'faktur_id' => 'required',     
            'rate'      => 'required',
            // 'cheque_no' => 'nullable',
            // 'cheque_date' => 'required_if:cheque_no,'       
        ];
    }
}
