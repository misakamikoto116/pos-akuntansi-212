<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenerimaanBarangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            // 'receipt_no' => 'nullable',
            'pemasok_id' => 'required',
            // 'alamat_pengiriman' => 'required',
            // 'akun_dp_id' => 'required',
            // 'term_id' => 'required',
            // 'ship_id' => 'required',
            'produk_id' => 'required',
        ];

        // if ($this->isMethod('post')) {
        //     if ( empty($rules['receipt_no']) ) {
        //         // Nothing To Do, so...
        //     } else {
        //         $rules['receipt_no'] .= '|unique:pesanan_pembelian';
        //     }
        // }

        return $rules;
    }
}
