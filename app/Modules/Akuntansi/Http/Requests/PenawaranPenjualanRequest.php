<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenawaranPenjualanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'no_penawaran' => 'required',
            'pelanggan_id' => 'required',
            'produk_id' => 'required',
            'alamat_pengiriman' => 'required',
        ];

        if ($this->isMethod('post')) {
            $rules['no_penawaran'] .= '|unique:penawaran_penjualan,no_penawaran,NULL,id,deleted_at,NULL';
        }

        return $rules;
    }
}
