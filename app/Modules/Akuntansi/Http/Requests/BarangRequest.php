<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BarangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'no_barang'         => 'required',
            'keterangan'        => 'required',
            'status'            => 'required',
            'tipe_barang'       => 'required',
        ];

        if (request()->get("tipe_barang") == 0) {
            // $rules['multi_gudang_id'] = 'required';
            // $rules['no_faktur_gudang'] = 'required';
        }

        return $rules;
    }
}
