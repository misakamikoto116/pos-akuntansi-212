<?php

namespace App\Modules\Akuntansi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PembiayaanPesananRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'batch_no' => 'required',
            'date' => 'required',
            'akun_pembiayaan_pesanan_id' => 'required',
            'produk_id' => 'required',
        ];

        if ($this->isMethod('post')) {
            $rules['batch_no'] .= '|unique:pembiayaan_pesanan';
        }

        return $rules;
    }
}
