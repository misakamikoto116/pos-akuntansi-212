<?php 

namespace App\Modules\Akuntansi\Http\ViewComposers;

use Illuminate\View\View;
use App\Modules\Akuntansi\Repositories\PenawaranPenjualanRepository;

class LaporanComposer
{
    /**
     * The message repository implementation.
     *
     * @var PenawaranPenjualanRepository
     */
    protected $item;

    /**
     * Create a new profile composer.
     *
     * @param  PenawaranPenjualanRepository  $laporan
     * @return void
     */
    public function __construct(PenawaranPenjualanRepository $laporan)
    {
        $this->laporan = $laporan;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(['dataPelanggan' => $this->laporan->listPelanggan(), 'dataProduk' => $this->laporan->listProduk()]);
    }
}