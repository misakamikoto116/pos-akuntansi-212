<div class="form-group row">
  <div class="col-1 col-form-label">Order By</div>
  <div class="col-2">
  {!! Form::text('no_pelanggan',null,['class' => 'form-control','id' => 'no_pelanggan','readonly'])!!}
  </div>
  <div class="col-4">
  {!! Form::select('pelanggan_id',$pelanggan,null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'pelanggan_id'])!!}
  </div>
  <div class="col-2">
  	<button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-list"></i>&nbsp; Pilih Penawaran</button>

    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Penawaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Transaksi</th>
              <th>Tanggal</th>
          </tr>
          </thead>
          <tbody id="table-list-penawaran">
          
          </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="filterSelectedOffer()" data-dismiss="modal">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

  </div>
  <div class="col-3 form-inline">
    <div class="form-check form-check-inline">
    @if (isset($item))
    {!! Form::checkbox('taxable', 1, $item->taxable == 1 ? true : false,['class' => 'form-check-input taxable','id' => 'group1','onchange' => 'calculate()']) !!}
    @else
    {!! Form::checkbox('taxable', 1, null,['class' => 'form-check-input taxable','id' => 'group1','onchange' => 'calculate()']) !!}
    @endif
    <label class="form-check-label" for="group1"> Cust. is Taxable</label>
  </div>
  <div class="form-check form-check-inline">
    @if (isset($item))
    {!! Form::checkbox('in_tax', 1, $item->in_tax == 1 ? true : false,['class' => 'form-check-input group2 in_tax', 'id' => 'inlineCheckbox2', 'onchange' => 'calculate()']) !!}
    @else
    {!! Form::checkbox('in_tax', 1, null,['class' => 'form-check-input group2 in_tax', 'id' => 'inlineCheckbox2', 'onchange' => 'calculate()']) !!}
    @endif
    <label class="form-check-label" for="inlineCheckbox2"> Inclusive Tax</label>
  </div>
  </div>
</div>

<hr>
{{-- Pajak --}}
{{ Form::hidden("nilai_pajak", null,['id' => 'nilai_pajak_id','disabled']) }}
{{ Form::hidden("subTotal", null,['class' => 'sub-total-cetak']) }}
<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
	    <label for="">Tagihan Ke</label>
	    {!! Form::textarea('alamat_tagihan',null,['class' => 'form-control','id' => 'alamat_asal','readonly'])!!}
	  	</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
	    <label for="">Kirim Ke</label>
	    {!! Form::textarea('alamat_pengiriman',null,['class' => 'form-control','id' => 'alamat_pengiriman',''])!!}
	  	</div>
      <div>
        {!! Form::select('pelanggan_kirim_ke_id',$pelanggan,null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'pelanggan_kirim_ke',''])!!}
      </div>
	</div>
	<div class="offset-sm-2 col-sm-4">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
			    <div class="tag">PO. No.</div>
          {!! Form::text('po_number',(empty($item->po_number) ? $idPrediction : $item->po_number),['class' => 'form-control','id' => ''])!!}
			  	</div>
			</div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">SO Number</div>
          {!! Form::text('so_number',(empty($item->so_number) ? $idPrediction : $item->so_number),['class' => 'form-control','id' => ''])!!}
          </div>
      </div>
			<div class="col-sm-4">
				<div class="form-group">
			    <div class="tag">SO Date</div>
          @if (isset($item))
          {!! Form::text('so_date',null,['class' => 'form-control tanggal_pesanan','id' => 'so_date','required'])!!}
          @else
			    {!! Form::text('so_date',null,['class' => 'form-control tanggal1','id' => 'so_date','required'])!!}
          @endif
			  	</div>
			</div>
      <div class="offset-4 col-sm-4">
        <div class="form-group">
          <div class="tag">Ship Date</div>
          @if (isset($item))
          {!! Form::text('ship_date',null,['class' => 'form-control tanggal_pesanan','id' => 'ship_date',''])!!}
          @else
          {!! Form::text('ship_date',null,['class' => 'form-control tanggal1','id' => 'ship_date',''])!!}
          @endif
          </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">FOB</div>
          {!! Form::select('fob',['placeholder'=>'-Pilih-','0' => 'Shiping Point', '1' => 'Destination' ],null,['class' => 'form-control select2','id' => 'fob'])!!}
          </div>
      </div>
      <div class="offset-4 col-sm-4">
        <div class="form-group">
          <div class="tag tag-terms">Terms</div>
          {!! Form::select('term_id', $termin, null, ['placeholder' => '--Pilih--', 'class' => 'form-control select2','id' => 'term_id'])!!}
          </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag tag-ships">Ship Via</div>
          {!! Form::select('ship_id', $pengiriman, null, ['placeholder' => '--Pilih--', 'class' => 'form-control select2','id' => 'ship_id'])!!}
          </div>
      </div>
		</div>
	</div>
</div>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>
<div class="pesanan-sections" data-id="1">
<div class="duplicate-pesanan-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
    <div style="width: 2000px;">
      <table width="100%" class="table">
        <thead class="thead-light" style="border-collapse: collapse;">
          <tr>
            <th width="13%">Item</th>
            <th width="13%">Item Description</th>
            <th>Qty</th>
            <th>Satuan</th>
            <th>Unit Price</th>
            <th>Disc %</th>
            <th>Tax</th>
            <th>Amount</th>
            <th width="10%">Dept</th>
            <th width="10%">Proyek</th>
            <th>Terproses</th>
            <th>Ditutup</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="purchasePesanan">
            @if(old('produk_id') !== null)  
                  @foreach(old('produk_id') as $row => $produk_id )
                    @include('akuntansi::pesanan_penjualan/components/item_pesanan',[
                      'row'                   => $row, 
                      'produk_id'             => $produk_id,
                      'no_produk'             => old('no_produk')[$row],
                      'penawaran_id'          => old('penawaran_id')[$row],
                      'keterangan_produk'     => old('keterangan_produk')[$row],
                      'qty_produk'            => old('qty_produk')[$row],
                      'satuan_produk'         => old('satuan_produk')[$row],
                      'unit_harga_produk'     => old('unit_harga_produk')[$row],
                      'diskon_produk'         => old('diskon_produk')[$row],
                      'kode_pajak_id'         => old('kode_pajak_id')[$row],
                      'amount_produk'         => old('amount_produk')[$row],
                      'dept_id'               => old('dept_id')[$row],
                      'proyek_id'             => old('proyek_id')[$row],
                      'terproses'             => old('terproses')[$row],
                      'ditutup'               => old('ditutup')[$row],
                      'harga_modal'           => old('harga_modal')[$row],
                      'barang_id'             => old('barang_id')[$row],
                    ])
                  @endforeach      
              @endif
        </tbody>
      </table>
    </div>
    <a class="btn btn-info add-pesanan" ><i class="fa fa-plus"> Tambah</i></a>
  </div>

</div><!-- END PILLS -->
</div>
</div>

<script type="text/template" id="table_pesanan_section" data-id="">
  @include('akuntansi::pesanan_penjualan/components/item_pesanan')
</script>

<hr>
<div class="row">
	<div class="col-sm-4">
	 	<div class="form-group">
	    <label for="exampleFormControlTextarea1">Description</label>
	    {!! Form::textarea('keterangan',null,['class' => 'form-control','id' => 'catatan'])!!}
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Akun DP</label>
        {!! Form::select('akun_dp_id',$akun, !isset($item) ? $getAkunDp : null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'akun_dp_id','required'])!!}
      </div>  
	</div>
	<div class="offset-sm-4 col-sm-4">
		<div class="form-group row">
		<div class="offset-2 col-3 col-form-label text-right"><strong>Sub Total</strong></div>
		<div class="col-7 text-right col-form-label" id="subtotal"><strong>0</strong></div>
		</div>
		<div class="form-group row">
			<div class="col-2 col-form-label">Discount:</div>
			<div class="col-3 col-form-label">{!! Form::text('diskon',isset($item->diskon) ? $item->diskon : null ,['class' => 'form-control','id' => 'diskonTotal', 'onchange' => 'calculate()'])!!}</div>
			<div class="col-2 col-form-label text-center">% =</div>
			<div class="col-5 col-form-label">{!! Form::text('total_potongan_rupiah', isset($item->total_potongan_rupiah) ? $item->total_potongan_rupiah : 0,['class' => 'form-control mask','id' => 'totalPotonganRupiah','onchange' => 'calculate()','required'])!!}</div>
    </div>
    
		<div class="form-group row">
			<div class="col-5 col-form-label"></div>
			<div class="col-5 col-form-label" id="taxNaration"></div>
      {!! Form::hidden('tax_cetak0', null,['id' => 'tax_cetak0'])!!}
      {!! Form::hidden('tax_cetak1', null,['id' => 'tax_cetak1'])!!}
      {!! Form::hidden('tax_cetak_label0', null,['id' => 'tax_cetak_label0'])!!}
      {!! Form::hidden('tax_cetak_label1', null,['id' => 'tax_cetak_label1'])!!}
		</div>
		<div class="form-group row" style="margin-top: 50px;">
		<div class="offset-2 col-3 col-form-label text-right">Freight</div>
		<div class="col-7">{!! Form::text('ongkir',isset($item->ongkir) ? $item->ongkir : 0,['class' => 'form-control mask','id' => 'ongkir','onchange'=>'calculate()','required'])!!}</div>
		</div>
		<div class="form-group row">
		<div class="offset-1 col-4 col-form-label text-right"><strong>Total Invoice</strong></div>
		<div class="col-7 text-right col-form-label grandtotal"><strong>0</strong></div>
    {!! Form::hidden('grandtotal', null,['id' => 'grand_total'])!!}
		</div>
	</div>
</div>

@include('akuntansi::pesanan_modal/modal_barang')