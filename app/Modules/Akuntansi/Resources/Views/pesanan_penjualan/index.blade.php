@extends( auth()->user()->hasPermissionTo('daftar_pesanan_penjualan') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                @include('chichi_theme.layout.filter')
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Daftar Pesanan Penjualan</a></li>
                    </ol>
                </div>
            </div>
<!-- END Page-Title -->

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">{{$title_document}}</h5>
                @can('buat_pesanan_penjualan')
                    <div class="menu-header">
                        <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-plus"></i></span>
                            Tambah
                        </a>
                    </div>
                @endcan
            </div>
            <div class="card-body">
                @if($items->isEmpty())
                    <div class="alert alert-warning"> Tidak ada data. </div>
                @else
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>NO.SO</th>
                            <th>No Pelanggan</th>
                            <th>Nama Pelanggan</th>
                            <th>Status</th>
                            <th>No .PO</th>
                            <th>Diskon</th>
                            <th>Pajak</th>
                            <th>Biaya Kirim</th>
                            <th>Nilai Faktur</th>
                            {{-- <th>Uang Muka</th> --}}
                            {{-- <th>Uang Muka Terpakai</th> --}}
                            <th>Keterangan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                        <tr>
                            <td>{{\Helpers\IndonesiaDate::indonesiaDate($item->so_date)}}</td>
                            <td>{{$item->so_number}}</td>
                            <td>{{ $item->pelanggan->no_pelanggan }}</td>
                            <td>{{ $item->pelanggan->nama }}</td>
                            <td>{!! $item->status_formatted !!}</td>
                            <td>{{$item->po_number}}</td>
                            <td>{{ number_format($item->total_potongan_rupiah) }}</td>
                            <td>{{ number_format($item->nilai_pajak) }}</td>
                            <td>{{ number_format($item->ongkir) }}</td>
                            <td>{{ number_format($item->nilai_faktur) }}</td>
                            <td>{{$item->keterangan}}</td>
                            <td><span class="mask_index">{{$item->nominal}}</span></td>
                            @if ( auth()->user()->hasPermissionTo('lihat_pesanan_penjualan') || auth()->user()->hasPermissionTo('ubah_pesanan_penjualan') || auth()->user()->hasPermissionTo('hapus_pesanan_penjualan') )
                                <td>
                                    {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                        <div class="btn-group">
                                        <button id="option{{$item->id}}" type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                        <div class="dropdown-menu">
                                            @can('lihat_pesanan_penjualan')
                                                <a class="dropdown-item jurnal" href="#">
                                                    <i class="fa fa-table"></i>&nbsp;Detail
                                                </a>
                                            @endcan
                                            @can('ubah_pesanan_penjualan')
                                                <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i> Edit</a>
                                            @endcan
                                            @can('hapus_pesanan_penjualan')
                                                <a class="dropdown-item deleteBtn" href="#">
                                                    <i class="fa fa-trash"></i> Delete
                                                </a>
                                            @endcan
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </td>
                            @endif
                        @endforeach
                    </tbody>
                </table>
                @endif
                <div class="pull-right">
                    {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
    <script type="text/javascript">
        $('.mask_index').autoNumeric('init');
    </script>
@endsection