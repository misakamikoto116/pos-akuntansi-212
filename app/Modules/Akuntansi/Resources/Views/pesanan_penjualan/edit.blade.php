@extends( auth()->user()->hasPermissionTo('ubah_pesanan_penjualan') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
<link href="{{asset('assets/plugins/footable/css/footable.core.css')}}" rel="stylesheet">
<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788;
        color: white;
        text-align: center;
        position: relative;
        top: 4px;
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
    .warnatr{
        background: lightsteelblue;
        color: black;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Penjualan</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Pesanan Penjualan</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Sales Order / Pesanan Penjualan</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left','id' => 'form-penjualan-penawaran']) !!}
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary', 'id' => 'btn-cetak']) !!}
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" id="btn-submit" disabled data-toggle="dropdown">
                    Simpan <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        @if ( auth()->user()->hasPermissionTo('buat_pengiriman_penjualan') )
                            <li>
                                {!! Form::submit('Kirim Pesanan', ['class' => 'btn btn-success btn-block','value' => '1', 'name' => 'lanjutkan']) !!}
                            </li>
                        @endif
                        <li>
                            {!! Form::button('Simpan & Tutup',['type' => 'submit','class' => 'btn btn-warning btn-block']) !!}
                        </li>
                    </ul>
                </div>
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger']) !!}
                
                </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>
        <script type="text/javascript">
            var waktuItu;
            $('.tanggal_pesanan').datepicker({format: 'dd MM yyyy', autoclose: true});
            var q = 0;
            var pelanggan_id_old = "{{ old('pelanggan_id') }}";
            $(function() {
              enable_cb();
              $("#group1").click(enable_cb);
                clearTimeout(waktuItu);
                
                waktuItu = setTimeout(() => {
                    setDataPelanggan('with-pesanan');
                }, 250);
            });

            function enable_cb() {
              if ($("input#group1").is(':checked')) {
                $("input.group2").removeAttr("disabled");
              } else {
                $("input.group2").attr("disabled", true).removeAttr("checked");
              }
            }

            // function callSelect2AjaxProdukID(q, id) {
            //     $.ajax({
            //         url: "{{ route('akuntansi.get-id-produk') }}",
            //         type: 'GET',
            //         data: 'search='+id,
            //         dataType: 'JSON',
            //         success:function(data){
            //             $.each(data.item, function(key, value){
            //                 $('#produk_id'+q).append('<option selected value="'+value.id+'">'+value.text+'</option>');
            //             });   
            //         }
            //     });
            // }

        $('#selectAll').click(function(e){
            var table= $(e.target).closest('table');
            $('th input:checkbox',table).prop('checked',this.checked);
        });
        
        $( "#pelanggan_id" ).change(function() {
            clearTimeout(waktuItu);
            waktuItu = setTimeout(() => {
                setDataPelanggan();
            }, 250);
        });

        function setDataPelanggan(param = null){
            var pelanggan_id   = $('#pelanggan_id').val();
            var type = "edit";
            clearTimeout(waktuItu);
            waktuItu = setTimeout(() => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-alamat-pelanggan') }}",
                    data: {id : pelanggan_id, type,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                        clearTimeout(waktuItu);
                        waktuItu = setTimeout(() => {
                            $('#table-list-penawaran').find('tr').remove().end();
                            $('.purchasePesanan').find('tr').remove().end();
                            @if (empty($item['alamat_pengiriman']))
                                $('#alamat_pengiriman').val(response.alamat);
                        @endif   
                            $('#alamat_asal').val(response.alamat);
                            $('#no_pelanggan').val(response.no_pelanggan);
                            $("#tax_cetak0").val(null);
                            $("#tax_cetak1").val(null);
                            $("#tax_cetak_label0").val(null);
                            $("#tax_cetak_label1").val(null);
                            q = 0;
                            if (response.penawaran) {
                                clearTimeout(waktuItu);
                                waktuItu = setTimeout(() => {
                                    $.each(response.penawaran, function (index, item){
                                        $('#table-list-penawaran').append('<tr><th><input type="checkbox" value="'+item.id+'" class="item_penawaran" name="penawaran[]" id="penawaran[]"/></th><td>'+item.no_penawaran+'</td><td>'+item.tanggal+'</td></tr>');
                                    });
                                }, 250);
                            }
                            if(param == 'with-pesanan'){
                                setDetailBarang({{ $item->id }});
                            }else{
                                var item_penawaran = $('.item_penawaran').length;            
                                if(item_penawaran > 0){
                                    swal({
                                        title: "Peringatan!",
                                        text: "Pelanggan ini memiliki Penawaran dan tidak digunakan, ingin gunakan penawaran?",
                                        icon: "warning",
                                        buttons: {
                                            cancel: true,
                                            confirm: true,
                                        },
                                    }).then((result) => {
                                        if (result) {
                                            //$(".item_penawaran").each(function() {
                                            //    $(this).prop('checked', true);
                                            //});
                                            //filterSelectedOffer();
                                            $('#exampleModal').modal('show');
                                        }
                                    });
                                }
                            }
                        }, 250);
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
            }, 250);
        }

        $( "#pelanggan_kirim_ke" ).change(function() {
            clearTimeout(waktuItu);
            waktuItu = setTimeout(() => {
                var pelanggan_kirim_ke_id   = $('#pelanggan_kirim_ke').val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-alamat-pelanggan-kirim-ke') }}",
                    data: {id : pelanggan_kirim_ke_id,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                    $('#alamat_pengiriman').val(response.alamat);
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
            }, 250);
        });

        $(document).ready(function () {
            $('.duplicate-pesanan-sections').on('click', '.add-pesanan', function () {
                if (pelanggan_id_old !== "") {
                    clearTimeout(waktuItu);
                    waktuItu = setTimeout(() => {
                        duplicatePesanan();
                    }, 250);
                }else {
                    clearTimeout(waktuItu);
                    waktuItu = setTimeout(() => {
                        duplicateForm();
                    }, 250);
                }
            });
        });

            // function test(vale) {
            //     var produk_id   = $('#produk_id'+vale).val();
            //     var pelanggan_id = $("#pelanggan_id").val();
            //     var url = "{{ route('akuntansi.get-produk') }}";                    
            //           $.ajax({
            //             type: "GET",
            //             url: url,
            //             data: {id : produk_id, pelanggan_id : pelanggan_id,
            //                 _token: '{{ csrf_token() }}'},
            //             dataType: "json",
            //             success: function(response){
            //             $('#tax_produk'+vale).find('option').remove().end();                        
            //             $('#penawaran_id'+vale).val("");
            //             $('#keterangan_produk'+vale).val(response.keterangan);
            //             $('#qty_produk'+vale).val(response.jumlah);
            //             $('#satuan_produk'+vale).val(response.satuan);
            //             $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
            //             $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice); //karena defaultnya di * 1, maka cukup unitprice saja
            //             $('#diskon_produk'+vale).val('0');
            //             $('#terproses'+vale).val(response.terproses);
            //             if (response.harga_modal) {
            //                 $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
            //             }else {
            //                 $('#harga_modal'+vale).val('0');
            //             }
            //             if (response.tax) {
            //                 $.each(response.tax, function (index, item){
            //                         $('#tax_produk'+vale).append( 
            //                         $("<option></option>")
            //                         .text(item.nilai+ "/" +item.nama)
            //                         .val(item.id)
            //                         );
            //                 });
            //             }
            //             sumQtyAndUnit(vale);
                        
            //             }, failure: function(errMsg) {
            //                 alert(errMsg);
            //             }
            //         });
            // }
            function insertItemToForm(vale,item) {
                var pelanggan_id = $("#pelanggan_id").val();
                var url = "{{ route('akuntansi.get-produk-by-val') }}";
                var target = 'penawaran';                                    
                clearTimeout(waktuItu);
                waktuItu = setTimeout(() => {
                      $.ajax({
                        type: "GET",
                        url: url,
                        data: {id : item, type : target, pelanggan_id : pelanggan_id,
                            _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                            clearTimeout(waktuItu);
                            waktuItu = setTimeout(() => {
                                $('#tax_produk'+vale).find('option').remove().end();
                                $('#no_produk'+vale).val(response.text);  
                                $('#penawaran_id'+vale).val(response.penawaran_id);                      
                                $('#keterangan_produk'+vale).val(response.keterangan);
                                $('#qty_produk'+vale).val(response.jumlah);
                                $('#satuan_produk'+vale).val(response.satuan);
                                $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
                                $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0); 
                                $('#diskon_produk'+vale).val(response.diskon);
                                $('#barang_id'+vale).val(response.barang_id);
                                $('#terproses'+vale).val(response.terproses);
                                $("#harga_modal"+vale).val(response.harga_modal);
                                if(response.ditutup == 1){
                                    $('#ditutup'+vale).prop('checked', true);
                                }
                                clearTimeout(waktuItu);
                                waktuItu = setTimeout(() => {
                                    $.each(response.tax, function (index, item){
                                        if(target == 'penawaran'){
                                            if(response.kode_pajak_id == item.id || response.kode_pajak_2_id == item.id){
                                                $('#tax_produk'+vale).append( 
                                                $("<option></option>")
                                                .text(item.nilai+ " / " +item.nama)
                                                .val(item.id)
                                                .attr('selected','selected')
                                                );
                                            }else{
                                                $('#tax_produk'+vale).append( 
                                                $("<option></option>") 
                                                .text(item.nilai+ " / " +item.nama)
                                                .val(item.id)
                                                );
                                            }
                                        }else{
                                            $('#tax_produk'+vale).append( 
                                                $("<option></option>") 
                                            .text(item.nilai+ " / " +item.nama)
                                            .val(item.id)
                                            );
                                        }   
                                        
                                    });
                                }, 250);

                                if (response.taxable.taxable == 1) {
                                    $("#group1").prop('checked', true);
                                    $("#inlineCheckbox2").removeAttr('disabled');
                                }else if(response.taxable.taxable == 0 || response.taxable.taxable === null) {
                                    $("#group1").prop('checked', false);
                                    $("#inlineCheckbox2").attr('disabled','disabled');
                                }
                                if (response.taxable.in_tax == 1) {
                                    $("#inlineCheckbox2").prop('checked', true);
                                }else if(response.taxable.in_tax == 0 || response.taxable.in_tax  ===null){
                                    $("#inlineCheckbox2").prop('checked', false);
                                }
                                
                                sumQtyAndUnit(vale);
                            }, 250);
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                }, 250);
            }
            
         $('.tanggal1').datepicker({format: 'dd MM yyyy', autoclose: true});
         $('.tanggal1').datepicker('setDate', new Date());

         function filterSelectedOffer(){
            var checkCounted = $('input[name="penawaran[]"]:checked').length;
            if(checkCounted > 0){
                $('.purchasePesanan').find('tr').remove();
                q = 0;
                $('input[name="penawaran[]"]:checked').each(function() {
                    var val = $(this).val();
                    clearTimeout(waktuItu);
                    waktuItu = setTimeout(() => {
                        $.ajax({
                            type: "GET",
                            url: "{{ route('akuntansi.get-detail-barang') }}",
                            data: {id : val, type : 'penawaran',
                            _token: '{{ csrf_token() }}'},
                            dataType: "json",
                            success: function(response){
                                clearTimeout(waktuItu);
                                waktuItu = setTimeout(() => {
                                    $.each(response.barang, function(index, item){
                                        clearTimeout(waktuItu);
                                        waktuItu = setTimeout(() => {
                                            if (pelanggan_id_old !== "") {
                                                duplicatePesanan();
                                            }else {
                                                duplicateForm();
                                            }
                                            let countTemp = q - 1;
                                            callSelect2AjaxProdukID(countTemp, item.produk_id);
                                            $('#produk_id'+countTemp).val(item.produk_id);
                                            $('.select2').select2(); 
                                            insertItemToForm(countTemp, item.id); //-1 karena diatas (diplicateForm) sdh di increment
                                        }, 250);
                                    });
                                }, 250);
                            }, failure: function(errMsg) {
                                alert(errMsg);
                            }
                        });
                    }, 250);
                });
              }
        }

        function setDetailBarang(id){
            var pelanggan_id = $("#pelanggan_id").val();
            var type = "pesanan";
            clearTimeout(waktuItu);
            waktuItu = setTimeout(() => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-detail-barang') }}",
                    data: {id : id, type : type, pelanggan_id : pelanggan_id,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                        clearTimeout(waktuItu);
                        waktuItu = setTimeout(() => {
                            $.each(response.barang, function(index, key){
                                if (pelanggan_id_old !== "") {
                                    duplicatePesanan();
                                }else {
                                    duplicateForm();
                                }
                                let queue = q - 1;
                                clearTimeout(waktuItu);
                                waktuItu = setTimeout(() => {
                                    callSelect2AjaxProdukID(queue, key.produk.id);
                                    changeToMasking(key,queue);
                                }, 250);
                                $('#no_produk'+queue).val(key.produk.no_barang+' | '+key.produk.keterangan);
                                $('#produk_id'+queue).val(key.produk.id);
                                if (response.penawaran_id !== null) {
                                    $('#penawaran_id'+queue).val(response.penawaran_id.id);                        
                                }                        
                                $('#tax_produk'+queue).find('option').remove().end();                        
                                $('#keterangan_produk'+queue).val(key.produk.keterangan);
                                $('#qty_produk'+queue).val(key.jumlah);
                                $('#satuan_produk'+queue).val(key.satuan);
                                $('#unit_harga_produk'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set', key.harga);
                                $('#amount_produk'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set', key.harga_total); //karena defaultnya di * 1, maka cukup unitprice saja
                                $('#diskon_produk'+queue).val(key.diskon);
                                $('#barang_id'+queue).val(key.penawaran_penjualan_id);
                                $('#harga_modal'+queue).val(key.harga_modal);
                                $.each(response.kode_pajak, function (index, item){
                                    if(item.id == key.kode_pajak_id || item.id == key.kode_pajak_2_id){
                                        $('#tax_produk'+queue).append( 
                                        $("<option></option>")
                                        .text(item.nilai+ "/" +item.nama)
                                        .val(item.id)
                                        .attr('selected','selected')
                                        );
                                    }else {
                                        $('#tax_produk'+queue).append( 
                                        $("<option></option>")
                                            .text(item.nilai+ "/" +item.nama)
                                            .val(item.id)
                                        );
                                    }
                                });
                                $('.select3').select2();
                                sumQtyAndUnit(queue);
                                for(var a = 0; a < q; a++){
                                    clearTimeout(waktuItu);
                                    waktuItu = setTimeout(() => {
                                        callSelect2AjaxProduk(a, 'modal');
                                    }, 250);
                                }
                            });
                        }, 250);
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
            }, 250);
        }

        // Fungsi button cetak di klik
        $("#btn-cetak").on("click", function(e) {
            e.preventDefault();
            btnCetak()
        });

        function btnCetak() {
            if ($("#pelanggan_id").val() == "" || $('.produk_id').val() == "" || $('.produk_id').val() == undefined) {
                swal({
                    icon: "warning",
                    text: "Pelanggan/Barang Masih Kosong"
                });
            }else {
                var form = $("#form-penjualan-penawaran");
                form.attr('target','_blank');
                form.attr('action','{{ route('akuntansi.edit-cetak-pesanan-penjualan') }}');
                form.submit();
            }
        }

        // Fungsi button simpan tutup dan simpan lanjut di klik
        $("#btn-submit").on("click", function () {
            var form = $("#form-penjualan-penawaran");
            form.removeAttr('target');
            form.attr('action','{{ route($module_url->update,$item->id) }}');
            $("#btn-submit").on("submit", function () {
                form.submit();
            });
        });

        function changeToMasking(response, vale) {
            if(pelanggan_id_old !== ""){
                changeMaskingToNumber($('#unit_harga_produk'+vale).val(response.unitPrice));
                changeMaskingToNumber($('#amount_produk'+vale).val(0));
            }
        }
        </script>
        <script src="{{ asset('js/pesanan-penjualan.js') }}"></script>
        <script src="{{ asset('js/add-btn-tag-activity.js') }}"></script>
@endsection
