@extends( auth()->user()->hasPermissionTo('buat_pesanan_penjualan') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
<link href="{{asset('assets/plugins/footable/css/footable.core.css')}}" rel="stylesheet">
<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788;
        color: white;
        text-align: center;
        position: relative;
        top: 4px;
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
    .warnatr{
        background: lightsteelblue;
        color: black;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Penjualan</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Pesanan Penjualan</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Sales Order / Pesanan Penjualan</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left', 'id' => 'form-pesanan-penjualan']) !!}
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary', 'id' => 'btn-cetak']) !!}
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" id="btn-submit" disabled data-toggle="dropdown">
                    Simpan <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        @if ( auth()->user()->hasPermissionTo('buat_pengiriman_penjualan') )
                            <li>
                                {!! Form::submit('Kirim Pesanan', ['class' => 'btn btn-success btn-block','value' => '1', 'name' => 'lanjutkan']) !!}
                            </li>
                        @endif
                        <li>
                            {!! Form::submit('Simpan & Baru', ['class' => 'btn btn-info btn-block','value' => '0', 'name' => 'lanjutkan']) !!}
                        </li>
                        <li>
                            {!! Form::button('Simpan & Tutup',['type' => 'submit','class' => 'btn btn-warning btn-block']) !!}
                        </li>
                    </ul>
                </div>
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>
        <script type="text/javascript">
            var q = 0;
            var pelanggan_id_old = "{{ old('pelanggan_id') }}";

            $(function() {
              enable_cb();
              $("#group1").click(enable_cb);

              if(typeof GetURLParameter('pelanggan_id') == "string"){
                  $('#pelanggan_id').val(GetURLParameter('pelanggan_id')).trigger('change');
              }
              @if(!old('pelanggan_id'))
                  setDataPelanggan('with-pesanan');              
              @endif

            });

            @if (old('produk_id'))
                $(document).ready(function () {
                    q = $(".produk_id").length;
                    sumQtyAndUnit(q);
                    removeDelButton();
                });
            @endif

            function enable_cb() {
              if ($("input#group1").is(':checked')) {
                $("input.group2").removeAttr("disabled");
              } else {
                $("input.group2").attr("disabled", true).removeAttr("checked");
              }
            }

        $('#selectAll').click(function(e){
            var table= $(e.target).closest('table');
            $('th input:checkbox',table).prop('checked',this.checked);
        });
        
        $( "#pelanggan_id" ).change(function() {
            setDataPelanggan();
        });

        function setDataPelanggan(param = null){
            var pelanggan_id   = $('#pelanggan_id').val();
            var type = "create";
            var sub = GetURLParameter('penawaran_id');
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-alamat-pelanggan') }}",
                data: {id : pelanggan_id, type,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#table-list-penawaran').find('tr').remove().end();
                $('.purchasePesanan').find('tr').remove().end();   
                $('#alamat_pengiriman').val(response.alamat);
                $('#alamat_asal').val(response.alamat);
                $('#no_pelanggan').val(response.no_pelanggan);
                q = 0;
                $("#tax_cetak0").val(null);
                $("#tax_cetak1").val(null);
                $("#tax_cetak_label0").val(null);
                $("#tax_cetak_label1").val(null);
            
                if (response.penawaran) {
                    $.each(response.penawaran, function (index, item){     
                        if (item.taxable == 1) {
                            $("#group1").prop('checked', true);
                            $("#inlineCheckbox2").removeAttr('disabled');
                        }else if(item.taxable == 0 || item.taxable === null) {
                            $("#group1").prop('checked', false);
                            $("#inlineCheckbox2").attr('disabled','disabled');
                        }
                        
                        if (response.in_tax == 1) {
                            $("#inlineCheckbox2").prop('checked', true);
                        }else if(response.in_tax == 0 || response.in_tax  ===null){
                            $("#inlineCheckbox2").prop('checked', false);
                        }                

                        if(sub == item.id){
                            $("#diskonTotal").val(item.diskon);                       
                            $("#totalPotonganRupiah").autoNumeric('init',{aPad: false}).autoNumeric('set', item.total_potongan_rupiah);                       
                            $("#ongkir").autoNumeric('init',{aPad: false}).autoNumeric('set', item.ongkir);
                            $("#catatan").val(item.keterangan);

                            $('#table-list-penawaran').append('<tr><th><input type="checkbox" checked value="'+item.id+'" class="item_penawaran" name="penawaran[]" id="penawaran[]"/></th><td>'+item.no_penawaran+'</td><td>'+item.tanggal+'</td></tr>');                    
                        }else{
                            $('#table-list-penawaran').append('<tr><th><input type="checkbox" value="'+item.id+'" class="item_penawaran" name="penawaran[]" id="penawaran[]"/></th><td>'+item.no_penawaran+'</td><td>'+item.tanggal+'</td></tr>');                                            
                        }
                    });
                }
                if(param == 'with-pesanan'){
                    setDetailBarang(sub);
                }else{
                    var item_penawaran = $('.item_penawaran').length;            
                    if(item_penawaran > 0){
                        swal({
                            title: "Peringatan!",
                            text: "Pelanggan ini memiliki Penawaran dan tidak digunakan, ingin gunakan penawaran?",
                            icon: "warning",
                            buttons: {
                                cancel: true,
                                confirm: true,
                            },
                        }).then((result) => {
                            if (result) {
                                //$(".item_penawaran").each(function() {
                                //    $(this).prop('checked', true);
                                //});
                                //filterSelectedOffer();
                                $('#exampleModal').modal('show');
                            }
                        });
                    }
                }
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        $( "#pelanggan_kirim_ke" ).change(function() {
            var pelanggan_kirim_ke_id   = $('#pelanggan_kirim_ke').val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-alamat-pelanggan-kirim-ke') }}",
                data: {id : pelanggan_kirim_ke_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#alamat_pengiriman').val(response.alamat);
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        });

        $(document).ready(function () {
            $('.duplicate-pesanan-sections').on('click', '.add-pesanan', function () {
                if (pelanggan_id_old !== "") {
                    duplicatePesanan();
                }else {
                    duplicateForm();
                }
            });
        });
        
            // function test(vale) {
            //     var pelanggan_id   = $('#pelanggan_id').val();
            //     var produk_id   = $('#produk_id'+vale).val();
            //     var url = "{{ route('akuntansi.get-produk') }}";                    
            //           $.ajax({
            //             type: "GET",
            //             url: url,
            //             data: {id : produk_id, pelanggan_id : pelanggan_id,
            //                 _token: '{{ csrf_token() }}'},
            //             dataType: "json",
            //             success: function(response){
            //             $('#tax_produk'+vale).find('option').remove().end();                        
            //             $('#penawaran_id'+vale).val("");
            //             $('#keterangan_produk'+vale).val(response.keterangan);
            //             $('#qty_produk'+vale).val(response.jumlah);
            //             $('#satuan_produk'+vale).val(response.satuan);
            //             changeToMasking(response,vale);
            //             $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
            //             $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice); //karena defaultnya di * 1, maka cukup unitprice saja
            //             $('#diskon_produk'+vale).val('0');
            //             $('#terproses'+vale).val(response.terproses);     
            //             if (response.harga_modal !== null) {
            //                 $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
            //             }else {
            //                 $('#harga_modal'+vale).val('0');
            //             }     
            //             $.each(response.tax, function (index, item){
            //                     $('#tax_produk'+vale).append( 
            //                     $("<option></option>")
            //                     .text(item.nilai+ "/" +item.nama)
            //                     .val(item.id)
            //                     );
            //             });
            //             sumQtyAndUnit(vale);
                        
            //             }, failure: function(errMsg) {
            //                 alert(errMsg);
            //             }
            //         });
            // }
            function insertItemToForm(vale,item) {
                var url = "{{ route('akuntansi.get-produk-by-val') }}";
                var pelanggan_id = $("#pelanggan_id").val();                                    
                var target = 'penawaran';                                    
                      $.ajax({
                        type: "GET",
                        url: url,
                        data: {id : item, type : target, pelanggan_id : pelanggan_id,
                            _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                        $('#tax_produk'+vale).find('option').remove().end();  
                        $('#no_produk'+vale).val(response.text);
                        $('#penawaran_id'+vale).val(response.penawaran_id);                      
                        $('#keterangan_produk'+vale).val(response.keterangan);
                        $('#qty_produk'+vale).val(response.jumlah);
                        $('#satuan_produk'+vale).val(response.satuan);
                        changeToMasking(response,vale);
                        $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
                        $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0); 
                        $('#diskon_produk'+vale).val(response.diskon);
                        $('#barang_id'+vale).val(response.all.id);
                        $('#terproses'+vale).val(response.terproses);
                        $("#harga_modal"+vale).val(response.harga_modal);
                        if (response.ditutup == 1){
                            $('#ditutup'+vale).prop('checked', true);
                        }
                        $.each(response.tax, function (index, item){
                            if(target == 'penawaran'){
                                if(response.kode_pajak_id == item.id || response.kode_pajak_2_id == item.id){
                                    $('#tax_produk'+vale).append( 
                                    $("<option></option>")
                                    .text(item.nilai+ " / " +item.nama)
                                    .val(item.id)
                                    .attr('selected','selected')
                                    );
                                }else{
                                    $('#tax_produk'+vale).append( 
                                    $("<option></option>") 
                                    .text(item.nilai+ " / " +item.nama)
                                    .val(item.id)
                                    );
                                }
                            }else{
                                $('#tax_produk'+vale).append( 
                                    $("<option></option>") 
                                   .text(item.nilai+ " / " +item.nama)
                                   .val(item.id)
                                );
                            }   
                            
                       });

                        if (response.taxable.taxable == 1) {
                            $("#group1").prop('checked', true);
                            $("#inlineCheckbox2").removeAttr('disabled');
                        }else if(response.taxable.taxable == 0 || response.taxable.taxable === null) {
                            $("#group1").prop('checked', false);
                            $("#inlineCheckbox2").attr('disabled','disabled');
                        }
                        if (response.taxable.in_tax == 1) {
                            $("#inlineCheckbox2").prop('checked', true);
                        }else if(response.taxable.in_tax == 0 || response.taxable.in_tax  ===null){
                            $("#inlineCheckbox2").prop('checked', false);
                        }

                        sumQtyAndUnit(vale);
                        $('.select3').select2();
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
            }

         $('.tanggal1').datepicker({format: 'dd MM yyyy', autoclose: true});
         $('.tanggal1').datepicker('setDate', new Date());

         function filterSelectedOffer(){
            var checkCounted = $('input[name="penawaran[]"]:checked').length;
            if(checkCounted > 0){
                $('.purchasePesanan').find('tr').remove();
                q = 0;
                $('input[name="penawaran[]"]:checked').each(function() {
                    var val = $(this).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-detail-barang') }}",
                        data: {id : val, type : 'penawaran',
                        _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                            $.each(response.barang, function(index, item){
                                if (pelanggan_id_old !== "") {
                                    duplicatePesanan();
                                }else {
                                    duplicateForm();
                                }
                                let countTemp = q - 1;
                                callSelect2AjaxProdukID(countTemp, item.produk.id);
                                $('#produk_id'+countTemp).val(item.produk.id);
                                insertItemToForm(countTemp, item.id); //-1 karena diatas (diplicateForm) sdh di increment
                            });
                            for(a = 0; a < q; a++){
                                callSelect2AjaxProduk(a, 'modal');
                            }
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                });
              }
        }

        function setDetailBarang(id){
            var pelanggan_id = $("#pelanggan_id").val();
            var type = "penawaran";
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-detail-barang') }}",
                data: {id : id, type : type, pelanggan_id : pelanggan_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                    if (response.barang) {
                            $.each(response.barang, function(index, key){
                            if (pelanggan_id_old !== "") {
                                duplicatePesanan();
                            }else {
                                duplicateForm();
                            }
                            let queue = q - 1;
                            callSelect2AjaxProdukID(queue, key.produk.id);
                            $('#produk_id'+queue).val(key.produk.id);

                            if (id !== null) {
                                $('#penawaran_id'+queue).val(id);                        
                            }                        
                            $('#tax_produk'+queue).find('option').remove().end();                        
                            $('#keterangan_produk'+queue).val(key.produk.keterangan);
                            $('#qty_produk'+queue).val(key.jumlah);
                            $('#satuan_produk'+queue).val(key.satuan);
                            changeToMasking(response,queue);
                            $('#unit_harga_produk'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set', key.harga);
                            $('#amount_produk'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set', key.harga_total); //karena defaultnya di * 1, maka cukup unitprice saja
                            $('#diskon_produk'+queue).val(key.diskon);
                            
                            $('#barang_id'+queue).val(key.id); //penawaran penjualan id
                            $('#harga_modal'+queue).val(key.harga_modal);
                            $.each(response.kode_pajak, function (index, item){
                                if(item.id == key.kode_pajak_id || item.id == key.kode_pajak_2_id){
                                    $('#tax_produk'+queue).append( 
                                    $("<option></option>")
                                    .text(item.nilai+ "/" +item.nama)
                                    .val(item.id)
                                    .attr('selected','selected')
                                    );
                                }else {
                                    $('#tax_produk'+queue).append( 
                                    $("<option></option>")
                                        .text(item.nilai+ "/" +item.nama)
                                        .val(item.id)
                                    );
                                }
                            });
                            $('.select3').select2();
                            sumQtyAndUnit(queue);
                        });
                    }
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        // Fungsi button cetak di klik
        $("#btn-cetak").on("click", function(e) {
            e.preventDefault();
            btnCetak()
        });

        function btnCetak() {
            if ($("#pelanggan_id").val() == "" || $('.produk_id').val() == "" || $('.produk_id').val() == undefined) {
                swal({
                    icon: "warning",
                    text: "Pelanggan/Barang Masih Kosong"
                });
            }else {
                var form = $("#form-pesanan-penjualan");
                form.attr('target','_blank');
                form.attr('action','{{ route('akuntansi.cetak-pesanan-penjualan') }}');
                form.submit();
            }
        }

        // Fungsi button simpan tutup dan simpan lanjut di klik
        $(".btn-block").on("click", function () {
            var form = $("#form-pesanan-penjualan");
            form.removeAttr('target');
            form.attr('action','{{ route($module_url->store) }}');
            $(".btn-block").on("submit", function () {
                form.submit();
            });
        });

        function changeToMasking(response, vale) {
            if(pelanggan_id_old !== ""){
                changeMaskingToNumber($('#unit_harga_produk'+vale).val(response.unitPrice));
                changeMaskingToNumber($('#amount_produk'+vale).val(0));
            }
        }
       
        </script>
        <script src="{{ asset('js/pesanan-penjualan.js') }}"></script>
        <script src="{{ asset('js/add-btn-tag-activity.js') }}"></script>
@endsection
