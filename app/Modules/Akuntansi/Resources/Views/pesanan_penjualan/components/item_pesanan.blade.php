@if (isset($row))
<tr>
    <td>
        {!! Form::text('no_produk['.($row ?? 0).']', $no_produk ?? null,['placeholder' => 'Cari Barang', 'class' => 'form-control no_produk', 'id' => 'no_produk'.$row, 'onclick' => 'awas(' . $row . ')' ,'required', 'readonly'])!!}
        {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id ?? null,['class' => 'produk_id', 'id' => 'produk_id'.$row, 'required'])!!}
        {!! Form::hidden('penawaran_id['.($row ?? 0).']', $penawaran_id ?? null, ['class' => 'penawaran_id', 'id' => 'penawaran_id'.$row,]) !!}  
        {!! Form::hidden('produk_id_temp['.($row ?? 0).']', $produk_id ?? null, ['id' => 'produk_id_temp'.$row, 'class' => 'produk_id_temp']) !!}
        {{-- {!! Form::select('produk_id['.($row ?? 0).']',[], $produk_id ?? null,['placeholder' => '-Pilih-','class' => 'form-control produk_id','id' => 'produk_id'.$row,'onchange' => 'test('.$row.')'])!!} --}}
    </td>
    <td>{!! Form::text('keterangan_produk['.($row ?? 0).']', $keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.$row,'required'])!!}</td>
    <td>{!! Form::text('qty_produk['.($row ?? 0).']', $qty_produk ?? null,['class' => 'form-control qty_produk','id' => 'qty_produk'.$row,'onchange', 'sumQtyAndUnit(' . $row . ')','required'])!!}</td>
    <td>{!! Form::text('satuan_produk['.($row ?? 0).']', $satuan_produk ?? null,['class' => 'form-control satuan_produk','id' => 'satuan_produk'.$row,'required'])!!}</td>
    <td>{!! Form::text('unit_harga_produk['.($row ?? 0).']', $unit_harga_produk ?? null,['class' => 'mask form-control unit_harga_produk','id' => 'unit_harga_produk'.$row,'onchange', 'sumQtyAndUnit(' . $row . ')','required'])!!}</td>
    <td>{!! Form::text('diskon_produk['.($row ?? 0).']', $diskon_produk ?? null,['class' => 'form-control diskon_produk','id' => 'diskon_produk'.$row,'onchange', 'sumQtyAndUnit(' . $row . ')','required'])!!}</td>
    <td width="10%">
		{!! Form::select('kode_pajak_id['.($row ?? 0).'][]',$kode_pajak,  $data_penawaran['kode_pajak_id'] ?? null,['class' => 'form-control select3 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => 'tax_produk'.$row,'onchange', 'calculate()', 'readonly'])!!}
	</td>
    <td>{!! Form::text('amount_produk['.($row ?? 0).']', $amount_produk ?? null,['class' => 'mask form-control amount_produk','id' => 'amount_produk'.$row,'readonly'])!!}</td>
    <td>{!! Form::select('dept_id['.($row ?? 0).']',[], $dept_id ?? null,['class' => 'form-control select3 dept_id','id' => 'dept_id'.$row,''])!!}</td>
    <td>{!! Form::select('proyek_id['.($row ?? 0).']',[], $proyek_id ?? null,['class' => 'form-control select3 proyek_id','id' => 'proyek_id'.$row,''])!!}</td>
    <td>{!! Form::text('terproses['.($row ?? 0).']', $terproses ?? null,['class' => 'form-control terproses','id' => 'terproses'.$row,''])!!}</td>
    <td style="display: none;">{!! Form::hidden('barang_id['.($row ?? 0).']', $barang_id ?? null,['class' => 'form-control barang_id','id' => 'barang_id',''])!!}</td>
    <td>
        {!! Form::checkbox('ditutup['.($row ?? 0).']',1, $ditutup ?? null,['class' => 'form-control col-form-label ditutup','id' => 'ditutup'.$row,''])!!}
        {!! Form::hidden('harga_modal['.($row ?? 0).']', $harga_modal ?? null,['class' => 'form-control harga_modal','id' => 'harga_modal'.$row,''])!!}
    </td>
    <td><button href="" class="remove btn btn-danger remove-rincian-pesanan" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>
@else
    <tr>
        <td>
            {!! Form::text('no_produk['.($row ?? 0).']', $no_produk ?? null,['placeholder' => 'Cari Barang', 'class' => 'form-control no_produk','required', 'readonly'])!!}
            {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id ?? null,['class' => 'form-control produk_id','required'])!!}
            {!! Form::hidden('penawaran_id['.($row ?? 0).']', $penawaran_id ?? null, ['class' => 'penawaran_id']) !!}  
            {{-- {!! Form::select('produk_id['.($row ?? 0).']',[], $produk_id ?? null,['placeholder' => '-Pilih-','class' => 'form-control produk_id'])!!} --}}
        </td>
        <td>{!! Form::text('keterangan_produk['.($row ?? 0).']', $keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}</td>
        <td>{!! Form::text('qty_produk['.($row ?? 0).']', $qty_produk ?? null,['class' => 'form-control qty_produk','id' => '','onchange' => 'sumQtyAndUnit(0)','required'])!!}</td>
        <td>{!! Form::text('satuan_produk['.($row ?? 0).']', $satuan_produk ?? null,['class' => 'form-control satuan_produk','id' => '','required'])!!}</td>
        <td>{!! Form::text('unit_harga_produk['.($row ?? 0).']', $unit_harga_produk ?? null,['class' => 'mask form-control unit_harga_produk','id' => '','onchange' => 'sumQtyAndUnit(0)','required'])!!}</td>
        <td>{!! Form::text('diskon_produk['.($row ?? 0).']', $diskon_produk ?? null,['class' => 'form-control diskon_produk','id' => '','onchange' => 'sumQtyAndUnit(0)','required'])!!}</td>
        <td width="10%">
            {!! Form::select('kode_pajak_id['.($row ?? 0).'][]',$kode_pajak,  $data_penawaran['kode_pajak_id'] ?? null,['class' => 'form-control select3 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => '',''])!!}
        </td>
        <td>{!! Form::text('amount_produk['.($row ?? 0).']', $amount_produk ?? null,['class' => 'mask form-control amount_produk','id' => '',''])!!}</td>
        <td>{!! Form::select('dept_id['.($row ?? 0).']',[], $dept_id ?? null,['class' => 'form-control select3 dept_id','id' => '',''])!!}</td>
        <td>{!! Form::select('proyek_id['.($row ?? 0).']',[], $proyek_id ?? null,['class' => 'form-control select3 proyek_id','id' => '',''])!!}</td>
        <td>{!! Form::text('terproses['.($row ?? 0).']', $terproses ?? null,['class' => 'form-control terproses','id' => '',''])!!}</td>
        <td style="display: none;">{!! Form::hidden('barang_id['.($row ?? 0).']', $barang_id ?? null,['class' => 'form-control barang_id','id' => 'barang_id',''])!!}</td>
        <td>
            {!! Form::checkbox('ditutup['.($row ?? 0).']',1, $ditutup ?? null,['class' => 'form-control col-form-label ditutup','id' => '',''])!!}
            {!! Form::hidden('harga_modal['.($row ?? 0).']', $harga_modal ?? null,['class' => 'form-control harga_modal','id' => '',''])!!}
        </td>
        <td><button href="" class="remove btn btn-danger remove-rincian-pesanan" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
    </tr>
@endif