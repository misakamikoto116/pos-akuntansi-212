@extends( auth()->user()->hasPermissionTo('ubah_pembayaran_pembelian') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Pembelian</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Pembayaran Pembelian</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Purchase Payment / Pembayaran Pembelian</h5>
            </div>
            <div class="card-body">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left','id' => 'form-penjualan-penawaran']) !!}
                <div class="p-20">
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary','id' => 'btn-cetak']) !!}
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

        <script type="text/javascript">
            $('.tanggal_pembayaran').datepicker({format: 'dd MM yyyy', autoclose: true});
            var q = 0;
            $(function() {
              if({{ $item->void_cheque }} == 0){
                enable_cb();
                $("#group1").click(enable_cb);
              }
                $("#pemasok_id").select2({
                    tags: true
                });
                @if (!old('pemasok_id'))
                    checkPemasok();
                @endif
                // checkPaymentAmount();
                setNamaAkun($('#akun_id').val());
                // sumOwing();
            });

            function enable_cb() {
              if (this.checked) {
                $("input.group2").removeAttr("disabled");
              } else {
                $("input.group2").attr("disabled", true).removeAttr("checked");
              }
            }

        $( "#pemasok_id" ).change(function() {
            removeFaktur();
            checkPemasok();
        });

        function removeFaktur(){
            $('.listInvoice').empty();
            q = 0;        
        }


        function checkPemasok(){
            var pemasok_id   = $('#pemasok_id').val();
            let total_owing = 0;
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-alamat-pemasok') }}",
                data: {id : pemasok_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#alamat_asal').val(response.alamat);
                $('#no_pemasok').val(response.no_pemasok);
                $.each(response.faktur, function (index, item){
                    $.map(item.invoice, function (el, val) {
                        if ($(".pembayaran_pembelian_id").val() == el.pembayaran_pembelian_id) {
                            duplicateForm();
                            let countTemp = q - 1;
                            $('#faktur_id'+countTemp).val(item.id);
                            $('#no_faktur'+countTemp).val(item.no_faktur);
                            $('#date'+countTemp).val(item.invoice_date_formatted);
                            $('#due'+countTemp).val(item.invoice_date_formatted);
                            $('#amount'+countTemp).autoNumeric('init',{aPad: false}).autoNumeric('set', item.total_formatted);
                            $('#owing'+countTemp).autoNumeric('init',{aPad: false}).autoNumeric('set', item.owing_after_retur_pemasok);
                            $('#owing_asli'+countTemp).val(item.owing_after_retur_pemasok + el.payment_amount);
                            $('#payment_amount'+countTemp).autoNumeric('init',{aPad: false}).autoNumeric('set', el.payment_amount);
                            $('.select2').select2();
                            total_owing += parseFloat(item.owing);
                            $('#text_total_owing').html(total_owing); 
                            if(el.payment_amount > 0){
                                $('#pay'+countTemp).prop("checked",true);
                            }
                            // isPayed(Number(el.payment_amount).toLocaleString(),countTemp);
                        }
                    });
                });
                setAkun();
                $(".select3").select2();
                sumTotalOwing();
                sumPaymentAmount();
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        function setNamaAkun(val){
             $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get.akun_id.rekonsiliasi_bank') }}",
                data: {id : val,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                    $('#akun').val(response.kode_akun);            
                }, failure: function(errMsg) {
                    alert(errMsg);
                },
            });
        }

            function duplicateForm() {
                var row = $('#table_invoice_section').html();
                var clone = $(row).clone();
                    clone.find('.faktur_id').attr('id', 'faktur_id' + q);
                    clone.find('.no_faktur').attr('id', 'no_faktur' + q);
                    clone.find('.date').attr('id', 'date' + q);
                    clone.find('.due').attr('id', 'due' + q);
                    clone.find('.amount').attr('id', 'amount' + q);
                    clone.find('.owing').attr('id', 'owing' + q);
                    clone.find('.owing_asli').attr('id', 'owing_asli' + q);
                    clone.find('.payment_amount').attr('id', 'payment_amount' + q).attr('onchange','isPayed(this.value,'+q+')');
                    clone.find('.pay').attr('id', 'pay' + q).attr('onchange','isPayChecked('+ q +')');
                    clone.find('.pay_hidden').attr('id', 'pay_hidden' + q);
                $(clone).appendTo($('.duplicate-invoice-sections').closest('.invoice-sections').find('.listInvoice'));
                q++;
                $('.select2').select2();
            }

            function isPayed(val,index){
                val = changeMaskingToNumber(val);
                if ($.isNumeric(val)) {
                    if(parseFloat(val) <= parseFloat($("#owing_asli"+ index).val())){
                        $('#pay' + index).prop('checked', true);
                        $("#pay_hidden" + index).attr('disabled','disabled');
                        owe = changeMaskingToNumber($('#owing_asli' + index).val());
                        var owing_format = owe - val;
                        $('#owing' + index).val(owing_format.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
                        sumTotalOwing();
                        sumPaymentAmount();        
                    }else{
                        $('#pay' + index).prop('checked', true);
                        $("#pay_hidden").removeAttr('disabled');
                        checkAmountOwing(index);                    
                    }
                }else {
                    swal({
                        icon: "error",
                        text: "Maaf, nominal yang anda masukkan salah"
                    });    
                }                       
            }

            function sumPaymentAmount() {
                let c = 0;
                $('.payment_amount').each(function () {
                    c += parseFloat(changeMaskingToNumber(this.value));
                });
                $('#cheque_amount').val(c.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
                $("#text_total_payment").text(c.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
            }

            function sumOwing() {
                let d = 0;
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.cek-retur') }}",
                    data: {pemasok_id : $('#pemasok_id').val(),
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                        let pengurangan_item = 0;
                        $.each(response.perFaktur, function(index, val){
                            $('.faktur_id').each(function(i,v){
                                if(v.value == val.faktur_pembelian_id){
                                    pengurangan_item += val.total;
                                    let owing_var = $('#owing'+i).autoNumeric('get');
                                    let owing_asli_var = $('#owing_asli'+i).val();
                                    $('#owing_asli'+i).val(owing_asli_var - val.total);
                                    $('#owing'+i).autoNumeric('set', owing_var - val.total);
                                }
                            });
                        }); 

                        $('.owing').each(function () {
                            d += parseFloat(changeMaskingToNumber(this.value) * 1);
                        });
                        // $("#tot_owing").val(d);
                        $("#text_total_owing").autoNumeric('init',{aPad: false}).autoNumeric('set',d);
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    },
                });
            }

            function checkAmountOwing(index) {
                swal({
                    icon: "error",
                    text: "Maaf, jumlah yang di bayar lebih besar dari pada terutang"
                });
                $("#payment_amount"+ index).autoNumeric('init',{aPad: false}).autoNumeric('set', 0);
            }

            function isPayChecked(q) {
                if ($("#pay" + q).is(':checked')) {
                    $("#pay_hidden" + q).attr('disabled','disabled');
                }else {
                    $("#pay_hidden" + q).removeAttr('disabled');
                }
            }

            function sumTotalOwing() {
                var d = 0;
                $('.owing').each(function () {
                    d += parseFloat(changeMaskingToNumber(this.value) * 1);
                });
                $("#text_total_owing").autoNumeric('init',{aPad: false}).autoNumeric('set',d);
            }

            function setAkun() {
                $("#akun_id").select2({
                    minimumInputLength: 3,
                    formatInputTooShort: function () {
                        return "Ketik 3 character atau lebih";
                    },
                    ajax: {
                        url: "{{ route('akuntansi.get-nama-akun') }}",
                        type: 'GET',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                kas: 1,
                                type: 'public'
                            }
                            return query;
                        },
                        dataType: 'JSON',
                        processResults: function (data) {
                            return {
                                results: data,
                            };
                        }
                    }
                });
            }

            // Fungsi button cetak di klik
            $("#btn-cetak").on("click", function(e) {
                e.preventDefault();
                btnCetak()
            });

            function btnCetak() {
                if ($("#pemasok_id").val() == "") {
                    swal({
                        icon: "warning",
                        text: "Pemasok/Barang Masih Kosong"
                    });
                }else {
                    var form = $("#form-penjualan-penawaran");
                    form.attr('target','_blank');
                    form.attr('action','{{ route('akuntansi.edit-cetak-pembayaran-pembelian') }}');
                    form.submit();
                }
            }

            // Fungsi button simpan tutup dan simpan lanjut di klik
            $("#btn-submit").on("click", function () {
                var form = $("#form-penjualan-penawaran");
                form.removeAttr('target');
                form.attr('action','{{ route($module_url->update,$item->id) }}');
                $("#btn-submit").on("submit", function () {
                    form.submit();
                });
            });
        </script>

@endsection
