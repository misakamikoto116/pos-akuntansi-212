@if (isset($item))
  {!! Form::hidden('pembayaran_id',$item->id,['class' => 'pembayaran_pembelian_id']) !!}
@endif
<div class="form-group row">
  <div class="col-1 col-form-label">Vendor</div>
  <div class="col-2">
  {!! Form::text('no_pemasok',null,['class' => 'form-control','id' => 'no_pemasok','readonly'])!!}
  </div>
  <div class="col-4">
  {!! Form::select('pemasok_id',$pemasok,old('pemasok_id'),['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'pemasok_id'])!!}
  </div>
</div>
<div class="row">
  <div class="offset-sm-3 col-sm-3">
    {!! Form::textarea('alamat_asal',null,['class' => 'form-control','id' => 'alamat_asal','readonly'])!!}
  </div>
  <div class="offset-sm-2 col-sm-4">
    <div class="form-group row">
      <div class="col-4 col-form-label">Form no.</div>
      <div class="col-8">
        {!! Form::text('form_no',(empty($item->form_no) ? $idPrediction : $item->form_no),['class' => 'form-control','id' => 'form_no','required'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Payment Date</div>
      <div class="col-8">
        @if (isset($item) || old('payment_date'))
        {!! Form::text('payment_date',null,['class' => 'form-control tanggal_pembayaran','id' => 'payment_date'])!!}
        @else
        {!! Form::text('payment_date',null,['class' => 'form-control tanggal','id' => 'payment_date'])!!}
        @endif
      </div>
    </div>
  </div>
</div>

<hr>

<div class="row">
  <div class="offset-sm-2 col-sm-4">
    <div class="form-group row">
      <div class="col-3 col-form-label">Bank</div>
      <div class="col-5">
        {!! Form::text('akun',null,['class' => 'form-control','id' => 'akun','readonly'])!!}
      </div>
      <div class="col-4">
        {!! Form::select('akun_bank_id',[''=>'Pilih Akun']+$akun,null,['class' => 'form-control select2','id' => 'akun_id','onchange'=>'setNamaAkun(this.value)'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Rate</div>
      <div class="col-3">
        {!! Form::text('rate',1,['class' => 'form-control','id' => '','readonly' => ''])!!}
      </div>
      <div class="col-6 col-form-label">
        Currency&emsp;<strong>IDR</strong>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Department</div>
      <div class="col-9">
        {!! Form::select('departement',[],null,['class' => 'form-control select2','id' => 'departement'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Project</div>
      <div class="col-9">
        {!! Form::select('project',[],null,['class' => 'form-control select2','id' => 'project'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Memo</div>
      <div class="col-9">
        {!! Form::textarea('memo',null,['class' => 'form-control','id' => 'memo'])!!}
      </div>
    </div>
  </div>

  <div class="offset-sm-2 col-sm-4">
    <div class="offset-sm-4">
    <div class="form-group row">
      <div class="col-2"><input class="form-control" type="checkbox" id="group1" value="1" @if(!empty($item)) @if($item->void_cheque == 1) {{ "checked" }} @endif @endif name="void_cheque"></div>
      <div class="col-10 col-form-label">Void Cheque</div>
    </div>
    <div class="form-group row">
      <div class="col-2"><input class="form-control group2" type="checkbox" id="group2" value="1" @if(!empty($item)) @if($item->fiscal_payment == 1) {{ "checked" }} @endif @endif name="fiscal_payment"></div>
      <div class="col-10 col-form-label">Fiscal Payment</div>
    </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Cheque No</div>
      <div class="col-8">
        {!! Form::text('cheque_no',null,['class' => 'form-control','id' => 'cheque_no'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Cheque Date</div>
      <div class="col-8">
        @if (isset($item) || old('cheque_date'))
        {!! Form::text('cheque_date',null,['class' => 'form-control tanggal_pembayaran','id' => 'cheque_date'])!!}
        @else
        {!! Form::text('cheque_date',null,['class' => 'form-control tanggal','id' => 'cheque_date'])!!}
        @endif
      </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Cheque Amount</div>
      <div class="col-8">
        {!! Form::text('cheque_amount',null,['class' => 'form-control','id' => 'cheque_amount','readonly'])!!}
      </div>
    </div>
  </div>
</div>

<div class="row invoice-sections">
  <div class="col-12 duplicate-invoice-sections">
    <table class="table duplicate-sections">
      <thead class="thead-dark">
        <tr>
          <th width="20%">Invoice No.</th>
          <th>Date</th>
          <th>Due</th>
          <th>Amount</th>
          <th>Owing</th>
          <th>Payment Amount</th>
          <th width="5%">Pay</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="listInvoice">
            @if(old('faktur_id') !== null)
              @foreach(old('faktur_id') as $row => $faktur_id )
              @php 
                if(empty(old('pay')[$row])){
                  $payment = 0;
                }else{
                  $payment = old('pay')[$row];
                }
              @endphp
                @include('akuntansi::pembayaran_pembelian/components/item_pembayaran_pembelian',[
                  'row'           => $row,
                  'faktur_id'     => $faktur_id,
                  'no_faktur'     => old('no_faktur')[$row],
                  'date'          => old('date')[$row],
                  'due'           => old('due')[$row],
                  'amount'        => old('amount')[$row],
                  'owing'         => old('owing')[$row],
                  'owing_asli'    => old('owing_asli')[$row],
                  'payment_amount'=> old('payment_amount')[$row],
                  'pay'           => $payment,
                ])
              @endforeach      
            @endif
      </tbody>
    </table>
  </div>
</div>
<hr>

<div class="row">
  <div class="col-sm-6">
    <div class="row">
      <div class="col-3 text-center">Total Owing : <span id="text_total_owing">0</span></div>
      <div class="col-3 text-center">Total Payment : <span id="text_total_payment">0</span></div>
      <div class="col-3 text-center">Total Discount : 0</div>
    </div>
  </div>
</div>

<script type="text/template" id="table_invoice_section" data-id=""/>
    <tr>
      <td>
          {!! Form::hidden('faktur_id[]', null, ['class' => 'faktur_id']) !!}
          {!! Form::text('no_faktur[]',null,['class' => 'form-control no_faktur','id' => '','readonly'])!!}
      </td>
      <td>{!! Form::text('date[]',null,['class' => 'form-control date','id' => '','readonly'])!!}</td>
      <td>{!! Form::text('due[]',null,['class' => 'form-control due','id' => '','readonly'])!!}</td>
      <td>{!! Form::text('amount[]',null,['class' => 'form-control amount','id' => '','readonly'])!!}</td>
      <td>
          {!! Form::text('owing[]',null,['class' => 'form-control owing','id' => '','readonly'])!!}
          {!! Form::hidden('owing_asli[]',null,['class' => 'form-control owing_asli','id' => ''])!!}
      </td>
      <td>{!! Form::text('payment_amount[]',null,['class' => 'form-control payment_amount mask','id' => ''])!!}</td>
      <th>
          <input class="form-control pay" type="checkbox" id="" value="1" name="pay[]">
          {!! Form::hidden('pay[]',0,['class' => 'pay_hidden']) !!}
      </th>
    </tr>
</script>