@extends( auth()->user()->hasPermissionTo('daftar_pembayaran_pembelian') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                @include('chichi_theme.layout.filter')
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Daftar Pembayaran Pembelian</a></li>
                    </ol>
                </div>
            </div>
<!-- END Page-Title -->

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">{{$title_document}}</h5>
                @can('buat_pembayaran_pembelian')
                    <div class="menu-header">
                        <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-plus"></i></span>
                            Tambah
                        </a>
                    </div>
                @endcan
            </div>
            <div class="card-body">
                @if($items->isEmpty())
                    <div class="alert alert-warning"> Tidak ada data. </div>
                @else
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>No. Pemasok</th>
                            <th>Nama Pemasok</th>
                            <th>No Cek</th>
                            <th>Jumlah</th>
                            <th>Keterangan</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $key => $item)
                            <tr>
                                <td>{{ $key + $items->firstItem() }}</td>
                                <td>{{ \Helpers\IndonesiaDate::indonesiaDate($item->payment_date) }}</td>
                                <td>{{ $item->pemasok->no_pemasok }}</td>
                                <td>{{ $item->pemasok->nama }}</td>
                                <td>{{ $item->cheque_no }}</td>
                                <td>Rp. {{ number_format($item->cheque_amount) }}</td>
                                <td>{{ $item->memo }}</td>
                                @if ( auth()->user()->hasPermissionTo('lihat_pembayaran_pembelian') || auth()->user()->hasPermissionTo('ubah_pembayaran_pembelian') || auth()->user()->hasPermissionTo('hapus_pembayaran_pembelian') )
                                    <td>
                                        {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                                <div class="dropdown-menu">
                                                    @can('lihat_pembayaran_pembelian')
                                                        <a class="dropdown-item jurnal" href="#">
                                                            <i class="fa fa-table"></i>&nbsp;Detail
                                                        </a>
                                                    @endcan
                                                    @can('ubah_pembayaran_pembelian')
                                                        <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i> Edit</a>
                                                    @endcan
                                                    @can('hapus_pembayaran_pembelian')
                                                        <a class="dropdown-item deleteBtn" href="#">
                                                            <i class="fa fa-trash"></i> Delete
                                                        </a>
                                                    @endcan
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
                <div class="pull-right">
                    {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                </div>
            </div>
 </div>
    </div>
</div>
@endsection

@section('custom_js')
    <script type="text/javascript">
        $('.mask_index').autoNumeric('init');
    </script>
@endsection