<tr>
  <td>
      {!! Form::hidden('faktur_id['.($row ?? 0).']', $faktur_id ?? null, ['class' => 'faktur_id','id' => 'faktur_id'.$row]) !!}
      {!! Form::text('no_faktur['.($row ?? 0).']', $no_faktur ?? null,['class' => 'form-control no_faktur','id' => 'no_faktur'.$row,'readonly'])!!}
  </td>
  <td>{!! Form::text('date['.($row ?? 0).']', $date ?? null,['class' => 'form-control date','id' => 'date'.$row,'readonly'])!!}</td>
  <td>{!! Form::text('due['.($row ?? 0).']', $due ?? null,['class' => 'form-control due','id' => 'due'.$row,'readonly'])!!}</td>
  <td>{!! Form::text('amount['.($row ?? 0).']', $amount ?? null,['class' => 'form-control amount','id' => 'amount'.$row,'readonly'])!!}</td>
  <td>
      {!! Form::text('owing['.($row ?? 0).']',$owing ?? null,['class' => 'form-control owing','id' => 'owing'.$row,'readonly'])!!}
      {!! Form::hidden('owing_asli['.($row ?? 0).']',$owing_asli ?? null,['class' => 'form-control owing_asli','id' => 'owing_asli'.$row])!!}
  </td>
  <td>{!! Form::text('payment_amount['.($row ?? 0).']', $payment_amount ?? null,['class' => 'form-control payment_amount mask','id' => 'payment_amount'.$row,'onchange' => 'isPayed(this.value,'.$row.')'])!!}</td>
  <th>
      @if ($payment == 0)
        <input class="form-control pay" type="checkbox" id="pay{{ $row }}" value="1" name="pay[]" onchange="isPayChecked({{ $row }})">
        {!! Form::hidden('pay[]', !empty(old('pay')) ? 0 : 0,['class' => 'pay_hidden','id' => 'pay_hidden'.$row]) !!}
      @else
        <input class="form-control pay" type="checkbox" id="pay{{ $row }}" value="1" checked="" name="pay[]" onchange="isPayChecked({{ $row }})">
        {!! Form::hidden('pay[]', !empty(old('pay')) ? 0 : 0,['class' => 'pay_hidden','id' => 'pay_hidden'.$row,'disabled' => 'disabled']) !!}
      @endif
  </th>
</tr>