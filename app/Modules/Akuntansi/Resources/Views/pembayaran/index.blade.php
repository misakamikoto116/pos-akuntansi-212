@extends( $permission['daftar'] ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
<!-- Page-Title -->
    <div class="row">
        @include('chichi_theme.layout.filter')
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">{{$title}}</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Daftar Pembayaran</a></li>
            </ol>
        </div>
    </div>
<!-- END Page-Title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">{{$title_document}}</h5>
                    <div class="menu-header">
                        @can($permission['buat'])
                            <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                                <span class="btn-label"><i class="fa fa-plus"></i></span>
                                Tambah
                            </a>
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    @if($items->isEmpty())
                        <div class="alert alert-warning"> Tidak ada data. </div>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Penerima</th>
                                <th>Faktur</th>
                                <th>Cek</th>
                                <th>Tanggal</th>
                                <th>Catatan</th>
                                <th>Nominal</th>
                                <th>
                                    @can($permission['laporan'])
                                        <a href="{{ url('akuntansi/pembayaran/laporan/') }}" class="btn btn-info btn-rounded waves-effect waves-light">
                                            <span class="btn-label"><i class="fa fa-info"></i></span>
                                            Print
                                        </a>
                                    @endcan
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $key => $item)
                                <tr>
                                    <td>{{$key + $items->firstItem()}}</td>
                                    <td>{{$item->peruntukan}}</td>
                                    <td>{{$item->no_faktur}}</td>
                                    <td>{{$item->no_cek}}</td>
                                    <td>{{ \Helpers\IndonesiaDate::indonesiaDate($item->tanggal) }}</td>
                                    <td>{{$item->keterangan}}</td>
                                    <td><span class="mask_index">{{$item->nominal}}</span></td>
                                    <td>
                                        @if($permission['lihat'] || $permission['ubah'] || $permission['hapus'])
                                            {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                                    <div class="dropdown-menu">
                                                        @can($permission['lihat'])
                                                            <a class="dropdown-item jurnal" href="{{ route('akuntansi.show.jurnal_pembayaran',$item->id) }}">
                                                                <i class="fa fa-table"></i>&emsp;Jurnal
                                                            </a>
                                                        @endcan
                                                        @can($permission['ubah'])
                                                            <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i>&emsp;Edit
                                                            </a>
                                                        @endcan
                                                        @can($permission['hapus'])
                                                            <a class="dropdown-item deleteBtn" href="#">
                                                                <i class="fa fa-trash"></i>&emsp;Delete
                                                            </a>
                                                        @endcan
                                                    </div>
                                                </div>
                                            {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                    <div class="pull-right">
                        {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script type="text/javascript">
        $('.mask_index').autoNumeric('init',{aPad: false});
    </script>
@endsection