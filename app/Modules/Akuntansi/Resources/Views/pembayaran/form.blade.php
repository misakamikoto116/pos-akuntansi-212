<div class="row">
	<div class="col-md-12">
		<div class="form-group row">
		{!! Form::label('akun_id','Dari',['class' => 'col-1 col-form-label']) !!}
		<div class="col-md-2" style="padding-left: 20px;">
		{!! Form::text('ww',session()->get('akun_id'),['class' => 'form-control','id' => 'akun_id' , 'readonly'])!!}
		</div>
		<div class="col-md-5">
			{!! Form::select('akun_id', $listAkunId, null, ['id' => 'nama_akun', 'class' => 'form-control target select2','placeholder' => '- Pilih -']) !!}
		</div>
		</div>
	</div>

	<div class="col-md-6">
	<div class="form-group row">
	{!! Form::label('no_faktur','Voucher No.',['class' => 'col-sm-2 col-form-label']) !!}
	<div class="col-sm-10" style="padding-left: 20px;">
	{!! Form::text('no_faktur',session()->get('no_faktur'),['class' => 'form-control','id' => 'no_faktur'])!!}
	</div>
	</div>
	</div>

	<div class="col-md-6">
	<div class="form-group row">
	{!! Form::label('no_cek','No. Cek',['class' => 'col-sm-2 col-form-label']) !!}
	<div class="col-sm-10" style="padding-left: 20px;">
	{!! Form::text('no_cek',session()->get('no_cek'),['class' => 'form-control','id' => 'no_cek'])!!}
	</div>
	</div>		
	</div>

	<div class="col-md-6">
	<div class="form-group row">
	{!! Form::label('peruntukan','Penerima',['class' => 'col-sm-2 col-form-label']) !!}
	<div class="col-sm-10" style="padding-left: 20px;">
	{!! Form::text('peruntukan',session()->get('peruntukan'),['class' => 'form-control','id' => 'peruntukan'])!!}
	</div>
	</div>
	</div>

	<div class="col-md-6">
	<div class="form-group row">
	{!! Form::label('tanggal','Tanggal',['class' => 'col-sm-2 col-form-label']) !!}
	<div class="col-sm-10" style="padding-left: 20px;">
	@if (isset($item))
    	{!! Form::text('tanggal',null,['class' => 'form-control','id' => 'tanggal_pembayaran'])!!}
	@else
    	{!! Form::text('tanggal',null,['class' => 'form-control tanggal','id' => 'tanggal'])!!}
	@endif
	</div>
	</div>
	</div>

	<div class="col-md-12">
	<div class="form-group row">
	{!! Form::label('keterangan','Keterangan',['class' => 'col-sm-1 col-form-label']) !!}
	<div class="col-sm-11" style="padding-left: 20px;">
	{!! Form::textarea('keterangan',session()->get('keterangan'),['class' => 'form-control','id' => 'keterangan','rows' => '3'])!!}
	</div>
	</div>
	</div>

	<div class="col-md-6">
		<div class="form-group row">
			{!! Form::label('nominal','Jumlah',['class' => 'col-sm-2 col-form-label']) !!}
			<div class="col-sm-10" style="padding-left: 20px;">
				<div class="input-group mb-2 mr-sm-2 mb-sm-0">
					<div class="input-group-addon">Rp</div>
					{!! Form::text('nominal',session()->get('nominal'),['class' => 'form-control mask', 'id' => 'nominal', 'onkeyup' => 'terbilang();'])!!}
					<button class="input-group-addon" type="button" id="balancing">
						<i class="fa fa-calculator"></i>
					</button>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-6">
	<div class="form-group row">
	{!! Form::label('',session()->get('terbilang'),['class' => 'col-sm-10 col-form-label','id' => 'terbilang']) !!}
	</div>
	</div>

</div>

