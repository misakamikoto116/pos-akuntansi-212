@extends( auth()->user()->hasPermissionTo('ubah_pembayaran_lain') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ route('akuntansi.pembayaran.index') }}">Daftar Pembayaran</a></li>
                      <li><a href="#">Edit Pembayaran</a></li>
                    </ol>

                </div>
            </div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{$title_document}}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left myForm', 'id' => 'myForm']) !!}
                @include($form)
                </div>
            <div class="col-sm-12">
                {{-- <div class="menu-header">
            </div> --}}
            <div class="title" style="margin-bottom: 10px; font-size: 12px; font-weight: 1000;">Merincikan Pembayaran</div>
        <div class="form form-horizontal form-label-left bersih">
        <table id="myTable" class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="17%">No. Akun</th>
                        <th width="30%">Nama Akun</th>
                        <th>Jumlah</th>
                        <th>Catatan</th>
                    </tr>
                </thead>
                <tbody class="tbody_rincian">
                    <tr>
                        <td>{!! Form::text('akun_detail_id',null,['class' => 'form-control','id' => 'detail_akun_id','readonly'])!!}</td>
                        <td><select class="form-control nama_akun_ubah select2 rincian_pembayaran_required" name="nama_akun" id="detail_nama_akun" >
                                <option value="">- Pilih Akun -</option>
                                @foreach ($listAkun as $akun)
                                <option value="{{$akun}}">{{$akun}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon">Rp</div>
                        {!! Form::text('jumlah',null,['class' => 'form-control mask rincian_pembayaran_required','id' => 'detail_nominal'])!!}
                        </div>
                        </td>
                        <td>{!! Form::text('catatan',null,['class' => 'form-control rincian_pembayaran_required','id' => 'detail_catatan', 'placeholder' => '-'])!!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div style="float: left" class="block-hide">

        </div>

        <div class="hint" style="float: right; color: #618788;">
            <small>Tekan "TAB" pada keyboard untuk menambah rincian</small>
        </div>
        </div>
        </div>

        <div class="card-header" style="z-index: 0; position: relative; border-radius: 8px; margin-top: 20px;">
            <h5 class="title">Rincian Pembayaran</h5>
        </div>

        <div class="card-body">
        <table class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No. Akun</th>
                        <th>Nama Akun</th>
                        <th>Jumlah</th>
                        <th>Catatan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="example">
                @foreach (Cart::instance('pembayaran')->content() as $row)
                    <tr id="{{ $row->rowId }}">
                        <td class="td_id">{{$row->options->akun_id}}</td>
                        <td class="td_nama">{{$row->options->nama_detail_akun}}</td>
                        <td class="harga duit">{{$row->price}}</td>
                        <td class="td_catatan">{{$row->options->catatan_detail}}</td>
                        <td>
                            <a href="#" class="btn btn-warning edit_cart" data-id="{{ $row->rowId }}" data-link=""><i class="fa fa-pencil"></i> </a>
                            <a href="#" class="btn btn-danger remove_cart" data-id="{{ $row->rowId }}" data-link="{{ route('akuntansi.pembayaran.delete_cart') }}"> <i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <hr>
            <div align="right" style="padding: 0 10px">
               <strong>TOTAL :</strong>&emsp; <span id="total">Rp {{Cart::instance('pembayaran')->total()}}</span>
            </div>
            <hr>
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default' ,'id' => 'btnSubmit']) !!}
                </div>
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary','style' => 'float : right', 'id' => 'btn-cetak']) !!}
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

<script type="text/template" id="btnUpdate">
    <button type="button" class="btn btn-warning btn-md" id="btnBatal">Batal</button>
</script>

<script type="text/javascript">
    $('#tanggal_pembayaran').datepicker({format: 'dd MM yyyy', autoclose: true});
    $('.duit').autoNumeric('init',{aPad: false});

    $("#btn-cetak").on("click", function(e) {
        e.preventDefault();
        btnCetak()
    });

    function btnCetak() {
        if ($("#nama_akun").val() == "" || $('#peruntukan').val() == "" || $('#keterangan').val() == "") {
            swal({
                icon: "warning",
                text: "Masih Ada Form Yang Kosong!"
            });
        }else {
            var form = $("#myForm");
            form.attr('target','_blank');
            form.attr('action','{{ route('akuntansi.edit-cetak-pembayaran') }}');
            form.submit();
        }
    }

    $("#btnSubmit").on("click", function () {
        var form = $("#myForm");
        form.removeAttr('target');
        form.attr('action','{{ route($module_url->update,$item->id) }}');
        $("#btnSubmit").on("submit", function () {
            form.submit();
        });
    });
</script>
<script src="{{asset('assets/js/pembayaran.js')}}"></script>

@endsection