@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Jurnal Transaksi Pembayaran</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ route('akuntansi.pembayaran.index') }}">Daftar Pembayaran</a></li>
                      <li><a href="#">Jurnal Transaksi Pembayaran</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Jurnal Transaksi Pembayaran</h5>
            </div>
            <div class="card-body">
                <div class="p-20">

                <div class="row">

                <div class="col-md-6">

                <div class="form-group row">
                {!! Form::label('no_faktur','No.Faktur',['class' => 'col-sm-2 col-form-label']) !!}
                <div class="col-sm-10" style="padding-left: 20px;">
                    <label class="col-sm-2 col-form-label">{{$buku_keluar->no_faktur}}</label>
                </div>
                </div>

                <div class="form-group row">
                {!! Form::label('tanggal','Tanggal',['class' => 'col-sm-2 col-form-label']) !!}
                <div class="col-sm-10" style="padding-left: 20px;">
                    <label class="col-sm-10 col-form-label">{{ \Helpers\IndonesiaDate::indonesiaDate($buku_keluar->tanggal) }}</label>
                </div>
                </div>

                <div class="form-group row">
                {!! Form::label('keterangan','Keterangan',['class' => 'col-sm-2 col-form-label']) !!}
                <div class="col-sm-10" style="padding-left: 20px;">
                    <label class="col-sm-10 col-form-label">{{$buku_keluar->keterangan}}</label>
                </div>
                </div>

                </div>

                </div>

                <div>
                <div>
                <table class="table" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No. Akun</th>
                                <th>Nama Akun</th>
                                <th>Debet</th>
                                <th>Kredit</th>
                                <th>Catatan</th>
                            </tr>
                        </thead>
                        <tbody id="example">
                            @foreach($detail_buku_keluar as $detail)
                            <tr>
                                <td>{{$detail->akun->kode_akun}}</td>
                                <td>{{$detail->akun->nama_akun}}</td>
                                <td><span class="jurnal_mask">{{$detail->nominal}}</span></td>
                                <td></td>
                                <td>{{$detail->catatan}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td>{{$buku_keluar->akun->kode_akun}}</td>
                                <td>{{$buku_keluar->akun->nama_akun}}</td>
                                <td></td>
                                <td><span class="jurnal_mask">{{$buku_keluar->nominal}}</span></td>
                                <td>{{$buku_keluar->keterangan}}</td>
                            </tr>
                        </tbody>
                    </table>
            </div>
            </div>


            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
    {{-- Masking Nominal --}}
    <script type="text/javascript">
        $('.jurnal_mask').autoNumeric('init',{aPad: false, aSign: "Rp "});
    </script>
@endsection