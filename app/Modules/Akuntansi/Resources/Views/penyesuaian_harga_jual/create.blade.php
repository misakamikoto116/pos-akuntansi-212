@extends( auth()->user()->hasPermissionTo('buat_selling_price_adjusment') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9{
        padding: 0 15px;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Penyesuaian Harga Jual</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Penyesuaian Harga Jual</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Penyesuaian Harga Jual</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left', 'id' => 'form-pencatatan-nomor-serial']) !!}
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection
@section('custom_js')
    <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
    <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/starting.js') }}"></script>
    <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

    <script>
        var q = 0;
        function duplicatePenyesuaianHargaJual(){
            var row = $('#table_penyesuaian_harga_jual_section').html();
            var clone = $(row).clone();
            clone.find('.produk_id').attr('name','produk_id[]').attr('id','produk_id'+q).attr('onchange','test('+q+')').attr('class','select2 form-control');
            clone.find('.keterangan_produk').attr('name','keterangan_produk[]').attr('id','keterangan_produk'+q);
            clone.find('.qty_produk').attr('name','qty_produk[]').attr('id','qty_produk'+q).attr('onchange','sumQtyAndUnit('+q+')');
            clone.find('.satuan_produk').attr('name','satuan_produk[]').attr('id','satuan_produk'+q);
            clone.find('.harga_satu').attr('name','harga_satu[]').attr('id','harga_satu'+q).attr('onchange','sumQtyAndUnit('+q+')');
            clone.find('.harga_dua').attr('name','harga_dua[]').attr('id','harga_dua'+q).attr('onchange','sumQtyAndUnit('+q+')');
            clone.find('.nomor_tiga').attr('name','nomor_tiga[]').attr('id','nomor_tiga'+q).attr('onchange','sumQtyAndUnit('+q+')');
            clone.find('.nomor_empat').attr('name','nomor_empat[]').attr('id','nomor_empat'+q).attr('onchange','sumQtyAndUnit('+q+')');
            clone.find('.nomor_lima').attr('name','nomor_lima[]').attr('id','nomor_lima'+q).attr('onchange','sumQtyAndUnit('+q+')');
            $(clone).appendTo($('.duplicate-penyesuaian-harga-jual-sections').closest('.penyesuaian-harga-jual-sections').find('.penyesuaianhargajual'));
            q++;
            $('.select2').select2();
            saveToSession();
        }

        function test(vale) {
            var produk_id   = $('#produk_id'+vale).val();
            var pelanggan_id   = $('#pelanggan_id').val();
                    $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-produk') }}",
                    data: {id : produk_id, pelanggan_id : pelanggan_id,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                    $('#keterangan_produk'+vale).val(response.keterangan);
                    $('#qty_produk'+vale).val('1');
                    $('#satuan_produk'+vale).val(response.satuan);
                    sumQtyAndUnit(vale);            
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
        }

        $(document).ready(function () {
            $('.duplicate-penyesuaian-harga-jual-sections').on('click', '.add-penyesuaian-harga-jual', function () {
                duplicatePenyesuaianHargaJual();
            });
        });
        $('.duplicate-penyesuaian-harga-jual-sections').on('click', '.remove-rincian-penyesuaian-harga-jual', function () {
            if ($(this).closest('.penyesuaianhargajual').find('tr').length > 0) {
                $(this).closest('tr').remove();
            }
        });
    </script>
@endsection