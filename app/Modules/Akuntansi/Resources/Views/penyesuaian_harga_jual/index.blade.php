@extends( auth()->user()->hasPermissionTo('daftar_selling_price_adjusment') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title"></h4>

            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Penyesuaian Harga Jual</a></li>
            </ol>

        </div>
    </div>   

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">Penyesuaian Harga Jual</h5>
                    @can('buat_selling_price_adjusment')
                        <div class="menu-header">
                            <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                                <span class="btn-label"><i class="fa fa-plus"></i></span>
                                Tambah
                            </a>    
                        </div>
                    @endcan
                </div>
                <div class="card-body">
                    <table class="table">
                    @if ($items->isEmpty())
                        <div class="alert alert-warning"> Tidak ada data. </div>
                    @else
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Penyesuaian</th>
                                <th>Tanggal Penyesuaian</th>
                                <th>Tanggal Efektif</th>
                                <th>Keterangan</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $item)
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    @if(auth()->user()->hasPermissionTo('hapus_selling_price_adjusment') || auth()->user()->hasPermissionTo('ubah_selling_price_adjusment'))
                                        {!! Form::open(['route' => [$module_url->destroy, $item->id],'method' => 'DELETE', 'class' => 'delete']) !!}
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                                <div class="dropdown-menu">
                                                    @can('ubah_selling_price_adjusment')
                                                        <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i>&emsp;Edit</a>
                                                    @endcan
                                                    @can('hapus_selling_price_adjusment')
                                                        <a class="dropdown-item deleteBtn" href="#">
                                                            <i class="fa fa-trash"></i>&emsp;Delete
                                                        </a>
                                                    @endcan
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    @endif
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                    @endif
                    </table>
                    <div class="pull-right">                    
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
@endsection

@section('custom_js')

@endsection
