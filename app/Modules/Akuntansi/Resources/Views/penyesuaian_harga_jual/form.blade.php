<div class="form-group row">
  <div class="col-2 col-form-label">No. Penyesuaian</div>
  <div class="col-3">
    {!! Form::text('no_penyesuaian',null,['class'=>'form-control']) !!}
  </div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Tgl. Efektif</div>
  <div class="col-3">
    {!! Form::date('tgl_efektif',null,['class'=>'form-control','id' => 'tgl_efektif'])!!}
  </div>
</div>
<hr>
<div class="form-group row">
  <div class="col-2 col-form-label">Tgl. Penyesuaian</div>
  <div class="col-3">
    {!! Form::date('tgl_penyesuaian',null,['class'=>'form-control','id' => 'tgl_penyesuaian'])!!}
  </div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Keterangan</div>
  <div class="col-3">
    {!! Form::textarea('keterangan',null,['class'=>'form-control','id' => 'keterangan'])!!}
  </div>
</div>
<hr>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>

<div class="penyesuaian-harga-jual-sections" data-id="1">
<div class="duplicate-penyesuaian-harga-jual-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab">
    <table width="100%" class="table">
      <thead class="thead-light" style="border-collapse: collapse;">
        <tr>
          <th widht="20%">Item No</th>
          <th>Keterangan</th>
          <th>Biaya AKtual</th>
          <th>Anggaran</th>
          <th>Harga 1</th>
          <th>Harga 2</th>
          <th>Harga 3</th>
          <th>Harga 4</th>
          <th>Harga 5</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="penyesuaianhargajual">
        @if(isset($item))
          @if(old('produk_id') !== null)  
              @foreach(old('produk_id') as $row => $produk_id )
                @include('akuntansi::penyesuaian_harga_jual/components/item_penyesuaian_harga_jual',[
                  'row'                                 => $row, 
                  'produk_id'                           => $produk_id,
                  'keterangan_produk'                   => old('keterangan_produk')[$row],
                  'qty_produk'                          => old('qty_produk')[$row],
                  'satuan_produk'                       => old('satuan_produk')[$row],
                  'harga_satu'                          => old('harga_satu')[$row],
                  'harga_dua'                           => old('harga_dua')[$row],
                  'harga_tiga'                          => old('harga_tiga')[$row],
                  'harga_empat'                         => old('harga_empat')[$row],
                  'harga_lima'                          => old('harga_slima')[$row],
                ])
              @endforeach      
          @else
            <!-- Foreach -->
              <tr>
                <td>{{--!! Form::select('produk_id[]',$produk,$data->produk_id,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id', 'required'])!!--}}</td>
                <td>{{--!! Form::text('keterangan[]',$data->keterangan,['class' => 'form-control keterangan_produk','id' => '',''])!!--}}</td>
                <td>{{--!! Form::text('jumlah[]',$data->jumlah,['class' => 'form-control qty_produk','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!--}}</td>
                <td>{{--!! Form::text('satuan[]',$data->satuan,['class' => 'form-control satuan_produk','id' => '',''])!!--}}</td>
                <td>{{--!! Form::text('serial_number[]',$data->serial_number,['class' => 'form-control serial_number','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!--}}</td>
                <!-- <td><button href="" class="remove btn btn-danger remove-rincian-serial-barang" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td> -->
              </tr>
            <!-- Akhir Foreach -->
          @endif
        @elseif(old('produk_id') !== null)  
          @foreach(old('produk_id') as $row => $produk_id )
            @include('akuntansi::penyesuaian_harga_jual/components/item_penyesuaian_harga_jual',[
              'row'                                 => $row, 
              'produk_id'                           => $produk_id,
              'keterangan_produk'                   => old('keterangan_produk')[$row],
              'qty_produk'                          => old('qty_produk')[$row],
              'satuan_produk'                       => old('satuan_produk')[$row],
              'harga_satu'                          => old('harga_satu')[$row],
              'harga_dua'                           => old('harga_dua')[$row],
              'harga_tiga'                          => old('harga_tiga')[$row],
              'harga_empat'                         => old('harga_empat')[$row],
              'harga_lima'                          => old('harga_slima')[$row],
            ])
          @endforeach      
        @endif
      </tbody>
    </table>
    <a class="btn btn-info add-penyesuaian-harga-jual" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
</div>

</div><!-- END PILLS -->

</div> <!-- End Tab-Content -->
<script type="text/template" id="table_penyesuaian_harga_jual_section" data-id="">
  <tr>
    <td>{!! Form::select('produk_id[]',$produk,null,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id'])!!}</td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('qty_produk[]',0,['class' => 'form-control qty_produk','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('harga_satu[]',null,['class' => 'form-control harga_satu','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td>{!! Form::text('harga_dua[]',null,['class' => 'form-control harga_dua','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td>{!! Form::text('harga_tiga[]',null,['class' => 'form-control harga_tiga','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td>{!! Form::text('harga_empat[]',null,['class' => 'form-control harga_empat','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td>{!! Form::text('harga_lima[]',null,['class' => 'form-control harga_lima','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td><button href="" class="remove btn btn-danger remove-rincian-penyesuaian-harga-jual" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
	</tr>
</script>