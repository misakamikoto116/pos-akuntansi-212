<tr>
    <td>{!! Form::select('produk_id['.($row ?? 0).']',$produk,$produk ?? null,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id'])!!}</td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('qty_produk[]',0,['class' => 'form-control qty_produk','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('harga_satu[]',null,['class' => 'form-control harga_satu','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td>{!! Form::text('harga_dua[]',null,['class' => 'form-control harga_dua','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td>{!! Form::text('harga_tiga[]',null,['class' => 'form-control harga_tiga','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td>{!! Form::text('harga_empat[]',null,['class' => 'form-control harga_empat','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td>{!! Form::text('harga_lima[]',null,['class' => 'form-control harga_lima','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td><button href="" class="remove btn btn-danger remove-rincian-penyesuaian-harga-jual" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>