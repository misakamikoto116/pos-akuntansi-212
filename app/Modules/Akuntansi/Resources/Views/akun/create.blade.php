@extends( auth()->user()->hasPermissionTo('buat_daftar_akun') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

@endsection

@section('content')

<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ route('akuntansi.akun.index') }}">Daftar Akun</a></li>
                      <li><a href="#">Tambah Akun</a></li>
                    </ol>


                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left']) !!}

                @include($form)
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')
<script src="{{ asset('js/akun.js') }}"></script>
<script type="text/javascript">
    
/*get akun parent*/
$( ".target" ).change(function() {
            var tipe_akun_id   = $('#tipe_akun_id').val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-akun-tipe') }}",
                data: {id : tipe_akun_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                    $('.parent_id').find('option').remove().end();
                    $('.parent_id').find('optgroup').remove().end();
                    $.each(response, function(i, value) {
                        $.each(value, function (index, element) {
                            $('.parent_id').append($('<optgroup>').attr("label", index));
                            $.each(element, function (waw, wew) {
                                $('.parent_id').append($("<option value="+waw+">").text(wew));
                            });

                        });
                });
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        });

/*get kode akun*/
$( "#selectgrup" ).change(function() {
            var akun_id   = $('#selectgrup').val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-akun-kode') }}",
                data: {id : akun_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#kode_akun').val(response.kode_akun)
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        });
</script>
@endsection