<div class="form-group row">
	{!! Form::label('tipe_akun_id','Tipe Akun',['class' => 'col-2 col-form-label']) !!}
		<div class="col-6">
		{!! Form::hidden('tipe_akun_nama', null, ['id' => 'tipe_akun_nama']) !!}
		{!! Form::select('tipe_akun_id',$listTipeAkun,null,['class' => 'form-control target select2','id' => 'tipe_akun_id','placeholder' => '-- pilih --'])!!}
		</div>
</div>

<div class="form-group row opsi">
	{!! Form::label('Opsi','Opsi Akun',['class' => 'col-2 col-form-label']) !!}

		<div class="col-4" style="margin-left:12px;">
			{!! Form::label('use_parent','Gunakan Induk/Parent',['class' => 'col-10','style' => 'float:right']) !!}
			<div class="col-2">
				@if (isset($item))
					@if (!empty($item->parentAkun))
						{!! Form::checkbox('use_parent', 1, true,['class' => 'use_parent','style' => 'float:left']) !!}
					@else
						{!! Form::checkbox('use_parent', 1, false,['class' => 'use_parent','style' => 'float:left']) !!}
					@endif
				@else
					{!! Form::checkbox('use_parent', 1, false,['class' => 'use_parent','style' => 'float:left']) !!}
				@endif
			</div>
		</div>
	</div>

<div class="form-group row parent-row">
	{!! Form::label('parent_id','Akun Parent',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
        {!! Form::select('parent_id',[],null ,['class' => 'form-control parent_id select2','id' => 'selectgrup']) !!}
		
	</div>
</div>
<div class="form-group row">
	{!! Form::label('kode_akun','Kode Akun',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::text('kode_akun',null,['class' => 'form-control','id' => 'kode_akun'])!!}
</div>
</div>
<div class="form-group row">
	{!! Form::label('nama_akun','Nama Akun',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::text('nama_akun',null,['class' => 'form-control','id' => 'nama_akun'])!!}
	</div>
</div>

<div class="form-group row opsi">
	{!! Form::label('Opsi','Opsi',['class' => 'col-2 col-form-label']) !!}

	<div class="fiksal col-4" style="margin-left:12px;">
		{!! Form::label('fiksal','Fiksal',['class' => 'col-10','style' => 'float:right']) !!}
		<div class="col-2">
			{!! Form::checkbox('fiksal',1, null,['style' => 'float:left'])!!}
    	</div>
	</div>	

	<div class="alokasi_ke_produksi col-4" style="margin-left:12px;">
		{!! Form::label('alokasi_ke_produksi','Dialokasikan Ke Produksi',['class' => 'col-10','style' => 'float:right']) !!}
		<div class="col-2">
			{!! Form::checkbox('alokasi_ke_produksi',1, null,['style' => 'float:left'])!!}
    	</div>
	</div>	


</div>

<div class="form-group row mata_uang">
	{!! Form::label('mata_uang','Mata Uang',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
        {!! Form::select('mata_uang_id', $listMataUang, null ,['class' => 'form-control mata_uang_id select2','id' => 'mata_uang_id']) !!}
	</div>
</div>
<div class="form-group row saldo_awal">
	{!! Form::label('money_function','Saldo Awal',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::text('money_function',!empty($item) ? number_format($item->money_function_from_transaction) : 0,['class' => 'form-control mask','id' => 'money_function'])!!}
	</div>
</div>

<div class="form-group row tanggal_masuk">
	{!! Form::label('tanggal_masuk','Tanggal Masuk',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::text('tanggal_masuk',null,['class' => 'form-control tanggal','id' => 'tanggal_masuk'])!!}
	</div>
</div>


