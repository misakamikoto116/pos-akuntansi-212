@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')

<!-- Page-Title -->
    <div class="row">
        @include('chichi_theme.layout.filter')
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">{{$title}}</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Daftar Akun</a></li>
            </ol>

        </div>
    </div>   
<!-- END Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">Daftar Akun</h5>
                    @can($permission['buat'])
                        <div class="menu-header">
                            <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                                <span class="btn-label"><i class="fa fa-plus"></i></span>
                                Tambah
                            </a>    
                        </div>
                    @endcan
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kode Akun</th>
                                <th>Nama Akun</th>
                                <th>Tipe Akun</th>
                                <th>Saldo</th>
                                {{-- @include('chichi_theme.layout.created_updated_deleted_th') --}}
                                <th></th>
                            </tr>         
                        </thead>
                        <tbody>
                        <?php $i = 0; ?>
                        @foreach ($items as $item)
                        <tr
                            <?php
                                if($item['parent_stat'] == 1){
                                    echo "style='font-weight: bold'";
                                }
                            ?>
                        >
                            <td>{{$loop->iteration}}</td>
                            <td>
                                <?php 
                                    if($item['parent_stat'] == 0 ){
                                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                    }

                                    if($item['parent_stat'] == 1 && !empty($item['childAkun']) && $item['parent_id'] != null){
                                        echo "&nbsp;&nbsp;&nbsp;";
                                    }
                                ?>
                                {{ $item['kode_akun'] }}</td>
                            <td>{{ $item['nama_akun'] }}</td>
                            <td>{{ $item['tipe_akun'] }}</td>
                            <td>Rp. {{ number_format($item['saldo'] + $item['money_function']) }}</td>
                            {{-- @include('chichi_theme.layout.created_updated_deleted_td', compact($item)) --}}
                            <td>
                                @if( $permission['ubah'] || $permission['hapus'] )
                                    {!! Form::open(['route' => [$module_url->destroy, $item['id']], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item jurnal" href="{{ route($module_url->show, $item['id']) }}">
                                                    <i class="fa fa-table"></i>&nbsp;Detail
                                                </a>
                                                @if ($item['parent_id'] !== null && $item['tipe_akun'] == 'Kas/Bank')
                                                    @can($permission['laporan'])
                                                        <a class="dropdown-item btn-info" href="{{ route('akuntansi.akun-buku-bank').'?akun='.$item['id'] }}"><i class="fa fa-books"></i> History Bank</a>
                                                    @endcan
                                                @endif
                                                @can($permission['ubah'])
                                                    <a class="dropdown-item edit" href="{{ route($module_url->edit, $item['id']) }}"><i class="fa fa-pencil"></i> Edit</a>
                                                @endcan
                                                @can($permission['hapus'])
                                                    <a class="dropdown-item deleteBtn" href="#">
                                                        <i class="fa fa-trash"></i> Delete
                                                    </a>
                                                @endcan
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                        <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{--  {!! $items->links('vendor.pagination.bootstrap-4'); !!}  --}}
                    </div>
                </div>   
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection

@section('custom_js')
    <script type="text/javascript">
        $('.mask_duit').autoNumeric('init',{aSign: "Rp "});
    </script>
@endsection
            
