@extends('chichi_theme.layout.app')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 10px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .disabled-select {
        background-color:#d5d5d5;
        opacity:0.5;
        border-radius:3px;
        cursor:not-allowed;
        position:absolute;
        top:0;
        bottom:0;
        right:0;
        left:0;
    }
</style>

@endsection

@section('content')
    <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ url('akuntansi/barang') }}">{{ $title_document }}</a></li>
                    </ol>


                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                    {!! Form::open(['route' => 'akuntansi.post.pemasok-upload', 'files' => true ,'method' => 'POST', 'class' => 'form form-horizontal form-label-left', 'id' => 'formUpload']) !!}
                    <div class="form-group row">
                        <label class="col-2 col-form-label">File xlsx</label>
                        <div class="col-10">
                            <input name="import_file" required type="file" data-parsley-fileextension='csv' class="filestyle form-control" data-iconname="fa fa-cloud-upload">
                        </div>
                    </div>
                    <div class="submit">
                        @if(!is_null($data['fileFormatUrl']))
                        <a href="{!! $data['fileFormatUrl'] !!}" class="btn btn-default">Download Contah format</a>
                        @endif
                        {!! Form::button('<i class="fa fa-check"></i> Upload',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
                    </div>
                    {!! Form::close() !!}
                    <small>Pastikan file yang di upload berekstensi .xlsx</small>
            </div>

            </div>

        </div>
    </div>
@endsection

@section('custom_js')
        {{-- <script type="text/javascript" src="{{ asset('assets/plugins/parsleyjs/parsley.min.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                var formId = $("#formUpload");
                window.ParsleyValidator
                    .addValidator('fileextension', function (value, requirement) {
                        var fileExtension = value.split('.').pop();
                        return fileExtension === requirement;
                    }, 32)
                    .addMessage('en', 'fileextension', 'tipe harus csv');
                formId.parsley();
                // formId.on('submit', function(e) {
                //     var f = $(this);
                //     f.parsley().validate();
                //     e.preventDefault();
                //     return true;
                // });
            });
        </script> --}}
@endsection