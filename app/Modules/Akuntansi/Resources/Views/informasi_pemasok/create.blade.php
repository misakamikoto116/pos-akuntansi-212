@extends( auth()->user()->hasPermissionTo('buat_data_pemasok') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 10px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ url('akuntansi/informasi-pemasok') }}">Daftar Pemasok</a></li>
                      <li><a href="#">Tambah Pemasok</a></li>
                    </ol>

                </div>
            </div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left', 'files' => true]) !!}

                @include($form)
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function () {
            $('.duplicate-saldo-sections').on('click', '.add', function () {
                var row = $('#table_saldo_section').html();
                var clone = $(row).clone();

                // clear the values
                clone.find('input[type=text]').val('');

                clone.find('.no_faktur_pelanggan').attr('name','no_faktur_saldo[]');
                clone.find('.tanggal_pelanggan').attr('name','tanggal_saldo[]');
                clone.find('.saldo_awal').attr('name','saldo_awal[]');
                clone.find('.syarat_pembayaran').attr('name','syarat_pembayaran[]');

                $(clone).appendTo($(this).closest('.saldo-sections').find('.purchaseItemsSaldo'));

                $('.date-saldo').datepicker({format: 'dd MM yyyy', autoclose: true});
                $('.date-saldo').datepicker('setDate', new Date());
                $('.mask').autoNumeric('init', {aPad: false, aForm:false});

            });
            $('.duplicate-saldo-sections').on('click', '.removeRow', function () {
                if ($(this).closest('.purchaseItemsSaldo').find('tr').length > 0) {
                    $(this).closest('tr').remove();
                }
            });
        });
        $('#syarat_pembayaran_id').on('change',function(){
            var str = $(this).find(":selected").text();
            var spl = str.split("/");
            $('#diskon_termin').html(spl[0]);
            $('#hari_diskon_termin').html(spl[1]);
            $('#jatuh_tempo_termin').html(spl[2].replace('n', ''));
        });
        $('.duplicate-saldo-sections').on('click', '.simpan-saldo', function () {
            var totalSaldo = 0;
            var itemSaldo = $(this).closest('.modal').find('.purchaseItemsSaldo tr');

                $(itemSaldo).each(function(index, el) {
                  var saldoPelanggan = $(el).find('.saldo_awal').autoNumeric('get');
                  totalSaldo += parseFloat(saldoPelanggan);
                });
                  var hasil_saldo = $('.saldo-awal-input');
                  console.log(hasil_saldo);
                  $(hasil_saldo).autoNumeric('init', {aPad: false, aForm:false});
                  $(hasil_saldo).autoNumeric('set', totalSaldo);

                  $(this).closest('.modal').modal('hide');
          });

            $('.image_foto_pemasok').change(function(){
                var input = this;
                var url	  = $(this).val();
                var ext		= url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
                                var reader = new FileReader();

                                reader.onload = function (e) {
                                        $('#img_pemasok').attr('src', e.target.result);
                                }
                        reader.readAsDataURL(input.files[0]);
                }else{
                        $('#img_pemasok').attr('src', '');
                }
            });

            $('.image_foto_nik_pemasok').change(function(){
                var input = this;
                var url	  = $(this).val();
                var ext		= url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
                                var reader = new FileReader();

                                reader.onload = function (e) {
                                        $('#img_nik_pemasok').attr('src', e.target.result);
                                }
                        reader.readAsDataURL(input.files[0]);
                }else{
                        $('#img_nik_pemasok').attr('src', '');
                }
            });

            $('button[type=reset]').click(function(e){
                $('#img_pemasok').attr('src', '');
                $('#img_nik_pemasok').attr('src', '');
            });
        </script>

@endsection
