<fieldset>
	<legend class="border">
		Informasi Diskon
	</legend>
	<div class="form-group row">
		{!! Form::label('nama','Nama',['class' => 'col-2 col-form-label']) !!}
		<div class="col-9">
			{!! Form::text('nama',null,['class' => 'form-control','id' => 'nama'])!!}
		</div>
	</div>	
	<div class="form-group row">
		{!! Form::label('jika_membayar_antara','Jika Membayar Antara',['class' => 'col-2 col-form-label']) !!}
		<div class="col-9">
			{!! Form::number('jika_membayar_antara',null,['class' => 'form-control','id' => 'jika_membayar_antara'])!!}
		</div>
		{!! Form::label('keterangan_jika_membayar_antara',' Hari',['class' => 'col-1 col-form-label']) !!}
	</div>
	
	<div class="form-group row">
		{!! Form::label('akan_dapat_diskon','Akan Dapat Diskon',['class' => 'col-2 col-form-label']) !!}
		<div class="col-9">
			{!! Form::number('akan_dapat_diskon',null,['class' => 'form-control','id' => 'akan_dapat_diskon'])!!}
		</div>
		{!! Form::label('keterangan_akan_dapat_diskon',' %',['class' => 'col-1 col-form-label']) !!}
	</div>
	<div class="form-group row">
		{!! Form::label('jatuh_tempo','Jatuh Tempo',['class' => 'col-2 col-form-label']) !!}
		<div class="col-9">
			{!! Form::number('jatuh_tempo',null,['class' => 'form-control','id' => 'jatuh_tempo'])!!}
		</div>
		{!! Form::label('keterangan_jatuh_tempo',' Hari',['class' => 'col-1 col-form-label']) !!}
	</div>
	<div class="form-group row">
		{!! Form::label('keterangan','Keterangan',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
			{!! Form::textarea('keterangan',null,['class' => 'form-control','id' => 'keterangan'])!!}
		</div>
	</div>
</fieldset>