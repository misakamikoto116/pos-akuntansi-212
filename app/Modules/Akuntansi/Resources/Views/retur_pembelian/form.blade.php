<div class="form-group row">
  <div class="col-1 col-form-label">Vendor</div>
  <div class="col-2">
  {!! Form::text('no_pemasok',null,['class' => 'form-control','id' => 'no_pemasok','readonly'])!!}
  </div>
  <div class="col-6">
  {!! Form::select('pemasok_id',$pemasok,null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'pemasok_id'])!!}
  </div>
</div>

<div class="row">
  <div class="offset-sm-3 col-sm-3">
    <div class="form-group">
      {!! Form::textarea('alamat_asal',null,['class' => 'form-control','id' => 'alamat_asal','readonly'])!!}
      </div>
  </div>
  </div>
  
  {!! Form::hidden('total', null, ['id' => 'grandtotal']) !!}
  
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group row">
      <div class="col-6 col-form-label text-right">Receipt / Inv. No.</div>
      <div class="col-6">{!! Form::select('faktur_pembelian_id',[],null,['class' => 'form-control list-purchase-invoice select2','id' => 'invoice_no'])!!}</div>
      </div>
    </div>
    <div class="col-3 form-inline">
  	<div class="form-check form-check-inline">
	  @if (isset($item))
    {!! Form::checkbox('taxable', 1, $item->taxable == 1 ? true : false,['class' => 'form-check-input taxable','id' => 'group1','onchange' => 'calculate()']) !!}
    @else
    {!! Form::checkbox('taxable', 1, true,['class' => 'form-check-input taxable','id' => 'group1','onchange' => 'calculate()']) !!}
    @endif
	  <label class="form-check-label" for="group1">Vendor is Taxable</label>
	</div>
	<div class="form-check form-check-inline">
	  @if (isset($item))
    {!! Form::checkbox('in_tax', 1, $item->in_tax == 1 ? true : false,['class' => 'form-check-input group2 in_tax', 'id' => 'inlineCheckbox2', 'onchange' => 'calculate()']) !!}
    @else
    {!! Form::checkbox('in_tax', 1, null,['class' => 'form-check-input group2 in_tax', 'id' => 'inlineCheckbox2', 'onchange' => 'calculate()']) !!}
    @endif
	  <label class="form-check-label" for="inlineCheckbox2">Inclusive Tax</label>
	</div>
  </div>
    <div class="col-sm-3">
      <div class="row">
        <div class="col-6">
          <div class="form-group">
          <div class="tag">Return No.</div>
          {!! Form::text('return_no',null,['class' => 'form-control','id' => '','required'])!!}
          </div>
        </div>
        <div class="col-6">
          <div class="form-group">
          <div class="tag">Date</div>
          @if (isset($item) || old('tanggal'))
          {!! Form::text('tanggal',null,['class' => 'form-control tanggal_retur','id' => '',''])!!}
          @else
          {!! Form::text('tanggal',null,['class' => 'form-control tanggal','id' => '',''])!!}
          @endif
          </div>
        </div>
        <div class="col-6 tax" style="display: none;">
          <div class="form-group">
          <div class="tag">No. Fiscal</div>
          {!! Form::text('no_fiscal',null,['class' => 'form-control','id' => 'no_fiscal_id','required'])!!}
          </div>
        </div>
        <div class="col-6 tax" style="display: none;">
          <div class="form-group">
          <div class="tag">Tgl Fiscal</div>
          @if (isset($item) || old('tgl_fiscal'))
          {!! Form::text('tgl_fiscal',null,['class' => 'form-control tanggal_retur','id' => 'tgl_fiscal_id'])!!}
          @else
          {!! Form::text('tgl_fiscal',null,['class' => 'form-control tanggal','id' => 'tgl_fiscal_id'])!!}
          @endif
          </div>
        </div>
        <div class="col-12">
          <div class="form-group row">
            <div class="col-6 col-form-label">Nilai Tukar</div>
            <div class="col-6">{!! Form::text('nilai_tukar',null,['class' => 'form-control date','id' => '','readonly'])!!}</div>
          </div>
          <div class="form-group row">
            <div class="col-6 col-form-label">Nilai Tukar Pajak</div>
            <div class="col-6">{!! Form::text('nilai_tukar_pajak',null,['class' => 'form-control date','id' => '','readonly'])!!}</div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="form-group">
    <label>Description :</label>
    {!! Form::textarea('keterangan',null,['class' => 'form-control','id' => ''])!!}
  </div>


<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>


<div class="faktur-sections" data-id="1">
  <div class="duplicate-faktur-sections">
  <div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
      <div style="width: 1800px;">
          <table width="100%" class="table duplicate-sections">
            <thead class="thead-light" style="border-collapse: collapse;">
              <tr>
                <th width="12%">Item</th>
                <th width="12%">Item Description</th>
                <th>Qty</th>
                <th>Item Unit</th>
                <th>Unit Price</th>
                <th>Tax</th>
                <th>Amount</th>
                <th width="10%">Dept</th>
                <th width="10%">Proyek</th>
                <th width="10%">Gudang</th>
                <th>SN</th>
                <th></th>
              </tr>
            </thead>
            <tbody class="purchaseFaktur">
              @if(old('produk_id') !== null)  
                @foreach(old('produk_id') as $row => $produk_id)
                    <tr>
                      <td>
                        {!! Form::hidden('barang_faktur_pembelian_id['.($row ?? 0).']', old('barang_faktur_pembelian_id')[$row] ?? null, ['class' => 'barang_faktur_pembelian_id','id' => 'barang_faktur_pembelian_id'.$row]) !!}
                        {!! Form::hidden('produk_id_temp['.($row ?? 0).']', $produk_id ?? null,['id' => 'produk_id_temp'.$row]) !!}
                        {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id ?? null,['class' => 'form-control produk_id','id' => 'produk_id'.$row,'required', 'readonly'])!!} 
                        {!! Form::hidden('harga_modal['.($row ?? 0).']', old('harga_modal')[$row] ?? null, ['class' => 'harga_modal','id' => 'harga_modal'.$row]) !!}
                        {!! Form::hidden('harga_terakhir['.($row ?? 0).']', old('harga_terakhir')[$row] ?? null, ['class' => 'harga_terakhir','id' => 'harga_terakhir'.$row]) !!}
                      </td>
                      <td>{!! Form::text('keterangan_produk['.($row ?? 0).']',old('keterangan_produk')[$row] ?? null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.$row,'required', 'readonly'])!!}</td>
                      <td>{!! Form::text('qty_produk['.($row ?? 0).']',old('qty_produk')[$row] ?? 0,['class' => 'form-control qty_produk','id' => 'qty_produk'.$row,'required','onchange' => 'sumQtyAndUnit('. $row .')'])!!}</td>
                      <td>{!! Form::text('satuan_produk['.($row ?? 0).']',old('satuan_produk')[$row] ?? null,['class' => 'form-control satuan_produk','id' => 'satuan_produk'.$row,'required'])!!}</td>
                      <td>
                        {!! Form::text('unit_harga_produk['.($row ?? 0).']',old('unit_harga_produk')[$row] ?? 0,['class' => 'form-control unit_harga_produk mask','id' => 'unit_harga_produk'.$row,'required','onchange' => 'sumQtyAndUnit('. $row .')'])!!}
                      </td>
                      <td width="10%">
                        {!! Form::select('tax_produk['.($row ?? 0).'][]',$kode_pajak,old('tax_produk')[$row] ?? null,['class' => 'form-control select2 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => 'tax_produk'.$row,'onchange' => 'calculate()'])!!}
                      </td>
                      <td>
                        {!! Form::text('amount_produk['.($row ?? 0).']',old('amount_produk')[$row] ?? 0,['class' => 'form-control amount_produk','id' => 'amount_produk'.$row,'readonly'])!!}
                      </td>
                      <td>{!! Form::select('dept_id['.($row ?? 0).']',[],old('dept_id')[$row] ?? null,['class' => 'form-control select2 dept_id','id' => 'dept_id'.$row,''])!!}</td>
                      <td>{!! Form::select('proyek_id['.($row ?? 0).']',[],old('proyek_id')[$row] ?? null,['class' => 'form-control select2 proyek_id','id' => 'proyek_id'.$row,''])!!}</td>
                      <td>{!! Form::select('gudang_id['.($row ?? 0).']',$gudang,old('gudang_id')[$row] ?? null,['class' => 'form-control select2 gudang_id','id' => 'gudang_id'.$row,''])!!}</td>
                      <td>{!! Form::text('sn['.($row ?? 0).']',old('sn')[$row] ?? null,['class' => 'form-control sn','id' => 'sn'.$row,'readonly'])!!}</td>
                      <td><button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
                    </tr>
                @endforeach      
            @endif
            </tbody>
          </table>
      </div>
    </div>
  
  </div><!-- END PILLS -->
  </div>
  </div>
<hr>
<div class="row">
<div class="offset-sm-8 col-sm-4">
    <div class="form-group row">
      <div class="offset-1 col-4 col-form-label text-right"><strong>Sub Total</strong></div>
      <div class="col-7 text-right col-form-label" id="subtotal"><strong>0</strong></div>
    </div>
    <div class="form-group row">
			<div class="offset-1 col-4 col-form-label text-right"></div>
			<div class="col-7 text-right col-form-label" id="taxNaration"></div>
      {!! Form::hidden('tax_cetak0', null,['id' => 'tax_cetak0'])!!}
      {!! Form::hidden('tax_cetak1', null,['id' => 'tax_cetak1'])!!}
		</div>
    <div class="form-group row">
      <div class="offset-1 col-4 col-form-label text-right"><strong>Total Retur</strong></div>
      <div class="col-7 text-right col-form-label grandtotal"><strong>0</strong></div>
    </div>
  </div>
</div>

<script type="text/template" id="table_faktur_section" data-id=""/>
  <tr>
    <td>
        {!! Form::hidden('barang_faktur_pembelian_id[]', null, ['class' => 'barang_faktur_pembelian_id']) !!}
        {!! Form::text('no_produk[]',null,['class' => 'form-control no_produk','id' => '','required', 'readonly'])!!}
        {!! Form::hidden('produk_id[]', null,['class' => 'form-control produk_id','required'])!!}
        {!! Form::hidden('harga_modal[]', null, ['class' => 'harga_modal']) !!}
        {!! Form::hidden('harga_terakhir[]', null, ['class' => 'harga_terakhir']) !!}
    </td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '','required','readonly'])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('unit_harga_produk[]',null,['class' => 'form-control unit_harga_produk','id' => '','required'])!!}</td>
    <td width="10%">
			{!! Form::select('tax_produk[][]',$kode_pajak,null,['class' => 'form-control select2 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => '',''])!!}
    </td>
    <td>{!! Form::text('amount_produk[]',null,['class' => 'form-control amount_produk','id' => '','required'])!!}</td>
    <td>{!! Form::select('dept_id[]',[],null,['class' => 'form-control select2 dept_id','id' => '',''])!!}</td>
    <td>{!! Form::select('proyek_id[]',[],null,['class' => 'form-control select2 proyek_id','id' => '',''])!!}</td>
    <td>{!! Form::select('gudang_id[]',[],null,['class' => 'form-control select2 gudang_id','id' => 'gudang_id',''])!!}</td>
    <td>{!! Form::text('sn[]',null,['class' => 'form-control col-form-label sn','id' => '','readonly' => ''])!!}</td>
    <td><button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>

@include('akuntansi::pesanan_modal/modal_barang')