@extends( auth()->user()->hasPermissionTo('buat_retur_pembelian') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788; 
        color: white; 
        text-align: center; 
        position: relative; 
        top: 4px; 
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Pembelian</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Retur Pembelian</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Purchase Return / Retur Pembelian</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left', 'id' => 'form-penjualan-penawaran']) !!}
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary', 'id' => 'btn-cetak']) !!}
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" id="btn-submit" disabled data-toggle="dropdown">
                    Simpan <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            {!! Form::submit('Simpan & Baru', ['class' => 'btn btn-info btn-block','value' => '0', 'name' => 'lanjutkan']) !!}
                        </li>
                        <li>
                            {!! Form::button('Simpan & Tutup',['type' => 'submit','class' => 'btn btn-warning btn-block']) !!}
                        </li>
                    </ul>
                </div>
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger']) !!}
                
                </div>
                </div>
                {!! Form::close() !!}
                
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

        <script type="text/javascript">
            var pemasok_id_old = '{{ old('pemasok_id') }}';
            $(function() {
                var q = 0;
                enable_cb();
                $("#group1").click(enable_cb);
            });

            @if (old('produk_id'))
                $(document).ready(function () {
                    q = $(".produk_id").length;
                    getListInvoice();
                    sumQtyAndUnit(q);
                });
            @endif

            $("#pemasok_id").change(function () {
                setPemasok();                
            });


            function getListInvoice() {
                var pemasok_id   = $('#pemasok_id').val();
                var faktur_pembelian_old = '{{ old('faktur_pembelian_id') }}';
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-alamat-pemasok') }}",
                    data: {id : pemasok_id,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                    q = 0;
                    $('.list-purchase-invoice').append( 
                        $("<option></option>") 
                            .text('-- Pilih Sales Invoice --')
                    );
                    $.each(response.faktur, function (index, item){
                        if (item.id == faktur_pembelian_old) {
                            $('.list-purchase-invoice').append( 
                                $("<option></option>") 
                                    .text(item.no_faktur)
                                    .val(item.id)
                                    .attr('selected','selected')
                            );    
                        }else {
                            $('.list-purchase-invoice').append( 
                                $("<option></option>") 
                                    .text(item.no_faktur)
                                    .val(item.id)
                            );
                        }
                    });
                    $('.select2').select2();
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
            };


            function setPemasok() {
                var pemasok_id   = $('#pemasok_id').val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-alamat-pemasok') }}",
                    data: {id : pemasok_id,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                    q = 0;
                    $('.list-purchase-invoice').find('option').remove().end();   
                    $('.purchaseFaktur').find('tr').remove().end();   
                    $('#no_pemasok').val(response.no_pemasok);    
                    $('#alamat_asal').val(response.alamat);
                    $("#tax_cetak0").val(null);
                    $("#tax_cetak1").val(null);
                    $('.list-purchase-invoice').append( 
                        $("<option></option>") 
                            .text('-- Pilih Sales Invoice --')
                    );
                    $.each(response.faktur, function (index, item){
                        $('.list-purchase-invoice').append( 
                            $("<option></option>") 
                                .text(item.no_faktur)
                                .val(item.id)
                        );
                    });
                    $('.select2').select2();
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
            };

            function filterSelectedId(id){
                var target = 'faktur-pembelian';
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-detail-barang') }}",
                        data: {id : id, type : target,
                        _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                            q = 0;
                            $('.purchaseFaktur').find('tr').remove().end();
                            $.each(response.barang, function(index, item){
                                duplicateForm();
                                let countTemp = q - 1;
                                callSelect2AjaxProdukID(countTemp, item.produk_id);
                                $('#no_produk'+countTemp).val(item.produk.no_barang); 
                                $('#produk_id'+countTemp).val(item.produk_id); 
                                // $('#produk_id'+countTemp).append( // Append an object to the inside of the select box
                                //     $("<option></option>") // Yes you can do this.
                                //         .text(item.produk.no_barang)
                                //         .val(item.produk_id)
                                // );
                                insertItemToForm(countTemp, item.id, target); //-1 karena diatas (diplicateForm) sdh di increment
                            });
                            // for(a = 0; a < q; a++){
                            //     callSelect2AjaxProduk(a);
                            // }
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
            }

            function insertItemToForm(vale, item, target) {
                var url = "{{ route('akuntansi.get-produk-by-val') }}";
                var pemasok_id = $("#pemasok_id").val();                                    
                   $.ajax({
                       type: "GET",
                       url: url,
                       data: {id : item, type : target, pemasok_id : pemasok_id,
                           _token: '{{ csrf_token() }}'},
                       dataType: "json",
                       success: function(response){
                       $('#tax_produk'+vale).find('option').remove().end();                        
                       $('#gudang_id'+vale).find('option').remove().end(); 
                       $('#barang_faktur_pembelian_id'+vale).val(response.id);
                       $('#keterangan_produk'+vale).val(response.keterangan);
                       $('#qty_produk'+vale).val(response.jumlah);
                       $('#satuan_produk'+vale).val(response.satuan);
                       changeToMasking(response, vale);
                       $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
                       $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0); 
                       $('#sn'+vale).val();
                       $("#harga_modal"+vale).val(response.harga_modal.harga_modal);
                       $("#harga_terakhir"+vale).val(response.harga_modal.harga_terakhir);
                       if(response.ditutup == 1){
                           $('#ditutup'+vale).prop('checked', true);
                       }
                       $.each(response.tax, function (index, item){
                        if(response.kode_pajak_id == item.id || response.kode_pajak_2_id == item.id){
                           $('#tax_produk'+vale).append( // Append an object to the inside of the select box
                               $("<option></option>") // Yes you can do this.
                                    .text(item.nilai+ " / " +item.nama)
                                    .val(item.id)
                                    .attr('selected','selected')
                           );
                        }else{
                            $('#tax_produk'+vale).append( // Append an object to the inside of the select box
                               $("<option></option>") // Yes you can do this.
                                   .text(item.nilai+ " / " +item.nama)
                                   .val(item.id)
                           );
                        }
                       });
                       $(response.multiGudang).each(function (val, text) {
                           $(text.gudang).each(function (index, item) {
                               $('#gudang_id'+vale).append(`<option value="${item.id}">${item.nama}</option>`);
                           });
                       });
                       sumQtyAndUnit(vale);
                       if (response.taxable.taxable == 1) {
                            $("#group1").prop('checked', true);
                            $("#inlineCheckbox2").removeAttr('disabled');
                            $(".tax").show();
                            $("#no_fiscal_id").removeAttr("disabled");
                            $("#tgl_fiscal_id").removeAttr("disabled");
                        }else if(response.taxable.taxable == 0 || response.taxable.taxable === null) {
                            $("#group1").prop('checked', false);
                            $("#inlineCheckbox2").attr('disabled','disabled');
                            $("#no_fiscal_id").attr("disabled", true);
                            $("#tgl_fiscal_id").attr("disabled", true);
                            $(".tax").hide();
                        }
                        if (response.taxable.in_tax == 1) {
                            $("#inlineCheckbox2").prop('checked', true);
                        }else if(response.taxable.in_tax == 0 || response.taxable.in_tax  ===null){
                            $("#inlineCheckbox2").prop('checked', false);
                        }
                       $('.select3').select2();
                       }, failure: function(errMsg) {
                           alert(errMsg);
                       }
                   });
           }

           function changeToMasking(response, vale) {
                if(pemasok_id_old !== ""){
                    changeMaskingToNumber($('#unit_harga_produk'+vale).val(response.unitPrice));
                    changeMaskingToNumber($('#amount_produk'+vale).val(0));
                }
            }
        </script>
        <script src="{{ asset('js/retur_pembelian.js') }}"></script>
@endsection
