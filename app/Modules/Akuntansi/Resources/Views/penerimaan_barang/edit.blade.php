@extends( auth()->user()->hasPermissionTo('ubah_penerimaan_pembelian') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788; 
        color: white; 
        text-align: center; 
        position: relative; 
        top: 4px; 
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
    .warnatr{
        background: lightsteelblue;
        color: black;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Pembelian</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Penerimaan Barang</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Received Item / Penerimaan Barang</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left','id' => 'form-penjualan-penawaran']) !!}
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary', 'id' => 'btn-cetak']) !!}
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" id="btn-submit" disabled data-toggle="dropdown">
                    Simpan <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        @if ( auth()->user()->hasPermissionTo('buat_faktur_pembelian') )
                            <li>
                                {!! Form::submit('Buat Faktur', ['class' => 'btn btn-success btn-block','value' => '1', 'name' => 'lanjutkan']) !!}
                            </li>
                        @endif
                        <li>
                            {!! Form::button('Simpan & Tutup',['type' => 'submit','class' => 'btn btn-warning btn-block']) !!}
                        </li>
                    </ul>
                </div>
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger']) !!}
                
                </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>
        <script src="{{ asset('js/penerimaan-barang.js') }}"></script>

        <script type="text/javascript">
            var q = 0;

            $(function() {
                enable_cb();
                $("#group1").click(enable_cb);
                @if(!old('produk_id'))
                    setPemasok();
                    // setDataPemasok();
                @else
                    q = $(".produk_id").length;
                    setDataPemasokWhenOld({{ old('pemasok_id') }});
                    removeDelButton(); 
                @endif
            });

            $('.duplicate-penawaran-sections').on('click', '.add-penawaran', function () {
                @if (old('produk_id'))
                    duplicateFormWhenOld();
                @else
                    duplicateForm();
                @endif
            });

            function btnCetak() {
                if ($("#pemasok_id").val() == "" || $('.produk_id').val() == "" || $('.produk_id').val() == undefined) {
                    swal({
                        icon: "warning",
                        text: "Pemasok/Barang Masih Kosong"
                    });
                }else {
                    var form = $("#form-pesanan-pembelian");
                    form.attr('target','_blank');
                    form.attr('action','{{ route("akuntansi.edit-cetak-penerimaan-pembelian") }}');
                    form.submit();
                }
            }

            // Fungsi button simpan tutup dan simpan lanjut di klik
            $("#btn-submit").on("click", function () {
                var form = $("#form-pesanan-pembelian");
                form.removeAttr('target');
                form.attr('action','{{ route($module_url->update,$item->id) }}');
                $("#btn-submit").on("submit", function () {
                    form.submit();
                });
            });

            function setPemasok(){
                var pemasok_id   = $('#pemasok_id').val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-alamat-pemasok') }}",
                    data: {id : pemasok_id,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                    q = 0;
                    $('#alamat_pengiriman').val(response.alamat);
                    $('#alamat_asal').val(response.alamat);
                    $('#no_pemasok').val(response.no_pemasok);
                    $.each(response.pesanan, function (index, item){
                        if (item.sum_updated_qty !== 0) {
                            $('#table-list-pesanan').append('<tr><th><input type="checkbox" value="'+item.id+'" name="pesanan[]" id="pesanan[]"/></th><td>'+item.po_number+'</td><td>'+item.po_date+'</td></tr>');
                        }
                    });
                    setDetailBarang();
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
        }

        function setDetailBarang(){
                var pemasok_id = $("#pemasok_id").val();
                var id = $("#penerimaan_pembelian_id").val();
                var type = "penerimaan-pembelian";
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-detail-barang') }}",
                    data: {id : id, type : type, pemasok_id : pemasok_id,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                    $.each(response.barang, function(index, key){
                        duplicateForm();
                        let queue = q - 1;
                        callSelect2AjaxProdukID(queue, key.produk.id);
                        $('#produk_id'+queue).val(key.produk.id);                        
                        $('#barang_pesanan_pembelian_id'+queue).val(key.barang_pesanan_pembelian_id);                                                
                        $('#keterangan_produk'+queue).val(key.produk.keterangan);
                        $('#qty_produk'+queue).val(key.jumlah);
                        $('#unit_price'+queue).val(key.barang_pesanan_pembelian.harga);
                        $('#satuan_produk'+queue).val(key.satuan);
                        $('#harga_modal'+queue).val(key.harga_modal);
                        $('#gudang_id'+queue).val(key.gudang_id);                    
                        $('.select2').select2();
                        cekQtyandPrice(queue);
                    });
                    for (let a = 0; a < q; a++) {
                        callSelect2AjaxProduk(a, 'modal');
                    }
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
            }

            // Fungsi button cetak di klik
            $("#btn-cetak").on("click", function(e) {
                e.preventDefault();
                btnCetak()
            });

            function btnCetak() {
                if ($("#pemasok_id").val() == "" || $('.produk_id').val() == "" || $('.produk_id').val() == undefined) {
                    swal({
                        icon: "warning",
                        text: "Pemasok/Barang Masih Kosong"
                    });
                }else {
                    var form = $("#form-penjualan-penawaran");
                    form.attr('target','_blank');
                    form.attr('action','{{ route('akuntansi.edit-cetak-penerimaan-pembelian') }}');
                    form.submit();
                }
            }

            // Fungsi button simpan tutup dan simpan lanjut di klik
            $("#btn-submit").on("click", function () {
                var form = $("#form-penjualan-penawaran");
                form.removeAttr('target');
                form.attr('action','{{ route($module_url->update,$item->id) }}');
                $("#btn-submit").on("submit", function () {
                    form.submit();
                });
            });

        </script>
        <script src="{{ asset('js/add-btn-tag-activity.js') }}"></script>
@endsection
