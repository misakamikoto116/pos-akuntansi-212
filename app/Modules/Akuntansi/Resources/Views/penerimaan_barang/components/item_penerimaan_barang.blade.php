<tr>
     <td>
        {!! Form::hidden('total_produk['.($row ?? 0).']', $total_produk ?? null, ['class' => 'total_produk','id' => 'total_produk'.$row]) !!}
        {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id ?? null,['class' => 'form-control produk_id','required','id' => 'produk_id'.$row])!!}
        {!! Form::text('no_produk['.($row ?? 0).']', $no_produk ?? null,['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required','id' => 'no_produk'.$row,'onclick' => 'awas(' . $row . ')', 'readonly'])!!}
        {!! Form::hidden('produk_id_temp['.($row ?? 0).']', $produk_id ?? null, ['id' => 'produk_id_temp'.$row]) !!}
        
        {!! Form::hidden('barang_pesanan_pembelian_id['.($row ?? 0).']', $barang_pesanan_pembelian_id ?? null, ['class' => 'barang_pesanan_pembelian_id','id' => 'barang_pesanan_pembelian_id'.$row]) !!}
        {!! Form::hidden('harga_modal['.($row ?? 0).']', $harga_modal ?? null, ['class' => 'harga_modal','id' => 'harga_modal'.$row]) !!}
        {!! Form::hidden('unit_price['.($row ?? 0).']', $unit_price ?? null, ['class' => 'unit_price','id' => 'unit_price'.$row]) !!}
        {!! Form::hidden('harga_terakhir['.($row ?? 0).']', $harga_terakhir ?? null, ['class' => 'harga_terakhir','id' => 'harga_terakhir'.$row]) !!}
        {!! Form::hidden('diskon_produk['.($row ?? 0).']', $diskon_produk ?? null, ['class' => 'diskon_produk','id' => 'diskon_produk'.$row]) !!}
    </td>
    <td>{!! Form::text('keterangan_produk['.($row ?? 0).']', $keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.$row,'required'])!!}</td>
    <td>{!! Form::text('qty_produk['.($row ?? 0).']', $qty_produk ?? null,['class' => 'form-control qty_produk','id' => 'qty_produk'.$row,'required'])!!}</td>
    <td>{!! Form::text('satuan_produk['.($row ?? 0).']', $satuan_produk ?? null,['class' => 'form-control satuan_produk','id' => 'satuan_produk'.$row,'required'])!!}</td>
    <td>{!! Form::select('dept_id['.($row ?? 0).']',[], $dept_id ?? null,['class' => 'form-control select2 dept_id','id' => 'dept_id'.$row,''])!!}</td>
    <td>{!! Form::select('proyek_id['.($row ?? 0).']',[], $proyek_id ?? null,['class' => 'form-control select2 proyek_id','id' => 'proyek_id'.$row,''])!!}</td>
    <td>{!! Form::select('gudang_id['.($row ?? 0).']',isset($item) || old('gudang_id') !== null ? $gudang : [], $gudang_id ?? null,['class' => 'form-control select2 gudang_id','id' => 'gudang_id'.$row,''])!!}</td>
    <td>{!! Form::text('sn['.($row ?? 0).']', $sn ?? null,['class' => 'form-control sn','id' => 'sn'.$row,''])!!}</td>
    <td><button href="" class="remove btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>