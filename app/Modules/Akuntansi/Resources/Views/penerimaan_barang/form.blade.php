@if (isset($item))
  {!! Form::hidden('penerimaan_pembelian', $item->id,['id' => 'penerimaan_pembelian_id'])!!}
@endif
<div class="form-group row">
  <div class="col-1 col-form-label">Vendor</div>
  <div class="col-2">
  {!! Form::text('no_pemasok',null,['class' => 'form-control','id' => 'no_pemasok','readonly'])!!}
  </div>
  <div class="col-4">
  {!! Form::select('pemasok_id',$pemasok,null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'pemasok_id'])!!}
  </div>
  <div class="col-2">
    <button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-list"></i>&nbsp; Pilih Pesanan</button>

    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pesanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Transaksi</th>
              <th>Tanggal</th>
          </tr>
          </thead>
          <tbody id="table-list-pesanan">
          
        </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-default" onclick="filterSelectedRequest()" data-dismiss="modal">Simpan</a>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

  </div>
</div>

<hr>

<div class="row">
  <div class="offset-sm-3 col-sm-3">
    <div class="form-group">
      {!! Form::textarea('alamat_pengiriman',null,['class' => 'form-control','id' => 'alamat_asal','readonly'])!!}
      </div>
  </div>

  <div class="offset-sm-3 col-sm-3">
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">Receive No.</div>
          {!! Form::text('receipt_no',(empty($item->receipt_no) ? $idPrediction : $item->receipt_no),['class' => 'form-control','id' => ''])!!}
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">Receipt Date</div>
          @if (isset($item) || old('receive_date'))
          {!! Form::text('receive_date',null,['class' => 'form-control tanggal_penerimaan','id' => 'receive_date','required'])!!}
          @else
          {!! Form::text('receive_date',null,['class' => 'form-control tanggal','id' => 'receive_date','required'])!!}
          @endif
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">Form No.</div>
          {!! Form::text('form_no',(empty($item->form_no) ? $idPrediction : $item->form_no),['class' => 'form-control date','id' => ''])!!}
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">FOB</div>
          {!! Form::select('fob',['0' => 'Shiping Point', '1' => 'Destination' ],null,['placeholder' => '- Pilih -','class' => 'form-control select2','id' => 'fob_id'])!!}
          </div>
      </div>
      <div class="col-sm-6">
          <div class="form-group">
          <div class="tag">Ship Date</div>
            @if (isset($item) || old('ship_date'))
            {!! Form::text('ship_date',null,['class' => 'form-control tanggal_penerimaan','id' => 'ship_date','required'])!!}
            @else
            {!! Form::text('ship_date',null,['class' => 'form-control tanggal','id' => 'ship_date','required'])!!}
            @endif
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag tag-ships">Ship Via</div>
          {!! Form::select('ship_id',$pengiriman,null,['placeholder' => '- Pilih -','class' => 'form-control select2','id' => 'ship_id'])!!}
          </div>
      </div>
    </div>
  </div>
</div>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>
<div class="penawaran-sections" data-id="1">
<div class="duplicate-penawaran-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
    <div style="width: 1800px;">
      <table width="100%" class="table">
        <thead class="thead-light" style="border-collapse: collapse;">
          <tr>
            <th width="15%">Item</th>
            <th width="15%">Description</th>
            <th width="5%">Qty</th>
            <th>Satuan</th>
            <th width="15%">Dept</th>
            <th width="15%">Proyek</th>
            <th width="15%">Gudang</th>
            <th>SN</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="purchasePenawaran">

          @if (old('produk_id') !== null)
            @foreach (old('produk_id') as $row => $produk_id)
              @include('akuntansi::penerimaan_barang/components/item_penerimaan_barang',[
                  'row'                               => $row,
                  'total_produk'                      => old('total_produk')[$row],
                  'produk_id'                         => $produk_id,
                  'no_produk'                         => old('no_produk')[$row],
                  'barang_pesanan_pembelian_id'       => old('barang_pesanan_pembelian_id')[$row],
                  'harga_modal'                       => old('harga_modal')[$row],
                  'unit_price'                        => old('unit_price')[$row],
                  'keterangan_produk'                 => old('keterangan_produk')[$row],
                  'qty_produk'                        => old('qty_produk')[$row],
                  'satuan_produk'                     => old('satuan_produk')[$row],
                  'dept_id'                           => old('dept_id')[$row],
                  'proyek_id'                         => old('proyek_id')[$row],
                  'gudang_id'                         => old('gudang_id')[$row],
                  'unit_price'                        => old('unit_price')[$row],
                  'harga_terakhir'                    => old('harga_terakhir')[$row],
                  'diskon_produk'                     => old('diskon_produk')[$row],
                  'sn'                                => old('sn')[$row],
                ])
            @endforeach
          @endif

        </tbody>
    </table>
    </div>
    <a class="btn btn-info add-penawaran" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
</div><!-- END PILLS -->
</div>
</div>

<script type="text/template" id="table_penawaran_section" data-id=""/>
<tr>
   <td>
      {!! Form::hidden('total_produk', null, ['class' => 'total_produk']) !!}
      {!! Form::hidden('produk_id[]', null,['class' => 'form-control produk_id','required'])!!}
      {!! Form::text('no_produk[]', null,['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required', 'readonly'])!!}
      {!! Form::hidden('produk_id_temp[]', null, ['class' => 'produk_id_temp']) !!}
      {!! Form::hidden('barang_pesanan_pembelian_id', null, ['class' => 'barang_pesanan_pembelian_id']) !!}
      {!! Form::hidden('harga_modal', null, ['class' => 'harga_modal']) !!}
      {!! Form::hidden('harga_terakhir', null, ['class' => 'harga_terakhir']) !!}
      {!! Form::hidden('unit_price', null, ['class' => 'unit_price']) !!}
      {!! Form::hidden('diskon_produk',null,['class'=> 'diskon_produk']) !!}
  </td>
  <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}</td>
  <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '','required'])!!}</td>
  <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '','required'])!!}</td>
  <td>{!! Form::select('dept_id[]',[],null,['class' => 'form-control select2 dept_id','id' => '',''])!!}</td>
  <td>{!! Form::select('proyek_id[]',[],null,['class' => 'form-control select2 proyek_id','id' => '',''])!!}</td>
  <td>{!! Form::select('gudang_id[]',isset($item) ? $gudang : [],null,['class' => 'form-control select2 gudang_id','id' => '',''])!!}</td>
  <td>{!! Form::text('sn',null,['class' => 'form-control sn','id' => '',''])!!}</td>
  <td><button href="" class="remove btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>
</script>

<hr>
<div class="row">
  <div class="col-sm-4">
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Description</label>
      {!! Form::textarea('keterangan',null,['class' => 'form-control','id' => 'catatan'])!!}
    </div>
  </div>
</div>

@include('akuntansi::pesanan_modal/modal_barang')