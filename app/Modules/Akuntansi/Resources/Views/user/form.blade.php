<div class="form-group row">
                {!! Form::label('nama','Username',['class' => 'col-2 col-form-label']) !!}
<div class="col-10">
                {!! Form::text('email',null,['class' => 'form-control','id' => 'username'])!!}
</div>
</div>
<div class="form-group row">
                {!! Form::label('Password Lama','Password Lama',['class' => 'col-2 col-form-label']) !!}
<div class="col-10">
                {!! Form::text('passwordlama',null,['class' => 'form-control','id' => 'passwordlama','required'])!!}
                <span class="text-danger text-salah" style="display: none;">Password Salah</span>
</div>
</div>
<div class="form-group row">
                {!! Form::label('Password Baru','Password Baru',['class' => 'col-2 col-form-label']) !!}
<div class="col-10">
                {!! Form::text('passwordbaru',null,['class' => 'form-control','id' => 'passwordbaru','required','disabled'])!!}
</div>
</div>