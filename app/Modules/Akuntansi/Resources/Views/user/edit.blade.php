@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ route('tipe-akun.index') }}">Daftar Tipe Akun</a></li>
                      <li><a href="#">Edit User</a></li>
                    </ol>

                </div>
            </div>
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Informasi</strong>
                    <ul class="list-unstyled">
                        @foreach($errors->all() as $error)
                            <li class="">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($show,['route' => ['edit.password'],'method' => 'POST','class' => 'form-horizontal form-label-left']) !!}
                @include('user/form')
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Perbarui',['type' => 'submit','class' => 'btn btn-default']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')
<script type="text/javascript">
    
$("#passwordlama").focusout(function() {
    var passwordlama = $('#passwordlama').val();
    $.ajax({
      type: "POST",
      url:"{{ route('validate.passlama') }}",
      dataType: "json",
      data: {_token : '{{ csrf_token() }}', passwordlama : passwordlama},
      success: function(data) {
           if (data == 'benar' ) {
                $('.text-salah').fadeOut("slow");
                $('#passwordlama').css({'border-color' : 'inherit'});
                $('#passwordbaru').attr('disabled', false);
            }else{
                $('.text-salah').fadeIn("slow");
                $('#passwordlama').css({'border-color' : 'red'});
                $('#passwordbaru').attr('disabled', true);
            }
        }
    });
  })
</script>
@endsection