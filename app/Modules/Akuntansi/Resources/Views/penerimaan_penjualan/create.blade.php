@extends( auth()->user()->hasPermissionTo('buat_penerimaan_penjualan') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Penjualan</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Penerimaan Penjualan</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Customer Receipt / Penerimaan Penjualan</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left', 'id' => 'form-penjualan-penawaran','btn-cetak']) !!}
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary', 'id' => 'btn-cetak']) !!}
                <div class="btn-group">
                    <button type="button" class="btn btn-block btn-default dropdown-toggle" id="btn-submit" disabled data-toggle="dropdown">
                    Simpan <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            {!! Form::submit('Simpan & Baru', ['class' => 'btn btn-info btn-block','value' => '0', 'name' => 'lanjutkan']) !!}
                        </li>
                        <li>
                            {!! Form::button('Simpan & Tutup',['type' => 'submit','class' => 'btn btn-warning btn-block']) !!}
                        </li>
                    </ul>
                </div>
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger']) !!}
                
                </div>
                </div>
                {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>
        <script type="text/javascript">
            var q = 1;
            var count = 0;
            var owe = 0;
            
            function GetURLParameter(sParam) {
                var sPageURL = window.location.search.substring(1);
                var sURLVariables = sPageURL.split('&');
                for (var i = 0; i < sURLVariables.length; i++) {
                    var sParameterName = sURLVariables[i].split('=');
                    if (sParameterName[0] == sParam) {
                        return sParameterName[1];
                    }
                }
            }

            $(function() {
              enable_cb();
                $('#cheque_amount').val(0);
                $("#group1").click(enable_cb);
                $('#pelanggan_id').val(GetURLParameter('pelanggan_id'));
                $("#pelanggan_id").select2({
                    tags: true
                });
                setDataPelanggan();
            });
            
            $( "#pelanggan_id" ).change(function() {
                setDataPelanggan();
            });

            function setDataPelanggan(){
                var pelanggan_id   = $('#pelanggan_id').val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-alamat-pelanggan') }}",
                    data: {id : pelanggan_id,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                    $('.listInvoice').find('tr').remove().end();
                    $('#no_pelanggan').val(response.no_pelanggan);    
                    $('#alamat_asal').val(response.alamat);
                    if (response.fakturPelanggan) {
                        $.each(response.fakturPelanggan, function(item, key){
                            if (key.owing > 0) {
                                duplicateForm();
                                // var jml_owing = 0;
                                // $.map(key.transaksi_uang_muka, function(elem, index) {
                                //     jml_owing = elem.jumlah + elem.total_pajak1 + elem.total_pajak2;
                                // });
                                // let new_owing = key.nilai_faktur - jml_owing;
                                // $('#owing_asli'+queue).val(new_owing);                                
                                // $('#owing'+queue).val(new_owing);
                                let queue = q - 1;
                                $('#faktur_id'+queue).val(key.id);
                                $('#no_faktur'+queue).val(key.no_faktur);
                                $('#tanggal'+queue).val(key.invoice_date_indo);
                                $('#amount'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set',key.nilai_faktur);
                                $('#owing'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set', key.owing);
                                $('#owing_asli'+queue).val(key.owing);
                                // $('#owing'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set',key.owing);
                                // $('#owing_asli'+queue).val(key.owing);
                                $('#payment_amount'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set',key.nilai_faktur);
                                $('#diskon'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set',0);
                                $('#diskon'+queue).val();
                                $('#diskon_date'+queue).val(key.invoice_date_indo);
                                $('#status_pay'+queue).val();
                                $('#membayar_antara'+queue).val(key.term_id);
                                isPayed(key.nilai_faktur, queue);
                            }
                        });
                    }
                    setAkun();
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    },
                });
            }

            function setNamaAkun(val){
                 $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get.akun_id.rekonsiliasi_bank') }}",
                    data: {id : val,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                        $('#akun').val(response.kode_akun);            
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    },
                });
            }

            // Fungsi button cetak di klik
            $("#btn-cetak").on("click", function(e) {
                e.preventDefault();
                btnCetak()
            });

            function btnCetak() {
                if ($("#pelanggan_id").val() == "") {
                    swal({
                        icon: "warning",
                        text: "Pelanggan/Barang Masih Kosong"
                    });
                }else {
                    var form = $("#form-penjualan-penawaran");
                    form.attr('target','_blank');
                    form.attr('action','{{ route('akuntansi.cetak-penerimaan-penjualan') }}');
                    form.submit();
                }
            }

            // Fungsi button simpan tutup dan simpan lanjut di klik
            $(".btn-block").on("click", function () {
                var form = $("#form-penjualan-penawaran");
                form.removeAttr('target');
                form.attr('action','{{ route($module_url->store) }}');
                $(".btn-block").on("submit", function () {
                    form.submit();
                });
            });

            
        </script>
        <script src="{{ asset('js/penerimaan-penjualan.js') }}"></script>
@endsection
