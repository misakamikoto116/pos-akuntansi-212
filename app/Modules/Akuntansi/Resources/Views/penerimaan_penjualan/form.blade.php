@if (isset($item))
  {!! Form::hidden('penerimaan_id',$item->id,['class' => 'penerimaan_penjualan_id']) !!}
@endif
<div class="form-group row">
  <div class="col-1 col-form-label">Order By</div>
  <div class="col-2">
  {!! Form::text('no_pelanggan',(empty($item->pelanggan) ? null : $item->pelanggan->no_pelanggan),['class' => 'form-control','id' => 'no_pelanggan','readonly'])!!}
  </div>
  <div class="col-5">
  {!! Form::select('pelanggan_id',$pelanggan,null,['placeholder' => '-Pilih-','class' => 'form-control select3','id' => 'pelanggan_id'])!!}
  </div>
</div>
<div class="row">
  <div class="offset-sm-3 col-sm-3">
    {!! Form::textarea('alamat_asal',(empty($item->pelanggan) ? null : $item->pelanggan->alamat),['class' => 'form-control','id' => 'alamat_asal','readonly'])!!}
  </div>
  <div class="offset-sm-2 col-sm-4">
    <div class="form-group row">
      <div class="col-4 col-form-label">Form no.</div>
      <div class="col-8">
        {!! Form::text('form_no',(empty($item->form_no) ? $idPrediction : $item->form_no ),['class' => 'form-control','id' => 'form_no', 'required'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Payment Date</div>
      <div class="col-8">
        @if (isset($item))
        {!! Form::text('payment_date',null,['class' => 'form-control tanggal_penerimaan','id' => 'payment_date'])!!}
        @else
        {!! Form::text('payment_date',null,['class' => 'form-control tanggal','id' => 'payment_date'])!!}
        @endif
      </div>
    </div>
  </div>
</div>

<hr>

<div class="row">
  <div class="offset-sm-2 col-sm-4">
    <div class="form-group row">
      <div class="col-3 col-form-label">Bank</div>
      <div class="col-5">
        {!! Form::text('akun',null,['class' => 'form-control','id' => 'akun','readonly' => 'readonly'])!!}
      </div>
      <div class="col-4">
        {!! Form::select('akun_bank_id',[],null,['class' => 'form-control akun_id','id' => 'akun_id','onchange'=>'setNamaAkun(this.value)','required','placeholder' => '- Pilih Akun -'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Rate</div>
      <div class="col-3">
        {!! Form::text('rate',1,['class' => 'form-control','id' => '','readonly' => ''])!!}
      </div>
      <div class="col-6 col-form-label">
        Currency&emsp;<strong>IDR</strong>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Cheque No.</div>
      <div class="col-6">
        {!! Form::text('cheque_no',null,['class' => 'form-control','id' => 'cheque_no','onchange' => 'checkCheque(this.val)'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Cheque Date</div>
      <div class="col-6">
        @if (isset($item))
        {!! Form::text('cheque_date',null,['class' => 'form-control tanggal_penerimaan','id' => 'cheque_date','disabled'])!!}
        @else
        {!! Form::text('cheque_date',null,['class' => 'form-control tanggal','id' => 'cheque_date','disabled'])!!}
        @endif
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Department</div>
      <div class="col-9">
        {!! Form::select('departement',[],null,['class' => 'form-control select3','id' => ''])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Project</div>
      <div class="col-9">
        {!! Form::select('project',[],null,['class' => 'form-control select3','id' => ''])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Memo</div>
      <div class="col-9">
        {!! Form::textarea('memo',null,['class' => 'form-control','id' => ''])!!}
      </div>
    </div>
  </div>

  <div class="offset-sm-2 col-sm-4">
    <div class="offset-sm-4">
    <div class="form-group row">
      <div class="col-2"><input class="form-control" type="checkbox" id="group1" value="1" name="kosong"></div>
      <div class="col-10 col-form-label">Kosong</div>
    </div>
    <div class="form-group row">
      <div class="col-2"><input class="form-control group2" type="checkbox" id="group2" value="1" name="fiscal_payment"></div>
      <div class="col-10 col-form-label">Fiscal Payment</div>
    </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Cheque Amount</div>
      <div class="col-6">
        {!! Form::text('cheque_amount',null,['class' => 'form-control','id' => 'cheque_amount','readonly'])!!}
      </div>
      <div class="col-2">
        <a onclick="sumPaymentAmount()" class="btn btn-default btn-sm"><i class="fa fa-calculator"></i></a>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Equiv Amount</div>
      <div class="col-8">
        {!! Form::text('equiv_amount',0,['class' => 'form-control','id' => 'equiv_amount','readonly'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Existing Credits</div>
      <div class="col-8">
        {!! Form::text('existing_credits',0,['class' => 'form-control','id' => 'existing_credits','readonly'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Distribute Amount</div>
      <div class="col-8">
        {!! Form::text('distribute_amount',0,['class' => 'form-control','id' => 'distribute_amount','readonly'])!!}
      </div>
    </div>
  </div>
</div>

<div class="row invoice-sections">
  <div class="col-12 duplicate-invoice-sections">
    <table class="table duplicate-sections">
      <thead class="thead-dark">
        <tr>
          <th width="20%">Invoice No.</th>
          <th>Date</th>
          <th>Amount</th>
          <th>Owing</th>
          <th>Payment Amount</th>
          <th>Total Disc.</th>
          <th></th>
          <th width="5%">Pay</th>
          <th>Discount Date</th>
        </tr>
      </thead>
      <tbody class="listInvoice">
        
      @if(old('faktur_id') !== null)
        @foreach(old('faktur_id') as $row => $faktur_id )
          @include('akuntansi::penerimaan_penjualan/components/item_invoice',['data_invoice' => [
            'row'       => $row,
            'faktur_id' => $faktur_id,
            'no_faktur' => old('no_faktur')[$row],
            'tanggal' => old('tanggal')[$row],
            'amount' => old('amount')[$row],
            'owing' => old('owing')[$row],
            'owing_asli' => old('owing_asli')[$row],
            'pure_owing' => old('pure_owing')[$row],
            'payment_amount' => old('payment_amount')[$row],
            'diskon' => old('diskon')[$row],
            'status_pay' => old('status_pay')[$row] ?? false,
            'diskon_date' => old('diskon_date')[$row],
            'membayar_antara' => old('membayar_antara')[$row]
          ]]);      
        @endforeach      
      @endif
      
      </tbody>
    </table>
  </div>
</div>
<hr>

<div class="row">
  <div class="col-sm-9">
    <div class="row">
      <div class="col-3 text-center">Total Owing : <span id="owing-label">0</span></div>
      {{ Form::hidden('tot_owing',null,['id' => 'tot_owing']) }}
      <div class="col-3 text-center">Total Payment : <span id="payment-label">0</span></div>
      <div class="col-3 text-center">Overpay : <span id="overpay-label">0</span></div>
      {{ Form::hidden('tot_overpay',null,['id' => 'tot_overpay']) }}
      <div class="col-3 text-center">Total Discount : <span id="discount-label">0</span></div>
      {{ Form::hidden('tot_diskon',null,['id' => 'tot_diskon']) }}
    </div>
  </div>
</div>

<!-- Pembatas -->

<script type="text/template" id="table_invoice_section" data-id="">
  @include('akuntansi::penerimaan_penjualan/components/item_invoice');
</script>

<!-- Modal -->
<div id="modalTambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title">Tambah Pembayaran</h2>
      </div>
      <div class="modal-body">
        {!! Form::hidden('id_bayar', null, ['class' => 'id_bayar']) !!}
        <table width="100%">
          <tr>
            <td>Tanggal</td>
            <td> 
              {!! Form::date('tgl_bayar', null, ['class' => 'form-control tgl_bayar']) !!}
             </td>
          </tr>
          <tr>
            <td>Jumlah</td>
            <td> 
              {!! Form::number('jumlah', null, ['class' => 'form-control jumlah_bayar']) !!}
             </td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="simpan()">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

 <!-- Modal Termin -->
<script type="text/template" id="mdl_termin"/>
  <div class="modal modal_termin"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Informasi Diskon</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group row">
              <div class="col-7 col-form-label">No Faktur</div>
              <div class="col-5">
                <label class="col-form-label pull-right no_faktur_mdl"></label>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-7 col-form-label">Nilai Faktur</div>
              <div class="col-5">
                <label class="col-form-label pull-right nilai_faktur_mdl"></label>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-7 col-form-label">Terutang</div>
              <div class="col-5">
                <label class="col-form-label pull-right nilai_terutang_mdl"></label>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-7 col-form-label">Syarat Pembayaran</div>
              <div class="col-5">
                <label class="col-form-label pull-right syarat_pembayaran_mdl"></label>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-7 col-form-label">Diskon Anjuran</div>
              <div class="col-5">
                <label class="col-form-label pull-right diskon_anjuran_mdl"></label>
              </div>
            </div>
            <hr style="border-bottom: 2px solid #eceeef">
            <div class="form-group row">
              <div class="col-4 col-form-label">Jumlah Diskon</div>
              <div class="col-6">
                {!! Form::text('jml_diskon', 0,['class' => 'form-control jml_diskon_mdl']) !!}
              </div>
            </div>
            <div class="form-group row">
              <div class="col-4 col-form-label">Akun Diskon</div>
              <div class="col-8">
                {!! Form::select('akun_diskon',$listAkunDiskon,null,['class' => 'form-control select3 akun_diskon_mdl','id' => '','placeholder' => '- Pilih Akun Diskon -']) !!}
              </div>
            </div>
            <div class="form-group row">
              <div class="col-4 col-form-label">Departement</div>
              <div class="col-8">
                {!! Form::select('departement',[],null,['class' => 'form-control select3 dept_mdl','id' => '']) !!}
              </div>
            </div>
            <div class="form-group row">
              <div class="col-4 col-form-label">Proyek</div>
              <div class="col-8">
                {!! Form::select('proyek',[],null,['class' => 'form-control select3 proyek_mdl','id' => '']) !!}
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default ok_mdl">Ok</button>
          <button type="button" data-dismiss="modal" class="btn btn-warning">Batal</button>
        </div>
      </div>
    </div>
  </div><!-- END Modal -->
</script>
<!-- Modal -->
<div id="modalDetail" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title">Detail Pembayaran</h2>
      </div>
      <div class="modal-body">
        <table>
          <thead>
            <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>Jumlah</th>
            <th>Opsi</th>
          </tr>
          </thead>
          <tbody id="listDetailPembayaran">
            <tr>
              <td>1</td>
              <td>24 April 2018</td>
              <td>2.000.000</td>
              <td>
                <a onclick="edit(this.value)" class="btn btn-warning btn-sm">Edit</a>
                <a onclick="hapus(this.value)" class="btn btn-danger btn-sm">Hapus</a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>