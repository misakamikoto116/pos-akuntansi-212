<tr>
  <td>
    {!! Form::hidden('faktur_id[]', isset($data_invoice) ? $data_invoice['faktur_id'] : null, ['class' => 'faktur_id']) !!}
    {!! Form::text('no_faktur[]',isset($data_invoice) ? $data_invoice['no_faktur'] : null,['class' => 'form-control no_faktur','id' => '','readonly'])!!}
  </td>
  <td>{!! Form::text('tanggal[]',isset($data_invoice) ? $data_invoice['tanggal'] : null,['class' => 'form-control tanggal date','id' => '','readonly'])!!}</td>
  <td>{!! Form::text('amount[]',isset($data_invoice) ? $data_invoice['amount'] : null,['class' => 'form-control amount','id' => '','readonly'])!!}</td>
  <td>
    {!! Form::text('owing[]',isset($data_invoice) ? $data_invoice['owing'] : null,['class' => 'form-control owing','id' => '','readonly'])!!}
    {!! Form::hidden('owing_asli[]', isset($data_invoice) ? $data_invoice['owing_asli'] : null, ['class' => 'owing_asli']) !!}
    {!! Form::hidden('pure_owing[]', isset($data_invoice) ? $data_invoice['pure_owing'] : null, ['class' => 'pure_owing']) !!}
  </td>
  @if (isset($data_invoice))
    <td>{!! Form::text('payment_amount[]',isset($data_invoice) ? $data_invoice['payment_amount'] : null,['class' => 'form-control payment_amount mask_payment','onclick' => 'checkTermin('. ($data_invoice['row'] + 1) .')'])!!}</td>
  @else
    <td>{!! Form::text('payment_amount[]',isset($data_invoice) ? $data_invoice['payment_amount'] : null,['class' => 'form-control payment_amount mask_payment'])!!}</td>
  @endif
  <td>{!! Form::text('diskon[]',isset($data_invoice) ? $data_invoice['diskon'] : null,['class' => 'form-control diskon','id' => '','readonly'])!!}</td>
  <td><button type="button" class="btn btn-primary btn-sm btn-diskon">%</button></td>
  @if (isset($data_invoice))
    <td>
        {!! Form::hidden('status_pay[]', 0,['class' => 'status_pay_hidden']) !!}
        {!! Form::checkbox('status_pay[]', 1, isset($data_invoice) ? $data_invoice['status_pay'] : null, ['class' => 'form-control group1 status_pay','onchange' => 'isPayChecked('. ($data_invoice['row'] + 1) .')']) !!}
    </td>
  @else
    <td>
        {!! Form::hidden('status_pay[]', 0,['class' => 'status_pay_hidden']) !!}
        {!! Form::checkbox('status_pay[]', 1, isset($data_invoice) ? $data_invoice['status_pay'] : null, ['class' => 'form-control group1 status_pay']) !!}
    </td>
  @endif
  <td>{!! Form::text('diskon_date[]',isset($data_invoice) ? $data_invoice['diskon_date'] : null,['class' => 'form-control diskon_date date','id' => '','readonly'])!!}</td>
  <td style="display: none;">{!! Form::text('membayar_antara[]',isset($data_invoice) ? $data_invoice['membayar_antara'] : null,['class' => 'form-control membayar_antara'])!!}</td>
</tr>