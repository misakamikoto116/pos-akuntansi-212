@if (isset($item))
  {!! Form::hidden('pesanan_pembelian', $item->id,['id' => 'pesanan_pembelian_id'])!!}
@endif
<div class="form-group row">
  <div class="col-1 col-form-label">Vendor</div>
  <div class="col-2">
  {!! Form::text('no_pemasok',null,['class' => 'form-control','id' => 'no_pemasok','readonly'])!!}
  </div>
  <div class="col-4">
  {!! Form::select('pemasok_id',$pemasok,null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'pemasok_id'])!!}
  </div>
  <div class="col-2">
    <button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-list"></i>&nbsp; Requisition</button>

    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Permintaan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Transaksi</th>
              <th>Tanggal</th>
          </tr>
          </thead>
          <tbody id="table-list-permintaan">
         
          </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-default" data-dismiss="modal" onclick="filterSelectedRequest()">Simpan</a>
      </div>
    </div>
  </div>
</div><!-- END Modal -->
  
  </div>
  <div class="col-3 form-inline">
    <div class="form-check form-check-inline">
    @if (isset($item))
    {!! Form::checkbox('taxable', 1, $item->taxable == 1 ? true : false,['class' => 'form-check-input taxable','id' => 'group1','onchange' => 'calculate()']) !!}
    @else
    {!! Form::checkbox('taxable', 1, true,['class' => 'form-check-input taxable','id' => 'group1','onchange' => 'calculate()']) !!}
    @endif
    <label class="form-check-label" for="group1"> Vendor is Taxable</label>
  </div>
  <div class="form-check form-check-inline">
    @if (isset($item))
    {!! Form::checkbox('in_tax', 1, $item->in_tax == 1 ? true : false,['class' => 'form-check-input group2 in_tax', 'id' => 'inlineCheckbox2', 'onchange' => 'calculate()']) !!}
    @else
    {!! Form::checkbox('in_tax', 1, null,['class' => 'form-check-input group2 in_tax', 'id' => 'inlineCheckbox2', 'onchange' => 'calculate()']) !!}
    @endif
    <label class="form-check-label" for="inlineCheckbox2"> Inclusive Tax</label>
  </div>
  </div>
</div>

<hr>

<div class="row">
  <div class="col-sm-3">
    <div class="form-group">
      <label for="">Alamat</label>
      {!! Form::textarea('alamat_asal',null,['class' => 'form-control','id' => 'alamat_asal','readonly'])!!}
      </div>
  </div>
  <div class="col-sm-3">
    <div class="form-group">
      <label for="">Kirim Ke</label>
      {!! Form::textarea('alamat_pengiriman',null,['class' => 'form-control','id' => 'alamat_pengiriman',''])!!}
      </div>
  </div>

  <div class="offset-sm-3 col-sm-3">
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">PO. Number</div>
          {!! Form::text('po_number',(empty($item->po_number) ? $idPrediction : $item->po_number),['class' => 'form-control','id' => ''])!!}
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">PO. Date</div>
          @if (isset($item) || old('po_date'))
          {!! Form::text('po_date',null,['class' => 'form-control tanggal_pesanan','id' => 'po_date','required'])!!}
          @else
          {!! Form::text('po_date',null,['class' => 'form-control tanggal','id' => 'po_date','required'])!!}
          @endif
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">Expected Date</div>
          @if (isset($item) || old('expected_date'))
          {!! Form::text('expected_date',null,['class' => 'form-control tanggal_pesanan','id' => 'expected_date','required'])!!}
          @else
          {!! Form::text('expected_date',null,['class' => 'form-control tanggal','id' => 'expected_date','required'])!!}
          @endif
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">FOB</div>
          {!! Form::select('fob',['0' => 'Shiping Point', '1' => 'Destination' ],null,['placeholder' => '--Pilih--', 'class' => 'form-control select2','id' => ''])!!}
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag tag-terms">Terms</div>
            {!! Form::select('term_id',$termin,null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'term_id'])!!}
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag tag-ships">Ship Via</div>
          {!! Form::select('ship_id',$pengiriman,null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'ship_id'])!!}
          </div>
      </div>
    </div>
  </div>
</div>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>
<div class="penawaran-sections" data-id="1">
<div class="duplicate-penawaran-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
    <div style="width: 1800px;">
      <table width="100%" class="table">
        <thead class="thead-light" style="border-collapse: collapse;">
          <tr>
            <th width="12%">Item</th>
            <th width="12%">Description</th>
            <th width="5%">Qty</th>
            <th>Satuan</th>
            <th width="10%">Unit Price</th>
            <th>Disc%</th>
            <th>Tax</th>
            <th>Amount</th>
            <th width="10%">Dept</th>
            <th width="10%">Proyek</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="purchasePenawaran">

            @if (old('produk_id') !== null)
              @foreach (old('produk_id') as $row => $produk_id)
                @include('akuntansi::pesanan_pembelian/components/item_pesanan_pembelian',[
                    'row'                               => $row,
                    'barang_permintaan_pembelian_id'    => old('barang_permintaan_pembelian_id')[$row],
                    'produk_id'                         => $produk_id,
                    'no_produk'                         => old('no_produk')[$row],
                    'keterangan_produk'                 => old('keterangan_produk')[$row],
                    'qty_produk'                        => old('qty_produk')[$row],
                    'satuan_produk'                     => old('satuan_produk')[$row],
                    'unit_harga_produk'                 => old('unit_harga_produk')[$row],
                    'diskon_produk'                     => old('diskon_produk')[$row],
                    'kode_pajak_id'                     => old('kode_pajak_id')[$row],
                    'amount_produk'                     => old('amount_produk')[$row],
                    'dept_id'                           => old('dept_id')[$row],
                    'proyek_id'                         => old('proyek_id')[$row],
                    'harga_modal'                       => old('harga_modal')[$row],
                  ])
              @endforeach
            @endif

        </tbody>
      </table>
    </div>
  	<a class="btn btn-info add-penawaran" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
</div><!-- END PILLS -->
</div>
</div>

<script type="text/template" id="table_penawaran_section" data-id=""/>
  <tr>
    <td>
        {!! Form::hidden('barang_permintaan_pembelian_id', null, ['class' => 'barang_permintaan_pembelian_id']) !!}        
        {!! Form::text('no_produk[]', null, ['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required', 'readonly'])!!}
        {!! Form::hidden('produk_id[]', null,['class' => 'form-control produk_id','required'])!!}
    </td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('unit_harga_produk[]',null,['class' => 'form-control tanggal unit_harga_produk mask','id' => '','required'])!!}</td>
    <td>{!! Form::text('diskon_produk[]',null,['class' => 'form-control diskon_produk','id' => '','required'])!!}</td>
    <td width="10%">
			{!! Form::select('kode_pajak_id[][]',[],null,['class' => 'form-control select2 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => '',''])!!}
    </td>
    <td>{!! Form::text('amount_produk[]',null,['class' => 'form-control amount_produk','id' => '','','readonly' => ''])!!}</td>
    <td>{!! Form::select('dept_id[]',[],null,['class' => 'form-control select2 dept_id','id' => '',''])!!}</td>
    <td>
       {!! Form::select('proyek_id[]',[],null,['class' => 'form-control select2 proyek_id','id' => '',''])!!}
       {!! Form::hidden('harga_modal[]',null,['class' => 'form-control harga_modal','id' => '',''])!!}
     </td>
    <td><button href="" class="remove btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>

<hr>
<div class="row">
  <div class="col-sm-4">
    
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Description</label>
      {!! Form::textarea('keterangan',null,['class' => 'form-control','id' => 'catatan'])!!}
    </div>
    <div class="form-group akun_ongkir">
      <label for="exampleFormControlTextarea1">Freight Account</label>
      <div class="form-group row">
        <div class="col-6 col-form-label text-right">{!! Form::text('akun_ongkir_kode', old("akun_ongkir_kode"), ["class" => "form-control akun_kode",'readonly']) !!}</div>
        @if ((isset($item)) && ($item === true))
          <div class="col-6" style="margin-top:5px;">
            {!! Form::select('akun_ongkir_id', $akun, (isset($item)) && ($item === true) ? $item->akun_ongkir_id : null, ['class' => 'form-control select2 akun_id','placeholder' => '-- Pilih Akun --']) !!}
          </div>
        @elseif(!empty(old('akun_ongkir_id')) ? old('akun_ongkir_id') : null)
          <div class="col-6" style="margin-top:5px;">
            {!! Form::select('akun_ongkir_id', $akun, !empty(old('akun_ongkir_id')) ? old('akun_ongkir_id') : null, ['class' => 'form-control select2 akun_id','placeholder' => '-- Pilih Akun --']) !!}
          </div>
        @else
          <div class="col-6" style="margin-top:5px;">
            {!! Form::select('akun_ongkir_id', $akun, null, ['class' => 'form-control select2 akun_id','placeholder' => '-- Pilih Akun --']) !!}
          </div>
        @endif
      </div>
    </div>

    <div class="form-group">
      <label for="exampleFormControlTextarea1">Akun DP</label>
      {!! Form::select('akun_dp_id',$akun, !isset($item) ? $getAkunDp : null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'catatan'])!!}
    </div>
    <hr>
    <div class="row">
    <div class="col-5 col-form-label">Uang Muka</div>
    <div class="col-7 col-form-label"><strong>0</strong></div>
    </div>
    <div class="row">
    <div class="col-5 col-form-label">Uang Muka Terpakai</div>
    <div class="col-7 col-form-label"><strong>0</strong></div>
    </div>
    <div class="row">
    <div class="col-5 col-form-label">Sisa Uang Muka</div>
    <div class="col-7 col-form-label"><strong>0</strong></div>
    </div>
  </div>
  	<div class="offset-sm-4 col-sm-4">
		<div class="form-group row">
		<div class="offset-2 col-3 col-form-label text-right"><strong>Sub Total</strong></div>
		<div class="col-7 text-right col-form-label" id="subtotal"><strong>0</strong></div>
    {{ Form::hidden("subTotal", null,['class' => 'sub-total-cetak']) }}
		</div>
		<div class="form-group row">
			<div class="col-2 col-form-label">Discount:</div>
			<div class="col-3 col-form-label">{!! Form::text('diskon', isset($item) ? $item->diskon : 0,['class' => 'form-control','id' => 'diskonTotal', 'onchange' => 'calculate()','required'])!!}</div>
			<div class="col-2 col-form-label text-center">% =</div>
			<div class="col-5 col-form-label">{!! Form::text('total_potongan_rupiah', isset($item) ? $item->total_potongan_rupiah : 0,['class' => 'form-control mask','id' => 'totalPotonganRupiah','onchange' => 'calculate()','required'])!!}</div>
    </div>
    <div class="form-group row">
			<div class="col-5 col-form-label"></div>
			<div class="col-5 col-form-label" id="taxNaration"></div>
      {!! Form::hidden('tax_cetak0', null,['id' => 'tax_cetak0'])!!}
      {!! Form::hidden('tax_cetak1', null,['id' => 'tax_cetak1'])!!}
      {!! Form::hidden('tax_cetak_label0', null,['id' => 'tax_cetak_label0'])!!}
      {!! Form::hidden('tax_cetak_label1', null,['id' => 'tax_cetak_label1'])!!}
		</div>
		<div class="form-group row" style="margin-top: 50px;">
		<div class="offset-2 col-3 col-form-label text-right">Freight</div>
		<div class="col-7">{!! Form::text('ongkir', isset($item) ? $item->ongkir : 0,['class' => 'form-control mask','id' => 'ongkir','onchange'=>'calculate()','required'])!!}</div>
		</div>
		<div class="form-group row">
		<div class="offset-1 col-4 col-form-label text-right"><strong>Total Invoice</strong></div>
		<div class="col-7 text-right col-form-label grandtotal"><strong>0</strong></div>
    {!! Form::hidden('grandtotal', null,['id' => 'grand_total'])!!}
		</div>
	</div>
</div>

@include('akuntansi::pesanan_modal/modal_barang')