<tr>
  <td>
      {!! Form::hidden('barang_permintaan_pembelian_id['.($row ?? 0).']', $barang_permintaan_pembelian_id ?? null, ['class' => 'barang_permintaan_pembelian_id', 'id' => 'barang_permintaan_pembelian_id'.$row]) !!}
      {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id ?? null, ['class' => 'form-control produk_id','required', 'id' => 'produk_id'.$row])!!}
      {!! Form::text('no_produk['.($row ?? 0).']', $no_produk ?? null,['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required', 'id' => 'no_produk'.$row,'onclick' => 'awas('. $row .')', 'readonly'])!!}
  </td>
  <td>{!! Form::text('keterangan_produk['.($row ?? 0).']', $keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.$row,'required'])!!}</td>
  <td>{!! Form::text('qty_produk['.($row ?? 0).']', $qty_produk ?? null,['class' => 'form-control qty_produk','id' => 'qty_produk'.$row,'required','onchange' => 'sumQtyAndUnit(' . $row . ')'])!!}</td>
  <td>{!! Form::text('satuan_produk['.($row ?? 0).']', $satuan_produk ?? null,['class' => 'form-control satuan_produk','id' => 'satuan_produk'.$row,'required'])!!}</td>
  <td>{!! Form::text('unit_harga_produk['.($row ?? 0).']', $unit_harga_produk ?? null,['class' => 'form-control unit_harga_produk mask','id' => 'unit_harga_produk'.$row,'required'])!!}</td>
  <td>{!! Form::text('diskon_produk['.($row ?? 0).']', $diskon_produk ?? null,['class' => 'form-control diskon_produk','id' => 'diskon_produk'.$row,'required'])!!}</td>
  <td width="10%">
		{!! Form::select('kode_pajak_id['.($row ?? 0).'][]',$pajak, $kode_pajak_id ?? null,['class' => 'form-control select2 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => 'tax_produk'.$row,''])!!}
  </td>
  <td>{!! Form::text('amount_produk['.($row ?? 0).']', $amount_produk ?? null,['class' => 'form-control amount_produk','id' => 'amount_produk'.$row,'','readonly' => ''])!!}</td>
  <td>{!! Form::select('dept_id['.($row ?? 0).']',[], $dept_id ?? null,['class' => 'form-control select2 dept_id','id' => 'dept_id'.$row,''])!!}</td>
  <td>
     {!! Form::select('proyek_id['.($row ?? 0).']',[], $proyek_id ?? null,['class' => 'form-control select2 proyek_id','id' => 'proyek_id'.$row,''])!!}
     {!! Form::hidden('harga_modal['.($row ?? 0).']', $harga_modal ?? null,['class' => 'form-control harga_modal','id' => 'harga_modal'.$row,''])!!}
   </td>
  <td><button href="" class="remove btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>