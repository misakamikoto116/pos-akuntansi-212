@extends( auth()->user()->hasPermissionTo('buat_pesanan_pembelian') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788;
        color: white;
        text-align: center;
        position: relative;
        top: 4px;
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
    .warnatr{
        background: lightsteelblue;
        color: black;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Pembelian</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Pesanan Pembeliaan</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Purcase Order / Pesanan Pembeliaan</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                 {!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left', 'id' => 'form-pesanan-pembelian']) !!}
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary', 'id' => 'btn-cetak']) !!}
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" id="btn-submit" disabled data-toggle="dropdown">
                    Simpan <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        @if ( auth()->user()->hasPermissionTo('buat_penerimaan_pembelian') )
                            <li>
                                {!! Form::submit('Terima Barang', ['class' => 'btn btn-success btn-block','value' => '1', 'name' => 'lanjutkan']) !!}
                            </li>
                        @endif
                        <li>
                            {!! Form::submit('Simpan & Baru', ['class' => 'btn btn-info btn-block','value' => '0', 'name' => 'lanjutkan']) !!}
                        </li>
                        <li>
                            {!! Form::button('Simpan & Tutup',['type' => 'submit','class' => 'btn btn-warning btn-block']) !!}
                        </li>
                    </ul>
                </div>
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

        <script type="text/javascript">
            var q = 0;

             $(function() {
                enable_cb();
                $("#group1").click(enable_cb);
                var col_length = $('.produk_id').length;
                for(var a = 0; a < col_length; a++){
                    callSelect2AjaxProdukID(a, $('#produk_id_temp'+a).val());
                    callSelect2AjaxProduk(a, 'modal');
                }

                // cek kalau old
                @if (old('produk_id'))
                    $(document).ready(function () {
                        q = $(".produk_id").length;
                        x = 0;
                        setDataPemasokWhenOld({{ old('pemasok_id') }});
                        $('.tanggal_pesanan').datepicker({format: 'dd MM yyyy', autoclose: true});
                        sumQtyAndUnit(x);
                        removeDelButton();
                        showAkunOngkir();
                    });
                @endif
            });

            function enable_cb() {
              if ($("input#group1").is(':checked')) {
                $("input.group2").removeAttr("disabled");
              } else {
                $("input.group2").attr("disabled", true).removeAttr("checked");
              }
            }

            $('#selectAll').click(function(e){
                var table= $(e.target).closest('table');
                $('th input:checkbox',table).prop('checked',this.checked);
            });

            $(document).ready(function () {
                $('.duplicate-penawaran-sections').on('click', '.add-penawaran', function () {
                    @if (old('produk_id'))
                        duplicateFormWhenOld();
                    @else
                        duplicateForm();
                    @endif
                });
            });

            // function test(vale) {
            //     var produk_id   = $('#produk_id'+vale).val();
            //     var pemasok_id   = $('#pemasok_id').val();
            //           $.ajax({
            //             type: "GET",
            //             url: "{{ route('akuntansi.get-produk') }}",
            //             data: {id : produk_id, pemasok_id : pemasok_id,
            //                 _token: '{{ csrf_token() }}'},
            //             dataType: "json",
            //             success: function(response){

            //             $('#unit_harga_produk'+vale).autoNumeric('init', {
            //                 aPad: false
            //             });

            //             $('#tax_produk'+vale).find('option').remove().end();
            //             $('#keterangan_produk'+vale).val(response.keterangan)
            //             $('#qty_produk'+vale).val('1')
            //             $('#satuan_produk'+vale).val(response.satuan)
            //             if (response.unitPrice !== null) {
            //                 $('#unit_harga_produk'+vale).autoNumeric('set', response.unitPrice);
            //             }else {
            //                 $('#unit_harga_produk'+vale).val(0);
            //             }
            //             $('#diskon_produk'+vale).val('0')
            //             $.each(response.tax, function (index, item){
            //                     $('#tax_produk'+vale).append( 
            //                     $("<option></option>")
            //                     .text(item.nilai+ "/" +item.nama)
            //                     .val(item.id)
            //                     );
            //             });
            //             if (response.harga_modal !== null) {
            //                 $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
            //             }else {
            //                 $('#harga_modal'+vale).val(0);
            //             }
            //             sumQtyAndUnit(vale);
            //             }, failure: function(errMsg) {
            //                 alert(errMsg);
            //             }
            //         });
            // }
            
          $( "#pemasok_id" ).change(function() {
            var pemasok_id   = $('#pemasok_id').val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-alamat-pemasok') }}",
                data: {id : pemasok_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#table-list-permintaan').find('tr').remove().end();
                $('.purchasePenawaran').find('tr').remove().end();
                $('#alamat_pengiriman').val(response.alamat);
                $('#alamat_asal').val(response.alamat);
                $('#no_pemasok').val(response.no_pemasok);
                q = 0;
                $("#tax_cetak0").val(null);
                $("#tax_cetak1").val(null);
                $("#tax_cetak_label0").val(null);
                $("#tax_cetak_label1").val(null);
                if (response.permintaan) {
                    $.each(response.permintaan, function (index, item){
                        $('#table-list-permintaan').append('<tr><th><input type="checkbox" value="'+item.id+'" name="permintaan[]" id="permintaan[]"/></th><td>'+item.request_no+'</td><td>'+item.request_date+'</td></tr>');
                    });
                }
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        });
         $('.tanggal1').datepicker({format: 'yyyy-mm-dd', autoclose: true});
         $('.tanggal1').datepicker('setDate', new Date());

         function filterSelectedRequest(){
            var checkCounted = $('input[name="permintaan[]"]:checked').length;
            if(checkCounted > 0){
                $('input[name="permintaan[]"]:checked').each(function() {
                    var val = $(this).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-detail-barang') }}",
                        data: {id : val, type : 'permintaan-pembelian',
                        _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                            $.each(response.barang, function(index, item){
                                duplicateForm();
                                let countTemp = q - 1;
                                callSelect2AjaxProdukID(countTemp, item.produk_id);
                                $('#produk_id'+countTemp).val(item.produk_id);
                                $('.select2').select2(); 
                                insertItemToForm(countTemp, item.id); //-1 karena diatas (diplicateForm) sdh di increment
                            });
                            for(var a = 0; a < q; a++){
                                callSelect2AjaxProduk(a, 'modal');
                            }
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                });
              }
         }

         function insertItemToForm(vale,item) {
                var url = "{{ route('akuntansi.get-produk-by-val') }}";
                var pemasok_id = $("#pemasok_id").val();                             
                      $.ajax({
                        type: "GET",
                        url: url,
                        data: {id : item, type : 'permintaan-pembelian', pemasok_id : pemasok_id,
                            _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                        $('#tax_produk'+vale).find('option').remove().end();                        
                        $('#keterangan_produk'+vale).val(response.keterangan);
                        $('#barang_permintaan_pembelian_id'+vale).val(response.id);
                        $('#qty_produk'+vale).val(response.jumlah);
                        $('#satuan_produk'+vale).val(response.satuan);
                        $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).val(0); // 0 karena ngisi sendiri langsung di inputan
                        $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).val(0); 
                        $('#diskon_produk'+vale).val(0);
                        $('#terproses'+vale).val(response.terproses);
                        if(response.ditutup == 1){
                            $('#ditutup'+vale).prop('checked', true);
                        }
                        if (response.harga_modal === null) {
                            $('#harga_modal'+vale).val(0);
                        }else {
                            $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
                        }
                        $.each(response.tax, function (index, item){
                                $('#tax_produk'+vale).append( 
                                $("<option></option>")
                                .text(item.nilai+ "/" +item.nama)
                                .val(item.id)
                                );
                        });
                        sumQtyAndUnit(vale);
                        
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
            }

            // Fungsi button cetak di klik
            $("#btn-cetak").on("click", function(e) {
                e.preventDefault();
                btnCetak()
            });

            function btnCetak() {
                if ($("#pemasok_id").val() == "" || $('.produk_id').val() == "" || $('.produk_id').val() == undefined) {
                    swal({
                        icon: "warning",
                        text: "Pemasok/Barang Masih Kosong"
                    });
                }else {
                    var form = $("#form-pesanan-pembelian");
                    form.attr('target','_blank');
                    form.attr('action','{{ route('akuntansi.cetak-pesanan-pembelian') }}');
                    form.submit();
                }
            }

            // Fungsi button simpan tutup dan simpan lanjut di klik
            $(".btn-block").on("click", function () {
                var form = $("#form-pesanan-pembelian");
                form.removeAttr('target');
                form.attr('action','{{ route($module_url->store) }}');
                $(".btn-block").on("submit", function () {
                    form.submit();
                });
            });

        
        </script>
        <script src="{{ asset('js/pesanan.js') }}"></script>
        <script src="{{ asset('js/add-btn-tag-activity.js') }}"></script>
@endsection
