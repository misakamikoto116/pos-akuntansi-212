@extends( auth()->user()->hasPermissionTo('daftar_pesanan_pembelian') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                @include('chichi_theme.layout.filter')
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Daftar Pesanan Pembelian</a></li>
                    </ol>
                </div>
            </div>
<!-- END Page-Title -->

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">{{$title_document}}</h5>
                @can('buat_pesanan_pembelian')
                    <div class="menu-header">
                        <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-plus"></i></span>
                            Tambah
                        </a>
                    </div>
                @endcan
            </div>
            <div class="card-body">
                @if($items->isEmpty())
                    <div class="alert alert-warning"> Tidak ada data. </div>
                @else
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tanggal PO</th>
                            <th>No .PO</th>
                            <th>No Pelanggan</th>
                            <th>Nama Pelanggan</th>
                            <th>Status</th>
                            <th>Diskon</th>
                            <th>Biaya Kirim</th>
                            <th>Keterangan</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td>{{\Helpers\IndonesiaDate::indonesiaDate($item->po_date)}}</td>
                                <td>{{$item->po_number}}</td>
                                <td>{{ $item->pemasok->no_pemasok }}</td>
                                <td>{{ $item->pemasok->nama }}</td>
                                <td>{!! $item->status_formatted !!}</td>
                                <td>{{ number_format($item->total_potongan_rupiah) }}</td>
                                <td>{{number_format($item->ongkir)}}</td>
                                <td>{{$item->keterangan}}</td>
                                @if(auth()->user()->hasPermissionTo('lihat_pesanan_pembelian') || auth()->user()->hasPermissionTo('ubah_pesanan_pembelian') || auth()->user()->hasPermissionTo('hapus_pesanan_pembelian'))
                                    <td>
                                        {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                                <div class="dropdown-menu">
                                                    @can('lihat_pesanan_pembelian')
                                                        <a class="dropdown-item jurnal" href="{{ route('akuntansi.show.jurnal_penerimaan',$item->id) }}">
                                                            <i class="fa fa-table"></i>&nbsp;Detail
                                                        </a>
                                                    @endcan
                                                    @can('ubah_pesanan_pembelian')
                                                        <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i> Edit</a>
                                                    @endcan
                                                    @can('hapus_pesanan_pembelian')
                                                        <a class="dropdown-item deleteBtn" href="#">
                                                            <i class="fa fa-trash"></i> Delete
                                                        </a>
                                                    @endcan
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
                <div class="pull-right">
                    {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
    <script type="text/javascript">
        $('.mask_index').autoNumeric('init');
    </script>
@endsection