@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')

<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Daftar Saldo Akun</a></li>
                    </ol>

                </div>
            </div>
<!-- END Page-Title -->
<div class="row">
        <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Daftar Saldo Akun</h5>
            <div class="menu-header">
                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalPencarian"><i class="fa fa-search"></i> Pencarian ...</button>
            </div>
            </div>
            <div class="card-body" style="overflow : auto;">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="100px">No. Akun</th>
                            <th width="100px">Nama Akun</th>
                            <th width="100px">Tahun</th>
                            <th width="100px">Saldo Awal</th>
                            @for($i = 1; $i <= 12; $i++)
                                <th width="100px">Periode {{ $i }}</th>
                            @endfor
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td>{{ $item->kode_akun }}</td>
                                <td>{{ $item->nama_akun }}</td>
                                <td>@if(empty(Request::get('search'))) {{ date('Y') }} @else {{ Request::get('search') }} @endif</td>
                                <td>{{ number_format($item->saldo_awal) }}</td>
                                <td>{{ number_format($item->periode_1) }}</td>
                                <td>{{ number_format($item->periode_2) }}</td>
                                <td>{{ number_format($item->periode_3) }}</td>
                                <td>{{ number_format($item->periode_4) }}</td>
                                <td>{{ number_format($item->periode_5) }}</td>
                                <td>{{ number_format($item->periode_6) }}</td>
                                <td>{{ number_format($item->periode_7) }}</td>
                                <td>{{ number_format($item->periode_8) }}</td>
                                <td>{{ number_format($item->periode_9) }}</td>
                                <td>{{ number_format($item->periode_10) }}</td>
                                <td>{{ number_format($item->periode_11) }}</td>
                                <td>{{ number_format($item->periode_12) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div> <!-- container -->
            <div class="pull-right">
                {!! $items->appends(Request::all())->links() !!}
            </div>
        </div> <!-- content -->
    </div>

    <div id="modalPencarian" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modalPencarian" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title mt-0">Pencarian</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                {!! Form::open(['route' => $module_url->index, 'method' => 'GET']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('search','Filter Tahun',['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::select('search', $tahun, $thisYear, ['class' => 'form-control select2']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('tipeAkun','Filter Tipe Akun',['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::select('tipe', $tipeAkun ?? null, null, ['class' => 'form-control select2']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                 <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                {!! Form::submit('Cari !', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('custom_js')
    <script type="text/javascript">
        $('.mask_duit').autoNumeric('init',{aSign: "Rp "});
        $(function() {
            $( "#datepicker" ).datepicker({dateFormat: 'yy'});
        });​
    </script>
@endsection
