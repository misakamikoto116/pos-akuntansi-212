<div id="modalBarang" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Barang</h2>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('kriteria', 'Kriteria', ['class' => 'col-1', 'style' => 'col-form-label']) !!}
                    <div class="col col-3">
                        {!! Form::select('kategori_barang', [ 'Semua', 'Kategori' ], 0, ['class' => 'form-control select2', 'id' => 'kategori_barang']) !!}
                    </div>
                    <div class="col col-6">
                        {!! Form::select('searchByKategori', [], null, ['class' => 'form-control select2 searchByKategori', 'id' => 'searchByKategori', 'disabled']) !!}
                    </div>
                    <div class="col col-2">
                        <a href="{{ route('akuntansi.barang.create') }}" target="_blank" class="btn btn-success form-control"><i class="fa fa-plus"></i> Barang</a>
                    </div>
                </div>
                <div class="modal-sections" data-id="1">
                        <div class="duplicate-modal-sections">
                            <table class="table modal_table_barang" width="100%" id="modal-table-barang">
                                <thead id="modal_thead" class="modal_thead" style="display:table; width:100%; table-layout:fixed;">	
                                </thead>
                                <tbody class="modal_tbody" style="display:table; width:100%; table-layout:fixed; display:block; overflow:auto; height: 190px;">
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
            <div class="modal-footer modal_footer_gudang">
                <div class="form-group row mr-auto" style="width: 100%;">
                    <div class="col-4">
                        {!! Form::label('label_tampilkan_gudang', 'Tampilkan Qty di Gudang : ', ['class' => 'col-form-label']) !!}
                    </div>
                    <div class="col-8">
                        {!! Form::select('modal_gudang', ['Semua Gudang'], null ,['class' => 'form-control select2', 'id' => 'modal_gudang']) !!}
                        {!! Form::hidden('modal_gudang_temp', null ,['class' => 'form-control', 'id' => 'modal_gudang_temp']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer modal_footer_qty">
                <div class="form-group row mr-auto" style="width: 100%;">
                    <div class="col-4">
                        {!! Form::label('label_qty_gudang', 'Kuantitas : ', ['class' => 'col-form-label']) !!}
                    </div>
                    <div class="col-8">
                        {!! Form::label('label_qty_gudang_footer', ' ', ['class' => 'col-form-label qty_gudang_footer', 'id' => 'qty_gudang_footer']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default modal_btn_batal" onclick="barangBatal()" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
  
<script type="text/template" id="modal_thead_barang" data-id="">
    <tr class="modal_thead_barang_tr">
        <td width="26%">
            {!! Form::text('modal_kode_barang[]', null, ['placeholder' => 'Cari Berdasarkan No Barang', 'class' => 'form-control modal_kode_barang']) !!}
        </td>
        <td width="3%"></td>
        <td width="55%">
            {!! Form::text('modal_nama_barang[]', null, ['placeholder' => 'Cari Berdasarkan Nama Barang', 'class' => 'form-control modal_nama_barang']) !!}
        </td>
    </tr>
</script>

<div id="modalPriceHistory" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">
                    <b>
                        Barang : <span class="price_history_name"></span>, 
                        Dijual Pada : <span class="pelanggan_price_history"></span>
                    </b>
                </h2>
            </div>
            <div class="modal-body">
                <table class="table modal_table_price_history" width="100%" id="modal-table-price-history">
                    <thead id="modal_thead" class="modal_thead" style="display:table; width:100%; table-layout:fixed;">	
                        <tr>
                            <th> Tanggal </th>
                            <th> Kuantitas </th>
                            <th> Unit </th>
                            <th> Harga </th>
                            <th> Diskon </th>
                        </tr>
                    </thead>
                    <tbody class="modal_tbody_price_history" style="display:table; width:100%; table-layout:fixed; overflow:auto; height: 190px; text-align: center;">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default modal_btn_batal" onclick="barangBatal()" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>