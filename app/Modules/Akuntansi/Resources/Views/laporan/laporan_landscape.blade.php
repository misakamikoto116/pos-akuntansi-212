<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title', chichi_title ())</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <meta name="csrf-token" content="{{ csrf_token() }}">        
        <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/papper.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/laporan.css')}}" rel="stylesheet" type="text/css" />
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
        <style>
            @page { 
                size: legal landscape;
                margin: 2cm 3cm;
            }
            @media screen{
                .margin{
                    margin : 0px 50px;
                }
                .sheet.padding-0mm{
                    padding: 10mm;
                }
            }
            .border-top{
                border-top: 2px solid black !important;
            }
            @media print{
                nav{
                    display: none;
                }
            body {
                -webkit-print-color-adjust: exact;
            }
            .watermark{
                display: block !important;
                top: 0;
                left: 20%;
                right: 20%;
                position: fixed !important;
                opacity: 0.2;
                z-index: 1;
            }
        }
        .watermark{
            display: none;
        }
        </style>
    </head>
    <body class="landscape margin">
        <nav>
            <div class="row">
                <div class="col-8"></div>
                <div class="col-1">
                    <button class="btn btn-sm btn-success" onclick="showFilterLaporan('{{ request()->url() }}')"><i class="ti-filter"> Filter</i></button>
                </div>
                <div class="col-1">
                    <button class="btn btn-sm btn-warning" onclick="refreshPage()"><i class="ti-reload"> Refresh</i></button>
                </div>
                <div class="col-1">
                    <button class="btn btn-sm btn-info save-page" ><i class="ti-save"> Save</i></button>
                </div>
                <div class="col-1">
                    <button class="btn btn-sm btn-info" onclick="printPage()"><i class="ti-printer"> Print</i></button>
                </div>
            </div>
        </nav>
        <img class="watermark" src="{{asset('assets/images/chichi_render.png')}}">
        <section class="sheet padding-0mm">
            <article>
                @yield('laporan')
            </article>
        </section>
        @include('akuntansi::laporan.footer_laporan')
        <div id="laporan_general_filter" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Filter Laporan</h4>
                    </div>
                    <form id="form_filter_laporan" method="get">

                    <div class="modal-body">
                        @if(isset($formFilter))
                            @include($formFilter)
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Lanjutkan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        @yield('custom_js')
    </body>
    <script>
        $(document).ready(function($) {
            var tampil_total        = GetURLParameter('tampil_total');   
            var tampil_induk        = GetURLParameter('tampil_induk');   
            var tampil_anak         = GetURLParameter('tampil_anak');   
            var saldo_nol           = GetURLParameter('saldo_nol');   
            var tampil_jumlah_induk = GetURLParameter('tampil_jumlah_induk');   

            if (tampil_total) {
                $("#tampil_total").prop('checked', true);
            }else {
                $("#tampil_total").prop('checked', false);
            }

            if (tampil_induk) {
                $("#tampil_induk").prop('checked', true);
            }else {
                $("#tampil_induk").prop('checked', false);
            }

            if (tampil_anak) {
                $("#tampil_anak").prop('checked', true);
            }else {
                $("#tampil_anak").prop('checked', false);
            }

            if (saldo_nol) {
                $("#saldo_nol").prop('checked', true);
            }else {
                $("#saldo_nol").prop('checked', false);
            }

            if (tampil_jumlah_induk) {
                $("#tampil_jumlah_induk").prop('checked', true);
            }else {
                $("#tampil_jumlah_induk").prop('checked', false);
            }

        });

        function GetURLParameter(sParam) {
                var sPageURL = window.location.search.substring(1);
                var sURLVariables = sPageURL.split('&');
                for (var i = 0; i < sURLVariables.length; i++) {
                    var sParameterName = sURLVariables[i].split('=');
                    if (sParameterName[0] == sParam) {
                        return true;
                    }
                }
        }

        function showFilterLaporan(_url) {
            //alert("filter laporan");
            $("#form_filter_laporan").attr('action', _url);
            $('#laporan_general_filter').modal('show');
        }

        function refreshPage() {
            location.reload();
        }
    </script>
