<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title', chichi_title ())</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="{{ chichi_title() }}">
        <meta name="author" content="Thortech Asia Software">
        <meta name="csrf-token" content="{{ csrf_token() }}">        
        <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/papper.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/laporan.css')}}" rel="stylesheet" type="text/css" />
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    </head>
    <body class="container">
        <div id="page-loader"></div>
        <p id="page-loader-text">
            Loading ...
        </p>
        <div id="page-show">
            <nav>
                <div class="row">
                    <div class="col-8"></div>
                    <div class="col-1">
                        <button class="btn btn-sm btn-success" onclick="showFilterLaporan('{{ request()->url() }}')"><i class="ti-filter"> Filter</i></button>
                    </div>
                    <div class="col-1">
                        <button class="btn btn-sm btn-warning" onclick="refreshPage()"><i class="ti-reload"> Refresh</i></button>
                    </div>
                    <div class="col-1">
                        <button class="btn btn-sm btn-info print-page" ><i class="ti-printer"> Print</i></button>
                    </div>
                    <div class="col-1">
                        <button class="btn btn-sm btn-info save-page"><i class="ti-save"> Save</i></button>
                    </div>
                </div>
            </nav>
            <header>

            </header>
            <section class="sheet padding-0mm">
                <article>
                    @yield('laporan')
                </article>
            </section>
            @include('akuntansi::laporan.footer_laporan')
            <div id="laporan_general_filter" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Filter Laporan</h4>
                        </div>
                        <form id="form_filter_laporan" method="get">

                        <div class="modal-body">
                            @if(isset($formFilter))
                                @include($formFilter)
                            @endif
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Lanjutkan</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @yield('custom_js')
    </body>
</html>
<script>
    $(document).ready(function($) {
        $('.save-page').prop('disabled', true);

        var tampil_total        = GetURLParameter('tampil_total');   
        var tampil_induk        = GetURLParameter('tampil_induk');   
        var tampil_anak         = GetURLParameter('tampil_anak');   
        var saldo_nol           = GetURLParameter('saldo_nol');   
        var tampil_jumlah_induk = GetURLParameter('tampil_jumlah_induk');   
        var pelanggan_transaksi = GetURLParameter('pelanggan_transaksi');  
        var pemasok_transaksi   = GetURLParameter('pemasok_transaksi')

        if (tampil_total) {
            $("#tampil_total").prop('checked', true);
        }else {
            $("#tampil_total").prop('checked', false);
        }

        if (tampil_induk) {
            $("#tampil_induk").prop('checked', true);
        }else {
            $("#tampil_induk").prop('checked', false);
        }

        if (tampil_anak) {
            $("#tampil_anak").prop('checked', true);
        }else {
            $("#tampil_anak").prop('checked', false);
        }

        if (saldo_nol) {
            $("#saldo_nol").prop('checked', true);
        }else {
            $("#saldo_nol").prop('checked', false);
        }

        if (tampil_jumlah_induk) {
            $("#tampil_jumlah_induk").prop('checked', true);
        }else {
            $("#tampil_jumlah_induk").prop('checked', false);
        }

        if (pelanggan_transaksi) {
            $("#pelanggan_transaksi").prop('checked', true);
        }else {
            $("#pelanggan_transaksi").prop('checked', false);
        }

        if (pemasok_transaksi) {
            $("#pemasok_transaksi").prop('checked', true);
        }else {
            $("#pemasok_transaksi").prop('checked', false);
        }

    });

    function GetURLParameter(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return true;
                }
            }
    }

    function showFilterLaporan(_url) {
        //alert("filter laporan");
        $("#form_filter_laporan").attr('action', _url);
        $('#laporan_general_filter').modal('show');
    }

    function refreshPage() {
        location.reload();
    }

    $('.select_barang').select2({
        ajax: {
            url: "http://localhost:8000/akuntansi/ajax/get-nama-produk",
            type: 'GET',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                };
                return query;
            },
            dataType: 'JSON',
            processResults: function (data) {
                return {
                    results: data
                };
            }
        }
    });
</script>