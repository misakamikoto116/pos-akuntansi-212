@extends('chichi_theme.layout.app')

@section('custom_css')
    <style>
        #ul-group > li > a > i{
            font-size: 15px;
            color: black;
        }
        .list-group-item > a{
            color: black;
        }
    </style>
@endsection

@section('content')
<div class="form-group row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="title">Kategori Laporan</h4>
            </div>
            <div class="card-body">
                <br>
                <div class="form-group row">
                    <div class="col-md-6">
                        <h1 class="title" align="center">Kategori Laporan</h1>
                        <ul id="ul-group" class="nav nav-pills list-group" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3);">
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-keuangan" data-toggle="pill" href="#laporan_keuangan" role="tab" aria-controls="laporan_keuangan" aria-selected="true"><i class="fa fa-folder"> Laporan Keuangan</i></a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-buku-besar" data-toggle="pill" href="#laporan_buku_besar" role="tab" aria-controls="laporan_buku_besar" aria-selected="true"><i class="fa fa-folder"> Laporan Buku Besar</i></a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-kas-dan-bank" data-toggle="pill" href="#laporan_kas_dan_bank" role="tab" aria-controls="laporan_kas_dan_bank" aria-selected="true"><i class="fa fa-folder"> Laporan Kas & Bank</i></a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-akun-piutang-pelanggan" data-toggle="pill" href="#laporan_akun_piutang_pelanggan" role="tab" aria-controls="laporan_akun_piutang_pelanggan" aria-selected="true"><i class="fa fa-folder"> Laporan Akun & Piutang Pelanggan</i></a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-penjualan" data-toggle="pill" href="#laporan_penjualan" role="tab" aria-controls="laporan_penjualan" aria-selected="true"><i class="fa fa-folder"> Laporan Penjualan</i></a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-akun-hutang-pemasok" data-toggle="pill" href="#laporan_akun_hutang_pemasok" role="tab" aria-controls="laporan_akun_hutang_pemasok" aria-selected="true"><i class="fa fa-folder"> Laporan Akun Hutang & Pemasok</i></a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-pembelian" data-toggle="pill" href="#laporan_pembelian" role="tab" aria-controls="laporan_pembelian" aria-selected="true"><i class="fa fa-folder"> Laporan Pembelian</i></a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-pembiayaan-pesanan" data-toggle="pill" href="#laporan_pembiayaan_pesanan" role="tab" aria-controls="laporan_pembiayaan_pesanan" aria-selected="true"><i class="fa fa-folder"> Laporan Pembiayaan Pesanan</i></a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-aktiva-tetap" data-toggle="pill" href="#laporan_aktiva_tetap" role="tab" aria-controls="laporan_aktiva_tetap" aria-selected="true"><i class="fa fa-folder"> Laporan Aktiva Tetap</i></a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-persediaan" data-toggle="pill" href="#laporan_persediaan" role="tab" aria-controls="laporan_persediaan" aria-selected="true"><i class="fa fa-folder"> Laporan Persediaan</i></a>
                            </li>
                            {{-- <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-pabrikasi" data-toggle="pill" href="#laporan_pabrikasi" role="tab" aria-controls="laporan_pabrikasi" aria-selected="true"><i class="fa fa-folder"> Laporan Pabrikasi</i></a>
                            </li> --}}
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-gudang" data-toggle="pill" href="#laporan_gudang" role="tab" aria-controls="laporan_gudang" aria-selected="true"><i class="fa fa-folder"> Laporan Gudang</i></a>
                            </li>
                            {{-- <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-proyek" data-toggle="pill" href="#laporan_proyek" role="tab" aria-controls="laporan_proyek" aria-selected="true"><i class="fa fa-folder"> Laporan Proyek</i></a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-departemen" data-toggle="pill" href="#laporan_departemen" role="tab" aria-controls="laporan_departemen" aria-selected="true"><i class="fa fa-folder"> Laporan Departemen</i></a>
                            </li> --}}
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-pemeriksaan" data-toggle="pill" href="#laporan_pemeriksaan" role="tab" aria-controls="laporan_pemeriksaan" aria-selected="true"><i class="fa fa-folder"> Laporan Pemeriksaan</i></a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-pajak" data-toggle="pill" href="#laporan_pajak" role="tab" aria-controls="laporan_pajak" aria-selected="true"><i class="fa fa-folder"> Laporan Pajak</i></a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-lain" data-toggle="pill" href="#laporan_lain" role="tab" aria-controls="laporan_lain" aria-selected="true"><i class="fa fa-folder"> Laporan Lain</i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade" id="laporan_keuangan" role="tabpanel" aria-labelledby="tab-laporan-keuangan">
                                @include(auth()->user()->hasPermissionTo('laporan_laporan_keuangan') ? 'akuntansi::laporan.include_laporan.laporan_keuangan' : 'exception.include_error')
                            </div>
                            <div class="tab-pane fade" id="laporan_buku_besar" role="tabpanel" aria-labelledby="tab-laporan-buku-besar">
                                @include('akuntansi::laporan.include_laporan.laporan_buku_besar')
                            </div>
                            <div class="tab-pane fade" id="laporan_kas_dan_bank" role="tabpanel" aria-labelledby="tab-laporan-kas-dan-bank">
                                @include('akuntansi::laporan.include_laporan.laporan_kas_bank')
                            </div>
                            <div class="tab-pane fade" id="laporan_akun_piutang_pelanggan" role="tabpanel" aria-labelledby="tab-laporan-akun-piutang-pelanggan">
                                @include('akuntansi::laporan.include_laporan.laporan_akun_piutang_pelanggan')
                            </div>
                            <div class="tab-pane fade" id="laporan_penjualan" role="tabpanel" aria-labelledby="tab-laporan-penjualan">
                                @include( auth()->user()->hasPermissionTo('laporan_penawaran_penjualan') || auth()->user()->hasPermissionTo('laporan_pesanan_penjualan') || auth()->user()->hasPermissionTo('laporan_pengiriman_penjualan') || auth()->user()->hasPermissionTo('laporan_faktur_penjualan') || auth()->user()->hasPermissionTo('laporan_penerimaan_penjualan') ||auth()->user()->hasPermissionTo('laporan_retur_penjualan') ? 'akuntansi::laporan.include_laporan.laporan_penjualan' : 'exception.include_error' )
                            </div>
                            <div class="tab-pane fade" id="laporan_akun_hutang_pemasok" role="tabpanel" aria-labelledby="tab-laporan-penjualan">
                                @include('akuntansi::laporan.include_laporan.laporan_akun_hutang_pemasok')
                            </div>
                            <div class="tab-pane fade" id="laporan_pembelian" role="tabpanel" aria-labelledby="tab-laporan-pembelian">
                                @include( auth()->user()->hasPermissionTo('laporan_permintaan_pembelian') ||  auth()->user()->hasPermissionTo('laporan_pesanan_pembelian') ||  auth()->user()->hasPermissionTo('laporan_penerimaan_pembelian') ||  auth()->user()->hasPermissionTo('laporan_faktur_pembelian') ||  auth()->user()->hasPermissionTo('laporan_pembayaran_pembelian') ||  auth()->user()->hasPermissionTo('laporan_retur_pembelian') ? 'akuntansi::laporan.include_laporan.laporan_pembelian' : 'exception.include_error')
                            </div>
                            <div class="tab-pane fade" id="laporan_pembiayaan_pesanan" role="tabpanel" aria-labelledby="tab-laporan-pembelian-pesanan">
                                @include(auth()->user()->hasPermissionTo('laporan_pembiayaan_pesanan') ? 'akuntansi::laporan.include_laporan.laporan_pembiayaan_pesanan' : 'exception.include_error')
                            </div>
                            <div class="tab-pane fade" id="laporan_aktiva_tetap" role="tabpanel" aria-labelledby="tab-laporan-aktiva-tetap">
                                @include(auth()->user()->hasPermissionTo('laporan_aktiva_tetap') ? 'akuntansi::laporan.include_laporan.laporan_aktiva_tetap' : 'exception.include_error')
                            </div>
                            <div class="tab-pane fade" id="laporan_persediaan" role="tabpanel" aria-labelledby="tab-laporan-persediaan">
                                @include('akuntansi::laporan.include_laporan.laporan_persediaan')
                            </div>
                            <div class="tab-pane fade" id="laporan_pabrikasi" role="tabpanel" aria-labelledby="tab-laporan-pabrikasi">
                                @include('akuntansi::laporan.include_laporan.laporan_pabrikasi')
                            </div>
                            <div class="tab-pane fade" id="laporan_gudang" role="tabpanel" aria-labelledby="tab-laporan-gudang">
                                @include(auth()->user()->hasPermissionTo('laporan_gudang') ? 'akuntansi::laporan.include_laporan.laporan_gudang' : 'exception.include_error')
                            </div>
                            <div class="tab-pane fade" id="laporan_proyek" role="tabpanel" aria-labelledby="tab-laporan-proyek">
                                @include('akuntansi::laporan.include_laporan.laporan_proyek')
                            </div>
                            <div class="tab-pane fade" id="laporan_departemen" role="tabpanel" aria-labelledby="tab-laporan-departemen">
                                @include('akuntansi::laporan.include_laporan.laporan_departemen')
                            </div>
                            <div class="tab-pane fade" id="laporan_pemeriksaan" role="tabpanel" aria-labelledby="tab-laporan-pemeriksaan">
                                @include('akuntansi::laporan.include_laporan.laporan_pemeriksaan')
                            </div>
                            <div class="tab-pane fade" id="laporan_pajak" role="tabpanel" aria-labelledby="tab-laporan-pajak">
                                @include('akuntansi::laporan.include_laporan.laporan_pajak')
                            </div>
                            <div class="tab-pane fade" id="laporan_lain" role="tabpanel" aria-labelledby="tab-laporan-lain">
                                @include('akuntansi::laporan.include_laporan.laporan_lain')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
    <script>
        function scrolltop() {
            $("html, body").animate({ scrollTop: 125 }, 1000);
        }

        $(document).ready(function () {
            var base_url = window.location.origin + '/akuntansi';
            var token_ = $("meta[name='csrf-token']").attr("content");

            $.ajax({
                type: "GET",
                url: "" + base_url + "/ajax/get-all-kategori-produk",
                data: { _token: token_ },
                dataType: "json",
                success: function(response){
                    $.each(response, function (key, item){
                        $('#kategori_barang_rincian_umur').append( 
                            $("<option></option>")
                            .text(item)
                            .val(key)
                        );
                    });
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });

            $('#kategori_barang_rincian_umur').change( function () {
                resetBarang();
            });

            $('#reset_barang_rincian_umur').click( function () {
                resetBarang();
            });

            function resetBarang()
            {
                $('#barang_rincian_umur').html('<option value="0" selected>Semua</ option>');
            }

            $('#barang_rincian_umur').select2({
                minimumInputLength: 1,
                formatInputTooShort: function () {
                    return "Ketik 1 Karakter";
                },
                ajax: {
                    url: "" + base_url + "/ajax/get-nama-produk",
                    type: 'GET',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            kategoriBySearch : $('#kategori_barang_rincian_umur').val(),
                            _token : token_,
                            type: 'public'
                        }
                        return query;
                    },
                    dataType: 'JSON',
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                }
            });
            // function ajaxModalKategori(searchBy){
            //     $.ajax({
            //         type: "GET",
            //         url: "" + base_url + "/ajax/get-all-kategori-produk",
            //         data: { _token: token_ },
            //         dataType: "json",
            //         success: function(response){
            //             $.each(response, function (key, item){
            //                 searchBy.append( 
            //                     $("<option></option>")
            //                     .text(item)
            //                     .val(key)
            //                 );
            //             });
            //         }, failure: function(errMsg) {
            //             alert(errMsg);
            //         }
            //     });
            // }
        });
    </script>
@endsection