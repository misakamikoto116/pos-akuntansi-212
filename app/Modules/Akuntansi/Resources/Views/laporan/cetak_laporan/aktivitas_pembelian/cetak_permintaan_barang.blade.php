<style>
    .pad-text{
        font-size: 15px!important;
        padding: 5px;
    }
    .mark{
        background: none !important;
        border-bottom : 1px solid black;
    }
    .label{
        font-size: 12px;
        margin: 5px 0px;
    }
    .card{
        border: 1px solid black !important; 
        border-radius: 15px!important;
        padding: 0px 15px;
        overflow:hidden;
        vertical-align: middle;
    }
    .radius-top{
        border-radius: 15px 15px 0px 0px !important;
    }
    .radius-bottom{
        border-radius: 0px 0px 15px 15px !important;
    }
    .rata-tengah{
        text-align:center;
    }
    .rata-kiri{
        text-align:left;
    }
    .rata-kanan{
        text-align:right;
    }
    .border{
        border: 1px solid black;
    }
    th{
        border: 2px solid black !important;
        text-align: center !important;
    }
    tr:last-child{
        border-bottom: none !important;
    }
    td{
        border: 2px solid black;
        border-top: none;
        border-bottom: none;
        font-size: 12px;
    }
    tr:last-child{
        border-bottom: 2px solid black !important;
    }
    tr:nth-child(even){
        background-color: #f2f2f2;
    }
    .legend{
        background: white;
        position: absolute;
        margin: -13px 20px;
        z-index: 9999;
    }
</style>
@extends('akuntansi::laporan.laporan_landscape')
@section('title')
    Cetak Permintaan Pembelian
@endsection
@section('laporan')
<div class="form-group row">
    <div class="col-7">
        <div class="form-group row">
            <div class="col-3"></div>
            <div class="col">
                <div class="card" style="height: 100px;">
                    <div class="row mark">
                        <div class="col-4 label" style="font-size: 15px; font-weight: bolder;">{{ $identitas->nama_perusahaan ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-4 label">{{ $identitas->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-5 rata-tengah">
        <h4>Purchase Requisition</h4>
        <div class="col pad-text">
            <div class="card label">
                <div class="row">
                    <div class="col rata-kiri">
                        Request Date
                        <div class="col rata-kanan">
                            {{ $items['request_date'] ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Request Number
                        <div class="col rata-kanan">
                            {{ $items['request_no'] ?? null }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr class="table-header">
                    <th>Item</th>
                    <th>Item Description</th>
                    <th>Qty</th>
                    <th>Required</th>
                    <th>Notes</th>
                    <th>Qty Order</th>
                    <th>Qty Received</th>
                </tr>
            </thead>
            @foreach ($arrayDetailBarang as $dataBarang)
                <tr>
                    <td>{{ $dataBarang['no_item'] ?? null }}</td>
                    <td>{{ $dataBarang['item_deskripsi'] ?? null }}</td>
                    <td>{{ $dataBarang['qty_produk'] ?? null }}</td>
                    <td>{{ $dataBarang['required_date'] ?? null }}</td>
                    <td>{{ $dataBarang['notes'] ?? null }}</td>
                    <td>{{ $dataBarang['qty_ordered'] ?? null }}</td>
                    <td>{{ $dataBarang['qty_received'] ?? null }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="row">
    <div class="col-7">
        <div class="form-group row">
            <div class="col">
                <span class="legend">Description</span>
                <div class="card" style="height:100px;">
                    <div class="pad-text" style="padding : 10px 10px !important;">
                        {{ $items['catatan'] ?? null }}
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
@endsection