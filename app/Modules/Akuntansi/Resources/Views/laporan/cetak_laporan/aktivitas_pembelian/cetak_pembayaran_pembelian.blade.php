<style>
    .pad-text{
        padding: 5px;
        font-size: 15px!important;
    }
    .mark{
        background: none !important;
        border-bottom : 1px solid black;
    }
    .label{
        font-size: 12px;
        margin: 5px 0px;
    }
    .card{
        border: 1px solid black !important; 
        border-radius: 15px!important;
        padding: 0px 15px;
        overflow:hidden;
        vertical-align: middle;
    }
    .radius-top{
        border-radius: 15px 15px 0px 0px !important;
    }
    .radius-bottom{
        border-radius: 0px 0px 15px 15px !important;
    }
    .rata-tengah{
        text-align:center;
    }
    .rata-kiri{
        text-align:left;
    }
    .rata-kanan{
        text-align:right;
    }
    .border{
        border: 1px solid black;
    }
    th{
        border: 2px solid black !important;
        text-align: center !important;
    }
    tr:last-child{
        border-bottom: none !important;
    }
    td{
        border: 2px solid black;
        border-top: none;
        border-bottom: none;
        font-size: 12px;
    }
    tr:last-child{
        border-bottom: 2px solid black !important;
    }
    tr:nth-child(even){
        background-color: #f2f2f2;
    }
    .legend{
        background: white;
        position: absolute;
        margin: -13px 20px;
        z-index: 9999;
    }
</style>
@extends('akuntansi::laporan.laporan_landscape')
@section('title')
    Cetak Pembayaran Pembelian
@endsection
@section('laporan')
<div class="form-group row">
    <div class="col-7">
        <div class="form-group row">
            <div class="col-3"></div>
            <div class="col">
                <div class="card" style="height: 100px;">
                    <div class="row mark">
                        <div class="col-4 label" style="font-size: 15px; font-weight: bolder;">{{ $identitas->nama_perusahaan ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-4 label">{{ $identitas->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-2">
                <div class="label pad-text">Vendor :</div>
            </div>
            <div class="col">
                <div class="card" style="height: 100px;">
                    <div class="row mark">
                        <div class="col-4 label" style="font-size: 15px; font-weight: bolder;">{{ $pemasok->nama ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-4 label">{{ $pemasok->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-5 rata-tengah">
        <h4>Purchase Payment</h4>
        <div class="col pad-text">
            <div class="card label">
                <div class="row mark">
                    <div class="col rata-kiri">
                        Payment Date
                        <div class="col rata-kanan">
                            {{ $items['payment_date'] ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Form No
                        <div class="col rata-kanan">
                            {{ $items['form_no'] ?? null }}
                        </div>
                    </div>
                </div>
                <div class="row mark">
                    <div class="col rata-kiri">
                        Cheque Date
                        <div class="col rata-kanan">
                            {{ $items['cheque_date'] ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Cheque No
                        <div class="col rata-kanan">
                            {{ $items['cheque_no'] ?? null}}
                        </div>
                    </div>
                </div>
                <div class="row mark">
                    <div class="col rata-kiri">
                        Bank
                        <div class="col rata-kanan">
                            {{ $akun->kode_akun ?? null }} &ndash; {{ $akun->nama_akun ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Cheque Amount
                        <div class="col rata-kanan">
                            {{ $items['cheque_amount'] ?? null }}
                        </div>
                    </div>
                </div>
                <div class="row mark">
                    <div class="col rata-kiri">
                        Currency
                        <div class="col rata-kanan">
                            IDR
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Rate
                        <div class="col rata-kanan">
                            {{ $items['rate'] ?? null }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr class="table-header">
                    <th>Invoice No.</th>
                    <th>Date</th>
                    <th>Due</th>
                    <th>Amount</th>
                    <th>Owing</th>
                    <th>Payment Amount</th>
                    <th>Disc. Amount</th>
                </tr>
            </thead>
            @foreach ($arrayFaktur as $faktur)
                <tr>
                    <td>{{ $faktur['no_faktur'] ?? null }}</td>
                    <td>{{ $faktur['tanggal'] ?? null }}</td>
                    <td>{{ $faktur['due'] ?? null }}</td>
                    <td>{{ $faktur['amount'] ?? null }}</td>
                    <td>{{ $faktur['owing'] ?? null }}</td>
                    <td>{{ $faktur['payment_amount'] ?? null }}</td>
                    <td>0</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="form-group row">
    <div class="col-7">
        <div class="form-group row">
            <div class="col-1">
                <div class="label" style="max-height: 40px;">Say</div>
            </div>
            <div class="col-11">
                <div class="card label" style="padding: 0px 20px;min-height: 20px; max-height: 40px; border-radius: 10px 10px 10px 10px!important;">
                    <span id="terbilang"></span>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-3">
                Prepared By
                <br>
                <br>
                <br>
                <br>
                <div class="border"></div>
                Date : 
            </div>
            <div class="col-3">
                Received By
                <br>
                <br>
                <br>
                <br>
                <div class="border"></div>
                Date : 
            </div>
            <div class="col-3">
                Approved By
                <br>
                <br>
                <br>
                <br>
                <div class="border"></div>
                Date : 
            </div>
            <div class="col-3">
                Received By
                <br>
                <br>
                <br>
                <br>
                <div class="border"></div>
                Date : 
            </div>
        </div>
    </div>    
    <div class="col-5 rata-tengah">
        <div class="form-group row">
            <div class="col-12">
                <div class="card">
                    <div class="row">
                        <div class="col-6 pad-text">
                            Sub Total
                        </div>
                        <div class="col-6 pad-text rata-kanan">
                            {{ $items['cheque_amount'] ?? null }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 pad-text">
                            Discount
                        </div>
                        <div class="col-6 pad-text rata-kanan">
                            0
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <div class="card">
                    <div class="row">
                        <div class="col-6 pad-text">
                            Total Payment
                        </div>
                        <div class="col-6 pad-text rata-kanan">
                            <span class="total-invoice">{{ $items['cheque_amount'] ?? null }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col">
                <span class="legend">Memo</span>
                <div class="card" style="height:100px;">
                    <div class="pad-text">
                        {{ $items['memo'] ?? null }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{ asset('assets/js/terbilang.js') }}"></script>