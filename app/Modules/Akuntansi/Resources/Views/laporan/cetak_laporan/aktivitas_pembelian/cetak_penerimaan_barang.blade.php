<style>
    .pad-text{
        padding: 5px;
        font-size: 15px!important;
    }
    .mark{
        background: none !important;
        border-bottom : 1px solid black;
    }
    .label{
        font-size: 12px;
        margin: 5px 0px;
    }
    .card{
        border: 1px solid black !important; 
        border-radius: 15px!important;
        padding: 0px 15px;
        overflow:hidden;
        vertical-align: middle;
    }
    .radius-top{
        border-radius: 15px 15px 0px 0px !important;
    }
    .radius-bottom{
        border-radius: 0px 0px 15px 15px !important;
    }
    .rata-tengah{
        text-align:center;
    }
    .rata-kiri{
        text-align:left;
    }
    .rata-kanan{
        text-align:right;
    }
    .border{
        border: 1px solid black;
    }
    th{
        border: 2px solid black !important;
        text-align: center !important;
    }
    tr:last-child{
        border-bottom: none !important;
    }
    td{
        border: 2px solid black;
        border-top: none;
        border-bottom: none;
        font-size: 12px;
    }
    tr:last-child{
        border-bottom: 2px solid black !important;
    }
    tr:nth-child(even){
        background-color: #f2f2f2;
    }
    .legend{
        background: white;
        position: absolute;
        margin: -13px 20px;
        z-index: 9999;
    }
</style>
@extends('akuntansi::laporan.laporan_landscape')
@section('title')
    Cetak Penerimaan Barang
@endsection
@section('laporan')
<div class="form-group row">
    <div class="col-7">
        <div class="form-group row">
            <div class="col-3"></div>
            <div class="col">
                <div class="card" style="height: 80px;">
                    <div class="row mark">
                        <div class="col-4 label" style="font-size: 15px; font-weight: bolder;">{{ $identitas->nama_perusahaan ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-4 label">{{ $identitas->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-2">
                <div class="label pad-text">Vendor :</div>
            </div>
            <div class="col">
                <div class="card" style="height: 80px;">
                    <div class="row">
                        <div class="col-4 label" style="font-size: 15px; font-weight: bolder;">{{ $pemasok->nama ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-4 label">{{ $pemasok->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-5 rata-tengah">
        <h4>Received Item</h4>
        <div class="col pad-text">
            <div class="card label">
                <div class="row mark">
                    <div class="col rata-kiri">
                        Received Date
                        <div class="col rata-kanan">
                            {{ $items['receive_date'] ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Received Number
                        <div class="col rata-kanan">
                            {{ $items['receipt_no'] ?? null }}
                        </div>
                    </div>
                </div>
                <div class="row mark">
                    <div class="col rata-kiri">
                        Form No.
                        <div class="col rata-kanan">
                            {{ $items['form_no'] ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        FOB
                        <div class="col rata-kanan">
                            @if ($items['fob'] == "0")
                                Shiping Point
                            @elseif($items['fob'] == "1")
                                Destination
                            @elseif($items['fob'] == 'placeholder')
                                {{ null }}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row mark">
                    <div class="col rata-kiri">
                        Ship Via
                        <div class="col rata-kanan">
                            {{ $ship ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Ship Date
                        <div class="col rata-kanan">
                            {{ $items['ship_date'] }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr class="table-header">
                    <th width="25%">Item</th>
                    <th width="45%">Item Description</th>
                    <th width="10%">Qty</th>
                    <th width="25%">Serial Number</th>
                </tr>
            </thead>
            @foreach($arrayDetailBarang as $dataBarang)
                <tr>
                    <td>{{ $dataBarang['no_item'] ?? null }}</td>
                    <td>{{ $dataBarang['item_deskripsi'] }}</td>
                    <td>{{ $dataBarang['qty_produk'] }}</td>
                    <td>{{ $dataBarang['sn'] }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group row">
            <div class="col-4">
                Prepared By
                <br>
                <br>
                <br>
                <br>
                <div class="border"></div>
                Date : 
            </div>
            <div class="col-4">
                Received By
                <br>
                <br>
                <br>
                <br>
                <div class="border"></div>
                Date : 
            </div>
            <div class="col-4">
                Approved By
                <br>
                <br>
                <br>
                <br>
                <div class="border"></div>
                Date : 
            </div>
        </div>
    </div>    
    <div class="col-6">
    <div class="form-group row">
            <div class="col">
                <span class="legend">Description</span>
                <div class="card" style="height:100px;">
                    <div class="pad-text">
                        {{ $items['keterangan'] ?? null }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection