<style>
    .pad-text{
        padding: 5px;
        font-size: 15px!important;
    }
    .mark{
        background: none !important;
        border-bottom : 1px solid black;
    }
    .label{
        font-size: 12px;
        margin: 5px 0px;
    }
    .card{
        border: 1px solid black !important; 
        border-radius: 15px!important;
        padding: 0px 15px;
        overflow:hidden;
        vertical-align: middle;
    }
    .radius-top{
        border-radius: 15px 15px 0px 0px !important;
    }
    .radius-bottom{
        border-radius: 0px 0px 15px 15px !important;
    }
    .rata-tengah{
        text-align:center;
    }
    .rata-kiri{
        text-align:left;
    }
    .rata-kanan{
        text-align:right;
    }
    .border{
        border: 1px solid black;
    }
    th{
        border: 2px solid black !important;
        text-align: center !important;
    }
    tr:last-child{
        border-bottom: none !important;
    }
    td{
        border: 2px solid black;
        border-top: none;
        border-bottom: none;
        font-size: 12px;
    }
    tr:last-child{
        border-bottom: 2px solid black !important;
    }
    tr:nth-child(even){
        background-color: #f2f2f2;
    }
    .legend{
        background: white;
        position: absolute;
        margin: -13px 20px;
        z-index: 9999;
    }
</style>
@extends('akuntansi::laporan.laporan_landscape')
@section('title')
    Cetak Faktur Pembelian
@endsection
@section('laporan')
<div class="form-group row">
    <div class="col-7">
        <div class="form-group row">
            <div class="col-3"></div>
            <div class="col">
                <div class="card" style="height: 100px;">
                    <div class="row mark">
                        <div class="col-4 label"style="font-size: 15px; font-weight: bolder;">{{ $identitas->nama_perusahaan ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-4 label">{{ $identitas->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-2">
                <div class="label pad-text">Vendor :</div>
            </div>
            <div class="col">
                <div class="card" style="height: 100px;">
                    <div class="row mark">
                        <div class="col-4 label"style="font-size: 15px; font-weight: bolder;">{{ $pemasok->nama ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-4 label">{{ $pemasok->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-5 rata-tengah">
        <h4>Purchase Invoice</h4>
        <div class="col pad-text">
            <div class="card label">
                <div class="row mark">
                    <div class="col rata-kiri">
                        Invoice No
                        <div class="col rata-kanan">
                            {{ $items['no_faktur'] ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Invoice Date
                        <div class="col rata-kanan">
                            {{ $items['invoice_date'] ?? null }}
                        </div>
                    </div>
                </div>
                <div class="row mark">
                    <div class="col rata-kiri">
                        Forms No
                        <div class="col rata-kanan">
                            {{ $items['form_no'] ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Terms
                        <div class="col rata-kanan">
                            {{ $termin ?? null }}
                        </div>
                    </div>
                </div>
                <div class="row mark">
                    <div class="col rata-kiri">
                        Amount
                        <div class="col rata-kanan">
                            {{ number_format($items['total']) ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        FOB
                        <div class="col rata-kanan">
                            @if ($items['fob'] == "0")
                                Shiping Point
                            @elseif($items['fob'] == "1")
                                Destination
                            @elseif($items['fob'] == 'placeholder')
                                {{ null }}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row mark">
                    <div class="col rata-kiri">
                        Ship Via
                        <div class="col rata-kanan">
                            {{ $ship ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Ship Date
                        <div class="col rata-kanan">
                            {{ $items['ship_date'] ?? null }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr class="table-header">
                    <th width="20%">Item</th>
                    <th width="25%">Item Description</th>
                    <th width="5%">Qty</th>
                    <th width="20%">Unit Price</th>
                    <th width="5%">Disc</th>
                    <th width="5%">Tax</th>
                    <th width="20%">Amount</th>
                </tr>
            </thead>
            @foreach ($arrayDetailBarang as $detailBarang)
                <tr>
                    <td width="20%">{{ $detailBarang['no_item'] }}</td>
                    <td width="30%">{{ $detailBarang['item_deskripsi'] }}</td>
                    <td>{{ $detailBarang['qty_produk'] }}</td>
                    <td>{{ $detailBarang['unit_harga_produk'] ?? 0 }}</td>
                    <td width="5%">{{ $detailBarang['diskon_produk'] }}</td>
                    <td width="5%">
                        @foreach ($detailBarang['kode_pajak'] as $dataPajak)
                            {{ $dataPajak }} &nbsp;
                        @endforeach
                    </td>
                    <td>{{ $detailBarang['amount_produk'] ?? 0 }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<div class="row">
    <div class="col-7">
        <div class="form-group row">
            <div class="col-1">
                <div class="label" style="max-height: 40px;">Say</div>
            </div>
            <div class="col-11">
                <div class="card label" style="padding: 0px 20px;min-height: 20px; max-height: 40px; border-radius: 10px 10px 10px 10px!important;">
                    <span id="terbilang"></span>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col">
                <span class="legend">Description</span>
                <div class="card" style="height:100px;">
                    <div class="pad-text">
                        {{ $items['catatan'] ?? null }}
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-3">
                Prepared By
                <br>
                <br>
                <br>
                <br>
                <div class="border"></div>
                Date : 
            </div>
            <div class="col-3">
                Received By
                <br>
                <br>
                <br>
                <br>
                <div class="border"></div>
                Date : 
            </div>
            <div class="col-3">
                Approved By
                <br>
                <br>
                <br>
                <br>
                <div class="border"></div>
                Date : 
            </div>
        </div>
    </div>    
    <div class="col-5 rata-tengah">
        <div class="form-group row">
            <div class="col-12">
                <div class="card">
                    <div class="row">
                        <div class="col-6 pad-text">
                            Sub Total
                        </div>
                        <div class="col-6 pad-text rata-kanan">
                            {{ number_format($items['subtotal']) ?? 0 }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 pad-text">
                            Discount
                        </div>
                        <div class="col-6 pad-text rata-kanan">
                            {{ number_format($items['total_diskon_faktur']) ?? 0 }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <div class="card">
                    <div class="row">
                        <div class="col-6 pad-text">
                            {{ $items['tax_cetak_label0'] ?? null }}
                        </div>
                        <div class="col-6 pad-text rata-kanan">
                            {{ number_format($items['tax_cetak0']) ?? null }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 pad-text">
                            {{ $items['tax_cetak_label1'] ?? null }}
                        </div>
                        <div class="col-6 pad-text rata-kanan">
                            {{ number_format($items['tax_cetak1']) ?? null }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <div class="card">
                    <div class="row">
                        <div class="col-6 pad-text">
                            Total Order
                        </div>
                        <div class="col-6 pad-text rata-kanan">
                            <span class="total-invoice">{{ number_format($items['total']) ?? 0 }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{ asset('assets/js/terbilang.js') }}"></script>