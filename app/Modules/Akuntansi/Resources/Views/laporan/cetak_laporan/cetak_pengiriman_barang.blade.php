<style>
    .sign-text{
        padding-bottom: 80px;
    }
    .nota-transaksi{
        height: 20px;
        overflow: hidden;
    }
    .pad-text{
        padding: 5px;
        font-size: 15px!important;
    }
    .mark{
        background: none !important;
        border-bottom : 1px solid black;
    }
    .label{
        font-size: 12px;
        margin: 5px 0px;
    }
    .card{
        border: 1px solid black !important; 
        border-radius: 15px!important;
        padding: 0px 15px;
        overflow:hidden;
    }
    .radius-top{
        border-radius: 15px 15px 0px 0px !important;
    }
    .radius-bottom{
        border-radius: 0px 0px 15px 15px !important;
    }
    .rata-tengah{
        text-align:center;
    }
    .rata-kiri{
        text-align:left;
    }
    .rata-kanan{
        text-align:right;
    }
    .border{
        border: 1px solid black;
    }
    th{
        border: 2px solid black !important;
        text-align: center !important;
    }
    tr:last-child{
        border-bottom: none !important;
    }
    td{
        border: 2px solid black;
        border-top: none;
        border-bottom: none;
        font-size: 12px;
    }
    tr:last-child{
        border-bottom: 2px solid black !important;
    }
    tr:nth-child(even){
        background-color: #f2f2f2;
    }
</style>
@extends('akuntansi::laporan.laporan_landscape')
@section('title')
    Cetak Pengiriman Barang
@endsection
@section('laporan')
<div class="form-group row">
    <div class="col-7">
        <div class="form-group row">
            <div class="col-3"></div>
            <div class="col">
                <div class="card">
                    <div class="row mark">
                        <div class="col-12 label" style="font-size: 15px; font-weight: bolder;">{{ $identitas->nama_perusahaan ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-12 label">{{ $identitas->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-2">
                <div class="label pad-text">Bill To :</div>
            </div>
            <div class="col">
                <div class="card radius-top">
                    <div class="row">
                        <div class="col-12 label" style="font-size: 15px; font-weight: bolder;">{{ $pelanggan->nama ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-12 label">{{ $pelanggan->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-2">
                <div class="label pad-text">Ship To :</div>
            </div>
            <div class="col">
                <div class="card radius-bottom">
                    <div class="row">
                        <div class="col-12 label" style="font-size: 15px; font-weight: bolder;">{{ $pelanggan_kirim->nama  ?? $pelanggan->nama }}</div>
                    </div>
                    <div class="row">
                        <div class="col-12 label">{{ $pelanggan_kirim->alamat ?? $pelanggan->alamat }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-5 rata-tengah">
        <h1>Delivery Order</h1>            
        <div class="col pad-text">
            <div class="card">
                <div class="row mark">
                    <div class="col rata-kiri">
                        Delivery Date
                        <div class="col rata-kanan">
                            {{ $items['delivery_date'] }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Delivery No
                        <div class="col rata-kanan">
                            {{ $items['delivery_no'] }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col rata-kiri">
                        Ship Via
                        <div class="col rata-kanan">
                            {{ $ship }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        PO. No
                        <div class="col rata-kanan">
                            {{ $items['po_no'] }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr class="table-header">
                    <th width="20%">Item</th>
                    <th width="40%">Item Description</th>
                    <th widht="10%">Qty</th>
                    <th widht="30%">Serial Number</th>
                </tr>
            </thead>
             @foreach ($arrayDetailBarang as $detailBarang)
                <tr>
                    <td width="20%">{{ $detailBarang['no_item'] }}</td>
                    <td width="30%">{{ $detailBarang['item_deskripsi'] }}</td>
                    <td>{{ $detailBarang['qty_produk'] }}</td>
                    <td>{{ $detailBarang['sn'] }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
    <table class="table">
        <tbody>
            <tr style="border:none!important;">
                <td style="border:none!important;">
                    <div class="row">
                        <div class="col-2">
                            <div class="sign-text">Prepared By</div>
                            <div class="border"></div>
                            Date : 
                        </div>
                        <div class="col-2">
                            <div class="sign-text">Approved By</div>
                            <div class="border"></div>
                            Date : 
                        </div>
                        <div class="col-2">
                            <div class="sign-text">Shipping By</div>
                            <div class="border"></div>
                            Date : 
                        </div>
                        <div class="col-2">
                            <div class="sign-text">Received By</div>
                            <div class="border"></div>
                            Date : 
                        </div>
                        <div class="col-4">
                            <span class="legend">Description</span>
                            <div class="card" style="height:95px;">
                                <div class="pad-text">
                                    {{$items['keterangan']}}
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
@endsection