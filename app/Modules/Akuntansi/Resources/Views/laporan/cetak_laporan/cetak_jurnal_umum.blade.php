<style>
    .sign-text{
        padding-bottom: 80px;
    }
    .nota-transaksi{
        height: 20px;
        overflow: hidden;
    }
    .pad-text{
        padding: 5px;
        font-size: 15px!important;
    }
    .mark{
        background: none !important;
        border-bottom : 1px solid black;
    }
    .label{
        font-size: 12px;
        margin: 5px 0px;
    }
    .card{
        border: 1px solid black !important; 
        border-radius: 15px!important;
        padding: 0px 15px;
        overflow:hidden;
        vertical-align: middle;
    }
    .radius-top{
        border-radius: 15px 15px 0px 0px !important;
    }
    .radius-bottom{
        border-radius: 0px 0px 15px 15px !important;
    }
    .rata-tengah{
        text-align:center;
    }
    .rata-kiri{
        text-align:left;
    }
    .rata-kanan{
        text-align:right;
    }
    .border{
        border: 1px solid black;
    }
    th{
        border: 2px solid black !important;
        text-align: center !important;
    }
    tr:last-child{
        border-bottom: none !important;
    }
    td{
        border: 2px solid black;
        border-top: none;
        border-bottom: none;
        font-size: 12px;
    }
    tr:last-child{
        border-bottom: 2px solid black !important;
    }
    tr:nth-child(even){
        background-color: #f2f2f2;
    }
    .legend{
        background: white;
        position: absolute;
        margin: -13px 20px;
        z-index: 9999;
    }
</style>
@extends('akuntansi::laporan.laporan_landscape')
@section('title')
    Cetak Jurnal Umum
@endsection
@section('laporan')
<div class="form-group row">
    <div class="col-7">
        <div class="form-group row">
            <div class="col-3"></div>
            <div class="col">
                <div class="card">
                    <div class="row mark">
                        <div class="col-12 label" style="font-size: 15px; font-weight: bolder;">{{ $identitas->nama_perusahaan ?? '-' }}</div>
                    </div>
                    <div class="row">
                        <div class="col-12 label">{{ $identitas->alamat ?? '-' }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-5 rata-tengah">
        <div class="card label">
            <div class="row mark">
                <div class="col rata-tengah">
                    <h4>Jurnal Voucher</h4>
                </div>
            </div>
            <div class="row">
                <div class="col rata-kiri">
                    Date
                    <div class="col rata-kanan">
                        {{ $items['tanggal_jurmum'] ?? '-' }}
                    </div>
                </div>
                <div class="col rata-kiri">
                    Voucher No.
                    <div class="col rata-kanan">
                        {{ $items['no_faktur_jurmum'] ?? '-' }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr class="table-header">
                    <th width="20%">Account No.</th>
                    <th width="20%">Account Name</th>
                    <th widht="20%">Debit</th>
                    <th widht="20%">Credit</th>
                    <th widht="20%">Memo</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cart as $cartPembayaran)
                    <tr>
                        <td width="20%">{{ $cartPembayaran->options->akun ?? '-'}}</td>
                        <td width="20%">{{ $cartPembayaran->name ?? '-'}}</td>
                        <td width="20%">{{ number_format($cartPembayaran->options->debet) ?? '-'}}</td>
                        <td width="20%">{{ number_format($cartPembayaran->options->kredit) ?? '-'}}</td>
                        <td width="20%">{{ $cartPembayaran->options->memo ?? '-'}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<table class="table">
    <tbody>
        <tr style="border:none !important;">
            <td style="border:none !important;">
                <div class="row">
                    <div class="col-7">
                        <div class="form-group row">
                            <div class="col-1">
                                <div class="label" style="max-height: 40px;">Say : </div>
                            </div>
                            <div class="col-11">
                                <div class="card label" style="padding: 10px 20px;min-height: 20px; max-height: 40px; border-radius: 10px 10px 10px 10px!important;">
                                    <span id="terbilang"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <div class="sign-text">Prepared By</div>
                                <div class="border"></div>
                                Date : 
                            </div>
                            <div class="col-3">
                                <div class="sign-text">Reviewed By</div>
                                <div class="border"></div>
                                Date : 
                            </div>
                            <div class="col-3">
                                <div class="sign-text">Approved By</div>
                                <div class="border"></div>
                                Date : 
                            </div>
                        </div>
                    </div>
                    <div class="col-5 rata-tengah">
                        <span class="total-invoice" style="display : none;">{{ $debit == $kredit ? $debit : '-' }}</span>
                        <div class="col pad-text">
                            <div class="card form-group" style="padding : 5px 10px">
                                <div class="row nota-transaksi">
                                    <div class="col-8 rata-kiri">
                                        Debits :
                                    </div>
                                    <div class="col-4 rata-kanan">
                                        <span>{{ $debit ?? 0 }}</span>
                                    </div>
                                </div>
                                <div class="row nota-transaksi">
                                    <div class="col-8 rata-kiri">
                                        Credits :
                                    </div>
                                    <div class="col-4 rata-kanan">
                                        <span>{{ $kredit ?? 0 }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col rata-kiri">
                            <span class="legend">Memo</span>
                            <div class="card" style="height:100px;">
                                <div class="pad-text">
                                    {{ $items['description_jurmum'] ?? '-' }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
@endsection
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{ asset('assets/js/terbilang.js') }}"></script>