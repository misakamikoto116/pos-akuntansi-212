<style>
    .sign-text{
        padding-bottom: 50px;
    }
    .nota-transaksi{
        height: 20px;
        overflow: hidden;
    }
    .pad-text{
        font-size: 15px!important;
        padding: 5px;
    }
    .mark{
        background: none !important;
        border-bottom : 1px solid black;
    }
    .label{
        font-size: 12px;
        margin: 5px 0px;
    }
    .card{
        border: 1px solid black !important; 
        border-radius: 15px!important;
        padding: 0px 15px;
        overflow:hidden;
        vertical-align: middle;
    }
    .radius-top{
        border-radius: 15px 15px 0px 0px !important;
    }
    .radius-bottom{
        border-radius: 0px 0px 15px 15px !important;
    }
    .rata-tengah{
        text-align:center;
    }
    .rata-kiri{
        text-align:left;
    }
    .rata-kanan{
        text-align:right;
    }
    .border{
        border: 1px solid black;
    }
    .table td, .table th{
        padding: 5px !important;
    }
    th{
        border: 2px solid black !important;
        text-align: center !important;
    }
    td{
        border: 2px solid black;
        border-top: none;
        border-bottom: none;
        font-size: 12px;
    }
    tr:last-child{
        border-bottom: 2px solid black !important;
    }
    tr:nth-child(even){
        background-color: #f2f2f2;
    }
    .legend{
        background: white;
        position: absolute;
        margin: -13px 20px;
        z-index: 9999;
    }
</style>
@extends('akuntansi::laporan.laporan_landscape')
@section('title')
    Cetak Pesanan Penjualan
@endsection
@section('laporan')
<div class="form-group row">
    <div class="col-7">
        <div class="form-group row">
            <div class="col-3"></div>
            <div class="col">
                <div class="card">
                    <div class="row mark">
                        <div class="col-12 label" style="font-size: 15px; font-weight: bolder;">{{ $identitas->nama_perusahaan ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-12 label">{{ $identitas->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-2">
                <div class="label pad-text">Order By :</div>
            </div>
            <div class="col">
                <div class="card radius-top">
                    <div class="row">
                        <div class="col-12 label" style="font-size: 15px; font-weight: bolder;">{{ $pelanggan->nama ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-12 label">{{ $pelanggan->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-2">
                <div class="label pad-text">Ship To :</div>
            </div>
            <div class="col">
                <div class="card radius-bottom">
                    <div class="row">
                        <div class="col-12 label" style="font-size: 15px; font-weight: bolder;">{{ $pelanggan_kirim->nama  ?? $pelanggan->nama }}</div>
                    </div>
                    <div class="row">
                        <div class="col-12 label">{{ $pelanggan_kirim->alamat ?? $pelanggan->alamat }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-5 rata-tengah">
        <h4>Sales Order</h4>
        <div class="col pad-text">
            <div class="card label">
                <div class="row mark">
                    <div class="col rata-kiri">
                        SO Date
                        <div class="col rata-kanan">
                            {{ $items['so_date'] ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        SO Number
                        <div class="col rata-kanan">
                            {{ $items['so_number'] ?? null }}
                        </div>
                    </div>
                </div>
                <div class="row mark">
                    <div class="col rata-kiri">
                        Terms
                        <div class="col rata-kanan">
                            {{ $termin }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        FOB
                        <div class="col rata-kanan">
                            @if ($items['fob'] == "0")
                                Shiping Point
                            @elseif($items['fob'] == "1")
                                Destination
                            @elseif($items['fob'] == 'placeholder')
                                {{ null }}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row mark">
                    <div class="col rata-kiri">
                        Ship Via
                        <div class="col rata-kanan">
                            {{ $ship ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Ship Date
                        <div class="col rata-kanan">
                            {{ $items['ship_date'] ?? null }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col rata-kiri">
                        PO. Number
                        <div class="col rata-kanan">
                            {{ $items['po_number'] ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Salesman
                        <div class="col rata-kanan">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr class="table-header">
                    <th width="20%">Item</th>
                    <th width="25%">Item Description</th>
                    <th width="5%">Qty</th>
                    <th width="20%">Unit Price</th>
                    <th width="5%">Disc</th>
                    <th width="10%">Tax</th>
                    <th width="20%">Amount</th>
                </tr>
            </thead>            
            @foreach ($arrayDetailBarang as $detailBarang)
                <tr>
                    <td>{{ $detailBarang['no_item'] }}</td>
                    <td>{{ $detailBarang['item_deskripsi'] }}</td>
                    <td>{{ $detailBarang['qty_produk'] }}</td>
                    <td>{{ $detailBarang['unit_harga_produk'] }}</td>
                    <td>{{ $detailBarang['diskon_produk'] }}</td>
                    <td>
                        @foreach ($detailBarang['kode_pajak'] as $dataPajak)
                            {{ $dataPajak }} &nbsp;
                        @endforeach
                    </td>
                    <td>{{ $detailBarang['amount_produk'] }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<table class="table">
    <tbody>
        <tr style="border:none !important;">
            <td style="border:none !important;">
                <div class="row">
                    <div class="col-7">
                        <div class="form-group row">
                            <div class="col-1">
                                <div class="label" style="max-height: 40px;">Say : </div>
                            </div>
                            <div class="col-11">
                                <div class="card label" style="padding: 0px 20px;min-height: 20px; max-height: 40px; border-radius: 10px 10px 10px 10px!important;">
                                    <span id="terbilang"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <span class="legend">Description</span>
                                <div class="card" style="height:100px;">
                                    <div class="pad-text" style="padding : 10px 10px !important;">
                                        {{ $items['keterangan'] ?? null }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <div class="sign-text">Prepared By</div>
                                <div class="border"></div>
                                Date : 
                            </div>
                            <div class="col-6"></div>
                            <div class="col-3">
                                <div class="sign-text">Approved By</div>
                                <div class="border"></div>
                                Date : 
                            </div>
                        </div>
                    </div>    
                    <div class="col-5 rata-tengah">
                        <div class="col pad-text">
                            <div class="card form-group">
                                <div class="row nota-transaksi">
                                    <div class="col-8 rata-kiri">
                                        Sub Total
                                    </div>
                                    <div class="col-4 rata-kanan">
                                        {{ number_format($items['subTotal']) ?? null }}
                                    </div>
                                </div>
                                <div class="row nota-transaksi">
                                    <div class="col-8 rata-kiri">
                                        Discount
                                    </div>
                                    <div class="col-4 rata-kanan">
                                        {{ $items['total_potongan_rupiah'] ?? null }}
                                    </div>
                                </div>
                            </div>
                            <div class="card form-group">
                                <div class="row nota-transaksi">
                                    <div class="col-8 rata-kiri">
                                        {{ $items['tax_cetak_label0'] ?? null }}
                                    </div>
                                    <div class="col-4 rata-kanan">
                                        {{ number_format($items['tax_cetak0']) ?? null }}
                                    </div>
                                </div>
                                <div class="row nota-transaksi">
                                    <div class="col-8 rata-kiri">
                                        {{ $items['tax_cetak_label1'] ?? null }}
                                    </div>
                                    <div class="col-4 rata-kanan">
                                        {{ number_format($items['tax_cetak1']) ?? null }}
                                    </div>
                                </div>
                            </div>
                            <div class="card form-group">
                                <div class="row nota-transaksi">
                                    <div class="col-8 rata-kiri">
                                        Freight
                                    </div>
                                    <div class="col-4 rata-kanan">
                                        {{ $items['ongkir'] ?? null }}
                                    </div>
                                </div>
                            </div>
                            <div class="card form-group">
                                <div class="row nota-transaksi">
                                    <div class="col-8 rata-kiri">
                                        Total Invoice
                                    </div>
                                    <div class="col-4 rata-kanan">
                                        <span class="total-invoice">{{ number_format($items['grandtotal']) ?? null }}</span>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
@endsection
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{ asset('assets/js/terbilang.js') }}"></script>