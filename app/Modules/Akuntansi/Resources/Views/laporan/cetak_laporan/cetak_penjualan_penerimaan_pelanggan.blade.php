<style>
    .sign-text{
        padding-bottom: 70px;
    }
    .nota-transaksi{
        height: 20px;
        overflow: hidden;
    }
    .pad-text{
        padding: 5px;
        font-size: 15px!important;
    }
    .mark{
        background: none !important;
        border-bottom : 1px solid black;
    }
    .label{
        font-size: 12px;
        margin: 5px 0px;
    }
    .card{
        border: 1px solid black !important; 
        border-radius: 15px!important;
        padding: 0px 15px;
        overflow:hidden;
        vertical-align: middle;
    }
    .radius-top{
        border-radius: 15px 15px 0px 0px !important;
    }
    .radius-bottom{
        border-radius: 0px 0px 15px 15px !important;
    }
    .rata-tengah{
        text-align:center;
    }
    .rata-kiri{
        text-align:left;
    }
    .rata-kanan{
        text-align:right;
    }
    .border{
        border: 1px solid black;
    }
    th{
        border: 2px solid black !important;
        text-align: center !important;
    }
    tr:last-child{
        border-bottom: none !important;
    }
    td{
        border: 2px solid black;
        border-top: none;
        border-bottom: none;
        font-size: 12px;
    }
    tr:last-child{
        border-bottom: 2px solid black !important;
    }
    tr:nth-child(even){
        background-color: #f2f2f2;
    }
    .legend{
        background: white;
        position: absolute;
        margin: -13px 20px;
        z-index: 9999;
    }
</style>
@extends('akuntansi::laporan.laporan_landscape')
@section('title')
    Penjualan Penerimaan Pelanggan
@endsection
@section('laporan')
<div class="form-group row">
    <div class="col-7">
        <div class="form-group row">
            <div class="col-3"></div>
            <div class="col">
                <div class="card">
                    <div class="row mark">
                        <div class="col-12 label" style="font-size: 15px; font-weight: bolder;">{{ $identitas->nama_perusahaan ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-12 label">{{ $identitas->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <div class="label pad-text">Received From :</div>
            </div>
            <div class="col">
                <div class="card radius" style="height:90px;">
                    <div class="row">
                        <div class="col-12 label" style="font-size: 15px; font-weight: bolder;">{{ $pelanggan->nama ?? null }}</div>
                    </div>
                    <div class="row">
                        <div class="col-12 label">{{ $pelanggan->alamat ?? null }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-5 rata-tengah">
        <h4>Cust. Receipt (Sales)</h4>
        <div class="col pad-text">
            <div class="card label">
                <div class="row mark">
                    <div class="col rata-kiri">
                        Payment Date
                        <div class="col rata-kanan">
                            {{ $items['payment_date'] ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Form No.
                        <div class="col rata-kanan">
                            {{ $items['form_no'] ?? null }}
                        </div>
                    </div>
                </div>
                <div class="row mark">
                    <div class="col rata-kiri">
                        Cheque Date
                        <div class="col rata-kanan">
                            {{ $items['cheque_date'] ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Cheque No.
                        <div class="col rata-kanan">
                            {{ $items['cheque_no'] }}
                        </div>
                    </div>
                </div>
                <div class="row mark">
                    <div class="col rata-kiri">
                        Bank
                        <div class="col rata-kanan">
                            {{ $akun->kode_akun ?? null }} &ndash; {{ $akun->nama_akun ?? null }}
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Cheque Amount
                        <div class="col rata-kanan">
                            {{ $items['cheque_amount'] ?? null }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col rata-kiri">
                        Currency
                        <div class="col rata-kanan">
                            IDR
                        </div>
                    </div>
                    <div class="col rata-kiri">
                        Rate
                        <div class="col rata-kanan">
                            {{ $items['rate'] ?? null }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr class="table-header">
                    <th width="15%">No. Invoice</th>
                    <th width="15%">Date</th>
                    <th width="15%">Amount</th>
                    <th width="15%">Owing</th>
                    <th width="15%">Payment Amount</th>
                    <th width="15%">Total Disc.</th>
                </tr>
            </thead>
            @foreach ($arrayFaktur as $faktur)
                <tr>
                    <td>{{ $faktur['no_faktur'] ?? null }}</td>
                    <td>{{ $faktur['tanggal'] ?? null }}</td>
                    <td>{{ $faktur['amount'] ?? null }}</td>
                    <td>{{ $faktur['owing'] ?? null }}</td>
                    <td>{{ $faktur['payment_amount'] ?? null }}</td>
                    <td>{{ $faktur['diskon'] ?? null }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<table class="table">
    <tbody>
        <tr style="border:none !important;">
            <td style="border:none !important;">
                <div class="row">
                    <div class="col-7">
                        <div class="form-group row">
                            <div class="col-1">
                                <div class="label"  style="padding: 25px 0;">Say : </div>
                            </div>
                            <div class="col-11">
                                <div class="card label" style="padding: 4px 20px;height: 60px; border-radius: 10px 10px 10px 10px!important;">
                                    <span id="terbilang"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="height:40px;"></div>
                        <div class="form-group row">
                            <div class="col-3">
                                <div class="sign-text">Prepared By</div>
                                <div class="border"></div>
                                Date : 
                            </div>
                            <div class="col-3">
                                <div class="sign-text">Reviewed By</div>
                                <div class="border"></div>
                                Date : 
                            </div>
                            <div class="col-3">
                                <div class="sign-text">Paid By</div>
                                <div class="border"></div>
                                Date : 
                            </div>
                            <div class="col-3">
                                <div class="sign-text">Received By</div>
                                <div class="border"></div>
                                Date : 
                            </div>    
                            </div>
                        </div>
                    <div class="col-5 rata-tengah">
                        <div class="col pad-text">
                            <div class="card">
                                <div class="row nota-transaksi">
                                    <div class="col-8 rata-kiri">
                                        Total Owing
                                    </div>
                                    <div class="col-4 rata-kanan">
                                        {{ number_format($items['tot_owing']) ?? null }}
                                    </div>
                                </div>
                                <div class="row nota-transaksi">
                                    <div class="col-8 rata-kiri">
                                        Total Discount
                                    </div>
                                    <div class="col-4 rata-kanan">
                                        {{ number_format($items['tot_diskon']) ?? null }}
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="row nota-transaksi">
                                    <div class="col-8 rata-kiri">
                                        Total Payment
                                    </div>
                                    <div class="col-4 rata-kanan">
                                        <span class="total-invoice">{{ $items['cheque_amount'] ?? null }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="card form-group">
                                <div class="row nota-transaksi">
                                    <div class="col-8 rata-kiri">
                                        Overpay
                                    </div>
                                    <div class="col-4 rata-kanan">
                                        0
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col">
                                    <span  style="text-align:left!important;">Description</span>
                                    <div class="card" style="height:100px;">
                                        <div class="pad-text rata-kiri">
                                            {{ $items['memo'] ?? null }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
@endsection
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{ asset('assets/js/terbilang.js') }}"></script>