<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title', chichi_title ())</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <meta name="csrf-token" content="{{ csrf_token() }}">        
        <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/papper.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/laporan.css')}}" rel="stylesheet" type="text/css" />
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body class="container">
        <nav>
            <div class="row">
                <div class="col-8"></div>
                <div class="col-1">
                    <button class="btn btn-sm btn-success btn-filter"><i class="ti-filter"> Filter</i></button>
                </div>
                <div class="col-1">
                    <button class="btn btn-sm btn-warning" onclick="refreshPage()"><i class="ti-reload"> Refresh</i></button>
                </div>
                <div class="col-1">
                    <button class="btn btn-sm btn-info save-page" ><i class="ti-save"> Save</i></button>
                </div>
                <div class="col-1">
                    <button class="btn btn-sm btn-info" onclick="printPage()"><i class="ti-printer"> Print</i></button>
                </div>
            </div>
        </nav>
        <header></header>
        {{-- <img class="watermark" src="{{asset('assets/images/chichi_render.png')}}"> --}}
         	@yield('laporan_khusus')
        <footer>
            <label style="margin: 5px 20px;">
                @yield('title')
                {{date('Y-m-d \ H:i:s')}}
            </label>
        </footer>
        <div id="laporan_general_filter" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Filter Laporan</h4>
                    </div>
                    <form id="form_filter_laporan" method="get">

                    <div class="modal-body">
                        @if(isset($formFilter))
                            @include($formFilter)
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Lanjutkan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        @yield('custom_js')
    </body>
</html>
<script type="text/javascript">
    $('.btn-filter').on('click', function (e) {
        e.preventDefault();
        $("#form_filter_laporan").attr('action', '{{ request()->url() }}');
        $('#laporan_general_filter').modal('show');
    });

    $(document).ready(function($) {
        var pelanggan_transaksi = GetURLParameter('pelanggan_transaksi');
        var pemasok_transaksi   = GetURLParameter('pemasok_transaksi');

        if (pelanggan_transaksi) {
            $("#pelanggan_transaksi").prop('checked', true);
        }else {
            $("#pelanggan_transaksi").prop('checked', false);
        }

        if (pemasok_transaksi) {
            $("#pemasok_transaksi").prop('checked', true);
        }else {
            $("#pemasok_transaksi").prop('checked', false);
        }
    });

    function GetURLParameter(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return true;
                }
            }
    }
</script>