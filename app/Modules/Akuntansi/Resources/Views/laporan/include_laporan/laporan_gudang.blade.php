<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Gudang</h1>
        <ul class="list-group">
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinmts"><i class="fa fa-tags"></i> Rincian Mutasi</a></li>
            <div id="rinmts" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#mutasi_perbarang_pergudang">Mutasi Barang per Gudang</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#mutasi_pergudang_perbarang">Mutasi Gudang per Gudang</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#ringkasan_mutasi_gudang_perbarang">Ringkasan Mutasi per Gudang per Barang</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#kuantitas_barang_per_daftar_gudang">Kuantitas Barang per Daftar Gudang</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#lembar_perhitungan_stok_fisik_barang">Lembar Perhitungan Stok Fisik Barang</a></li>
                </ul>
            </div>
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinns"><i class="fa fa-tags"></i> Rincian Nomor Serial</a></li>
            <div id="rinns" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#">Penerimaan Nomor Seri/Produksi</a></li>
                    <li class="list-group-item"><a href="#">Nomor Seri/Produksi Terkirim</a></li>
                    <li class="list-group-item"><a href="#">Nomor Seri/Produksi Tersedia</a></li>
                    <li class="list-group-item"><a href="#">Histori Nomor Seri</a></li>
                    <li class="list-group-item"><a href="#">Nomor Seri per Gudang</a></li>
                    <li class="list-group-item"><a href="#">Histori Barang Rusak</a></li>
                    <li class="list-group-item"><a href="#">Analisa Barang Rusak</a></li>
                    <li class="list-group-item"><a href="#">Umur Kadaluarsa Barang</a></li>
                    <li class="list-group-item"><a href="#">Rincian Umur Kadaluarsa Barang</a></li>
                    <li class="list-group-item"><a href="#">Laporan Selisih antara Kuantitas Barang dan Kuantitas Barang dan Nomor Seri</a></li>
                </ul>
            </div>
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#ringdn"><i class="fa fa-tags"></i> Rincian Gudang</a></li>
            <div id="ringdn" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/gudang/daftar-gudang') }}" target="_blank">Daftar Gudang</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_daftar_perpindahan_barang">Rincian Daftar Perpindahan Barang</a></li>
                </ul>
            </div>
        </ul>
    </div>
</section>

{{-- Modal Rincian Daftar Perpindahan Barang --}}
<div id="rincian_daftar_perpindahan_barang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Daftar Perpindahan Barang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/gudang/rincian-daftar-perpindahan-barang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Mutasi Barang per Gudang --}}
<div id="mutasi_perbarang_pergudang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Mutasi Barang per Gudang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/gudang/mutasi-perbarang-pergudang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Mutasi Gudang per Gudang --}}
<div id="mutasi_pergudang_perbarang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Mutasi Gudang per Gudang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/gudang/mutasi-pergudang-perbarang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Ringkasan Mutasi per Gudang per Barang --}}
<div id="ringkasan_mutasi_gudang_perbarang" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Mutasi per Gudang per Barang</h4>
        </div>
            {!! Form::open(['url' => url('akuntansi/laporan/gudang/ringkasan-mutasi-gudang-perbarang'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                    </div>
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>

{{-- Modal Kuantitas Barang per Daftar Gudang --}}
<div id="kuantitas_barang_per_daftar_gudang" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pilih Tanggal Untuk Laporan Kuantitas Barang per Daftar Gudang</h4>
        </div>
            {!! Form::open(['url' => url('akuntansi/laporan/gudang/kuantitas-barang-per-daftar-gudang'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                    </div>
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>

{{-- Modal Lembar Perhitungan Stok Fisik Barang --}}
<div id="lembar_perhitungan_stok_fisik_barang" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pilih Tanggal Untuk Laporan Lembar Perhitungan Stok Fisik Barang</h4>
        </div>
            {!! Form::open(['url' => url('akuntansi/laporan/gudang/lembar-perhitungan-stok-fisik-barang'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                    </div>
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>