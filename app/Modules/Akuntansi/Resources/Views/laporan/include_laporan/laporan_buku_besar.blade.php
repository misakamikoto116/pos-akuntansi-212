<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Buku Besar</h1>
        <ul class="list-group">
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#jurnal"><i class="fa fa-tags"></i> Ringkasan Jurnal</a></li>
        <div id="jurnal" class="collapse">
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ url('akuntansi/laporan/buku-besar/daftar-histori-gl') }}" data-toggle="modal" data-target="#histori_gl">Daftar Histori GL</a></li>
                <li class="list-group-item"><a href="{{ url('akuntansi/laporan/buku-besar/keseluruhan-jurnal') }}" data-toggle="modal" data-target="#keseluruhan_jurnal">Keseluruhan Jurnal</a></li>
                <li class="list-group-item"><a href="{{ url('akuntansi/laporan/buku-besar/ringkasan-buku-besar') }}">Ringkasan Buku Besar</a></li>
                <li class="list-group-item"><a href="{{ url('akuntansi/laporan/buku-besar/buku-besar-rinci') }}" data-toggle="modal" data-target="#buku_besar_rinci">Buku Besar - Rinci</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#neraca_saldo">Neraca Saldo</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#neraca_saldo_klasik">>Neraca Saldo (Klasik)</a></li>
                <li class="list-group-item"><a href="{{ url('akuntansi/laporan/buku-besar/untung-rugi-realisir') }}">Untung/Rugi ter-realisir</a></li>
                <li class="list-group-item"><a href="{{ url('akuntansi/laporan/buku-besar/untung-rugi-nonrealisir') }}">Untung/Rugi tidak ter-realisir</a></li>
            </ul>
        </div>
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#buktijurnal"><i class="fa fa-tags"></i> Bukti Jurnal Umum</a></li>
        <div id="buktijurnal" class="collapse">
            <ul class="list-group">
                <li class="list-group-item"><a href="#">Daftar Akun</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#bukti_jurnal_umum">Bukti Jurnal Umum</a></li>
            </ul>
        </div>
        </ul>
    </div>
</section>

{{-- Modal --}}
<div id="keseluruhan_jurnal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Keseluruhan Jurnal</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/buku-besar/keseluruhan-jurnal'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Daftar Histori GL --}}
<div id="histori_gl" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Daftar Histori GL</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/buku-besar/daftar-histori-gl'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-histori-gl"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-histori-gl']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Buku Besar Rinci --}}
<div id="buku_besar_rinci" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Keseluruhan Jurnal</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/buku-besar/buku-besar-rinci'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Neraca Saldo --}}
<div id="neraca_saldo" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Neraca Saldo</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/buku-besar/neraca-saldo'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Bukti jurnal umum --}}
<div id="bukti_jurnal_umum" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Bukti Jurnal Umum</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/buku-besar/bukti-jurnal-umum'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-bukti-jurnal-umum"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-bukti-jurnal-umum']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Neraca Saldo (Klasik) --}}
<div id="neraca_saldo_klasik" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pilih Tanggal Untuk Laporan Neraca Saldo (Klasik)</h4>
        </div>
            {!! Form::open(['url' => url('akuntansi/laporan/buku-besar/neraca-saldo-klasik'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-bukti-jurnal-umum"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                    </div>
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
    </div>