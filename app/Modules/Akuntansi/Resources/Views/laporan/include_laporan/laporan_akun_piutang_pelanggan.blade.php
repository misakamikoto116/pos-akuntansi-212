<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Akun Piutang & Pelanggan</h1>
        <ul class="list-group">
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#piutang"><i class="fa fa-tags"></i> Rincian Piutang</a></li>
        <div id="piutang" class="collapse">
            <ul class="list-group">
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#faktur_belum_lunas">Faktur Belum Lunas</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#ringkasan_umur_piutang">Ringkasan Umur Piutang</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_umur_piutang">Rincian Umur Piutang</a></li>
                <li class="list-group-item"><a href="#">Grafik Umur Piutang</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#ringkasan_buku_besar_pembantu_piutang">Ringkasan Buku Besar Pembantu Piutang</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_buku_besar_pembantu_piutang">Rincian Buku Besar Pembantu Piutang</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#histori_piutang_pelanggan">Histori Piutang Pelanggan</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#ringkasan_pembayaran_faktur">Ringkasan Pembayaran Faktur</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_pembayaran_faktur">Rincian Pembayaran Faktur</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#laporan_pelanggan">Laporan Pelanggan</a></li>
            </ul>
        </div>
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#grafikpenagihan"><i class="fa fa-tags"></i> Grafik Penagihan</a></li>
        <div id="grafikpenagihan" class="collapse">
            <ul class="list-group">
                <li class="list-group-item"><a href="#">Grafik Penarikan per Periode</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penagihan_per_pelanggan">Penagihan per Pelanggan</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penagihan_per_tipe_pelanggan">Penagihan per Tipe Pelanggan</a></li>
            </ul>
        </div>
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#datapelanggan"><i class="fa fa-tags"></i> Data Pelanggan</a></li>
        <div id="datapelanggan" class="collapse">
            <ul class="list-group">
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#daftar_pelanggan">Daftar Pelanggan</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#ringkasan_penerimaan_penjualan">Ringkasan Penerimaan Penjualan</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_penerimaan_penjualan">Rincian Penerimaan Penjualan</a></li>
            </ul>
        </div>
        </ul>
    </div>
</section>

{{-- Modal Faktur Belum Lunas --}}
<div id="faktur_belum_lunas" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Faktur Belum Lunas</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/faktur-belum-lunas'), 'method' => 'get', 'target' => '_blank']) !!}
      
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Per Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Umur Piutang --}}
<div id="rincian_umur_piutang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Umur Piutang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/rincian-umur-piutang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">  
            <div class="form-group row">
                {!! Form::label('Per Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Ringkasan Umur Piutang --}}
<div id="ringkasan_umur_piutang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Umur Piutang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/ringkasan-umur-piutang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Per Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Daftar Pelanggan --}}
<div id="daftar_pelanggan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Daftar Pelanggan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/daftar-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Per Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Ringkasan Penerimaan Penjualan --}}
<div id="ringkasan_penerimaan_penjualan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Penerimaan Penjualan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/ringkasan-penerimaan-penjualan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-rincian-buku-besar']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Penerimaan Penjualan --}}
<div id="rincian_penerimaan_penjualan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Penerimaan Penjualan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/rincian-penerimaan-penjualan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-rincian']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Buku Besar Pembantu Piutang --}}
<div id="rincian_buku_besar_pembantu_piutang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Buku Besar Pembantu Piutang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/rincian-buku-besar-pembantu-piutang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-rincian-buku-besar']) !!}          
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <div>
                    <input name="pelanggan_transaksi"  id="pelanggan_transaksi" type="checkbox">
                    <label for="pelanggan_transaksi">
                        Hanya menampilkan pelanggan mempunyai transaksi
                    </label>
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Ringkasan Buku Besar Pembantu Piutang --}}
<div id="ringkasan_buku_besar_pembantu_piutang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Buku Besar Pembantu Piutang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/ringkasan-buku-besar-pembantu-piutang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-rincian-buku-besar']) !!}          
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <div>
                    <input name="pelanggan_transaksi"  id="pelanggan_transaksi" type="checkbox">
                    <label for="pelanggan_transaksi">
                        Hanya menampilkan pelanggan mempunyai transaksi
                    </label>
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Pembayaran Faktur --}}
<div id="rincian_pembayaran_faktur" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Pembayaran Faktur</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/rincian-pembayaran-faktur'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-pembayaran-faktur"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-rincian-pembayaran-faktur']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Pembayaran Faktur --}}
<div id="ringkasan_pembayaran_faktur" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Pembayaran Faktur</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/ringkasan-pembayaran-faktur'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-ringkasan-pembayaran-faktur"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-ringkasan-pembayaran-faktur']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Histori Piutang Pelanggan --}}
<div id="histori_piutang_pelanggan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Histori Piutang Pelanggan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/histori-piutang-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-histori-piutang-pelanggan"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-histori-piutang-pelanggan']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Laporan Pelanggan --}}
<div id="laporan_pelanggan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pelanggan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/laporan-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-laporan-pelanggan"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-laporan-pelanggan']) !!}          
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <div>
                    <input name="pelanggan_transaksi"  id="pelanggan_transaksi" type="checkbox">
                    <label for="pelanggan_transaksi">
                        Hanya menampilkan pelanggan mempunyai transaksi
                    </label>
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Laporan Penagihan Per Pelanggan --}}
<div id="penagihan_per_pelanggan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penagihan Per Pelanggan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/penagihan-per-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::text('start-date-rincian', null, ['class' => 'form-control month-only','onchange' => 'var end = document.getElementById("end-date-penagihan-per-pelanggan"); end.setAttribute("min", this.value); end.value = (this.value);', 'autocomplete' => 'off']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::text('end-date-rincian', null, ['class' => 'form-control month-only','id' => 'end-date-penagihan-per-pelanggan', 'autocomplete' => 'off']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Laporan Penagihan Per Tipe Pelanggan --}}
<div id="penagihan_per_tipe_pelanggan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penagihan Per Tipe Pelanggan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-piutang-pelanggan/penagihan-per-tipe-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::text('start-date-rincian', null, ['class' => 'form-control month-only','onchange' => 'var end = document.getElementById("end-date-penagihan-per-tipe-pelanggan"); end.setAttribute("min", this.value); end.value = (this.value);', 'autocomplete' => 'off']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::text('end-date-rincian', null, ['class' => 'form-control month-only','id' => 'end-date-penagihan-per-tipe-pelanggan', 'autocomplete' => 'off']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>