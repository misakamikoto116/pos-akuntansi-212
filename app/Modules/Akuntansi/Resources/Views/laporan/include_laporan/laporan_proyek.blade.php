<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Proyek</h1>
        <ul class="list-group">
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinproyek"><i class="fa fa-tags"></i> Rincian Proyek</a></li>
            <div id="rinproyek" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#">Daftar Proyek</a></li>
                    <li class="list-group-item"><a href="#">Histori Akun Proyek</a></li>
                    <li class="list-group-item"><a href="#">Histori Pembukuan Proyek</a></li>
                    <li class="list-group-item"><a href="#">Laba & Rugi per Proyek</a></li>
                    <li class="list-group-item"><a href="#">Laba/Rugi (Multi Periode) per Proyek</a></li>
                    <li class="list-group-item"><a href="#">Profit Lost (Multi Periode) per Proyek</a></li>
                    <li class="list-group-item"><a href="#">Lembar Kerja Induk Proyek</a></li>
                </ul>
            </div>
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinrangkumanproyek"><i class="fa fa-tags"></i> Rangkuman Proyek</a></li>
            <div id="rinrangkumanproyek" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#">Rangkuman Proyek</a></li>
                    <li class="list-group-item"><a href="#">Rencana Anggaran Pelaksanaan</a></li>
                    <li class="list-group-item"><a href="#">Anggaran Proyek per Pekerjaan</a></li>
                    <li class="list-group-item"><a href="#">Anggaran dan Biaya Aktual</a></li>
                    <li class="list-group-item"><a href="#">Arus Kas per Proyek</a></li>
                    <li class="list-group-item"><a href="#">Arus Kas Sebenarnya per Proyek</a></li>
                    <li class="list-group-item"><a href="#">Perbandingan Alur Kas Proyek</a></li>
                    <li class="list-group-item"><a href="#">Estimasi per Proyek (Kurva-S)</a></li>
                    <li class="list-group-item"><a href="#">Biaya Upah Proyek</a></li>
                    <li class="list-group-item"><a href="#">Histori Proyek Kontraktor</a></li>
                </ul>
            </div>
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#ringkasnproyek"><i class="fa fa-tags"></i> Ringkasan Proyek</a></li>
            <div id="ringkasnproyek" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#">Saldo Awal Proyek</a></li>
                    <li class="list-group-item"><a href="#">Anggaran Proyek (Ringkasan)</a></li>
                    <li class="list-group-item"><a href="#">Anggaran Proyek (Rinci)</a></li>
                    <li class="list-group-item"><a href="#">Analisa Anggaran Proyek</a></li>
                </ul>
            </div>
        </ul>
    </div>
</section>