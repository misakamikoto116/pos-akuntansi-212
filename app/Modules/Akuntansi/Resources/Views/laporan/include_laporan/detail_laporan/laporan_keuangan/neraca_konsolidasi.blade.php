@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Neraca Konsolidasi
@endsection
@section('laporan')
    <div class="row">
        <div class="col col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="3">
                            <p><h4>CHICHI<h4></p>
                            <p><h6>Neraca (Konsolidasi)</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Per Tangal <!-- dari Bulan --></p>
                        </td>
                    </tr>
                    <tr>
                        <th align="center">Keterangan</th>
                        <th align="center">Chichi Jaya</th>
                        <th align="center">Jumlah</th>
                    </tr>
                </thead>
                <tr>
                    <td colspan="3">Aktiva</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;Aktiva Lancar</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kas dan Bank</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Kas dan Bank</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Piutang Dagang</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Piutang Dagang</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Persediaan</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Persediaan</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aktiva Lancar Lainnya</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Aktiva Lancar Lainnya</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Jumlah Aktiva Lancar</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;Aktiva Tetap</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nilai Histori</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Nilai Histori</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Akumulasi Penyusutan</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Akumulasi Penyusutan</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Jumlah Aktiva Tetap</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;Order Assets</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Jumlah Otder Assets</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td>Jumlah Aktiva</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td colspan="3">Kewajiban dan Ekuitas</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;Kewajiban</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kewajiban Lancar</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hutang Dagang</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Hutang Dagang</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kewajiban Lancar Lain</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Kewajiban Lancar Lain </td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Kewajiban Lancar</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kewajiban Jangka Panjang</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Kewajiban Jangka Panjang</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Jumlah Kewajiban</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;Ekuitas</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Jumlah Ekuitas</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td>Jumlah Kewajiban dan Ekuitas</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
            </table>
        </div>
    </div>
@endsection
