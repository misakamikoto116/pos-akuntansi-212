@extends('akuntansi::laporan.laporan')

@section('title')
    {{ $title }}
@endsection

@section('stylesheet')

@endsection

@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                </thead>
                <tbody>                    
                    <tr>
                        <td>
                            <div id="chart_prestasi_penjualan_barang">
                                <p class="labels"></p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        var data  = {!! $item !!};
        var morrisData = [];
        var randomWarna = [];

        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        $.each(data, function(key, value){
            var aAja = getRandomColor();
            morrisData.push({
                key        : key !== undefined ? key : ' ',
                value0     : value[0] !== undefined ? value[0] : 0,
                value1     : value[2] !== undefined ? value[2] : 0,
                value2     : value[4] !== undefined ? value[4] : 0,
                value3     : value[6] !== undefined ? value[6] : 0,
                value4     : value[8] !== undefined ? value[8] : 0,
                pelanggan0 : value[1] !== undefined ? value[1] : '-',
                pelanggan1 : value[3] !== undefined ? value[3] : '-',
                pelanggan2 : value[5] !== undefined ? value[5] : '-',
                pelanggan3 : value[7] !== undefined ? value[7] : '-',
                pelanggan4 : value[9] !== undefined ? value[9] : '-',
            });
            randomWarna.push( aAja );
        });
    
        Morris.Bar({
            element : 'chart_prestasi_penjualan_barang',
            data    : morrisData,
            xkey    : 'key',
            ykeys   : ['value0', 'value1', 'value2', 'value3', 'value4'],
            labels  : ['pelanggan0', 'pelanggan1', 'pelanggan2', 'pelanggan3', 'pelanggan4'],
            hoverCallback:  function (index, options, content, row) {
                                var x = $('.labels').html(
                                    "<p>"+ row.key +"</p>" +
                                    "<p>"+ row.pelanggan0 +"    :   "+ row.value0+"</p>" +
                                    "<p>"+ row.pelanggan1 +"    :   "+ row.value1+"</p>" +
                                    "<p>"+ row.pelanggan2 +"    :   "+ row.value2+"</p>" +
                                    "<p>"+ row.pelanggan3 +"    :   "+ row.value3+"</p>" +
                                    "<p>"+ row.pelanggan4 +"    :   "+ row.value4+"</p>"
                                );
                                return x;
                            },
            parseTime: false,
        });
  </script>
@endsection
