@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Barang</th>
                        <th>Deskripsi Barang</th>
                        <th>Opening Balance</th>
                        <th>Kts. Masuk</th>
                        <th>Kts. Keluar</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=6 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia.
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($item as $dataGudang)
                            <tr>
                                <td colspan=6><b>{{     $dataGudang['nama_gudang']     }}</b></td>
                            </tr>
                            @foreach($dataGudang['data_barang'] as $dataBarang)
                                <tr>
                                    <td style="padding-left:25px;">
                                        {{$dataBarang['kode_barang']}}
                                    </td>
                                    <td>{{$dataBarang['nama_barang']}}</td>
                                    <td>{{$dataBarang['saldo_awal']}}</td>
                                    <td>{{$dataBarang['sum_kts_masuk']}}</td>
                                    <td>{{$dataBarang['sum_kts_keluar']}}</td>
                                    <td>{{$dataBarang['balance']}}</td>
                                </tr>
                            @endforeach
                        @endforeach
                        <tr>
                            <td colspan=2></td>
                            <td class="border-top">{{$dataGudang['sum_total_saldo_awal']}}</td>
                            <td class="border-top">{{$dataGudang['sum_total_kts_masuk']}}</td>
                            <td class="border-top">{{$dataGudang['sum_total_kts_keluar']}}</td>
                            <td class="border-top">{{$dataGudang['sum_total_balance']}}</td>
                        </tr>
                    @endif
                </tbody>                
            </table>
        </div>
    </div>
@endsection