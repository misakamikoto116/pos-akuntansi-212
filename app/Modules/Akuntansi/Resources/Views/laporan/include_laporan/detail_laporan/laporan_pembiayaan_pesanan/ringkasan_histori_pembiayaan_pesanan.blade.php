@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Ringkasan Histori Pembiayaan Pesanan
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12" align="center">
            <p><h4>CHICHI</h4></p>
            <p><h6>Ringkasan Histori Pembiayaan Pesanan</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>Dari 01 Mei 2018 ke 30 Mei 2018</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table">
                <tr>
                    <td>Tanggal Pemakaian</td>
                    <td>Detail Status</td>
                    <td>Nilai Barang</td>
                    <td>Nilai Beban</td>
                    <td>Total Pembiayaan Pesanan</td>
                </tr>
                <!-- Perulangan Ringkasan Histori Pembiayaan Pesanan -->
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                <!-- Akhir Perulangan Ringkasan Histori Pembiayaan Pesanan -->
            </table>
        </div>
    </div>
@endsection