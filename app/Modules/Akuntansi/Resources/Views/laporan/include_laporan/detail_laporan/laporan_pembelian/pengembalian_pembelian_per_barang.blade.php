@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan='6'>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Faktur</th>
                        <th>Tanggal Faktur</th>
                        <th>Katerangan</th>
                        <th>Kuantitas</th>
                        <th>Satuan</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=6 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php $totalQty = 0; $totalHarga = 0; @endphp
                    @foreach($item as $item_key => $dataBarangRetur)
                        <tr>
                            <td>{{ $item_key }}</td>
                            <td>{{ $dataBarangRetur[0]['namabarang'] }}</td>
                        </tr>
                        @php $subTotalQty = 0; $subTotalHarga = 0; @endphp
                        @foreach($dataBarangRetur as $dataBarang)
                            <tr>
                                <td>{{ $dataBarang['no_retur'] }}</td>
                                <td>{{ $dataBarang['tanggal_retur'] }}</td>
                                <td>{{ $dataBarang['keterangan'] }}</td>
                                <td>{{ $dataBarang['qty'] }}</td>
                                <td>{{ $dataBarang['unit'] }}</td>
                                <td>{{ number_format($dataBarang['total']) }}</td>
                            </tr>
                            @php $subTotalQty += $dataBarang['qty']; $subTotalHarga += $dataBarang['total'] @endphp
                        @endforeach
                        <tr>
                            <td colspan="3"></td>
                            <td class="border-top">{{ number_format($subTotalQty) }}</td>
                            <td></td>
                            <td class="border-top">{{ number_format($subTotalHarga) }}</td>
                        </tr>
                        @php $totalQty += $subTotalQty; $totalHarga += $subTotalHarga; @endphp
                    @endforeach
                    <tr>
                        <td colspan="3"></td>
                        <td class="border-top">{{ number_format($totalQty) }}</td>
                        <td></td>
                        <td class="border-top">{{ number_format($totalHarga) }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection