@extends('akuntansi::laporan.laporan_khusus')
@section('title')
    {{ $title }}
@endsection
<style type="text/css">
    .td-blue-left-bold {
        text-align: left;
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
    }
    .td-blue-bold {
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
        text-align: left;
    }
</style>
@section('laporan_khusus')
        <section class="sheet padding-0mm">
            <article>
                <div class="row">
                    <div class="col-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td colspan=6>
                                        <p><h4>{{ $perusahaan }}</h4></p>
                                        <p><h6>{{ $title }}</h6></p>
                                        <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                                        <p>Dari {{ $tanggal_awal }} ke {{ $tanggal_akhir }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td-blue-left-bold">Nama Kasir</td>
                                    <td class="td-blue-bold">: {{ $nama ?? null }}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>No Penjualan</th>
                                    <th>No Barang</th>
                                    <th>Nama Barang</th>
                                    <th style="text-align: right;">Harga</th>
                                    <th style="text-align: right;">Qty</th>
                                    <th style="text-align: right;">Total</th>
                                </tr>
                            </thead>
                            <!-- Perulangan Laporan Pelanggan -->
                                @foreach ($items as $data_faktur)
                                    <tr>
                                        <td>{{ $data_faktur['tanggal'] }}</td>
                                        <td>{{ $data_faktur['no_faktur'] }}</td>
                                        <td colspan="5">{{ $data_faktur['status'] }}</td>
                                    </tr>
                                    @foreach ($data_faktur['barang'] as $barang)
                                        <tr>
                                            <td colspan="2"></td>
                                            <td>{{ $barang['barcode_barang'] }}</td>
                                            <td>{{ $barang['nama_barang'] }}</td>
                                            <td style="text-align: right";>{{ $barang['harga'] }}</td>
                                            <td style="text-align: right";>{{ $barang['qty'] }}</td>
                                            <td style="text-align: right";>{{ $barang['total'] }}</td>
                                        </tr>
                                    @endforeach
                                <!-- Akhir Perulangan Laporan Pelanggan -->
                                    <tr>
                                        <td colspan="6"></td>
                                        <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">{{ $data_faktur['total'] }}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </article>
        </section>
@endsection