@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12" align="center">
            <p><h4>{{ $nama_perusahaan ?? null }}</h4></p>
            <p><h6>{{ $title }}</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>Period {{ $bulan_awal }} to {{ $bulan_akhir }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nama Pelanggan</th>
                        <th>Hari Berbayar</th>
                        <th>Syarat Bayar</th>
                        <th>Nunggak</th>
                    </tr>
                </thead>
                <!-- Perulangan Daftar Pelanggan -->
                    @foreach ($items as $item)
                        <tr>
                            <td>{{ $item['nama_pelanggan'] ?? null }}</td>
                            <td>{{ $item['hari_bayar'] ?? null }}</td>
                            <td>{{ $item['syarat_bayar'] ?? null }}</td>
                            <td>{{ $item['nunggak'] ?? null }}</td>
                        </tr>
                    @endforeach
                <!-- Akhir Perulangan Daftar Pelanggan -->
                    <tr>
                        <td colspan="1"></td>
                        <td class="border-top">{{ $sum_hari_bayar ?? null }}</td>
                        <td class="border-top">{{ $sum_syarat_bayar ?? null }}</td>
                        <td class="border-top">{{ $sum_nunggak ?? null }}</td>
                    </tr>
                <!-- Akhir Perulangan Mata Uang -->
            </table>
        </div>
    </div>
@endsection