@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12" align="center">
            <p><h4>{{ $perusahaan }}</h4></p>
            <p><h6>{{ $title }}</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>{{ $subTitle }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table">
                <tr>
                    <th>Nama Pemasok</th>
                    @foreach($dates as $item)
                        <th>{{ $item['tgl'] }} </th>
                    @endforeach
                </tr>
            </table>
        </div>
    </div>
@endsection