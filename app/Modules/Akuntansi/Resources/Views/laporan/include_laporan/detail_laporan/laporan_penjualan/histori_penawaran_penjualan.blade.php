@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4>{{ $perusahaan }}</h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b> {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tipe Proses</th>
                        <th>No. Faktur</th>
                        <th>Tanggal Faktur</th>
                        <th>Kuantitas Faktur</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=4 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    {{ $subTitle   }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($item as $item_key => $dataHistoriPenawaranPenjualan)
                            <tr>    
                                <td>{{ $dataHistoriPenawaranPenjualan[0]['nopelanggan']       ??  null    }}</td>
                                <td>{{ $dataHistoriPenawaranPenjualan[0]['tglpenawaran']    ??  null    }}</td>
                                <td>{{ $item_key }}</td>
                                <td>@if($dataHistoriPenawaranPenjualan[0]['status'] == 2) Terproses @endif</td>
                            </tr>
                            @foreach($dataHistoriPenawaranPenjualan as $dataHistoriPenawaranPenjualan_key => $dataHistoriPenawaran)
                                
                                <tr>
                                    <td style="padding: 12px 25px !important;">{{ $dataHistoriPenawaran['nobarang']   ??  null }}</td>
                                    <td>{{ $dataHistoriPenawaran['nmbarang']       ??  null    }}</td>
                                    <td></td>
                                    <td>
                                        {{ $dataHistoriPenawaran['qty'] ?? null }} / {{ $dataHistoriPenawaran['unit']     ??   null }}
                                        <div style="float:right;">0<div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><div style="float: right;">Pesanan Penjualan</div></td>
                                    <td>{{ $dataHistoriPenawaran['nopesanan']   ??  null  }}</td>
                                    <td>{{ $dataHistoriPenawaran['tglpesanan']  ??  null  }}</td>
                                    <td><div style="float:right">{{ $dataHistoriPenawaran['qty'] ?? null }}</div></td>
                                </tr>
                                <tr>
                                    <td colspan=3></td>
                                    <td class="border-top" align="right">{{ $dataHistoriPenawaran['qty'] ?? null }}</td>
                                </tr>
                                <tr>
                                    <td colspan=4>&nbsp;</td>
                                </tr>
                            @endforeach
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection