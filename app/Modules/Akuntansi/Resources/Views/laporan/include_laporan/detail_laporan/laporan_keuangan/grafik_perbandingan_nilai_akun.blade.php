@extends('akuntansi::laporan.laporan')

@section('title')
    {{ $title }}
@endsection

@section('stylesheet')

@endsection

@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4> {{ $nama_perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            {{-- <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p> --}}
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div id="chart_grafik_perbandingan_akun">
                                <p class="labels"></p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        var data  = {!! $item !!};
        var morrisData = [];
        var randomWarna = [];
        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
        $.each(data, function(key, value){
            var aAja = getRandomColor();
            morrisData.push({
                key     : value['nama_akun'] !== undefined ? value['nama_akun'] : ' ',
                saldo   : value['saldo'] !== undefined ? value['saldo'] : 0,
            });
            randomWarna.push( aAja );
        });
        Morris.Bar({
            element : 'chart_grafik_perbandingan_akun',
            data    : morrisData,
            xkey    : 'key',
            ykeys   : ['saldo'],
            labels  : ['saldo'],
            barColors: function (row, series, type) {
                    return randomWarna[row.x];
            }, 
            hoverCallback:  function (index, options, content, row) {
                                var x = $('.labels').html(
                                    "<p>"+ row.key +"    :   "+ row.saldo+"</p>"
                                );
                                return x;
                            },
            parseTime: false,
        });
  </script>
@endsection
