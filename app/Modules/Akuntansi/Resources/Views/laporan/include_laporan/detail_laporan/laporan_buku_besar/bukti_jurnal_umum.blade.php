@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=7>
                            <p><h4>{{ $nama_perusahaan ?? null }}</h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari Tanggal {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir ?? null }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tgl JV</th>
                        <th>No. Akun</th>
                        <th>Nama Akun</th>
                        <th>Debit</th>
                        <th>Kredit</th>
                        <th>Nama Dep</th>
                    </tr>
                </thead>
                <!-- Perulangan Tipe Mata Uang-->

                    @php
                        $sum_jurnal_all_debet = 0; 
                        $sum_jurnal_all_kredit = 0; 
                    @endphp 

                    @foreach ($items as $data_jurnal_umum)

                        @php
                            $sum_jurnal_debet   = 0; 
                            $sum_jurnal_kredit  = 0; 
                        @endphp

                        <tr>
                            <td><strong>{{ Carbon\Carbon::parse($data_jurnal_umum->tanggal)->format('d F Y') ?? null }}</strong></td>
                            <td><strong>{{ $data_jurnal_umum->no_faktur ?? null }}</strong></td>
                            <td colspan="5"><strong>{{ $data_jurnal_umum->description ?? null }}</strong></td>
                        </tr>

                        @foreach ($data_jurnal_umum->detailJurnalUmum as $detail_jurnal_umum)

                            @php
                                $sum_jurnal_debet       += $detail_jurnal_umum->debit;
                                $sum_jurnal_kredit      += $detail_jurnal_umum->kredit;

                                $sum_jurnal_all_debet   += $detail_jurnal_umum->debit;
                                $sum_jurnal_all_kredit  += $detail_jurnal_umum->kredit;
                            @endphp

                            <tr>
                                <td style="padding-left: 25px;">{{ Carbon\Carbon::parse($data_jurnal_umum->tanggal)->format('d F Y') ?? null }}</td>
                                <td style="padding-left: 25px;">{{ $detail_jurnal_umum->akun->kode_akun }}</td>
                                <td style="padding-left: 25px;">{{ $detail_jurnal_umum->akun->nama_akun }}</td>
                                <td style="padding-left: 25px; text-align: right;">{{ number_format($detail_jurnal_umum->debit) }}</td>
                                <td style="padding-left: 25px; text-align: right;">{{ number_format($detail_jurnal_umum->kredit) }}</td>
                                <td style="padding-left: 25px;"></td>
                            </tr>
                        @endforeach
                        
                        <tr>
                            <td colspan="3"></td>
                            <td class="border-top" style="padding-left: 25px; text-align: right;">{{ number_format($sum_jurnal_debet) }}</td>
                            <td class="border-top" style="padding-left: 25px; text-align: right;">{{ number_format($sum_jurnal_kredit) }}</td>
                            <td></td>
                        </tr>
                    
                    @endforeach
                    <tr>
                        <td colspan="3"></td>
                        <td class="border-top" style="padding-left: 25px; text-align: right;">{{ number_format($sum_jurnal_all_debet) }}</td>
                        <td class="border-top" style="padding-left: 25px; text-align: right;">{{ number_format($sum_jurnal_all_kredit) }}</td>
                        <td></td>
                    </tr>
                    <!-- Akhir Perulangan Faktur Belum Lunas -->
                <!-- Akhir Perulangan -->
            </table>
        </div>
    </div>
@endsection