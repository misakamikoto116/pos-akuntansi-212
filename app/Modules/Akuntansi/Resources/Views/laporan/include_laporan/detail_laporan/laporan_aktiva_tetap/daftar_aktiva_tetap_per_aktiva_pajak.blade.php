@extends('akuntansi::laporan.laporan_khusus')
@section('title')
    {{ $title }}
@endsection
<style type="text/css">
    .td-blue-left-bold {
        text-align: left;
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
    }
    .td-blue-bold {
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
        text-align: left;
    }
</style>
@section('laporan_khusus')
    @if($item->isEmpty())
        <section class="sheet padding-0mm">
            <article>
                <table class="table">
                    <thead>
                        <tr>
                            <td colspan=9>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                                <p><h6> {{ $title }} </h6></p>
                                <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                                <p> Per <b> {{ $dari_tanggal }} </b></p>
                            </td>
                        </tr>
                    </thead>
                    <tr>
                        <td colspan=9 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }}.
                            </div>
                        </td>
                    </tr>
                </table>
            </article>
        </section>
    @else
        @foreach($item as $item_key => $dataAktiva)
            <section class="sheet padding-0mm">
                <article>
                    <div class="row">
                        <div class="col-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td colspan=9>
                                        <p><h4> {{ $perusahaan ?? null }} </h4></p>
                                            <p><h6> {{ $title }} </h6></p>
                                            <p> Per <b> {{ $dari_tanggal }} </b></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=9>
                                            <div class="row" style="text-align: left">
                                                <div class="col-1"></div>
                                                <div class="col-2 col-form-label">Tipe Aktiva Tetap Pajak</div>
                                                <div>:</div>
                                                <div class="col-3 col-form-label">{{ $item_key }}</div>
                                                <div class="col"></div>
                                                <div class="col-2 col-form-label">Estimasi Umur</div>
                                                <div>:</div>
                                                <div class="col-3 col-form-label">{{ $dataAktiva[0]['tahunEs'] }}</div>
                                            </div>
                                            <div class="row" style="text-align: left">
                                                <div class="col-1"></div>
                                                <div class="col-2 col-form-label">Metode Penyusutan</div>
                                                <div>:</div>
                                                <div class="col-3 col-form-label">{{ $dataAktiva[0]['metode'] }}</div>
                                                <div class="col"></div>
                                                <div class="col-2 col-form-label">Nilai Tukar Fiscal</div>
                                                <div>:</div>
                                                <div class="col-3 col-form-label">{{ $dataAktiva[0]['nilaiTukar'] }}</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <tr>
                                            <th>Kode Aktiva</th>
                                            <th>Nama Aktiva</th>
                                            <th>Harga Perolehan</th>
                                            <th>Penyesuaian Tahun Ini</th>
                                            <th>Akumulasi Depr.</th>
                                            <th>Book Value</th>
                                            <th>Depresiasi Tahun Ini</th>
                                            <th>Tanggal Pemakaian</th>
                                            <th>Tanggal Pembelian</th>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $totalHargaPerolehan        = 0;
                                        $totalPenyesuaianTahun      = 0;
                                        $totalAkumulasiDep          = 0;
                                        $totalBookValue             = 0;
                                        $totalDepresiasiTahunIni    = 0;
                                    @endphp
                                    @foreach($dataAktiva as $dataTipeAktivaTetap)
                                        <tr>
                                            <td>{{ $dataTipeAktivaTetap['kodeAktiva'] }}</td>
                                            <td>{{ $dataTipeAktivaTetap['keterangan'] }}</td>
                                            <td>{{ $dataTipeAktivaTetap['hargaPerolehan'] }}</td>
                                            <td>{{ $dataTipeAktivaTetap['penyesuaianTahun'] }}</td>
                                            <td>{{ $dataTipeAktivaTetap['akumulasiDep'] }}</td>
                                            <td>{{ $dataTipeAktivaTetap['bookValue'] }}</td>
                                            <td>{{ $dataTipeAktivaTetap['depresiasiTahunIni'] }}</td>
                                            <td>{{ $dataTipeAktivaTetap['tglPemakaian'] }}</td>
                                            <td>{{ $dataTipeAktivaTetap['tglPembelian'] }}</td>
                                        </tr>
                                        @php
                                            $totalHargaPerolehan        +=  $dataTipeAktivaTetap['hargaPerolehan'];
                                            $totalPenyesuaianTahun      +=  $dataTipeAktivaTetap['penyesuaianTahun'];
                                            $totalAkumulasiDep          +=  $dataTipeAktivaTetap['akumulasiDep'];
                                            $totalBookValue             +=  $dataTipeAktivaTetap['bookValue'];
                                            $totalDepresiasiTahunIni    +=  $dataTipeAktivaTetap['depresiasiTahunIni'];
                                        @endphp
                                    @endforeach
                                    <tr style="color:red;">
                                        <td colspan="2">&nbsp;</td>
                                        <td class="border-top">{{ $totalHargaPerolehan }}</td>
                                        <td class="border-top">{{ $totalPenyesuaianTahun }}</td>
                                        <td class="border-top">{{ $totalAkumulasiDep }}</td>
                                        <td class="border-top">{{ $totalBookValue }}</td>
                                        <td class="border-top">{{ $totalDepresiasiTahunIni }}</td>
                                        <td colspan="2"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </article>
            </section>
        @endforeach
    @endif
@endsection