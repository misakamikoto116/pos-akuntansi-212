@extends('akuntansi::laporan.laporan')
@section('title')
Laporan Rincian Proyeksi Arus Kas
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="6">
                            <p><h4>CHICHI</h4></p>
                            <p><h6>Rincian Proyeksi Arus Kas</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Per Tanggal 30 Mei 2018 <!-- Tanggal --></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tgl. Faktur</th>
                        <th>No. Faktur</th>
                        <th>Nama</th>
                        <th>Jatuh Tempo</th>
                        <th>Nilai Terima (Asing)</th>
                        <th>Nilai Terima (IDR)</th>
                    </tr>
                </thead>
                <!-- Start Sample -->
                @php
                    $datamatauang = ['IDR','USD'];
                @endphp
                <!-- End Sample -->
                @for($i = 0; $i < 2; $i++)
                <tr>
                    <td colspan="6">{{ $datamatauang[$i] }}</td>
                </tr>
                    @for($j = 0; $j < 1; $j++)
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp; Tgl Faktur</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp; Nomor faktur</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp; Namas</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp; Jatuh Tempo</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp; Nilai Asing</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp; Nilai Terima</td> <!-- Kode Parent Akun -->
                    </tr>
                    @endfor
                    <tr>
                        <td colspan="4"></td>
                        <td class="border-top">Jumlah Nilai Asing</td>
                        <td class="border-top">Jumlah Nilai Terima</td>
                        <td></td>
                    </tr>
                <tr><td>&nbsp;</td></tr>
                @endfor
                <tr>
                    <td colspan="5">Arus Kas yang Diterima</td>
                    <td class="border-top">Total Nilai Terima</td> <!-- Hasilnya Minus -->
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
@endsection