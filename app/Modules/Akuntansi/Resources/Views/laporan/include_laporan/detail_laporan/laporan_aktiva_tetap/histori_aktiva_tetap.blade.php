@extends('akuntansi::laporan.laporan_khusus')
@section('title')
    {{ $title }}
@endsection
<style type="text/css">
    .td-blue-left-bold {
        text-align: left;
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
    }
    .td-blue-bold {
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
        text-align: left;
    }
</style>
@section('laporan_khusus')
    @if ($item->isEmpty())
        <section class="sheet padding-0mm">
            <article>
                <div class="alert alert-warning" style="text-align:center;">
                    Tidak ada data {{ $title }} yang tersedia <br>
                    {{  $subTitle  }}.
                </div>
        </section>
    @else
        @foreach ($item as $item_aktiva)
            <section class="sheet padding-0mm">
                <article>
                    <table class="table">
                        <tr>
                            <td colspan=10>
                                <div style="text-align:center;">
                                    <p><h4> {{ $perusahaan }}   </h4></p>
                                    <p><h6> {{ $title }}        </h6></p>
                                    <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                                    <p><b>  {{ $subTitle   }}   </b></p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">Kode Aktiva Tetap</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['kode_aktiva'] }}</td>
                            <td colspan="2">Akun-akun Aktiva</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['akun_aktiva'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Nama Aktiva Tetap</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['nama_aktiva'] }}</td>
                            <td colspan="2">Akun Akumulasi Penyusutan</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['akun_akum_penyusutan'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Nama Tipe Aktiva Tetap</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['nama_tipe_aktiva'] }}</td>
                            <td colspan="2">Akun Beban Penyusutan</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['akun_beban_penyusutan'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Nama Dep</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['nama_dep'] }}</td>
                            <td colspan="2">Estimasi Umur</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['estimasi_umur'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Tanggal Pembelian</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['tgl_pembelian'] }}</td>
                            <td colspan="2">Metode Penyusutan</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['metode_penyusutan'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Tanggal Pemakaian</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['tgl_pemakaian'] }}</td>
                            <td colspan="2">% Rasio Penyusutan</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['rasio_penyusutan'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Tanggal Jurnal Terakhir</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['tgl_jurnal_akhir'] }}</td>
                            <td colspan="2">Nilai Residu</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['nilai_residu'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Nama Golongan Aktiva Tetap</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['nama_gol_aktiva_tetap'] }}</td>
                            <td colspan="2">Catatan</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['catatan'] }}</td>
                        </tr>
                        <tr class="border-bottom">
                            <td colspan="2">Dihentikan</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['dihentikan'] }}</td>
                            <td colspan="5"></td>
                        </tr>
                        <tr style="font-weight: bold;">
                            <td></td>
                            <td class="border-bottom">Tanggal</td>
                            <td class="border-bottom">No. Sumber</td>
                            <td class="border-bottom">Keterangan</td>
                            <td class="border-bottom">Harga Perolehan</td>
                            <td class="border-bottom">Akumulasi Penyusutan</td>
                            <td class="border-bottom">Nilai Buku</td>
                            <td class="border-bottom">Beban Penyusutan</td>
                            <td class="border-bottom">FA Laba/Rugi</td>
                            <td></td>
                        </tr>
                        @php
                            $sumPerolehan = 0; $sumAkumulasi = 0; $sumNilaiBuku = 0; $sumBeban = 0; $sumFA = 0;
                        @endphp
                        @foreach ($item_aktiva as $as => $data_aktiva)
                            @foreach ($data_aktiva['collectionY'] as $key => $item)
                                <tr>
                                    <td></td>
                                    <td>{{ $item->last()['tanggal'] }}</td>
                                    <td>{{ $item->last()['no_sumber'] }}</td>
                                    <td>{{ $item->last()['keterangan'] }}</td>
                                    <td>{{ number_format($item->first()['harga_perolehan'], 2) }}</td>
                                    <td>{{ number_format($item->last()['akumulasi_dep'], 2) }}</td>
                                    <td>{{ number_format($item->last()['akumulasi_dep'], 2) }}</td>
                                    <td>{{ number_format($item->last()['beban_dep'], 2) }}</td>
                                    <td>{{ number_format($item->last()['fa_laba_rugi'], 2) }}</td>
                                    <td></td>
                                </tr>
                                    @php
                                        $sumPerolehan += $item->first()['harga_perolehan']; $sumAkumulasi += $item->last()['akumulasi_dep'];
                                        $sumNilaiBuku  = $item->last()['nilai_buku']; $sumBeban += $item->last()['beban_dep'];
                                        $sumFA        += $item->last()['fa_laba_rugi'];
                                    @endphp
                            @endforeach
                        <tr>
                            <td colspan="3"></td>
                            <td class="border-top">Net Change of {{ $item_aktiva->first()['kode_aktiva'] }}</td>
                            <td class="border-top" style="font-weight: bold; ">{{ number_format($sumPerolehan, 2) }}</td>
                            <td class="border-top" style="font-weight: bold; ">{{ number_format($sumAkumulasi, 2) }}</td>
                            <td class="border-top" style="font-weight: bold; ">{{ number_format($sumNilaiBuku, 2) }}</td>
                            <td class="border-top" style="font-weight: bold; ">{{ number_format($sumBeban, 2) }}</td>
                            <td class="border-top" style="font-weight: bold; ">{{ number_format($sumFA, 2) }}</td>
                            <td colspan="2"></td>
                        </tr>
                        @endforeach
                    </table>
                </article>
            </section>
        @endforeach
    @endif
@endsection