@extends('akuntansi::laporan.laporan')
@section('title')
Laporan Arus Kas Per Akun
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="4">
                            <p><h4>CHICHI</h4></p>
                            <p><h6>Arus Kas per Akun</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari 1 Mei 2018 ke 30 Mei 2018 <!-- Tanggal --></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tipe Sumber</th>
                        <th>Tanggal</th>
                        <th>Jumlah</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <!-- Perulangan KAS -->
                <tr>
                    <th colspan="4">Kas IDR</th>
                </tr>
                    <!-- Perulangan  -->
                    <tr>
                        <td colspan="6">Tanah</td>
                    </tr>
                        <!-- Ulang -->
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;FA Akuisisi</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;19 Mei 2018</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;10.000.000</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;Aktiva Tetap KD.004</td>
                        </tr>
                        <!-- -->
                    <tr>
                        <td colspan="3"></td>
                        <td class="border-top">10.000.000</td>
                        <td></td>
                    </tr>
                <!-- Akhir Perulangan KAS-->
                    <tr>
                        <td colspan="3"></td>
                        <td class="border-top">10.000.000</td>
                        <td></td>
                    </tr>
            </table>
        </div>
    </div>
@endsection