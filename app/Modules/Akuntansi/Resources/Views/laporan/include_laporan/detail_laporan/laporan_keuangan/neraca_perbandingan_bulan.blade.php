<style>
    .pad-row{
        padding: 15px;
    }
</style>
@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row pad-row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="{{ $priodes->count()+3 }}">
                            <p><h4>{{ $nama_perusahaan }}<h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Periode {{$priodes->first()}} sampai {{$priodes->last()}}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan</th>
                        @foreach ($priodes as $priode)
                            <th>{{$priode}}</th>    
                        @endforeach
                        <th>Selisih</th>
                        <th>% Selisih</th>
                    </tr>
                </thead>              
                @foreach ($items->chunk(2) as $i => $accountGroups)
                    <tr>
                        <th colspan="{{$priodes->count()+3}}">{{$i === 0 ? 'Aktiva' : 'Kewajiban & Ekuitas'}}</th>
                    </tr>
                    @foreach ($accountGroups as $name => $accountGroup)
                        <tr>
                            <th colspan="{{$priodes->count()+3}}" style="padding-left: 25px;">{{$name}}</th>
                        </tr>
                        @foreach ($accountGroup as $name_tipe_akun => $accounts)
                            @if (!empty($accounts['child_and_parent']))
                                <tr class="akun_parent">
                                    <th colspan="{{ $priodes->count()+3 }}" style="padding-left: 40px;">{{ $name_tipe_akun }}</th>
                                </tr>
                                @foreach ($accounts['child_and_parent'] as $key => $account)
                                    @if ($account['akun_child']->isNotEmpty())
                                        @if (array_key_exists('nama_akun_parent', $account))
                                            <tr>
                                                <th style="padding-left: 60px;">{{ $account['nama_akun_parent'] ?? null }}</th>
                                                @php
                                                    $selisihParent = 0;
                                                @endphp
                                                @foreach ($account['sum_akun_parent'] as $item)
                                                    @php $selisihParent -= $item; @endphp
                                                    <th style="text-align: right;">{{ number_format($item) }}</th>    
                                                @endforeach
                                                <th style="text-align: right;">{{ number_format($selisihParent) }}</th>
                                                @php
                                                    $arrSelisihParent = [];
                                                    foreach ($account['sum_akun_parent'] as $key => $value) {
                                                        if($value !== 0.0 && $value !== 0){
                                                            $valuePeriode = $value;
                                                            array_push($arrSelisihParent, $valuePeriode);
                                                        }
                                                    }
                                                    $persenParent =  array_first($arrSelisihParent) !== null ? ($selisihParent / array_first($arrSelisihParent)) * 100 : 0 ;
                                                @endphp
                                                <th style="text-align: right;">{{ number_format($persenParent,2) }} %</th>
                                            </tr>
                                        @endif
                                    @endif
                                    @foreach ($account['akun_child'] as $account_child)
                                        <tr class="akun_child">
                                            <td style="padding-left:  70px">{{ $account_child['nama_akun_child'] }}</td>
                                            @php $selisihChild = 0; @endphp
                                            @foreach ($account_child['sum_akun_child'] as $item)
                                                @php $selisihChild -= $item; @endphp
                                                <td style="text-align: right;">{{ number_format($item) }}</td>    
                                            @endforeach
                                            <td style="text-align: right;">{{ number_format($selisihChild) }}</td>
                                            @php
                                                $arrSelisihChild = [];
                                                foreach ($account_child['sum_akun_child'] as $key => $value) {
                                                    if($value !== 0.0 && $value !== 0){
                                                        $valuePeriode = $value;
                                                        array_push($arrSelisihChild, $valuePeriode);
                                                    }
                                                }
                                                $persenChild =  array_first($arrSelisihChild) !== null ? ($selisihChild / array_first($arrSelisihChild)) * 100 : 0 ;
                                            @endphp
                                            <td style="text-align: right;">{{ number_format($persenChild,2) }} %</td>
                                        </tr>                                           
                                    @endforeach
                                    @if ($account['akun_child']->isEmpty())
                                        @if (array_key_exists('nama_akun_parent', $account))
                                            <tr>
                                                <th style="font-weight: normal; padding-left: 60px;">{{ $account['nama_akun_parent'] ?? null }}</th>
                                                @php $selisihEmptyChild = 0;  @endphp
                                                @foreach ($account['sum_akun_parent'] as $item)
                                                    @php $selisihEmptyChild -= $item;  @endphp
                                                    <th style="text-align: right;">{{ number_format($item) }}</th>
                                                @endforeach
                                                <th style="text-align: right;">{{ number_format($selisihEmptyChild) }}</th>
                                                @php
                                                    $arrSelisihEmptyChild = [];
                                                    foreach ($account['sum_akun_parent'] as $key => $value) {
                                                        if($value !== 0.0 && $value !== 0){
                                                            $valuePeriode = $value;
                                                            array_push($arrSelisihEmptyChild, $valuePeriode);
                                                        }
                                                    }
                                                    $persenEmptyChild =  array_first($arrSelisihEmptyChild) !== null ? ($selisihEmptyChild / array_first($arrSelisihEmptyChild)) * 100 : 0 ;
                                                @endphp
                                                <th style="text-align: right;">{{ number_format($persenEmptyChild,2) }} %</th>
                                            </tr>    
                                        @endif
                                    @endif
                                @endforeach
                                <tr>
                                    <th style="padding-left: 40px;">Jumlah {{ $name_tipe_akun }}</th>
                                    @php $selisihSumSubParent = 0;  @endphp
                                    @foreach ($accounts['sum_tipe_akun'] as $item)
                                        @php $selisihSumSubParent -= $item;  @endphp
                                        <th style="text-align: right;">{{ number_format($item) }}</th>    
                                    @endforeach
                                    <th style="text-align: right;">{{ number_format($selisihSumSubParent) }}</th>
                                    @php
                                        $arrSelisihSumSubParent = [];
                                        foreach ($accounts['sum_tipe_akun'] as $key => $value) {
                                            if($value !== 0.0 && $value !== 0){
                                                $valuePeriode = $value;
                                                array_push($arrSelisihSumSubParent, $valuePeriode);
                                            }
                                        }
                                        $persenSumSubParent =  array_first($arrSelisihSumSubParent) !== null ? ($selisihSumSubParent / array_first($arrSelisihSumSubParent)) * 100 : 0 ;
                                    @endphp
                                    <th style="text-align: right;">{{ number_format($persenSumSubParent,2) }} %</th>
                                </tr>
                            @else
                                @if (!empty($accounts['akun_child']))
                                    @if ($accounts['akun_child']->isNotEmpty())
                                        <tr>
                                            <th style="padding-left: 60px;">{{ $accounts['nama_akun_parent'] ?? null }}</th>
                                            @php
                                                $selisihParent = 0;
                                            @endphp
                                            @foreach ($accounts['sum_akun_parent'] as $item)
                                                @php $selisihParent -= $item; @endphp
                                                <th style="text-align: right;">{{ number_format($item) }}</th>    
                                            @endforeach
                                            <th style="text-align: right;">{{ number_format($selisihParent) }}</th>
                                            @php
                                                $arrSelisihParent = [];
                                                foreach ($accounts['sum_akun_parent'] as $key => $value) {
                                                    if($value !== 0.0 && $value !== 0){
                                                        $valuePeriode = $value;
                                                        array_push($arrSelisihParent, $valuePeriode);
                                                    }
                                                }
                                                $persenParent =  array_first($arrSelisihParent) !== null ? ($selisihParent / array_first($arrSelisihParent)) * 100 : 0 ;
                                            @endphp
                                            <th style="text-align: right;">{{ number_format($persenParent,2) }} %</th>
                                        </tr>
                                    @endif
                                  @endif
                                  @if (array_key_exists('nama_akun_parent', $accounts))
                                    <tr class="akun_child">
                                        <td style="padding-left:  70px">{{ $accounts['nama_akun_parent'] }}</td>
                                        @php $selisihChild = 0; @endphp
                                        @foreach ($accounts['sum_akun_parent'] as $item)
                                            @php $selisihChild -= $item; @endphp
                                            <td style="text-align: right;">{{ number_format($item) }}</td>    
                                        @endforeach
                                        <td style="text-align: right;">{{ number_format($selisihChild) }}</td>
                                        @php
                                            $arrSelisihChild = [];
                                            foreach ($accounts['sum_akun_parent'] as $key => $value) {
                                                if($value !== 0.0 && $value !== 0){
                                                    $valuePeriode = $value;
                                                    array_push($arrSelisihChild, $valuePeriode);
                                                }
                                            }
                                            $persenChild =  array_first($arrSelisihChild) !== null ? ($selisihChild / array_first($arrSelisihChild)) * 100 : 0 ;
                                        @endphp
                                        <td style="text-align: right;">{{ number_format($persenChild,2) }} %</td>
                                    </tr>
                                  @endif
                            @endif
                        @endforeach
                        <tr>
                            <th style="padding-left: 25px;">Jumlah {{$name}}</th>
                            @php $selisihSumParent = 0;  @endphp
                            @foreach ($neraca_sum[$name] as $priode)
                                @php $selisihSumParent -= $priode;  @endphp
                                <th style="text-align: right;">{{number_format($priode)}}</th>
                            @endforeach
                            <th style="text-align: right;">{{number_format($selisihSumParent)}}</th>
                            @php
                                $arrSelisihSumParent = [];
                                foreach ($neraca_sum[$name] as $key => $value) {
                                    if($value !== 0.0 && $value !== 0){
                                        $valuePeriode = $value;
                                        array_push($arrSelisihSumParent, $valuePeriode);
                                    }
                                }
                                $persenSumParent =  array_first($arrSelisihSumParent) !== null ? ($selisihSumParent / array_first($arrSelisihSumParent)) * 100 : 0 ;
                            @endphp
                            <th style="text-align: right;">{{ number_format($persenSumParent,2) }} %</th>
                        </tr> 
                        @endforeach 
                        <tr>
                            <th>{{$sum_neraca_group[$i]['title']}}</th>
                            @php $selisihSumNeraca = 0;  @endphp
                            @foreach ($sum_neraca_group[$i]['jumlah'] as $priode)
                                @php $selisihSumNeraca -= $priode;  @endphp
                                <th style="text-align: right;">{{number_format($priode)}}</th>
                            @endforeach
                            <th style="text-align: right;">{{number_format($selisihSumNeraca)}}</th>
                            @php
                                $arrSelisihNeraca = [];
                                foreach ($sum_neraca_group[$i]['jumlah'] as $key => $value) {
                                    if($value !== 0.0 && $value !== 0){
                                        $valuePeriode = $value;
                                        array_push($arrSelisihNeraca, $valuePeriode);
                                    }
                                }
                                $persenSumNeraca =  array_first($arrSelisihNeraca) !== null ? ($selisihSumNeraca / array_first($arrSelisihNeraca)) * 100 : 0 ;
                            @endphp
                            <th style="text-align: right;">{{ number_format($persenSumNeraca,2) }} %</th>
                        </tr> 
                    @endforeach
            </table>
        </div>
    </div>
@endsection