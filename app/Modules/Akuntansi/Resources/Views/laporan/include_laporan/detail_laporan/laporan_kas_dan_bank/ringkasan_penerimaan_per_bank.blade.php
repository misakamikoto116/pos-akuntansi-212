@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=7>
                            <p><h4>{{ $nama_perusahaan }}</h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir ?? null }} <!-- Tanggal --></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal Transaksi</th>
                        <th>No. Bukti</th>
                        <th style="text-align: center;">Keterangan</th>
                        <th style="text-align: center;">Jumlah</th>
                        <th style="text-align: center;">No. Urut</th>
                        <th style="text-align: center;">Nilai (Domestik)</th>
                        <th style="text-align: center;">Nilai Tukar</th>
                    </tr>
                </thead>
                <!-- Perulangan -->
                @foreach ($items as $akun_bank)

                    @php
                        $sum_penerimaan = 0;
                    @endphp

                    <tr>
                        <td colspan="6"><strong>{{ $akun_bank->nama_akun ?? null }}</strong></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 25px;"><strong>{{ $akun_bank->mataUang->kode ?? null }}</strong></td>
                    </tr>
                        @foreach ($akun_bank->penerimaan as $data_penerimaan)
                            
                            @php
                                $sum_penerimaan += $data_penerimaan->nominal;
                            @endphp

                            <tr>
                                <td style="padding-left: 35px;">{{ Carbon\Carbon::parse($data_penerimaan->tanggal)->format('d F Y') ?? null }}</td>
                                <td style="padding-left: 35px;">{{ $data_penerimaan->no_faktur ?? null }}</td>
                                <td style="padding-left: 35px;">{{ $data_penerimaan->keterangan ?? null }}</td>
                                <td style="padding-left: 35px; text-align: right;">{{ number_format($data_penerimaan->nominal) }}</td>
                                <td style="padding-left: 35px;">0</td> <!--  -->
                                <td style="padding-left: 35px; text-align: right;">{{ number_format($data_penerimaan->nominal) }}</td>
                                <td style="padding-left: 35px; text-align: right;">{{ $akun_bank->mataUang->nilai_tukar ?? null }}</td> <!--  -->
                            </tr>


                        @endforeach
                        <tr>
                            <td style="padding-left: 25px; color: blue;">Total dari {{ $akun_bank->mataUang->kode ?? null }}</td>
                            <td colspan="2"></td>
                            <td class="border-top" style="text-align: right; color: blue;">{{ number_format($sum_penerimaan) }}</td>
                            <td></td>
                        </tr>
                        <!-- Akhir Ulang -->
                        

                @endforeach
                <!-- Akhir Perulangan  -->
            </table>
        </div>
    </div>
@endsection