@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col col-12">
            @if($item->isEmpty())
                <span>No Data</span>
            @else
                <table class="table">
                    <thead>
                        <tr>
                            <td colspan="7">
                                <p><h4>{{ $perusahaan }}</h4></p>
                                <p><h6>{{ $title }}</h6></p>
                                <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                                <p>{{ $subTitle }}</p>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal</th>
                            <th>Sumber</th>
                            <th>No. Sumber</th>
                            <th>Keterangan</th>
                            <th>Debit</th>
                            <th>Kredit</th>
                            <th>Balance</th>
                        </tr>
                    </thead>
                    @foreach($item as $headerKey => $header)
                        <tr>
                            <th>{{ $header['no_akun_header'] }}</th>
                            <th>{{ $header['nama_akun_header'] }}</th>
                            <th>{{ $header['tipe_akun_header'] }}</th>
                            <th colspan="4">{{ $header['saldo_awal_header'] == null ? '0 Dr' : $header['saldo_awal_header'].' Dr' }}</th>
                        </tr>
                        @php $total = 0; $debit = 0; $kredit = 0; @endphp
                        @foreach($header['dataTransaksi'] as $dataTransaksi)
                            @php  $total += $dataTransaksi['nominal_debet'] - $dataTransaksi['nominal_kredit']  @endphp
                            <tr>
                                <td>{{ $dataTransaksi['tanggal'] }}</td>
                                <td>{{ $dataTransaksi['sumber'] }}</td>
                                <td>{{ $dataTransaksi['no_akun'] }}</td>
                                <td>{{ $dataTransaksi['keterangan'] }}</td>
                                <td>{{ number_format($dataTransaksi['nominal_debet']) }}</td>
                                <td>{{ number_format($dataTransaksi['nominal_kredit']) }}</td>
                                <td>
                                    {{ number_format($total) }}
                                </td>
                            </tr>
                            @php $debit += $dataTransaksi['nominal_debet']; $kredit += $dataTransaksi['nominal_kredit']; @endphp
                        @endforeach
                        <tr>
                            <td colspan="4">&nbsp;</td>
                            <td class="border-top"><b>{{ number_format($debit) }}</b></td>
                            <td class="border-top"><b>{{ number_format($kredit) }}</b></td>
                            <td>&nbsp;</td>
                        </tr>
                    @endforeach
                    
                </table>
            @endif
        </div>
    </div>
@endsection