@extends('akuntansi::laporan.laporan')
@section('title')
{{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4>{{ $nama_perusahaan }}</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir ?? null }} <!-- Tanggal --></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal Transaksi</th>
                        <th>No. Bukti</th>
                        <th>No. Akun</th>
                        <th>Nama Akun</th>
                        <th>Catatan</th>
                        <th>Nilai Bayar</th>
                    </tr>
                </thead>
                <!-- Perulangan -->
                @foreach ($items as $akun_bank)
                    <tr>
                        <td colspan="6"><strong>{{ $akun_bank['nama_bank'] ?? null }}</strong></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 25px;"><strong>{{ $akun_bank['kode_mata_uang'] ?? null }}</strong></td>
                    </tr>
                        <!-- Ulang -->
                        @foreach ($akun_bank['data_pembayaran'] as $data_pembayaran)
                            @foreach ($data_pembayaran as $detail_pembayaran)
                                <tr>
                                    <td style="padding-left: 35px;">{{ Carbon\Carbon::parse($detail_pembayaran['tanggal_transaksi'])->format('d F Y') ?? null }}</td>
                                    <td style="padding-left: 35px;">{{ $detail_pembayaran['no_bukti'] ?? null }}</td>
                                    <td style="padding-left: 35px;">{{ $detail_pembayaran['no_akun'] ?? null }}</td> <!--  -->
                                    <td style="padding-left: 35px;">{{ $detail_pembayaran['nama_akun'] ?? null }}</td> <!--  -->
                                    <td style="padding-left: 35px;">{{ $detail_pembayaran['catatan'] ?? null }}</td>
                                    <td style="padding-left: 35px; text-align: right;">{{ number_format($detail_pembayaran['nilai_bayar']) }}</td>
                                </tr>
                            @endforeach
                        @endforeach
                        <tr>
                            <td style="padding-left: 25px; color: blue;">Total dari {{ $akun_bank['kode_mata_uang'] ?? null }}</td>
                            <td colspan="4"></td>
                            <td class="border-top" style="text-align: right; color: blue;">{{ number_format($akun_bank['sum_nominal']) }}</td>
                            <td></td>
                        </tr>
                @endforeach
                <!-- Akhir Perulangan  -->
            </table>
        </div>
    </div>
@endsection