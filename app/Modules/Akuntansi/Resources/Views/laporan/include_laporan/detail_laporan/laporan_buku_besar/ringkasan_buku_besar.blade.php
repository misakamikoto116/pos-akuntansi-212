@extends('akuntansi::laporan.laporan_landscape')
@section('title')
    Laporan Daftar Ringkasan Buku Besar
@endsection
@section('laporan')
    <div class="row">
        <div class="col col-12" align="center">
            <p><h4>Chichi</h4></p>
            <p><h6>Ringkasan Buku Besar</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>Dari <!-- Awal Bulan --> Ke <!-- tutup buku --></p>
        </div>
    </div>
    <div class="row">
        <div class="col col-12">
            <table class="table">
                <tr>
                    <th>No. Akun</th>
                    <th>Nama Akun</th>
                    <th>Saldo Awal</th>
                    <th>Perubahan Debit</th>
                    <th>Perubahan Kredit</th>
                    <th>Perubahan Bersih</th>
                    <th>Saldo Akhir</th>
                </tr>
                <!-- Perulangan Seluruh Akun Parent-->
                <tr>
                    <td>Kode Akun Parent (1101)</td>
                    <td>Nama Akun Parent (Kas)</td>
                    <td>(Dr) 22.730,000</td>
                    <td>0</td>
                    <td>10,000,000</td>
                    <td>(Cr) -10.000,000</td>
                    <td>(Dr) 12.730.000</td>
                </tr>
                    <!-- Perulangan Seluruh Child Akun -->
                    <tr>
                        <td>1101-001</td>
                        <td>Kas IDR</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>1101-002</td>
                        <td>Kas USD</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <!-- End Perulangan Child AKun -->
                <!-- Akhir Perulangan Seluruh Akun Parent-->
                <tr>
                    <td colspan="5">&nbsp;</td>
                    <td class="border-top">Total Nilai Debit</td>
                    <td class="border-top">Total Nilai Kredit</td>
                </tr>
            </table>
        </div>
    </div>
@endsection