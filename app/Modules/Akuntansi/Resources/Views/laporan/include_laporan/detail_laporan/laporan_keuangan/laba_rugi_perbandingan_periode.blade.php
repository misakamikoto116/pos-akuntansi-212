<style>
    .pad-row{
        padding: 15px;
    }
</style>
@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    @php
        $periodes = $priodes->count()-1;
    @endphp
    <div class="row pad-row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan={{$priodes->count()+1}}>
                            <p><h4>{{ $nama_perusahaan }}<h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Periode {{$priodes->first()}} sampai {{ $priodes->slice(0,$periodes)->last() }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan</th>
                        @foreach ($priodes as $priode)
                            <th>{{$priode}}</th>    
                        @endforeach
                        <th>%</th>
                    </tr>
                </thead>
                @foreach ($items as $i => $accountGroups)
                    @foreach ($accountGroups as $name => $accountGroup)
                        <tr>
                            <th colspan="{{$priodes->count()+1}}" style="padding-left: 25px;">{{ $accountGroup['tipe_akun_parent'] }}</th>
                        </tr>
                            @if ($loop)
                                <tr>
                                    <th style="padding-left: 50px;">{{ $accountGroup['nama_akun_parent'] ?? null }}</th>
                                    @php $selisihParent = 0 @endphp
                                    @foreach ($accountGroup['sum_akun_parent'] as $item)
                                        @php $selisihParent -= $item @endphp
                                        <th class="border-bottom" style="text-align: right;">{{ number_format($item) }}</th>
                                    @endforeach
                                    <th class="border-bottom" style="text-align: right;">{{ number_format($selisihParent) }}</th>
                                    @php
                                        // rumusnya : (nilai selisih dibagi nilai periode awal) dikali 100
                                        $arrSelisihParent = [];
                                        foreach ($accountGroup['sum_akun_parent'] as $key => $value) {
                                            if($value !== 0.0){
                                                $valuePeriode = $value;
                                                array_push($arrSelisihParent, $valuePeriode);
                                            }
                                        }
                                        $persenParent =  array_first($arrSelisihParent) !== null ? ($selisihParent / array_first($arrSelisihParent)) * 100 : 0 ;
                                    @endphp
                                    <th class="border-bottom" width="90px" style="text-align: right;">{{ number_format($persenParent,2) }} %</th>
                                </tr>
                            @endif
                                @foreach ($accountGroup['akun_child'] as $accountGroup_child)
                                    <tr>
                                        <td style="padding-left:  75px">{{ $accountGroup_child['nama_akun_child'] }}</td>
                                        @php $selisihChild = 0 @endphp
                                        @foreach (array_slice($accountGroup_child['sum_akun_child'], 0, $periodes) as $item)
                                            @php $selisihChild -= $item @endphp
                                            <td style="text-align: right;">{{ number_format($item) }}</td>
                                        @endforeach
                                        <td style="text-align: right;">
                                            {{ number_format($selisihChild) }}
                                        </td>
                                        @php
                                            $arrSelisihChild = [];
                                            foreach ($accountGroup_child['sum_akun_child'] as $key => $value) {
                                                if($value !== 0.0){
                                                    $valuePeriode = $value;
                                                    array_push($arrSelisihChild, $valuePeriode);
                                                }
                                            }
                                            $persenChild =  array_first($arrSelisihChild) !== null ? ($selisihChild / array_first($arrSelisihChild)) * 100 : 0 ;
                                        @endphp
                                        <td style="text-align: right;">{{ number_format($persenChild,2) }} %</td>
                                    </tr>                                           
                                @endforeach
                            @if ($accountGroup['akun_child']->isEmpty())
                                <tr>
                                    <th style="font-weight: normal; padding-left: 50px;">{{ $accountGroup['nama_akun_parent'] ?? null }}</th>
                                    @php $selisihEmptyChild = 0 @endphp
                                    @foreach ($accountGroup['sum_akun_parent'] as $item)
                                        @php $selisihEmptyChild -= $item @endphp
                                        <th style="text-align: right;">{{ number_format($item) }}</th>
                                    @endforeach
                                    <th style="text-align: right;">{{ number_format($selisihEmptyChild) }}</th>
                                    @php
                                        $arrSelisihEmptyChild = [];
                                        foreach ($accountGroup['sum_akun_parent'] as $key => $value) {
                                            if($value !== 0.0){
                                                $valuePeriode = $value;
                                                array_push($arrSelisihEmptyChild, $valuePeriode);
                                            }
                                        }
                                        $persenEmptyChild =  array_first($arrSelisihEmptyChild) !== null ? ($selisihEmptyChild / array_first($arrSelisihEmptyChild)) * 100 : 0 ;
                                    @endphp
                                    <th style="text-align: right;">{{ number_format($persenEmptyChild,2) }} %</th>
                                </tr>    
                            @else
                            @endif
                        @endforeach 
                    {{-- <tr>
                        <th>{{$sum_neraca_group[$i]['title']}}</th>
                        @foreach ($sum_neraca_group[$i]['jumlah'] as $priode)
                            <th style="text-align: right;">{{number_format($priode)}}</th>
                        @endforeach
                    </tr>  --}}
                @endforeach
            </table>
        </div>
    </div>
@endsection