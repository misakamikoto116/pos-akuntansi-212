@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=3>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan Barang</th>
                        <th>Satuan</th>
                        <th><b> {{ $dari_tanggal }} </b> - <b> {{ $ke_tanggal }} </b></th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=3 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php $total = 0; @endphp
                    @foreach($item as $pembeliannPerbarang => $dataPembelian)
                        @php $subTotal = 0; @endphp
                        @foreach($dataPembelian as $data)
                            <tr>
                                <td>{{ $pembeliannPerbarang   ??  null }}</td>
                                <td>{{ $data['unit']         ??  null }}</td>
                                <td>
                                    @for( $i = 0; $i < 1; $i++)
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    @endfor
                                    {{ $data['total']        ??  null }}
                                </td>
                            </tr>
                            @php $subTotal += $data['total']; @endphp
                        @endforeach
                        @php $total += $subTotal; @endphp
                    @endforeach
                    <tr>
                        <td colspan=2></td>
                        <td class="border-top">
                            @for( $i = 0; $i < 1; $i++)
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            @endfor{{$total}}
                        </td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection