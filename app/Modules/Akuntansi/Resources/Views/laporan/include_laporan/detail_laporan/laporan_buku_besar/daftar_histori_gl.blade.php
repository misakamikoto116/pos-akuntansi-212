@extends('akuntansi::laporan.laporan_landscape')

<style>
    tr th {
        font-size: 13px;
        color: #0066cc;
        text-align: center;
        text-decoration: underline;
    }
    tr td {
        font-size: 12px;
    }
    .text-kanan {
        text-align: right;
    }
</style>

@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <table class="table">
        @if($item->isEmpty())
            <tr>
                <td colspan=4 align="center">
                    <div class="alert alert-warning">
                        Tidak ada data {{ $title }} yang tersedia <br>
                        {{  $subTitle  }}.
                    </div>
                </td>
            </tr>
        @else 
            <thead>
                <tr>
                    <td colspan="8">
                        <div class="col col-12" align="center">
                            <p><h2>{{ $perusahaan }}<h2></p>
                            <p><h3 style="color: #cc0000;">{{ $title }}</h3></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><h6 style="color:black !important"><b>{{ $subTitle }}</b></h6></p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>Tangal</th>
                    <th>Tipe Sumber</th>
                    <th>No. Sumber</th>
                    <th>No. Akun</th>
                    <th>Nama Akun</th>
                    <th>Keterangan</th>
                    <th>Nilai Debit</th>
                    <th>Nilai Kredit</th>
                </tr>
            </thead>
            @foreach ($item as $data)
                @foreach ($data as $laporan)    
                    <tr>
                        <td>{{ $laporan['tanggal'] }}</td>
                        <td>{{ $laporan['tipe_sumber'] }}</td>
                        <td>{{ $laporan['no_sumber'] }}</td>
                        <td>{{ $laporan['no_akun'] }}</td>
                        <td>{{ $laporan['nama_akun'] }}</td>
                        <td>{{ $laporan['keterangan_transaksi'] }}</td>
                        <td>{{ number_format($laporan['nominal_debet']) }}</td>
                        <td>{{ number_format($laporan['nominal_kredit']) }}</td>
                    </tr>
                @endforeach
            @endforeach
            <tr>
                <td colspan="6">&nbsp;</td>
                <td class="border-top">{{ number_format($total_debet) }}</td>
                <td class="border-top">{{ number_format($total_kredit) }}</td>
            </tr>
        @endif
    </table>
@endsection