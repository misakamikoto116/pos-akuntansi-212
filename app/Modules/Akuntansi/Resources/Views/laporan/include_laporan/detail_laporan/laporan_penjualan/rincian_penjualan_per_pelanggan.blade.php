@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan='4'>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Faktur</th>
                        <th>Tanggal Faktur</th>
                        <th>Keterangan</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=4 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                {{ $subTitle   }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php $total = 0; @endphp
                    @foreach($item as $datapelanggan => $datapenjualan)
                        @php $subTotal = 0; @endphp
                        <tr>
                            <td colspan=4>{{ $datapelanggan ?? null }}</td>
                        </tr>
                        @foreach($datapenjualan as $penjualan)
                            <tr>
                                <td>{{ $penjualan['no_faktur']              ?? null }}</td>
                                <td>{{ $penjualan['tanggal_faktur']         ?? null }}</td>
                                <td>{{ $penjualan['keterangan']             ?? null }}</td>
                                <td>{{ number_format($penjualan['total'])   ?? null }}</td>
                            </tr>
                            @php $subTotal += $penjualan['total'] ?? null; @endphp
                        @endforeach
                        <tr>
                            <td colspan=3></td>
                            <td class="border-top">{{ number_format($subTotal) }}</td>
                        </tr>
                        @php $total += $subTotal; @endphp
                    @endforeach
                    <tr>
                        <td colspan=2></td>
                        <td>Rincian Total</td>
                        <td class="border-top">{{ number_format($total) }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection