<style>
    .pad-row{
        padding: 15px;
    }
</style>
@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row pad-row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="4">
                        <p><h4>{{ $nama_perusahaan }}<h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Periode {{$priodes->first()}} sampai {{$priodes->last()}}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan</th>
                        @foreach ($priodes as $priode)
                        <th>{{$priode}}</th>    
                        @endforeach
                    </tr>
                </thead>
                @foreach ($items->chunk(2) as $i => $accountGroups)
                  <tr>
                    <th colspan="{{$priodes->count()+1}}">{{$i === 0 ? 'Aktiva' : 'Kewajiban & Ekuitas'}}</th>
                  </tr>
                  @foreach ($accountGroups as $name => $accountGroup)
                  <tr>
                    <th colspan="{{$priodes->count()+1}}" style="padding-left: 25px;">{{$name}}</th>
                  </tr>
                    @foreach ($accountGroup as $account)
                      @if ($loop->first)
                        <tr>
                            <th colspan="2" style="padding-left: 40px;">{{ $account['tipe_akun_parent'] }}</th>
                        </tr>
                        <tr>
                            <th style="padding-left: 60px;">{{ $account['nama_akun_parent'] ?? null }}</th>
                            @foreach ($account['sum_akun_parent'] as $item)
                              <th style="text-align: right;">{{ number_format($item) }}</th>    
                            @endforeach
                        </tr>
                      @endif
                      @foreach ($account['akun_child'] as $account_child)
                        <tr>
                            <td style="padding-left:  70px">{{ $account_child['nama_akun_child'] }}</td>
                            @foreach ($account_child['sum_akun_child'] as $item)
                              <td style="text-align: right;">{{ number_format($item) }}</td>    
                            @endforeach
                        </tr>                                           
                      @endforeach
                      @if ($account['akun_child']->isEmpty())
                        <tr>
                            <th style="font-weight: normal; padding-left: 60px;">{{ $account['nama_akun_parent'] ?? null }}</th>
                            @foreach ($account['sum_akun_parent'] as $item)
                              <th style="text-align: right;">{{ number_format($item) }}</th>    
                            @endforeach
                        </tr>    
                      @else
                        <tr>
                            <th style="padding-left: 40px;">Jumlah {{ $account['tipe_akun_parent'] }}</th>
                            @foreach ($account['sum_akun_parent'] as $item)
                              <th style="text-align: right;">{{ number_format($item) }}</th>    
                            @endforeach
                        </tr>
                      @endif
                    @endforeach
                    <tr>
                      <th style="padding-left: 25px;">Jumlah {{$name}}</th>
                      @foreach ($neraca_sum[$name] as $priode)
                        <th style="text-align: right;">{{number_format($priode)}}</th>
                      @endforeach
                    </tr> 
                  @endforeach 
                  <tr>
                      <th>{{$sum_neraca_group[$i]['title']}}</th>
                      @foreach ($sum_neraca_group[$i]['jumlah'] as $priode)
                        <th style="text-align: right;">{{number_format($priode)}}</th>
                      @endforeach
                    </tr> 
                @endforeach
            </table>
        </div>
    </div>
@endsection