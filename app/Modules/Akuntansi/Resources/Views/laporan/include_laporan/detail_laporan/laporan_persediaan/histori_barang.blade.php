@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=8>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b> Per {{ $dari_tanggal }} </b><b> Ke {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>Keterangan</th>
                        <th>No. Faktur</th>
                        <th>Price (Base)</th>
                        <th>Kts Masuk</th>
                        <th>Kts Keluar</th>
                        <th>Kuantitas</th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=8 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @foreach($item as $item_key => $itemProduk)
                        <tr style="font-weight:bold;">
                            <td>{{ $item_key                           ?? null }}</td>
                            <td>{{ $itemProduk[0]['nama_barang']       ?? null }}</td>
                            <td colspan=4>&nbsp;</td>
                            <td>{{ $itemProduk[0]['kuantitas']   ?? null }}</td>
                        </tr>
                        @foreach($itemProduk as $itemTransaksi)
                            <tr>
                                <td style="padding-left:25px;">{{  $itemTransaksi['tanggal']  }}</td>
                                <td>{{  $itemTransaksi['catatan']  }}</td>
                                <td>{{  $itemTransaksi['no_transaksi']  }}</td>
                                <td>{{  $itemTransaksi['harga']  }}</td>
                                <td>{{  $itemTransaksi['kuantitas_masuk']  }}</td>
                                <td>{{  $itemTransaksi['kuantitas_keluar']  }}</td>
                                <td>{{  $itemTransaksi['kuantitas_saldo']  }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan=4>&nbsp;</td>
                            <td class="border-top">{{ $itemTransaksi['sum_kty_masuk'] }}</td>
                            <td class="border-top">{{ $itemTransaksi['sum_kty_keluar'] }}</td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
    </div>
@endsection