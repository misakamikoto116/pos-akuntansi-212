@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <td>Keterangan Barang</td>
                        <td>Kuantitas</td>
                        <td>Unit 1 Barang</td>
                        <td>Jumlah</td>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=4 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php $totalQty = 0; $totalHarga = 0; @endphp
                    @foreach($item as $item_key => $dataBarangPembelian)
                        @php $subTotalQty = 0; $subTotalHarga = 0; @endphp
                        @foreach($dataBarangPembelian as $dataBarang)
                            <tr>
                                <td>{{ $item_key            ?? null }}</td>
                                <td>{{ $dataBarang['qty']   ?? null }}</td>
                                <td>{{ $dataBarang['unit']  ?? null }}</td>
                                <td>{{ number_format($dataBarang['total']) ?? null }}</td>   
                            </tr>
                            @php $subTotalQty += $dataBarang['qty']; $subTotalHarga += $dataBarang['total']; @endphp
                        @endforeach
                        @php $totalQty += $subTotalQty; $totalHarga += $subTotalHarga; @endphp
                    @endforeach
                    <tr>
                        <td></td>
                        <td class="border-top">{{ $totalQty }}</td>
                        <td></td>
                        <td class="border-top">{{ number_format($totalHarga) }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection