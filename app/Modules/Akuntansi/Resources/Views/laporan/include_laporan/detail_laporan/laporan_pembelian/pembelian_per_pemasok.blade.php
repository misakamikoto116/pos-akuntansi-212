@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12" align="center">
            <p><h4>{{ $perusahaan }}</h4></p>
            <p><h6>{{ $title }}</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>{{ $subTitle }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table">
                <!-- Perulangan Pembelian per Pelanggan -->
                @php $total = 0; @endphp
                @foreach($item as $rs => $key)
                @php $subTotal = 0; @endphp
                <tr>
                    <th>Nama Pemasok</th>
                    <th>{{ date('d F Y',strtotime($rs)) }}</th> <!-- -->
                </tr>
                    @foreach($key as $kunci)
                    <tr>
                        <td>{{ $kunci['nama'] }}</td>
                        <td>{{ number_format($kunci['total']) }}</td>
                    </tr>
                    @php $subTotal += $kunci['total']; @endphp
                    @endforeach
                <!-- Akhir Perulangan Pembelian per Pemasok -->
                <tr>
                    <td></td>
                    <td class="border-top">{{ number_format($subTotal) }}</td>
                </tr>
                @php $total += $subTotal; @endphp
                @endforeach
                <tr>
                    <td></td>
                    <td class="border-top">{{ number_format($total) }}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection