@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>Faktur Uang Muka</th>
                        <th>Tanggal Faktur Uang Muka</th>
                        <th>Deskripsi Faktur Uang Muka</th>
                        <th>Nilai DP Faktur Uang Muka</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=4 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    {{ $subTitle   }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php $total = 0; @endphp
                        @foreach($item as $item_key => $dataUangMuka)
                            @foreach($dataUangMuka as $dataUang)
                                @php $subTotal = 0; @endphp
                                <tr>
                                    <td>{{ $item_key }}</td>
                                    <td>{{ date('d-m-Y', strtotime($dataUang['tglfak'])) }}</td>
                                    <td>{{ $dataUang['catatan'] }}</td>
                                    <td>{{ number_format($dataUang['total']) }}</td>
                                </tr>
                                @php $subTotal += $dataUang['total']; @endphp
                                <tr>
                                    <td colspan=3></td>
                                    <td class="border-top">{{ number_format($dataUang['total']) }}</td>
                                </tr>
                                <!-- Akhir Perulangan Uang Muka Pesanan Penjualan -->
                                @php $total += $subTotal; @endphp
                            @endforeach
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection