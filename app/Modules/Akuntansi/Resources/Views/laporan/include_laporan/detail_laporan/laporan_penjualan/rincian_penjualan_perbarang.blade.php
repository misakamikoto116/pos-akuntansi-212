@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Faktur</th>
                        <th>Tanggal Faktur</th>
                        <th>Keterangan</th>
                        <th>Kuantitas</th>
                        <th>Unit 1 Barang</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=6 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    {{ $subTitle   }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php $totalHarga = 0; $totalQty = 0;  @endphp
                        @foreach($item as $item_key => $dataRincianPenjualanPerbarang)
                            <tr>
                                <td colspan=6>{{    $item_key   }}</td>
                            </tr>
                            @php $subTotalHarga = 0; $subTotalQty = 0; @endphp
                                @foreach($dataRincianPenjualanPerbarang as $dataRincianPenjualan)
                                    <tr>
                                        <td>{{ $dataRincianPenjualan['no_faktur']            }}</td>
                                        <td>{{ $dataRincianPenjualan['tgl_faktur']           }}</td>
                                        <td>{{ $dataRincianPenjualan['keterangan']           }}</td>
                                        <td>{{ $dataRincianPenjualan['kuantitas']            }}</td>
                                        <td>{{ $dataRincianPenjualan['satuan']               }}</td>
                                        <td>{{ number_format($dataRincianPenjualan['total']) }}</td>
                                    </tr>
                                    @php $subTotalQty += $dataRincianPenjualan['kuantitas']; $subTotalHarga += $dataRincianPenjualan['total']; @endphp
                                @endforeach
                                <tr>
                                    <td colspan=3></td>
                                    <td class="border-top">{{   number_format($subTotalQty)     }}</td>
                                    <td></td>
                                    <td class="border-top">{{   number_format($subTotalHarga)   }}</td>
                                </tr>
                            @php $totalHarga += $subTotalHarga; $totalQty += $subTotalQty;  @endphp
                        @endforeach
                        <tr>
                            <td colspan=3></td>
                            <td class="border-top">{{   number_format($totalQty)     }}</td>
                            <td></td>
                            <td class="border-top">{{   number_format($totalHarga)   }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection