@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Ringkasan Umur Piutang
@endsection
@section('laporan')    
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=8>
                            <p><h4>{{ $nama_perusahaan->nama_perusahaan ?? null }}</h4></p>
                            <p><h6>Ringkasan Umur Piutang</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Per Tanggal {{ $tanggal ?? null }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Nama Pelanggan</th>
                        <th>Jumlah Mata Uang Asing</th>
                        <th>Belum</th>
                        <th>1-30</th>
                        <th>31-60</th>
                        <th>61-90</th>
                        <th>91-120</th>
                        <th>>120</th>
                    </tr>
                </thead>
                <!-- Perulangan Tipe Mata Uang-->
                    <tr>
                        <td colspan="7"><strong>IDR</strong></td>
                    </tr>
                    @foreach ($items as $data_faktur)
                        <tr>
                            <td>{{ $data_faktur['nama_pelanggan'] }}</td>
                            <td>{{ $data_faktur['sum_total'] }}</td>
                            <td>{{ $data_faktur['sum_faktur_belum'] }}</td>
                            <td>{{ $data_faktur['sum_faktur_kurang_30'] }}</td>
                            <td>{{ $data_faktur['sum_faktur_kurang_60'] }}</td>
                            <td>{{ $data_faktur['sum_faktur_kurang_90'] }}</td>
                            <td>{{ $data_faktur['sum_faktur_kurang_120'] }}</td>
                            <td>{{ $data_faktur['sum_faktur_lebih_120'] }}</td>
                        </tr>
                    @endforeach
                    <!-- Akhir Perulangan Faktur Belum Lunas -->
                    <tr>
                        <td></td>
                        <td class="border-top">{{ $grand_sum_total }}</td>
                        <td class="border-top">{{ $grand_sum_faktur_belum }}</td>
                        <td class="border-top">{{ $grand_sum_faktur_kurang_30 }}</td>
                        <td class="border-top">{{ $grand_sum_faktur_kurang_60 }}</td>
                        <td class="border-top">{{ $grand_sum_faktur_kurang_90 }}</td>
                        <td class="border-top">{{ $grand_sum_faktur_kurang_120 }}</td>
                        <td class="border-top">{{ $grand_sum_faktur_lebih_120 }}</td>
                    </tr>
                <!-- Akhir Perulangan -->
            </table>
        </div>
    </div>
@endsection