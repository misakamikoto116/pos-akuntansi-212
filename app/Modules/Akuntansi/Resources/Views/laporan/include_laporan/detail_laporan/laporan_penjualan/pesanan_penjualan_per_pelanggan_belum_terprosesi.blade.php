@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=5>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Pesanan</th>
                        <th>Tgl Pesan</th>
                        <th>ShipDate</th>
                        <th>Jumlah</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=5 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                {{ $subTitle   }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php $total = 0; @endphp
                        @foreach($item as $item_key => $dataPesananPenjualan)
                            <tr>    
                                <td>{{ $item_key }}</td>
                                <td colspan=5></td>
                            </tr>
                            @php $subTotal = 0; @endphp
                                @foreach($dataPesananPenjualan as $dataPesanan)
                                    <tr>
                                        <td>{{ $dataPesanan['sonum']        ??   null  }}</td>
                                        <td>{{ $dataPesanan['tanggal']      ??   null  }}</td>
                                        <td>{{ $dataPesanan['tanggalkirim'] ??   null  }}</td>
                                        <td>{{ number_format($dataPesanan['total'])  ??   null }}</td>
                                        <td>@if($dataPesanan['status'] == 0) Mengantri @endif</td>
                                    </tr>
                                    @php $subTotal += $dataPesanan['total']; @endphp
                                @endforeach
                            @php $total += $subTotal; @endphp
                            <tr>
                                <td colspan="3"></td>
                                <td class="border-top">{{ number_format($total) }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection