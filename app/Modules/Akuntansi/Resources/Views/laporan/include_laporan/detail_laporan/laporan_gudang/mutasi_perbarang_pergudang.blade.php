@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=7>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>No. Sumber</th>
                        <th>Tipe</th>
                        <th>Keterangan</th>
                        <th>Kts. Masuk</th>
                        <th>Kts. Keluar</th>
                        <th>Saldo</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=7 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php
                            $qtyMasuk    =   0;
                            $qtyKeluar   =   0;
                            $qtySaldo    =   0;
                        @endphp
                        @foreach($item as $dataBarang)
                            @if (!$dataBarang['gudang']->isEmpty())
                                @php
                                    $subQtyMasuk    =   0;
                                    $subQtyKeluar   =   0;
                                    $subQtySaldo    =   0;
                                @endphp
                                <tr>
                                    <td><b>{{  $dataBarang['kode_barang']   }}</b></td>
                                    <td><b>{{  $dataBarang['nama_barang']   }}</b></td>
                                    <td colspan=5>&nbsp;</td>
                                </tr>
                                @foreach($dataBarang['gudang'] as $dataGudang_key => $dataGudang)
                                    <tr>
                                        <td style="padding-left:25px;">
                                            <b>{{    $dataGudang_key               ??  null    }}</b>
                                        </td>
                                        <td colspan=5>&nbsp;</td>
                                        <td>   {{    $dataBarang['saldo_awal']     ??  null    }}</td>
                                    </tr>
                                    @php $kts_masuk = 0; $kts_keluar = 0; @endphp
                                    @foreach($dataGudang as $data)
                                        @foreach($data['transaksi'] as $transaksi)
                                            <tr>
                                                <td style="padding-left:50px;">{{    $transaksi['tanggal']    ??  null    }}</td>
                                                <td>{{    $transaksi['no_transaksi']    ??  null    }}</td>
                                                <td>{{    $transaksi['tipe']            ??  null    }}</td>
                                                <td>{{    $transaksi['keterangan']      ??  null    }}</td>
                                                <td>{{    $transaksi['kts_masuk']       ??  null    }}</td>
                                                <td>{{    $transaksi['kts_keluar']      ??  null    }}</td>
                                                <td>{{    $transaksi['saldo']           ??  null    }}</td>
                                            </tr>
                                        @endforeach
                                        @php $kts_masuk += $transaksi['kts_masuk']; $kts_keluar += $transaksi['kts_keluar']; @endphp
                                    @endforeach
                                @endforeach
                                <tr>
                                    <td colspan=4></td>
                                    <td class="border-top">{{ $kts_masuk ?? 0}}</td>
                                    <td class="border-top">{{ $kts_keluar ?? 0}}</td>
                                    <td>&nbsp;</td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                </tbody>                
            </table>
        </div>
    </div>
@endsection