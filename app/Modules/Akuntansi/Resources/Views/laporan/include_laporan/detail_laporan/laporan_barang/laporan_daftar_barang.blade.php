@extends( auth()->user()->hasPermissionTo('laporan_barang') ? 'akuntansi::laporan.laporan' : 'exception.error')
@section('title')
    {{ $title }}
@endsection
<style>
    tr th {
        font-size: 13px;
        color: #0066cc;
        text-align: center;
        text-decoration: underline;
    }
    tr td {
        font-size: 12px;
    }
    .text-kanan {
        text-align: right;
    }
</style>
@section('laporan')
    <div class="row">
        <div class="col col-12" align="center">
            <p><h2>{{ $perusahaan }}<h2></p>
            <p><h3 style="color: #cc0000;">{{ $title }}</h3></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p><h6 style="color:black !important"><b>{{ $subTitle }}</b></h6></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>No. Barang</th>
                        <th>Deskripsi Barang</th>
                        <th>Kuantitas</th>
                        <th>Biaya Rata-rata</th>
                        <th>Biaya Terakhir</th>
                        <th>Harga Satuan</th>
                        <th>Tipe Barang</th>
                        <th>Tipe Persediaan</th>
                    </tr>
                </thead>
                <tbody class="post-all">
                    @foreach ($item as $data)
                        <tr class="post-original">
                            <td>{{ $data['noBarang'] }}</td>
                            <td>{{ $data['namaBarang'] }}</td>
                            <td class="text-kanan">{!! $data['kuantitas'] !!}</td>
                            <td class="text-kanan">{{ $data['biayaRata'] }}</td>
                            <td class="text-kanan">{{ $data['biayaTerakhir'] }}</td>
                            <td class="text-kanan">{{ $data['hargaSatuan'] }}</td>
                            <td>{!! $data['tipeBarang'] !!}</td>
                            <td>{!! $data['tipePersediaan'] !!}</td>
                        </tr>
                        @endforeach
                    <tr class="post-data"></tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection 

@section('custom_js')
    <script>
        $(document).ready(function(){
            var page = 1;
            $(window).scroll(function() {
                if($(window).scrollTop() + $(window).height() >= $(document).height()) {
                    page++;
                    loadMoreData(page);
                }
            });

            function loadMoreData(page){
                if (page > {{ $item->lastPage() }}) {
                    // Do Nothing . . .
                }else{
                    var oneDate = "{{ $dari_tanggal }}";
                    var save    = 0;
                    var gudang  = {{ $gudang }};
                    $.ajax({
                        url: '?page=' + page +'&one-date='+ oneDate +'&save='+ save +'&gudang_id='+ gudang,
                        type: "get",
                        beforeSend: function(){
                            $('.ajax-load').show();
                        }
                    })
                    .done(function(data){
                        if(data == ""){
                            $('.ajax-load').html("Semua Data Telah Berhasil di Tampilkan");
                            return;
                        }
                        $('.ajax-load').hide();
                        $.map(data, function(item, index) {
                            $.map(item, function(itemData, index) {
                                $(".post-data").before("<tr class='post-original'>"+
                                                            "<td>"+itemData['noBarang']+"</td>"+
                                                            "<td>"+itemData['namaBarang']+"</td>"+
                                                            "<td>"+itemData['kuantitas']+"</td>"+
                                                            "<td>"+itemData['biayaRata']+"</td>"+
                                                            "<td>"+itemData['biayaTerakhir']+"</td>"+
                                                            "<td>"+itemData['hargaSatuan']+"</td>"+
                                                            "<td>"+itemData['tipeBarang']+"</td>"+
                                                            "<td>"+itemData['tipePersediaan']+"</td>"+
                                                        "</tr>");
                            });
                        });
                    })
                    .fail(function(jqXHR, ajaxOptions, thrownError){
                        alert('Server tidak Merespon');
                    });
                }
            }
            $('.print-page').click(function (){
                var allPage = {{ $item->lastPage() }};
                var tanggal = '{{ $dari_tanggal }}';
                var gudang = '{{ $gudang }}';
                $.ajax({
                    url: '?one-date='+ tanggal +'&gudang_id='+ gudang,
                    type: "get",
                    timeout: 6000,
                    beforeSend: function(){
                        $('.ajax-load').show();
                    }
                })
                .done(function(data){
                    $('.post-original').remove();
                    $('.post-data').remove();
                    if(data == ""){
                        $('.ajax-load').html("No more records found");
                        return;
                    }
                    $('.ajax-load').hide();
                    $.map(data, function(item, index) {
                        var sumKuantitas = 0;
                        $.map(item, function(itemData, index) {
                            $(".post-all").append("<tr class='post-original'>"+
                                                        "<td>"+itemData['noBarang']+"</td>"+
                                                        "<td>"+itemData['namaBarang']+"</td>"+
                                                        "<td>"+itemData['kuantitas']+"</td>"+
                                                        "<td>"+itemData['biayaRata']+"</td>"+
                                                        "<td>"+itemData['biayaTerakhir']+"</td>"+
                                                        "<td>"+itemData['hargaSatuan']+"</td>"+
                                                        "<td>"+itemData['tipeBarang']+"</td>"+
                                                        "<td>"+itemData['tipePersediaan']+"</td>"+
                                                    "</tr>");
                            $('.totalLoad').autoNumeric('init', {vMin: '0', vMax: '999999999' })
                        });
                    });
                    window.print();
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    alert('Server Gagal Merespon');
                });
                page = page + allPage;
                page++;
            });
        });
    </script>
@endsection