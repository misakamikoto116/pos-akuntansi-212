@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Ringkasan Penerimaan Penjualan
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12" align="center">
            <p><h4>{{ $nama_perusahaan->nama_perusahaan ?? null }}</h4></p>
            <p><h6>Ringkasan Penerimaan Penjualan</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>Dari {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir ?? null }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>No. Form</th>
                        <th>Tanggal Terima</th>
                        <th>No. Cek</th>
                        <th>Tanggal Cek</th>
                        <th>Nama Pelanggan</th>
                        <th>Jumlah Cek</th>
                        <th>Nilai Tukar</th>
                    </tr>
                </thead>
                <!-- Perulangan Mata Uang -->
                <tr>
                    <td>IDR</td> <!-- Mata Uang -->
                </tr>
                <!-- Perulangan Ringkasan Penerimaan Penjualan -->
                    @foreach ($items as $data_penerimaan)
                        <tr>
                            <td>{{ $data_penerimaan->form_no ?? null }}</td>
                            <td>{{ Carbon\Carbon::parse($data_penerimaan->payment_date)->format('d F Y') ?? null }}</td>
                            <td>{{ $data_penerimaan->cheque_no ?? null }}</td>
                            <td>{{ Carbon\Carbon::parse($data_penerimaan->cheque_date)->format('d F Y') ?? null }}</td>
                            <td>{{ $data_penerimaan->pelanggan->nama ?? null }}</td>
                            <td>{{ number_format($data_penerimaan->cheque_amount) }}</td>
                            <td>{{ $data_penerimaan->rate }}</td>
                        </tr>
                    @endforeach
                <!-- Akhir Perulangan Ringkasan Penerimaan Penjualan -->
                    <tr>
                        <td colspan="5" style="color: #0066cc; font-weight: bold;">Total dari IDR</td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">{{ number_format($items->sum('cheque_amount')) }}</td>
                        <td></td>
                    </tr>
                <!-- Akhir Perulangan Mata Uang -->
            </table>
        </div>
    </div>
@endsection