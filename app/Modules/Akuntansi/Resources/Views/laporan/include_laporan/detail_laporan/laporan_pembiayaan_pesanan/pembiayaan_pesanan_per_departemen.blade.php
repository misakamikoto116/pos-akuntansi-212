@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Pembiayaan Pesanan per Departemen
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12" align="center">
            <p><h4>CHICHI</h4></p>
            <p><h6>Pembiayaan Pesanan per Departemen</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>Dar 01 Mei 2018 Ke 30 Mei 2018</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table">
                <tr>
                    <td>No. Barang (No. Akun)</td>
                    <td>Tanggal Pemakaian</td>
                    <td>Detail Status</td>
                    <td>Kts</td>
                    <td>Satuan</td>
                    <td>Alokasi Biaya</td>
                </tr>
                <!-- Perulangan Pembiayaan Pesanan per Departemen -->
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                <!-- Akhir Perulangan Pembiayaan Pesanan per Departemen -->
            </table>
        </div>
    </div>
@endsection