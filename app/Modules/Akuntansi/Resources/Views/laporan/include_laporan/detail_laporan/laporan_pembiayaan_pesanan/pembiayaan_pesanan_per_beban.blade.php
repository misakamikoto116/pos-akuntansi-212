@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=8>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Batch</th>
                        <th>Keterangan</th>
                        <th>Tanggal Pemakaian</th>
                        <th>Detail Status</th>
                        <th>Catatan</th>
                        <th>Alokasi Biaya</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=8 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($item  as $itemKey =>  $itemBebanPembiayaan)
                            <tr style="font-weight: bold">
                                <td>{{  $itemKey                                 }}</td>
                                <td>{{  $itemBebanPembiayaan[0]['nama_beban']    }}</td>
                                <td colspan=6>&nbsp;</td>
                            </tr>
                            @foreach($itemBebanPembiayaan as $itemPembiayaan)
                                <tr>
                                    <td style="padding-left: 25px;">
                                        {{  $itemPembiayaan['no_pembiayaan']     }}
                                    </td>
                                    <td>{{  $itemPembiayaan['keterangan']        }}</td>
                                    <td>{{  $itemPembiayaan['tanggal']           }}</td>
                                    <td>{{  $itemPembiayaan['status']            }}</td>
                                    <td>{{  $itemPembiayaan['catatan']           }}</td>
                                    <td>{{  $itemPembiayaan['alokasi_biaya']     }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan=5>&nbsp;</td>
                                <td class="border-top">{{  $itemPembiayaan['sum_alokasi_biaya']     }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection