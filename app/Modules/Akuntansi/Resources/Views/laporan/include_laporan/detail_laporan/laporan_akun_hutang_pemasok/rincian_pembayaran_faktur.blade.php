@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4>{{ $nama_perusahaan }}</h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} Ke {{ $tanggal_akhir ?? null }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Form</th>
                        <th>No. Cek</th>
                        <th>Tanggal Cek</th>
                        <th>Faktur Telah Dibayar</th>
                        <th>Pembayaran Pajak</th>
                        <th>Diskon</th>
                    </tr>
                </thead>
                    <!-- Perulangan Mata Uang -->
                    <tr>
                        <td colspan="6"><strong>IDR</strong></td>
                    </tr>
                    <!-- Perulangan Group by Pemasok -->
                        @foreach ($items as $data_pemasok)
                                <tr>
                                    <td style="padding-left: 20px;"><strong>{{ $data_pemasok['nama_pemasok'] ?? null }}</strong></td>
                                </tr>
                                @foreach ($data_pemasok['data_faktur'] as $data_faktur)
                                    <tr>
                                        <td style="padding-left: 35px;"><strong>{{ $data_faktur['no_faktur'] ?? null }}</strong></td>
                                        <td style="padding-left: 35px;"><strong>{{ Carbon\Carbon::parse($data_faktur['invoice_date'])->format('d F Y') }}</strong></td>
                                        <td colspan="4"></td>
                                    </tr>
                                    @foreach ($data_faktur['data_invoice_pembayaran'] as $data_invoice_pembayaran)
                                        <tr>
                                            <td style="padding-left: 50px;">{{ $data_invoice_pembayaran['form_no'] ?? null }}</td>
                                            <td style="padding-left: 50px;">{{ $data_invoice_pembayaran['cheque_no'] ?? null }}</td>
                                            <td style="padding-left: 50px;">{{ Carbon\Carbon::parse($data_invoice_pembayaran['cheque_date'])->format('d F Y') ?? null }}</td>
                                            <td style="padding-left: 50px; text-align: right;">{{ number_format($data_invoice_pembayaran['payment_amount']) }}</td>
                                            <td style="padding-left: 50px; text-align: right;">0</td>
                                            <td style="padding-left: 50px; text-align: right;">{{ number_format($data_invoice_pembayaran['diskon']) }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="3" style="padding-left: 50px; color: blue;">Total dari {{ $data_faktur['no_faktur'] ?? null }}</td>
                                        <td class="border-top" style="text-align: right;">{{ number_format($data_faktur['sum_invoice_pembayaran']) }}</td>
                                        <td class="border-top" style="text-align: right;">0</td>
                                        <td class="border-top" style="text-align: right;">{{ number_format($data_faktur['sum_invoice_diskon']) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td style="padding-left: 50px; color: blue;">Nilai Faktur</td>
                                        <td style="text-align: right;">{{ number_format($data_faktur['total_faktur']) }}</td>
                                        <td style="text-align: right;">0</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td style="padding-left: 50px; color: blue;">Hutang Faktur</td>
                                        <td style="text-align: right;">{{ number_format($data_faktur['last_owing']) }}</td>
                                        <td style="text-align: right;">0</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6"></td>
                                    </tr>
                                @endforeach
                            @endforeach
                <!-- Akhir Perulangan Mata Uang -->
            </table>
        </div>
    </div>
@endsection