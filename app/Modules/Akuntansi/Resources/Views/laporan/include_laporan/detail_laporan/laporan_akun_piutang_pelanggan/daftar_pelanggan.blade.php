@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Daftar Pelanggan
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12" align="center">
            <p><h4>{{ $nama_perusahaan->nama_perusahaan ?? null }}</h4></p>
            <p><h6>Daftar Pelanggan</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>Per Tanggal {{ $tanggal ?? null }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>No. Pelanggan</th>
                        <th>Nama Pelanggan</th>
                        <th>Saldo (Asing)</th>
                        <th>Saldo Pajak</th>
                    </tr>
                </thead>
                <!-- Perulangan Mata Uang -->
                <tr>
                    <td><strong>IDR</strong></td> <!-- Mata Uang -->
                </tr>
                <!-- Perulangan Daftar Pelanggan -->
                @foreach ($items as $data_pelanggan)
                    <tr>
                        <td>{{ $data_pelanggan['nomor_pelanggan'] ?? null }}</td>
                        <td>{{ $data_pelanggan['nama_pelanggan'] ?? null }}</td>
                        <td>{{ $data_pelanggan['sum_total'] }}</td>
                        <td>0</td>
                    </tr>
                @endforeach
                <!-- Akhir Perulangan Daftar Pelanggan -->
                    <tr>
                        <td colspan="2"></td>
                        <td class="border-top">{{ $grand_sum_total }}</td>
                        <td class="border-top">0</td>
                    </tr>
                <!-- Akhir Perulangan Mata Uang -->
            </table>
        </div>
    </div>
@endsection