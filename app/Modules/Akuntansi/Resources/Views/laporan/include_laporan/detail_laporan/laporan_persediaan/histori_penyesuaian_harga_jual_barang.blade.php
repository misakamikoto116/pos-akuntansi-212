@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=8>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b> Per {{ $dari_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Barang</th>
                        <th>Deskripsi Barang</th>
                        <th>Tingkat 1 Berubah</th>
                        <th>Tingkat 2 Berubah</th>
                        <th>Tingkat 3 Berubah</th>
                        <th>Tingkat 4 Berubah</th>
                        <th>Tingkat 5 Berubah</th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=8 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php 
                        $satu   =  0; $dua    =  0; $tiga   =  0;
                        $empat  =  0;               $lima   =  0;
                    @endphp
                    @foreach($item as $item_key => $dataHistoriTingkat)
                        @foreach($dataHistoriTingkat as $dataHistori)                            
                            <tr>
                                <td>{{ $dataHistori['no_barang']                              ?? null }}</td>
                                <td>{{ $dataHistori['nm_barang']                             ?? null }}</td>
                                <td>{{ number_format($dataHistori['tingkat_satu'])            ?? null }}</td>
                                <td>{{ number_format($dataHistori['tingkat_dua'])             ?? null }}</td>
                                <td>{{ number_format($dataHistori['tingkat_tiga'])            ?? null }}</td>
                                <td>{{ number_format($dataHistori['tingkat_empat'])           ?? null }}</td>
                                <td>{{ number_format($dataHistori['tingkat_lima'])            ?? null }}</td>
                            </tr>                            
                            @php
                                $satu   +=  $dataHistori['tingkat_satu'];
                                $dua    +=  $dataHistori['tingkat_dua'];
                                $tiga   +=  $dataHistori['tingkat_tiga'];
                                $empat  +=  $dataHistori['tingkat_empat'];
                                $lima   +=  $dataHistori['tingkat_lima'];
                            @endphp
                        @endforeach
                    @endforeach
                    <tr>
                        <td colspan=2>&nbsp;</td>
                        <td class="border-top">{{ number_format($satu)  }}</td>
                        <td class="border-top">{{ number_format($dua)   }}</td>
                        <td class="border-top">{{ number_format($tiga)  }}</td>
                        <td class="border-top">{{ number_format($empat) }}</td>
                        <td class="border-top">{{ number_format($lima)  }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection