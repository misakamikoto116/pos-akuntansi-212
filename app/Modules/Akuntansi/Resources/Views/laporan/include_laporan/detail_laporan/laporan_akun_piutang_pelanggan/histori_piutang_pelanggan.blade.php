@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4>{{ $nama_perusahaan }}</h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal }} ke {{ $tanggal_akhir }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>Keterangan</th>
                        <th style="text-align: center;">Nilai (Asing)</th>
                        <th style="text-align: center;">Nilai Pajak</th>
                    </tr>
                </thead>
                <!-- Perulangan Tipe Mata Uang-->
                    <tr>
                        <td colspan="4"><strong>IDR</strong></td>
                    </tr>
                    @foreach ($items as $data_pelanggan)             
                        <tr>
                            <td style="padding-left: 25px;"><strong>{{ $data_pelanggan['no_pelanggan'] ?? null }}</strong></td> <!-- Akun Piutang -->
                            <td style="padding-left: 25px;" colspan="3"><strong>{{ $data_pelanggan['nama_pelanggan'] ?? null }}</strong></td> <!-- Nama Pelanggan -->
                        </tr>
                        <!-- Perulangan Histori Piutang Pelanggan -->
                            {{-- Faktur --}}
                            @foreach ($data_pelanggan['data_faktur'] as $data_faktur)
                                <tr>
                                    <td style="padding-left: 35px;">{{ $data_faktur['tanggal'] ?? null }}</td> <!-- -->
                                    <td style="padding-left: 35px;">{{ $data_faktur['keterangan'] ?? null }}</td>
                                    <td style="text-align: right; color: {{ $data_faktur['css_child_faktur'] }};">{{ number_format($data_faktur['nilai_asing']) ?? null }}</td>
                                    <td style="text-align: right;">{{ number_format($data_faktur['nilai_pajak']) ?? null }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="2" style="padding-left: 35px; color: #0066cc; font-weight: bold;">Total dari Faktur Penjualan</td>
                                <td class="border-top" style="text-align: right; color: {{ $data_pelanggan['css_faktur'] }}; font-weight: bold;">{{ number_format($data_pelanggan['sum_faktur']) }}</td>
                                <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">0</td>
                            </tr>

                            {{-- Penerimaan --}}
                            @foreach ($data_pelanggan['data_penerimaan'] as $data_penerimaan)
                                <tr>
                                    <td style="padding-left: 35px;">{{ $data_penerimaan['tanggal'] ?? null }}</td> <!-- -->
                                    <td style="padding-left: 35px;">{{ $data_penerimaan['keterangan'] ?? null }}</td>
                                    <td style="text-align: right; color: {{ $data_penerimaan['css_child_penerimaan'] }};">{{ number_format($data_penerimaan['nilai_asing']) ?? null }}</td>
                                    <td style="text-align: right;">{{ number_format($data_penerimaan['nilai_pajak']) ?? null }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="2" style="padding-left: 35px; color: #0066cc; font-weight: bold;">Total dari Penerimaan Penjualan</td>
                                <td class="border-top" style="text-align: right; color: {{ $data_pelanggan['css_penerimaan'] }}; font-weight: bold;">{{ number_format($data_pelanggan['sum_penerimaan']) }}</td>
                                <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">0</td>
                            </tr>

                            {{-- Retur --}}
                            @foreach ($data_pelanggan['data_retur'] as $data_retur)
                                <tr>
                                    <td style="padding-left: 35px;">{{ $data_retur['tanggal'] ?? null }}</td> <!-- -->
                                    <td style="padding-left: 35px;">{{ $data_retur['keterangan'] ?? null }}</td>
                                    <td style="text-align: right; {{ $data_retur['css_child_retur'] }};">{{ number_format($data_retur['nilai_asing']) ?? null }}</td>
                                    <td style="text-align: right;">{{ number_format($data_retur['nilai_pajak']) ?? null }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="2" style="padding-left: 35px; color: #0066cc; font-weight: bold;">Total dari Retur Penjualan</td>
                                <td class="border-top" style="text-align: right; color: {{ $data_pelanggan['css_retur'] }}; font-weight: bold;">{{ number_format($data_pelanggan['sum_retur']) }}</td>
                                <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">0</td>
                            </tr>

                            {{-- <tr>
                                <td style="padding-left: 35px;">13 Mei 2018</td>
                                <td style="padding-left: 35px;">Sales Invoice : Serv/09/005</td>
                                <td style="text-align: right;">0</td>
                                <td style="text-align: right;">0</td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-left: 35px; color: #0066cc; font-weight: bold;">Total dari Bukti Jurnal</td>
                                <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">0</td>
                                <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">0</td>
                            </tr> --}}
                        <!-- Akhir Perulangan Histori Piutang Pelanggan -->
                        <tr>
                            <td colspan="2" style="padding-left: 25px; color: #0066cc; font-weight: bold;">Total Penambahan</td>
                            <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">{{ number_format($data_pelanggan['sum_penambahan']) }}</td>
                            <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">0</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-left: 25px; color: #0066cc; font-weight: bold;">Total Penurunan</td>
                            <td style="text-align: right; color: #0066cc; font-weight: bold;">{{ number_format($data_pelanggan['sum_penurunan']) }}</td>
                            <td style="text-align: right; color: #0066cc; font-weight: bold;">0</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-left: 25px; color: #0066cc; font-weight: bold;">Perubahan Bersih</td>
                            <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">{{ number_format($data_pelanggan['total_perubahan']) }}</td>
                            <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">0</td>
                        </tr>
                    @endforeach
                <tr>
                    <td colspan="2" style="color: #0066cc; font-weight: bold;">Total dari IDR</td>
                    <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">{{ number_format($total_all) }}</td>
                    <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">0</td>
                </tr>

                <tr>
                    <td colspan="1"></td>
                    <td colspan="" style="text-align: right; color: #0066cc; font-weight: bold;">{{ $count_faktur }} Invoice</td>
                    <td style="text-align: right; color: #0066cc; font-weight: bold;">{{ number_format($sum_faktur_all) }}</td>
                    <td style="text-align: right; color: #0066cc; font-weight: bold;">0</td>
                </tr>

                <tr>
                    <td colspan="1"></td>
                    <td colspan="" style="text-align: right; color: #0066cc; font-weight: bold;">{{ $count_penerimaan }} Cheque</td>
                    <td style="text-align: right; color: #0066cc; font-weight: bold;">{{ number_format($sum_penerimaan_all) }}</td>
                    <td style="text-align: right; color: #0066cc; font-weight: bold;">0</td>
                </tr>

                <tr>
                    <td colspan="1"></td>
                    <td colspan="" style="text-align: right; color: #0066cc; font-weight: bold;">{{ $count_retur }} Return</td>
                    <td style="text-align: right; color: #0066cc; font-weight: bold;">{{ number_format($sum_retur_all) }}</td>
                    <td style="text-align: right; color: #0066cc; font-weight: bold;">0</td>
                </tr>
                <!-- Akhir Perulangan -->
            </table>
        </div>
    </div>
@endsection