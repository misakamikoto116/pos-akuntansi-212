@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Histori Perubahan Asset Tetap
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12" align="center"> 
            <p><h4>CHICHI</h4></p>
            <p><h6>Histori Perubahan Asset Tetap</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>Dari 01 Mei 2018 ke 30 Mei 2018</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table">
                <tr>
                    <td class="align-mid">Perubahan</td>
                    <td class="align-mid">Tanggal Ubah</td>
                    <td class="align-mid">Value Sebelum Perubahan</td>
                </tr>
                <!-- Perulangan Aktiva -->
                    <tr>
                        <td colspan="3">Ruko 3 Lt.</td>
                    </tr>
                    <!-- Perulangan Histori Perubahan Asset Tetap -->
                        <tr>
                            <td>Akun Akumulasi Penyusutan</td>
                            <td>29 Mei 2018</td>
                            <td></td>
                        </tr>
                    <!-- Akhir Perulangan Histori Perubahan Asset Tetap -->
                <!-- Akhir Perulangan Aktiva -->
            </table>
        </div>
    </div>
@endsection