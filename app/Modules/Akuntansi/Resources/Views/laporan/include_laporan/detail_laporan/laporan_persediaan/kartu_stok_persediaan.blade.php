@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=8>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>Tipe</th>
                        <th>No. Faktur</th>
                        <th>Keterangan</th>
                        <th>Kts. Masuk</th>
                        <th>Kts. Keluar</th>
                        <th>Kuantitas</th>
                        <th>Gudang</th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=8 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @foreach($item as $dataKartuStokPersediaan)
                        <tr>
                            <td>{{ $dataKartuStokPersediaan['no_barang']   ??   null }}</td>
                            <td>{{ $dataKartuStokPersediaan['nm_barang']   ??   null }}</td>
                            <td colspan=4>&nbsp;</td>
                            <td>{{ $dataKartuStokPersediaan['kts_saldo']   ??   null }}</td>
                        </tr>
                        @php $qtyMasuk = 0 ; $qtyKeluar = 0; @endphp
                        @foreach($dataKartuStokPersediaan['item'] as $dataKartuStok)
                            @foreach ($dataKartuStok as $itemKartuStok)
                                @php
                                    $qtyMasuk  += $itemKartuStok['kts_masuk'];
                                    $qtyKeluar += $itemKartuStok['kts_keluar'];
                                    $qty        = $qtyMasuk + $qtyKeluar;
                                @endphp
                                <tr>
                                    <td>{{ $itemKartuStok['tanggal']              ?? null }}</td>
                                    <td>{{ $itemKartuStok['tipe']                 ?? null }}</td>
                                    <td>{{ $itemKartuStok['no_faktur']            ?? null }}</td>
                                    <td>{{ $itemKartuStok['keterangan']           ?? null }}</td>
                                    <td>{{ $itemKartuStok['kts_masuk']            ?? null }}</td>
                                    <td>{{ abs($itemKartuStok['kts_keluar'])      ?? null }}</td>
                                    <td>{{ $qty                                   ?? null }}</td>
                                    <td>{{ $itemKartuStok['gudang']               ?? null }}</td>
                                </tr>
                            @endforeach
                        @endforeach
                        <tr>
                            <td colspan=4>&nbsp;</td>
                            <td class="border-top">{{ number_format($qtyMasuk)  }}</td>
                            <td class="border-top">{{ number_format(abs($qtyKeluar)) }}</td>
                            <td colspan=2>&nbsp;</td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
    </div>
@endsection