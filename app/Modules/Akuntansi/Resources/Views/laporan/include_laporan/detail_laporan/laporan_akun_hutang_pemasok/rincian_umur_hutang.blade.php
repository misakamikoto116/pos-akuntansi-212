@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title= 'Laporan Rincian Umur Hutang'}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=9>
                            <p><h4>{{ $nama_perusahaan->nama_perusahaan ?? null }}</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Per Tanggal {{ $per_tanggal ?? null }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Faktur</th>
                        <th>Tanggal Faktur</th>
                        <th>Jumlah Mata Uang Asing</th>
                        <th>Belum</th>
                        <th>0-30</th>
                        <th>31-60</th>
                        <th>61-90</th>
                        <th>91-120</th>
                        <th>>120</th>
                    </tr>
                </thead>
                <!-- Perulangan Mata Uang -->
                    <tr>
                        <td colspan="9"><strong>IDR</strong></td>
                    </tr>
                    <!-- Perulangan Pemasok-->
                        @foreach ($items as $item)
                            <tr>
                                <td><strong>{{ $item['nomor_pemasok'] ?? null }}</strong></td>
                                <td colspan="8"><strong>{{ $item['nama_pemasok'] ?? null }}</strong></td>
                            </tr>
                        <!-- Perulangan Ringkasan Umur Hutang -->
                            @foreach ($item['faktur'] as $data_faktur)
                                <tr>
                                    <td>{{ $data_faktur['no_faktur'] ?? null }}</td>
                                    <td>{{ $data_faktur['tanggal_faktur'] ?? null }}</td>
                                    <td>{{ $data_faktur['nilai_faktur'] ?? null }}</td>
                                    <td>{{ $data_faktur['nilai_faktur_belum'] ?? 0 }}</td>
                                    <td>{{ $data_faktur['nilai_faktur_kurang_30'] ?? 0 }}</td>
                                    <td>{{ $data_faktur['nilai_faktur_kurang_60'] ?? 0 }}</td>
                                    <td>{{ $data_faktur['nilai_faktur_kurang_90'] ?? 0 }}</td>
                                    <td>{{ $data_faktur['nilai_faktur_kurang_120'] ?? 0 }}</td>
                                    <td>{{ $data_faktur['nilai_faktur_lebih_120'] ?? 0 }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="2"></td>
                                <td class="border-top">{{ $item['sum_total'] ?? null }}</td>
                                <td class="border-top">{{ $item['sum_faktur_belum'] ?? 0 }}</td>
                                <td class="border-top">{{ $item['sum_faktur_kurang_30'] ?? 0 }}</td>
                                <td class="border-top">{{ $item['sum_faktur_kurang_60'] ?? 0 }}</td>
                                <td class="border-top">{{ $item['sum_faktur_kurang_90'] ?? 0 }}</td>
                                <td class="border-top">{{ $item['sum_faktur_kurang_120'] ?? 0 }}</td>
                                <td class="border-top">{{ $item['sum_faktur_lebih_120'] ?? 0 }}</td>
                            </tr>
                        @endforeach
                        <!-- Akhir Perulangan Ringkasan Umur Hutang -->
                    <!-- Akhir Perulangan Faktur -->
                <!-- Akhir Perulangan Mata Uang -->
            </table>
        </div>
    </div>
@endsection