@extends('akuntansi::laporan.laporan')
@section('title')
Laporan Daftar Akun Kas dan Bank
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="6">
                            <p><h4>CHICHI</h4></p>
                            <p><h6>Daftar Akun Kas dan Bank</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Per Tanggal 30 Mei 2018 <!-- Tanggal --></p>
                            <p>Dari 
                                <b>
                                    @if(Request::get('start-date') && Request::get('end-date')) 
                                        {{ date('d-m-Y', strtotime(Request::get('start-date'))) }}</b> Ke <b>{{ date('d-m-Y', strtotime(Request::get('end-date'))) }}
                                    @else
                                        {{ date('d-m-Y', strtotime('-1 days')) }}</b> Ke <b>{{ date('d-m-Y', strtotime('+1 days')) }}
                                    @endif
                                </b> 
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Akun</th>
                        <th>Nama Akun</th>
                        <th>Nilai Tukar</th>
                        <th>Saldo (Asing)</th>
                        <th>Akun Saldo</th>
                        <th>Akun Induk</th>
                    </tr>
                </thead>
                <?php $total_asing = 0; $total = 0; ?>
                @foreach($data as $matauang)
                <?php $sub_total_asing = 0; $sub_total = 0; ?>
                <tr>
                    <td colspan="6">{{ $matauang->kode }}</td>
                </tr>
                    <!-- Perulangan Akun Kas da Bank -->
                    @foreach($matauang->akun as $akun)
                    <?php 
                        $asing = $akun->transaksi->sum('nominal') / 1;
                        $sub_total_asing += $asing;
                        $sub_total += $akun->transaksi->sum('nominal');
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;{{ $akun->kode_akun }}</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;{{ $akun->nama_akun }}</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;{{ 1 }}</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;{{ "Rp. ".number_format($asing) }}</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;{{ "Rp. ".number_format($akun->transaksi->sum('nominal')) }}</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;{{ empty($akun->parent_id) ? '-' : $akun->parentAkun->kode_akun }}</td> <!-- Kode Parent Akun -->
                    </tr>
                    @endforeach
                    <!-- Akhir Perulangan -->
                    <tr>
                        <td colspan="3"></td>
                        <td class="border-top">Rp. {{ number_format($sub_total_asing) }}</td> <!-- Jumlahkan semua saldo asing per group -->
                        <td class="border-top">Rp. {{ number_format($sub_total) }}</td>
                        <td></td>
                    </tr>
                    <?php $total_asing += $sub_total_asing; $total += $sub_total; ?>
                @endforeach
                <!-- Akhir Perulangan Group Mata Uang-->
                <tr>
                        <td colspan="3"></td>
                        <td class="border-top">Rp. {{ number_format($total_asing) }}</td> <!-- Jumlahkan semua saldo asing pada seluruh group -->
                        <td class="border-top">Rp. {{ number_format($total) }}</td>
                        <td></td>
                    </tr>
            </table>
        </div>
    </div>
@endsection