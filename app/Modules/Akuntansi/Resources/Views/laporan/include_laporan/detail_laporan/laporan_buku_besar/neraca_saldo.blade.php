<style>
    thead > tr > th {
        font-size : 13px !important;
    }
</style>
@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="7">
                            <p><h4>{{ $nama_perusahaan ?? null }}</h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal }} Ke {{ $tanggal_akhir }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th width="13%">No. Akun</th>
                        <th>Nama Akun</th>
                        <th>Saldo Awal Debet</th>
                        <th>Saldo Awal Kredit</th>
                        <th>Perubahan Debet</th>
                        <th>Perubahan Kredit</th>
                        <th>Saldo Akhir Debet</th>
                        <th>Saldo Akhir Kredit</th>
                    </tr>
                </thead>
                @foreach ($items as $item)
                    @php

                        // Akun Parent
                        $saldo_akhir_parent_debet   = 0;
                        $saldo_akhir_parent_kredit  = 0;
                        $sum_child_saldo_awal       = $item['childAkun']->sum('money_function');
                        $sum_child_perubahan_debet  = $item['childAkun']->reduce(function ($carry, $item)
                        {
                            return $carry + $item->transaksi->where('status', 1)->sum('nominal'); 
                        }, 0);
                        $sum_child_perubahan_kredit = $item['childAkun']->reduce(function ($carry, $item)
                        {
                            return $carry + $item->transaksi->where('status', 0)->sum('nominal'); 
                        }, 0);  

                        $saldo_akhir_parent = $sum_child_saldo_awal + $sum_child_perubahan_debet - $sum_child_perubahan_kredit;

                        if ($saldo_akhir_parent < 0) {
                            $saldo_akhir_parent_kredit = $saldo_akhir_parent;
                        }else if ($saldo_akhir_parent >= 0) {
                            $saldo_akhir_parent_debet  = $saldo_akhir_parent;
                        }


                        // Akun Child
                        $saldo_akhir_kredit = 0;
                        $saldo_akhir_debet  = 0;
                        $saldo_akhir        = $item['money_function'] + $item['perubahan_debet'] - $item['perubahan_kredit'];
                        if ($saldo_akhir < 0) {
                            $saldo_akhir_kredit = $saldo_akhir;
                        }else if ($saldo_akhir >= 0) {
                            $saldo_akhir_debet = $saldo_akhir;
                        }
                    @endphp
                    <tr
                        <?php
                            if($item['parent_stat'] == 1){
                                echo "style='font-weight: bold'";
                            }
                        ?>
                    >
                        <td>
                            <?php 
                                if($item['parent_stat'] == 0 ){
                                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                }

                                if($item['parent_stat'] == 1 && !empty($item['childAkun']) && $item['parent_id'] != null){
                                    echo "&nbsp;&nbsp;&nbsp;";
                                }
                            ?>
                            {{ $item['kode_akun'] }}
                        </td>
                        <td>{{ $item['nama_akun'] }}</td>
                        <td>
                            @php
                                if ($item['parent_id'] === null) {
                                    echo number_format($sum_child_saldo_awal, 2);
                                }else {

                                    echo number_format($item['money_function'], 2);
                                }
                            @endphp
                        </td>
                        <td>0</td>
                        <td>
                            @php
                                if ($item['parent_id'] === null) {
                                    echo number_format($sum_child_perubahan_debet, 2);
                                }else {
                                    echo number_format($item['perubahan_debet'], 2);
                                }
                            @endphp
                        </td>
                        <td>
                            @php
                                if ($item['parent_id'] === null) {
                                    echo number_format($sum_child_perubahan_kredit, 2);
                                }else {
                                    echo number_format($item['perubahan_kredit'], 2);
                                }
                            @endphp
                        </td>
                        <td>
                            @php
                                if ($item['parent_id'] === null) {
                                    echo number_format($saldo_akhir_parent_debet, 2);
                                }else {
                                    echo number_format($saldo_akhir_debet, 2);       
                                }
                            @endphp
                        </td>
                        <td>
                            @php
                                if ($item['parent_id'] === null) {
                                    echo number_format($saldo_akhir_parent_kredit, 2);
                                }else {
                                    echo number_format($saldo_akhir_kredit, 2);       
                                }
                            @endphp
                        </td>
                    </tr>
                <!-- Akun Parent -->
                @endforeach
            </table>
        </div>
    </div>
@endsection