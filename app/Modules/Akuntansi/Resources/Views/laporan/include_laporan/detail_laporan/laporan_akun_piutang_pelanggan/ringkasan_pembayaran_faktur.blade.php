@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4>{{ $nama_perusahaan }}</h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} Ke {{ $tanggal_akhir ?? null }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal Faktur</th>
                        <th>No. Faktur</th>
                        <th>Nama Pelanggan</th>
                        <th>Jumlah Pembayaran</th>
                        <th>No. Cek</th>
                        <th>Tanggal Cek</th>
                    </tr>
                </thead>
                <!-- Perulangan Tipe Mata Uang-->
                    <tr>
                        <td colspan="8"><strong>IDR</strong></td>
                    </tr>
                    <!-- Perulangan Ringkasan Pembayaran Faktur -->
                    @php
                        $sum_penerimaan = 0;
                    @endphp
                    @foreach ($items as $item)
                        @foreach ($item as $data_penerimaan)
                            @php
                                $sum_penerimaan += $data_penerimaan['payment_amount'];
                            @endphp
                            <tr>
                                <td style="padding-left: 25px;">{{ $data_penerimaan['invoice_date'] }}</td>
                                <td style="padding-left: 25px;">{{ $data_penerimaan['no_faktur'] }}</td>
                                <td style="padding-left: 25px;">{{ $data_penerimaan['nama_pelanggan'] }}</td>
                                <td style="text-align: right;">{{ number_format($data_penerimaan['payment_amount']) }}</td>
                                <td style="padding-left: 25px;">{{ $data_penerimaan['cheque_no'] }}</td>
                                <td style="padding-left: 25px;">{{ $data_penerimaan['cheque_date'] }}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    <!-- Akhir Perulangan Ringkasan Pembayaran Faktur -->
                    <tr>
                        <td colspan="3" style="color: blue;">Total dari IDR</td>
                        <td class="border-top" style="text-align: right;">{{ number_format($sum_penerimaan) }}</td>
                        <td></td>
                    </tr>
                <!-- Akhir Perulangan -->
            </table>
        </div>
    </div>
@endsection