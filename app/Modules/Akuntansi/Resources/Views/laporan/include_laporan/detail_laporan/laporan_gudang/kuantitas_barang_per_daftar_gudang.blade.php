@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan={{$item->first()->count()}}>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                        </td>
                    </tr>
                    <tr>
                        @foreach($item->first() as $dataGudang)
                            <th>
                                {{  $dataGudang  }}
                            </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                                <td colspan={{$item->first()->count()}} align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia.
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($item->splice(1) as $dataGudang)
                            <tr>
                                @foreach($dataGudang as $data)
                                    <td>
                                        {{$data}}
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    @endif
                </tbody>                
            </table>
        </div>
    </div>
@endsection