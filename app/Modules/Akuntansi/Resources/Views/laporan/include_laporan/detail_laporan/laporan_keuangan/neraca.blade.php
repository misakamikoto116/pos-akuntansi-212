@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row pad-row">
        <div class="col-md-12">
            <table id="table_data" class="table">
                <thead>
                    <tr>
                        <td colspan="4">
                          <p><h4>{{ $nama_perusahaan ?? null }}<h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>{{$sub_title}}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan</th>
                        <th width="20%">Balance</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($items->chunk(2) as $i => $accountGroups)
                    <tr>
                      <th>{{$i === 0 ? 'Aktiva' : 'Kewajiban & Ekuitas'}}</th>
                    </tr>
                    @foreach ($accountGroups as $name => $accountGroup)
                      <tr>
                        <th style="padding-left: 25px;">{{$name}}</th>
                      </tr>
                      @foreach ($accountGroup as $name_tipe_akun => $accounts)
                        @if (!empty($accounts['child_and_parent']))
                          <tr>
                              <th colspan="2" style="padding-left: 40px;">{{ $name_tipe_akun }}</th>
                          </tr>
                          @foreach ($accounts['child_and_parent'] as $account)
                            @if ($account['akun_child']->isNotEmpty())
                              @if (array_key_exists('nama_akun_parent', $account))
                                <tr class="akun_parent">
                                      <th style="padding-left: 60px;">{{ $account['nama_akun_parent'] ?? null }}</th>
                                      @foreach ($account['sum_akun_parent'] as $item)
                                        <th class="akun_val" style="text-align: right;">{{ number_format($item) }}</th>
                                      @endforeach
                                </tr>
                              @endif
                            @endif
                            @foreach ($account['akun_child'] as $account_child)
                              <tr class="akun_child">
                                    <td style="padding-left:  70px">{{ $account_child['nama_akun_child'] }}</td>
                                    @foreach ($account_child['sum_akun_child'] as $item)
                                      <td class="akun_val akun_child_val" style="text-align: right;">{{ number_format($item) }}</td>
                                    @endforeach
                              </tr>                                           
                            @endforeach
                            @if ($account['akun_child']->isEmpty())
                              @if (array_key_exists('nama_akun_parent', $account))
                                <tr class="akun_child">
                                      <th style="font-weight: normal; padding-left: 60px;">{{ $account['nama_akun_parent'] ?? null }}</th>
                                      @foreach ($account['sum_akun_parent'] as $item)
                                        <th class="akun_val akun_child_val" style="text-align: right;">{{ number_format($item) }}</th>
                                      @endforeach
                                </tr>
                              @endif    
                            @endif
                          @endforeach
                          <tr class="akun_parent">
                                <th style="padding-left: 40px;">Jumlah {{ $name_tipe_akun }}</th>
                                @foreach ($accounts['sum_tipe_akun'] as $item)
                                  <th class="akun_val" style="text-align: right;">{{ number_format($item) }}</th>
                                @endforeach
                          </tr>
                        @else
                          @if (!empty($accounts['akun_child']))
                            @if ($accounts['akun_child']->isNotEmpty())
                              <tr class="akun_parent">
                                    <th style="padding-left: 60px;">{{ $accounts['nama_akun_parent'] ?? null }}</th>
                                    @foreach ($accounts['sum_akun_parent'] as $item)
                                      <th class="akun_val" style="text-align: right;">{{ number_format($item) }}</th>
                                    @endforeach
                              </tr>
                            @endif
                          @endif
                          @if (array_key_exists('nama_akun_parent', $accounts))
                            <tr class="akun_child">
                                  <th style="font-weight: normal; padding-left: 60px;">{{ $accounts['nama_akun_parent'] ?? null }}</th>
                                  @foreach ($accounts['sum_akun_parent'] as $item)
                                    <th class="akun_val akun_child_val" style="text-align: right;">{{ number_format($item) }}</th>
                                  @endforeach
                            </tr>    
                          @endif
                        @endif
                      @endforeach
                      <tr>
                          <th style="padding-left: 25px;">Jumlah {{$name}}</th>
                          @foreach ($neraca_sum[$name] as $priode)
                            <th style="text-align: right;">{{number_format($priode)}}</th>
                          @endforeach
                      </tr> 
                    @endforeach 
                    <tr>
                          <th>{{$sum_neraca_group[$i]['title']}}</th>
                          @foreach ($sum_neraca_group[$i]['jumlah'] as $priode)
                            <th style="text-align: right;">{{number_format($priode)}}</th>
                          @endforeach
                    </tr> 
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('custom_js')
    {{-- <script>
        $( window ).on( "load", function() {
            const tampil_total = "{{ request()->has('tampil_total') ? request()->get('tampil_total') : '' }}";
            const tampil_induk = "{{ request()->has('tampil_induk') ? request()->get('tampil_induk') : '' }}";
            const tampil_anak = "{{ request()->has('tampil_anak') ? request()->get('tampil_anak') : '' }}";
            const tampil_jumlah_induk = "{{ request()->has('tampil_jumlah_induk') ? request()->get('tampil_jumlah_induk') : '' }}";
            const geser_kanan = "{{ request()->has('geser_kanan') ? request()->get('geser_kanan') : '' }}";
            const saldo_nol = "{{ request()->has('saldo_nol') ? request()->get('saldo_nol') : '' }}";
            if(tampil_total !== 'on'){
                $('.akun_parent').remove();
            }
            if(geser_kanan === 'on'){
                $('.akun_child_val').css("padding-right", "60px");
            }
            if(tampil_induk !== 'on'){
                $('.akun_parent').remove();
            }
            if(saldo_nol === 'on'){

                var nol = $(this).closest('td').find('.akun_child_val').text();
                $('.nilai_saldo').val('');
            }
        });
        $(document).ready(function() {
            $('#table_data tbody tr:last').children('td').each(function(index,event) {
                console.log($(event).text());
                if ($(event).text() === "0") {
                    $('#table_data th').each(function(indexIn,eventIn) {
                        $(eventIn).children('td').eq(index).css('display','none');
                        $(eventIn).children('th').eq(index).css('display','none');
                    });
                }
            });
        });

    </script> --}}
@endsection