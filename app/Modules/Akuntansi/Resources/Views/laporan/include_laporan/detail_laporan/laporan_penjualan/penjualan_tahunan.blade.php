@extends('akuntansi::laporan.laporan')

@section('title')
    {{ $title }}
@endsection

@section('stylesheet')

@endsection

@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div id="chart_penjualan_tahunan">
                                <p class="labels"></p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function(){
            var data  = {!! $item !!};
            var morrisData = [];
            var randomWarna = [];

            function getRandomColor() {
                var letters = '0123456789ABCDEF';
                var color = '#';
                for (var i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                return color;
            }

            $.each(data, function(key, value){
                var aAja = getRandomColor();
                morrisData.push({
                    key     : key   !== undefined ? key : ' ',
                    value   : value !== undefined ? value : 0,
                });
                randomWarna.push( aAja );
            });
            
            Morris.Bar({
                element : 'chart_penjualan_tahunan',
                data    : morrisData,
                xkey    : 'key',
                ykeys   : ['value'],
                labels  : ['Total'],
                barColors: function (row, series, type) {
                    return randomWarna[row.x];
                }, 
                parseTime: false,
            });
        });
  </script>
@endsection
