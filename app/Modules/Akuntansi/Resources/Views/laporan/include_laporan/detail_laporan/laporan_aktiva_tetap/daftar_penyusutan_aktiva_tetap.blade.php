@extends('akuntansi::laporan.laporan_khusus')
@section('title')
    {{ $title }}
@endsection
<style type="text/css">
    .td-blue-left-bold {
        text-align: left;
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
    }
    .td-blue-bold {
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
        text-align: left;
    }
</style>
@section('laporan_khusus')
    @if ($item->isEmpty())
        <section class="sheet padding-0mm">
            <article>
                <div class="alert alert-warning" style="text-align:center;">
                    Tidak ada data {{ $title }} yang tersedia <br>
                    {{  $subTitle  }}.
                </div>
        </section>
    @else
        @foreach ($item as $item_aktiva)
            <section class="sheet padding-0mm">
                <article>
                    <table class="table">
                        <tr>
                            <td colspan=10>
                                <div style="text-align:center;">
                                    <p><h4> {{ $perusahaan }}   </h4></p>
                                    <p><h6> {{ $title }}        </h6></p>
                                    <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                                    <p><b>  {{ $subTitle   }}   </b></p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">Kode Aktiva Tetap</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['kode_aktiva'] }}</td>
                            <td colspan="2">Estimasi Umur</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['estimasi_umur'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Nama Tipe Aktiva Tetap</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['nama_tipe_aktiva'] }}</td>
                            <td colspan="2">Metode Penyusutan</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['metode_penyusutan'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Nama Aktiva Tetap</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['nama_aktiva'] }}</td>
                            <td colspan="2">Harga Perolehan</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ number_format($item_aktiva->first()['harga_perolehan'], 2) }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Tanggal Pembelian</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['tgl_pembelian'] }}</td>
                            <td colspan="2">Penyusutan</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ number_format($item_aktiva->first()['penyusutan_tahun'], 2) }}</td>
                        </tr>
                        <tr class="border-bottom">
                            <td colspan="2">Tanggal Pemakaian</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ $item_aktiva->first()['tgl_pemakaian'] }}</td>
                            <td colspan="2">Penyusutan</td>
                            <td style="width:1px !important;">:</td>
                            <td colspan="2">{{ number_format($item_aktiva->first()['penyusutan_bulan'], 2) }}</td>
                        </tr>
                        <tr style="font-weight: bold; text-align:center;">
                            <td colspan="4"></td>
                            <td class="border-bottom">Tahun</td>
                            <td class="border-bottom">Penyusutan</td>
                            <td colspan="4"></td>
                        </tr>
                        @foreach ($item_aktiva as $key => $data_aktiva)
                            @foreach ($data_aktiva['collectionW'] as $item_key => $item_aktiva)
                                <tr style="text-align:center;">
                                    <td colspan="4"></td>
                                    <td>{{ $item_key }}</td>
                                    <td>{{ number_format($item_aktiva->sum('penyusutan'), 2) }}</td>
                                    <td colspan="4"></td>
                                </tr>
                            @endforeach    
                        @endforeach
                    </table>
                </article>
            </section>
        @endforeach
    @endif
@endsection