@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Rincian Jurnal Pembiayaan Pesanan
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12" align="center">
            <p><h4>CHICHI</h4></p>
            <p><h6>Rincian Jurnal Pembiayaan Pesanan</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>Dar 01 Mei 2018 Ke 30 Mei 2018</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table">
                <tr>
                    <td>No. Akun</td>
                    <td>Nama Akun</td>
                    <td>Debit (Domestik)</td>
                    <td>Kredit (Domestik)</td>
                    <td>No. Departemen</td>
                </tr>
                <!-- Perulangan Akun -->
                    <tr>
                        <td>1101</td>
                        <td colspan="4">KAS</td>
                    </tr>
                    <!-- Perulangan Rincian Jurnal Pembiayaan Pesanan -->
                        <tr>
                            <td>1101-001</td>
                            <td>Kas Kecil</td>
                            <td>0</td>
                            <td>0</td>
                            <td></td>
                        </tr>
                    <!-- Akhir Perulangan Rincian Jurnal Pembiayaan Pesanan -->
                <!-- Akhir Perulangan Akun -->
                <tr>
                    <td colspan="2"></td>
                    <td class="border-top">0</td>
                    <td class="border-top">0</td>
                </tr>
            </table>
        </div>
    </div>
@endsection