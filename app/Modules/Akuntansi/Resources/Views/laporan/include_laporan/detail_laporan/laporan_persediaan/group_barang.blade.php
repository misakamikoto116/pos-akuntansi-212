@extends('akuntansi::laporan.laporan_khusus')
@section('title')
    {{ $title }}
@endsection
<style type="text/css">
    .td-blue-left-bold {
        text-align: left;
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
    }
    .td-blue-bold {
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
        text-align: left;
    }
</style>
@section('laporan_khusus')
    @if($item->isEmpty())
        <tr>
            <td colspan=11 align="center">
                <div class="alert alert-warning">
                    Tidak ada data {{ $title }} yang tersedia <br>
                    pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                </div>
            </td>
        </tr>
    @else
        @foreach($item as $item_key => $dataGroupBarang)
            <section class="sheet padding-0mm">
                <article>
                    <div class="row">
                        <div class="col-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td colspan=11>
                                            <p><h4> {{ $perusahaan }} </h4></p>
                                            <p><h6> {{ $title }} </h6></p>
                                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="text-align:left; padding:5px;">No. Grup</td>
                                        <td>:</td>
                                        <td colspan=5 style="text-align:left; padding:5px;">{{ $item_key }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="text-align:left; padding:5px;">Keterangan</td>
                                        <td>:</td>
                                        <td colspan=5 style="text-align:left; padding:5px;">{{ $dataGroupBarang[0]['keterangan'] }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="text-align:left; padding:5px;">Cetak Barang</td>
                                        <td>:</td>
                                        <td colspan=5 style="text-align:left; padding:5px;">{{ $dataGroupBarang[0]['cetak'] }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="text-align:left; padding:5px;">Def. Harga Jual 1</td>
                                        <td>:</td>
                                        <td colspan=5 style="text-align:left; padding:5px;">{{ $dataGroupBarang[0]['harga_jual_1'] }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="text-align:left; padding:5px;">Pajak Jual</td>
                                        <td>:</td>
                                        <td colspan=5 style="text-align:left; padding:5px;">{{ $dataGroupBarang[0]['akun_pajak_jual'] }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="text-align:left; padding:5px;">Non-aktif</td>
                                        <td>:</td>
                                        <td colspan=5 style="text-align:left; padding:5px;">{{ $dataGroupBarang[0]['status'] }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="text-align:left; padding:5px;">Akun Penjualan</td>
                                        <td>:</td>
                                        <td colspan=5 style="text-align:left; padding:5px;">{{ $dataGroupBarang[0]['akun_penjualan'] }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="text-align:left; padding:5px;">Akun Retur Penjualan</td>
                                        <td>:</td>
                                        <td colspan=5 style="text-align:left; padding:5px;">{{ $dataGroupBarang[0]['akun_retur'] }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="text-align:left; padding:5px;">Akun Diskon Penjualan Barang</td>
                                        <td>:</td>
                                        <td colspan=5 style="text-align:left; padding:5px;">{{ $dataGroupBarang[0]['akun_diskon'] }}</td>
                                    </tr>
                                    <tr class="border-top">
                                        <th>No. Barang</th>
                                        <th>Keterangan Barang</th>
                                        <th>Kuantitas</th>
                                        <th>Satuan</th>
                                        <th>Def. Harga Jual 1</th>
                                        <th>Def. Harga Jual 2</th>
                                        <th>Def. Harga Jual 3</th>
                                        <th>Def. Harga Jual 4</th>
                                        <th>Def. Harga Jual 5</th>
                                    </tr>
                                </thead>
                                    @foreach($dataGroupBarang as $dataGroup)
                                        <tr>
                                            <td>{{ $dataGroup['no_barang']                           ?? null }}</td>
                                            <td>{{ $dataGroup['nm_barang']                           ?? null }}</td>
                                            <td>{{ $dataGroup['kuantitas']                           ?? null }}</td>
                                            <td>{{ $dataGroup['satuan']                              ?? null }}</td>
                                            <td>{{ number_format($dataGroup['harga_jual_1'])         ?? null }}</td>
                                            <td>{{ number_format($dataGroup['harga_jual_2'])         ?? null }}</td>
                                            <td>{{ number_format($dataGroup['harga_jual_3'])         ?? null }}</td>
                                            <td>{{ number_format($dataGroup['harga_jual_4'])         ?? null }}</td>
                                            <td>{{ number_format($dataGroup['harga_jual_5'])         ?? null }}</td>
                                        </tr>
                                    @endforeach
                            </table>
                        </div>
                    </div>
                </article>
            </section>
        @endforeach
    @endif
@endsection