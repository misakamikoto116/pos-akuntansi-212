@if (isset($formFilter))
    @if (isset($typeFilter))
        <div>
            {{-- Double date --}}
            @if (in_array('double_date', $typeFilter))
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        <input type="month" name="date_start" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        <input type="month" name="date_end" class="form-control">
                    </div>
                </div>
            @endif
            {{-- Single Date --}}
            @if (in_array('single_date', $typeFilter))
                <div class="form-group row">
                    {!! Form::label('Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control']) !!}
                    </div>
                </div>
            @endif
            {{-- Single Date --}}
            @if (in_array('double_date_not_month', $typeFilter))
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-rincian-buku-besar']) !!}          
                    </div>
                </div>
            @endif
            {{-- Tampil pemasok transaksi --}}
            @if (in_array('pemasok_transaksi', $typeFilter))
                <div>
                    <div>
                        <input name="pemasok_transaksi"  id="pemasok_transaksi" type="checkbox">
                        <label for="pemasok_transaksi">
                            Hanya menampilkan pemasok mempunyai transaksi
                        </label>
                    </div>
                </div>
            @endif
        </div>
    @endif
@endif