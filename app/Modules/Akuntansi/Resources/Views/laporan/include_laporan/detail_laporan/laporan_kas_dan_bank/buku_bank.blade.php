@extends('akuntansi::laporan.laporan_khusus')
@section('title')
    {{ $title }}
@endsection
<style type="text/css">
    .td-blue-left-bold {
        font-size: 13px;
        text-align: left;
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
    }
    .td-blue-bold {
        font-size: 13px;
        color: #0066cc;
        font-weight: bold; 
        line-height: 15px;
        text-align: left;
    }
</style>
@section('laporan_khusus')
@foreach ($items as $data_akun)
    <section class="sheet padding-0mm">
        <article>
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead>
                        <tr>
                            <td colspan=6>
                                <p><h4>{{ $nama_perusahaan }}</h4></p>
                                <p><h6>{{ $title }}</h6></p>
                                <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                                <p>Dari {{ $tanggal_awal }} ke {{ $tanggal_akhir }}</p>
                            </td>
                        </tr>
                        <tr>
                            <tr>
                                <td colspan="2" class="td-blue-left-bold">No. Akun</td>
                                <td class="td-blue-bold">: {{ $data_akun['no_akun'] ?? null }}</td>
                                <td colspan="2" class="td-blue-left-bold">Nama Mata Uang</td>
                                <td class="td-blue-bold">: {{ $data_akun['mata_uang'] ?? null }}</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="td-blue-left-bold">Nama Akun</td>
                                <td class="td-blue-bold">: {{ $data_akun['nama_akun'] ?? null }}</td>
                            </tr>
                        </tr>

                        <tr>
                            <th>Tanggal</th>
                            <th>No. Sumber</th>
                            <th>Keterangan</th>
                            <th>Penambahan</th>
                            <th>Pengurangan</th>
                            <th style="text-align: center;">Saldo</th>
                        </tr>
                    </thead>
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="3">Saldo Awal</td>
                        <td style="text-align: right;">{{ number_format($data_akun['saldo_awal']) }}</td>
                    </tr>
                    <!-- Perulangan Group Mata Uang (IDR, SGD, USD)-->
                    @foreach ($data_akun['transaksi'] as $data_transaksi)
                        <tr>
                            <td>{{ $data_transaksi['tanggal'] ?? null }}</td>
                            <td>{{ $data_transaksi['no_sumber'] ?? null }}</td>
                            <td>{{ $data_transaksi['keterangan'] }}</td>
                            <td style="text-align: right;">{{ number_format($data_transaksi['penambahan']) }}</td>
                            <td style="text-align: right;">{{ number_format($data_transaksi['pengurangan']) }}</td>
                            <td style="text-align: right; color: {{ $data_transaksi['css_saldo'] }}">{{ number_format($data_transaksi['saldo']) }}</td>
                        </tr>
                    @endforeach
                    <!-- Akhir Perulangan Group Mata Uang-->
                    <tr>
                        <td colspan="3"></td>
                        <td class="border-top" style="text-align: right;">{{ number_format($data_akun['sum_penambahan']) }}</td> <!-- Jumlahkan semua saldo asing pada seluruh group -->
                        <td class="border-top" style="text-align: right;">{{ number_format($data_akun['sum_pengurangan']) }}</td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        </article>
    </section>
@endforeach
@endsection