@extends('akuntansi::laporan.laporan_khusus')
@section('title')
    {{ $title }}
@endsection
<style type="text/css">
    .td-blue-left-bold {
        text-align: left;
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
    }
    .td-blue-bold {
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
        text-align: left;
    }
</style>
@section('laporan_khusus')

    @foreach ($items as $data_pelanggan)
        <section class="sheet padding-0mm">
            <article>
                <div class="row">
                    <div class="col-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td colspan=6>
                                        <p><h4>{{ $nama_perusahaan }}</h4></p>
                                        <p><h6>{{ $title }}</h6></p>
                                        <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                                        <p>Dari {{ $tanggal_awal }} ke {{ $tanggal_akhir }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td-blue-left-bold">Nama Pelanggan</td>
                                    <td class="td-blue-bold">: {{ $data_pelanggan->nama ?? null }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td-blue-left-bold">Alamat 1</td>
                                    <td class="td-blue-bold">: {{ $data_pelanggan->alamat ?? null }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td-blue-left-bold">Nama Kontak</td>
                                    <td class="td-blue-bold">: </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td-blue-left-bold">Telepon</td>
                                    <td class="td-blue-bold">: {{ $data_pelanggan->telepon ?? null }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td-blue-left-bold">Faksimili</td>
                                    <td class="td-blue-bold">: </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td-blue-left-bold">E-Mail</td>
                                    <td class="td-blue-bold">: {{ $data_pelanggan->email ?? null }}</td>
                                </tr>
                                <tr>
                                    <th width="15%">No. Sumber</th>
                                    <th>Tanggal</th>
                                    <th style="text-align: center;">Keterangan</th>
                                    <th style="text-align: center;">Penambahan</th>
                                    <th style="text-align: center;">Pengurangan</th>
                                    <th style="text-align: center;">Saldo</th>
                                </tr>
                            </thead>
                            <tr>
                                <td colspan="5"><strong>Base Currency(IDR)</strong></td>
                                <td style="text-align: right;"><strong>{{ number_format($data_pelanggan->sum_pelanggan) }}</strong></td>
                            </tr>
                            <!-- Perulangan Laporan Pelanggan -->
                                @foreach ($data_pelanggan->data as $data_faktur)
                                    <tr>
                                        <td width="10%">{{ $data_faktur['no_sumber'] ?? null }}</td>
                                        <td>{{ $data_faktur['tanggal'] ?? null }}</td>
                                        <td>{{ $data_faktur['keterangan_sumber'] ?? null }}</td>
                                        <td style="text-align: right;">{{ number_format($data_faktur['penambahan']) ?? null }}</td>
                                        <td style="text-align: right;">{{ number_format($data_faktur['pengurangan']) ?? null }}</td>
                                        <td style="text-align: right;">{{ number_format($data_faktur['saldo']) }}</td>
                                    </tr>
                                @endforeach
                            <!-- Akhir Perulangan Laporan Pelanggan -->
                                <tr>
                                    <td colspan="3"></td>
                                    <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">{{ number_format($data_pelanggan->sum_penambahan) ?? null }}</td>
                                    <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">{{ number_format($data_pelanggan->sum_pengurangan) ?? null }}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                    <td colspan="2" style="color: #0066cc; font-weight: bold;">{{ $data_pelanggan->nama ?? null }}</td> <!-- Nama Pelanggan -- >
                                </tr>
                                <!-- Nilai total dan total penurunan harus balance -->
                                <tr>
                                    <td colspan="4"></td>
                                    <td colspan="2" style="color: #0066cc; font-weight: bold;">BASE CURRENCY (IDR)</td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                    <td style="color: #0066cc;">Saldo Awal</td>
                                    <td style="color: #0066cc;">{{ number_format($data_pelanggan->sum_pelanggan) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                    <td style="color: #0066cc;">Total</td>
                                    <td style="color: #0066cc;">{{ number_format($data_pelanggan->sum_penambahan) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                    <td style="color: #0066cc;">Total Penurunan</td>
                                    <td style="color: #0066cc;">{{ number_format($data_pelanggan->sum_pengurangan) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                    <td style="color: #0066cc;">Perubahan Bersih</td>
                                    <td style="color: #0066cc;">{{ number_format($data_pelanggan->perubahan_bersih) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                    <td style="color: #0066cc;">Saldo Akhir</td>
                                    <td style="color: #0066cc;">{{ number_format($data_pelanggan->perubahan_bersih) }}</td>
                                </tr>
                        </table>
                    </div>
                </div>
            </article>
        </section>
    @endforeach
@endsection