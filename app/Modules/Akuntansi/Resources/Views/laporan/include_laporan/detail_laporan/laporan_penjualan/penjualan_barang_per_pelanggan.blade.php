@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Penjualan Barang per Pelanggan
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=2>
                            <p><h4>CHICHI</h4></p>
                            <p><h6>Penjualan Barang per Pelanggan</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari 01 Mei 2018 Ke 30 Mei 2018</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan Barang</th>
                        <th>Cash IDR</th>
                    </tr>
                </thead>
                <!-- Perulangan Penjualan Barang per Pelanggan -->
                <tr>
                    <td>Jasa Pemasangan</td>
                    <td>6.000.000</td>
                </tr>
                <tr>
                    <td>Jasa Perbaikan</td>
                    <td>1.875.000</td>
                </tr>
                <tr>
                    <td>Jasa Tukang</td>
                    <td>24.300.000</td>
                </tr>
                <tr>
                    <td>Tagihan Proyek</td>
                    <td></td>
                </tr>
                <!-- Akhir Perulangan Penjualan Barang per Pelanggan -->
                <tr>
                    <th>Grand Total</th>
                    <th>32.175.000</th>
                    <th></th>
                </tr>
            </table>
        </div>
    </div>
@endsection