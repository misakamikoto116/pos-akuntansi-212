@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari Tanggal <b> {{ $dari_tanggal }}</b> Ke <b> {{ $ke_tanggal }}</b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. PO</th>
                        <th>Tanggal Faktur</th>
                        <th>Kts. Dipesan</th>
                        <th>Nama Pemasok</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=4 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    pada tanggal {{ $dari_tanggal }}.
                                </div>
                            </td>
                        </tr>
                    @else
                    @php $totalQty = 0; @endphp
                    @foreach($item as $item_key => $dataPermintaanPembelian)
                        <tr>
                            <td>{{ $item_key }}</td>
                            <td>{{ $dataPermintaanPembelian[0]['tanggal'] }}</td>
                            <td colspan=2>{{ $dataPermintaanPembelian[0]['status'] }}</td>
                        </tr>
                        @php $subTotalQty = 0; @endphp
                            @foreach($dataPermintaanPembelian as $dataPermintaan)
                                <tr>
                                    <td style="padding-left : 25px;">
                                        {{ $dataPermintaan['nobarang']      ??   null  }}
                                    </td>
                                    <td>{{ $dataPermintaan['qty']           ??   null  }}</td>
                                    <td>{{ $dataPermintaan['satuan']        ??   null }}</td>
                                    <td>{{ $dataPermintaan['valqty']        ??   null  }}</td>
                                </tr>
                                @if($dataPermintaan['nopo'] != null)
                                    <tr>
                                        <td style="padding-left : 50px;">
                                            {{ $dataPermintaan['nopo']          ??   null  }}
                                        </td>
                                        <td>{{ $dataPermintaan['tglpesan']      ??   null  }}</td>
                                        <td>{{ $dataPermintaan['qtypesan']      ??   null }}</td>
                                        <td>{{ $dataPermintaan['nmpemasok']     ??   null  }}</td>
                                    </tr>
                                @php $subTotalQty += $dataPermintaan['qtypesan']; @endphp
                                @else
                                <tr>
                                    <td colspan=2></td>
                                    <td>{{$dataPermintaan['qtypesan'] ?? 0}}</td>
                                    <td colspan=2></td>
                                </tr>
                                @php $subTotalQty += $dataPermintaan['qtypesan']; @endphp
                            @endif
                            <tr>
                                <td colspan="2"></td>
                                <td class="border-top">{{   $subTotalQty       }}</td>
                                <td colspan=2></td>
                            </tr>
                            @endforeach
                        @endforeach
                    @endif
                </tbody>                
            </table>
        </div>
    </div>
@endsection