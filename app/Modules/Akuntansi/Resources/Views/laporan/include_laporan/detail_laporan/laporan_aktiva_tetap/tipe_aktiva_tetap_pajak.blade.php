@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                        <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tipe Aktiva Tetap Pajak</th>
                        <th>Metode Penyusutan</th>
                        <th>Estimasi Umur</th>
                        <th>Nilai Tukar Fiscal</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=4 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia. <br>
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($item as $dataTipeAktivakTetapPajak)
                            <tr>
                                <td>{{ $dataTipeAktivakTetapPajak['tipeAktiva'] }}</td>
                                <td>{{ $dataTipeAktivakTetapPajak['metode'] }}</td>
                                <td>{{ $dataTipeAktivakTetapPajak['estimasi'] }}</td>
                                <td>{{ $dataTipeAktivakTetapPajak['nilai_tukar'] }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection