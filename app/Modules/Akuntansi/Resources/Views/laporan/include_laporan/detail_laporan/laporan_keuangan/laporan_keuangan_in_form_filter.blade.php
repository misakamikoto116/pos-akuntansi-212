@if (isset($formFilter))
    @if (isset($typeFilter))
        <div>
            {{-- Double date --}}
            @if (in_array('double_date', $typeFilter))
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        <input type="month" name="date_start" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        <input type="month" name="date_end" class="form-control">
                    </div>
                </div>
            @endif
            {{-- Single Date --}}
            @if (in_array('single_date', $typeFilter))
                <div class="form-group row">
                    {!! Form::label('Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control']) !!}
                    </div>
                </div>
            @endif
            {{-- Tampil total --}}
            @if (in_array('tampil_total', $typeFilter))
                <div>
                    <div>
                        <input name="tampil_total"  id="tampil_total" type="checkbox">
                        <label for="tampil_total">
                            Tampilkan Total
                        </label>
                    </div>
                </div>
            @endif
            {{-- Tampil induk --}}
            @if (in_array('tampil_induk', $typeFilter))
                <div>
                    <div>
                        <input name="tampil_induk" id="tampil_induk" checked="" type="checkbox">
                        <label for="tampil_induk">
                            Tampilkan Induk
                        </label>
                    </div>
                </div>
            @endif
            {{-- Tampil anak --}}
            @if (in_array('tampil_anak', $typeFilter))
                <div>
                    <div>
                        <input id="tampil_anak" name="tampil_anak" checked="" type="checkbox">
                        <label for="tampil_anak">
                            Tampilkan Anak
                        </label>
                    </div>
                </div>
            @endif
            {{-- Tampil Saldo nol --}}
            @if (in_array('tampil_saldo_nol', $typeFilter))
                <div>
                    <div>
                        <input name="saldo_nol" id="saldo_nol" checked type="checkbox">
                        <label for="saldo_nol">
                            Termasuk Saldo nol
                        </label>
                    </div>
                </div>
            @endif
            {{-- Tampil jumlah induk --}}
            @if (in_array('tampil_jumlah_induk', $typeFilter))
                <div>
                    <div>
                        <input name="tampil_jumlah_induk" id="tampil_jumlah_induk" checked="" type="checkbox">
                        <label for="tampil_jumlah_induk">
                            Tampilkan Jumlah Induk
                        </label>
                    </div>
                </div>
            @endif
        </div>
    @endif
@endif