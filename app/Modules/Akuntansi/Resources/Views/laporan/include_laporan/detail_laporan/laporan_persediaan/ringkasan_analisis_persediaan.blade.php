@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=8>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b> Per {{ $dari_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Deskripsi Barang</th>
                        <th>Ditangan</th>
                        <th>Dipesan</th>
                        <th>Dijual</th>
                        <th>Penjualan/Minggu</th>
                        <th>Kts Stok</th>
                        <th>Unit</th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=8 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @foreach($item as $item_key => $dataHistoriTingkat)
                        @foreach($dataHistoriTingkat as $dataHistori)                            
                            <tr>
                                <td>{{ $dataHistori['nama_barang']        ?? null }}</td>
                                <td>{{ $dataHistori['ditangan']           ?? null }}</td>
                                <td>{{ $dataHistori['dipesan']            ?? null }}</td>
                                <td>{{ $dataHistori['dijual']             ?? null }}</td>
                                <td>{{ $dataHistori['akumulasi']          ?? null }}</td>
                                @if($dataHistori['kts_stok'] >= 0 )
                                    <td>
                                        {{ $dataHistori['kts_stok']           ?? null }}
                                    </td>
                                @elseif($dataHistori['kts_stok'] < 0 )
                                    <td style="color: red;">
                                        {{ $dataHistori['kts_stok']           ?? null }}
                                    </td>
                                @endif
                                <td>{{ $dataHistori['satuan']             ?? null }}</td>
                            </tr>                            
                        @endforeach
                    @endforeach
                    <tr>
                        <td>&nbsp;</td>
                        <td class="border-top">{{ $dataHistori['sum_ditangan']           ?? null }}</td>
                        <td class="border-top">{{ $dataHistori['sum_dipesan']            ?? null }}</td>
                        <td class="border-top">{{ $dataHistori['sum_dijual']             ?? null }}</td>
                        <td class="border-top">{{ $dataHistori['sum_akumulasi']          ?? null }}</td>
                        <td class="border-top">{{ $dataHistori['sum_kts_stok']           ?? null }}</td>
                        <td>&nbsp;</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection