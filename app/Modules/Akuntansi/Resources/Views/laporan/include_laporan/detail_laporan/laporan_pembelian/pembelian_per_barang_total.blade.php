@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=2>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan Barang</th>
                        <th><b> {{ $dari_tanggal }} </b> - <b> {{ $ke_tanggal }} </b></th>
                    </tr>
                </thead>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=2 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php $total = 0; @endphp
                        @foreach($item as $rs => $key)
                            @php $subTotal = 0; @endphp
                            @foreach($key as $kunci)
                                <tr>
                                    <td>{{ $kunci['nama']                   ?? null }}</td>
                                    <td>{{ number_format($kunci['total'])   ?? null }}</td>
                                </tr>
                                @php $subTotal += $kunci['total']; @endphp
                            @endforeach
                            @php $total += $subTotal; @endphp
                        @endforeach
                        <tr>
                            <td align="right">Total</td>
                            <td class="border-top">{{ number_format($total) }}</td>
                        </tr>
                    @endif
            </table>
        </div>
    </div>
@endsection