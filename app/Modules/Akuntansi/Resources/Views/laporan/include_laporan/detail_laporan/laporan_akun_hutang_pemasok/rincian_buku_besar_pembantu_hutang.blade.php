@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title = 'Laporan Rincian Buku Besar Pembantu Hutang'}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=8>
                            <p><h4>{{ $nama_perusahaan->nama_perusahaan ?? null }}</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir ?? null }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>Sumber</th>
                        <th>No. Faktur</th>
                        <th>Keterangan</th>
                        <th>Nilai (Asing)</th>
                        <th>Nilai Pajak</th>
                        <th>Saldo (Asing)</th>
                        <th>Saldo Pajak</th>
                    </tr>
                </thead>
                <!-- Perulangan Mata Uang -->
                    <tr>
                        <td colspan="8"><strong>IDR</strong></td>
                    </tr>
                        <!-- Perulangan Rincian Buku Besar Pembantu Hutang -->
                        @foreach ($items as $data_pemasok)
                            <tr>
                                <td><strong>{{ $data_pemasok->no_pemasok ?? null }}</strong></td>
                                <td colspan="5"><strong>{{ $data_pemasok->nama ?? null }}</strong></td>
                                <td><strong></strong>{{ number_format($data_pemasok->detailInformasiPemasok->sum('saldo_awal')) }}</td>
                                <td><strong></strong>0</td>
                            </tr>
                            
                            @php
                                $saldo_asing        = $data_pemasok->detailInformasiPemasok->sum('saldo_awal');
                                $sum_penambahan     = 0;
                                $sum_penurunan      = 0;
                            @endphp

                            @foreach ($data_pemasok['data'] as $data_hutang)
                                @php
                                    $saldo_asing            = $saldo_asing + $data_hutang['nilai_sumber'];
                                    if ($data_hutang['nilai_sumber'] < 0) {
                                        $sum_penurunan       += $data_hutang['nilai_sumber'];
                                    }else {
                                        $sum_penambahan       += $data_hutang['nilai_sumber'];
                                    }
                                @endphp
                                <tr>
                                    <td>{{ Carbon\Carbon::parse($data_hutang['tanggal'])->format('d F Y') ?? null }}</td>
                                    <td>{{ $data_hutang['sumber'] ?? null }}</td>
                                    <td>{{ $data_hutang['no_sumber'] ?? null }}</td>
                                    <td>{{ $data_hutang['keterangan_sumber'] ?? null }}</td>
                                    @if ($data_hutang['nilai_sumber'] < 0)
                                        <td style="color: red;">{{ number_format($data_hutang['nilai_sumber']) ?? null }}</td>
                                    @else
                                        <td>{{ number_format($data_hutang['nilai_sumber']) ?? null }}</td>
                                    @endif
                                    <td>{{ $data_hutang['nilai_pajak'] ?? null }}</td>
                                    @if ($saldo_asing < 0)
                                        <td style="color: red;">{{ number_format($saldo_asing) ?? null }}</td>
                                    @else
                                        <td>{{ number_format($saldo_asing) ?? null }}</td>
                                    @endif
                                    <td>{{ $data_hutang['saldo_pajak'] ?? null }}</td>
                                </tr>
                            @endforeach
                            @php
                                $perubahan_bersih = $sum_penambahan + $sum_penurunan;
                            @endphp
                            <tr>
                                <td colspan="4" style="color: #0066cc; font-weight: bold;">Total Penambahan</td>
                                <td class="border-top" style="color: #0066cc; font-weight: bold;">{{ number_format($sum_penambahan) ?? null }}</td>
                                <td class="border-top" style="color: #0066cc; font-weight: bold;">0</td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="4" style="color: #0066cc; font-weight: bold;">Total Penurunan</td>
                                <td style="color: #0066cc; font-weight: bold;">{{ number_format($sum_penurunan) ?? null }}</td>
                                <td style="color: #0066cc; font-weight: bold;">0</td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="4" style="color: #0066cc; font-weight: bold;">Perubahan Bersih</td>
                                <td class="border-top" style="color: #0066cc; font-weight: bold;">{{ number_format($perubahan_bersih) ?? null }}</td>
                                <td class="border-top" style="color: #0066cc; font-weight: bold;">0</td>
                                <td colspan="2"></td>
                            </tr>
                        @endforeach
                        <!-- Akhir Perulangan Rincian Buku Besar Pembantu Hutang -->
                <!-- Akhir Perulangan Mata Uang -->
            </table>
        </div>
    </div>
@endsection