@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4>{{ $perusahaan }}</h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tipe Proses</th>
                        <th>No. Faktur</th>
                        <th>Tanggal Faktur</th>
                        <th>Kuantitas Faktur</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=4 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    {{ $subTitle   }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($item as $item_key => $dataHistoriPengirimanPenjualan)
                            <tr>    
                                <td>{{ $dataHistoriPengirimanPenjualan[0]['nopelanggan']       ??  null    }}</td>
                                <td>{{ $dataHistoriPengirimanPenjualan[0]['tglpengiriman']        ??  null    }}</td>
                                <td>{{ $item_key }}</td>
                                <td>@if($dataHistoriPengirimanPenjualan[0]['status'] == 0 ) TerFakturkan @endif</td>
                            </tr>
                            @foreach($dataHistoriPengirimanPenjualan as $dataHistoriPengirimanPenjualan_key => $dataHistoriPengiriman)                                
                                <tr>
                                    <td style="padding: 12px 25px !important;">{{ $dataHistoriPengiriman['nobarang']   ??  null }}</td>
                                    <td>{{ $dataHistoriPengiriman['nmbarang']       ??  null    }}</td>
                                    <td></td>
                                    <td>
                                        {{ $dataHistoriPengiriman['qty'] ?? null }} / {{ $dataHistoriPengiriman['unit']     ??   null }}
                                        <div style="float:right;">0<div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><div style="float: right;">Faktur Penjualan</div></td>
                                    <td>{{ $dataHistoriPengiriman['nofaktur']   ??  null  }}</td>
                                    <td>{{ $dataHistoriPengiriman['tglfaktur']  ??  null  }}</td>
                                    <td><div style="float:right">{{ $dataHistoriPengiriman['qty'] ?? null }}</div></td>
                                </tr>
                                <tr>
                                    <td colspan=3></td>
                                    <td class="border-top" align="right">{{ $dataHistoriPengiriman['qty'] ?? null }}</td>
                                </tr>
                            @endforeach
                                <tr>
                                    <td colspan=4>&nbsp;</td>
                                </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection