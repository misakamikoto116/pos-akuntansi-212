@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan='6'>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <td>No. Faktur</td>
                        <td>Tanggal Faktur</td>
                        <td>Katerangan</td>
                        <td>Kuantitas</td>
                        <td>Unit 1 Barang</td>
                        <td>Jumlah</td>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=6 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php $totalQty = 0; $totalHarga = 0; @endphp
                        @foreach($item as $item_key => $dataRincianPembelianBarang)
                            <tr>
                                <td>{{ $item_key ?? null }}</td>
                                <td colspan=5>{{ $dataRincianPembelianBarang[0]['namaproduk'] }}</td>
                            </tr>
                            @php $subTotalQty = 0; $subTotalHarga = 0; @endphp
                            @foreach($dataRincianPembelianBarang as $dataRincianPembelian)
                                <tr>
                                    <td>{{ $dataRincianPembelian['no_faktur']              ?? null }}</td>
                                    <td>{{ $dataRincianPembelian['tanggal_faktur']         ?? null }}</td>
                                    <td>{{ $dataRincianPembelian['keterangan']             ?? null }}</td>
                                    <td>{{ $dataRincianPembelian['qty'] }}</td>
                                    <td>{{ $dataRincianPembelian['unit'] }}</td>
                                    <td>{{ $dataRincianPembelian['total']   ?? null }}</td>
                                </tr>
                                @php $subTotalQty += $dataRincianPembelian['qty']; $subTotalHarga += $dataRincianPembelian['total']; @endphp
                            @endforeach
                            <tr>
                                <td colspan=3></td>
                                <td class="border-top">{{ $subTotalQty }}</td>
                                <td></td>
                                <td class="border-top">{{ number_format($subTotalHarga) }}</td>
                            </tr>
                            @php $totalQty += $subTotalQty; $totalHarga += $subTotalHarga; @endphp
                        @endforeach
                        <tr>
                            <td colspan=2></td>
                            <td>Total Kuantitas</td>
                            <td class="border-top">{{ $totalQty }}</td>
                            <td>Total Harga</td>
                            <td class="border-top">{{ number_format($totalHarga) }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection