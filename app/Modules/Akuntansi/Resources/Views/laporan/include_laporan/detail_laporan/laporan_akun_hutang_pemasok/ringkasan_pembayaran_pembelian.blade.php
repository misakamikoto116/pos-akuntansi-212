@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title='Laporan Ringkasan Pembayaran Pembelian'}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=7>
                            <p><h4>{{ $nama_perusahaan->nama_perusahaan ?? null }}</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir ?? null }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Form</th>
                        <th>Tanggal Pembayaran</th>
                        <th>No. Cek</th>
                        <th>Tanggal Cek</th>
                        <th>Nama Pemasok</th>
                        <th>Jumlah Cek</th>
                        <th>Nilai Tukar</th>
                    </tr>
                </thead>
                <!-- Perulangan Tipe Mata Uang-->
                    <tr>
                        <td colspan="8"><strong>IDR</strong></td>
                    </tr>
                    <!-- Perulangan Ringkasan Pembayaran Pembelian -->
                        @foreach ($items as $data_pembayaran)
                            <tr>
                                <td>{{ $data_pembayaran->form_no ?? null }}</td>
                                <td>{{ Carbon\Carbon::parse($data_pembayaran->payment_date)->format('d F Y') ?? null }}</td>
                                <td>{{ $data_pembayaran->cheque_no ?? null }}</td>
                                <td>{{ Carbon\Carbon::parse($data_pembayaran->cheque_date)->format('d F Y') ?? null }}</td>
                                <td>{{ $data_pembayaran->pemasok->nama ?? null }}</td>
                                <td>{{ number_format($data_pembayaran->cheque_amount) }}</td>
                                <td>{{ $data_pembayaran->rate }}</td>
                            </tr>
                        @endforeach
                    <!-- Akhir Perulangan Ringkasan Pembayaran Pembelian -->
                    <tr>
                        <td colspan="5" style="color: #0066cc; font-weight: bold;">Total dari IDR</td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">{{ number_format($items->sum('cheque_amount')) }}</td>
                        <td></td>
                    </tr>
                <!-- Akhir Perulangan -->
            </table>
        </div>
    </div>
@endsection