@extends('akuntansi::laporan.laporan_khusus')
@section('title')
    {{ $title }}
@endsection
<style type="text/css">
    .td-blue-left-bold {
        text-align: left;
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
    }
    .td-blue-bold {
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
        text-align: left;
    }
</style>
@section('laporan_khusus')
    @if($item->isEmpty())
        <section class="sheet padding-0mm">
            <article>
                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-warning" style="text-align: center;">
                            Tidak ada data {{ $title }} yang tersedia <br>
                            pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                        </div>
                    </div>
                </div>
            </article>
        </section>
    @else
        @foreach($item as $item_key => $dataTransaksi)
            <section class="sheet padding-0mm">
                <article>
                    <div class="row">
                        <div class="col-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td colspan=11>
                                            <p><h4> {{ $perusahaan }} </h4></p>
                                            <p><h6> {{ $title }} </h6></p>
                                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="text-align:left; padding:5px;">No. Barang</td>
                                        <td colspan=3 style="text-align:left; padding:5px;">{{ $item_key }}</td>
                                        <td></td>
                                        <td colspan=2 style="text-align:left; padding:5px;">Kts. Saldo Awal</td>
                                        <td colspan=3 style="text-align:left; padding:5px;">{{ number_format($dataTransaksi[0]['kts_saldo']) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="text-align:left; padding:5px;">Deskripsi Barang</td>
                                        <td colspan=3 style="text-align:left; padding:5px;">{{ $dataTransaksi[0]['nm_barang'] }}</td>
                                        <td></td>
                                        <td colspan=2 style="text-align:left; padding:5px;">Nilai Saldo Awal</td>
                                        <td colspan=3 style="text-align:left; padding:5px;">{{ number_format($dataTransaksi[0]['nilai_saldo']) }}</td>
                                    </tr>
                                    <tr class="border-top">
                                        <th>Tanggal</th>
                                        <th>Tipe</th>
                                        <th>No. Faktur</th>
                                        <th>Kts. Masuk</th>
                                        <th>Biaya/Unit Masuk</th>
                                        <th>Nilai Masuk</th>
                                        <th>Kts. Keluar</th>
                                        <th>Biaya/Unit Keluar</th>
                                        <th>Nilai Keluar</th>
                                        <th>Kuantitas</th>
                                        <th>Nilai Akhir</th>
                                    </tr>
                                </thead>
                                @php
                                    $ktsMasuk    = 0; $nilaiMasuk  = 0;
                                    $ktsKeluar   = 0; $nilaiKeluar = 0;
                                    $qty         = 0; $nilaiAkhir  = 0;
                                @endphp
                                @foreach($dataTransaksi as $data)
                                    @php
                                        $ktsMasuk    += $data['kts_masuk'];
                                        $nilaiMasuk  += $data['nilai_masuk'];
                                        $ktsKeluar   += $data['kts_keluar'];
                                        $nilaiKeluar += $data['nilai_keluar'];
                                        $qty          = $dataTransaksi[0]['kts_saldo']   + ($ktsMasuk + $ktsKeluar);
                                        $nilaiAkhir   = $dataTransaksi[0]['nilai_saldo'] + ($nilaiMasuk + $nilaiKeluar);
                                    @endphp
                                    <tr>
                                        <td>{{ $data['tanggal_transaksi']                        ?? null }}</td>
                                        <td>{{ $data['tipe_transaksi']                           ?? null }}</td>
                                        <td>{{ $data['no_faktur']                                ?? null }}</td>
                                        <td>{{ $data['kts_masuk']                                ?? null }}</td>
                                        <td>{{ number_format($data['biaya_nilai_masuk'])         ?? null }}</td>
                                        <td>{{ number_format($data['nilai_masuk'])               ?? null }}</td>
                                        <td>{{ abs($data['kts_keluar'])                          ?? null }}</td>
                                        <td>{{ number_format(abs($data['biaya_nilai_keluar']))   ?? null }}</td>
                                        <td>{{ number_format(abs($data['nilai_keluar']))         ?? null }}</td>
                                        <td>{{ $qty                                              ?? null }}</td>
                                        <td>{{ number_format($nilaiAkhir)                        ?? null }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan=3>&nbsp;</td>
                                    <td class="border-top">{{   number_format($ktsMasuk)            }}</td>
                                    <td>&nbsp;</td>
                                    <td class="border-top">{{   number_format($nilaiMasuk)          }}</td>
                                    <td class="border-top">{{   number_format(abs($ktsKeluar))      }}</td>
                                    <td>&nbsp;</td>
                                    <td class="border-top">{{   number_format(abs($nilaiKeluar))    }}</td>
                                    <td colspan=2>&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </article>
            </section>
        @endforeach
    @endif
@endsection