@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=5>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Pengajuan</th>
                        <th>Tgl Penawaran</th>
                        <th>Keterangan</th>
                        <th>Jumlah</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=5 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    {{ $subTitle   }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php $total = 0; @endphp
                        @foreach($item as $item_key => $dataPelanggan)
                            <tr>
                                <td colspan="5">{{$item_key}}</td>
                            </tr>
                                @php $subTotal = 0; @endphp
                                @foreach($dataPelanggan as $data)
                                    <tr>
                                        <td>{{ $data['no_penawaran'] }}</td>
                                        <td>{{ $data['tgl_penawaran'] }}</td>
                                        <td>{{ $data['keterangan']}}</td>
                                        <td>{{ number_format($data['total']) }}</td>
                                        <td>@if($data['status'] == 0) mengantri @endif</td>
                                    </tr>
                                    @php $subTotal += $data['total']; @endphp
                                @endforeach
                            @php $total += $subTotal; @endphp
                            <tr>
                                <td colspan="3"></td>
                                <td class="border-top">{{ number_format($total) }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection