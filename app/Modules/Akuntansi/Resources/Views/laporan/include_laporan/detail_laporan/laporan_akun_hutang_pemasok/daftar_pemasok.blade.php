@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title='Laporan Daftar Pemasok'}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4>{{ $nama_perusahaan->nama_perusahaan }}</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Per Tanggal {{ $tanggal }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Pemasok</th>
                        <th>Nama Pemasok</th>
                        <th>Saldo (Asing)</th>
                        <th>Saldo Pajak</th>
                    </tr>
                </thead>
                <!-- Perulangan Mata Uang -->
                    <tr>
                        <td colspan="6"><strong>IDR</strong></td>
                    </tr>
                    <!-- Perulangan Daftar Pemasok -->
                    @foreach ($items as $data_faktur)
                        <tr>
                            <td>{{ $data_faktur['nomor_pemasok'] ?? null }}</td>
                            <td>{{ $data_faktur['nama_pemasok'] ?? null }}</td>
                            <td>{{ $data_faktur['sum_total'] }}</td>
                            <td>0</td>
                        </tr>
                    @endforeach
                    <!-- Akhir Perulangan Daftar Pemasok -->
                    <tr>
                        <td colspan="2"></td>
                        <td class="border-top">{{ $grand_sum_total }}</td>
                        <td class="border-top">0</td>
                    </tr>
                <!-- Akhir Perulangan Mata Uang -->
            </table>
        </div>
    </div>
@endsection