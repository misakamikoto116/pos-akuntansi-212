<style>
    .border-top{
        border-top: 2px solid black !important;
    }

    .tab{
        padding: 7.5px 20px !important;
    }
</style>
@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col col-12">
            @if($item->isEmpty())
                Tidak ada data.
            @else 
                <table class="table">
                    <thead>
                        <tr>
                            <td colspan="4">
                                <p><h4>{{ $perusahaan }}</h4></p>
                                <p><h6>{{ $title }}</h6></p>
                                <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                                <p>{{ $subTitle }}</p>
                            </td>
                        </tr>
                        <tr>
                            <th>No. Akun</th>
                            <th>Nama Akun</th>
                            <th>Debit (Domestik)</th>
                            <th>Kredit (Domestik)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php    $debit = 0; $kredit = 0; @endphp
                        @foreach ($item as $key => $data)
                            @php    $debitDetail = 0; $kreditDetail = 0; @endphp
                            @foreach ($data as $keys => $itemJurnal)
                                <tr>
                                    <th>{{ $keys }}</th>
                                    <td></td>
                                    <th colspan="2">{{ $key }}</th>
                                </tr>
                                @foreach ($itemJurnal as $dataJurnal)
                                    <tr>
                                        <td class="tab">{{ $dataJurnal['no_akun'] }}</td>
                                        <td class="tab">{{ $dataJurnal['nama_akun'] }}</td>
                                        <td class="tab">{{ number_format($dataJurnal['debit']) }}</td>
                                        <td class="tab">{{ number_format($dataJurnal['kredit']) }}</td>
                                    </tr>
                                    @php $debit += $dataJurnal['debit']; $kredit += $dataJurnal['kredit']; @endphp
                                    @php $debitDetail += $dataJurnal['debit']; $kreditDetail += $dataJurnal['kredit']; @endphp
                                @endforeach
                            @endforeach
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="border-top">Rp. {{ number_format($debitDetail) }}</td>
                                <td class="border-top">Rp. {{ number_format($kreditDetail) }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="border-top">Rp. {{ number_format($debit) }}</td>
                            <td class="border-top">Rp. {{ number_format($kredit) }}</td>
                        </tr>
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection