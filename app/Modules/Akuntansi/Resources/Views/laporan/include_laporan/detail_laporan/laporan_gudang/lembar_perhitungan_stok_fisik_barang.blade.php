@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=7>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><h6> Per {{ $dari_tanggal }} </h6></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Barang</th>
                        <th>Deskripsi Barang</th>
                        <th>Kuantitas</th>
                        <th>Unit</th>
                        <th>Kontrol Qty</th>
                        <th>Hitungan ke-1</th>
                        <th>Hitungan ke-2</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=7 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia.
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($item as $dataBarang)
                            <tr>
                                <td>{{ $dataBarang['kode_barang'] }}</td>
                                <td>{{ $dataBarang['nama_barang'] }}</td>
                                <td>{{ $dataBarang['kuantitas'] }}</td>
                                <td>{{ $dataBarang['satuan'] }}</td>
                                <td>{{ $dataBarang['kontrol_qty'] }}</td>
                                <td class="border-bottom"></td>
                                <td class="border-bottom"></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan=2>&nbsp;</td>
                            <td class="border-top">{{ $dataBarang['total_kuantitas'] }}</td>
                            <td class="border-top">{{ $dataBarang['total_kontrol_qty'] }}</td>
                            <td colspan=2>&nbsp;</td>
                        </tr>
                    @endif
                </tbody>                
            </table>
        </div>
    </div>
@endsection