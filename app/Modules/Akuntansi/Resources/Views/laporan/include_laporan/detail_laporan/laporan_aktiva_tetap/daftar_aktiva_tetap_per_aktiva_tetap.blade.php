@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=9>
                        <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Per <b> {{ $dari_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Kode Aktiva</th>
                        <th>Nama Aktiva</th>
                        <th>Harga Perolehan</th>
                        <th>Penyesuaian Tahun Ini</th>
                        <th>Akumulasi Depr.</th>
                        <th>Book Value</th>
                        <th>Depresiasi Tahun Ini</th>
                        <th>Tanggal Pemakaian</th>
                        <th>Tanggal Pembelian</th>
                    </tr>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=9 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    pada tanggal {{ $dari_tanggal }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php
                            $totalHargaPerolehan        = 0;
                            $totalPenyesuaianTahun      = 0;
                            $totalAkumulasiDep          = 0;
                            $totalBookValue             = 0;
                            $totalDepresiasiTahunIni    = 0;
                        @endphp
                        @foreach($item as $item_key => $dataTipeAktivaTetap)
                            <tr>
                                <td colspan="9">{{ $item_key }}</td>
                            </tr>
                            @php
                                $subTotalHargaPerolehan        = 0;
                                $subTotalPenyesuaianTahun      = 0;
                                $subTotalAkumulasiDep          = 0;
                                $subTotalBookValue             = 0;
                                $subTotalDepresiasiTahunIni    = 0;
                            @endphp
                            @foreach($dataTipeAktivaTetap as $dataTipeAktiva)
                                <tr>
                                    <td>{{ $dataTipeAktiva['kodeAktiva'] }}</td>
                                    <td>{{ $dataTipeAktiva['keterangan'] }}</td>
                                    <td>{{ $dataTipeAktiva['hargaPerolehan'] }}</td>
                                    <td>{{ $dataTipeAktiva['penyesuaianTahun'] }}</td>
                                    <td>{{ $dataTipeAktiva['akumulasiDep'] }}</td>
                                    <td>{{ $dataTipeAktiva['bookValue'] }}</td>
                                    <td>{{ $dataTipeAktiva['depresiasiTahunIni'] }}</td>
                                    <td>{{ $dataTipeAktiva['tglPemakaian'] }}</td>
                                    <td>{{ $dataTipeAktiva['tglPembelian'] }}</td>
                                </tr>
                                @php
                                    $subTotalHargaPerolehan        += $dataTipeAktiva['hargaPerolehan'];
                                    $subTotalPenyesuaianTahun      += $dataTipeAktiva['penyesuaianTahun'];
                                    $subTotalAkumulasiDep          += $dataTipeAktiva['akumulasiDep'];
                                    $subTotalBookValue             += $dataTipeAktiva['bookValue'];
                                    $subTotalDepresiasiTahunIni    += $dataTipeAktiva['depresiasiTahunIni'];
                                @endphp
                            @endforeach
                            <tr>
                                <td colspan="2"></td>
                                <td class="border-top">{{ $subTotalHargaPerolehan }}</td>
                                <td class="border-top">{{ $subTotalPenyesuaianTahun }}</td>
                                <td class="border-top">{{ $subTotalAkumulasiDep }}</td>
                                <td class="border-top">{{ $subTotalBookValue }}</td>
                                <td class="border-top">{{ $subTotalDepresiasiTahunIni }}</td>
                                <td colspan="2"></td>
                            </tr>
                            @php
                                $totalHargaPerolehan        += $subTotalHargaPerolehan;
                                $totalPenyesuaianTahun      += $subTotalPenyesuaianTahun;
                                $totalAkumulasiDep          += $subTotalAkumulasiDep;
                                $totalBookValue             += $subTotalBookValue;
                                $totalDepresiasiTahunIni    += $subTotalDepresiasiTahunIni;
                            @endphp
                        @endforeach
                        <tr style="color:red;">
                            <td colspan="2">Jumlah Aktiva Tetap</td>
                            <td class="border-top">{{ $totalHargaPerolehan }}</td>
                            <td class="border-top">{{ $totalPenyesuaianTahun }}</td>
                            <td class="border-top">{{ $totalAkumulasiDep }}</td>
                            <td class="border-top">{{ $totalBookValue }}</td>
                            <td class="border-top">{{ $totalDepresiasiTahunIni }}</td>
                            <td colspan="2"></td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection