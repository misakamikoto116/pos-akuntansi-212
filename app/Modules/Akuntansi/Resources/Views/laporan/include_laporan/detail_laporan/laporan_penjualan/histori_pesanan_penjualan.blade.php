@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4>{{ $perusahaan }}</h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tipe Proses</th>
                        <th>No. Faktur</th>
                        <th>Tanggal Faktur</th>
                        <th>Kuantitas Faktur</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=4 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    {{ $subTitle   }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($item as $item_key => $dataHistoriPesananPenjualan)
                            <tr>    
                                <td>{{ $dataHistoriPesananPenjualan[0]['nopelanggan']       ??  null    }}</td>
                                <td>{{ $dataHistoriPesananPenjualan[0]['tglpesanan']        ??  null    }}</td>
                                <td>{{ $item_key }}</td>
                                <td>@if($dataHistoriPesananPenjualan[0]['status'] == 2) Terproses @endif</td>
                            </tr>
                            @foreach($dataHistoriPesananPenjualan as $dataHistoriPesananPenjualan_key => $dataHistoriPesanan)
                                
                                <tr>
                                    <td style="padding: 12px 25px !important;">{{ $dataHistoriPesanan['nobarang']   ??  null }}</td>
                                    <td>{{ $dataHistoriPesanan['nmbarang']       ??  null    }}</td>
                                    <td></td>
                                    <td>
                                        {{ $dataHistoriPesanan['qty'] ?? null }} / {{ $dataHistoriPesanan['unit']     ??   null }}
                                        <div style="float:right;">0<div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><div style="float: right;">Pengiriman Penjualan</div></td>
                                    <td>{{ $dataHistoriPesanan['nopengiriman']   ??  null  }}</td>
                                    <td>{{ $dataHistoriPesanan['tglpengiriman']  ??  null  }}</td>
                                    <td><div style="float:right">{{ $dataHistoriPesanan['qty'] ?? null }}</div></td>
                                </tr>
                                <tr>
                                    <td colspan=3></td>
                                    <td class="border-top" align="right">{{ $dataHistoriPesanan['qty'] ?? null }}</td>
                                </tr>
                                <tr>
                                    <td colspan=4>&nbsp;</td>
                                </tr>
                            @endforeach
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection