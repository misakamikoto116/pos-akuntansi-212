@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=5>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari Tanggal <b> {{ $dari_tanggal }}</b> Ke <b> {{ $ke_tanggal }}</b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. PO</th>
                        <th>Tanggal PO</th>
                        <th>Tanggal Diminta</th>
                        <th>Jumlah</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=5 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    pada tanggal {{ $dari_tanggal }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php $total = 0; @endphp
                        @foreach($item as $item_key => $dataPesananPembelian)
                            <tr>    
                                <td>{{ $item_key }}</td>
                                <td colspan=4>{{ $dataPesananPembelian[0]['nmpemasok'] }}</td>
                            </tr>
                            @php $subTotal = 0; @endphp
                                @foreach($dataPesananPembelian as $dataPembelian)
                                    <tr>
                                        <td>{{ $dataPembelian['ponum']        ??   null  }}</td>
                                        <td>{{ $dataPembelian['tanggal']      ??   null  }}</td>
                                        <td>{{ $dataPembelian['tanggalpesan'] ??   null  }}</td>
                                        <td>{{ number_format($dataPembelian['total'])  ??   null }}</td>
                                        <td>@if($dataPembelian['status'] == 0) Sedang Proses @endif</td>
                                    </tr>
                                    @php $subTotal += $dataPembelian['total']; @endphp
                                @endforeach
                            @php $total += $subTotal; @endphp
                            <tr>
                                <td colspan="3"></td>
                                <td class="border-top">{{ number_format($total) }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3"></td>
                            <td class="border-top">{{ number_format($total) }}</td>
                            <td></td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection