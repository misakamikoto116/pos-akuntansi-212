@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title='Laporan Rincian Pembayaran Pembelian'}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4>{{ $nama_perusahaan->nama_perusahaan ?? null }}</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} Ke {{ $tanggal_akhir ?? null }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Faktur</th>
                        <th>Tanggal Faktur</th>
                        <th>Total Bayar</th>
                        <th>Jumlah Pembayaran</th>
                        <th>Total Diskon</th>
                        <th>Akun Diskon(DP)</th>
                    </tr>
                </thead>
                <!-- Perulangan Mata Uang -->
                    <tr>
                        <td colspan="6"><strong>IDR</strong></td>
                    </tr>
                    <!-- Perulangan Faktur -->
                    @foreach ($items as $data_pembayaran)
                        <tr>
                            <td><strong>{{ $data_pembayaran->form_no ?? null }}</strong></td>
                            <td><strong>{{ Carbon\Carbon::parse($data_pembayaran->payment_date)->format('d F Y') ?? null }}</strong></td>
                            <td><strong>{{ $data_pembayaran->cheque_no ?? null }}</strong></td>
                            <td><strong>{{ Carbon\Carbon::parse($data_pembayaran->cheque_date)->format('d F Y') ?? null }}</strong></td>
                            <td><strong>{{ $data_pembayaran->pemasok->nama ?? null }}</strong></td>
                            <td><strong>{{ $data_pembayaran->akun->nama_akun ?? null }}</strong></td>
                        </tr>
                        <!-- Perulangan Rincian Pembayaran Pembelian -->
                            @foreach ($data_pembayaran->invoice as $data_invoice)
                                <tr>
                                    <td>{{ $data_invoice->fakturPembelian->no_faktur ?? null }}</td>
                                    <td>{{ Carbon\Carbon::parse($data_invoice->fakturPembelian->invoice_date)->format('d F Y') ?? null }}</td>
                                    <td>{{ number_format($data_invoice->payment_amount) }}</td>
                                    <td>{{ number_format($data_invoice->payment_amount) }}</td>
                                    <td>{{ number_format($data_invoice->diskon) }}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                        <!-- Akhir Perulangan Rincian Pembayaran Pembelian -->
                    <!-- Perulangan Faktur -->
                        <tr>
                            <td colspan="2"></td>
                            <td class="border-top">{{ number_format($data_pembayaran->invoice->sum('payment_amount')) }}</td>
                            <td class="border-top">{{ number_format($data_pembayaran->invoice->sum('payment_amount')) }}</td>
                            <td class="border-top">{{ number_format($data_pembayaran->invoice->sum('diskon')) }}</td>
                            <td></td>
                        </tr>
                    @endforeach
                <!-- Akhir Perulangan Mata Uang -->
            </table>
        </div>
    </div>
@endsection