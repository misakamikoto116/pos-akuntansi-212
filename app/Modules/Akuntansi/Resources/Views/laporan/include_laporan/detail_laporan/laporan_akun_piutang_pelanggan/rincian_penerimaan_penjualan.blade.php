@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Rincian Penerimaan Penjualan
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12" align="center">
            <p><h4>{{ $nama_perusahaan->nama_perusahaan ?? null }}</h4></p>
            <p><h6>Rincian Penerimaan Penjualan</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>Dari {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir ?? null }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>No. Form</th>
                        <th>Tanggal Terima</th>
                        <th>No. Cek</th>
                        <th>Tanggal Cek</th>
                        <th>Nama Pelanggan</th>
                    </tr>
                    <tr>
                        <th>No. Faktur .(SO)</th>
                        <th>Tanggal Faktur</th>
                        <th>Total Diterima</th>
                        <th>Nilai Terima</th>
                        <th>Total Diskon</th>
                    </tr>
                </thead>
                <!-- Perulangan Mata Uang -->
                <tr>
                    <td><strong>IDR</strong></td> <!-- Mata Uang -->
                </tr>
                <!-- Perulangan Rincian Penerimaan Penjualan -->
                @foreach ($items as $data_penerimaan)
                    <tr>
                        <td><strong>{{ $data_penerimaan->form_no ?? null }}</strong></td>
                        <td><strong>{{ Carbon\Carbon::parse($data_penerimaan->payment_date)->format('d F Y') ?? null }}</strong></td>
                        <td><strong>{{ $data_penerimaan->cheque_no ?? null }}</strong></td>
                        <td><strong>{{ Carbon\Carbon::parse($data_penerimaan->cheque_date)->format('d F Y') ?? null }}</strong></td>
                        <td><strong>{{ $data_penerimaan->pelanggan->nama ?? null }}</strong></td>
                    </tr>
                    @foreach ($data_penerimaan->invoice as $data_invoice)
                        <tr>
                            <td>{{ $data_invoice->fakturPenjualan->no_faktur ?? null }}</td>
                            <td>{{ Carbon\Carbon::parse($data_invoice->fakturPenjualan->invoice_date)->format('d F Y') ?? null }}</td>
                            <td>{{ number_format($data_invoice->payment_amount) }}</td>
                            <td>{{ number_format($data_invoice->payment_amount) }}</td>
                            <td>{{ number_format($data_invoice->diskon) }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2"></td>
                        <td class="border-top">{{ number_format($data_penerimaan->invoice->sum('payment_amount')) }}</td>
                        <td class="border-top">{{ number_format($data_penerimaan->invoice->sum('payment_amount')) }}</td>
                        <td class="border-top">{{ number_format($data_penerimaan->invoice->sum('diskon')) }}</td>
                    </tr>
                @endforeach
                <!-- Akhir Perulangan Rincian Penerimaan Penjualan -->
                <!-- Akhir Perulangan Mata Uang -->
            </table>
        </div>
    </div>
@endsection