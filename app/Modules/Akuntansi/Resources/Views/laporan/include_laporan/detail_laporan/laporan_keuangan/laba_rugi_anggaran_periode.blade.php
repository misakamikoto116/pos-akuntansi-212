<style>
    .pad-row{
        padding: 15px;
    }
</style>
@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    @php
        $periodes = $priodes->count()-1;
    @endphp
    <div class="row pad-row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan={{$priodes->count()}}>
                            <p><h4>{{ $nama_perusahaan }}<h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Periode {{$priodes->first()}} sampai {{ $priodes->slice(0, $periodes)->last() }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan</th>
                        @foreach ($priodes->slice(0,$periodes) as $priode)
                            <th>{{$priode}}</th>
                        @endforeach
                    </tr>
                </thead>                
                @foreach ($items as $i => $accountGroups)
                    @foreach ($accountGroups as $name => $accountGroup)
                        <tr>
                            <th colspan="{{$priodes->count()}}" style="padding-left: 25px;">{{ $accountGroup['tipe_akun_parent'] }}</th>
                        </tr>
                            @if ($loop)
                                <tr>
                                    <th style="padding-left: 50px;">{{ $accountGroup['nama_akun_parent'] ?? null }}</th>
                                    @foreach (array_slice($accountGroup['sum_akun_parent'], 0, $periodes) as $item)
                                        <th class="border-bottom" style="text-align: right;">{{ number_format($item) }}</th>    
                                    @endforeach
                                </tr>
                            @endif
                                @foreach ($accountGroup['akun_child'] as $accountGroup_child)
                                    <tr>
                                        <td style="padding-left:  75px">{{ $accountGroup_child['nama_akun_child'] }}</td>
                                        @foreach (array_slice($accountGroup_child['sum_akun_child'], 0, $periodes) as $item)
                                            <td style="text-align: right;">{{ number_format($item) }}</td>    
                                        @endforeach
                                    </tr>                                           
                                @endforeach
                            @if ($accountGroup['akun_child']->isEmpty())
                                <tr>
                                    <th style="font-weight: normal; padding-left: 50px;">{{ $accountGroup['nama_akun_parent'] ?? null }}</th>
                                    @foreach (array_slice($accountGroup['sum_akun_parent'], 0, $periodes) as $item)
                                        <th style="text-align: right;">{{ number_format($item) }}</th>    
                                    @endforeach
                                </tr>
                            @endif
                        @endforeach 
                @endforeach
            </table>
        </div>
    </div>
@endsection