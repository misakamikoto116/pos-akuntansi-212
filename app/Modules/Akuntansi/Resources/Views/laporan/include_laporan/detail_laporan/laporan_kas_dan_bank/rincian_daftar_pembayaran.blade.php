@extends('akuntansi::laporan.laporan')
@section('title')
{{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4>{{ $nama_perusahaan }}</h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir ?? null }} <!-- Tanggal --></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Akun</th>
                        <th>Nama Akun</th>
                        <th>Catatan</th>
                        <th>Nilai (Asing)</th>
                        <th>Nama Dep</th>
                        <th>Nama Proyek</th>
                    </tr>
                    <!-- Perulangan -->
                </thead>
                @foreach ($items as $data_pembayaran)
                    @php
                        $sum_pembayaran = 0;
                    @endphp
                    <tr>
                        <td><strong>{{ Carbon\Carbon::parse($data_pembayaran->tanggal)->format('d F Y') ?? null }} &ndash; {{ $data_pembayaran->no_faktur ?? null }}</strong></td>
                        <td><strong>{{ $data_pembayaran->akun->nama_akun ?? null }}</strong></td>
                        <td><strong>{{ $data_pembayaran->keterangan ?? null }}</strong></td> <!--  -->
                        <td><strong>{{ $data_pembayaran->akun->mataUang->kode ?? null }}</strong></td> <!--  -->
                        <td></td>
                        <td></td> <!--  -->
                    </tr>
                    @foreach ($data_pembayaran->detailBukuKeluar as $detail_pembayaran)
                            
                        @php
                            $sum_pembayaran += $detail_pembayaran->nominal;
                        @endphp

                        <tr>
                            <td style="padding-left: 25px;">{{ $detail_pembayaran->akun->kode_akun ?? null }}</td>
                            <td style="padding-left: 25px;">{{ $detail_pembayaran->akun->nama_akun ?? null }}</td>
                            <td style="padding-left: 25px;">{{ $detail_pembayaran->catatan ?? null }}</td>
                            <td style="padding-left: 25px; text-align: right;">{{ number_format($detail_pembayaran->nominal) }}</td>
                            <td style="padding-left: 25px;"></td>
                            <td style="padding-left: 25px;"></td> <!--  -->
                        </tr>
                    @endforeach
                    <!-- Akhir Ulang -->
                    <tr>
                        <td style="padding-left: 25px; color: blue;">Total dari {{ $data_pembayaran->no_faktur ?? null }}</td>
                        <td style="padding-left: 25px;" colspan="2"></td>
                        <td style="padding-left: 25px; text-align: right; color: blue;" class="border-top">{{ number_format($sum_pembayaran) }}</td>
                        <td style="padding-left: 25px;" colspan="2"></td>
                    </tr>
                @endforeach
                <!-- Akhir Perulangan  -->
            </table>
        </div>
    </div>
@endsection