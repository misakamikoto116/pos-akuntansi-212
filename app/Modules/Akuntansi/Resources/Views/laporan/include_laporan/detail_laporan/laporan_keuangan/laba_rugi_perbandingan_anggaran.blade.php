@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Laba/Rugi Perbandingan Anggaran
@endsection
@section('laporan')
    <div class="row">
        <div class="col col-12" align="center">
            <p><h4>CHICHI</h4></p>
            <p><h6>Laba Rugi (Perbandingan Anggaran)</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>Per Tanggal <!-- 1 sampai tgl tutup buku Mei 2018  --></p>
        </div>
    </div>
    <div class="row">
        <div class="col col-12">
            <table class="table">
                <tr>
                    <td >Keterangan</td>
                    <td >Balance</td>
                    <td >Budget</td>
                    <td >Selisih</td>
                    <td >%</td>
                </tr>
                <tr>
                    <th colspan="6">Pendapatan</th>
                </tr>
                <tr>
                    <th>Jumlah Pendapatan</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th> %</th>
                </tr>
                <tr>
                    <th colspan="6">Harga Pokok Penjualan</th>
                </tr>
                <tr>
                    <th>Jumlah Harga Pokok Penjualan</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th> %</th>
                </tr>
                <tr>
                    <th>Laba Kotor</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th> %</th>
                </tr>
                <tr>
                    <th colspan="6">Beban Operasi</th>
                </tr>
                <tr>
                    <th>Jumlah Beban Operasi</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th> %</th>
                </tr>
                <tr>
                    <th>Pendapatan Operasi</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th> %</th>
                </tr>
                <tr>
                    <th colspan="6">Pendapatan dan Beban Lain</th>
                </tr>
                <tr>
                    <th colspan="6">&nbsp;&nbsp;&nbsp;Pendapatan Lain</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;Jumlah Pendapatan Lain</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th> %</th>
                </tr>
                <tr>
                    <th colspan="6">&nbsp;&nbsp;&nbsp;Beban Lain-lain</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;Jumlah Beban Lain-lain</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th> %</th>
                </tr>
                <tr>
                    <th>Jumlah Pendapatan dan Beban Lain</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th> %</th>
                </tr>
                <tr>
                    <th>LABA(RUGI) Bersih (Sebelum Pajak)</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th> %</th>
                </tr>
                <tr>
                    <th>LABA(RUGI) Bersih (Setelah Pajak)</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th> %</th>
                </tr> 
            </table>
        </div>
    </div>
@endsection
