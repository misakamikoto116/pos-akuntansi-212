@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Laba/Rugi Konsolidasi
@endsection
@section('laporan')
    <div class="row">
        <div class="col col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="3">
                            <p><h4>CHICHI</h4></p>
                            <p><h6>Laba Rugi (Konsolidasi)</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Per Tanggal <!-- awal bulan sampai tanggal tutup buku Mei 2018--></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan</th>
                        <th>Chichi</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tr>
                    <th colspan="3">Pendapatan</th>
                </tr>
                <tr>
                    <th>Jumlah Pendapatan</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="3">Harga Pokok Penjualan</th>
                </tr>
                <tr>
                    <th>Jumlah Harga Pokok Penjualan</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>Laba Kotor</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="3">Beban Operasi</th>
                </tr>
                <tr>
                    <th>Jumlah Beban Operasi</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>Pendapatan Operasi</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="3">Pendapatan dan Beban Lain</th>
                </tr>
                <tr>
                    <th colspan="3">&nbsp;&nbsp;&nbsp;Pendapatan Lain</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;Jumlah Pendapatan Lain</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="3">&nbsp;&nbsp;&nbsp;Beban Lain-lain</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;Jumlah Beban Lain-lain</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>Jumlah Pendapatan dan Beban Lain</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>LABA(RUGI) Bersih (Sebelum Pajak)</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>LABA(RUGI) Bersih (Setelah Pajak)</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr> 
            </table>
        </div>
    </div>
@endsection
