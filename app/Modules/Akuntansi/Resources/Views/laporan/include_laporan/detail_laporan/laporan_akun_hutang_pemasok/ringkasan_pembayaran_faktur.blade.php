@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title ='Laporan Ringkasan Pembayaran Faktur'}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=7>
                            <p><h4>{{ $nama_perusahaan }}</h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} Ke {{ $tanggal_akhir ?? null }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal Faktur</th>
                        <th>No. Faktur</th>
                        <th>Nama Pemasok</th>
                        <th>Faktur Telah Dibayar</th>
                        <th>No. Cek</th>
                        <th>Tanggal Cek</th>
                        <th>Terekonsili</th>
                    </tr>
                </thead>
                <!-- Perulangan Mata Uang -->
                    <tr>
                        <td colspan="7"><strong>IDR</strong></td>
                    </tr>
                    <!-- Perulangan Ringkasan Pembayaran Faktur -->
                    @php
                        $sum_pembayaran = 0;
                    @endphp
                    @foreach ($items as $item)
                        @foreach ($item as $data_pembayaran)
                            @php
                                $sum_pembayaran += $data_pembayaran['payment_amount'];
                            @endphp
                            <tr>
                                <td style="padding-left: 25px;">{{ $data_pembayaran['invoice_date'] }}</td>
                                <td style="padding-left: 25px;">{{ $data_pembayaran['no_faktur'] }}</td>
                                <td style="padding-left: 25px;">{{ $data_pembayaran['nama_pemasok'] }}</td>
                                <td style="text-align: right;">{{ number_format($data_pembayaran['payment_amount']) }}</td>
                                <td style="padding-left: 25px;">{{ $data_pembayaran['cheque_no'] }}</td>
                                <td style="padding-left: 25px;">{{ $data_pembayaran['cheque_date'] }}</td>
                                <td style="padding-left: 25px;">Tidak</td>
                            </tr>
                        @endforeach
                    @endforeach
                    <!-- Akhir Perulangan Ringkasan Pembayaran Faktur -->
                    <tr>
                        <td colspan="3" style="color: blue;">Total dari IDR</td>
                        <td class="border-top" style="text-align: right;">{{ number_format($sum_pembayaran) }}</td>
                        <td></td>
                    </tr>
                <!-- Akhir Perulangan Mata Uang -->
            </table>
        </div>
    </div>
@endsection