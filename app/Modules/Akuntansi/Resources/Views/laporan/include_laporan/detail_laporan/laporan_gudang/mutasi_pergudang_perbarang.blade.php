@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=7>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>No. Sumber</th>
                        <th>Tipe</th>
                        <th>Keterangan</th>
                        <th>Kts. Masuk</th>
                        <th>Kts. Keluar</th>
                        <th>Saldo</th>
                    </tr>
                </thead>
                <tbody>
                    @if($items->isEmpty())
                        <tr>
                            <td colspan=7 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia.
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($items as $item)
                            @if (!$item['produk']->isEmpty())
                                <tr>
                                    <td colspan=7><b>{{$item['nama_gudang']}}</b></td>
                                </tr>
                                @foreach($item['produk'] as $data_produk)
                                    <tr>
                                        <td style="padding-left:25px;"><b>{{$data_produk['kode_barang']}}</b></td>
                                        <td><b>{{$data_produk['nama_barang']}}</b></td>
                                        <td colspan=4>&nbsp;</td>
                                        <td><b>{{$data_produk['kuantitas']}}</b></td>
                                    </tr>
                                    @foreach($data_produk['saldo_awal'] as $data_saldo)
                                        <tr>
                                            <td style="padding-left:55px;">{{$data_saldo['tanggal']}}</td>
                                            <td>{{$data_saldo['no_transaksi']}}</td>
                                            <td>{{$data_saldo['tipe']}}</td>
                                            <td>{{$data_saldo['keterangan']}}</td>
                                            <td>{{$data_saldo['kts_masuk']}}</td>
                                            <td>{{$data_saldo['kts_keluar']}}</td>
                                            <td>{{$data_saldo['saldo']}}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan=4></td>
                                        <td class="border-top">{{$data_produk['sum_kts_masuk']}}</td>
                                        <td class="border-top">{{$data_produk['sum_kts_keluar']}}</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                </tbody>                
            </table>
        </div>
    </div>
@endsection