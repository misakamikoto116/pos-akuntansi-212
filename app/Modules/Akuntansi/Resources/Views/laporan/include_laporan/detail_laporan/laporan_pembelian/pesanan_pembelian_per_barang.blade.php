@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari Tanggal <b> {{ $dari_tanggal }}</b> Ke <b> {{ $ke_tanggal }}</b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. PO</th>
                        <th>Tanggal PO</th>
                        <th>Kuantitas</th>
                        <th>Kuantitas Dikirim</th>
                        <th>Satuan</th>
                        <th>Status Barang</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=6 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    pada tanggal {{ $dari_tanggal }}.
                                </div>
                            </td>
                        </tr>
                    @else
                    @php $totalQty = 0; $totalQtyKirim = 0; @endphp
                    @foreach($item as $item_key => $dataPesananPembelian)
                        <tr>    
                            <td>{{ $item_key }}</td>
                            <td colspan=5>{{ $dataPesananPembelian[0]['nmbarang'] }}</td>
                        </tr>
                        @php $subTotalQty = 0; $subTotalQtyKirim = 0; @endphp
                            @foreach($dataPesananPembelian as $dataPembelian)
                                <tr>
                                    <td>{{ $dataPembelian['ponum']        ??   null  }}</td>
                                    <td>{{ $dataPembelian['tanggal']      ??   null  }}</td>
                                    <td>{{ $dataPembelian['qty']          ??   null }}</td>
                                    <td>{{ $dataPembelian['qtydikirm']    ??   null  }}</td>
                                    <td>{{ $dataPembelian['satuan']       ??   null  }}</td>
                                    <td>@if($dataPembelian['status'] == 0) Sedang Proses @elseif($dataPembelian['status'] == 1) Menunggu @endif</td>
                                </tr>
                                @php $subTotalQty += $dataPembelian['qty']; $subTotalQtyKirim += $dataPembelian['qtydikirm']; @endphp
                            @endforeach
                            <tr>
                                <td colspan="2"></td>
                                <td class="border-top">{{   $subTotalQty       }}</td>
                                <td class="border-top">{{   $subTotalQtyKirim  }}</td>
                                <td colspan=2></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection