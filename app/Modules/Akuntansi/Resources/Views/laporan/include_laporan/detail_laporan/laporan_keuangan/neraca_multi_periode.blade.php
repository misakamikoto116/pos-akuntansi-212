<style>
    .pad-row{
        padding: 15px;
    }
</style>
@extends('akuntansi::laporan.laporan')
@section('title')
    Neraca Multi Periode
@endsection
@section('laporan')
    <div class="row pad-row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="4">
                            <p><h4>CHICHI<h4></p>
                            <p><h6>Neraca (Multi Periode)</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Periode {{$priodes->first()}} sampai {{$priodes->last()}}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan</th>
                        @foreach ($priodes as $priode)
                        <th>{{$priode}}</th>    
                        @endforeach
                    </tr>
                </thead>
                {{-- <tr>
                  <th colspan="2">Aktiva</th>
                </tr> --}}
                @foreach ($items->chunk(2) as $i => $accountGroups)
                  <tr>
                    <th colspan="{{$priodes->count()+1}}">{{$i === 0 ? 'Aktiva' : 'Kewajiban & Ekuitas'}}</th>
                  </tr>
                  @foreach ($accountGroups as $name => $accountGroup)
                  <tr>
                    <th colspan="{{$priodes->count()+1}}" style="padding-left: 25px;">{{$name}}</th>
                  </tr>
                    @foreach ($accountGroup as $name_tipe_akun => $accounts)
                      @if (!empty($accounts['child_and_parent']))
                        <tr>
                            <th colspan="2" style="padding-left: 40px;">{{ $name_tipe_akun }}</th>
                        </tr>
                        @foreach ($accounts['child_and_parent'] as $key => $account)
                          @if ($account['akun_child']->isNotEmpty())
                            @if (array_key_exists('nama_akun_parent', $account))
                              <tr class="akun_parent">
                                    <th style="padding-left: 60px;">{{ $account['nama_akun_parent'] ?? null }}</th>
                                    @foreach ($account['sum_akun_parent'] as $item)
                                      <th style="text-align: right;">{{ number_format($item) }}</th>    
                                    @endforeach
                              </tr>
                            @endif
                          @endif
                          @foreach ($account['akun_child'] as $account_child)
                            <tr class="akun_child">
                                  <td style="padding-left:  70px">{{ $account_child['nama_akun_child'] }}</td>
                                  @foreach ($account_child['sum_akun_child'] as $item)
                                    <td style="text-align: right;">{{ number_format($item) }}</td>    
                                  @endforeach
                            </tr>                                           
                          @endforeach
                          @if ($account['akun_child']->isEmpty())
                            @if (array_key_exists('nama_akun_parent', $account))
                              <tr class="akun_child">
                                    <th style="font-weight: normal; padding-left: 60px;">{{ $account['nama_akun_parent'] ?? null }}</th>
                                    @foreach ($account['sum_akun_parent'] as $item)
                                      <th style="text-align: right; font-weight: normal;">{{ number_format($item) }}</th>    
                                    @endforeach
                              </tr>
                            @endif    
                          @endif
                        @endforeach
                        <tr class="akun_parent">
                              <th style="padding-left: 40px;">Jumlah {{ $name_tipe_akun }}</th>
                              @foreach ($accounts['sum_tipe_akun'] as $item)
                                <th style="text-align: right;">{{ number_format($item) }}</th>    
                              @endforeach
                        </tr>
                      @else
                        @if (!empty($accounts['akun_child']))
                          @if ($accounts['akun_child']->isNotEmpty())
                            <tr class="akun_parent">
                                  <th style="padding-left: 60px;">{{ $accounts['nama_akun_parent'] ?? null }}</th>
                                  @foreach ($account['sum_akun_parent'] as $item)
                                    <th style="text-align: right;">{{ number_format($item) }}</th>    
                                  @endforeach
                            </tr>
                          @endif
                        @endif
                        @if (array_key_exists('nama_akun_parent', $accounts))
                          <tr class="akun_child">
                                <th style="font-weight: normal; padding-left: 60px;">{{ $accounts['nama_akun_parent'] ?? null }}</th>
                                @foreach ($account['sum_akun_parent'] as $item)
                                  <th style="text-align: right;">{{ number_format($item) }}</th>    
                                @endforeach
                          </tr>    
                        @endif
                      @endif
                    @endforeach
                    <tr>
                        <th style="padding-left: 25px;">Jumlah {{$name}}</th>
                        @foreach ($neraca_sum[$name] as $priode)
                          <th style="text-align: right;">{{number_format($priode)}}</th>
                        @endforeach
                    </tr>
                  @endforeach
                  <tr>
                        <th>{{$sum_neraca_group[$i]['title']}}</th>
                        @foreach ($sum_neraca_group[$i]['jumlah'] as $priode)
                          <th style="text-align: right;">{{number_format($priode)}}</th>
                        @endforeach
                  </tr> 
                @endforeach
            </table>
        </div>
    </div>
@endsection