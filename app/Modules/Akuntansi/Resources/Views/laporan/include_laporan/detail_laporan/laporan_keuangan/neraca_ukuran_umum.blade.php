@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row pad-row">
        <div class="col-md-12">
            <table id="table_data" class="table">
                <thead>
                    <tr>
                        <td colspan="4">
                          <p><h4>{{ $nama_perusahaan ?? null }}<h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>{{$sub_title}}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan</th>
                        <th style="padding-left: 28px;">Balance</th>
                        <th style="padding-left: 26px;">Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($items->chunk(2) as $i => $accountGroups)
                    <tr>
                      <th>{{$i === 0 ? 'Aktiva' : 'Kewajiban & Ekuitas'}}</th>
                      <th></th>
                      <th></th>
                    </tr>
                    @foreach ($accountGroups as $name => $accountGroup)
                      <tr>
                        <th style="padding-left: 25px;">{{$name}}</th>
                        <th></th>
                        <th></th>
                      </tr>
                      @foreach ($accountGroup as $name_tipe_akun => $accounts)
                        @if (!empty($accounts['child_and_parent']))
                          <tr>
                              <th colspan="2" style="padding-left: 40px;">{{ $name_tipe_akun }}</th>
                              <th></th>
                          </tr>
                          @foreach ($accounts['child_and_parent'] as $account)
                            @if ($account['akun_child']->isNotEmpty())
                              @if (array_key_exists('nama_akun_parent', $account))
                                <tr class="akun_parent">
                                      <th style="padding-left: 60px;">{{ $account['nama_akun_parent'] ?? null }}</th>
                                      @foreach ($account['sum_akun_parent'] as $key => $item)
                                        @php
                                            if ($sum_neraca_group[$i]['jumlah'][$key] != 0) {
                                              $decimal_akun_parent    = ($item / $sum_neraca_group[$i]['jumlah'][$key]);
                                              $presentase_akun_parent = $decimal_akun_parent * 100;  
                                            }else {
                                              $presentase_akun_parent = 0;
                                            }
                                        @endphp
                                        <th class="akun_val" style="text-align: right;">{{ number_format($item) }}</th>
                                        <th class="akun_val" style="text-align: right;">{{ number_format($presentase_akun_parent, 2) }} %</th>
                                      @endforeach
                                </tr>
                              @endif
                            @endif
                            @foreach ($account['akun_child'] as $account_child)
                              <tr class="akun_child">
                                    <td style="padding-left:  70px">{{ $account_child['nama_akun_child'] }}</td>
                                    @foreach ($account_child['sum_akun_child'] as $key => $item)
                                        @php
                                            if ($sum_neraca_group[$i]['jumlah'][$key] != 0) {
                                              $decimal_akun_child    = ($item / $sum_neraca_group[$i]['jumlah'][$key]);
                                              $presentase_akun_child = $decimal_akun_child * 100; 
                                            }else {
                                              $presentase_akun_child = 0;
                                            } 
                                        @endphp
                                        <td class="akun_val akun_child_val" style="text-align: right;">{{ number_format($item) }}</td>
                                        <td class="akun_val akun_child_val" style="text-align: right;">{{ number_format($presentase_akun_child, 2) }} %</td>
                                    @endforeach
                              </tr>                                           
                            @endforeach
                            @if ($account['akun_child']->isEmpty())
                              @if (array_key_exists('nama_akun_parent', $account))
                                <tr class="akun_child">
                                      <th style="font-weight: normal; padding-left: 60px;">{{ $account['nama_akun_parent'] ?? null }}</th>
                                      @foreach ($account['sum_akun_parent'] as $key => $item)
                                        @php
                                            if ($sum_neraca_group[$i]['jumlah'][$key] != 0) {
                                              $decimal_akun_parent_second    = ($item / $sum_neraca_group[$i]['jumlah'][$key]);
                                              $presentase_akun_parent_second = $decimal_akun_parent_second * 100;
                                            }else {
                                              $presentase_akun_parent_second = 0;
                                            }
                                        @endphp
                                        <th class="akun_val akun_child_val" style="text-align: right;">{{ number_format($item) }}</th>
                                        <th class="akun_val akun_child_val" style="text-align: right;">{{ number_format($presentase_akun_parent_second, 2) }} %</th>
                                      @endforeach
                                </tr>
                              @endif    
                            @endif
                          @endforeach
                          <tr class="akun_parent">
                                <th style="padding-left: 40px;">Jumlah {{ $name_tipe_akun }}</th>
                                @foreach ($accounts['sum_tipe_akun'] as $item)
                                    @php
                                        if ($sum_neraca_group[$i]['jumlah'][$key] != 0) {
                                          $decimal_tipe_akun    = ($item / $sum_neraca_group[$i]['jumlah'][$key]);
                                          $presentase_tipe_akun = $decimal_tipe_akun * 100;
                                        }else {
                                          $presentase_tipe_akun = 0;
                                        } 
                                    @endphp
                                    <th class="akun_val" style="text-align: right;">{{ number_format($item) }}</th>
                                    <th class="akun_val" style="text-align: right;">{{ number_format($presentase_tipe_akun, 2) }} %</th>
                                @endforeach
                          </tr>
                        @else
                          @if (!empty($accounts['akun_child']))
                            @if ($accounts['akun_child']->isNotEmpty())
                              <tr class="akun_parent">
                                    <th style="padding-left: 60px;">{{ $accounts['nama_akun_parent'] ?? null }}</th>
                                    @foreach ($accounts['sum_akun_parent'] as $key => $item)
                                        @php
                                            if ($sum_neraca_group[$i]['jumlah'][$key]) {
                                              $decimal_akun_parent_third    = ($item / $sum_neraca_group[$i]['jumlah'][$key]);
                                              $presentase_akun_parent_third = $decimal_akun_parent_third * 100;
                                            }else {
                                              $presentase_akun_parent_third = 0;
                                            }  
                                        @endphp
                                        <th class="akun_val" style="text-align: right;">{{ number_format($presentase_akun_parent_third, 2) }} %</th>
                                    @endforeach
                              </tr>
                            @endif
                          @endif
                          @if (array_key_exists('nama_akun_parent', $accounts))
                            <tr class="akun_child">
                                  <th style="font-weight: normal; padding-left: 60px;">{{ $accounts['nama_akun_parent'] ?? null }}</th>
                                  @foreach ($accounts['sum_akun_parent'] as $key => $item)
                                    @php
                                        if ($sum_neraca_group[$i]['jumlah'][$key] != 0) {
                                          $decimal_akun_parent_fourth    = ($item / $sum_neraca_group[$i]['jumlah'][$key]);
                                          $presentase_akun_parent_fourth = $decimal_akun_parent_fourth * 100; 
                                       }else {
                                          $presentase_akun_parent_fourth = 0; 
                                       } 
                                    @endphp
                                    <th class="akun_val akun_child_val" style="text-align: right;">{{ number_format($item) }}</th>
                                    <th class="akun_val akun_child_val" style="text-align: right;">{{ number_format($presentase_akun_parent_fourth, 2) }} %</th>
                                  @endforeach
                            </tr>    
                          @endif
                        @endif
                      @endforeach
                      <tr>
                          <th style="padding-left: 25px;">Jumlah {{$name}}</th>
                          @foreach ($neraca_sum[$name] as $key => $priode)
                            @php
                                if ($sum_neraca_group[$i]['jumlah'][$key] != 0) {
                                  $decimal_akun_jumlah    = ($priode / $sum_neraca_group[$i]['jumlah'][$key]);
                                  $presentase_akun_jumlah = $decimal_akun_jumlah * 100;
                                }else {
                                  $presentase_akun_jumlah = 0;
                                }  
                            @endphp
                            <th style="text-align: right;">{{number_format($priode)}}</th>
                            <th style="text-align: right;">{{number_format($presentase_akun_jumlah, 2)}} %</th>
                          @endforeach
                      </tr> 
                    @endforeach 
                    <tr>
                          <th>{{$sum_neraca_group[$i]['title']}}</th>
                          @foreach ($sum_neraca_group[$i]['jumlah'] as $key => $priode)
                            @php
                                if ($sum_neraca_group[$i]['jumlah'][$key] != 0) {
                                  $decimal_akun_akhir    = ($priode / $sum_neraca_group[$i]['jumlah'][$key]);
                                  $presentase_akun_akhir = $decimal_akun_akhir * 100;
                                }else {
                                  $presentase_akun_akhir = 0;
                                }  
                            @endphp
                            <th style="text-align: right;">{{number_format($priode)}}</th>
                            <th style="text-align: right;">{{number_format($presentase_akun_akhir, 2)}} %</th>
                          @endforeach
                    </tr> 
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('custom_js')
    {{-- <script>
        $( window ).on( "load", function() {
            const tampil_total = "{{ request()->has('tampil_total') ? request()->get('tampil_total') : '' }}";
            const tampil_induk = "{{ request()->has('tampil_induk') ? request()->get('tampil_induk') : '' }}";
            const tampil_anak = "{{ request()->has('tampil_anak') ? request()->get('tampil_anak') : '' }}";
            const tampil_jumlah_induk = "{{ request()->has('tampil_jumlah_induk') ? request()->get('tampil_jumlah_induk') : '' }}";
            const geser_kanan = "{{ request()->has('geser_kanan') ? request()->get('geser_kanan') : '' }}";
            const saldo_nol = "{{ request()->has('saldo_nol') ? request()->get('saldo_nol') : '' }}";
            if(tampil_total !== 'on'){
                $('.akun_parent').remove();
            }
            if(geser_kanan === 'on'){
                $('.akun_child_val').css("padding-right", "60px");
            }
            if(tampil_induk !== 'on'){
                $('.akun_parent').remove();
            }
            if(saldo_nol === 'on'){

                var nol = $(this).closest('td').find('.akun_child_val').text();
                $('.nilai_saldo').val('');
            }
        });
        $(document).ready(function() {
            $('#table_data tbody tr:last').children('td').each(function(index,event) {
                console.log($(event).text());
                if ($(event).text() === "0") {
                    $('#table_data th').each(function(indexIn,eventIn) {
                        $(eventIn).children('td').eq(index).css('display','none');
                        $(eventIn).children('th').eq(index).css('display','none');
                    });
                }
            });
        });

    </script> --}}
@endsection