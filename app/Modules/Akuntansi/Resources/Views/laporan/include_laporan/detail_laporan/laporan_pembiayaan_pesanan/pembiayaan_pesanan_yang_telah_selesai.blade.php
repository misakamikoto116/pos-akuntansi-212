@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=8>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            No. Batch
                            <br>No. Akun Jon(Nama Akun Job)
                        </td>
                        <td>Tanggal</td>
                        
                        <td>Keterangan
                            <br>No. Barang(No. Akun)
                        </td>
                        <td>Nilai Barang
                            <br>Detail Status
                        </td>
                        <td>Nilai Beban
                            <br>Kts
                        </td>
                        <td>Total Pembiayaan
                            <br>Satuan
                        </td>
                        <td>Tanggal Selesai
                            <br>Alokasi Biaya
                        </td>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=8 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($item as $itemKey => $itemPembiayaanPesananSelesai)
                        {{-- {{dd($itemPembiayaanPesananSelesai)}} --}}
                        `   <tr style="font-weight: bold">
                                <td>{{  $itemKey                                            }}</td>
                                <td>{{  $itemPembiayaanPesananSelesai[0]['tanggal']         }}</td>
                                <td>{{  $itemPembiayaanPesananSelesai[0]['keterangan']      }}</td>
                                <td>{{  $itemPembiayaanPesananSelesai[0]['cost']            }}</td>
                                <td>{{  $itemPembiayaanPesananSelesai[0]['amount']          }}</td>
                                <td>{{  $itemPembiayaanPesananSelesai[0]['total_biaya']     }}</td>
                            <td>{{  $itemPembiayaanPesananSelesai[0]['tanggal_selesai'] }}</td>
                            </tr>
                            @foreach($itemPembiayaanPesananSelesai as $itemSelesai)
                                <tr>
                                    <td style="padding-left: 25px;">
                                        {{  $itemSelesai['akun_pembiayaan']   }}
                                        {{  $itemSelesai['nama_akun']         }}
                                    </td>
                                    <td></td>
                                    <td>{{  $itemSelesai['status']            }}</td>
                                    <td>{{  $itemSelesai['kuantitas']         }}</td>
                                    <td>{{  $itemSelesai['satuan']            }}</td>
                                    <td></td>
                                    <td>{{  $itemSelesai['total_biaya']       }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan=6></td>
                                <td class="border-top" style="color:blueviolet;">
                                    {{  $itemSelesai['total_biaya']       }}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection