@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4> {{ $perusahaan }}   </h4></p>
                            <p><h6> {{ $title }}        </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{  $subTitle  }}   </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan Barang</th>
                        <th>Kuantitas</th>
                        <th>Unit 1</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody class="post-all">
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=4 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    {{  $subTitle  }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($item as $item_key => $dataPenjualanPerbarang)
                            <tr class="post-original">
                                <td>{{ $dataPenjualanPerbarang['nama_barang']          }}</td>
                                <td>{{ $dataPenjualanPerbarang['kuantitas']            }}</td>
                                <td>{{ $dataPenjualanPerbarang['satuan']               }}</td>
                                <td>{{ number_format($dataPenjualanPerbarang['total']) }}</td>
                            </tr>
                        @endforeach
                        <tr class="post-data">
                            <td>Total</td>
                            <td class="border-top sumDataKuantitas">{{   number_format($dataPenjualanPerbarang['sumKuantitas'])     }}</td>
                            <td></td>
                            <td class="border-top sumDataTotal">{{   number_format($dataPenjualanPerbarang['sumTotal'])          }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function(){
            if ( '{{ $checkUs }}' == 1 ) {
                var urlRoute = "{{ route( 'akuntansi.get-laporan-penjualan-perbarang-pos' ) }}";
            } else {
                var urlRoute = "{{ route( 'akuntansi.get-laporan-penjualan-perbarang-perkasir' ) }}";
            }
            var
                page = 1,
                start = "{{ $dari_tanggal }}",
                end   = "{{ $ke_tanggal }}"
            ;

            (
                function(){
                    $('.ti-save').hide();
                    let tipe = 'generatePDF';
                    $.ajax({
                        url: urlRoute,
                        data: {
                            startDate : start,
                            endDate : end,
                            tipe : tipe,
                            kasir_id : "{{ $kasir_id }}",
                        },
                        dataType: 'JSON',
                        type: 'GET',
                        beforeSend: function( XMLHttpRequest ){
                            $('.save-page').append('<div id="ajax-loading"><i class="loading-btn fa fa-circle-o-notch fa-spin"></i> Loading </div>');
                        }
                    })
                    .done( function ( data ) {
                        if ( data.code == 200 ) {
                            $('#ajax-loading').hide();
                            $('.ti-save').show();
                            $('.save-page').prop('disabled', false)
                        } else {
                        }
                    })
                    .fail( function () {
                        
                    });
                }
            )();
            
            $(window).scroll(function() {
                if($(window).scrollTop() + $(window).height() >= $(document).height()) {
                    page++;
                    loadMoreData(page);
                }
            });

            function loadMoreData(page){
                if (page > {{ $item->lastPage() }}) {
                    page = {{ $item->lastPage() }};
                    // Do Nothing . . .
                }else{
                    var
                        save  = 0,
                        tipe  = 'scroll'
                    ;

                    $.ajax({
                        url : urlRoute,
                        data : {
                            page        : page,
                            startDate   : start,
                            endDate     : end,
                            save        : save,
                            tipe        : tipe,
                            kasir_id : "{{ $kasir_id }}",
                        },
                        type: "get",
                        dataType : 'JSON',
                        beforeSend: function(){
                            $('.ajax-load').show();
                        }
                    })
                    .done(function(data){
                        if(data == ""){
                            $('.ajax-load').html("Semua Data Telah Berhasil di Tampilkan");
                            return;
                        }
                        $('.ajax-load').hide();
                        $.map(data, function(item, index) {
                            var sumKuantitas = 0;
                            $.map(item, function(itemData, index) {
                                $(".post-data").before("<tr class='post-original'>"+
                                                            "<td>"+itemData['namaBarang']+"</td>"+
                                                            "<td>"+itemData['kuantitas']+"</td>"+
                                                            "<td>"+itemData['satuan']+"</td>"+
                                                            "<td class='totalLoad'>"+itemData['total']+"</td>"+
                                                        "</tr>");
                                $('.totalLoad').autoNumeric('init', {vMin: '-999999999', vMax: '999999999' })
                            });
                            var htmlSumKuantitas    =   parseInt($('.sumDataKuantitas').html());
                            var sumKuantitas        =   htmlSumKuantitas + item[item.length -1]['sumKuantitas'];
                            var htmlSumTotal        =   parseInt($('.sumDataTotal').html().replace(/,/g,""));
                            var sumTotal            =   htmlSumTotal + item[item.length -1]['sumTotal'];
                            
                            $(".post-data").html("<td>Total</td>"+
                                                "<td class='border-top sumDataKuantitas'>"+ sumKuantitas +"</td>"+
                                                "<td></td>"+
                                                "<td class='border-top sumDataTotal'>"+ sumTotal +"</td>");
                            $('.sumDataKuantitas').autoNumeric('init', {vMin: '-999999999', vMax: '999999999' })
                            $('.sumDataTotal').autoNumeric('init', {vMin: '-999999999', vMax: '999999999' })
                        });
                    })
                    .fail(function(jqXHR, ajaxOptions, thrownError){
                        alert('Server tidak Merespon');
                        page--;
                    });
                }
            }

            $('.print-page').click(function(e){
                var
                    allPage = {{ $item->lastPage() }},
                    tipe    = 'print';
                $.ajax({
                    url : urlRoute,
                    data : {
                        startDate   : start,
                        endDate     : end,
                        tipe        : tipe,
                        kasir_id : "{{ $kasir_id }}",
                    },
                    dataType : 'JSON',
                    type: "get",
                    timeout: 6000,
                    beforeSend: function(){
                        hidePage();
                    }
                })
                .done(function(data){
                    showPage();
                    $('.post-original').remove();
                    $('.post-data').remove();
                    if(data == ""){
                        $('.ajax-load').html("No more records found");
                        return;
                    }
                    $('.ajax-load').hide();
                    $.map(data, function(item, index) {
                        var sumKuantitas = 0;
                        $(".post-all").append("<tr class='post-original'>"+
                                                    "<td>"+item['nama_barang']+"</td>"+
                                                    "<td>"+item['kuantitas']+"</td>"+
                                                    "<td>"+item['satuan']+"</td>"+
                                                    "<td class='totalLoad'>"+item['total']+"</td>"+
                                                "</tr>");
                        $('.totalLoad').autoNumeric('init', {vMin: '-999999999', vMax: '999999999' })
                    });
                    $(".post-all").after("<tr class='post-data'>"+
                                            "<td>Total</td>"+
                                            "<td class='border-top sumDataKuantitas'>"+ data[data.length - 1]['sumKuantitas'] +"</td>"+
                                            "<td></td>"+
                                            "<td class='border-top sumDataTotal'>"+ data[data.length - 1]['sumTotal'] +"</td>"+
                                            "</tr>");
                    $('.sumDataKuantitas').autoNumeric('init', {vMin: '-999999999', vMax: '999999999' })
                    $('.sumDataTotal').autoNumeric('init', {vMin: '-999999999', vMax: '999999999' })
                    page = allPage;
                    setTimeout(function() { window.print(); }, 500);
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    alert('Server Gagal Merespon');
                    showPage();
                });
            });

            $(".save-page").click(function (e) {
                e.preventDefault();
                let
                    tipe  = 'download'
                ;
                $.ajax({
                    url : urlRoute,
                    data : {
                        startDate   : start,
                        endDate     : end,
                        tipe        : tipe,
                        kasir_id : "{{ $kasir_id }}",
                    },
                    dataType : 'JSON',
                    type: "get",
                    timeout: 6000,
                }).done(function(data){
                    var link = document.createElement("a");
                    link.download = data.title;
                    link.href = data.path;
                    document.body.appendChild(link);
                    if (link.click()) {
                        document.body.removeChild(link);
                        delete link;
                    }
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    alert('Server Gagal Merespon');
                    page--;
                });
            });
        });
    </script>
@endsection