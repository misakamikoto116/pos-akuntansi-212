@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=3>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan Barang</th>
                        <th>Satuan</th>
                        <th><b> {{ $subTitle   }} </b></th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=3 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                {{ $subTitle   }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php $total = 0; @endphp
                    @foreach($item as $penjualanPerbarang => $dataPenjualan)
                        @php $subTotal = 0; @endphp
                        @foreach($dataPenjualan as $data)
                            <tr>
                                <td>{{ $penjualanPerbarang   ??  null }}</td>
                                <td>{{ $data['unit']         ??  null }}</td>
                                <td>
                                    @for( $i = 0; $i < 1; $i++)
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    @endfor
                                    {{ $data['total']        ??  null }}
                                </td>
                            </tr>
                            @php $subTotal += $data['total']; @endphp
                        @endforeach
                        @php $total += $subTotal; @endphp
                    @endforeach
                    <tr>
                        <td colspan=2></td>
                        <td class="border-top">
                            @for( $i = 0; $i < 1; $i++)
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            @endfor{{$total}}
                        </td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection