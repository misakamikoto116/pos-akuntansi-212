@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title='Laporan Pendapatan RMA'}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=5>
                            <p><h4>CHICHI</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari Tanggal 01 Mei Ke 30 Mei 2018</p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. RMA</th>
                        <th>Tanggal</th>
                        <th>Nilai Komponen</th>
                        <th>Biaya</th>
                        <th>Laba Kotor</th>
                    </tr>
                </thead>
                    <!-- Perulangan Pendapatan RMA -->
                    <tr>    
                        <td>RMA/05/001</td>
                        <td>4 Mei 2018</td>
                        <td>1.020.000</td>
                        <td>0</td>
                        <td>1.020.000</td>
                    </tr>
                    <!-- Akhir Perulangan Pendapatan RMA -->
                <tr>
                    <td colspan="2"></td>
                    <td class="border-top">1.020.000</td>
                    <td class="border-top">0</td>
                    <td class="border-top">1.020.000</td>
                </tr>
            </table>
        </div>
    </div>
@endsection