@extends('akuntansi::laporan.laporan_khusus')
@section('title')
    {{ $title }}
@endsection
<style type="text/css">
    .td-blue-left-bold {
        text-align: left;
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
    }
    .td-blue-bold {
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
        text-align: left;
    }
</style>
@section('laporan_khusus')

        <section class="sheet padding-0mm">
            <article>
                <div class="row">
                    <div class="col-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td colspan=6>
                                        <p><h4>{{ $nama_perusahaan }}</h4></p>
                                        <p><h6>{{ $title }}</h6></p>
                                        <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                                        <p>Dari 1 ke 2</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td-blue-left-bold">No. Akun</td>
                                    <td class="td-blue-bold">: A</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td-blue-left-bold">Nama Akun</td>
                                    <td class="td-blue-bold">: A</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td-blue-left-bold">Mata Uang</td>
                                    <td class="td-blue-bold">: A</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td-blue-left-bold">Tgl. Pernyataan</td>
                                    <td class="td-blue-bold">: A</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td-blue-left-bold">Saldo Bank Pada</td>
                                    <td class="td-blue-bold">: A</td>
                                </tr>
                                <tr>
                                    <th width="15%">Tanggal</th>
                                    <th>No Cek</th>
                                    <th style="text-align: center;">Keterangan</th>
                                    <th style="text-align: center;">Nilai Tukar</th>
                                    <th style="text-align: center;">Setoran (Dr)</th>
                                    <th style="text-align: center;">Penarikan (Cr)</th>
                                </tr>
                            </thead>
                            <tr>
                                <td colspan="6"><strong>Telah Direkonsiliasi</strong></td>
                            </tr>
                            <!-- Perulangan Laporan Pelanggan -->
                                <tr>
                                    <td width="10%">A</td>
                                    <td>B</td>
                                    <td>C</td>
                                    <td style="text-align: right;">D</td>
                                    <td style="text-align: right;">E</td>
                                    <td style="text-align: right;">F</td>
                                </tr>
                            <!-- Akhir Perulangan Laporan Pelanggan -->
                                <tr>
                                    <td colspan="4"></td>
                                    <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">Hasil</td>
                                    <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">Hasil</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="6"><strong>Dalam Peredaran (Belum Direkonsiliasi)</strong></td>
                                </tr>
                            <!-- Perulangan Laporan Pelanggan -->
                                <tr>
                                    <td width="10%">A</td>
                                    <td>B</td>
                                    <td>C</td>
                                    <td style="text-align: right;">D</td>
                                    <td style="text-align: right;">E</td>
                                    <td style="text-align: right;">F</td>
                                </tr>
                            <!-- Akhir Perulangan Laporan Pelanggan -->
                                <tr>
                                    <td colspan="4"></td>
                                    <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">Hasil</td>
                                    <td class="border-top" style="text-align: right; color: #0066cc; font-weight: bold;">Hasil</td>
                                    <td></td>
                                </tr>
                        </table>
                    </div>
                </div>
            </article>
        </section>
