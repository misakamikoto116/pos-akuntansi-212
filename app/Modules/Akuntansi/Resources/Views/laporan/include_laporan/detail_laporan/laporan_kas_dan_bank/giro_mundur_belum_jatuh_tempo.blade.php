@extends('akuntansi::laporan.laporan')
@section('title')
Laporan Giro Mundur/Belum Jatuh Tempo
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="8">
                            <p><h4>CHICHI</h4></p>
                            <p><h6>Giro Mundur/Belum Jatuh Tempo</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Per Tanggal 30 Mei 2018 <!-- Tanggal --></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal Transaksi</th>
                        <th>Tanggal Cek</th>
                        <th>No. Cek</th>
                        <th>Sumber</th>
                        <th>Nama Pelanggan/Nama Pemasok</th>
                        <th>Setoran</th>
                        <th>Pembayaran</th>
                        <th>Nilai Tukar</th>
                    </tr>
                </thead>
                <!-- Perulangan KAS -->
                <tr>
                    <td>1101-001</td>
                    <td>Kas IDR</td>
                    <td colspan="6"IDR></td>
                </tr>
                    <!-- ulang -->
                    <tr>
                        <td>19 Mei 2018</td>
                        <td>19 Mei 2018</td>
                        <td>KD.004</td>
                        <td>Aktiva Tetap</td>
                        <td></td>
                        <td>0</td>
                        <td>10.000.000</td>
                        <td>1</td>
                    </tr>
                    <!--  -->
                    <tr>
                        <td colspan="5"></td>
                        <td class="border-top">0</td>
                        <td class="border-top">10.000.000</td>
                    </tr>
                <!-- Akhir Perulangan KAS-->
            </table>
        </div>
    </div>
@endsection