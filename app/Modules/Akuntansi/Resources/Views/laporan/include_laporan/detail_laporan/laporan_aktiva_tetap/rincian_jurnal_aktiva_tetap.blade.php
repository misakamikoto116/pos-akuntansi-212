@extends('akuntansi::laporan.laporan_khusus')
@section('title')
    {{ $title }}
@endsection
<style type="text/css">
    .td-blue-left-bold {
        text-align: left;
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
    }
    .td-blue-bold {
        color: #0066cc;
        font-weight: bold; 
        line-height: 3px;
        text-align: left;
    }
</style>
@section('laporan_khusus')
    @if ($item->isEmpty())
        <section class="sheet padding-0mm">
            <article>
                <div class="alert alert-warning" style="text-align:center;">
                    Tidak ada data {{ $title }} yang tersedia <br>
                    {{  $subTitle  }}.
                </div>
        </section>
    @else
        @foreach ($item as $item_aktiva)
            <section class="sheet padding-0mm">
                <article>
                    <table class="table">
                        <tr>
                            <td colspan=8>
                                <div style="text-align:center;">
                                    <p><h4> {{ $perusahaan }}   </h4></p>
                                    <p><h6> {{ $title }}        </h6></p>
                                    <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                                    <p><b>  {{  $subTitle  }}   </b></p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Kode Aktiva Tetap</td>
                            <td>:</td>
                            <td>{{ $item_aktiva->first()['kode_aktiva'] }}</td>
                            <td></td><td></td>
                            <td>Akun-akun Aktiva</td>
                            <td>:</td>
                            <td>{{ $item_aktiva->first()['akun_aktiva'] }}</td>
                        </tr>
                        <tr>
                            <td>Nama Aktiva Tetap</td>
                            <td>:</td>
                            <td>{{ $item_aktiva->first()['nama_aktiva'] }}</td>
                            <td></td><td></td>
                            <td>Akun Akumulasi Penyusutan</td>
                            <td>:</td>
                            <td>{{ $item_aktiva->first()['akun_akum_penyusutan'] }}</td>
                        </tr>
                        <tr class="border-bottom">
                            <td>Nama Tipe Aktiva Tetap</td>
                            <td>:</td>
                            <td>{{ $item_aktiva->first()['nama_tipe_aktiva'] }}</td>
                            <td></td><td></td>
                            <td>Akun Beban Penyusutan</td>
                            <td>:</td>
                            <td>{{ $item_aktiva->first()['akun_beban_penyusutan'] }}</td>
                        </tr>
                        <tr style="font-weight: bold;">
                            <td></td>
                            <td width="100px" class="border-bottom">No. Sumber</td>
                            <td width="100px" class="border-bottom">No. Akun</td>
                            <td width="200px" class="border-bottom">Nama Akun</td>
                            <td width="100px" class="border-bottom">Debit (Domestik)</td>
                            <td width="100px" class="border-bottom">Kredit (Domestik)</td>
                            <td></td>
                        </tr>
                        @php
                            $sumDebit = 0; $sumKredit = 0;
                        @endphp
                        @foreach ($item_aktiva as $as => $data_aktiva)
                            @foreach ($data_aktiva['arrayDetail'] as $key => $item)
                                <tr>
                                    <td style="padding-left : 90px; font-weight: bold; " colspan="7">{{ $key }}</td>
                                    <td></td>
                                </tr>
                                @foreach ($item as $data)
                                    <tr>
                                        <td></td>
                                        <td>{{ $data['no_sumber'] }}</td>
                                        <td>{{ $data['no_akun'] }}</td>
                                        <td>{{ $data['nama_akun'] }}</td>
                                        <td>{{ number_format($data['debit'],2) }}</td>
                                        <td>{{ number_format($data['kredit'],2) }}</td>
                                        <td colspan="2"></td>
                                    </tr>
                                    @php
                                        $sumDebit += $data['debit']; $sumKredit += $data['kredit'];
                                    @endphp
                                @endforeach
                            @endforeach
                        <tr>
                            <td></td>
                            <td class="border-top" colspan="3"></td>
                            <td class="border-top" style="font-weight: bold; ">{{ number_format($sumDebit, 2) }}</td>
                            <td class="border-top" style="font-weight: bold; ">{{ number_format($sumKredit, 2) }}</td>
                            <td colspan="2"></td>
                        </tr>
                        @endforeach
                    </table>
                </article>
            </section>
        @endforeach
    @endif
@endsection