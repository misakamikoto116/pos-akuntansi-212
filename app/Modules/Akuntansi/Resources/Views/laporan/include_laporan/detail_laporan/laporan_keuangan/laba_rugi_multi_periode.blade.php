<style>
    .pad-row{
        padding: 15px;
    }
</style>
@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    @php
        $periodes = $priodes->count()-1;
    @endphp
    <div class="row pad-row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan={{$priodes->count()}}>
                            <p><h4>{{ $nama_perusahaan }}<h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Periode {{$priodes->first()}} sampai {{ $priodes->slice(0, $periodes)->last() }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan</th>
                        @foreach ($priodes->slice(0,$periodes) as $priode)
                            <th>{{$priode}}</th>
                        @endforeach
                    </tr>
                </thead>                
                @foreach ($items as $header_akun => $data_all_akun)

                    <tr>
                        <th colspan="{{$priodes->count()+1}}">{{ $header_akun ?? null }}</th>
                    </tr>

                    @foreach ($data_all_akun as $tipe_akun_parent => $data_per_tipe_akun)

                        @if (!empty($data_per_tipe_akun['child_and_parent']))

                            @foreach ($data_per_tipe_akun['child_and_parent'] as  $data_akun)

                                @if($data_akun['akun_child']->isNotEmpty())

                                    @if (array_key_exists('nama_akun_parent', $data_akun))

                                        <tr>

                                            <th style="padding-left: 25px;">{{ $data_akun['nama_akun_parent'] ?? null }}</th>
                                    
                                            @foreach ($data_akun['sum_akun_parent'] as $item)

                                                <th style="text-align: right;">{{ number_format($item) ?? null }}</th>
                                            
                                            @endforeach

                                        </tr>

                                    @endif

                                    @foreach ($data_akun['akun_child'] as $data_child)

                                        <tr>

                                            <td style="padding-left: 40px;">{{ $data_child['nama_akun_child'] ?? null }}</td>

                                            @foreach ($data_child['sum_akun_child'] as $item)

                                                <td style="text-align: right;">{{ number_format($item) ?? null }}</td>

                                            @endforeach
                                            
                                        </tr>
                                    
                                    @endforeach

                                @elseif($data_akun['akun_child']->isEmpty())

                                    @if (array_key_exists('nama_akun_parent', $data_akun))

                                        <tr>
                                            <th style="font-weight: normal; padding-left: 25px;">{{ $data_akun['nama_akun_parent'] ?? null }}</th>

                                            @foreach ($data_akun['sum_akun_parent'] as $item)

                                                <th style="text-align: right; font-weight: normal;">{{ number_format($item) }}</th>

                                            @endforeach

                                        </tr>

                                    @endif

                                @endif

                            @endforeach

                            <tr>
                                <th>Jumlah {{ $tipe_akun_parent }}</th>

                                @foreach ($data_per_tipe_akun['sum_tipe_akun'] as $item)

                                    <th style="text-align: right; border-top: 1px solid;">{{ number_format($item ?? 0) }}</th>

                                @endforeach
                            
                            </tr>

                            @if ($tipe_akun_parent == 'Harga Pokok Penjualan')

                                <tr>
                                    <th>LABA KOTOR</th>
                                    
                                    @foreach ($laba_rugi['laba_kotor'] as $item)

                                        <th style="text-align: right; border-top: 1px solid;">{{ number_format( $item ?? 0) }}</th>

                                    @endforeach

                                </tr>                                

                            @endif

                            @if ($tipe_akun_parent == 'Beban')

                                <tr>
                                    <th>PENDAPATAN OPERASI</th>

                                     @foreach ($laba_rugi['pendapatan_operasi'] as $item)

                                        <th style="text-align: right; border-top: 1px solid;">{{ number_format( $item ?? 0) }}</th>
                                
                                    @endforeach

                                </tr>                                

                            @endif

                        @else

                            @foreach ($data_per_tipe_akun as $tipe_akun_parents => $data_per_tipe_akuns)

                                <tr>
                                    <th style="padding-left: 25px;">{{ $tipe_akun_parents ?? null }}</th>
                                </tr>

                                @foreach ($data_per_tipe_akuns['child_and_parent'] as  $data_akun)

                                    @if($data_akun['akun_child']->isNotEmpty())

                                        @if (array_key_exists('nama_akun_parent', $data_akun))

                                            <tr>

                                                <th style="padding-left: 40px;">{{ $data_akun['nama_akun_parent'] ?? null }}</th>
                                                
                                                @foreach ($data_akun['sum_akun_parent'] as $item)

                                                    <th style="text-align: right;">{{ number_format($item) ?? null }}</th>

                                                @endforeach

                                            </tr>

                                        @endif

                                        @foreach ($data_akun['akun_child'] as $data_child)

                                            <tr>

                                                <td style="padding-left: 55px;">{{ $data_child['nama_akun_child'] ?? null }}</td>
                                            
                                                @foreach ($data_child['sum_akun_child'] as $item)
                                                    
                                                    <td style="text-align: right;">{{ number_format($item) ?? null }}</td>
                                                
                                                @endforeach
                                            
                                            </tr>
                                        
                                        @endforeach

                                    @elseif($data_akun['akun_child']->isEmpty())

                                        @if (array_key_exists('nama_akun_parent', $data_akun))

                                            <tr>
                                                <th style="font-weight: normal; padding-left: 40px;">{{ $data_akun['nama_akun_parent'] ?? null }}</th>
                                                
                                                @foreach ($data_akun['sum_akun_parent'] as $item)

                                                    <th style="text-align: right; font-weight: normal;">{{ number_format($item) }}</th>

                                                @endforeach

                                            </tr>                                    

                                        @endif

                                    @endif

                                @endforeach

                                <tr>
                                    
                                    <th style="padding-left: 25px;">Jumlah {{ $tipe_akun_parents }}</th>
                                
                                    @foreach ($data_per_tipe_akuns['sum_tipe_akun'] as $item)
                                        
                                        <th style="text-align: right; border-top: 1px solid;">{{ number_format( $item ?? 0) }}</th>
                                    
                                    @endforeach

                                </tr>

                            @endforeach

                            @if ($tipe_akun_parents == 'Beban lain-lain')

                                <tr>

                                    <th>Jumlah Pendapatan dan Beban Lain</th>
                                    
                                    @foreach ($laba_rugi['jumlah_pendapatan_beban_lain'] as $item)

                                        <th style="text-align: right; border-top: 1px solid;">{{ number_format( $item ?? 0) }}</th>

                                    @endforeach
                                
                                </tr>                                

                            @endif

                        @endif

                    @endforeach

                @endforeach


                <tr>
                    <th>LABA(RUGI) Bersih (Sebelum Pajak)</th>

                    @foreach ($laba_rugi['jumlah_pendapatan_beban_lain'] as $item)

                        <th style="text-align: right; border-top: 1px solid;">{{ number_format($item ?? 0) }}</th>
                
                    @endforeach

                </tr>
                 
                <tr>

                    <th>Beban Pajak Penghasilan</th>
                    
                    @foreach ($laba_rugi['laba_rugi_sebelum_pajak'] as $item)
                                            {{-- expr --}}
                        <th style="text-align: right;">{{ number_format($laba_rugi_sebelum_pajak ?? 0) }}</th>
                    
                    @endforeach                    
               
                </tr>

                <tr>

                    <th>LABA(RUGI) Bersih (Setelah Pajak)</th>
                    
                    @foreach ($laba_rugi['laba_rugi_sebelum_pajak'] as $item)

                        <th style="text-align: right; border-top: 1px solid;">{{ number_format($item ?? 0) }}</th>
                    
                    @endforeach    
                
                </tr>

                    {{-- <tr>
                        <th>{{$sum_neraca_group[$i]['title']}}</th>
                        @foreach ($sum_neraca_group[$i]['jumlah'] as $priode)
                            <th style="text-align: right;">{{number_format($priode)}}</th>
                        @endforeach
                    </tr>  --}}
            </table>
        </div>
    </div>
@endsection