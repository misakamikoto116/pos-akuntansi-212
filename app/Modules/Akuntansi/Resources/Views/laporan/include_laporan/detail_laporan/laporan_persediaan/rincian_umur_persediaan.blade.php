@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=9>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Per <b> {{ $dari_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Transaksi</th>
                        <th>Tanggal Transaksi</th>
                        <th>Jenis Transaksi</th>
                        <th>Qty. (1-30)</th>
                        <th>Qty. (31-60)</th>
                        <th>Qty. (61-90)</th>
                        <th>Qty. (91-120)</th>
                        <th>Qty. (>121)<th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=9 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @foreach($item as $produk)
                        <tr style="font-weight: bold">
                          <td colspan="1">{{$produk['nama_produk']}}</td>
                          <td colspan="2" style="text-align:left">{{$produk['no_produk']}}</td>
                          <td colspan="5">{{$produk['sum']}}</td>
                        </tr>
                        @foreach($produk['transaksi'] as $transaksi)
                            <tr>
                              <td>{{$transaksi['no_transaksi']}}</td>
                              <td>{{$transaksi['tanggal']}}</td>
                              <td>{{$transaksi['jenis_transaksi']}}</td>
                              @foreach ($transaksi['kuantitas'] as $kuantitas)
                                <td>{{$kuantitas}}</td>
                              @endforeach
                            </tr>
                        @endforeach
                        <tr>
                          <td colspan="3">&nbsp;</td>
                          @foreach ($produk['sumUmur'] as $umur)
                            <td class="border-top">{{$umur}}</td>
                          @endforeach
                        </tr>
                    @endforeach
                    
                @endif
            </table>
        </div>
    </div>
@endsection