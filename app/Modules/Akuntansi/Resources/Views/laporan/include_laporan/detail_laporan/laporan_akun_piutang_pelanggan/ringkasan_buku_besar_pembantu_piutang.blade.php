@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title ='Laporan Ringkasan Buku Besar Pembantu Piutang'}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=8>
                            <p><h4>{{ $nama_perusahaan->nama_perusahaan ?? null }}</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir ?? null }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Nama Pelanggan</th>
                        <th>Saldo Awal</th>
                        <th>Faktur Penjualan</th>
                        <th>Penerimaan Penjualan</th>
                        <th>Retur Penjualan</th>
                        <th>Uang Muka</th>
                        <th>Bukti Jurnal</th>
                        <th>Saldo</th>
                    </tr>
                </thead>
                @php
                    $grand_saldo_awal                   = 0;
                    $grand_faktur_penjualan             = 0;
                    $grand_penerimaan_penjualan         = 0;
                    $grand_retur_penjualan              = 0;
                    $grand_uang_muka                    = 0;
                    $grand_bukti_jurnal                 = 0;
                    $grand_saldo                        = 0;
                @endphp 
                <!-- Perulangan Tipe Mata Uang-->
                    <tr>
                        <td colspan="8"><strong>IDR</strong></td>
                    </tr>
                    @foreach ($items as $data_pelanggan)
                        @php
                            $grand_saldo_awal                  += $data_pelanggan->saldo_awal;
                            $grand_faktur_penjualan            += $data_pelanggan->sum_faktur_penjualan;
                            $grand_penerimaan_penjualan        += $data_pelanggan->sum_penerimaan_penjualan;
                            $grand_retur_penjualan             += $data_pelanggan->sum_retur_penjualan;
                            $grand_uang_muka                   += $data_pelanggan->sum_uang_muka;
                            $grand_saldo                       += $data_pelanggan->saldo;
                        @endphp
                        <tr>
                            <td>{{ $data_pelanggan->nama ?? null }}</td>
                            <td>{{ number_format($data_pelanggan->saldo_awal) }}</td>
                            <td>{{ number_format($data_pelanggan->sum_faktur_penjualan) }}</td>
                            <td>{{ number_format($data_pelanggan->sum_penerimaan_penjualan) }}</td>
                            <td>{{ number_format($data_pelanggan->sum_retur_penjualan) }}</td>
                            <td>{{ number_format($data_pelanggan->sum_uang_muka) }}</td>
                            <td>{{ number_format($data_pelanggan->sum_bukti_jurnal) }}</td>
                            <td>{{ number_format($data_pelanggan->saldo) }}</td>
                        </tr>
                    @endforeach
                    <!-- Akhir Perulangan Ringkasan Buku Besar Pembantu Piutang -->
                    <tr>
                        <td style="color: #0066cc; font-weight: bold;">Total dari IDR</td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">
                            {{ number_format($grand_saldo_awal) }}
                        </td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">
                            {{ number_format($grand_faktur_penjualan) }}
                        </td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">
                            {{ number_format($grand_penerimaan_penjualan) }}
                        </td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">
                            {{ number_format($grand_retur_penjualan) }}
                        </td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">
                            {{ number_format($grand_uang_muka) }}
                        </td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">
                            {{ number_format($grand_bukti_jurnal) }}
                        </td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">
                            {{ number_format($grand_saldo) }}
                        </td>
                    </tr>
                <!-- Akhir Perulangan -->
            </table>
        </div>
    </div>
@endsection