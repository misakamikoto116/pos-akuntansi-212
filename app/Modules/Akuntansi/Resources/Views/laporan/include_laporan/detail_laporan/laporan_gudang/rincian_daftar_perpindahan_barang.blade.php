@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=5>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Barang</th>
                        <th>Keterangan Barang</th>
                        <th>Kuantitas Default</th>
                        <th>Kuantitas</th>
                        <th>Satuan</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=5 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php $totalQty = 0; @endphp
                        @foreach($item as $item_key => $dataPindahBarang)
                            <tr>
                                <td>
                                    <div style="float: left;">{{ $item_key }}</div>
                                    <div style="float: right;">{{ $dataPindahBarang[0]['tgl_transfer'] }}</div>
                                </td>
                                <td>{{ $dataPindahBarang[0]['keterangan'] }}</td>
                                <td>{{ $dataPindahBarang[0]['dari_gudang'] }}</td>
                                <td>{{ $dataPindahBarang[0]['ke_gudang'] }}</td>
                                <td></td>
                            </tr>
                            @php $subTotalQty = 0; @endphp
                            @foreach($dataPindahBarang as $dataPindah)
                                <tr>
                                    <td style="padding-left: 25px;">{{ $dataPindah['kode_barang'] }}</td>
                                    <td>{{ $dataPindah['nama_barang'] }}</td>
                                    <td>{{ $dataPindah['qtydefault'] }}</td>
                                    <td>{{ $dataPindah['qty'] }}</td>
                                    <td>{{ $dataPindah['satuan'] }}</td>
                                </tr>
                                @php $subTotalQty += $dataPindah['qtydefault']; @endphp
                            @endforeach
                            <tr>
                                <td colspan=2></td>
                                <td class="border-top">{{ $subTotalQty }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>                
            </table>
        </div>
    </div>
@endsection