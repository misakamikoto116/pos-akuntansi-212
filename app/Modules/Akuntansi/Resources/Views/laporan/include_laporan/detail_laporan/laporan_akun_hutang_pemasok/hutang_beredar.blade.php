@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=7>
                            <p><h4>{{ $perusahaan->nama_perusahaan ?? null }}</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Per Tanggal {{ $tanggal }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Faktur</th>
                        <th>Tanggal Faktur</th>
                        <th>Jatuh Tempo</th>
                        <th>Nilai Faktur</th>
                        <th>Hutang (Asing)</th>
                        <th>Hutang Pajak</th>
                        <th>Berdasarkan Jatuh Tempo</th>
                    </tr>
                </thead>
                <!-- Perulangan Mata Uang -->
                    <tr>
                        <td colspan="7">IDR</td>
                    </tr>
                    @foreach ($items as $item)
                        <tr>
                            <td><strong>{{ $item['nomor_pemasok'] ?? null }}</strong></td>
                            <td colspan="6"><strong>{{ $item['nama_pemasok'] ?? null }}</strong></td>
                        </tr>
                        <!-- Perulangan Hutang Beredar -->
                        @foreach ($item['faktur'] as $data_faktur)
                            <tr>
                                <td>{{ $data_faktur['no_faktur'] ?? null }}</td>
                                <td>{{ $data_faktur['tanggal_faktur'] ?? null }}</td>
                                <td>{{ $data_faktur['jatuh_tempo'] ?? null }}</td>
                                <td>{{ $data_faktur['nilai_faktur'] ?? null }}</td>
                                <td>{{ $data_faktur['hutang_asing'] ?? null }}</td>
                                <td>0</td>
                                <td>{{ $data_faktur['tgl_jatuh_tempo'] }} Hari</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3"></td>
                            <td class="border-top">{{ $item['sum_total'] ?? null}}</td>  
                            <td class="border-top">{{ $item['sum_hutang_asing'] ?? null}}</td>
                            <td class="border-top">0</td>
                            <td></td>
                        </tr>
                        <!-- Akhir Perulangan Hutang Beredar -->
                    <!-- Akhir Perulangan Faktur -->
                    @endforeach
                    <tr>
                        <td colspan="3"></td>
                        <td class="border-top">{{ $grand_sum_total ?? null }}</td>  
                        <td class="border-top">{{ $grand_sum_hutang_asing ?? null }}</td>
                        <td class="border-top">0</td>
                        <td></td>
                    </tr>
                <!-- Akhir Perulangan Mata Uang -->
            </table>
        </div>
    </div>
@endsection