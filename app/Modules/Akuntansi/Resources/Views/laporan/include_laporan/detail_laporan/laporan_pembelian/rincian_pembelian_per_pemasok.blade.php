@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan='4'>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Faktur</th>
                        <th>Tanggal Faktur</th>
                        <th>Keterangan</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=4 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php $total = 0; @endphp
                    @foreach($item as $item_key => $dataPembelian)
                        @php $subTotal = 0; @endphp
                        <tr>
                            <td>{{ $item_key ?? null }}</td>
                            <td colspan=3>{{ $dataPembelian[0]['nama'] ?? null }}</td>
                        </tr>
                        @foreach($dataPembelian as $data)
                            <tr>
                                <td>{{ $data['no_faktur']              ?? null }}</td>
                                <td>{{ $data['tanggal_faktur']         ?? null }}</td>
                                <td>{{ $data['keterangan']             ?? null }}</td>
                                <td>{{ number_format($data['total'])   ?? null }}</td>
                            </tr>
                            @php $subTotal += $data['total'] ?? null; @endphp
                        @endforeach
                        <tr>
                            <td colspan=3></td>
                            <td class="border-top">{{ number_format($subTotal) }}</td>
                        </tr>
                        @php $total += $subTotal; @endphp
                    @endforeach
                    <tr>
                        <td colspan=2></td>
                        <td>Rincian Total</td>
                        <td class="border-top">{{ number_format($total) }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection