@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title='Laporan Tagihan RMA'}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=7>
                            <p><h4>CHICHI</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari Tanggal 01 Mei Ke 30 Mei 2018</p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. RMA</th>
                        <th>Tanggal</th>
                        <th>No. Faktur</th>
                        <th>Tanggal Faktur</th>
                        <th>Nilai Faktur</th>
                        <th>Durasi</th>
                        <th>Tipe Aksi</th>
                    </tr>
                </thead>
                    <!-- Perulangan Tagihan RMA -->
                    <tr>    
                        <td>RMA/05/001</td>
                        <td>4 Mei 2018</td>
                        <td>Serv/05/001</td>
                        <td>11 Mei 2018</td>
                        <td>1.020.000</td>
                        <td>7 Hari</td>
                        <td>Diperbaiki</td>
                    </tr>
                    <!-- Akhir Perulangan Tagihan RMA -->
            </table>
        </div>
    </div>
@endsection