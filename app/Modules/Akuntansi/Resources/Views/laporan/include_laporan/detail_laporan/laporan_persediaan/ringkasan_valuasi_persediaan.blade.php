@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=9>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Barang</th>
                        <th>Kts. Saldo Awal</th>
                        <th>Nilai Saldo Awal</th>
                        <th>Kts. Masuk</th>
                        <th>Nilai Masuk</th>
                        <th>Kts. Keluar</th>
                        <th>Nilai Keluar</th>
                        <th>Kuantitas</th>
                        <th>Nilai Akhir</th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=9 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php
                        $ktsSaldo  = 0; $nilaiSaldo  = 0; $ktsMasuk  = 0; $nilaiMasuk = 0;
                        $ktsKeluar = 0; $nilaiKeluar = 0; $kuantitas = 0; $nilaiAkhir = 0;
                    @endphp
                    @foreach($item as $item_key => $dataTransaksi)
                        <tr>
                            <td>{{ $dataTransaksi->last()['no_barang']      ?? null }}</td>
                            <td>{{ $dataTransaksi->last()['kts_saldo']      ?? null }}</td>
                            <td>{{ $dataTransaksi->last()['nilai_saldo']    ?? null }}</td>
                            <td>{{ $dataTransaksi->last()['kts_masuk']      ?? null }}</td>
                            <td>{{ $dataTransaksi->last()['nilai_masuk']    ?? null }}</td>
                            <td>{{ $dataTransaksi->last()['kts_keluar']     ?? null }}</td>
                            <td>{{ $dataTransaksi->last()['nilai_keluar']   ?? null }}</td>
                            <td>{{ $dataTransaksi->last()['kuantitas']      ?? null }}</td>
                            <td>{{ $dataTransaksi->last()['nilai_akhir']    ?? null }}</td>
                        </tr>
                        @php
                            $ktsSaldo  += $dataTransaksi->last()['kts_saldo'];  $nilaiSaldo   += $dataTransaksi->last()['nilai_saldo'];
                            $ktsMasuk  += $dataTransaksi->last()['kts_masuk'];  $nilaiMasuk   += $dataTransaksi->last()['nilai_masuk'];
                            $ktsKeluar += $dataTransaksi->last()['kts_keluar']; $nilaiKeluar  += $dataTransaksi->last()['nilai_keluar'];
                            $kuantitas += $dataTransaksi->last()['kuantitas'];  $nilaiAkhir   += $dataTransaksi->last()['nilai_akhir'];
                        @endphp
                    @endforeach
                    <tr>
                        <td>&nbsp;</td>
                        <td class="border-top">{{ number_format($ktsSaldo)      }}</td>
                        <td class="border-top">{{ number_format($nilaiSaldo)    }}</td>
                        <td class="border-top">{{ number_format($ktsMasuk)      }}</td>
                        <td class="border-top">{{ number_format($nilaiMasuk)    }}</td>
                        <td class="border-top">{{ number_format($ktsKeluar)     }}</td>
                        <td class="border-top">{{ number_format($nilaiKeluar)   }}</td>
                        <td class="border-top">{{ number_format($kuantitas)     }}</td>
                        <td class="border-top">{{ number_format($nilaiAkhir)    }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection