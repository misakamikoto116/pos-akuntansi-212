@extends('akuntansi::laporan.laporan_landscape')
@section('title')
    Laporan Laba/Rugi Perbandingan Anggaran Periode
@endsection
@section('laporan')
    <div class="row">
        <div class="col col-12">
            <table class="table">
                <tr>
                    <td colspan="7">
                        <p><h4>CHICHI</h4></p>
                        <p><h6>Laba Rugi (Perbandingan Anggaran Periode)</h6></p>
                        <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                        <p>Periode <!-- Bualn apa sampai Bulan Apa 2018  --></p>
                    </td>
                </tr>
                <tr>
                    <th>Keterangan</th>
                    <th>April Aktual</th>
                    <th>April Anggaran</th>
                    <th>Mei Aktual</th>
                    <th>Mei Anggaran</th>
                    <th>Jumlah Aktual</th>
                    <th>Jumlah Anggaran</th>
                </tr>
                <tr>
                    <th colspan="7">Pendapatan</th>
                </tr>
                <tr>
                    <th>Jumlah Pendapatan</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">Harga Pokok Penjualan</th>
                </tr>
                <tr>
                    <th>Jumlah Harga Pokok Penjualan</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>Laba Kotor</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">Beban Operasi</th>
                </tr>
                <tr>
                    <th>Jumlah Beban Operasi</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>Pendapatan Operasi</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">Pendapatan dan Beban Lain</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;&nbsp;&nbsp;Pendapatan Lain</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;Jumlah Pendapatan Lain</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;&nbsp;&nbsp;Beban Lain-lain</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;Jumlah Beban Lain-lain</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>Jumlah Pendapatan dan Beban Lain</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>LABA(RUGI) Bersih (Sebelum Pajak)</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>LABA(RUGI) Bersih (Setelah Pajak)</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr> 
            </table>
        </div>
    </div>
@endsection
