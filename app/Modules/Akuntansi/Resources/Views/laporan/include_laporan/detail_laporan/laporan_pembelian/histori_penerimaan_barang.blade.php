@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari Tanggal <b> {{ $dari_tanggal }}</b> Ke <b> {{ $ke_tanggal }}</b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>    No. Terima          <!-- Baris 1 -->
                            <br>No.Barang
                            <br>Tipe Proses
                        </th>
                        <th>    Tanggal. Terima
                            <br>Keterangan Barang
                            <br>No. Faktur
                        </th>
                        <th>    Status
                            <br>Kuantitas
                            <br>Tanggal Faktur
                        </th>
                        <th>    Nama Pemasok
                            <br>Saldo Kts.
                            <br>Kts. Faktur
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=4 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    pada tanggal {{ $dari_tanggal }}.
                                </div>
                            </td>
                        </tr>
                    @else
                    @php $totalQty = 0; @endphp
                    @foreach($item as $item_key => $dataPenerimaanPembelian)
                        <tr>
                            <td>{{ $item_key }}</td>
                            <td>{{ $dataPenerimaanPembelian[0]['recdate'] }}</td>
                            <td>{{ $dataPenerimaanPembelian[0]['status'] }}</td>
                            <td>{{ $dataPenerimaanPembelian[0]['nmpemasok'] }}</td>
                        </tr>
                        @php $subTotalQty = 0; @endphp
                            @foreach($dataPenerimaanPembelian as $dataPenerimaan)
                                <tr>
                                    <td style="padding-left : 25px;">
                                        {{ $dataPenerimaan['nobarang']          ??   null  }}
                                    </td>
                                    <td>{{ $dataPenerimaan['nmbarang']          ??   null  }}</td>
                                    <td>{{ $dataPenerimaan['qty']               ??   null  }}</td>
                                    <td>{{ $dataPenerimaan['valqty']            ??   null  }}</td>
                                </tr>
                                @if($dataPenerimaan['faktnum'] != null)
                                    <tr>
                                        <td style="padding-left : 50px;">
                                            {{ $dataPenerimaan['faktnum']         ??   null  }}
                                        </td>
                                        <td>{{ $dataPenerimaan['fakttgl']         ??   null  }}</td>
                                        <td>{{ $dataPenerimaan['qtyfakt']         ??   null }}</td>
                                    </tr>
                                @php $subTotalQty += $dataPenerimaan['qtyfakt']; @endphp
                                @else
                                <tr>
                                    <td colspan=2></td>
                                    <td>0</td>
                                    <td colspan=2></td>
                                </tr>
                            @endif
                            @endforeach
                            <tr>
                                <td colspan="2"></td>
                                <td class="border-top">{{   $subTotalQty       }}</td>
                                <td colspan=2></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>                
            </table>
        </div>
    </div>
@endsection