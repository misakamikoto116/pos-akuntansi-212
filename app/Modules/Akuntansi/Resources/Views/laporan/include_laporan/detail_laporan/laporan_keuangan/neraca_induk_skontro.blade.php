@extends('akuntansi::laporan.laporan_landscape')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col col-12" align="center">
            <p><h4>{{ $nama_perusahaan ?? null }}<h4></p>
            <p><h6 style="color: red;">{{ $title }}</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>Per Tangal {{ $tanggal ?? null }}</p>
        </div>
    </div>
    <div class="row">
        @foreach ($items->chunk(2) as $i =>  $accountGroups)
            <div class="col col-6">
                <table class="table">
                    <tr>
                        <th style="color: #0066cc;">Keterangan</th>
                        <th style="color: #0066cc;">Balance</th>
                    </tr>
                    <tr>
                        <th colspan="2">{{$i === 0 ? 'Aktiva' : 'Kewajiban & Ekuitas'}}</th>
                    </tr>   
                    @foreach ($accountGroups as $name => $accountGroup)
                        <tr>
                            <th colspan="2" style="padding-left: 25px;">{{ $name }}</th>
                        </tr>
                        @foreach ($accountGroup as $tipe_akun_parent => $data_akun)
                            @if (!empty($data_akun['child_and_parent']))
                                <tr class="akun_parent">
                                    <th colspan="2" style="padding-left: 40px;">{{ $tipe_akun_parent }}</th>
                                </tr>
                                @foreach ($data_akun['child_and_parent'] as $account)
                                    @if ($account['akun_child']->isNotEmpty())
                                        @if (array_key_exists('nama_akun_parent', $account))
                                            <tr>
                                                <th style="padding-left: 60px;">{{ $account['nama_akun_parent'] ?? null }}</th>
                                                @foreach ($account['sum_akun_parent'] as $item)
                                                    <th style="text-align: right;">{{  number_format($item)  }}</th>
                                                @endforeach
                                            </tr>
                                        @endif
                                    @endif
                                    @foreach ($account['akun_child'] as $account_child)
                                        <tr>
                                            <td style="padding-left:  70px">{{ $account_child['nama_akun_child'] }}</td>
                                            @foreach ($account_child['sum_akun_child'] as $item)
                                                <td style="text-align: right;">{{ number_format($item) }}</td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                    @if ($account['akun_child']->isEmpty())
                                      @if (array_key_exists('nama_akun_parent', $account))
                                        <tr class="akun_child">
                                            <th style="font-weight: normal; padding-left: 60px;">{{ $account['nama_akun_parent'] ?? null }}</th>
                                            @foreach ($account['sum_akun_parent'] as $item)
                                                <th style="text-align: right; font-weight: normal;">{{ number_format($item) }}</th>
                                            @endforeach
                                        </tr>
                                      @endif    
                                    @endif
                                @endforeach
                                <tr>
                                    <th style="padding-left: 40px;">Jumlah {{ $tipe_akun_parent }}</th>
                                    @foreach ($data_akun['sum_tipe_akun'] as $item)
                                        <th style="text-align: right; border-top: 1px solid;">{{ number_format($item) }}</th>
                                    @endforeach
                                </tr>
                            @else
                                @if (!empty($data_akun['akun_child']))
                                  @if ($data_akun['akun_child']->isNotEmpty())
                                    <tr>
                                        <th style="padding-left: 60px;">{{ $data_akun['nama_akun_parent'] ?? null }}</th>
                                        @foreach ($data_akun['sum_akun_parent'] as $item)
                                            <th style="text-align: right;">{{  number_format($item)  }}</th>
                                        @endforeach
                                    </tr>
                                  @endif
                                @endif
                                @if (array_key_exists('nama_akun_parent', $data_akun))
                                    <tr>
                                        <th style="padding-left: 60px;">{{ $data_akun['nama_akun_parent'] ?? null }}</th>
                                        @foreach ($data_akun['sum_akun_parent'] as $item)
                                            <th style="text-align: right;">{{  number_format($item)  }}</th>
                                        @endforeach
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                        <tr>
                            <th style="padding-left: 25px;">Jumlah {{ $name }}</th>
                            @foreach ($neraca_sum[$name] as $item)
                                <th style="text-align: right; border-top: 1px solid;">{{ number_format($item) }}</th>
                            @endforeach
                        </tr>
                    @endforeach
                    <tr>
                        <th>{{ $sum_neraca_group[$i]['title'] }}</th>
                        @foreach ($sum_neraca_group[$i]['jumlah'] as $item)
                            <th style="text-align: right; border-top: 1px solid;">{{ number_format($item) }}</th>
                        @endforeach
                    </tr>
                </table>
            </div>    
        @endforeach
    </div>    
@endsection
