@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title = 'Laporan Untung Rugi Non-Realisir'}}
@endsection
@section('laporan')
<div class="row">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr>
                    <td colspan=3>
                        <p><h4>Chichi</h4></p>
                        <p><h6>{{$title}}</h6></p>
                        <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                        <p>Per Tanggal <!-- Tanggal --></p>
                    </td>
                </tr>
                <tr>
                    <th>a</th>
                    <th>b</th>
                    <th>c</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection