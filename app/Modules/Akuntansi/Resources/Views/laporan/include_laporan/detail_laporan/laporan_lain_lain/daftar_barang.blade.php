@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
<style>
    tr th {
        font-size: 13px;
        color: #0066cc;
        text-align: center;
        text-decoration: underline;
    }
    tr td {
        font-size: 12px;
    }
    .text-kanan {
        text-align: right;
    }
</style>
@section('laporan')
    <div class="row">
        <div class="col col-12" align="center">
            <p><h5>{{ $nama_perusahaan }}<h4></p>
            <p><h4 style="color: #cc0000;">Daftar Barang</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
            <p>Per Tangal <b>
                {{--  @if(Request::get('start-date') && Request::get('end-date')) 
                    {{ date('d-m-Y', strtotime(Request::get('start-date'))) }}</b> Ke <b>{{ date('d-m-Y', strtotime(Request::get('end-date'))) }}
                @else
                    {{ "01-".date('m-Y') }}</b> Ke <b>{{ date('d-m-Y') }}
                @endif  --}}
                @if(Request::get('start-date'))
                    {{ date('d-m-Y', strtotime(Request::get('start-date'))) }}
                @else 
                    {{ date('d-m-Y') }}
                @endif
            </b></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tr>
                    <th>No. Barang</th>
                    <th>Deskripsi Barang</th>
                    <th>Kuantitas</th>
                    <th>Biaya Rata-rata</th>
                    <th>Biaya Terakhir</th>
                    <th>Harga Satuan</th>
                    <th>Tipe Barang</th>
                    <th>Tipe Persediaan</th>
                </tr>
                @foreach ($datas as $data)
                    <tr>
                        <td>{{ $data->no_barang }}</td>
                        <td>{{ $data->keterangan }}</td>
                        <td class="text-kanan">{!! $data->updated_qty !!}</td>
                        <td class="text-kanan">{{ !empty($data->saldoAwalBarang->last()->harga_modal) ? number_format($data->saldoAwalBarang->last()->harga_modal) : 0 }}</td>
                        <td class="text-kanan">{{ !empty($data->saldoAwalBarang->last()->harga_terakhir) ? number_format($data->saldoAwalBarang->last()->harga_terakhir) : 0 }}</td>
                        <td class="text-kanan">{{ !empty($data->harga_jual) ? number_format($data->harga_jual) : 0 }}</td>
                        <td>{!! $data->tipe_formatted !!}</td>
                        <td>{!! $data->tipe_persediaan_formatted !!}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection 
