@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Akun</th>
                        <th>Nama Akun</th>
                        <th>Debit (Domestik)</th>
                        <th>Kredit (Domestik)</th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=4 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php $debet = 0 ; $kredit = 0; @endphp
                    @foreach($item as $item_key => $dataBarangPembelian)
                        <tr>
                            <td>{{ $dataBarangPembelian[0]['tanggal_transaksi']   ?? null }}</td>
                            <td>{{ $dataBarangPembelian[0]['sumber']              ?? null }}</td>
                            <td>{{ $item_key                                      ?? null }}</td>
                            <td>{{ $dataBarangPembelian[0]['keterangan']          ?? null }}</td>
                        </tr>
                        @php $subDebet = 0; $subKredit = 0; @endphp
                        @foreach($dataBarangPembelian as $dataBarang)                            
                            <tr>
                                <td style="padding-left: 25px;">{{ $dataBarang['kode_akun']              ?? null }}</td>
                                <td style="padding-left: 25px;">{{ $dataBarang['nama_akun']              ?? null }}</td>
                                <td style="padding-left: 25px;">
                                    @if($dataBarang['status'] == 1)
                                        @php $subDebet += $dataBarang['nominal']; @endphp
                                        {{ number_format($dataBarang['nominal']) ?? 0 }}
                                    @else 0
                                    @endif
                                </td>
                                <td style="padding-left: 25px;">
                                    @if($dataBarang['status'] == 0)
                                        @php $subKredit += $dataBarang['nominal']; @endphp
                                        {{ number_format($dataBarang['nominal']) ?? 0 }}
                                    @else 0
                                    @endif
                                </td>
                            </tr>                            
                        @endforeach
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="border-top">{{ number_format($subDebet)  }}</td>
                            <td class="border-top">{{ number_format($subKredit) }}</td>
                        </tr>
                    @php $debet += $subDebet; $kredit += $subKredit; @endphp
                    @endforeach
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class="border-top">{{ number_format($debet)  }}</td>
                        <td class="border-top">{{ number_format($kredit) }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection