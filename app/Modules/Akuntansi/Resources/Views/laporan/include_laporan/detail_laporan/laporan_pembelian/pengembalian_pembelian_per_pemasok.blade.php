@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan='4'>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <td>No. Faktur</td>
                        <td>Tanggal Faktur</td>
                        <td>Katerangan</td>
                        <td>Jumlah</td>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=4 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php $total = 0; @endphp
                    @foreach($item as $item_key => $dataPemasok)
                        <tr>
                            <td>{{ $item_key }}</td>
                            <td>{{ $dataPemasok[0]['namapemasok'] }}</td>
                        </tr>
                        @php $subTotal = 0; @endphp
                        @foreach($dataPemasok as $data)
                            <tr>
                                <td>{{ $data['no_retur'] }}</td>
                                <td>{{ $data['tanggal_retur'] }}</td>
                                <td>{{ $data['keterangan'] }}</td>
                                <td>{{ number_format($data['total']) }}</td>
                            </tr>
                            @php $subTotal += $data['total'] @endphp
                        @endforeach
                        <tr>
                            <td colspan="3"></td>
                            <td class="border-top">{{ number_format($subTotal) }}</td>
                        </tr>
                        @php $total += $subTotal; @endphp
                    @endforeach
                    <tr>
                        <td colspan="3"></td>
                        <td class="border-top">{{ number_format($total) }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection