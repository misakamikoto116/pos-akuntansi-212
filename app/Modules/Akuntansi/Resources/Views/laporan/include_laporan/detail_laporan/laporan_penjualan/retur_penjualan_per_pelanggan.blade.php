@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Faktur</th>
                        <th>Tgl. Faktur</th>
                        <th>Keterangan</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=4 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                {{ $subTitle   }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php $total = 0; @endphp
                    @foreach($item as $pelanggan => $dataPelanggan)
                        <tr>
                            <td colspan=4>{{ $pelanggan }}</td>
                        </tr>
                        @php $subTotal = 0; @endphp
                        @foreach($dataPelanggan as $VPelangaan)
                            <tr>
                                <td>{{ $VPelangaan['nofaktur']    ??  null }}</td>
                                <td>{{ $VPelangaan['tanggal']     ??  null }}</td>
                                <td>{{ $VPelangaan['keterangan']  ??  null  }}</td>
                                <td>{{ number_format($VPelangaan['total'])  ??  null }}</td>
                            </tr>
                            @php $subTotal += $VPelangaan['total']; @endphp
                        @endforeach
                        @php $total += $subTotal; @endphp
                        <tr>
                            <td colspan='3' align="right">Total {{ $pelanggan }}</td>
                            <td class="border-top">{{ number_format($total) }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan='3' align="right">Total Keseluruhan</td>
                        <td class="border-top">{{ number_format($total) }}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection