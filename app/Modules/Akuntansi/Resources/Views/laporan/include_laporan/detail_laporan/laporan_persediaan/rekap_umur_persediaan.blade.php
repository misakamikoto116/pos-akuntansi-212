@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=9>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Per <b> {{ $dari_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Barang</th>
                        <th>Deskripsi Barang</th>
                        <th>Saldo Kts</th>
                        <th>Qty. (1-30)</th>
                        <th>Qty. (31-60)</th>
                        <th>Qty. (61-90)</th>
                        <th>Qty. (91-120)</th>
                        <th>Qty. (>121)<th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=9 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php
                        $kts1  = 0; $kts2  = 0; $kts3  = 0;
                        $kts4  = 0; $kts5  = 0; $sum   = 0;
                    @endphp
                    @foreach($item as $dataTransaksi)
                        <tr>
                            <td>{{ $dataTransaksi['kode_barang']        ?? null }}</td>
                            <td>{{ $dataTransaksi['nama_barang']        ?? null }}</td>
                            <td>{{ $dataTransaksi['kuantitas']          ?? null }}</td>
                            <td>{{ $dataTransaksi['umur_saldo']['30']   ?? null }}</td>
                            <td>{{ $dataTransaksi['umur_saldo']['60']   ?? null }}</td>
                            <td>{{ $dataTransaksi['umur_saldo']['90']   ?? null }}</td>
                            <td>{{ $dataTransaksi['umur_saldo']['120']  ?? null }}</td>
                            <td>{{ $dataTransaksi['umur_saldo']['120+'] ?? null }}</td>
                        </tr>
                        @php
                            $kts1  += $dataTransaksi['umur_saldo']['30'];  $kts2   += $dataTransaksi['umur_saldo']['60'];
                            $kts3  += $dataTransaksi['umur_saldo']['90'];  $kts4   += $dataTransaksi['umur_saldo']['120'];
                            $kts5  += $dataTransaksi['umur_saldo']['120+']; $sum   += $dataTransaksi['kuantitas'];
                        @endphp
                    @endforeach
                    <tr>
                        <td colspan=2>&nbsp;</td>
                        <td class="border-top">{{   $sum      }}</td>
                        <td class="border-top">{{   $kts1     }}</td>
                        <td class="border-top">{{   $kts2     }}</td>
                        <td class="border-top">{{   $kts3     }}</td>
                        <td class="border-top">{{   $kts4     }}</td>
                        <td class="border-top">{{   $kts5     }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection