@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title = "Laporan Ringkasan Buku Besar Pembantu Hutang"}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=8>
                            <p><h4>{{ $nama_perusahaan->nama_perusahaan ?? null }}</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir ?? null }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Nama Pemasok</th>
                        <th>Saldo Awal</th>
                        <th>Faktur Pembelian</th>
                        <th>Pembayaran Pemasok</th>
                        <th>Retur Pembelian</th>
                        <th>Uang Muka</th>
                        <th>Bukti Jurnal</th>
                        <th>Saldo</th>
                    </tr>
                </thead>
                @php
                    $grand_saldo_awal                   = 0;
                    $grand_faktur_pembelian             = 0;
                    $grand_pembayaran_pembelian         = 0;
                    $grand_retur_pembelian              = 0;
                    $grand_uang_muka                    = 0;
                    $grand_bukti_jurnal                 = 0;
                    $grand_saldo                        = 0;
                @endphp
                <!-- Perulangan Mata Uang -->
                    <tr>
                        <td colspan="9"><strong>IDR</strong></td>
                    </tr>
                        <!-- Perulangan Ringkasan Buku Besar Pembantu Hutang -->
                        @foreach ($items as $data_pemasok)
                            @php
                                $grand_saldo_awal                  += $data_pemasok->saldo_awal;
                                $grand_faktur_pembelian            += $data_pemasok->sum_faktur_pembelian;
                                $grand_pembayaran_pembelian        += $data_pemasok->sum_pembayaran_pembelian;
                                $grand_retur_pembelian             += $data_pemasok->sum_retur_pembelian;
                                $grand_uang_muka                   += $data_pemasok->sum_uang_muka;
                                $grand_saldo                       += $data_pemasok->saldo;
                            @endphp
                            <tr>
                                <tr>
                                    <td>{{ $data_pemasok->nama ?? null }}</td>
                                    <td>{{ number_format($data_pemasok->saldo_awal) }}</td>
                                    <td>{{ number_format($data_pemasok->sum_faktur_pembelian) }}</td>
                                    <td>{{ number_format($data_pemasok->sum_pembayaran_pembelian) }}</td>
                                    <td>{{ number_format($data_pemasok->sum_retur_pembelian) }}</td>
                                    <td>{{ number_format($data_pemasok->sum_uang_muka) }}</td>
                                    <td>{{ number_format($data_pemasok->sum_bukti_jurnal) }}</td>
                                    <td>{{ number_format($data_pemasok->saldo) }}</td>
                                </tr>
                            </tr>
                        @endforeach
                        <tr>
                            <td style="color: #0066cc; font-weight: bold;">Total dari IDR</td>
                            <td class="border-top" style="color: #0066cc; font-weight: bold;">
                                {{ number_format($grand_saldo_awal) }}
                            </td>
                            <td class="border-top" style="color: #0066cc; font-weight: bold;">
                                {{ number_format($grand_faktur_pembelian) }}
                            </td>
                            <td class="border-top" style="color: #0066cc; font-weight: bold;">
                                {{ number_format($grand_pembayaran_pembelian) }}
                            </td>
                            <td class="border-top" style="color: #0066cc; font-weight: bold;">
                                {{ number_format($grand_retur_pembelian) }}
                            </td>
                            <td class="border-top" style="color: #0066cc; font-weight: bold;">
                                {{ number_format($grand_uang_muka) }}
                            </td>
                            <td class="border-top" style="color: #0066cc; font-weight: bold;">
                                {{ number_format($grand_bukti_jurnal) }}
                            </td>
                            <td class="border-top" style="color: #0066cc; font-weight: bold;">
                                {{ number_format($grand_saldo) }}
                            </td>
                        </tr>
                        <!-- Akhir Perulangan Ringkasan Buku Besar Pembantu Hutang -->
                <!-- Akhir Perulangan Mata Uang -->
            </table>
        </div>
    </div>
@endsection