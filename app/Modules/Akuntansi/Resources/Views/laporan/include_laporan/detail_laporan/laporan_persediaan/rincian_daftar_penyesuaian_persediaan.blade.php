@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b> Per {{ $dari_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Barang</th>
                        <th>Keterangan Barang</th>
                        <th>Satuan</th>
                        <th>Qty. Diff. Ratio</th>
                        <th>Biaya Rata-rata</th>
                        <th>Nilai Selisih</th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=6 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @php $nilaiSelisih   =  0; @endphp
                    @foreach($item as $item_key => $dataHistoriTingkat)
                        <tr>
                            <td>{{ $dataHistoriTingkat[0]['tanggal']              ?? null }}</td>
                            <td>{{ $dataHistoriTingkat[0]['no_penyesuaian']       ?? null }}</td>
                            <td>{{ $dataHistoriTingkat[0]['tipe_akun']            ?? null }}</td>
                            <td>&nbsp;</td>
                            <td>{{ $dataHistoriTingkat[0]['keterangan']           ?? null }}</td>
                            <td>&nbsp;</td>
                        </tr>                            
                        @foreach($dataHistoriTingkat as $dataHistori)                            
                            <tr>
                                <td>{{ $dataHistori['no_barang']                        ?? null }}</td>
                                <td>{{ $dataHistori['nm_barang']                        ?? null }}</td>
                                <td>{{ $dataHistori['satuan']                           ?? null }}</td>
                                <td>{{ $dataHistori['qty_diff']                         ?? null }}</td>
                                <td>{{ number_format($dataHistori['biaya_rata'])        ?? null }}</td>
                                <td>{{ number_format($dataHistori['nilai_selisih'])     ?? null }}</td>
                            </tr>                            
                            @php
                                $nilaiSelisih   +=  $dataHistori['nilai_selisih'];
                            @endphp
                        @endforeach
                        <tr>
                            <td colspan=5>&nbsp;</td>
                            <td class="border-top">{{ number_format($nilaiSelisih)  }}</td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
    </div>
@endsection