@extends('akuntansi::laporan.laporan')
@section('title')
{{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4>{{ $nama_perusahaan }}</h4></p>
                            <p><h6>{{ $title }}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir }} <!-- Tanggal --></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Akun</th>
                        <th>Nama Akun</th>
                        <th>Catatan</th>
                        <th>Nilai (Asing)</th>
                        <th>Nama Dep</th>
                        <th>Nama Proyek</th>
                    </tr>
                </thead>
                <!-- Perulangan -->
                @foreach ($items as $data_penerimaan)
                    
                    @php
                        $sum_penerimaan = 0;
                    @endphp
                    
                    <tr>
                        <td><strong>{{ Carbon\Carbon::parse($data_penerimaan->tanggal)->format('d F Y') ?? null }} &ndash; {{ $data_penerimaan->no_faktur ?? null }}</strong></td>
                        <td><strong>{{ $data_penerimaan->akun->nama_akun ?? null }}</strong></td>
                        <td><strong>{{ $data_penerimaan->keterangan ?? null }}</strong></td> <!--  -->
                        <td><strong>{{ $data_penerimaan->akun->mataUang->kode ?? null }}</strong></td> <!--  -->
                        <td></td>
                        <td></td> <!--  -->
                    </tr>
                    <!-- Ulang -->
                    @foreach ($data_penerimaan->detailBukuMasuk as $detail_penerimaan)
                        @php
                            $sum_penerimaan += $detail_penerimaan->nominal;
                        @endphp

                        <tr>
                            <td style="padding-left: 25px;">{{ $detail_penerimaan->akun->kode_akun ?? null }}</td>
                            <td style="padding-left: 25px;">{{ $detail_penerimaan->akun->nama_akun ?? null }}</td>
                            <td style="padding-left: 25px;">{{ $detail_penerimaan->catatan ?? null }}</td>
                            <td style="padding-left: 25px; text-align: right;">{{ number_format($detail_penerimaan->nominal) }}</td>
                            <td style="padding-left: 25px;"></td>
                            <td style="padding-left: 25px;"></td> <!--  -->
                        </tr>
                    @endforeach
                    <!-- Akhir Ulang -->
                    <tr>
                        <td style="padding-left: 25px; color: blue;">Total dari {{ $data_penerimaan->no_faktur ?? null }}</td>
                        <td style="padding-left: 25px;" colspan="2"></td>
                        <td style="padding-left: 25px; text-align: right; color: blue;" class="border-top">{{ number_format($sum_penerimaan) }}</td>
                        <td style="padding-left: 25px;" colspan="2"></td>
                    </tr>
                @endforeach
                <!-- Akhir Perulangan  -->

            </table>
        </div>
    </div>
@endsection