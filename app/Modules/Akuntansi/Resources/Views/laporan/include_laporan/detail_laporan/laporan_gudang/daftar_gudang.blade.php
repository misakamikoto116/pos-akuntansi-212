@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Nama</th>
                        <th>Keterangan</th>
                        <th>Penanggung Jawab</th>
                        <th>ALamat</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=4 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia.
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($item as $item_key => $dataGudang)
                            <tr>
                                <td>{{ $dataGudang['nama'] }}</td>
                                <td>{{ $dataGudang['keterangan'] }}</td>
                                <td>{{ $dataGudang['pj'] }}</td>
                                <td colspan=2>{{ $dataGudang['alamat'] }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>                
            </table>
        </div>
    </div>
@endsection