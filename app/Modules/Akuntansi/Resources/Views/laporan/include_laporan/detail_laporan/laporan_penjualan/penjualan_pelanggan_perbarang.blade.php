@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=2>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>Nama Pelanggan</th>
                        <th>Nama Produk</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=2 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    {{ $subTitle   }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php $Total = 0; @endphp
                        @foreach($item as $item_key => $dataPelanggan)
                            <tr>
                                <td colspan=2><b>{{  $item_key   }}</b></td>
                            </tr>
                            @php $subTotal = 0; @endphp
                            @foreach($dataPelanggan as $data)
                                <tr>
                                    <td style="padding-left:25px">
                                        {{  $data['nama_produk']   }}
                                    </td>
                                    <td>{{  number_format($data['harga_terakhir'])   }}</td>
                                </tr>
                                @php $subTotal += $data['harga_terakhir'] @endphp
                            @endforeach
                            <tr>
                                <td>&nbsp;</td>
                                <td class="border-top">{{  number_format($subTotal)   }}</td>
                            </tr>
                            @php $Total += $subTotal; @endphp
                        @endforeach
                        <tr>
                            <td>&nbsp;</td>
                            <td class="border-top">{{  number_format($Total)   }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection