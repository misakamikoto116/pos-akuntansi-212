<div class="form-group">
    <div class="checkbox checkbox-primary mb-2">
        <input name="tampil_total" class="form-check-input"  id="tampil_total" type="checkbox">
        <label for="tampil_total">
            Tampilkan Total
        </label>
    </div>
</div>
<div class="form-group">
    <div class="checkbox checkbox-primary mb-2">
        <input name="tampil_induk" class="form-check-input" id="tampil_induk" checked="" type="checkbox">
        <label for="tampil_induk">
            Tampilkan Induk
        </label>
    </div>
</div>
<div class="form-group">
    <div class="checkbox checkbox-primary mb-2">
        <input id="tampil_anak" class="form-check-input" name="tampil_anak" checked="" type="checkbox">
        <label for="tampil_anak">
            Tampilkan Anak
        </label>
    </div>
</div>
<div class="form-group">
    <div class="checkbox checkbox-primary mb-2">
        <input name="saldo_nol" class="form-check-input" id="saldo_nol" checked type="checkbox">
        <label for="saldo_nol">
            Termasuk Saldo nol
        </label>
    </div>
</div>
<div class="form-group">
    <div class="checkbox checkbox-primary mb-2">
        <input name="tampil_jumlah_induk" class="form-check-input" id="tampil_jumlah_induk" checked="" type="checkbox">
        <label for="tampil_jumlah_induk">
            Tampilkan Jumlah Induk
        </label>
    </div>
</div>
<div class="form-group">
    <div class="checkbox checkbox-primary mb-2">
        <input name="geser_kanan" class="form-check-input" id="geser_kanan" type="checkbox">
        <label for="geser_kanan">
            Geser Kekanan jumlah nilai
        </label>
    </div>
</div>