@extends('akuntansi::laporan.laporan')
@section('title')
Laporan Ringkasan Proyeksi Arus Kas
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="3">
                            <p><h4>CHICHI</h4></p>
                            <p><h6>Ringkasan Proyeksi Arus Kas</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Per Tanggal 30 Mei 2018 <!-- Tanggal --></p>
                        </td>
                    </tr>
                </thead>
                <tr>
                    <td colspan="3">Proyeksi Pembayaran Kas</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Projected Cash Payment from Account Payable IDR</td>
                    <td>IDR 241.204.540</td>
                    <td>IDR 241.204.540</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Projected Cash Payment from Account Payable SGD</td>
                    <td>SGD 4.300 + IDR 0</td>
                    <td>IDR 29.240.000</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Projected Cash Payment from Account Payable USD</td>
                    <td>USD 700 + IDR 0</td>
                    <td>IDR 6.510.000</td>
                </tr>
                <tr>
                    <td colspan="2">Total dari Proyeksi Pembayaran</td>
                    <td class="border-top">276.954.450</td>
                </tr>
                <tr>
                    <td colspan="2">Arus Kas yang Diterima</td>
                    <td class="border-top">-276.954.450</td>
                </tr>
            </table>
        </div>
    </div>
@endsection