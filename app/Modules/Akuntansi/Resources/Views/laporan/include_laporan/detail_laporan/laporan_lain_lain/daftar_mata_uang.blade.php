@extends('akuntansi::laporan.laporan')
<style>
    tr th {
        font-size: 13px;
        color: #0066cc;
        text-align: center;
        text-decoration: underline;
    }
    tr td {
        font-size: 12px;
    }
    .text-kanan {
        text-align: right;
    }
</style>
@section('laporan')
    <div class="row">
        <div class="col col-12" align="center">
            <p><h5>{{ $nama_perusahaan }}<h4></p>
            <p><h4 style="color: #cc0000;">{{ $title }}</h6></p>
            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tr>
                    <th>Nama</th>
                    <th>Exchange Rate</th>
                    <th>Country Name</th>
                    <th>Simbol</th>
                </tr>
                @foreach ($items as $item)
                    <tr>
                        <td>{{ $item->nama ?? null }}</td>
                        <td>{{ $item->nilai_tukar ?? null }}</td>
                        <td>{{ $item->negara ?? null }}</td>
                        <td>{{ $item->kode_simbol ?? null }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection 
