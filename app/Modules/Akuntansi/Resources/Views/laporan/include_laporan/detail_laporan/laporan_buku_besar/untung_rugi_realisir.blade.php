@extends('akuntansi::laporan.laporan')
@section('title')
    Laporan Untung/Rugi Realisir
@endsection
@section('laporan')
    <div class="row">
        <div class="col col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="3">
                            <p><h4>Chichi</h4></p>
                            <p><h6>Untung/Rugi Realisir</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Per</p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Akun</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection