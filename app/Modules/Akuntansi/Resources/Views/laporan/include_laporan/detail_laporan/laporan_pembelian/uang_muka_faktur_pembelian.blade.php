@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=4>
                        <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Per <b> {{ $dari_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>DP Terpakai</th>
                        <th>Tanggal DP Terpakai</th>
                        <th>Keterangan DP Terpakai</th>
                        <th>Nilai DP Terpakai</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=4 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    pada tanggal {{ $dari_tanggal }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php $total = 0; @endphp
                        @foreach($item as $item_key => $dataUangMukaPenjualan)
                            @php $subTotal = 0; @endphp
                            <tr>    
                                <td>{{ $dataUangMukaPenjualan[0]['nopo'] }}</td>
                                <td>{{ date('d-m-Y', strtotime($dataUangMukaPenjualan[0]['tglpo'])) }}</td>
                                <td colspan=2></td>
                            </tr>
                            <tr>
                                <td>{{ $item_key }}</td>
                                <td>{{ date('d-m-Y', strtotime($dataUangMukaPenjualan[0]['tglfak'])) }}</td>
                                <td>{{ $dataUangMukaPenjualan[0]['keterangan'] }}</td>
                                <td>{{ number_format($dataUangMukaPenjualan[0]['total']) }}</td>
                            </tr>
                            @php $subTotal += $dataUangMukaPenjualan[0]['total']; @endphp
                            <tr>
                                <td colspan=3></td>
                                <td class="border-top">{{ number_format($subTotal) }}</td>
                            </tr>
                            @php $total += $subTotal; @endphp
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection