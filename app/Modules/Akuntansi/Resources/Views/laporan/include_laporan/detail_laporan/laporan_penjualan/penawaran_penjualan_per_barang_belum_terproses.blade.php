@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Pengajuan</th>
                        <th>Tgl Penawaran</th>
                        <th>Kuantitas</th>
                        <th>Kuantitas Diterima</th>
                        <th>Satuan</th>
                        <th>Status Barang</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=6 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    {{ $subTitle   }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php $qty = 0; $qtyTerima = 0; @endphp
                        @foreach($item as $item_key => $dataPenawaranPenjualan)
                            <tr>
                                <td colspan="6">{{ $item_key }}</td>
                            </tr>
                                @php $subQty = 0; $subQtyTerima = 0; @endphp
                                @foreach($dataPenawaranPenjualan as $dataPenawaran)
                                <tr>    
                                    <td>{{ $dataPenawaran['nopenawaran']    ??  null   }}</td>
                                    <td>{{ $dataPenawaran['tanggal']        ??  null   }}</td>
                                    <td>{{ $dataPenawaran['qty']            ??  null   }}</td>
                                    <td>{{ $dataPenawaran['qtyterima']      ??  null   }}</td>
                                    <td>{{ $dataPenawaran['satuan']         ??  null   }}</td>
                                    <td>@if($dataPenawaran['status'] == 0) Sedang Proses @endif</td>
                                </tr>
                                @php $subQty += $dataPenawaran['qty']; $subQtyTerima += $dataPenawaran['qtyterima']; @endphp
                                @endforeach
                            <tr>
                                <td colspan="2"></td>
                                <td class="border-top">{{ $subQty }}</td>
                                <td class="border-top">{{ $subQtyTerima }}</td>
                                <td cplspan="2"></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection