@extends('akuntansi::laporan.laporan')
@section('title')
{{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4>{{ $nama_perusahaan }}</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir ?? null }} <!-- Tanggal --></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal Transaksi</th>
                        <th>No. Bukti</th>
                        <th>No. Cek</th>
                        <th style="text-align: center;">Keterangan</th>
                        <th style="text-align: center;">Jumlah</th>
                        <th style="text-align: center;">Nilai Tukar</th>
                    </tr>
                </thead>
                <!-- Perulangan -->
                @foreach ($items as $akun_bank)
                    
                    @php
                        $sum_pembayaran = 0;
                    @endphp

                    <tr>
                        <td colspan="6"><strong>{{ $akun_bank->nama_akun ?? null }}</strong></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 25px;"><strong>{{ $akun_bank->mataUang->kode ?? null }}</strong></td>
                    </tr>
                        @foreach ($akun_bank->pembayaran as $data_pembayaran)

                            @php
                                $sum_pembayaran += $data_pembayaran->nominal;
                            @endphp

                            <tr>
                                <td style="padding-left: 35px;">{{ Carbon\Carbon::parse($data_pembayaran->tanggal)->format('d F Y') ?? null }}</td>
                                <td style="padding-left: 35px;">{{ $data_pembayaran->no_faktur ?? null }}</td>
                                <td style="padding-left: 35px;">{{ $data_pembayaran->no_cek ?? null }}</td> <!--  -->
                                <td style="padding-left: 35px;">{{ $data_pembayaran->keterangan ?? null }}</td>
                                <td style="padding-left: 35px; text-align: right;">{{ number_format($data_pembayaran->nominal) }}</td>
                                <td style="padding-left: 35px; text-align: right;">{{ $akun_bank->mataUang->nilai_tukar ?? null }}</td> <!--  -->
                            </tr>
                        @endforeach
                        <tr>
                            <td style="padding-left: 25px; color: blue;">Total dari {{ $akun_bank->mataUang->kode ?? null }}</td>
                            <td colspan="3"></td>
                            <td class="border-top" style="text-align: right; color: blue;">{{ number_format($sum_pembayaran) }}</td>
                            <td></td>
                        </tr>
                @endforeach
                <!-- Akhir Perulangan  -->
            </table>
        </div>
    </div>
@endsection