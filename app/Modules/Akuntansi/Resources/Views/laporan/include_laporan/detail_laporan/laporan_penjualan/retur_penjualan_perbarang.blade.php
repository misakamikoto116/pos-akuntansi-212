@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                            <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Faktur</th>
                        <th>Tgl. Faktur</th>
                        <th>Keterangan</th>
                        <th>Kuantitas</th>
                        <th>Unit 1 Barang</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=6 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    {{ $subTitle   }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php $total = 0; $qty = 0; @endphp
                        @foreach($item as $produk_key => $produk)
                            <tr>
                                <td colspan=6>{{ $produk_key }}</td>
                            </tr>
                            @php $subTotal = 0; $subQty = 0; @endphp
                            @foreach($produk as $dataProduk)
                                <tr>
                                    <td>{{ $dataProduk['nofaktur']      ??   null }}</td>
                                    <td>{{ $dataProduk['tglfaktur']     ??   null }}</td>
                                    <td>{{ $dataProduk['keterangan']    ??   null }}</td>
                                    <td style="color:red;"> - {{ $dataProduk['kuantitas']  ??  null }}</td>
                                    <td>{{ $dataProduk['satuan']        ??   null }}</td>
                                    <td style="color:red;"> - {{ number_format($dataProduk['total'])  ??  null }}</td>
                                </tr>
                                @php $subTotal += $dataProduk['total']; $subQty += $dataProduk['kuantitas'];@endphp
                            @endforeach
                            <tr>
                                <td colspan=3></td>
                                <td class="border-top" style="color:red;"> - {{ number_format($subQty) }}</td>
                                <td></td>
                                <td class="border-top" style="color:red;"> - {{ number_format($subTotal) }}</td>
                            </tr>
                            @php $total += $subTotal; $qty += $subQty; @endphp
                        @endforeach
                        <tr>
                            <td colspan=3 align="right">Total Kuantitas</td>
                            <td class="border-top" style="color:red;"> - {{ number_format($qty) }}</td>
                            <td align="right">Total Jumlah</td>
                            <td class="border-top" style="color:red;"> - {{ number_format($total) }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection