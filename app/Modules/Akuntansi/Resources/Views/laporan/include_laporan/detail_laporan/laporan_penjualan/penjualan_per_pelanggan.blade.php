@extends('akuntansi::laporan.laporan')
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=2>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title      }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p><b>  {{ $subTitle   }} </b> </p>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-center">Nama Pelanggan</th>
                        <th class="text-center">{{ date('d F Y',strtotime($dari_tanggal)).' - '. date('d F Y',strtotime($ke_tanggal)) }}</th> <!-- -->
                    </tr>
                </thead>
                <tbody class="post-all">
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=4 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    {{ $subTitle   }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        <tbody>
                            @foreach ($item as $data)
                                <tr class="post-original">
                                    <td> {{ $data['nama'] }} </td>
                                    <td class="text-right"> {{ number_format($data['total'], 0) }} </td>
                                </tr>
                            @endforeach
                            <tr class="post-data">
                                <td>Total</td>
                                <td class="border-top text-right sumDataTotal">{{ number_format($data['sumTotal']) }}</td>
                            </tr>
                        </tbody>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function(){
            var
                page = 1,
                start = "{{ $dari_tanggal }}",
                end   = "{{ $ke_tanggal }}"
            ;

            (
                function(){
                    $('.ti-save').hide();
                    let tipe = 'generatePDF';
                    $.ajax({
                        url: "{{ route( 'akuntansi.get-laporan-penjualan-per_pelanggan' ) }}",
                        data: {
                            startDate : start,
                            endDate : end,
                            tipe : tipe,
                        },
                        dataType: 'JSON',
                        type: 'GET',
                        beforeSend: function( XMLHttpRequest ){
                            $('.save-page').append('<div id="ajax-loading"><i class="loading-btn fa fa-circle-o-notch fa-spin"></i> Loading </div>');
                        }
                    })
                    .done( function ( data ) {
                        if ( data.code == 200 ) {
                            $('#ajax-loading').hide();
                            $('.ti-save').show();
                            $('.save-page').prop('disabled', false)
                        } else {
                        }
                    })
                    .fail( function () {
                        
                    });
                }
            )();
            
            $(window).scroll(function() {
                if($(window).scrollTop() + $(window).height() >= $(document).height()) {
                    page++;
                    loadMoreData(page);
                }
            });

            function loadMoreData(page){
                if (page > {{ $item->lastPage() }}) {
                    page = {{ $item->lastPage() }};
                    // Do Nothing . . .
                }else{
                    var
                        save  = 0,
                        tipe  = 'scroll'
                    ;

                    $.ajax({
                        url : "{{ route('akuntansi.get-laporan-penjualan-per_pelanggan') }}",
                        data : {
                            page        : page,
                            startDate   : start,
                            endDate     : end,
                            save        : save,
                            tipe        : tipe,
                        },
                        type: "get",
                        dataType : 'JSON',
                        beforeSend: function(){
                            $('.ajax-load').show();
                        }
                    })
                    .done(function(data){
                        if(data == ""){
                            $('.ajax-load').html("Semua Data Telah Berhasil di Tampilkan");
                            return;
                        }
                        $('.ajax-load').hide();
                        $.map(data, function(item, index) {
                            $.map(item, function(itemData, index) {
                                $(".post-data").before("<tr class='post-original'>"+
                                                            "<td>"+itemData['nama']+"</td>"+
                                                            "<td class='totalLoad text-right'>"+itemData['total']+"</td>"+
                                                        "</tr>");
                                $('.totalLoad').autoNumeric('init', {vMin: '0', vMax: '999999999' })
                            });
                            var htmlSumTotal    =   parseInt($('.sumDataTotal').html().replace(/,/g,""));
                            var sumTotal        =   htmlSumTotal + item[item.length -1]['sumTotal'];
                            $(".post-data").html("<td>Total</td>"+
                                                "<td class='border-top sumDataTotal text-right'>"+ sumTotal +"</td>");
                            $('.sumDataTotal').autoNumeric('init', {vMin: '0', vMax: '999999999' })
                        });
                    })
                    .fail(function(jqXHR, ajaxOptions, thrownError){
                        alert('Server tidak Merespon');
                        page--;
                    });
                }
            }

            $('.print-page').click(function(e){
                var
                    allPage = {{ $item->lastPage() }},
                    tipe    = 'print';
                $.ajax({
                    url : "{{ route('akuntansi.get-laporan-penjualan-per_pelanggan') }}",
                    data : {
                        startDate   : start,
                        endDate     : end,
                        tipe        : tipe,
                    },
                    dataType : 'JSON',
                    type: "get",
                    timeout: 6000,
                    beforeSend: function(){
                        hidePage();
                    }
                })
                .done(function(data){
                    showPage();
                    $('.post-original').remove();
                    $('.post-data').remove();
                    if(data == ""){
                        $('.ajax-load').html("No more records found");
                        return;
                    }
                    $('.ajax-load').hide();
                    $.map(data, function(item, index) {
                        $(".post-all").append("<tr class='post-original'>"+
                                                    "<td>"+item['nama']+"</td>"+
                                                    "<td class='totalLoad text-right'>"+item['total']+"</td>"+
                                                "</tr>");
                        $('.totalLoad').autoNumeric('init', {vMin: '0', vMax: '999999999' })
                    });
                    $(".post-all").after("<tr class='post-data'>"+
                                            "<td>Total</td>"+
                                            "<td class='border-top sumDataTotal text-right'>"+ data[data.length - 1]['sumTotal'] +"</td>"+
                                            "</tr>");
                    $('.sumDataTotal').autoNumeric('init', {vMin: '0', vMax: '999999999' })
                    page = allPage;
                    window.print();
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    alert('Server Gagal Merespon');
                    showPage();
                });
            });

            $(".save-page").click(function (e) {
                e.preventDefault();
                let
                    tipe  = 'download'
                ;
                $.ajax({
                    url : "{{ route('akuntansi.get-laporan-penjualan-per_pelanggan') }}",
                    data : {
                        startDate   : start,
                        endDate     : end,
                        tipe        : tipe,
                    },
                    dataType : 'JSON',
                    type: "get",
                    timeout: 6000,
                }).done(function(data){
                    var link = document.createElement("a");
                    link.download = data.title;
                    link.href = data.path;
                    document.body.appendChild(link);
                    if (link.click()) {
                        document.body.removeChild(link);
                        delete link;
                    }
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    alert('Server Gagal Merespon');
                    page--;
                });
            });
        });
    </script>
@endsection