@extends('akuntansi::laporan.laporan')
@section('title')
    {{$title = 'Laporan Rincian Buku Besar Pembantu Piutang'}}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=8>
                            <p><h4>{{ $nama_perusahaan->nama_perusahaan ?? null }}</h4></p>
                            <p><h6>{{$title}}</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Dari {{ $tanggal_awal ?? null }} ke {{ $tanggal_akhir ?? null }}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>Sumber</th>
                        <th>No. Sumber</th>
                        <th>Keterangan</th>
                        <th>Nilai (Asing)</th>
                        <th>Nilai Pajak</th>
                        <th>Saldo (Asing)</th>
                        <th>Saldo Pajak</th>
                    </tr>
                </thead>
                <!-- Perulangan Tipe Mata Uang-->
                    <tr>
                        <td colspan="7"><strong>IDR</strong></td>
                    </tr>
                    @foreach ($items as $data_pelanggan)
                        <tr>
                            <td><strong>{{ $data_pelanggan->no_pelanggan ?? null }}</strong></td> <!-- Akun Piutang -->
                            <td colspan="5"><strong>{{ $data_pelanggan->nama ?? null }}</strong></td> <!-- Nama Pelanggan -->
                            <td><strong>{{ number_format($data_pelanggan->detailInformasiPelanggan->sum('saldo_awal')) }}</strong></td> <!-- Nama Pelanggan -->
                        </tr>

                        @php
                            $saldo_asing        = $data_pelanggan->detailInformasiPelanggan->sum('saldo_awal');
                            $sum_penambahan     = 0;
                            $sum_penurunan      = 0;
                        @endphp

                        @foreach ($data_pelanggan['data'] as $data_piutang)
                            @php
                                $saldo_asing            = $saldo_asing + $data_piutang['nilai_sumber'];
                                if ($data_piutang['nilai_sumber'] < 0) {
                                    $sum_penurunan       += $data_piutang['nilai_sumber'];
                                }else {
                                    $sum_penambahan       += $data_piutang['nilai_sumber'];
                                }
                            @endphp
                            <tr>
                                <td>{{ Carbon\Carbon::parse($data_piutang['tanggal'])->format('d F Y') ?? null }}</td>
                                <td>{{ $data_piutang['sumber'] ?? null }}</td>
                                <td>{{ $data_piutang['no_sumber'] ?? null }}</td>
                                <td>{{ $data_piutang['keterangan_sumber'] ?? null }}</td>
                                @if ($data_piutang['nilai_sumber'] < 0)
                                    <td style="color: red;">{{ number_format($data_piutang['nilai_sumber']) ?? null }}</td>
                                @else
                                    <td>{{ number_format($data_piutang['nilai_sumber']) ?? null }}</td>
                                @endif
                                <td>{{ $data_piutang['nilai_pajak'] ?? null }}</td>
                                @if ($saldo_asing < 0)
                                    <td style="color: red;">{{ number_format($saldo_asing) ?? null }}</td>
                                @else
                                    <td>{{ number_format($saldo_asing) ?? null }}</td>
                                @endif
                                <td>{{ $data_piutang['saldo_pajak'] ?? null }}</td>
                            </tr>
                        @endforeach

                    @php
                        $perubahan_bersih = $sum_penambahan + $sum_penurunan;
                    @endphp
                    <tr>
                        <td colspan="4" style="color: #0066cc; font-weight: bold;">Total Penambahan</td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">{{ number_format($sum_penambahan) ?? null }}</td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">0</td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="4" style="color: #0066cc; font-weight: bold;">Total Penurunan</td>
                        <td style="color: #0066cc; font-weight: bold;">{{ number_format($sum_penurunan) ?? null }}</td>
                        <td style="color: #0066cc; font-weight: bold;">0</td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="4" style="color: #0066cc; font-weight: bold;">Perubahan Bersih</td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">{{ number_format($perubahan_bersih) ?? null }}</td>
                        <td class="border-top" style="color: #0066cc; font-weight: bold;">0</td>
                        <td colspan="2"></td>
                    </tr>
                    @endforeach
                <!-- Akhir Perulangan -->
            </table>
        </div>
    </div>
@endsection