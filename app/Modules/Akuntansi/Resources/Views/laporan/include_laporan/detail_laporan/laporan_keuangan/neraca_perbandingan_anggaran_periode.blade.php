@extends('akuntansi::laporan.laporan_landscape')
@section('title')
    Neraca Perbandingan Anggaran Periode
@endsection
@section('laporan')
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="7">
                            <p><h4>CHICHI<h4></p>
                            <p><h6>Neraca (Perbandingan Anggaran Periode)</h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p>Periode <!-- dari Bulan --> ke <!-- ke Bulan --></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Keterangan</th>
                        <th>Bulan 1 Aktual</th>
                        <th>Bulan 1 Anggaran</th>
                        <th>Bulan 2 Aktual</th>
                        <th>Bulan 2 Anggaran</th>
                        <th>Bulan 3 Aktual</th>
                        <th>Bulan 3 Anggaran</th>
                    </tr>
                </thead>
                <tr>
                    <th colspan="7">Aktiva</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;Aktiva Lancar</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;Kas dan Bank</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;&nbsp;&nbsp;Kas</th>
                </tr>
                <!-- Perulangan Akun Kas -->
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Contoh (Kas IDR)</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <!--  -->
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;Bank</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <!-- Perulangan Akun Bank -->
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Contoh (Mandiri IDR)</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <!--  -->
                <tr>
                    <th>&nbsp;&nbsp;Jumlah Kas dan Bank</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;&nbsp;Piutang Dagang</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;Uang Muka Pembelian</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Uang Muka Pembelian IDR</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;Jumlah Piutang Dagang</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;&nbsp;Persediaan</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;Persediaan Barang Dagang</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Persedian Barang Bangunan</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Persedian Barang Perkakas</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Persedian Barang Elektronik</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Persedian Barang Furniture</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Persediaan Dalam Proses Manufaktur</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;Jumlah Persediaan</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;&nbsp;Aktiva Lancar Lainnya</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;Biaya Dibayar Dimuka</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Sewa Dibayar Dimuka</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Asuransi Bayar Dimuka</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;PPN Masukan</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Proyek Dalam Proses</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;Jumlah Aktiva Lancar Lainnya</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>&nbsp;Jumlah Aktiva Lancar</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">Aktiva Tetap</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;Nilai Histori</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;&nbsp;Aktiva Tetap</th><tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Tanah</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Bangunan</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Peralatan Kantor</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Peralatan Toko</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Kendaraan</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <th>&nbsp;Jumlah Nilai Histori</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;Akumulasi Penyusutan</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;&nbsp;Akumulasi Penyusutan</th>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Akum. Penys. Peralatan Kantor</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Akum. Penys. Peralatan Toko</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Akum. Penys. Kendaraan</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <th>&nbsp;Jumlah Akumulasi Penyusutan</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>&nbsp;Jumlah Aktiva Tetap</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>&nbsp;Other Assets</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>&nbsp;Jumlah Other Assets</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>Jumlah Aktiva</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">Kewajiban dan Ekuitas</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;Kewajiban</th>
                </tr>
                <tr>
                    <td colspan="7">&nbsp;&nbsp;Kewajiban Lancar</td>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;&nbsp;&nbsp;Hutang Dagang</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;Hutang Usaha</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hutang Usaha IDR</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hutang Usaha USD</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;Jumlah Hutang Dagang</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;&nbsp;&nbsp;Kewajiban Lancar Lain</th>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;PPn Keluaran</td>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;&nbsp;&nbsp;&nbsp;Hutang Biaya</th>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hutang Bunga</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hutang Gaji</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hutang Sewa Alat Proyek</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hutang Biaya Proyek Lain-lain</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hutang Gaji/Upah Karyawan Proyek</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;Jumlah Kewajiban Lancar Lain </th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;Jumlah Kewajiban Lancar</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;&nbsp;Kewajiban Jangka Panjang</th>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;Hutang Jangka Panjang</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <th>&nbsp;&nbsp;Jumlah Kewajiban Jangka Panjang</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>&nbsp;Jumlah Kewajiban</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th colspan="7">&nbsp;Ekuitas</th>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Modal</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Deviden</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;Laba Ditahan</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                    <td>Rp. 0,00</td>
                </tr>
                <tr>
                    <th>&nbsp;Jumlah Ekuitas</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
                <tr>
                    <th>Jumlah Kewajiban dan Ekuitas</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                    <th>Rp. 0,00</th>
                </tr>
            </table>
        </div>
    </div>
@endsection