@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=6>
                        <p><h4> {{ $perusahaan ?? null }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p><b>Nominal Dalam Rupiah (Rp)</b></p>
                            <p> Per <b> {{ $dari_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Permintaan</th>
                        <th>Tanggal Permintaan</th>
                        <th>Kuantitas</th>
                        <th>Kuantitas Dipesan</th>
                        <th>Satuan</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan=6 align="center">
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    pada tanggal {{ $dari_tanggal }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @php $qty = 0; @endphp
                        @foreach($item as $item_key => $dataPermintaanPembelian)
                            <tr>    
                                <td>{{ $item_key }}</td>
                                <td colspan=4>{{ $dataPermintaanPembelian[0]['nmbarang'] }}</td>
                            </tr>
                            @php $subQty = 0; @endphp
                            @foreach($dataPermintaanPembelian as $dataPermintaan)
                                <tr>
                                    <td>{{ $dataPermintaan['reqnum']          ??  null }}</td>
                                    <td>{{ $dataPermintaan['tanggal']         ??  null }}</td>
                                    <td>{{ $dataPermintaan['qty']             ??  null }}</td>
                                    <td>{{ $dataPermintaan['qtydipesan']      ??  null }}</td>
                                    <td>{{ $dataPermintaan['unit']            ??  null }}</td>
                                    <td>@if($dataPermintaan['status'] == 0) Sedang diproses @elseif($dataPermintaan['status'] == 1) Sedang diproses @endif</td>
                                </tr>
                                @php $subQty += $dataPermintaan['qty'];  @endphp
                            @endforeach
                            <tr>
                                <td colspan="3"></td>
                                <td class="border-top">{{ $subQty }}</td>
                                <td cplspan="2"></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection