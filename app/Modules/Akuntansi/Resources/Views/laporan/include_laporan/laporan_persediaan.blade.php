<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Persediaan</h1>
        <ul class="list-group">
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinpersediaan"><i class="fa fa-tags"></i> Rincian Persediaan</a></li>
            <div id="rinpersediaan" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/persediaan/ringkasan-valuasi-persediaan') }}" data-toggle="modal" data-target="#ringkasan_valuasi_persediaan">Ringkasan Valuasi Persediaan</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/persediaan/rincian-valuasi-persediaan') }}" data-toggle="modal" data-target="#rincian_valuasi_persediaan">Rincian Valuasi Persediaan</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#ringkasan_analisis_persediaan">Ringkasan Analisis Persediaan</a></li>
                    <li class="list-group-item"><a href="#">Rincian Analisis Persediaan</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rekap_umur_persediaan">Rekap Umur Persediaan</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_umur_persediaan">Rincian Umur Persediaan</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/persediaan/kartu-stok-persediaan')}}" data-toggle="modal" data-target="#kartu_stok_persediaan">Kartu Stok Persediaan</a></li>
                    <li class="list-group-item"><a href="#">Kartu Stok Persediaan (Kts Kontrol)</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/persediaan/jurnal-persediaan') }}" data-toggle="modal" data-target="#jurnal_persediaan">Jurnal Persediaan</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#histori_barang">Histori Barang</a></li>
                    <li class="list-group-item"><a href="#">Barang Untuk Pesan Ulang</a></li>
                </ul>
            </div>
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinhistoripersediaan"><i class="fa fa-tags"></i> Rincian Histori Persediaan</a></li>
            <div id="rinhistoripersediaan" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{url('akuntansi/laporan/persediaan/histori-penyesuaian-harga-jual-barang')}}" target="_blank">Histori Penyesuaian Harga Jual Barang</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/persediaan/rincian-daftar-penyesuaian-persediaan') }}" data-toggle="modal" data-target="#rincian_daftar_penyesuaian_persediaan">Rincian Daftar Penyesuaian Persediaan</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/persediaan/group-barang') }}" target="__blank">Group Barang</a></li>
                </ul>
            </div>
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinproseslain"><i class="fa fa-tags"></i> Rincian Proses Klaim</a></li>
            <div id="rinproseslain" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#">Daftar Komponen pada Proses Klaim</a></li>
                </ul>
            </div>
        </ul>
    </div>
</section>

{{-- Modal Jurnal Persediaan --}}
<div id="jurnal_persediaan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Jurnal Persediaan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/persediaan/jurnal-persediaan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Ringkasan Valuasi Persediaan --}}
<div id="ringkasan_valuasi_persediaan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Valuasi Persediaan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/persediaan/ringkasan-valuasi-persediaan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Valuasi Persediaan --}}
<div id="rincian_valuasi_persediaan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Valuasi Persediaan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/persediaan/rincian-valuasi-persediaan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Kartu Stok Persediaan --}}
<div id="kartu_stok_persediaan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Kartu Stok Persediaan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/persediaan/kartu-stok-persediaan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Daftar Penyesuaian Persediaan --}}
<div id="rincian_daftar_penyesuaian_persediaan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Daftar Penyesuaian Persediaan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/persediaan/rincian-daftar-penyesuaian-persediaan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rekap Umur Persediaan --}}
<div id="rekap_umur_persediaan" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rekap Umur Persediaan</h4>
        </div>
            {!! Form::open(['url' => url('akuntansi/laporan/persediaan/rekap-umur-persediaan'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                    </div>
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>

{{-- Modal Rincian Umur Persediaan --}}
<div id="rincian_umur_persediaan" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Umur Persediaan</h4>
        </div>
        {!! Form::open(['url' => url('akuntansi/laporan/persediaan/rincian-umur-persediaan'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12">Parameter Lain :</div>
                </div>
                <div class="form-group row">
                    <div class="col-1"></div>
                    {!! Form::label('Kategori Barang',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-8">
                        {!! Form::select('kategori_barang_rincian_umur', [0 => 'Semua'], null, ['class' => 'form-control select2', 'id' => 'kategori_barang_rincian_umur']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-1"></div>
                    {!! Form::label('Filter Barang',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-8">
                        {!! Form::select('barang_rincian_umur', [0 => 'Semua'], null, ['class' => 'form-control select2', 'id' => 'barang_rincian_umur']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-4"></div>
                    <div class="col-8">
                        {!! Form::button('Reset Barang', ['class' => 'form-control btn btn-warning', 'id' => 'reset_barang_rincian_umur']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Lanjutkan</button>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>

{{-- Modal Histori Barang --}}
<div id="histori_barang" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pilih Tanggal Untuk Laporan Histori Barang</h4>
        </div>
            {!! Form::open(['url' => url('akuntansi/laporan/persediaan/histori-barang'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                    </div>
                </div>
            </div>
            <select class="select_barang select2">

            </select>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>

{{-- Modal Ringkasan Analisis Persediaan --}}
<div id="ringkasan_analisis_persediaan" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Analisis Persediaan</h4>
        </div>
            {!! Form::open(['url' => url('akuntansi/laporan/persediaan/ringkasan-analisis-persediaan'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                    </div>
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>