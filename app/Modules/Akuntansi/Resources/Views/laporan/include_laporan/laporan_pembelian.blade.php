<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Pembelian</h1>
        <ul class="list-group">
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinpembelian"><i class="fa fa-tags"></i> Rincian Pembelian</a></li>
            <div class="collapse" id="rinpembelian">
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/pembelian-per-pemasok') }}" data-toggle="modal" data-target="#penjualan_per_pemasok">Pembelian per Pemasok</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/rincian-pembelian-per-pemasok') }}" data-toggle="modal" data-target="#rincian_pembelian_per_pemasok">Rincian Pembelian per Pemasok</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/pembelian-per-barang-total') }}" data-toggle="modal" data-target="#pembelian_per_barang_total">Pembelian per Barang (Total)</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/pembelian-per-barang-kuantitas') }}" data-toggle="modal" data-target="#pembelian_per_barang_kuantitas">Pembelian per Barang (Kuantitas)</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/pembelian-per-barang') }}" data-toggle="modal" data-target="#pembelian_per_barang">Pembelian per Barang</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/rincian-pembelian-per-barang') }}" data-toggle="modal" data-target="#rincian_pembelian_per_barang">Rincian Pembelian per Barang</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/pembelian-per-biaya') }}" data-toggle="modal" data-target="#pembelian_per_biaya">Pembelian per Biaya</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/pengembalian-pembelian-per-pemasok') }}" data-toggle="modal" data-target="#pengembalian_pembelian_per_pemasok">Pengembalian Pembelian per Pemasok</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/pengembalian-pembelian-per-barang') }}" data-toggle="modal" data-target="#pengembalian_pembelian_per_barang">Pengembalian Pembelian per Barang</a></li>
                </ul>
            </div>
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinpesanbeli"><i class="fa fa-tags"></i> Rincian Pesanan & Pembelian</a></li>
            <div id="rinpesanbeli" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/permintaan-pembelian-belum-proses') }}" data-toggle="modal" data-target="#permintaan_pembelian_belum_terproses">Permintaan Pembelian (Belum Terproses)</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/pesanan-pembelian-per-pemasok-belum-proses') }}" data-toggle="modal" data-target="#pesanan_pembelian_perpemasok_belum_terproses">Pesanan Pembelian per Pemasok (Belum Terproses)</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/pesanan-pembelian-per-barang') }}" data-toggle="modal" data-target="#pesanan_pembelian_perbarang">Pesanan Pembelian per Barang</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/histori-permintaan-pembelian') }}" data-toggle="modal" data-target="#histori_permintaan_perbarang">Histori Permintaan Pembelian</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/histori-pesanan-pembelian') }}" data-toggle="modal" data-target="#histori_pesanan_perbarang">Histori Pesanan Pembelian</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/histori-penerimaan-barang') }}" data-toggle="modal" data-target="#histori_penerimaan_perbarang">Histori Penerimaan Barang</a></li>
                </ul>
            </div>
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinfapes"><i class="fa fa-tags"></i> Rincian Uang Muka Faktur & Pesanan</a></li>
            <div class="collapse" id="rinfapes">
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/uang-muka-faktur-pembelian') }}" data-toggle="modal" data-target="#uang_muka_faktur_pembelian">Uang Muka Faktur Pembelian</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembelian/uang-muka-pesanan-pembelian') }}" data-toggle="modal" data-target="#uang_muka_pesanan_pembelian">Uang Muka Pesanan Pembelian</a></li>
                </ul>
            </div>
        </ul>
    </div>
</section>

{{-- Modal Penjualan per Pemasok --}}

<div id="penjualan_per_pemasok" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Keseluruhan Jurnal</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/pembelian-per-pemasok'), 'method' => 'get', 'target' => '_blank']) !!}
      
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('bagi_nilai',"Bagi Nilai Dalam",['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    <select name="bagi_nilai" id="bagi_nilai">
                        <option value="hanya_total">Hanya Total</option>
                        <option value="harian">Harian</option>
                        <option value="mingguan">Mingguan</option>
                        <option value="kwartal">Kwartal</option>
                        <option value="tahunan">Tahunan</option>
                    </select>
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      
      {!! Form::close() !!}
      
    </div>

  </div>
</div>

{{-- Modal Rincian Pembelian per Pemasok --}}
<div id="rincian_pembelian_per_pemasok" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Pembelian per Pemasok</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/rincian-pembelian-per-pemasok'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Pembelian per Barang (Total) --}}
<div id="pembelian_per_barang_total" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pembelian per Barang (Total)</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/pembelian-per-barang-total'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('bagi_nilai',"Bagi Nilai Dalam",['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    <select name="bagi_nilai" id="bagi_nilai">
                        <option value="hanya_total">Hanya Total</option>
                        <option value="harian">Harian</option>
                        <option value="mingguan">Mingguan</option>
                        <option value="kwartal">Kwartal</option>
                        <option value="tahunan">Tahunan</option>
                    </select>
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Pembelian per Barang (Kuantitas) --}}
<div id="pembelian_per_barang_kuantitas" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pembelian per Barang (Kuantitas)</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/pembelian-per-barang-kuantitas'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('bagi_nilai',"Bagi Nilai Dalam",['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    <select name="bagi_nilai" id="bagi_nilai">
                        <option value="hanya_total">Hanya Total</option>
                        <option value="harian">Harian</option>
                        <option value="mingguan">Mingguan</option>
                        <option value="kwartal">Kwartal</option>
                        <option value="tahunan">Tahunan</option>
                    </select>
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Pembelian per Barang --}}
<div id="pembelian_per_barang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pembelian per Barang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/pembelian-per-barang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Pembelian per Barang --}}
<div id="rincian_pembelian_per_barang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Pembelian per Barang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/rincian-pembelian-per-barang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Pembelian per Biaya --}}
<div id="pembelian_per_biaya" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pembelian per Biaya</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/pembelian-per-biaya'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Pengembalian Pembelian per Pemasok --}}
<div id="pengembalian_pembelian_per_pemasok" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pengembalian Pembelian per Pemasok</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/pengembalian-pembelian-per-pemasok'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Pengembalian Pembelian per Barang --}}
<div id="pengembalian_pembelian_per_barang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pengembalian Pembelian per Barang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/pengembalian-pembelian-per-barang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Permintaan Pembelian (Belum Terproses) --}}
<div id="permintaan_pembelian_belum_terproses" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Permintaan Pembelian (Belum Terproses)</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/permintaan-pembelian-belum-proses'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Pesanan Pembelian per Pemasok (Belum Terproses) --}}
<div id="pesanan_pembelian_perpemasok_belum_terproses" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pesanan Pembelian per Pemasok (Belum Terproses)</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/pesanan-pembelian-per-pemasok-belum-proses'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Pesanan Pembelian per Barang --}}
<div id="pesanan_pembelian_perbarang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pesanan Pembelian per Barang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/pesanan-pembelian-per-barang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Histori Permintaan Pembelian --}}
<div id="histori_permintaan_perbarang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Histori Permintaan Pembelian</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/histori-permintaan-pembelian'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Histori Pesanan Pembelian --}}
<div id="histori_pesanan_perbarang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Histori Pesanan Pembelian</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/histori-pesanan-pembelian'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Histori Penerimaan Pembelian --}}
<div id="histori_penerimaan_perbarang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Histori Penerimaan Pembelian</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/histori-penerimaan-barang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Uang Muka Faktur Pembelian --}}
<div id="uang_muka_faktur_pembelian" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Uang Muka Faktur Pembelian</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/uang-muka-faktur-pembelian'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Uang Muka Pesanan Pembelian --}}
<div id="uang_muka_pesanan_pembelian" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Uang Muka Pesanan Pembelian</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/pembelian/uang-muka-pesanan-pembelian'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>