<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Pemeriksaan</h1>
        <ul class="list-group">
            <li class="list-group-item"><a href="#">Ringkasan Daftar Ratio</a></li>
            <li class="list-group-item"><a href="#">Rincian Daftar Ratio</a></li>
            <li class="list-group-item"><a href="#">Ringkasan Jejak Pemeriksaan</a></li>
            <li class="list-group-item"><a href="#">Rincian Jejak Pemeriksaan</a></li>
            <li class="list-group-item"><a href="#">Konfirmasi Saldo Piutang</a></li>
            <li class="list-group-item"><a href="#">Ketas Kerja Neraca Saldo</a></li>
            <li class="list-group-item"><a href="#">Laporan Selisih antara nilai Saldo Akun Persediaan dengan Nilai Barang</a></li>
        </ul>
    </div>
</section>