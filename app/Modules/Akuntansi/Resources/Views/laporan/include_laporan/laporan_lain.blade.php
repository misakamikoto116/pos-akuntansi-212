<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Lain-lain</h1>
        <ul class="list-group">
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#daftar_barang">Daftar Barang</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/laporan-lain-lain/daftar-mata-uang') }}">Daftar Mata Uang</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/laporan-lain-lain/daftar-rincian-mata-uang') }}">Rincian Daftar Mata Uang</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/laporan-lain-lain/daftar-pajak') }}">Daftar Pajak</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/laporan-lain-lain/daftar-syarat-pembayaran') }}">Daftar Syarat Pembayaran</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/laporan-lain-lain/daftar-pengiriman') }}">Daftar Pengiriman</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/laporan-lain-lain/daftar-tipe-pelanggan') }}">Daftar Tipe Pelanggan</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/laporan-lain-lain/daftar-batasan-komisi') }}">Daftar Batasan Komisi</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/laporan-lain-lain/ringkasan-daftar-pengguna') }}">Ringkasan Daftar Pengguna</a></li>
        </ul>
    </div>
</section>

{{-- Modal Rincian Umur Piutang --}}
<div id="daftar_barang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Daftar Barang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/laporan-lain-lain/daftar-barang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Per Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>