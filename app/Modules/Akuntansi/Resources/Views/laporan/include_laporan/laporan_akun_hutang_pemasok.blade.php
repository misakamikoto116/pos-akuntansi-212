<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Akun Hutang & Pemasok</h1>
        <ul class="list-group">
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinakhut"><i class="fa fa-tags"></i> Rincian Akun Hutang</a></li>
            <div id="rinakhut" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#hutang_beredar">Hutang Beredar</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#ringkasan_umur_hutang">Ringkasan Umur Hutang</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_umur_hutang">Rincian Umur Hutang</a></li>
                    <li class="list-group-item"><a href="#">Grafik Umur Hutang</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#ringkasan_buku_besar_pembantu_hutang">Ringkasan Buku Besar Pembantu Hutang</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_buku_besar_pembantu_hutang">Rincian Buku Besar Pembantu Hutang</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#histori_hutang_pemasok"> Histori Hutang Pemasok</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#ringkasan_pembayaran_faktur_pemasok">Ringkasan Pembayaran Faktur</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_pembayaran_faktur_pemasok">Rincian Pembayaran Faktur</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#laporan_pemasok">Laporan Pemasok</a></li>
                </ul>
            </div>
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinpemasok"><i class="fa fa-tags"></i> Rincian Pemasok</a></li>
            <div id="rinpemasok" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#daftar_pemasok">Daftar Pemasok</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#ringkasan_pembayaran_pembelian">Ringkasan Pembayaran Pembelian</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_pembayaran_pembelian">Rincian Pembayaran Pembelian</a></li>
                </ul>
            </div>
        </ul>
    </div>
</section>

{{-- Modal Hutang Beredar --}}
<div id="hutang_beredar" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Hutang Beredar</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-hutang-pemasok/hutang-beredar'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Per Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Umur Hutang Pemasok --}}
<div id="rincian_umur_hutang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Umur Hutang Pemasok</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-hutang-pemasok/rincian-umur-hutang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Per Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Ringkasan Umur Hutang Pemasok --}}
<div id="ringkasan_umur_hutang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Umur Hutang Pemasok</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-hutang-pemasok/ringkasan-umur-hutang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Per Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Daftar Pemasok Pemasok --}}
<div id="daftar_pemasok" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Daftar Pemasok</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-hutang-pemasok/daftar-pemasok'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Per Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Pembayaran Pembelian Pemasok --}}
<div id="rincian_pembayaran_pembelian" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Pembayaran Pembelian</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-hutang-pemasok/rincian-pembayaran-pembelian'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-rincian-buku-besar']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Ringaksan Pembayaran Pembelian Pemasok --}}
<div id="ringkasan_pembayaran_pembelian" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Pembayaran Pembelian</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-hutang-pemasok/ringkasan-pembayaran-pembelian'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-rincian-buku-besar']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Buku Besar Pembantu Hutang --}}
<div id="rincian_buku_besar_pembantu_hutang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Buku Besar Pembantu Hutang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-hutang-pemasok/rincian-buku-besar-pembantu-hutang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-rincian-buku-besar']) !!}          
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <div>
                    <input name="pemasok_transaksi"  id="pemasok_transaksi" type="checkbox">
                    <label for="pemasok_transaksi">
                        Hanya menampilkan pemasok mempunyai transaksi
                    </label>
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Ringkasan Buku Besar Pembantu Hutang --}}
<div id="ringkasan_buku_besar_pembantu_hutang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Buku Besar Pembantu Hutang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-hutang-pemasok/ringkasan-buku-besar-pembantu-hutang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-rincian-buku-besar']) !!}          
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <div>
                    <input name="pemasok_transaksi"  id="pemasok_transaksi" type="checkbox">
                    <label for="pemasok_transaksi">
                        Hanya menampilkan pemasok mempunyai transaksi
                    </label>
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Pembayaran Faktur --}}
<div id="rincian_pembayaran_faktur_pemasok" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Pembayaran Faktur</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-hutang-pemasok/rincian-pembayaran-faktur'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-pembayaran-faktur"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-pembayaran-faktur']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Ringkasan Pembayaran Faktur --}}
<div id="ringkasan_pembayaran_faktur_pemasok" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Pembayaran Faktur</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-hutang-pemasok/ringkasan-pembayaran-faktur'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-ringkasan-pembayaran-faktur-pemasok"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-ringkasan-pembayaran-faktur-pemasok']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Histori Hutang Pemasok --}}
<div id="histori_hutang_pemasok" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Histori Hutang Pemasok</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-hutang-pemasok/histori-hutang-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-histori-hutang-pemasok"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-histori-hutang-pemasok']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Histori Hutang Pemasok --}}
<div id="laporan_pemasok" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pemasok</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/akun-hutang-pemasok/laporan-pemasok'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date-rincian', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-laporan-pemasok"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date-rincian', null, ['class' => 'form-control','id' => 'end-date-laporan-pemasok']) !!}          
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <div>
                    <input name="pemasok_transaksi"  id="pemasok_transaksi" type="checkbox">
                    <label for="pemasok_transaksi">
                        Hanya menampilkan pemasok mempunyai transaksi
                    </label>
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>