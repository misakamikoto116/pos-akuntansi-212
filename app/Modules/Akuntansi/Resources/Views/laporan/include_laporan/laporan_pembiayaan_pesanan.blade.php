<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Pembiayaan Pesanan</h1>
        <ul class="list-group">
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinpembiayaanpesanan"><i class="fa fa-tags"></i> Rincian Pembiayaan Pesanan</a></li>
        <div id="rinpembiayaanpesanan" class="collapse">
            <ul class="list-group">
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#pembiayaan_pesanan_per_barang">Pembiayaan Pesanan per Barang</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#pembiayaan_pesanan_per_beban">Pembiayaan Pesanan per Beban</a></li>
                <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembiayaan-pesanan/pembiayaan-pesanan-per-departemen') }}">Pembiayaan Pesanan per Departemen</a></li>
                <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembiayaan-pesanan/pembiayaan-pesanan-per-proyek') }}">Pembiayaan Pesanan per Proyek</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#pembiayaan_pesanan_selesai">Pembiayaan Pesanan yang Telah Selesai</a></li>
            </ul>
        </div>
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rincianpenawaran"><i class="fa fa-tags"></i> Rincian Penawaran</a></li>
        <div id="rincianpenawaran" class="collapse">
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembiayaan-pesanan/rincian-jurnal-pembiayaan-pesanan') }}">Rincian Jurnal Pembiayaan Pesanan</a></li>
                <li class="list-group-item"><a href="{{ url('akuntansi/laporan/pembiayaan-pesanan/ringkasan-histori-pembiayaan-pesanan') }}">Ringkasan Histori Pembiayaan Pesanan </a></li>
            </ul>
        </div>
        </ul>
    </div>
</section>

{{-- Modal Pembiayaan Pesanan per Barang --}}
<div id="pembiayaan_pesanan_per_barang" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pembiayaan Pesanan per Barang</h4>
        </div>
            {!! Form::open(['url' => url('akuntansi/laporan/pembiayaan/pembiayaan-pesanan-per-barang'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                    </div>
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>

{{-- Modal Pembiayaan Pesanan per Beban --}}
<div id="pembiayaan_pesanan_per_beban" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pembiayaan Pesanan per Beban</h4>
        </div>
            {!! Form::open(['url' => url('akuntansi/laporan/pembiayaan/pembiayaan-pesanan-per-beban'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                    </div>
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>

{{-- Modal Pembiayaan Pesanan yang Telah Selesai --}}
<div id="pembiayaan_pesanan_selesai" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pembiayaan Pesanan yang Telah Selesai</h4>
        </div>
            {!! Form::open(['url' => url('akuntansi/laporan/pembiayaan-pesanan/pembiayaan-pesanan-yang-telah-selesai'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                    </div>
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>