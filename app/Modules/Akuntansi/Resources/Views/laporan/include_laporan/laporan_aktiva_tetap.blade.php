<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Aktiva Tetap</h1>
        <ul class="list-group">
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinaktivatetap"><i class="fa fa-tags"></i> Rincian Aktiva Tetap</a></li>
            <div id="rinaktivatetap" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/aktiva-tetap/tipe-aktiva-tetap-pajak') }}" target ="_blank">Tipe Aktiva Tetap Pajak</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#daftar_aktiva_tetap_per_aktiva_tetap">Daftar Aktiva Tetap per Aktiva Tetap</a></li>
                    <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#daftar_aktiva_tetap_per_aktiva_pajak">Daftar Aktiva Tetap per Aktiva Pajak</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/aktiva-tetap/histori-aktiva-tetap') }}">Histori Aktiva Tetap</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/aktiva-tetap/rincian-jurnal-aktiva-tetap') }}">Rincian Jurnal Aktiva Tetap</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/aktiva-tetap/daftar-penyusutan-aktiva-tetap') }}">Daftar Penyusutan Aktiva Tetap</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/aktiva-tetap/perbedaan-penyusutan-sementara') }}">Perbedaan Penyusutan Sementara</a></li>
                    <li class="list-group-item"><a href="{{ url('akuntansi/laporan/aktiva-tetap/histori-perubahan-asset-tetap') }}">Histori Perubahan Asset Tetap</a></li>
                </ul>
            </div>
        </ul>
    </div>
</section>

{{-- Modal Daftar Aktiva Tetap per Aktiva Tetap --}}
<div id="daftar_aktiva_tetap_per_aktiva_tetap" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pilih Tanggal Untuk Laporan Daftar Aktiva Tetap per Aktiva Tetap</h4>
            </div>
            {!! Form::open(['url' => url('akuntansi/laporan/aktiva-tetap/daftar-aktiva-tetap-per-aktiva-tetap'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start_date', null, ['class' => 'form-control']) !!}          
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Lanjutkan</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{-- Modal Daftar Aktiva Tetap per Aktiva Pajak --}}
<div id="daftar_aktiva_tetap_per_aktiva_pajak" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pilih Tanggal Untuk Laporan Daftar Aktiva Tetap per Aktiva Pajak</h4>
            </div>
            {!! Form::open(['url' => url('akuntansi/laporan/aktiva-tetap/daftar-aktiva-tetap-per-aktiva-pajak'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start_date', null, ['class' => 'form-control']) !!}          
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Lanjutkan</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>