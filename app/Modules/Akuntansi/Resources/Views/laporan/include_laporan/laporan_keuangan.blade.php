<section style="overflow: hidden;">
    <div class="form-group clearfix">
      <h1 class="title" align="center">Detail Laporan Keuangan</h1>
      <ul class="list-group">
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#neraca"><i class="fa fa-tags"></i> Neraca</a></li>
        <div id="neraca" class="collapse">
          <ul class="list-group">
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#neraca_standar">Neraca (Standar)</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#neraca_induk_skontro">Neraca (Induk Skontro)</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#neraca_multi_periode">Neraca (Multi Periode)</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#neraca_perbandingan_bulan">Neraca (Perbandingan Bulan)</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#neraca_anggaran_periode">Neraca (Anggaran Periode)</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/keuangan/neraca-perbandingan-anggaran') }}">Neraca (Perbandingan Anggaran)</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/keuangan/neraca-perbandingan-anggaran-periode') }}">Neraca (Perbandingan Anggaran Periode)</a></li>
            <li class="list-group-item"><a data-toggle="modal" data-target="#neraca_ukuran_umum" href="#">Neraca (Ukuran Umum)</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/keuangan/neraca-konsolidasi') }}">Neraca (Konsolidasi)</a></li>
          </ul>
        </div>
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#labarugi"><i class="fa fa-tags"></i> Laba/Rugi</a></li>
        <div id="labarugi" class="collapse">
          <ul class="list-group">
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#laba_rugi_standar">Laba/Rugi (Standar)</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#laba_rugi_multi_periode">Laba/Rugi (Multi Periode)</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#laba_rugi_perbandingan_periode">Laba/Rugi (Perbandingan Periode)</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#laba_rugi_anggaran_periode">Laba/Rugi (Anggaran Periode)</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/keuangan/laba-rugi-perbandingan-anggaran') }}">Laba/Rugi (Perbandingan Anggaran)</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/keuangan/laba-rugi-perbandingan-anggaran-periode') }}">Laba/Rugi (Perbandingan Anggaran Periode)</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/keuangan/laba-rugi-konsolidasi') }}">Laba/Rugi (Konsolidasi)</a></li>
          </ul>
        </div>
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#laporandata"><i class="fa fa-tags"></i> Laporan Arus Data</a></li>
        <div id="laporandata" class="collapse">
          <div class="panel-body">
            <ul class="list-group">
              <li class="list-group-item"><a href="#">Laporan Data Ditahan</a></li>
              <li class="list-group-item"><a href="#">Fokus Keuangan</a></li>
              <li class="list-group-item"><a href="#">Rincian Laporan Arus Kas (Metode Tak Langsung)</a></li>
              <li class="list-group-item"><a href="#">Laporan Arus Kas (Metode Tak Langsung)</a></li>
              <li class="list-group-item"><a href="#">Laporan Arus Kas (Metode Langsung)</a></li>
              <li class="list-group-item"><a href="#">Laporan Arus Kas per Bulan (Metode Tak Langsung)</a></li>
              <li class="list-group-item"><a href="#">Laporan Arus Kas per Bulan (Metode Langsung)</a></li>
              <li class="list-group-item"><a href="#">Perubahan Equitas Pemilik</a></li>
            </ul>
          </div>
        </div>
        <!-- List Detail Grafik -->
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#grafik"><i class="fa fa-tags"></i> Grafik</a></li>
        <div id="grafik" class="collapse">
          <ul class="list-group">
            <li class="list-group-item"><a href="{{url('akuntansi/laporan/keuangan/grafik-perbandingan-nilai-akun')}}" target="_blank">Grafik Perbandingan Akun</a></li>
            <li class="list-group-item"><a href="#">Grafik Pendapatan dan Beban</a></li>
            <li class="list-group-item"><a href="#">Grafik Harta Bersih</a></li>
            <li class="list-group-item"><a href="#">Grafik Likuiditas</a></li>
            <li class="list-group-item"><a href="#">Grafik Pengembalian Aset</a></li>
            <li class="list-group-item"><a href="#">Pengembalian Pada Modal</a></li>
          </ul>
        </div>
      </ul>
    </div>
</section>

{{-- Modal Laporan Neraca(standar) --}}
<div id="neraca_standar" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Neraca (Standar)</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/keuangan/neraca'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Per Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control']) !!}          
                </div>
            </div>
        </div>
        @include($formFilter)
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Laporan Neraca(Ukuran Umum) --}}
<div id="neraca_ukuran_umum" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Neraca (Ukuran Umum)</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/keuangan/neraca-ukuran-umum'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Per Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control']) !!}          
                </div>
            </div>
        </div>
        @include($formFilter)
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Laporan Neraca(Induk Skontro) --}}
<div id="neraca_induk_skontro" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Neraca (Induk Skontro)</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/keuangan/neraca-induk-skontro'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Per Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control']) !!}          
                </div>
            </div>
        </div>
        @include($formFilter)
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Laporan Neraca Multi Periode --}}
<div id="neraca_multi_periode" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pilih Tanggal Untuk Laporan Neraca Multi Periode</h4>
        </div>
          {!! Form::open(['url' => url('akuntansi/laporan/keuangan/neraca-multi-periode'), 'method' => 'get', 'target' => '_blank']) !!}
          <div class="modal-body">
              <div class="form-group row">
                  {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                  <div class="col-9">
                      {!! Form::text('date_start', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control month-only','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                  </div>
              </div>
              <div class="form-group row">
                  {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                  <div class="col-9">
                      {!! Form::text('date_end', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control month-only']) !!}
                  </div>
              </div>
          </div>
          @include($formFilter)
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>

{{-- Modal Laporan laba Rugi Standar --}}
<div id="laba_rugi_standar" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Laba Rugi Standar</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/keuangan/laba-rugi-standar'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start_date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end_date', null, ['class' => 'form-control','id' => 'end-date-rincian-buku-besar']) !!}          
                </div>
            </div>
        </div>
        @include($formFilter)
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Laporan Laba/Rugi (Multi Periode) --}}
<div id="laba_rugi_multi_periode" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Laba/Rugi (Multi Periode)</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/keuangan/laba-rugi-multi-periode'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::text('date_start', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control month-only','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::text('date_end', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control month-only']) !!}
                </div>
            </div>
        </div>
        @include($formFilter)
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Laba/Rugi (Perbandingan Periode) --}}
<div id="laba_rugi_perbandingan_periode" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Laba/Rugi (Perbandingan Periode)</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/keuangan/laba-rugi-perbandingan-periode'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::text('date_start', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control month-only','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::text('date_end', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control month-only']) !!}
                </div>
            </div>
        </div>
        @include($formFilter)
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
{{-- Modal Neraca (Perbandingan Bulan) --}}
<div id="neraca_perbandingan_bulan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Neraca (Perbandingan Bulan)</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/keuangan/neraca-perbandingan-bulan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::text('date_start', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control month-only','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::text('date_end', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control month-only']) !!}
                </div>
            </div>
            @include($formFilter)
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Neraca (Anggaran Periode) --}}
<div id="neraca_anggaran_periode" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Neraca (Anggaran Periode)</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/keuangan/neraca-anggaran-periode'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::text('date_start', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control month-only','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::text('date_end', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control month-only']) !!}
                </div>
            </div>
        </div>
        @include($formFilter)
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Laba/Rugi (Anggaran Periode) --}}
<div id="laba_rugi_anggaran_periode" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pilih Tanggal Untuk Laporan Laba/Rugi (Anggaran Periode)</h4>
        </div>
          {!! Form::open(['url' => url('akuntansi/laporan/keuangan/laba-rugi-anggaran-periode'), 'method' => 'get', 'target' => '_blank']) !!}
          <div class="modal-body">
              <div class="form-group row">
                  {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                  <div class="col-9">
                      {!! Form::text('date_start', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control month-only','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                  </div>
              </div>
              <div class="form-group row">
                  {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                  <div class="col-9">
                      {!! Form::text('date_end', null, ['placeholder' => 'Pilih Tanggal','class' => 'form-control month-only']) !!}
                  </div>
              </div>
          </div>
          @include($formFilter)
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>