<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Pabrikasi</h1>
        <ul class="list-group">
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinmutasi"><i class="fa fa-tags"></i> Rincian Mutasi</a></li>
            <div id="rinmutasi" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#">Mutasi Barang per Gudang</a></li>
                    <li class="list-group-item"><a href="#">Mutasi Gudang per Barang</a></li>
                    <li class="list-group-item"><a href="#">Ringkasan Mutasi per Gudang per Barang</a></li>
                    <li class="list-group-item"><a href="#">Kuantitas Barang per Daftar Gudang</a></li>
                    <li class="list-group-item"><a href="#">Lembar Perhitungan Stok Fisik Barang</a></li>
                </ul>
            </div>
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinnomorseri"><i class="fa fa-tags"></i> Rincian Nomor Serial</a></li>
            <div id="rinnomorseri" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#">Penerimaan Nomor Seri/Produksi</a></li>
                    <li class="list-group-item"><a href="#">Nomor Seri/Produksi Terkirim</a></li>
                    <li class="list-group-item"><a href="#">Nomor Seri/Produksi Tersedia</a></li>
                    <li class="list-group-item"><a href="#">Histori Nomor Seri</a></li>
                    <li class="list-group-item"><a href="#">Nomor Seri per Gudang</a></li>
                    <li class="list-group-item"><a href="#">Histori Barang Rusak</a></li>
                    <li class="list-group-item"><a href="#">Analisa Barang Rusak</a></li>
                    <li class="list-group-item"><a href="#">Umur Kadaluarsa Barang</a></li>
                    <li class="list-group-item"><a href="#">Rincian Umur Kadaluarsa Barang</a></li>
                    <li class="list-group-item"><a href="#">Laporan Selisih antara Kuantitas Barang dan Kuantitas Barang dan Nomor Seri</a></li>
                </ul>
            </div>
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#ringudang"><i class="fa fa-tags"></i> Rincian Gudang</a></li>
            <div id="ringudang" class="collapse">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#">Daftar Gudang</a></li>
                    <li class="list-group-item"><a href="#">Rincian Daftar Perpindahan Barang</a></li>
                </ul>
            </div>
        </ul>
    </div>
</section>