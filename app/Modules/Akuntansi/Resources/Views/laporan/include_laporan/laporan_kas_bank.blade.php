<section style="overflow: hidden;">
    <div class="form-group clearfix">
      <h1 class="title" align="center">Detail Laporan Kas & Bank</h1>
      <ul class="list-group">
        <li class="list-group-item"><a data-toggle="collapse" href="#kasdanbank"><i class="fa fa-tags"></i> Kas & Bank</a></li>
        <div id="kasdanbank" class="collapse">
          <ul class="list-group">
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#daftar-kas-bank">Daftar Akun Kas & Bank</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#buku_bank">Buku Bank</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/kas-dan-bank/rekonsili-bank') }}">Rekonsiliasi Bank</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/kas-dan-bank/histori-rekonsili-bank') }}">Histori Rekonsiliasi Bank</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/kas-dan-bank/ringkasan-proyeksi-arus-kas') }}">Ringkasan Proyeksi Arus Kas</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/kas-dan-bank/rincian-proyeksi-arus-kas') }}">Rincian Proyeksi Arus Kas</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/kas-dan-bank/arus-kas-per-akun') }}">Arus Kas per Akun</a></li>
            <li class="list-group-item"><a href="{{ url('akuntansi/laporan/kas-dan-bank/giro-mundur-belum-jatuh-tempo') }}">Giro Mundur/Belum Jatuh Tempo</a></li>
          </ul>
        </div>
        <li class="list-group-item"><a data-toggle="collapse" href="#rincian"><i class="fa fa-tags"></i> Rincian Pembayaran & Penerimaan</a></li>
        <div id="rincian" class="collapse">
          <ul class="list-group">
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_daftar_pembayaran">Rincian Daftar Pembayaran</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#ringkasan_pembayaran_per_bank">Ringkasan Pembayaran per Bank</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_pembayaran_per_bank" >Rincian Pembayaran per Bank</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_daftar_penerimaan">Rincian Daftar Penerimaan</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#ringkasan_penerimaan_per_bank">Ringkasan Penerimaan per Bank</a></li>
            <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_penerimaan_per_bank">Rincian Penerimaan per Bank</a></li>
          </ul>
        </div>
      </ul>
    </div>
</section>

{{-- Modal --}}
<div id="daftar-kas-bank" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Daftar Kas & Bank</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/kas-dan-bank/daftar-akun-kas-dan-bank'), 'method' => 'get', 'target' => '_blank']) !!}
      
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      
      {!! Form::close() !!}
      
    </div>

  </div>
</div>

<!-- Modal Buku Bank-->
<div id="buku_bank" class="modal fade" role="dialog">
    <div class="modal-dialog">  
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pilih Tanggal Untuk Laporan Buku Bank</h4>
        </div>
          {!! Form::open(['url' => url('akuntansi/laporan/kas-dan-bank/buku-bank'), 'method' => 'get', 'target' => '_blank']) !!}
            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('start-date', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                    <div class="col-9">
                        {!! Form::date('end-date', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
          </div>
        {!! Form::close() !!}        
      </div>
    </div>
  </div>

{{-- Modal Rincian Daftar Pembayaran --}}
<div id="rincian_daftar_pembayaran" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Daftar Pembayaran</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/kas-dan-bank/rincian-daftar-pembayaran'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-daftar-pembayaran"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-rincian-daftar-pembayaran']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Daftar Penerimaan --}}
<div id="rincian_daftar_penerimaan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Daftar Penerimaan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/kas-dan-bank/rincian-daftar-penerimaan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-daftar-penerimaan"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-rincian-daftar-penerimaan']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Ringkasan Pembayaran Per Bank --}}
<div id="ringkasan_pembayaran_per_bank" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Pembayaran per Bank</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/kas-dan-bank/ringkasan-pembayaran-per-bank'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-ringkasan-pembayaran-per-bank"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-ringkasan-pembayaran-per-bank']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Pembayaran Per Bank --}}
<div id="rincian_pembayaran_per_bank" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Pembayaran per Bank</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/kas-dan-bank/rincian-pembayaran-per-bank'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-pembayaran-per-bank"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-rincian-pembayaran-per-bank']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Ringkasan Penerimaan Per Bank --}}
<div id="ringkasan_penerimaan_per_bank" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Ringkasan Penerimaan per Bank</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/kas-dan-bank/ringkasan-penerimaan-per-bank'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-ringkasan-penerimaan-per-bank"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-ringkasan-penerimaan-per-bank']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Penerimaan Per Bank --}}
<div id="rincian_penerimaan_per_bank" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Penerimaan Per Bank</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/kas-dan-bank/rincian-penerimaan-per-bank'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-buku-bank"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-buku-bank']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>