<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Departemen</h1>
        <ul class="list-group">
            <li class="list-group-item"><a href="#">Daftar Departemen</a></li>
            <li class="list-group-item"><a href="#">Saldo Awal Departemen</a></li>
            <li class="list-group-item"><a href="#">Ringkasan Anggaran Departemen</a></li>
            <li class="list-group-item"><a href="#">Rincian Anggaran Departemen</a></li>
            <li class="list-group-item"><a href="#">Analisis Angaaran Departemen</a></li>
            <li class="list-group-item"><a href="#">Histori Akun Departemen</a></li>
            <li class="list-group-item"><a href="#">Histori Departemen</a></li>
            <li class="list-group-item"><a href="#">Laba & Rugi per Departemen</a></li>
            <li class="list-group-item"><a href="#">Laba/Rugi (Perbandingan Periode) per Departemen</a></li>
        </ul>
    </div>
</section>