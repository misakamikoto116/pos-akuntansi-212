<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Penjualan</h1>
        <ul class="list-group">
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinpenjualan"><i class="fa fa-tags"></i> Rincian Penjualan</a></li>
        <div id="rinpenjualan" class="collapse">
            <ul class="list-group">
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penjualan_per_pelanggan">Penjualan per Pelanggan</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_penjualan_per_pelanggan">Rincian Penjualan per Pelanggan</a></li></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_penjualan_per_barang_per_pelanggan">Penjualan per Barang per Pelanggan</a></li></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_penjualan_per_tipe_pelanggan">Rincian Penjualan per Tipe Pelanggan</a></li></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penjualan_per_omset">Penjualan Perbarang (Omset)</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penjualan_perbarang_kuantitas">Penjualan Perbarang (Kuantitas)</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penjualan_perbarang">Penjualan Perbarang</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penjualan_perbarang_pos">Penjualan Perbarang Pos</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penjualan_perbarang_perkasir">Penjualan Perbarang Perkasir</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penjualan_pertransaksi_perkasir">Penjualan Pertransaksi Perkasir</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#rincian_penjualan_perbarang">Rincian Penjualan Perbarang</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penjualan_pelanggan_per_barang">Penjualan Pelanggan per Barang</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penjualan_barang_per_pelanggan">Penjualan Barang per Pelanggan</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#retur_penjualan_perbarang">Retur Penjualan per Barang</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#retur_penjualan_perpelanggan">Retur Penjualan per Pelanggan</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penjualan_tahunan">Penjualan Tahunan</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#grafik_prestasi_penjualan_barang">Grafik Prestasi Penjualan Barang</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#grafik_prestasi_penjualan_pelanggan">Grafik Prestasi Penjualan Pelanggan</a></li>
            </ul>
        </div>
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinpenawaran"><i class="fa fa-tags"></i> Rincian Penawaran</a></li>
        <div id="rinpenawaran" class="collapse">
            <ul class="list-group">
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penawaran_penjualan_belum_terproses">Penawaran Penjualan per Pelanggan (Belum Terproses)</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#penawaran_penjualan_barang_belum_terproses">Penawaran Penjualan per Barang (Belum Terproses)</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#pesanan_penjualan_belum_terproses">Pesanan Penjualan per Pelanggan (Belum Terproses)</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#pesanan_penjualan_barang_belum_terproses">Pesanan Penjualan per Barang (Belum Terproses)</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#uang_muka_pesanan_penjualan">Uang Muka Pesanan Penjualan</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#uang_muka_perfaktur">Uang Muka per Faktur</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#histori_penawaran_penjualan">Histori Penawaran Penjualan</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#histori_pesanan_penjualan">Histori Pesanan Penjualan</a></li>
                <li class="list-group-item"><a href="#" data-toggle="modal" data-target="#histori_pengiriman_penjualan">Histori Pengiriman Pesanan</a></li>
            </ul>
        </div>
        <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rintapen"><i class="fa fa-tags"></i> Rincian Tagihan & Pendapatan RMA</a></li>
        <div id="rintapen" class="collapse">
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ url('akuntansi/laporan/penjualan/tagihan-rma') }}">Tagihan RMA</a></li>
                <li class="list-group-item"><a href="{{ url('akuntansi/laporan/penjualan/pendapatan-rma') }}">Pendapatan RMA</a></li>
            </ul>
        </div>
        </ul>
    </div>
</section>

{{-- Modal Penjualan per Pelanggan --}}

<div id="penjualan_per_pelanggan" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Keseluruhan Jurnal</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penjualan-per-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
      
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      
      {!! Form::close() !!}
      
    </div>

  </div>
</div>

{{-- Modal Rincian Penjualan per Pelanggan --}}
<div id="rincian_penjualan_per_pelanggan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Penjualan per Pelanggan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/rincian-penjualan-per-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Rincian Penjualan per Tipe Pelanggan --}}
<div id="rincian_penjualan_per_tipe_pelanggan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Penjualan per Tipe Pelanggan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/rincian-penjualan-per-tipe-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tipe Pelanggan', null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::select('tipe_pelanggan_id', $tipePelanggan, null,['class' => 'form-control select2', 'placeholder' => '- Pilih -','required']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
{{-- Modal Rincian Penjualan Per Barang Per Pelanggan --}}

<div id="rincian_penjualan_per_barang_per_pelanggan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Penjualan per Barang per Pelanggan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penjualan-per-barang-per-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Pelanggan', null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::select('pelanggan_id', $pelanggan, null,['class' => 'form-control select2', 'placeholder' => '- Pilih -','required']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{--  --}}

{{-- Modal Penjualan per Omset --}}
<div id="penjualan_per_omset" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penjualan per Omset</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penjualan-perbarang-omset'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Penjualan perbarang kuantitas --}}
<div id="penjualan_perbarang_kuantitas" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penjualan perbarang kuantitas</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penjualan-perbarang-kuantitas'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- B --}}

{{-- Modal Retur Penjualan perpelanggan --}}
<div id="retur_penjualan_perpelanggan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Retur Penjualan perpelanggan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/retur-penjualan-per-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Retur Penjualan perbarang --}}
<div id="retur_penjualan_perbarang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Retur Penjualan perbarang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/retur-penjualan-perbarang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Penawaran Penjualan belum terproses --}}
<div id="penawaran_penjualan_belum_terproses" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penawaran Penjualan belum terproses</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penawaran-penjualan-belum-terproses'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Penawaran Penjualan Barang Belum Terproses --}}
<div id="penawaran_penjualan_barang_belum_terproses" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penawaran Penjualan Barang Belum Terproses</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penawaran-penjualan-per-barang-belum-terproses'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>            
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Pesanan Penjualan per Pelanggan Belum Terprosesi --}}
<div id="pesanan_penjualan_belum_terproses" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pesanan Penjualan per Pelanggan Belum Terprosesi</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/pesanan-penjualan-per-pelanggan-belum-terprosesi'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Pesanan Penjualan per barang belum terprosesi --}}
<div id="pesanan_penjualan_barang_belum_terproses" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Pesanan Penjualan per barang belum terprosesi</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/pesanan-penjualan-per-barang-belum-terprosesi'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Uang Muka Pesanan Penjualan --}}
<div id="uang_muka_pesanan_penjualan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Uang Muka Pesanan Penjualan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/uang-muka-pesanan-penjualan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Uang Muka per Faktur --}}
<div id="uang_muka_perfaktur" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Uang Muka per Faktur</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/uang-muka-per-faktur'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Histori Penawaran Penjualan --}}
<div id="histori_penawaran_penjualan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Histori Penawaran Penjualan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/histori-penawaran-penjualan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
        </div>
        <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Histori Pesanan Penjualan --}}
<div id="histori_pesanan_penjualan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Histori Pesanan Penjualan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/histori-pesanan-penjualan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
            <div class="col-9">
                {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Histori Pengiriman Penjualan --}}
<div id="histori_pengiriman_penjualan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Histori Pengiriman Penjualan</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/histori-pengirim-pesanan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
            <div class="col-9">
                {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Penjualan Perbarang --}}
<div id="penjualan_perbarang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penjualan Perbarang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penjualan-perbarang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
            <div class="col-9">
                {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Penjualan Perbarang Pos --}}
<div id="penjualan_perbarang_pos" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penjualan Perbarang Pos</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penjualan-perbarang-pos'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
            <div class="col-9">
                {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Penjualan Perbarang Perkasir --}}
<div id="penjualan_perbarang_perkasir" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penjualan Perbarang Perkasir</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penjualan-perbarang-perkasir'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            
            <div class="form-group row">
              {!! Form::label('Kasir', null,['class' => 'col-3 col-form-label']) !!}
              <div class="col-9">
                {!! Form::select('kasir_id',$listUserKasir,null,['class' => 'form-control select2','placeholder' => '- Pilih -','required']) !!}
              </div>
            </div>

            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
            <div class="col-9">
                {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Penjualan Pertransaksi Perkasir --}}
<div id="penjualan_pertransaksi_perkasir" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penjualan Pertransaksi Perkasir</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penjualan-pertransaksi-perkasir'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            
            <div class="form-group row">
              {!! Form::label('Kasir', null,['class' => 'col-3 col-form-label']) !!}
              <div class="col-9">
                {!! Form::select('kasir_id',$listUserKasir,null,['class' => 'form-control select2','placeholder' => '- Pilih -','required']) !!}
              </div>
            </div>

            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
            <div class="col-9">
                {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>


{{-- Modal Rincian Penjualan Perbarang --}}
<div id="rincian_penjualan_perbarang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Rincian Penjualan Perbarang</h4>
      </div>
        {!! Form::open(['url' => url('akuntansi/laporan/penjualan/rincian-penjualan-perbarang'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                </div>
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
            <div class="col-9">
                {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>      
      {!! Form::close() !!}
    </div>
  </div>
</div>

{{-- Modal Penjualan Pelanggan per Barang --}}
<div id="penjualan_pelanggan_per_barang" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penjualan Pelanggan per Barang</h4>
        </div>
          {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penjualan-pelanggan-perbarang'), 'method' => 'get', 'target' => '_blank']) !!}
          <div class="modal-body">
              <div class="form-group row">
                  {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                  <div class="col-9">
                      {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                  </div>
              </div>
          </div>
          <div class="form-group row">
              {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
              <div class="col-9">
                  {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
              </div>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>      
        {!! Form::close() !!}
      </div>
    </div>
  </div>

{{-- Modal Penjualan Barang per Pelanggan --}}
<div id="penjualan_barang_per_pelanggan" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penjualan Barang per Pelanggan</h4>
        </div>
          {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penjualan-barang-per-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
          <div class="modal-body">
              <div class="form-group row">
                  {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                  <div class="col-9">
                      {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                  </div>
              </div>
          </div>
          <div class="form-group row">
              {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
              <div class="col-9">
                  {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
              </div>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>      
        {!! Form::close() !!}
      </div>
    </div>
  </div>

  {{-- Modal Grafik Prestasi Penjualan Barang --}}
<div id="grafik_prestasi_penjualan_barang" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pilih Tanggal Untuk Laporan Grafik Prestasi Penjualan Barang</h4>
        </div>
          {!! Form::open(['url' => url('akuntansi/laporan/penjualan/grafik-prestasi-penjualan-barang'), 'method' => 'get', 'target' => '_blank']) !!}
          <div class="modal-body">
              <div class="form-group row">
                  {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                  <div class="col-9">
                      {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                  </div>
              </div>
              <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                  {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
              </div>
              <div class="form-group row">
                {!! Form::label('Pilih Barang',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                  {!! Form::select('produk-item[]', $dataProduk, null, ['class' => 'form-control select2', 'id' => 'produk-item-grafik-prestasi', 'multiple', 'required']) !!}
                </div>
              </div>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>      
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  
  {{-- Modal Grafik Prestasi Penjualan Pelanggan --}}
  <div id="grafik_prestasi_penjualan_pelanggan" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pilih Tanggal Untuk Laporan Grafik Prestasi Penjualan Pelanggan</h4>
        </div>
          {!! Form::open(['url' => url('akuntansi/laporan/penjualan/grafik-prestasi-penjualan-pelanggan'), 'method' => 'get', 'target' => '_blank']) !!}
          <div class="modal-body">
              <div class="form-group row">
                  {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                  <div class="col-9">
                      {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                  </div>
              </div>
              <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                  {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
              </div>
              <div class="form-group row">
                {!! Form::label('Pilih Pelanggan',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                  {!! Form::select('pelanggan-item[]', $dataPelanggan, null, ['class' => 'form-control select2', 'id' => 'produk-item-grafik-prestasi', 'multiple', 'required']) !!}
                </div>
              </div>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>      
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  
  {{-- Modal Penjualan Tahunan --}}
  <div id="penjualan_tahunan" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pilih Tanggal Untuk Laporan Penjualan Tahunan</h4>
        </div>
          {!! Form::open(['url' => url('akuntansi/laporan/penjualan/penjualan-tahunan'), 'method' => 'get', 'target' => '_blank']) !!}
          <div class="modal-body">
              <div class="form-group row">
                  {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                  <div class="col-9">
                      {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}
                  </div>
              </div>
              <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                  {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
              </div>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>      
        {!! Form::close() !!}
      </div>
    </div>
  </div>