@extends( auth()->user()->hasPermissionTo('buat_faktur_pembelian') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788; 
        color: white; 
        text-align: center; 
        position: relative; 
        top: 4px; 
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
    .warnatr{
        background: lightsteelblue;
        color: black;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Pembelian</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Faktur Pembelian</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Purchase Invoice / Faktur Pembelian</h5>
            </div>
            <div class="card-body">
                {!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left','id' => 'form-faktur-pembelian']) !!}
                <div class="p-20">
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary', 'id' => 'btn-cetak' ]) !!}
                <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" id="btn-submit" disabled data-toggle="dropdown">
                Simpan <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        @if ( auth()->user()->hasPermissionTo('buat_pembayaran_pembelian') )
                            <li>
                                {!! Form::submit('Bayar Pemasok', ['class' => 'btn btn-success btn-block','value' => '1', 'name' => 'lanjutkan']) !!}
                            </li>
                        @endif
                        <li>
                            {!! Form::submit('Simpan & Baru', ['class' => 'btn btn-info btn-block','value' => '0', 'name' => 'lanjutkan']) !!}
                        </li>
                        <li>
                            {!! Form::button('Simpan & Tutup',['type' => 'submit','class' => 'btn btn-warning btn-block']) !!}
                        </li>
                    </ul>
                </div>
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                
                </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

        <script type="text/javascript">
        $('.tanggal_faktur').datepicker({format: 'dd MM yyyy', autoclose: true});
        var q               = 0;            
        var x               = 0;
        var z               = 0;
        var old_produk_id   = "{{ !empty(old('produk_id')) ? count(old('produk_id')) : null }}";
        var old_beban_id    = "{{ !empty(old('akun_beban_id')) ? count(old('akun_beban_id')) : null }}";

        @if (old('produk_id'))
            $(document).ready(function () {
                q = $(".produk_id").length;
                x = $(".akun_beban_id").length;
                setDataPemasokWhenOld({{ old('pemasok_id') }});
                sumQtyAndUnit(z);
                counterDp();
                removeDelButton();
                valueOnTabBarang();
                checkSesamaFungsi();
            });
        @endif

        $(function() {
              enable_cb();
              $("#group1").click(enable_cb);
                if(typeof GetURLParameter('pemasok_id') == 'string'){
                  $('#pemasok_id').val(GetURLParameter('pemasok_id'));
                }
              $("#pemasok_id").select2({
                    tags: true
                });
                @if (!old('produk_id'))
                    setDataPemasok();
                @endif
            });

        $( "#pemasok_id" ).change(function() {
            setDataPemasok();
        });

        function setDataPemasok(){
            var pemasok_id = $('#pemasok_id').val();
            var sub = GetURLParameter('penerimaan_id');   
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-alamat-pemasok') }}",
                data: {id : pemasok_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#table-list-pesanan').find('tr').remove().end();
                $('#table-list-penerimaan').find('tr').remove().end();
                $('#table-list-uang-muka').find('tr').remove().end();
                  
                $('.purchasePenawaran').find('tr').remove().end();
                $('.purchaseBeban').find('tr').remove().end();
                $('.purchaseDP').find('tr').remove().end();
                
                $('#alamat_pengiriman').val(response.alamat);
                $('#alamat_asal').val(response.alamat);
                $('#no_pemasok').val(response.no_pemasok);
                $("#tax_cetak0").val(null);
                $("#tax_cetak1").val(null);
                $("#tax_cetak_label0").val(null);
                $("#tax_cetak_label1").val(null);
                q = 0;
                // $('#counterDp').html(0);
                if (response.pesanan) {
                    $.each(response.pesanan, function (index, item){
                        $('#table-list-pesanan').append('<tr><th><input type="checkbox" value="'+item.id+'" name="pesanan-pembelian[]" id="pesanan-pembelian[]"/></th><td>'+item.po_number+'</td><td>'+item.po_date+'</td></tr>');
                    });
                }
                if (response.penerimaan) {
                    $.each(response.penerimaan, function (index, item){
                        if(sub == item.id){
                            $('#table-list-penerimaan').append('<tr><th><input type="checkbox" checked value="'+item.id+'" name="penerimaan-pembelian[]" class="penerimaan_item" id="penerimaan-pembelian[]"/></th><td>'+item.form_no+'</td><td>'+item.receipt_no+'</td><td>'+item.receive_date+'</td></tr>');
                            filterSelectedMethod('penerimaan-pembelian');
                        }else{
                            $('#table-list-penerimaan').append('<tr><th><input type="checkbox" value="'+item.id+'" name="penerimaan-pembelian[]" class="penerimaan_item" id="penerimaan-pembelian[]"/></th><td>'+item.form_no+'</td><td>'+item.receipt_no+'</td><td>'+item.receive_date+'</td></tr>');                        
                            checkAfterSetPemasok();
                        }
                    });
                }
                if (response.uangmuka) {
                    $.each(response.uangmuka, function (index, item){
                        if (item.updated_uang_muka > 0) {
                            $('#table-list-uang-muka').append('<tr><th><input type="checkbox" value="'+item.id+'" name="uangmuka[]" class="uangmuka_item" id="uangmuka[]"/></th><td>'+item.no_faktur+'</td><td>'+item.invoice_date+'</td><td>'+item.updated_uang_muka+'</td></tr>');
                            checkAfterSetPemasok();
                        }
                    });
                }
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        // function test(vale) {
        //     var produk_id   = $('#produk_id'+vale).val();
        //     var pemasok_id   = $('#pemasok_id').val();
        //         $.ajax({
        //             type: "GET",
        //             url: "{{ route('akuntansi.get-produk') }}",
        //             data: {id : produk_id, pemasok_id : pemasok_id,
        //                 _token: '{{ csrf_token() }}'},
        //             dataType: "json",
        //             success: function(response){
        //             $('#tax_produk'+vale).find('option').remove().end();   
        //             $('#gudang_id'+vale).find('option').remove().end();                                                                 
        //             $('#keterangan_produk'+vale).val(response.keterangan);
        //             $('#qty_produk'+vale).val('1');
        //             $('#satuan_produk'+vale).val(response.satuan);
        //             $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
        //             $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0);
        //             // if(!$.trim(response.harga_modal)){
        //             //     $('#amount_old_modal_produk'+vale).val(response.harga_modal.harga_modal);
        //             //     $('#amount_old_qty_produk'+vale).val(response.harga_modal.kuantitas);
        //             // }else{
        //             //     $('#amount_old_modal_produk'+vale).val(0);
        //             //     $('#amount_old_qty_produk'+vale).val(0);
        //             // }

        //             if(response.harga_modal !== null){
        //                 $('#amount_old_modal_produk'+vale).val(response.harga_modal.harga_modal);
        //                 $('#amount_old_qty_produk'+vale).val(response.harga_modal.kuantitas);
        //             }else{
        //                 $('#amount_old_modal_produk'+vale).val(0);
        //                 $('#amount_old_qty_produk'+vale).val(0);
        //             }

        //             $('#diskon_produk'+vale).val('0');
        //             $.each(response.tax, function (index, item){
        //                 $('#tax_produk'+vale).append( 
        //                 $("<option></option>")
        //                 .text(item.nilai+ "/" +item.nama)
        //                 .val(item.id)
        //                 );
        //             });
                    
        //             $(response.multiGudang).each(function (val, text) {
        //                 $(text.gudang).each(function (index, item) {
        //                     $('#gudang_id'+vale).append(`<option value="${item.id}">${item.nama}</option>`);
        //                 });
        //             });

        //             sumQtyAndUnit(vale);
                    
        //         }, failure: function(errMsg) {
        //                 alert(errMsg);
        //             }
        //         });
        // }

        function setNamaAkun(vale){
            var akun_id = $('#akun_beban_id'+vale).val();
            $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-akun_id') }}",
                    data: {id : akun_id,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                success: function(response){
                    $('#akun_beban_nama'+vale).val(response.nama_akun);   
                }, error: function(errMsg) {
                        alert(errMsg);
                    }
                });
        }

        @if(empty(old('produk_id')))
        function insertItemToForm(vale, item, target) {
                 var url = "{{ route('akuntansi.get-produk-by-val') }}";
                 var pemasok_id = $("#pemasok_id").val();                                    
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: {id : item, type : target, pemasok_id : pemasok_id,
                            _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                        $('#tax_produk'+vale).find('option').remove().end();                        
                        $('#gudang_id'+vale).find('option').remove().end();        
                        $('#keterangan_produk'+vale).val(response.keterangan);
                        $('#ship_id').val(response.ship_id);
                        if (response.ship_date !== undefined) {
                            $('#ship_date').val(response.ship_date);
                        }
                        $('#receive_date').val(response.receive_date);
                        $('#term_id').val(response.term_id);
                        $('#po_no').val(response.po_no);
                        $('#form_no').val(response.form_no);
                        $('#fob').val(response.fob);
                        $('#qty_produk'+vale).val(response.updated_qty);
                        $('#satuan_produk'+vale).val(response.satuan);
                        $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
                        $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0); 
                        $('#diskon_produk'+vale).val(response.diskon);
                        $('#barang_penerimaan_pembelian_id'+vale).val(response.barang_id);
                        $('#terproses'+vale).val(response.terproses);
                        $('#no_so'+vale).val(response.no_so);
                        $('#no_do'+vale).val(response.no_do);
                        if (response.taxable) {
                            $("#catatan").val(response.taxable.keterangan);
                            $("#diskonTotal").val(response.taxable.diskon);
                            $("#ongkir").val(response.taxable.ongkir);
                        }
                        // if(!$.trim(response.harga_modal)){
                        //     $('#amount_old_modal_produk'+vale).val(response.harga_modal.harga_modal);
                        //     $('#amount_old_qty_produk'+vale).val(response.harga_modal.kuantitas);
                        // }else{
                        //     $('#amount_old_modal_produk'+vale).val(0);
                        //     $('#amount_old_qty_produk'+vale).val(0);
                        // }
                        if(response.harga_modal !== null){
                            $('#amount_old_modal_produk'+vale).val(response.harga_modal.harga_modal);
                            $('#amount_old_qty_produk'+vale).val(response.harga_modal.kuantitas);
                        }else{
                            $('#amount_old_modal_produk'+vale).val(0);
                            $('#amount_old_qty_produk'+vale).val(0);
                        }

                        if(response.ditutup == 1){
                            $('#ditutup'+vale).prop('checked', true);
                        }
                        $.each(response.tax, function (index, item){
                            if(target == target){
                                if(response.kode_pajak_id == item.id || response.kode_pajak_2_id == item.id){
                                    $('#tax_produk'+vale).append( 
                                    $("<option></option>")
                                    .text(item.nilai+ " / " +item.nama)
                                    .val(item.id)
                                    .attr('selected','selected')
                                    );
                                }else{
                                    $('#tax_produk'+vale).append( 
                                    $("<option></option>") 
                                    .text(item.nilai+ " / " +item.nama)
                                    .val(item.id)
                                    );
                                }
                            }else{
                                $('#tax_produk'+vale).append( 
                                    $("<option></option>") 
                                   .text(item.nilai+ " / " +item.nama)
                                   .val(item.id)
                                );
                            }   
                            
                        });
                        $(response.multiGudang).each(function (val, text) {
                            $(text.gudang).each(function (index, item) {
                                $('#gudang_id'+vale).append(`<option value="${item.id}">${item.nama}</option>`);
                            });
                        });
                        if (response.taxable.taxable == 1) {
                            $("#group1").prop('checked', true);
                            $("#inlineCheckbox2").removeAttr('disabled');
                        }else if(response.taxable.taxable == 0 || response.taxable.taxable === null) {
                            $("#group1").prop('checked', false);
                            $("#inlineCheckbox2").attr('disabled','disabled');
                        }
                        if (response.taxable.in_tax == 1) {
                            $("#inlineCheckbox2").prop('checked', true);
                        }else if(response.taxable.in_tax == 0 || response.taxable.in_tax  ===null){
                            $("#inlineCheckbox2").prop('checked', false);
                        }
                        sumQtyAndUnit(vale);
                        valueOnTabBarang();
                        $('.select3').select2();
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
            }
        @endif

        /** Ketika dipilih salah satu dari pengiriman, pesanan dll maka eksekutornya disini **/
        function filterSelectedMethod(target){
            var checkCounted = $('input[name="'+target+'[]"]:checked').length;
            var input = $('input[name="'+target+'[]"]:checked');
            if(checkCounted > 0){
                input.each(function() {
                    var val = $(this).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-detail-barang') }}",
                        data: {id : val, type : target,
                        _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                            var i = 0;
                            $.each(response.barang, function(index, item){
                                duplicateForm();
                                let countTemp = q - 1;
                                callSelect2AjaxProdukID(countTemp, item.produk_id);
                                $('#produk_id'+countTemp).val(item.produk_id); 
                                insertItemToForm(countTemp, item.id, target); //-1 karena diatas (diplicateForm) sdh di increment
                                
                                if(item.pesanan_pembelian){
                                    if(i > 0){
                                        if(response.barang[i - 1].pesanan_pembelian_id != response.barang[i].pesanan_pembelian_id){
                                            // triger form beban & tambahkan item.pesanan_pembelian.ongkir ke dalamnya
                                            duplicateBeban();
                                            let countTempX = x - 1;
                                            callAkun(countTempX, item.pesanan_pembelian.akun_ongkir_id);
                                            $('#akun_beban_id'+countTempX).val(item.pesanan_pembelian.akun_ongkir_id);
                                            $('#amount_beban'+countTempX).val(item.pesanan_pembelian.ongkir);
                                            
                                        } 
                                    }else{
                                        duplicateBeban();
                                        let countTempX = x - 1;
                                        callAkun(countTempX, item.pesanan_pembelian.akun_ongkir_id);
                                        $('#akun_beban_id'+countTempX).val(item.pesanan_pembelian.akun_ongkir_id);
                                        $('#amount_beban'+countTempX).val(item.pesanan_pembelian.ongkir);
                                    }                                    
                                }
                                i++;
                            });
                            for(a = 0; a < q; a++){
                                callSelect2AjaxProduk(a, 'modal');
                            }
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                });
              }
        }

        function filterUangMuka(){
            var checkCounted = $('input[name="uangmuka[]"]:checked').length;
            var input = $('input[name="uangmuka[]"]:checked');
            if(checkCounted > 0){
                input.each(function() {
                    var val = $(this).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-faktur-pembelian') }}",
                        data: {id : val,
                        _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                            // console.log(response.data.updated_uang_muka);
                            duplicateDP();
                                let countTemp = z - 1;
                                $('#faktur_dp_id'+countTemp).val(response.data.id);
                                $('#keterangan_dp'+countTemp).val(response.penjelasan);
                                $.each(response.tax, function(index, item){
                                    $('#pajak_dp'+countTemp).append( 
                                        $("<option></option>")
                                        .text(item.nilai+ "/" +item.nama)
                                        .val(item.id)
                                        );
                                });
                                $('#total_dp'+countTemp).val(response.data.updated_uang_muka.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
                                $('#no_faktur_dp'+countTemp).val(response.data.no_faktur);
                                $('#no_po_dp'+countTemp).val(response.data.form_no);
                                $('#akun_dp_id'+countTemp).val(response.data.akun_uang_muka_id);
                                $('#include_tax_dp'+countTemp).val(response.termasuk_pajak);
                                counterDp();
                            }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                });
              }
        }

        function counterDp(){
            var pengurang = 0;                    
            var sum_dp = 0;
            $('.total_dp').each(function(){
                sum_dp += parseFloat(changeMaskingToNumber(this.value));
            });
            //$('.faktur_dp_id').each(function(){
            //    let id = this.value;
            //    $.ajax({
            //        type: "GET",
            //        url: "{{ route('akuntansi.check-dp-pembelian') }}",
            //        data: {id : id,
            //            _token: '{{ csrf_token() }}'},
            //        dataType: "json",
            //        success: function(response){
            //           pengurang += response; 
            //      }
            //    });
            //});
            $('#counterDp').html(Number(sum_dp - pengurang).toLocaleString());
        }

        // Fungsi button cetak di klik
        $("#btn-cetak").on("click", function(e) {
            e.preventDefault();
            btnCetak()
        });

        function btnCetak() {
            if ($("#pemasok_id").val() == "" || $('.produk_id').val() == "" || $('.produk_id').val() == undefined) {
                swal({
                    icon: "warning",
                    text: "Pemasok/Barang Masih Kosong"
                });
            }else {
                var form = $("#form-faktur-pembelian");
                form.attr('target','_blank');
                form.attr('action','{{ route('akuntansi.cetak-faktur-pembelian') }}');
                form.submit();
            }
        }

        // Fungsi button simpan tutup dan simpan lanjut di klik
        $(".btn-block").on("click", function () {
            var form = $("#form-faktur-pembelian");
            form.removeAttr('target');
            form.attr('action','{{ route($module_url->store) }}');
            $(".btn-block").on("submit", function () {
                form.submit();
            });
        });
        </script>
        <script src="{{ asset('js/faktur_pembelian.js') }}"></script>
        <script src="{{ asset('js/add-btn-tag-activity.js') }}"></script>
@endsection
