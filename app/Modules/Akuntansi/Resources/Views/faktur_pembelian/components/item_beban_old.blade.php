<tr>
  <td>{!! Form::select('akun_beban_id[' .($row ?? 0). ']',$akunKode, $akun_beban_id ?? null,['placeholder' => '-Pilih-','class' => 'form-control select2 akun_beban_id','required','id' => 'akun_beban_id'.($row ?? 0), 'onchange' => 'setNamaAkun(' .($row ?? 0). ')'])!!}</td>
  <td>{!! Form::text('amount_beban[' .($row ?? 0). ']', $amount_beban ?? null,['class' => 'form-control amount_beban','id' => 'amount_beban'.($row ?? 0),'required'])!!}</td>
  <td>{!! Form::text('notes_beban[' .($row ?? 0). ']', $notes_beban ?? null,['class' => 'form-control notes_beban','id' => 'notes_beban'.($row ?? 0)])!!}</td>
  <td>{!! Form::text('dept_beban_id[' .($row ?? 0). ']', $dept_beban_id ?? null,['class' => 'form-control dept_id','id' => 'dept_beban_id'.($row ?? 0)])!!}</td>
  <td>{!! Form::text('proyek_beban_id[' .($row ?? 0). ']', $proyek_beban_id ?? null,['class' => 'form-control proyek_id','id' => 'proyek_beban_id'.($row ?? 0)])!!}</td>
  <td>
    {!! Form::checkbox('alokasi_ke_barang[' .($row ?? 0). ']',1, $alokasi_ke_barang === 1 ? true : false,['class' => 'form-control alokasi_ke_barang','id' => 'alokasi_ke_barang'.($row ?? 0), 'onchange' => 'checkSesamaFungsi()'])!!}
    {!! Form::hidden('txt_alokasi_ke_barang[' .($row ?? 0). ']', $alokasi_ke_barang ?? null,['class' => 'txt_alokasi_ke_barang', 'id' => 'txt_alokasi_ke_barang'.($row ?? 0)]) !!}
  </td>
  <td>
    @if (!empty($pemasok_beban_id))
      <td>{!! Form::checkbox('buka_pemasok[]', 1, true,['class' => 'form-control buka_pemasok','id' => 'buka_pemasok'.$row,'onchange' => 'checkSesamaFungsi()'])!!}</td>
        <td>
          {!! Form::select('pemasok_beban_id[' .($row ?? 0). ']',$pemasok, $pemasok_beban_id ?? null,['placeholder' => '- Pilih -','class' => 'form-control pemasok_beban_id select2','id' => 'pemasok_beban_id'.$row, 'onchange' => 'checkSesamaFungsi()'])!!}
          {!! Form::hidden('pemasok_beban_id_real[' .($row ?? 0). ']', $pemasok_beban_id, ['class' => 'pemasok_beban_id_real']) !!}
        </td>
      </td>
      @else
      <td>{!! Form::checkbox('buka_pemasok[]', 0, false,['class' => 'form-control buka_pemasok','id' => 'buka_pemasok'.$row,'onchange' => 'checkSesamaFungsi()'])!!}</td>
        <td>
          {!! Form::select('pemasok_beban_id[' .($row ?? 0). ']',$pemasok, null,['placeholder' => '- Pilih -','class' => 'form-control pemasok_beban_id select2','id' => 'pemasok_beban_id'.$row, 'onchange' => 'checkSesamaFungsi()','disabled'])!!}
          {!! Form::hidden('pemasok_beban_id_real[' .($row ?? 0). ']', null, ['class' => 'pemasok_beban_id_real']) !!}
        </td>
      </td>
    @endif
  </td>
  <td><button href="" class="btn btn-danger remove-rincian-beban" onclick="checkSesamaFungsi()" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>