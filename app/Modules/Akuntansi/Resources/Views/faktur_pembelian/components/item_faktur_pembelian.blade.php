@if (isset($row))
  <tr>
  <td>
      {!! Form::hidden('produk_id_temp['.($row ?? 0).']', $produk_id, ['id' => 'produk_id_temp'.$row]) !!}
      {!! Form::text('no_produk['.($row ?? 0).']', $no_produk ?? null,['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required' ,'id' => 'no_produk'.$row, 'onclick' => 'awas('.$row.')', 'readonly'])!!}
      {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id ?? null,['class' => 'form-control produk_id','required' ,'id' => 'produk_id'.$row ])!!}
  </td>
  <td>
      {!! Form::text('keterangan_produk['.($row ?? 0).']',$keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.$row,'required'])!!}
  </td>
  <td>{!! Form::text('qty_produk['.($row ?? 0).']',$qty_produk ?? 0,['class' => 'form-control qty_produk','id' => 'qty_produk'.$row,'required'])!!}</td>
  <td>
      {!! Form::text('satuan_produk['.($row ?? 0).']',$satuan_produk ?? null,['class' => 'form-control satuan_produk','id' => 'satuan_produk'.$row,'required'])!!}
  </td>
  <td>
      {!! Form::text('unit_harga_produk['.($row ?? 0).']',$unit_harga_produk ?? 0,['class' => 'form-control unit_harga_produk','id' => 'unit_harga_produk'.$row,'required'])!!}
  </td>
  <td>
      {!! Form::text('diskon_produk['.($row ?? 0).']',$diskon_produk ?? 0,['class' => 'form-control diskon_produk','id' => 'diskon_produk'.$row,'required'])!!}
  </td>
  <td>
      {!! Form::select('kode_pajak_id['.($row ?? 0).'][]',$pajak,  $kode_pajak_id ?? null,['class' => 'form-control select3 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => 'tax_produk'.$row,''])!!}
  </td>
  <td>
    {!! Form::text('amount_produk['.($row ?? 0).']',$amount_produk ?? 0,['class' => 'form-control amount_produk mask','id' => 'amount_produk'.$row,'readonly' => ''])!!}
    {!! Form::hidden('amount_old_modal_produk['.($row ?? 0).']', $amount_old_modal_produk ?? 0,['class' => 'amount_old_modal_produk','id' => 'amount_old_modal_produk'.$row]) !!}
    {!! Form::hidden('amount_old_qty_produk['.($row ?? 0).']', $amount_old_qty_produk ?? 0,['class' => 'amount_old_qty_produk','id','amount_old_qty_produk'.$row]) !!}
    {!! Form::hidden('amount_modal_produk['.($row ?? 0).']', $amount_modal_produk ?? 0,['class' => 'amount_modal_produk','id','amount_modal_produk'.$row]) !!}
    {!! Form::hidden('barang_penerimaan_pembelian_id['.($row ?? 0).']', $barang_penerimaan_pembelian_id ?? null,['class' => 'barang_penerimaan_pembelian_id','id' => 'barang_penerimaan_pembelian_id'.$row]) !!}
  </td>
  <td>
    {!! Form::select('dept_id['.($row ?? 0).']',[],$dept_id ?? null,['class' => 'form-control select3 dept_id','id' => 'dept_id'.$row,''])!!}
  </td>
  <td>
    {!! Form::select('proyek_id['.($row ?? 0).']',[],$proyek_id ?? null,['class' => 'form-control select3 proyek_id','id' => 'proyek_id'.$row,''])!!}
  </td>
  <td>
    {!! Form::select('gudang_id['.($row ?? 0).']',$gudang,$gudang_id ?? null,['class' => 'form-control select3 gudang_id','id' => 'gudang_id'.$row,''])!!}
  </td>
  <td>
    {!! Form::text('sn['.($row ?? 0).']',$sn ?? null,['class' => 'form-control sn','id' => 'sn'.$row,'readonly'])!!}
  </td>
  <td><button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>
@else
  <tr>
  <td>
      
      {!! Form::hidden('produk_id_temp['.($row ?? 0).']', $produk_id, ['id' => '']) !!}
      {!! Form::text('no_produk['.($row ?? 0).']', $no_produk ?? null,['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required', 'readonly'])!!}
      {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id ?? null,['class' => 'form-control produk_id','required'])!!}
  </td>
  <td>
      {!! Form::text('keterangan_produk['.($row ?? 0).']',$keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}
  </td>
  <td>{!! Form::text('qty_produk['.($row ?? 0).']',$qty_produk ?? 0,['class' => 'form-control qty_produk','id' => '','required'])!!}</td>
  <td>
      {!! Form::text('satuan_produk['.($row ?? 0).']',$satuan_produk ?? null,['class' => 'form-control satuan_produk','id' => '','required'])!!}
  </td>
  <td>
      {!! Form::text('unit_harga_produk['.($row ?? 0).']',$unit_harga_produk ?? 0,['class' => 'form-control unit_harga_produk','id' => '','required'])!!}
  </td>
  <td>
      {!! Form::text('diskon_produk['.($row ?? 0).']',$diskon_produk ?? 0,['class' => 'form-control diskon_produk','id' => '','required'])!!}
  </td>
  <td>
      {!! Form::select('kode_pajak_id['.($row ?? 0).'][]',$pajak,  $kode_pajak_id ?? null,['class' => 'form-control select3 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => '',''])!!}
  </td>
  <td>
    {!! Form::text('amount_produk['.($row ?? 0).']',$amount_produk ?? 0,['class' => 'form-control amount_produk mask','id' => '','readonly' => ''])!!}
    {!! Form::hidden('amount_old_modal_produk['.($row ?? 0).']', $amount_old_modal_produk ?? 0,['class' => 'amount_old_modal_produk']) !!}
    {!! Form::hidden('amount_old_qty_produk['.($row ?? 0).']', $amount_old_qty_produk ?? 0,['class' => 'amount_old_qty_produk']) !!}
    {!! Form::hidden('amount_modal_produk['.($row ?? 0).']', $amount_modal_produk ?? 0,['class' => 'amount_modal_produk']) !!}
    {!! Form::hidden('barang_penerimaan_pembelian_id['.($row ?? 0).']', $barang_penerimaan_pembelian_id ?? null,['class' => 'barang_penerimaan_pembelian_id']) !!}
  </td>
  <td>
    {!! Form::select('dept_id['.($row ?? 0).']',[],$dept_id ?? null,['class' => 'form-control select3 dept_id','id' => '',''])!!}
  </td>
  <td>
    {!! Form::select('proyek_id['.($row ?? 0).']',[],$proyek_id ?? null,['class' => 'form-control select3 proyek_id','id' => '',''])!!}
  </td>
  <td>
    {!! Form::select('gudang_id['.($row ?? 0).']',$gudang,$gudang_id ?? null,['class' => 'form-control select3 gudang_id','id' => '',''])!!}
  </td>
  <td>
    {!! Form::text('sn['.($row ?? 0).']',$sn ?? null,['class' => 'form-control sn','id' => '','readonly'])!!}
  </td>
  <td><button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>
@endif
