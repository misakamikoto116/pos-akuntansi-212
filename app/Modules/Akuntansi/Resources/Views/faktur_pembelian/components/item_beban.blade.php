<tr>
  <td>{!! Form::select('akun_beban_id[' .($row ?? 0). ']',$akunKode, $dataBeban->akun_beban_id,['placeholder' => '-Pilih-','class' => 'form-control select2 akun_beban_id','required','id' => 'akun_beban_id'.($row ?? 0), 'onchange' => 'setNamaAkun(' .($row ?? 0). ')'])!!}</td>
  <td>{!! Form::text('amount_beban[' .($row ?? 0). ']', $dataBeban->amount,['class' => 'form-control amount_beban','id' => 'amount_beban'.($row ?? 0),'required'])!!}</td>
  <td>{!! Form::text('notes_beban[' .($row ?? 0). ']', $dataBeban->notes,['class' => 'form-control notes_beban','id' => 'notes_beban'.($row ?? 0)])!!}</td>
  <td>{!! Form::text('dept_beban_id[' .($row ?? 0). ']', null,['class' => 'form-control dept_id','id' => 'dept_beban_id'.($row ?? 0)])!!}</td>
  <td>{!! Form::text('proyek_beban_id[' .($row ?? 0). ']', null,['class' => 'form-control proyek_id','id' => 'proyek_beban_id'.($row ?? 0)])!!}</td>
  <td>
    {!! Form::checkbox('alokasi_ke_barang[' .($row ?? 0). ']',1, $dataBeban->alokasi_ke_barang === 1 ? true : false,['class' => 'form-control alokasi_ke_barang','id' => 'alokasi_ke_barang'.($row ?? 0), 'onchange' => 'checkSesamaFungsi()'])!!}
    {!! Form::hidden('txt_alokasi_ke_barang[' .($row ?? 0). ']', $dataBeban->alokasi_ke_barang,['class' => 'txt_alokasi_ke_barang', 'id' => 'txt_alokasi_ke_barang'.($row ?? 0)]) !!}
  </td>
  <td>{!! Form::checkbox('buka_pemasok[' .($row ?? 0). ']',1, is_numeric($dataBeban->pemasok_id) ? true : false,['class' => 'form-control buka_pemasok','id' => 'buka_pemasok'.$row,'onchange' => 'checkSesamaFungsi()'])!!}</td>
  <td>
    {!! Form::select('pemasok_beban_id[' .($row ?? 0). ']',$pemasok, $dataBeban->pemasok_id,['placeholder' => '- Pilih -','class' => 'form-control pemasok_beban_id select2','id' => 'pemasok_beban_id'.($row ?? 0), 'onchange' => 'checkSesamaFungsi()'])!!}
    {!! Form::hidden('pemasok_beban_id_real[]', $dataBeban->pemasok_id, ['class' => 'pemasok_beban_id_real']) !!}
  </td>
  <td><button href="" class="btn btn-danger remove-rincian-beban" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>