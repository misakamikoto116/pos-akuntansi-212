@if (isset($item))
  {!! Form::hidden('faktur_pembelian', $item->id,['id' => 'faktur_pembelian_id'])!!}
@endif
<div class="form-group row">
  <div class="col-1 col-form-label">Vendor</div>
  <div class="col-2">
  {!! Form::text('no_pemasok',null,['class' => 'form-control','id' => 'no_pemasok','readonly'])!!}
  </div>
  <div class="col-4">
  {!! Form::select('pemasok_id',$pemasok,null,['placeholder' => '- Pilih -','class' => 'form-control select3','id' => 'pemasok_id'])!!}
  </div>
  <div class="col-2">
    <div class="btn-group">
        <button type="button" class="btn btn-sm btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-list"></i>&nbsp; Option <span class="caret"></span></button>
        <div class="dropdown-menu">
            <a class="dropdown-item btn-info" data-toggle="modal" data-target="#pesanan">Pilih Pesanan</a>
            <a class="dropdown-item btn-info" data-toggle="modal" data-target="#penerimaan">Pilih Penerimaan</a>
            <a class="dropdown-item btn-info" data-toggle="modal" data-target="#pengiriman">Pilih Uang Muka</a>
        </div>
    </div>

 <!-- Modal Pesanan -->
<div class="modal fade" id="pesanan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pesanan Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Transaksi</th>
              <th>Tanggal</th>
          </tr>
          </thead>
          <tbody id="table-list-pesanan">
          
          </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="filterSelectedMethod('pesanan-pembelian')" data-dismiss="modal" class="btn btn-default">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

 <!-- Modal Pesanan -->
<div class="modal fade" id="penerimaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Penerimaan Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Form</th>
              <th>No. Penerimaan</th>
              <th>Tanggal</th>
          </tr>
          </thead>
          <tbody id="table-list-penerimaan">
          
          </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="filterSelectedMethod('penerimaan-pembelian')" data-dismiss="modal" class="btn btn-default">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

 <!-- Modal Pengiriman -->
<div class="modal fade" id="pengiriman" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Uang Muka</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Transaksi</th>
              <th>Tanggal</th>
              <th>Nilai Faktur</th>
          </tr>
          </thead>
          <tbody id="table-list-uang-muka">
          
          </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <a onclick="filterUangMuka()" data-dismiss="modal" type="button" class="btn btn-default">Save changes</a>
      </div>
    </div>
  </div>
</div><!-- END Modal -->
  {!! Form::hidden('total', null, ['class' => 'kolom_grandtotal']) !!}
  {!! Form::hidden('subtotal', null, ['class' => 'kolom_subtotal']) !!}
  {!! Form::hidden('subTotalWithBeban', null, ['class' => 'kolom_subtotal_beban']) !!}
  {!! Form::hidden('total_pajak', null, ['class' => 'kolom_pajaktotal']) !!}
  {!! Form::hidden('receive_date', null,['id' => 'receive_date']) !!}
  </div>
  <div class="col-3 form-inline">
    <div class="form-check form-check-inline">
    @if (isset($item))
    {!! Form::checkbox('taxable', 1, $item->taxable == 1 ? true : false,['class' => 'form-check-input taxable','id' => 'group1','onchange' => 'calculate()']) !!}
    @else
    {!! Form::checkbox('taxable', 1, true,['class' => 'form-check-input taxable','id' => 'group1','onchange' => 'calculate()']) !!}
    @endif
    <label class="form-check-label" for="group1"> Vendor is Taxable</label>
  </div>
  <div class="form-check form-check-inline">
    @if (isset($item))
    {!! Form::checkbox('in_tax', 1, $item->in_tax == 1 ? true : false,['class' => 'form-check-input group2 in_tax', 'id' => 'inlineCheckbox2', 'onchange' => 'calculate()']) !!}
    @else
    {!! Form::checkbox('in_tax', 1, null,['class' => 'form-check-input group2 in_tax', 'id' => 'inlineCheckbox2', 'onchange' => 'calculate()']) !!}
    @endif
    <label class="form-check-label" for="inlineCheckbox2"> Inclusive Tax</label>
  </div>
  </div>
</div>

<div class="row">
  <div class="offset-sm-3 col-sm-3">
    <div class="form-group">
      {!! Form::textarea('alamat_pengiriman',null,['class' => 'form-control','id' => 'alamat_pengiriman'])!!}
      </div>
  </div>
  <div class="offset-sm-2 col-sm-4">
    <div class="row">
      <div class="offset-sm-4 col-sm-4">
        <div class="form-group">
          <div class="tag">Form No</div>
          {!! Form::text('form_no', isset($item->form_no) ? $item->form_no : $form_no,['class' => 'form-control','id' => 'form_no'])!!}
          </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">Ship Date</div>
          @if (isset($item) || old('ship_date'))
          {!! Form::text('ship_date',null,['class' => 'form-control tanggal_faktur','id' => 'ship_date','required'])!!}
          @else
          {!! Form::text('ship_date',null,['class' => 'form-control tanggal','id' => 'ship_date','required'])!!}
          @endif
          </div>
      </div>
      <div class="offset-sm-4 col-sm-4">
        <div class="form-group">
          <div class="tag">Invoice No.</div>
          {!! Form::text('no_faktur', isset($item->no_faktur) ? $item->no_faktur : $no_faktur,['class' => 'form-control','id' => 'no_faktur'])!!}
          </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">Invoice Date</div>
          @if (isset($item) || old('invoice_date'))
          {!! Form::text('invoice_date',null,['class' => 'form-control tanggal_faktur','id' => 'invoice_date','required'])!!}
          @else
          {!! Form::text('invoice_date',null,['class' => 'form-control tanggal','id' => 'invoice_date','required'])!!}
          @endif
          </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">FOB</div>
          {!! Form::select('fob',['0' => 'Shiping Point', '1' => 'Destination' ],null,['placeholder' => '- Pilih -','class' => 'form-control select3','id' => 'fob'])!!}
          </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag tag-terms">Terms</div>
          {!! Form::select('term_id',$termin,null,['placeholder' => '- Pilih -','class' => 'form-control select3','id' => 'term_id'])!!}
          </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag tag-ships">Ship Via</div>
          {!! Form::select('ship_id',$pengiriman,null,['placeholder' => '- Pilih -','class' => 'form-control select3','id' => 'ship_id'])!!}
          </div>
      </div>
      <div class="col-sm-12">
        <div class="form-group row">
          <div class="col-4 col-form-label text-right">Amount</div>
          <div class="col-4">{!! Form::text('amount',null,['class' => 'form-control mask','id' => 'amount_calc',''])!!}</div>
           <div class="col-4"><a class="btn btn-default btn-sm" onclick="paymentAmount()"><i class="fa fa-calculator"></i></a></div>
          </div>
      </div>
    </div>
</div>
</div>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang <strong id="total-amount-barang"></strong></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-beban-tab" data-toggle="pill" href="#pills-beban" role="tab" aria-controls="pills-beban" aria-selected="true">Beban <strong id="total-amount-beban"></strong></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-uang-tab" data-toggle="pill" href="#pills-uang" role="tab" aria-controls="pills-uang" aria-selected="true">Uang Muka <strong id="counterDp"></strong></a>
  </li>
</ul>


<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
    <div class="penawaran-sections" data-id="1">
    <div class="duplicate-penawaran-sections" style="width: 2000px;">
    <table width="100%" class="table duplicate-sections">
      <thead class="thead-light" style="border-collapse: collapse;">
        <tr>
          <th width="12%">Item</th>
          <th width="12%">Item Description</th>
          <th>Qty</th>
          <th>Item Unit</th>
          <th>Unit Price</th>
          <th>Disc %</th>
          <th width="10%">Tax</th>
          <th>Amount</th>
          <th width="5%">Dept</th>
          <th width="5%">Proyek</th>
          <th width="10%">Gudang</th>
          <th width="10%">Expired Date</th>
          <th>SN</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="purchasePenawaran">
        @if(old('produk_id') !== null)
            @foreach(old('produk_id') as $row => $produk_id)
            @php 
              $kode_pajak = (empty(old('kode_pajak_id')[$row])) ? null : old('kode_pajak_id')[$row] ;
              $unit_harga = (empty(old('unit_harga')[$row])) ? null : old('unit_harga')[$row] ;
              $diskon_produk = (empty(old('diskon_produk')[$row])) ? null : old('diskon_produk')[$row] ;
            @endphp
              @include('akuntansi::faktur_pembelian/components/item_faktur_pembelian',[
                'row'                               => $row, 
                'produk_id'                         => $produk_id,
                'no_produk'                         => old('no_produk')[$row],
                'keterangan_produk'                 => old('keterangan_produk')[$row],
                'qty_produk'                        => old('qty_produk')[$row],
                'satuan_produk'                     => old('satuan_produk')[$row],
                'unit_harga_produk'                 => $unit_harga,
                'diskon_produk'                     => $diskon_produk,
                'kode_pajak_id'                     => $kode_pajak,
                'amount_produk'                     => old('amount_produk')[$row],
                'amount_old_modal_produk'           => old('amount_old_modal_produk')[$row],
                'amount_old_qty_produk'             => old('amount_old_qty_produk')[$row],
                'amount_modal_produk'               => old('amount_modal_produk')[$row],
                'barang_penerimaan_pembelian_id'    => old('barang_penerimaan_pembelian_id')[$row],
                'dept_id'                           => old('dept_id')[$row],
                'proyek_id'                         => old('proyek_id')[$row],
                'gudang_id'                         => old('gudang_id')[$row],
                'sn'                                => old('sn')[$row],
              ])
            @endforeach     
        @endif
      </tbody>
    </table>
    <a class="btn btn-info add-penawaran" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
  </div>
</div>

  <div class="tab-pane fade" id="pills-beban" role="tabpanel" aria-labelledby="pills-beban-tab" style="width: 100%; overflow: auto;">
    <div class="beban-sections" data-id="1">
    <div class="duplicate-beban-sections" style="width: 2000px;">
    <table width="100%" class="table duplicate-sections">
      <thead class="thead-light" style="border-collapse: collapse;">
        <tr>
          <th width="13%">Account No.</th>
          <th>Amount</th>
          <th width="17%">Notes</th>
          <th>Department</th>
          <th>Project</th>
          <th>Alokasi ke barang</th>
          <th>Pilih Pemasok</th>
          <th width="17%">Ke Pemasok lain</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="purchaseBeban">
        @if (old('akun_beban_id') !== null)
          @foreach (old('akun_beban_id') as $row => $akun_beban_id)
            @include('akuntansi::faktur_pembelian/components/item_beban_old',[
              'row'                   => $row,
              'akun_beban_id'         => $akun_beban_id,
              'amount_beban'          => old('amount_beban')[$row] ?? null,
              'notes_beban'           => old('notes_beban')[$row] ?? null,
              'dept_beban_id'         => old('dept_beban_id')[$row] ?? null,
              'proyek_beban_id'       => old('proyek_beban_id')[$row] ?? null,
              'alokasi_ke_barang'     => old('alokasi_ke_barang')[$row] ?? null,
              'txt_alokasi_ke_barang' => old('txt_alokasi_ke_barang')[$row] ?? null,
              'pemasok_beban_id'      => old('pemasok_beban_id')[$row] ?? null,
              'buka_pemasok'          => old('buka_pemasok')[$row] ?? [],
            ])
          @endforeach
        @endif

        @if (isset($bebanFakturPembelian))
          @foreach ($bebanFakturPembelian as $row =>  $dataBeban)
            @include('akuntansi::faktur_pembelian/components/item_beban', compact('row', 'dataBeban'))
          @endforeach
        @endif
      </tbody>
    </table>
    <a class="btn btn-info add-beban" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
  </div>
</div>

  <div class="tab-pane fade" id="pills-uang" role="tabpanel" aria-labelledby="pills-uang-tab">
    <div class="dp-sections" data-id="1">
    <div class="duplicate-dp-sections">
    <table class="table duplicate-sections" width="100%">
      <thead class="thead-light">
        <tr>
          <th>Penjelasan</th>
          <th>Pajak</th>
          <th width="20%">Total Uang Muka</th>
          <th>No Faktur</th>
          <th>No Pesanan</th>
          <th>Termasuk Pajak</th>
          <th>Opsi</th>
        </tr>
      </thead>
      <tbody class="purchaseDP">
        @if(old('akun_dp_id') !== null)  
            @foreach(old('akun_dp_id') as $row => $akun_dp_id)
              <tr>
                <td>
                  {!! Form::hidden('akun_dp_id['.($row ?? 0).']', $akun_dp_id ?? null, ['class' => 'akun_dp_id','id' => 'akun_dp_id'.$row]) !!}
                  {!! Form::hidden('faktur_dp_id['.($row ?? 0).']', old('faktur_dp_id')[$row] ?? null, ['class' => 'faktur_dp_id','id' => 'faktur_dp_id'.$row]) !!}
                  {!! Form::text('keterangan_dp['.($row ?? 0).']', old('keterangan_dp')[$row] ?? null,['class' => 'form-control keterangan_dp','id' => 'keterangan_dp'.$row]) !!}
                </td>
                <td width="12%">
                  {!! Form::select('pajak_dp['.($row ?? 0).'][]',$pajak, old('pajak_dp'),['class' => 'form-control select3 select2-multiple pajak_dp','multiple' => 'multiple','multiple','id' => 'pajak_dp'.$row])!!}
                </td>
                <td>{!! Form::text('total_dp['.($row ?? 0).']', old('total_dp')[$row] ?? 0,['class' => 'form-control total_dp mask','id' => 'total_dp'.$row,'onchange' => 'valueOnTabUangMuka()'])!!}</td>
                <td>{!! Form::text('no_faktur_dp['.($row ?? 0).']', old('no_faktur_dp')[$row] ?? null,['class' => 'form-control no_faktur_dp','id' => 'no_faktur_dp'.$row,'','readonly'])!!}</td>
                <td>{!! Form::text('no_po_dp['.($row ?? 0).']', old('no_po_dp')[$row] ?? null,['class' => 'form-control no_po_dp','id' => 'no_po_dp'.$row,'','readonly'])!!}</td>
                <td>{!! Form::text('include_tax_dp['.($row ?? 0).']', old('include_tax_dp')[$row] ?? null,['class' => 'form-control include_tax_dp','id', 'include_tax_dp'.$row,'','readonly'])!!}</td>
                <td><button href="" class="btn btn-danger remove-kolom-dp" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
              </tr>
            @endforeach      
        @endif
        @if (isset($item) && old('akun_dp_id') == null)
          @foreach ($item->transaksiUangMukaPemasok as $dataUangMuka)
              <tr>
                <td>
                  {!! Form::hidden('akun_dp_id[]', $dataUangMuka->fakturUangMukaPemasok->akun_uang_muka_id, ['class' => 'akun_dp_id']) !!}
                  {!! Form::hidden('faktur_dp_id[]', $dataUangMuka->faktur_uang_muka_pemasok_id, ['class' => 'faktur_dp_id']) !!}
                  {!! Form::text('keterangan_dp[]',$dataUangMuka->fakturUangMukaPemasok->barang->first()->item_deskripsi,['class' => 'form-control keterangan_dp','id' => '','']) !!}
                </td>
                <td width="12%">
                  {!! Form::select('pajak_dp[][]',[], null, ['class' => 'form-control select3 select2-multiple pajak_dp','multiple' => 'multiple','multiple','id' => '',''])!!}
                </td>
                <td>{!! Form::text('total_dp[]',$dataUangMuka->jumlah,['class' => 'form-control total_dp','id' => '',''])!!}</td>
                <td>{!! Form::text('no_faktur_dp[]',$dataUangMuka->fakturUangMukaPemasok->no_faktur,['class' => 'form-control no_faktur_dp','id' => '','','readonly'])!!}</td>
                <td>{!! Form::text('no_po_dp[]',null,['class' => 'form-control no_po_dp','id' => '','','readonly'])!!}</td>
                <td>{!! Form::text('include_tax_dp[]',$dataUangMuka->fakturUangMukaPemasok->in_tax_texted,['class' => 'form-control include_tax_dp','id' => '','','readonly'])!!}</td>
                <td><button href="" class="btn btn-danger remove-kolom-dp" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
              </tr>
          @endforeach          
        @endif  
      </tbody>
    </table>
  </div>
</div>
</div>

</div><!-- END PILLS -->

<script type="text/template" id="table_penawaran_section" data-id=""/>
  <tr>
    <td>
      {!! Form::hidden('produk_id[]', null, ['class' => 'form-control produk_id','required'])!!}
      {!! Form::text('no_produk[]', null, ['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required', 'readonly'])!!}
    </td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('unit_harga_produk[]',null,['class' => 'form-control unit_harga_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('diskon_produk[]',null,['class' => 'form-control diskon_produk','id' => '','required'])!!}</td>
    <td>
      {!! Form::select('kode_pajak_id[][]',$pajak,null,['class' => 'form-control select3 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => '',''])!!}
    </td>
    <td>
      {!! Form::text('amount_produk[]',null,['class' => 'form-control amount_produk','id' => '',''])!!}
      {!! Form::hidden('amount_old_modal_produk[]', null,['class' => 'amount_old_modal_produk']) !!}
      {!! Form::hidden('amount_old_qty_produk[]', null,['class' => 'amount_old_qty_produk']) !!}
      {!! Form::hidden('amount_modal_produk[]', null,['class' => 'amount_modal_produk']) !!}
      {!! Form::hidden('barang_penerimaan_pembelian_id[]', null,['class' => 'barang_penerimaan_pembelian_id']) !!}
    </td>
    <td>{!! Form::select('dept_id[]',[],null,['class' => 'form-control select3 dept_id','id' => '',''])!!}</td>
    <td>{!! Form::select('proyek_id[]',[],null,['class' => 'form-control select3 proyek_id','id' => '',''])!!}</td>
    <td>{!! Form::select('gudang_id[]',[],null,['class' => 'form-control select3 gudang_id','id' => '',''])!!}</td>
    <td>{!! Form::text('expired_date[]',null,['class' => 'form-control tanggal_faktur expired_date','id' => '']) !!}</td>
    <td>{!! Form::text('sn[]',null,['class' => 'form-control sn','id' => '','readonly'])!!}</td>
    <td><button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>

<script type="text/template" id="table_beban_section" data-id=""/>
  <tr>
    <td>
    {!! Form::select('akun_beban_id[]',[],null,['placeholder' => '-Pilih-','class' => 'form-control akun_id akun_beban_id','required'])!!}</td>
    <td>{!! Form::text('amount_beban[]',null,['class' => 'form-control amount_beban','id' => '','required'])!!}</td>
    <td>{!! Form::text('notes_beban[]',null,['class' => 'form-control notes_beban','id' => '',''])!!}</td>
    <td>{!! Form::text('dept_beban_id[]',null,['class' => 'form-control dept_id','id' => '',''])!!}</td>
    <td>{!! Form::text('proyek_beban_id[]',null,['class' => 'form-control proyek_id','id' => '',''])!!}</td>
    <td>
      {!! Form::checkbox('alokasi_ke_barang[]',1,false,['class' => 'form-control alokasi_ke_barang','id' => '',''])!!}
      {!! Form::hidden('txt_alokasi_ke_barang[]', 0, ['class' => 'txt_alokasi_ke_barang']) !!}
    </td>
    <td>{!! Form::checkbox('buka_pemasok[]',1,false,['class' => 'form-control buka_pemasok','id' => '',''])!!}</td>
    <td>
      {!! Form::select('pemasok_beban_id[]',['' => '-Pilih-']+$pemasok,null,['class' => 'form-control pemasok_beban_id','id' => '',''])!!}
      
      {!! Form::hidden('pemasok_beban_id_real[]', null, ['class' => 'pemasok_beban_id_real']) !!}
      
    </td>
    <td><button href="" class="btn btn-danger remove-rincian-beban" onclick="checkSesamaFungsi()" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>

<script type="text/template" id="table_dp_section"/>
  <tr>
    <td>
      {!! Form::hidden('faktur_dp_id[]', null, ['class' => 'faktur_dp_id']) !!}
      {!! Form::hidden('akun_dp_id[]', null, ['class' => 'akun_dp_id']) !!}
      {!! Form::text('keterangan_dp[]',null,['class' => 'form-control keterangan_dp','id' => '','']) !!}
    </td>
    <td width="12%">
      {!! Form::select('pajak_dp[][]',[], null, ['class' => 'form-control select3 select2-multiple pajak_dp','multiple' => 'multiple','multiple','id' => '',''])!!}
    </td>
    <td>{!! Form::text('total_dp[]',null,['class' => 'form-control total_dp','id' => '',''])!!}</td>
    <td>{!! Form::text('no_faktur_dp[]',null,['class' => 'form-control no_faktur_dp','id' => '','','readonly'])!!}</td>
    <td>{!! Form::text('no_po_dp[]',null,['class' => 'form-control no_po_dp','id' => '','','readonly'])!!}</td>
    <td>{!! Form::text('include_tax_dp[]',null,['class' => 'form-control include_tax_dp','id' => '','','readonly'])!!}</td>
    <td><button href="" class="btn btn-danger remove-kolom-dp" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>

<hr>
<div class="row">
	<div class="col-sm-4">
	 	<div class="form-group">
	    <label for="exampleFormControlTextarea1">Description</label>
	    {!! Form::textarea('catatan',null,['class' => 'form-control','id' => 'catatan'])!!}
	  </div>
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Akun Hutang</label>
      {!! Form::select('akun_hutang_id',$akun, !isset($item) ? $getAkunHutang : null,['placeholder'=>'--Pilih--','class' => 'form-control akun_id','id' => 'akun_hutang_id'])!!}
    </div>
    <hr>
    <div class="row">
      <div class="col-5 col-form-label">Saldo</div>
      <div class="col-7 col-form-label">: <strong>{{ !empty($item) ? number_format($item->total) : 0  }}</strong></div>  
    </div>
    <div class="row">
      <div class="col-5 col-form-label">Syarat Diskon</div>
      <div class="col-7 col-form-label">: <strong>{{ !empty($item) ? number_format($item->total_syarat_diskon, 2) : 0 }}</strong></div>
    </div>
	</div>
	<div class="offset-sm-4 col-sm-4">
		<div class="form-group row">
		<div class="offset-2 col-3 col-form-label text-right"><strong>Sub Total</strong></div>
		<div class="col-7 text-right col-form-label" id="subtotal"><strong>0</strong></div>
		</div>
		<div class="form-group row">
			<div class="col-2 col-form-label">Discount:</div>
      @if (old('diskon'))
        <div class="col-3 col-form-label">
          {!! Form::text('diskon',old('diskon') ?? 0,['class' => 'form-control','id' => 'diskonTotal', 'onchange' => 'calculate()','required'])!!}
        </div>
      @elseif(isset($item))
        <div class="col-3 col-form-label">
          {!! Form::text('diskon',isset($item) ? $item->diskon : 0,['class' => 'form-control','id' => 'diskonTotal', 'onchange' => 'calculate()','required'])!!}
        </div>
      @else
        <div class="col-3 col-form-label">
          {!! Form::text('diskon',0,['class' => 'form-control','id' => 'diskonTotal', 'onchange' => 'calculate()','required'])!!}
        </div>
      @endif
      
      <div class="col-2 col-form-label text-center">% =</div>
      @if (old('total_diskon_faktur'))
        <div class="col-5 col-form-label">
          {!! Form::text('total_diskon_faktur', old('total_diskon_faktur') ?? 0,['class' => 'form-control mask','id' => 'totalPotonganRupiah','onchange' => 'calculate()'])!!}
        </div>
      @elseif(isset($item))
        <div class="col-5 col-form-label">
          {!! Form::text('total_diskon_faktur', isset($item) ? $item->total_diskon_faktur : 0,['class' => 'form-control mask','id' => 'totalPotonganRupiah','onchange' => 'calculate()'])!!}
        </div>
      @else
      <div class="col-5 col-form-label">
          {!! Form::text('total_diskon_faktur', 0,['class' => 'form-control mask','id' => 'totalPotonganRupiah','onchange' => 'calculate()'])!!}
        </div>
      @endif
    </div>
    <div class="form-group row">
			<div class="col-5 col-form-label"></div>
			<div class="col-5 col-form-label" id="taxNaration"></div>
      {!! Form::hidden('tax_cetak0', null,['id' => 'tax_cetak0'])!!}
      {!! Form::hidden('tax_cetak1', null,['id' => 'tax_cetak1'])!!}
      {!! Form::hidden('tax_cetak_label0', null,['id' => 'tax_cetak_label0'])!!}
      {!! Form::hidden('tax_cetak_label1', null,['id' => 'tax_cetak_label1'])!!}
		</div>
		{{--  <div class="form-group row" style="margin-top: 50px;">
		<div class="offset-2 col-3 col-form-label text-right">Freight</div>
		  <div class="col-7">
        {!! Form::text('ongkir', (!empty(old('ongkir'))) ? old('ongkir') : (isset($item->ongkir) ? $item->ongkir : 0),['class' => 'form-control','id' => 'ongkir','onchange'=>'calculate()'])!!}
      </div>
		</div>  --}}
		<div class="form-group row">
		<div class="offset-1 col-4 col-form-label text-right"><strong>Total Invoice</strong></div>
    <div class="col-7 text-right col-form-label grandtotal"><strong>0</strong></div>
		</div>
	</div>
</div>

@include('akuntansi::pesanan_modal/modal_barang')