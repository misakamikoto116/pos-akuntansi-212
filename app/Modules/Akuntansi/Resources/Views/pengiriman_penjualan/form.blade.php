<div class="form-group row">
  <div class="col-1 col-form-label">Pelanggan</div>
  <div class="col-2">
  {!! Form::text('no_pelanggan',isset($item->pelanggan) ? $item->pelanggan->no_pelanggan : '',['class' => 'form-control','id' => 'no_pelanggan','readonly'])!!}
  </div>
  <div class="col-4">
  {!! Form::select('pelanggan_id',$pelanggan,null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'pelanggan_id'])!!}
  </div>
  <div class="col-2">
  	<button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-list"></i>&nbsp; Pilih Pesanan</button>

    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pesanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Transaksi</th>
              <th>Tanggal</th>
              <th>PO No.</th>
          </tr>
          </thead>
          <tbody id="table-list-pengiriman">
            
          </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="filterSelectedOffer()" data-dismiss="modal">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

  </div>
  <!-- <div class="col-3 form-inline">
    <div class="form-check form-check-inline">
    <input class="form-check-input" type="checkbox" id="group1" value="option1">
    <label class="form-check-label" for="group1"> Cust. is Taxable</label>
  </div>
  <div class="form-check form-check-inline">
    <input class="form-check-input group2" type="checkbox" id="inlineCheckbox2" value="option2" disabled="">
    <label class="form-check-label" for="inlineCheckbox2"> Inclusive Tax</label>
  </div>
  </div> -->
</div>

<hr>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
	    <label for="">Tagihan Ke</label>
	    {!! Form::textarea('alamat_tagihan',null,['class' => 'form-control','id' => 'alamat_asal','readonly'])!!}
	  	</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
	    <label for="">Kirim Ke</label>
	    {!! Form::textarea('alamat_pengiriman',null,['class' => 'form-control','id' => 'alamat_pengiriman',''])!!}
	  	</div>
      <div>
        {!! Form::select('pelanggan_kirim_ke_id',$pelanggan,null,['placeholder'=>'-Pilih-', 'class' => 'form-control select2','id' => 'pelanggan_kirim_ke',''])!!}
      </div>
	</div>
	<div class="offset-sm-2 col-sm-4">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
			    <div class="tag">PO. No.</div>
			    {!! Form::text('po_no',(empty($item->po_no) ? $idPrediction : $item->po_no),['class' => 'form-control','id' => 'po_no_id'])!!}
			  	</div>
			</div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">Delivery No</div>
          {!! Form::text('delivery_no',(empty($item->delivery_no) ? $idPrediction : $item->delivery_no),['class' => 'form-control','id' => '','required'])!!}
          </div>
      </div>
			<div class="col-sm-4">
				<div class="form-group">
			    <div class="tag">Delivery Date</div>
          @if (isset($item))
             {!! Form::text('delivery_date',null,['class' => 'form-control tanggal_pengiriman','id' => 'delivery_date', 'required'])!!}
          @else
             {!! Form::text('delivery_date',null,['class' => 'form-control tanggal','id' => 'delivery_date', 'required'])!!}
          @endif
			  	</div>
			</div>
      <div class="offset-8 col-sm-4">
        <div class="form-group">
          <div class="tag tag-ships">Ship Via</div>
          {!! Form::select('ship_id',$pengiriman,null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'ship_id',''])!!}
          </div>
      </div>
		</div>
	</div>
</div>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>

<div class="pengiriman-sections" data-id="1">
<div class="duplicate-pengiriman-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
    <div>
      <table width="100%" class="table">
        <thead class="thead-light" style="border-collapse: collapse;">
          <tr>
            <th width="13%">Item</th>
            <th width="13%">Item Description</th>
            <th width="5%">Qty</th>
            <th>Satuan</th>
            <th width="10%">Dept</th>
            <th width="10%">Proyek</th>
            <th width="13%">Gudang</th>
            <th>SN</th>
            <th>No SO</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="purchasePengiriman">
          @if (isset($item))
            @php $looping = $item->barangPengirimanPenjualan->count() + 1; @endphp
            @foreach ($item->barangPengirimanPenjualan as $dataBarang)
              <tr class="tr-class" data-id="{{ $looping }}">
                <td>
                  {!! Form::hidden('nominal[]', null, ['class' => 'nominal', 'id' => 'nominal'.$loop->iteration]) !!}
                  {!! Form::select('produk_id[]',[], $dataBarang->produk_id,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id', 'id' => 'produk_id'.$loop->iteration,'onchange' => 'test('.$loop->iteration.')','required'])!!}
                
                {!! Form::hidden('produk_id_temp', $dataBarang->produk_id, ['id' => 'produk_id_temp'.$loop->iteration,'required']) !!}
                
                </td>
                <td>{!! Form::text('keterangan_produk[]', $dataBarang->produk->keterangan,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.$loop->iteration,'required'])!!}</td>
                <td>{!! Form::number('qty_produk[]', $dataBarang->jumlah,['class' => 'form-control qty_produk','id' => 'qty_produk'.$loop->iteration, 'onchange' => 'cekGudangStock('.$loop->iteration.')','required'])!!}</td>
                <td>{!! Form::text('satuan_produk[]',$dataBarang->item_unit,['class' => 'form-control satuan_produk','id' => 'satuan_produk'.$loop->iteration,'required'])!!}</td>
                <td>{!! Form::select('dept_id[]',[],null,['class' => 'form-control select2 dept_id','id' => 'dept_id'.$loop->iteration])!!}</td>
                <td>{!! Form::select('proyek_id[]',[],null,['class' => 'form-control select2 proyek_id','id' => 'proyek_id'.$loop->iteration,''])!!}</td>
                <td>{!! Form::select('gudang_id[]',$gudang, $dataBarang->gudang_id,['class' => 'form-control select2 gudang_id','id' => 'gudang_id'.$loop->iteration,'required', 'onchange' => 'cekGudangStock('.$loop->iteration.')'])!!}</td>
                <td>{!! Form::text('sn[]',null,['class' => 'form-control sn','id' => 'sn'.$loop->iteration,''])!!}</td>
                <td>
                    {!! Form::text('no_so[]',$dataBarang->no_so,['class' => 'form-control no_so','id' => 'no_so'.$loop->iteration,'readonly' => ''])!!}
                    {!! Form::hidden('harga_modal[]',$dataBarang->harga_modal,['class' => 'form-control harga_modal','id' => 'harga_modal'.$loop->iteration])!!}
                    {!! Form::hidden('harga_terakhir[]', 0,['class' => 'form-control harga_terakhir','id' => 'harga_terakhir'.$loop->iteration])!!}
                </td>
                <td style="display: none;">{!! Form::hidden('barang_id[]',$dataBarang->barang_pesanan_penjualan_id,['class' => 'form-control barang_id','id' => 'barang_id'.$loop->iteration,''])!!}</td>
                <td><button href="" class="remove btn btn-danger remove-rincian-pengiriman" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
              </tr>
              @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>

</div><!-- END PILLS -->
</div>
</div>


  <script type="text/template" id="table_pengiriman_section" data-id="">
      @include('akuntansi::pengiriman_penjualan/components/item_pengiriman')
  </script>
<hr>
<div class="row">
	<div class="col-sm-4">
	 	<div class="form-group">
	    <label for="exampleFormControlTextarea1">Description</label>
	    {!! Form::textarea('keterangan',null,['class' => 'form-control','id' => 'catatan'])!!}
	  </div>
	</div>
</div>

@include('akuntansi::pesanan_modal/modal_barang')