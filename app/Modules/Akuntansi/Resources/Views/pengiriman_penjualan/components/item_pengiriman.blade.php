<tr>
    <td>
        {!! Form::hidden('nominal['. ($row ?? 0) .']', $nominal ?? null, ['class' => 'nominal', 'id' => 'nominal']) !!}
        {!! Form::text('no_produk['. ($row ?? 0) .']', $no_produk ?? null,['placeholder' => 'Cari Barang', 'class' => 'form-control no_produk','id' => 'no_produk', 'readonly'])!!}
        {!! Form::hidden('produk_id['. ($row ?? 0) .']', $produk_id ?? null,['class' => 'form-control produk_id','required'])!!}
        {{-- {!! Form::select('produk_id['. ($row ?? 0) .']',[], $produk_id ?? null,['class' => 'form-control select2 produk_id','required'])!!} --}}
    </td>
    <td>
        {!! Form::text('keterangan_produk['. ($row ?? 0) .']', $keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}
    </td>
    <td>
        {!! Form::number('qty_produk['. ($row ?? 0) .']', $qty_produk ?? null,['class' => 'form-control qty_produk','id' => '', 'onchange' => 'sumQtyAndUnit(0)', 'required'])!!}
    </td>
    <td>
        {!! Form::text('satuan_produk['. ($row ?? 0) .']', $satuan_produk ?? null,['class' => 'form-control satuan_produk','id' => '', 'required'])!!}
    </td>
    <td>
        {!! Form::select('dept_id['. ($row ?? 0) .']',[], $dept_id ?? null,['class' => 'form-control select2 dept_id','id' => '',''])!!}
    </td>
    <td>
        {!! Form::select('proyek_id['. ($row ?? 0) .']',[], $proyek_id ?? null,['class' => 'form-control select2 proyek_id','id' => '',''])!!}
    </td>
    <td>
        {!! Form::select('gudang_id['. ($row ?? 0) .']', $gudang, $gudang_id ?? null,['class' => 'form-control select2 gudang_id','id' => 'gudang_id','required'])!!}
    </td>
    <td>
        {!! Form::text('sn['. ($row ?? 0) .']', $sn ?? null,['class' => 'form-control sn','id' => '',''])!!}
    </td>
    <td>
        {!! Form::text('no_so['. ($row ?? 0) .']', $no_so ?? null,['class' => 'form-control no_so','id' => '','readonly' => ''])!!}
        {!! Form::hidden('harga_modal['. ($row ?? 0) .']', $harga_modal ?? 0,['class' => 'form-control harga_modal','id' => ''])!!}
        {!! Form::hidden('harga_terakhir['. ($row ?? 0) .']', $harga_terakhir ?? 0,['class' => 'form-control harga_terakhir','id' => ''])!!}
    </td>
    <td style="display: none;">
        {!! Form::hidden('barang_id['. ($row ?? 0) .']', $barang_id ?? null,['class' => 'form-control barang_id','id' => 'barang_id',''])!!}
    </td>
    <td><button href="" class="remove btn btn-danger remove-rincian-pengiriman" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>