@extends( auth()->user()->hasPermissionTo('ubah_pengiriman_penjualan') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
<link href="{{asset('assets/plugins/footable/css/footable.core.css')}}" rel="stylesheet">
<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788;
        color: white;
        text-align: center;
        position: relative;
        top: 4px;
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Penjualan</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Pengiriman Penjualan</a></li>
                    </ol>

                </div>
            </div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Delivery Order / Pengiriman Penjualan</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left','id' => 'form-penjualan-penawaran']) !!}
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary', 'id' => 'btn-cetak']) !!}
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" id="btn-submit" disabled data-toggle="dropdown">
                    Simpan <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        @if ( auth()->user()->hasPermissionTo('buat_faktur_penjualan') )
                            <li>
                                {!! Form::submit('Faktur', ['class' => 'btn btn-success btn-block','value' => '1', 'name' => 'lanjutkan']) !!}
                            </li>
                        @endif
                        <li>
                            {!! Form::button('Simpan & Tutup',['type' => 'submit','class' => 'btn btn-warning btn-block']) !!}
                        </li>
                    </ul>
                </div>
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger']) !!}
                
                </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>
        <script type="text/javascript">
            var q = $('.tr-class').data("id");
            $(function() {
                enable_cb();
                $("#group1").click(enable_cb);
                var loaded_item = $('.purchasePengiriman').find('tr').length;                           
                for(var a = 1; a <= loaded_item; a++){
                    callSelect2AjaxProdukID(a, $('#produk_id_temp'+a).val());
                    callSelect2AjaxProduk(a);
                }
            });

            function enable_cb() {
              if (this.checked) {
                $("input.group2").removeAttr("disabled");
              } else {
                $("input.group2").attr("disabled", true).removeAttr("checked");
              }
            }
            $( "#pelanggan_id" ).change(function() {
                var pelanggan_id   = $('#pelanggan_id').val();
                  $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-alamat-pelanggan') }}",
                    data: {id : pelanggan_id,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                    $('.purchasePengiriman').find('tr').remove().end();                           
                    $('#alamat_pengiriman').val(response.alamat)
                    $('#alamat_asal').val(response.alamat)
                    $('#no_pelanggan').val(response.no_pelanggan)
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
            });

        $('#selectAll').click(function(e){
            var table= $(e.target).closest('table');
            $('th input:checkbox',table).prop('checked',this.checked);
        });

        $('document').ready(function(){
            setDataPelanggan('onload');
        });

        $( "#pelanggan_id" ).change(function() {
            setDataPelanggan();
        });

       function setDataPelanggan(param = null) {
            var pelanggan_id   = $('#pelanggan_id').val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-alamat-pelanggan') }}",
                data: {id : pelanggan_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#table-list-pengiriman').find('tr').remove().end();
                // $('#alamat_asal').val(response.alamat)
                $.each(response.pesanan, function (index, item){
                    if(item.id == response.pengiriman[0].id){
                        $('#table-list-pengiriman').append('<tr><th><input type="checkbox" checked="checked" value="'+item.id+'" name="pesanan[]" class="item_pesanan" id="pesanan[]"/></th><td>'+item.so_number+'</td><td>'+item.so_date+'</td><td>'+item.po_number+'</td></tr>');
                    }else{
                        $('#table-list-pengiriman').append('<tr><th><input type="checkbox" value="'+item.id+'" name="pesanan[]" class="item_pesanan" id="pesanan[]"/></th><td>'+item.so_number+'</td><td>'+item.so_date+'</td><td>'+item.po_number+'</td></tr>');
                    }
                });
                var item_penawaran = $('.item_pesanan').length;    
                if(param == null){
                    if(item_penawaran > 0){
                        swal({
                            title: "Peringatan!",
                            text: "Pelanggan ini memiliki pesanan dan tidak digunakan, ingin gunakan pesanan?",
                            icon: "warning",
                            buttons: {
                                cancel: true,
                                confirm: true,
                            },
                        }).then((result) => {
                            if (result) {
                                //$(".item_pesanan").each(function() {
                                //    $(this).prop('checked', true);
                                //});
                                //filterSelectedOffer();
                                $('#exampleModal').modal('show');                                
                            }
                        });
                    }
                }        
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        };

        $( "#pelanggan_kirim_ke" ).change(function() {
            var pelanggan_kirim_ke_id   = $('#pelanggan_kirim_ke').val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-alamat-pelanggan-kirim-ke') }}",
                data: {id : pelanggan_kirim_ke_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#alamat_pengiriman').val(response.alamat);
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        });

    function duplicateForm(){
        var row = $('#table_pengiriman_section').html();
        var clone = $(row).clone();
        clone.find('.nominal').attr('id','nominal'+q);
        clone.find('.produk_id').attr('name','produk_id[]').attr('id','produk_id'+q).attr('onchange','test('+q+')').attr('class','select2 form-control');
        clone.find('.keterangan_produk').attr('name','keterangan_produk[]').attr('id','keterangan_produk'+q);
        clone.find('.qty_produk').attr('name','qty_produk[]').attr('id','qty_produk'+q).attr('onchange','cekGudangStock('+q+')');
        clone.find('.satuan_produk').attr('name','satuan_produk[]').attr('id','satuan_produk'+q);
        clone.find('.dept_id').attr('name','dept_q[]').attr('id','dept_id'+q);
        clone.find('.proyek_id').attr('name','proyek_q[]').attr('id','proyek_id'+q);
        clone.find('.barang_id').attr('name','barang_id[]').attr('id','barang_id'+q);
        clone.find('.gudang_id').attr('name','gudang_id[]').attr('id','gudang_id'+q).attr('onchange','cekGudangStock('+q+')');
        clone.find('.no_so').attr('name','no_so[]').attr('id','no_so'+q);
        clone.find('.sn').attr('name','sn[]').attr('id','sn'+q);
        clone.find('.harga_modal').attr('name','harga_modal[]').attr('id','harga_modal'+q);
        clone.find('.harga_terakhir').attr('name','harga_terakhir[]').attr('id','harga_terakhir'+q);
        $(clone).appendTo($('.duplicate-pengiriman-sections').closest('.pengiriman-sections').find('.purchasePengiriman'));
        $('.select2').select2();   
        callSelect2AjaxProduk(q);
        removeDelButton();
        q++;
    }

function callSelect2AjaxProdukID(q, id) {
    $.ajax({
        url: "{{ route('akuntansi.get-id-produk') }}",
        type: 'GET',
        data: 'search=' + id,
        dataType: 'JSON',
        success: function (data) {
            $.each(data.item, function (key, value) {
                $('#produk_id' + q).append('<option selected value="' + value.id + '">' + value.text + '</option>');
            });
        }
    });
}

function callSelect2AjaxProduk(q) {
    $('.produk_id').select2({
    minimumInputLength: 3,
    formatInputTooShort: function () {
        return "Ketik 3 Karakter";
    },
        ajax: {
            url: "{{ route('akuntansi.get-nama-produk') }}",
            type: 'GET',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            dataType: 'JSON',
            processResults: function (data) {
                return {
                    results: data
                };
            }
        }
    });
}

    $(document).ready(function () {
        $('.duplicate-pengiriman-sections').on('click', '.add-pengiriman', function () {
            duplicateForm();
        });
    });

    function test(vale) {
        var produk_id   = $('#produk_id'+vale).val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-produk') }}",
                data: {id : produk_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#gudang_id'+vale).find('option').remove().end();    
                $('#keterangan_produk'+vale).val(response.keterangan);
                $('#qty_produk'+vale).val('1');
                $('#satuan_produk'+vale).val(response.satuan);
                $('#unit_harga_produk'+vale).val(response.unitPrice);
                $('#diskon_produk'+vale).val(response.diskon);
                $(response.multiGudang).each(function (val, text) {
                    $(text.gudang).each(function (index, item) {
                        $('#gudang_id'+vale).append(`<option class="form-control select2" value="${item.id}">${item.nama}</option>`);
                    });
                });
                // $('#gudang_id'+vale).select2('data', response.multiGudang)
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
        });
    }

    function insertItemToForm(vale,item) {
        var url = "{{ route('akuntansi.get-produk-by-val') }}";                                    
              $.ajax({
                type: "GET",
                url: url,
                data: {id : item, type: 'pesanan',
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                callSelect2AjaxProdukID(vale, response.produk_id);
                $('#po_no_id').val(response.po_no);                        
                $('#ship_id').val(response.ship_id);
                $('#keterangan_produk'+vale).val(response.keterangan);
                $('#qty_produk'+vale).val(response.jumlah);
                $('#satuan_produk'+vale).val(response.satuan);
                $('#no_so'+vale).val(response.no_so);
                $('#barang_id'+vale).val(response.barang_id);
                $('#nominal'+vale).val(response.nominal);
                $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
                $('#harga_terakhir'+vale).val(response.harga_modal.harga_terakhir);
                $('#gudang_id'+vale).find('option').remove().end();
                $(response.multiGudang).each(function (val, text) {
                    $(text.gudang).each(function (index, item) {
                        $('#gudang_id'+vale).append(`<option class="form-control select2" value="${item.id}">${item.nama}</option>`);
                    });
                });
                $('.select2').select2();
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
    }

    function filterSelectedOffer() {
        var checkCounted = $('input[name="pesanan[]"]:checked').length;
        if(checkCounted > 0){
            $('input[name="pesanan[]"]:checked').each(function() {
                q = 0;
                $('.purchasePengiriman').find('tr').remove().end();                
                var val = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-detail-barang') }}",
                    data: {id : val, type: 'pesanan',
                    _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                        $.each(response.barang, function(index, item){
                            duplicateForm();
                            let countTemp = q - 1;
                            callSelect2AjaxProdukID(countTemp, item.produk_id);
                            $('#produk_id'+countTemp).val(item.produk_id);
                            $('.select2').select2(); 
                            insertItemToForm(countTemp, item.id); //-1 karena diatas (diplicateForm) sdh di increment
                            callSelect2AjaxProduk(countTemp - 1);
                        });
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
            });
          }
    }

        $('.tanggal_pengiriman').datepicker({format: 'dd MM yyyy', autoclose: true});
        $('.duplicate-pengiriman-sections').on('click', '.remove-rincian-pengiriman', function () {
            if ($(this).closest('.purchasePengiriman').find('tr').length > 0) {
                $(this).closest('tr').remove();
                $('.purchasePengiriman tr:last').find('td:last').append('<button href="" class="remove btn btn-danger remove-rincian-pengiriman" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button>');
                q -= 1;
            }
        });

        function cekGudangStock(param) {
            // cekQtyandPrice(param);
            // var goSubmit = 0;
            // var gudang_stock_id = $('#gudang_id' + param).val();
            // var produk_stock_id = $('#produk_id' + param).val();
            // var qty_stock_id = $('#qty_produk' + param).val();
            // $.ajax({
            //     type: "GET",
            //     url: "{{ route('akuntansi.get-gudang-stock') }}",
            //     data: {
            //         gudang_id: gudang_stock_id, produk_id: produk_stock_id, qty_id: qty_stock_id, _token: '{{ csrf_token() }}'
            //     },
            //     dataType: "json",
            //     success: function (response) {
            //         if (response.status === 0) {
            //             swal({
            //                 icon: 'error',
            //                 text: 'Stock ' + $("#keterangan_produk" + param).val() + ' di ' + response.gudang + ' tersisa ' + response.kuantitas + ' harap masukkan jumlah yang sesuai !!!'
            //             });
            //         }
            //     }, failure: function (errMsg) {
            //         alert(errMsg);
            //     }
            // });
        }

        function cekQtyandPrice(param) {
            if ($("#qty_produk"+ param).val() == "" || $("#qty_produk"+ param).val() <= 0) {
                $("#qty_produk"+ param).val(1);
                swal({
                    icon: "error",
                    text: "Barang Masih Kosong"
                });
            }
        }

        // Fungsi button cetak di klik
        $("#btn-cetak").on("click", function(e) {
            e.preventDefault();
            btnCetak()
        });

        function btnCetak() {
            if ($("#pelanggan_id").val() == "" || $('.produk_id').val() == "" || $('.produk_id').val() == undefined) {
                swal({
                    icon: "warning",
                    text: "Pelanggan/Barang Masih Kosong"
                });
            }else {
                var form = $("#form-penjualan-penawaran");
                form.attr('target','_blank');
                form.attr('action','{{ route('akuntansi.edit-cetak-pengiriman-penjualan') }}');
                form.submit();
            }
        }

        // Fungsi button simpan tutup dan simpan lanjut di klik
        $("#btn-submit").on("click", function () {
            var form = $("#form-penjualan-penawaran");
            form.removeAttr('target');
            form.attr('action','{{ route($module_url->update,$item->id) }}');
            $("#btn-submit").on("submit", function () {
                form.submit();
            });
        });

        // Fungsi delete terakhir tombol delete
        function removeDelButton() {
            $('.remove-rincian-pengiriman').not(':last').remove();
        }

        </script>
        <script src="{{ asset('js/add-btn-tag-activity.js') }}"></script>
@endsection