<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Report Check Out</title>
</head>

<body>
    <section class="sheet padding-10mm" style="padding: 5mm">

        <img style="margin-left:5mm" width="80%" src="" />

        <table cellpadding="10px">
            <tr>
                <td>Nama Dokumen</td>
                <td> :</td>
                <td> Setoran Pemasukan 212 Mart</td>
            </tr>
            <tr>
                <td>Operasional</td>
                <td> :</td>
                <td> {{ $tanggal_operasional }}</td>
            </tr>
            <tr>
                <td>Tanggal Setor</td>
                <td> :</td>
                <td> {{ $tanggal_setoran }}</td>
            </tr>
        </table>
        
        <p style="padding-left: 13px;">
            Dengan ini saya, Bagus Nugraha telah menerima uang sebesar Rp.{{ number_format($sum_setoran) }} dari saudara Rudi Juwair.
        </p>

        <p style="padding-left: 13px;">
            Note : 
        </p>
        <table cellpadding="10px">
            @foreach ($items as $item)
                <tr>
                    <td>{{ $item['nama_kasir'] }}</td>
                    <td> =</td>
                    <td> Rp.{{ number_format($item['sum_transaksi']) }}</td>
                </tr>
            @endforeach
        </table>

        <table width="100%" style="padding-top: 200px; padding-left: 250px;">
            <tr>
                <td style="padding-left: 40px;">Penyetor</td>
                <td style="padding-left: 250px;">Saksi</td>
                <td style="padding-left: 50px;">Penerima</td>
            </tr>
            <tr>
                <td colspan=3 style="padding-bottom: 180px;">&nbsp;</td>
            </tr>
            <tr>
                <td>( Rudi Juwair )</td>
                <td style="padding-left: 200px;">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fitri &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
                <td>( Bagus Nugraha )</td>
            </tr>
        </table>

    </section>
</body>
</html>
