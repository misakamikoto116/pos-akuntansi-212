@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">{{ $title }}</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">{{ $title }}</a></li>
            </ol>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">{{ $title }}</h5>
                </div>
                <div class="card-body">
                    @if(empty($items))
                        <div class="alert alert-warning"> Tidak ada data. </div>
                    @else
                    <table class="table">
                        <thead>
                            <th>No</th>
                            <th>No Barang</th>
                            <th>Nama Barang</th>
                            <th>Satuan</th>
                            <th>Jumlah</th>
                            <th>Harga Per Unit</th>
                            <th>Harga Modal Terakhir</th>
                            <th>Diskon</th>
                        </thead>

                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->produk->no_barang }}</td>
                                    <td>{{ $item->item_deskripsi }}</td>
                                    <td>{{ $item->item_unit }}</td>
                                    <td>{{ $item->jumlah }}</td>
                                    <td>{{ number_format($item->unit_price) }}</td>
                                    <td>{{ number_format($item->harga_modal) }}</td>
                                    <td>{{ $item->diskon }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>     
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
