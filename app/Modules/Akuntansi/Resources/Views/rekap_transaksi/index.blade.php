@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
    <div class="row">
        @include('chichi_theme.layout.filter')
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">Transaksi</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Transaksi</a></li>
            </ol>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">Transaksi</h5>
                    <div class="menu-header">
                        <a href="#" data-toggle="modal" data-target="#cetak" class="btn btn-default btn-rounded waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-print"></i></span>
                                Cetak Setoran Pemasukan
                        </a>
                    </div>
                </div>
                <div class="card-body">

                    @if(empty($items))
                        <div class="alert alert-warning"> Tidak ada data. </div>
                    @else
                    <table class="table">
                        <thead>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th style="text-align: right">Total Transaksi</th>
                            <th style="text-align: right">Harga Modal</th>
                            <th style="text-align: right">Laba</th>
                            <th style="text-align: center;">Option</th>
                        </thead>

                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item['tanggal'] }}</td>
                                    <td style="text-align: right">{{ number_format($item['sum_transaksi']) }}</td>
                                    <td style="text-align: right">{{ number_format($item['harga_modal']) }}</td>
                                    <td style="text-align: right">{{ number_format($item['laba']) }}</td>
                                    <td>
                                        <center>
                                            <a href="{{ route('akuntansi.penjualan.rekap.detail-transaksi', $item['tanggal_formatted']) }}" class="btn btn-default waves-effect waves-light">Detail</a>
                                        </center>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>  
                    @endif   
                </div>
            </div>
        </div>
    </div>

    {{-- Modal cetak rekap --}}
    <div class="modal fade" id="cetak" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel" style="text-align: center;">Cetak</h4>
                </div>
                {!! Form::open(['url' => url('akuntansi/penjualan/rekap/kasir'), 'method' => 'get', 'target' => '_blank']) !!}
                <div class="modal-body">
                    <div class="form-group row">
                        {!! Form::label('Tanggal Operasional',null,['class' => 'col-3 col-form-label']) !!}
                        <div class="col-9">
                            {!! Form::date('tanggal_operasional', null, ['class' => 'form-control', 'required']) !!}          
                        </div>
                    </div>

                    <div class="form-group row">
                        {!! Form::label('Tanggal Setoran',null,['class' => 'col-3 col-form-label']) !!}
                        <div class="col-9">
                            {!! Form::date('tanggal_setoran', null, ['class' => 'form-control', 'required']) !!}          
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Lanjutkan</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection
