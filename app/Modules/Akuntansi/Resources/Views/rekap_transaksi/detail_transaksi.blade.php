@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">{{ $title }}</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">{{ $title }}</a></li>
            </ol>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">{{ $title }}</h5>
                </div>
                <div class="card-body">
                    @if(empty($items))
                        <div class="alert alert-warning"> Tidak ada data. </div>
                    @else
                    <table class="table">
                        <thead>
                            <th>No</th>
                            <th>No Transaksi</th>
                            <th>Tanggal</th>
                            <th>Kasir</th>
                            <th style="text-align: right">Total Transaksi</th>
                            <th style="text-align: right">Harga Modal</th>
                            <th style="text-align: right">Laba</th>
                            <th style="text-align: center;">Option</th>
                        </thead>

                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item['no_transaksi'] }}</td>
                                    <td>{{ $item['tanggal'] }}</td>
                                    <td>{{ $item['kasir'] }}</td>
                                    <td style="text-align: right">{{ number_format($item['total_transaksi']) }}</td>
                                    <td style="text-align: right">{{ number_format($item['harga_modal']) }}</td>
                                    <td style="text-align: right">{{ number_format($item['laba']) }}</td>
                                    <td>
                                        <center>
                                            <a href="{{ route('akuntansi.penjualan.rekap.detail-barang', $item['id']) }}" class="btn btn-default waves-effect waves-light">Detail</a>
                                        </center>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>     
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
