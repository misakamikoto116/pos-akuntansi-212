@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Display Iklan POS</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Daftar Iklan</h5>
                    <div class="menu-header">
                        <a href="{{ route('akuntansi.iklan-manage.add.get') }}" class="btn btn-default btn-rounded waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-plus"></i></span>
                            Tambah
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    @if($data->isEmpty())
                        <div class="alert alert-warning"> Tidak ada data. </div>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th width="10%">Foto Iklan</th>
                                <th>Status</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $row)
                                <tr id="{{ $row->id }}">
                                    <td style="width: 5%">{{ $loop->iteration }}</td>
                                    <td><image width="50" src="{!! asset('storage/iklan/thumb_'.$row->image_path) !!}"></image></td>
                                    <td>{{ $row->publish }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item edit" href="{{ route('akuntansi.iklan-manage.edit.get', $row->id) }}"><i class="fa fa-pencil"></i>&emsp;Edit</a>
                                                <a class="dropdown-item delete-iklan" href="#">
                                                    <i class="fa fa-trash"></i>&emsp;Delete
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    @endif
                    </table>
                    <div class="pull-right">
                        {!! $data->links('vendor.pagination.bootstrap-4'); !!}
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('custom_js')
    <script type="text/javascript" src="{{ asset('assets/plugins/parsleyjs/parsley.min.js') }}"></script>

    <script type="text/javascript">
        function deleteIklan(_id) {
            $.ajax({
                method: "DELETE",
                url: "{{ route('akuntansi.iklan-manage.delete') }}",
                dataType: "json",
                data: { iklan_id: _id , _token: "{{ csrf_token() }}"}
            }).done(function (data) {
                var row = $(".table").find('tr#' + data.id);
                row.remove();
            });
        }
        $('.delete-iklan').on('click', function(e) {
            e.preventDefault();
            var id = $(this).closest('tr').attr('id');
            swal({
                title: "Hapus ?",
                text: "Apakah anda yakin untuk menghapus data?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                if (willDelete) {
                    deleteIklan(id)
                } else {
                    swal("Data batal dihapus", {
                icon: "success",
            });
        }
        });
        });

    </script>


@endsection