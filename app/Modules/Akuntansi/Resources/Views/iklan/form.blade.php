<div class="form-group row">
	{!! Form::label('nama_iklan','Nama Iklan',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::text('nama_iklan',null,['class' => 'form-control','id' => 'nama_iklan', 'required'])!!}
</div>
</div>

<div class="form-group row">
	<label class="col-2 col-form-label">Image Iklan</label>
	<div class="col-10">
		<input name="image_iklan" required type="file" class="filestyle form-control" data-iconname="fa fa-cloud-upload">
	</div>
</div>

<div class="form-group row">
	<label for="keterangan" class="col-2 col-form-label">Deskripsi Iklan</label>
	<div class="col-10">
		<textarea id="keterangan" name="keterangan" required class="form-control"></textarea>
	</div>
</div>

<div class="form-group row">
	<label class="col-2 col-form-label">Aktif/Non Aktif</label>
	<div class="col-10">
		<div class="radio radio-info form-check-inline" style="margin-top: 5px;margin-right: 20px;">
			<input type="radio" id="inlineRadio1" value="1" name="publish" checked="">
			<label style="margin-right: 10px" for="inlineRadio1">Aktif </label>
			<input type="radio" id="inlineRadio2" value="0" name="publish">
			<label for="inlineRadio2"> Tidak Aktif </label>
		</div>
	</div>
</div>

