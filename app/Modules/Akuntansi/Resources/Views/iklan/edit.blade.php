@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title_document}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ url('akuntansi/display-iklan') }}">Daftar Display Iklan</a></li>
                      <li><a href="#">Update Display Iklan</a></li>
                    </ol>

                </div>
            </div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::open(['url' => route('akuntansi.iklan-manage.edit.post', $data->id), 'files' => true ,'method' => 'POST', 'class' => 'form form-horizontal form-label-left']) !!}
                    <div class="form-group row">
                        {!! Form::label('nama_iklan','Nama Iklan',['class' => 'col-2 col-form-label']) !!}
                        <div class="col-10">
                            {!! Form::text('nama_iklan',$data->nama_iklan,['class' => 'form-control','id' => 'nama_iklan', 'required'])!!}
                        </div>
                    </div>

                    <div class="form-group row">
                        {!! Form::hidden('last_image', $data->image_path) !!}
                        <label class="col-2 col-form-label">Image Iklan</label>
                        <div class="col-10">
                            <input name="image_iklan" type="file" class="filestyle form-control" data-iconname="fa fa-cloud-upload">
                            <img src="{!! asset('storage/iklan/thumb_'.$data->image_path) !!}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="keterangan" class="col-2 col-form-label">Deskripsi Iklan</label>
                        <div class="col-10">
                            {!! Form::textarea('keterangan',$data->deskripsi,['class' => 'form-control','id' => 'keterangan', 'required'])!!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Aktif/Non Aktif</label>
                        <div class="col-10">
                            <div class="radio radio-info form-check-inline" style="margin-top: 5px;margin-right: 20px;">
                                <input type="radio" id="inlineRadio1" value="1" name="publish" checked>
                                <label style="margin-right: 10px" for="inlineRadio1">Aktif </label>
                                <input type="radio" id="inlineRadio2" value="0" name="publish">
                                <label for="inlineRadio2"> Tidak Aktif </label>
                            </div>
                        </div>
                    </div>

                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')
    <script type="text/javascript" src="{{ asset('assets/plugins/parsleyjs/parsley.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('form').parsley();
        });
    </script>


@endsection