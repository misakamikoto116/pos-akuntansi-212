@extends( $permission['daftar'] ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
    <div class="row">
        @include('chichi_theme.layout.filter')
        <div class="col-sm-12">
            <h4 class="page-title"></h4>
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Penyesuaian Persediaan</a></li>
            </ol>
        </div>
    </div>   

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">Penyesuaian Persediaan</h5>
                    @can($permission['buat'])
                        <div class="menu-header">
                            <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                                <span class="btn-label"><i class="fa fa-plus"></i></span>
                                Tambah
                            </a>    
                        </div>
                    @endcan
                </div>
                <div class="card-body">
                    <table class="table">
                        @if($items->isEmpty())
                            <div class="alert alert-warning"> Tidak ada data. </div>
                        @else
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Nomor Penyesuaian</th>
                                <th>Tipe</th>
                                <th>Keterangan</th>
                                <th>Total Nilai</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $key => $item)
                            <tr>
                                <td>{{ $key + $items->firstItem() }}</td>
                                <td>{{ \Helpers\IndonesiaDate::indonesiaDate($item->tgl_penyesuaian) }}</td>
                                <td>{{ $item->no_penyesuaian }}</td>
                                <td>{!! $item->tipe_penyesuaian_formatted !!}</td>
                                <td>{{ $item->keterangan }}</td>
                                <td>{{ number_format($item->detailPenyesuaian->sum('nilai_baru')) }}</td>
                                <td>
                                    @if($permission['lihat'] || $permission['buat'] || $permission['hapus'])
                                        {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                                <div class="dropdown-menu">
                                                    @can($permission['ubah'])
                                                        <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i> Edit</a>
                                                    @endcan
                                                    @can($permission['hapus'])
                                                        <a class="dropdown-item deleteBtn" href="#">
                                                            <i class="fa fa-trash"></i>&emsp;Delete
                                                        </a>
                                                    @endcan
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    @endif
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                        @endif
                    </table>
                    <div class="pull-right">
                        {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                    </div>
                </div>   
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
@endsection

@section('custom_js')

@endsection
