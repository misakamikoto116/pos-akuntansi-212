<tr>
    <td>
        {!! Form::text('no_produk['.($row ?? 0).']', null,['placeholder' => 'Cari Barang','class' => 'form-control no_produk', 'readonly', 'required'])!!}
        {!! Form::hidden('produk_id['.($row ?? 0).']', $poduk ?? null,['class' => 'form-control produk_id', 'required'])!!}
    </td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '','readonly'])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => ''])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('qty_baru[]',null,['class' => 'form-control qty_baru','id' => ''])!!}</td>
    <td class="kol_nilai_skrg">{!! Form::text('nilai_skrg[]',null,['class' => 'form-control nilai_skrg','id' => '','readonly'])!!}</td>
    <td class="kol_nilai_baru">{!! Form::text('nilai_baru[]',null,['class' => 'form-control nilai_baru','id' => ''])!!}</td>
    <td>{!! Form::text('departemen[]',null,['class' => 'form-control departemen','id' => ''])!!}</td>
    <td>{!! Form::select('gudang_id[]',$gudang, null,['class' => 'form-control select2 gudang_id']) !!}</td>
    <td>{!! Form::text('proyek[]',null,['class' => 'form-control proyek','id' => ''])!!}</td>
    <td>{!! Form::text('serial_number[]',null,['class' => 'form-control serial_number','id' => ''])!!}
        {!! Form::hidden('produk_detail_id[]', null, ['class' => 'produk_detail_id']) !!}
    </td>
    <td><button href="" class="remove btn btn-danger remove-rincian-penyesuaian-persediaan" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>