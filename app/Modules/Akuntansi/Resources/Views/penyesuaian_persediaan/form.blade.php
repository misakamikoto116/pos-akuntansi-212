<div class="form-group row">
  <div class="col-2 col-form-label">No. Penyesuaian</div>
  <div class="col-3">
    {!! Form::text('no_penyesuaian',null,['class'=>'form-control', 'id' => 'no_penyesuaian']) !!}
  </div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Tgl. Penyesuaian</div>
  <div class="col-3">
    {!! Form::text('tgl_penyesuaian',null,['class'=>'form-control tanggal','id' => 'tgl_penyesuaian'])!!}
  </div>
</div>
<hr>
<div class="form-group row">
  <div class="col-2 col-form-label">Akun Penyesuaian</div>
  <div class="col-3">
    {!! Form::text('akun_penyesuaian_nama',!empty($item) ? $item->akun->kode_akun : null,['class'=>'form-control','id' => 'kode_akun_id', 'readonly'])!!}
  </div>
  <div class="col-3">
    {!! Form::select('akun_penyesuaian',$akun, null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'akun_penyesuaian', 'data-tags'=>'true'])!!}
  </div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Keterangan</div>
  <div class="col-3">
    {!! Form::textarea('keterangan',null,['class'=>'form-control','id' => 'keterangan'])!!}
  </div>
</div>
 <div class="form-group row">
  <div class="col-2"></div>
  <div class="col-3">
    <label for="">
      @if (!empty($item))
        @if ($item->value_penyesuaian == 1)
          {!! Form::checkbox('',null,  true,['class' => 'form-control', 'id' => 'check_isi','disabled','readonly']) !!}
          {!! Form::hidden('value_penyesuaian',1) !!}
        @else
          {!! Form::checkbox('value_penyesuaian',1, null,['class' => 'form-control', 'id' => 'check_isi','disabled','readonly']) !!}
        @endif
      @else
        {!! Form::checkbox('value_penyesuaian',1, null,['class' => 'form-control', 'id' => 'check_isi']) !!}
      @endif
      Value Penyesuaian
    </label>
  </div>
</div> 
<hr>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>

<div class="penyesuaian-persediaan-sections" data-id="1">
<div class="duplicate-penyesuaian-persediaan-sections">
<div class="tab-content" id="pills-tabContent" style="overflow:auto; white-space: nowrap;">
  <div class="tab-pane fade show active" style="overflow:auto; white-space: nowrap;" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab">
    <table width="100%" class="table" style="overflow:auto; white-space: nowrap;">
      <thead class="thead-light" style="border-collapse: collapse;">
        <tr>
          <th width="12%">Item No</th>
          <th>Keterangan</th>
          <th>Satuan</th>
          @if (isset($item))
            <th>Qty Diff</th>
          @else
            <th>Jumlah Sekarang</th>
            <th>Jumlah Baru</th>
          @endif
          <th class="kol_nilai_skrg">Nilai Sekarang</th>
          <th class="kol_nilai_baru">Nilai Baru</th>
          <th>Departemen</th>
          <th>Gudang</th>
          <th>Proyek</th>
          <th>Serial Number</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="penyesuaianpersediaan">
      @if(isset($item))
        @if(old('produk_id') !== null)  
						@foreach(old('produk_id') as $row => $produk_id )
							@include('akuntansi::penyesuaian_persediaan/components/item_penyesuaian_persediaan',[
								'row'               => $row, 
                'produk_id'         => $produk_id,
                'no_produk'         => old('no_produk')[$row],
								'keterangan_produk' => old('keterangan_produk')[$row],
								'qty_produk'        => old('qty_produk')[$row],
								'satuan_produk'     => old('satuan_produk')[$row],
                'qty_baru'          => old('qty_baru')[$row],
                'nilai_skrg'        => old('nilai_skrg')[$row],
                'nilai_baru'        => old('nilai_baru')[$row],
                'departemen'        => old('departemen')[$row],
                'gudang_id'         => old('gudang_id')[$row],
                'proyek'            => old('proyek')[$row],
                'serial_number'     => old('serial_number')[$row],
                'produk_detail_id'  => old('produk_detail_id')[$row],
							])
						@endforeach      
				@else
          @foreach($item->detailpenyesuaian as $key => $data)
            <tr>
              <td>
                {!! Form::text('no_produk[]', $data->produk->no_barang, ['placeholder' => 'Cari Barang','class' => 'form-control no_produk','id' => 'produk_id' . $key,'onclick' => 'awas('. $key .')' ,'required', 'readonly'])!!}
                {!! Form::hidden('produk_id[]', $data->produk_id, ['class' => 'form-control produk_id','id' => 'produk_id'.$key, 'required'])!!}
              </td>
              <td>{!! Form::text('keterangan_produk[]',$data->keterangan_produk,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'. $key,'readonly'])!!}</td>
              <td>{!! Form::text('satuan_produk[]',$data->satuan_produk,['class' => 'form-control satuan_produk','id' => 'satuan_produk' . $key])!!}</td>
              {{-- <td>{!! Form::text('qty_produk[]',$data->qty_produk,['class' => 'form-control qty_produk','id' => '',''])!!}</td>
              <td>{!! Form::text('qty_baru[]',$data->qty_baru,['class' => 'form-control qty_baru','id' => ''])!!}</td> --}}
              <td>{!! Form::text('qty_diff[]',$data->qty_diff,['class' => 'form-control qty_diff','id' => 'qty_diff' . $key])!!}</td>
              <td class="kol_nilai_skrg">{!! Form::text('nilai_skrg[]',$data->nilai_skrg,['class' => 'form-control nilai_skrg mask_nilai','id' => 'nilai_skrg' . $key,'readonly'])!!}</td>
              <td class="kol_nilai_baru">{!! Form::text('nilai_baru[]',$data->nilai_baru,['class' => 'form-control nilai_baru mask_nilai','id' => 'nilai_baru' . $key])!!}</td>
              <td>{!! Form::text('departemen[]',$data->departemen,['class' => 'form-control departemen','id' => 'departemen' . $key])!!}</td>
              <td>{!! Form::select('gudang_id[]',$gudang, $data->gudang_id,['class' => 'form-control select2 gudang_id','id' => 'gudang_id' . $key])!!}</td>
              <td>{!! Form::text('proyek[]',$data->proyek,['class' => 'form-control proyek','id' => 'proyek' . $key])!!}</td>
              <td>{!! Form::text('serial_number[]',$data->serial_number,['class' => 'form-control serial_number','id' => 'serial_number' . $key])!!}
                  {!! Form::hidden('produk_detail_id[]', $data->produk_detail->id, ['class' => 'produk_detail_id','id' => 'produk_detail_id' . $key]) !!}
              </td>
              <td><button href="" class="remove btn btn-danger remove-rincian-serial-barang" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
            </tr>
          @endforeach
        @endif
			@elseif(old('produk_id') !== null)  
        @foreach(old('produk_id') as $row => $produk_id )
          @include('akuntansi::penyesuaian_persediaan/components/item_penyesuaian_persediaan',[
            'row'               => $row, 
            'produk_id'         => $produk_id,
            'no_produk'         => old('no_produk')[$row],
            'keterangan_produk' => old('keterangan_produk')[$row],
            'qty_produk'        => old('qty_produk')[$row],
            'satuan_produk'     => old('satuan_produk')[$row],
            'qty_baru'          => old('qty_baru')[$row],
            'nilai_skrg'        => old('nilai_skrg')[$row],
            'nilai_baru'        => old('nilai_baru')[$row],
            'departemen'        => old('departemen')[$row],
            'gudang_id'         => old('gudang_id')[$row],
            'proyek'            => old('proyek')[$row],
            'serial_number'     => old('serial_number')[$row],
            'produk_detail_id'  => old('produk_detail_id')[$row],
          ])
        @endforeach      
			@endif
      </tbody>
    </table>
    <a class="btn btn-info add-penyesuaian-persediaan" ><i class="fa fa-plus"> Tambah</i></a>
</div>
</div>
  </div>
</div>

@include('akuntansi::pesanan_modal/modal_barang')

<script type="text/template" id="table_penyesuaian_barang_section" data-id=""/>
  <tr>
    <td>
        {!! Form::text('no_produk[]', null, ['placeholder' => 'Cari Barang','class' => 'form-control no_produk', 'readonly', 'required'])!!}
        {!! Form::hidden('produk_id[]', null, ['class' => 'form-control produk_id', 'required'])!!}
    </td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '','readonly'])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => ''])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '','readonly'])!!}</td>
    <td>{!! Form::text('qty_baru[]',null,['class' => 'form-control qty_baru','id' => ''])!!}</td>
    <td class="kol_nilai_skrg">{!! Form::text('nilai_skrg[]',null,['class' => 'form-control nilai_skrg','id' => '','readonly'])!!}</td>
    <td class="kol_nilai_baru">{!! Form::text('nilai_baru[]',null,['class' => 'form-control nilai_baru','id' => ''])!!}</td>
    <td>{!! Form::text('departemen[]',null,['class' => 'form-control departemen','id' => ''])!!}</td>
    <td>{!! Form::select('gudang_id[]',$gudang, null,['class' => 'form-control select2 gudang_id']) !!}</td>
    <td>{!! Form::text('proyek[]',null,['class' => 'form-control proyek','id' => ''])!!}</td>
    <td>{!! Form::text('serial_number[]',null,['class' => 'form-control serial_number','id' => ''])!!}
        {!! Form::hidden('produk_detail_id[]', null, ['class' => 'produk_detail_id']) !!}
    </td>
    <td><button href="" class="remove btn btn-danger remove-rincian-penyesuaian-persediaan" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
	</tr>
</script>

<script type="text/template" id="table_penyesuaian_barang_section_edit" data-id=""/>
  <tr>
    <td>
      {!! Form::text('no_produk[]', null, ['placeholder' => 'Cari Barang','class' => 'form-control no_produk', 'readonly', 'required'])!!}
      {!! Form::hidden('produk_id[]', null, ['class' => 'form-control produk_id', 'required'])!!}
    </td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '','readonly'])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => ''])!!}</td>
    <td>{!! Form::text('qty_diff[]',null,['class' => 'form-control qty_diff','id' => ''])!!}</td>
    <td class="kol_nilai_skrg">{!! Form::text('nilai_skrg[]',null,['class' => 'form-control nilai_skrg','id' => '','readonly'])!!}</td>
    <td class="kol_nilai_baru">{!! Form::text('nilai_baru[]',null,['class' => 'form-control nilai_baru','id' => ''])!!}</td>
    <td>{!! Form::text('departemen[]',null,['class' => 'form-control departemen','id' => ''])!!}</td>
    <td>{!! Form::select('gudang_id[]',$gudang, null,['class' => 'form-control select2 gudang_id']) !!}</td>
    <td>{!! Form::text('proyek[]',null,['class' => 'form-control proyek','id' => ''])!!}</td>
    <td>{!! Form::text('serial_number[]',null,['class' => 'form-control serial_number','id' => ''])!!}
        {!! Form::hidden('produk_detail_id[]', null, ['class' => 'produk_detail_id']) !!}
    </td>
    <td><button href="" class="remove btn btn-danger remove-rincian-penyesuaian-persediaan" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>