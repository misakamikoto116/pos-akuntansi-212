<div class="form-group row">
  <div class="col-2 col-form-label">NIK Karyawan</div>
  	<div class="col-10">
  		{!! Form::text('nik_karyawan',null,['placeholder' => 'NIK Karyawan','class' => 'form-control','id' => 'nik_karyawan'])!!}
  	</div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">NIP Karyawan</div>
  	<div class="col-10">
  		{!! Form::text('nip_karyawan',null,['placeholder' => 'NIP Karyawan','class' => 'form-control','id' => 'nip_karyawan'])!!}
  	</div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Nama Karyawan</div>
  	<div class="col-10">
  		{!! Form::text('nama_karyawan',null,['placeholder' => 'Nama Karyawan','class' => 'form-control','id' => 'nama_karyawan'])!!}
  	</div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Jenis Kelamin</div>
  	<div class="col-10">
  		{!! Form::select('jenis_kelamin',['1' => 'Laki-Laki', '2' => 'Perempuan'],null,['placeholder' => '&ndash; Pilih Jenis Kelamin &ndash;','class' => 'form-control select2','id' => 'jenis_kelamin'])!!}
  	</div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Tempat Lahir</div>
  	<div class="col-10">
  		{!! Form::text('tempat_lahir',null,['placeholder' => 'Tempat Lahir','class' => 'form-control','id' => 'tempat_lahir'])!!}
  	</div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Tanggal Lahir</div>
  	<div class="col-10">
        @if (isset($item))
            {!! Form::text('tgl_lahir',null,['placeholder'=>'Tanggal - Bulan - Tahun','class' => 'form-control tgl-lhr','id' => 'tgl_lahir_up'])!!}
        @else
            {!! Form::text('tgl_lahir',null,['placeholder'=>'Tanggal - Bulan - Tahun','class' => 'form-control tgl-lhr','id' => 'tgl_lhr'])!!}
        @endif
  	</div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Alamat</div>
  	<div class="col-10">
      {!! Form::textarea('alamat_karyawan',null,['placeholder' => 'Alamat Karyawan', 'class' => 'form-control','id' => 'alamat_karyawan'])!!}
  	</div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">No. Telpon</div>
  	<div class="col-10">
      {!! Form::text('no_telp',null,['class' => 'form-control','id' => 'no_telp', 'placeholder'=>'Nomor Telpon'])!!}
  	</div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Jabatan</div>
  	<div class="col-10">
      {!! Form::select('jabatan',['1' => 'Atas', '2' => 'Bawah'],null,['class' => 'form-control select2','id' => 'jabatan', 'placeholder'=>'&nbsp; Jabatan &nbsp;'])!!}
  	</div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Status</div>
  	<div class="col-10">
      {!! Form::select('status',['1' => 'Menikah', '2' => 'Belum Menikah'],null,['class' => 'form-control select2','id' => 'status', 'placeholder'=>'&nbsp; Status &nbsp;'])!!}
  	</div>
</div>