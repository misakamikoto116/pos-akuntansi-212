@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection
@php
    $title = 'Data Karyawan'
@endphp
@section('title')
    {{$title}}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">{{$title}}</a></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5 style="color:white;">{{$title}}</h5>
                    <div class="menu-header">
                        <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-plus"></i></span>
                            Tambah
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table">
                                @if($items->isEmpty())
                                    <div class="alert alert-warning"> Tidak ada data. </div>
                                @else
                                    <thead>
                                        <tr>
                                            <th width="20%">No</th>
                                            <th width="20%">Nama</th>
                                            <th width="20%">Alamat</th>
                                            <th width="20%">No. Telpon</th>
                                            <th width="20%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($items as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->nama_karyawan }}</td>
                                                <td>{{ $item->alamat_karyawan }}</td>
                                                <td>{{ $item->no_telp }}</td>
                                                <td>
                                                    {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                                            <div class="dropdown-menu">
                                                            <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i>&emsp;Edit</a>
                                                                <a href="#" class="dropdown-item deleteBtn"><i class="fa fa-trash">&emsp;Delete</i></a>
                                                            </div>
                                                        </div>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                @endif
                            </table>
                            <div class="pull-right">
                                {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection