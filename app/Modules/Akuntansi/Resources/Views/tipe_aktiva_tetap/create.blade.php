@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ url('akuntansi/tipe-aktiva-tetap') }}">Daftar Tipe Aktiva Tetap</a></li>
                      <li><a href="#">Tambah Tipe Aktiva Tetap</a></li>
                    </ol>

                </div>
            </div>
          {{--   @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Informasi</strong>
                    <ul class="list-unstyled">
                        @foreach($errors->all() as $error)
                            <li class="">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
 --}}
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left']) !!}

                @include($form)
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')
<script type="text/javascript">
    // ajax get kode_akun
$( ".target" ).change(function() {
            var tipe_aktiva_tetap_pajak_id   = $('#tipe_aktiva_tetap_pajak_id').val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-aktiva-tetap') }}",
                data: {id : tipe_aktiva_tetap_pajak_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                    $('#tarif_penyusutan').val(response.tarif_penyusutan);
                    $('#estimasi_umur').val(response.estimasi_umur);
                    $('#metode_penyusutan').val(response.metode);
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        });

</script>
@endsection