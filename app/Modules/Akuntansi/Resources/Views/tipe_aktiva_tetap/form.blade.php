<div class="form-group row">
	{!! Form::label('tipe_aktiva_tetap','Tipe Aktiva Tetap',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::text('tipe_aktiva_tetap',null,['class' => 'form-control','id' => 'tipe_aktiva_tetap'])!!}
</div>
</div>
<div class="form-group row">
	{!! Form::label('tipe_aktiva_tetap_pajak','Tipe Aktiva Tetap Pajak',['class' => 'col-2 col-form-label']) !!}
	<div class="col-md-10">
	{!! Form::select('tipe_aktiva_tetap_pajak_id',$listTipeAktivaTetapPajak,null,['placeholder' => '- Pilih -','class' => 'form-control target select2','id' => 'tipe_aktiva_tetap_pajak_id'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('metode_penyusutan','Metode Penyusutan Pajak',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::text('metode_penyusutan',null,['class' => 'form-control','id' => 'metode_penyusutan','readonly'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('estimasi_umur','Task. Umur Ekonomis Pjk',['class' => 'col-2 col-form-label']) !!}
	<div class="col-2">
	{!! Form::number('estimasi_umur',null,['class' => 'form-control','id' => 'estimasi_umur','readonly'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('tarif_penyusutan','Tarif Penyusutan Pajak',['class' => 'col-2 col-form-label']) !!}
	<div class="col-2">
	{!! Form::number('tarif_penyusutan',null,['class' => 'form-control','id' => 'tarif_penyusutan','readonly'])!!}
	</div>
</div>
