@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                @include('chichi_theme.layout.filter')
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Detail Barang</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ route($module_url->index) }}">Daftar Barang</a></li>
                      <li><a href="#">Detail Barang</a></li>
                    </ol>

                </div>
            </div>
<!-- END Page-Title -->

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Detail Barang</h5>
            <div class="menu-header">
            
            </div>
            </div>
            <div class="card-body">
                {{-- @if($items->isEmpty())
                    <div class="alert alert-warning"> Tidak ada data. </div>
                @else --}}
                   <div style="float: right; padding-right: 37px; padding-bottom: 10px;">
                        <a href="#" data-toggle="modal" data-target="#cetak" class="btn btn-primary btn-rounded waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-print"></i></span>
                            Cetak
                        </a>
                   </div>
                   <table class="table">
                    <tr>
                        <th>No</th>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Invoice No</th>
                        <th>Price (Base)</th>
                        <th>In</th>
                        <th>Out</th>
                        <th>Quantity</th>
                    </tr>
                    @php $i = 0; @endphp
                    @foreach($items as $barang)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>
                            @if ($barang['tanggal'] != '0000-00-00')
                                {{ date('d-m-Y | H:i:s', strtotime($barang['tanggal'])) }}
                            @endif
                        </td>
                        <td>{{ $barang['keterangan'] }}</td>
                        <td>{{ $barang['invoice_no'] }}</td>
                        <td>{{ number_format($barang['harga_modal']) }}</td>
                        <td>
                            @if($barang['status'] == 'in')
                                {{ $barang['jumlah'] }}
                            @endif
                        </td>
                        <td>
                            @if($barang['status'] == 'out')
                                {{ $barang['jumlah'] }}
                            @endif
                        </td>
                        <td>
                            @if($i == 0)
                                {{ number_format($barang['jumlah']) }}
                                @php $total_barang = $barang['jumlah'] @endphp
                            @else
                                @if($barang['status'] == 'in')
                                    @php $total_barang += $barang['jumlah'] @endphp 
                                @else 
                                    @php $total_barang -= $barang['jumlah'] @endphp 
                                @endif
                                {{ number_format($total_barang) }}

                            @endif
                        </td>
                    </tr>
                    @php $i++ @endphp
                    @endforeach
                </table>
                <div class="pull-right">
                    {{-- {!! $items->links('vendor.pagination.bootstrap-4'); !!} --}}
                </div>
            </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>

    <div class="modal fade" id="cetak" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel" style="text-align: center;">Cetak</h4>
                </div>
                {!! Form::open(['url' => url('akuntansi/laporan/barang/daftar-barang'), 'method' => 'get', 'target' => '_blank']) !!}
                <div class="modal-body">
                Tanggal :
                <hr>
                    <div class="form-group row">
                        {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                        <div class="col-9">
                            {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                        <div class="col-9">
                            {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                        </div>
                    </div>
                <hr>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Lanjutkan</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection
@section('custom_js')
    <script type="text/javascript">
        $('.mask').autoNumeric('init');
    </script>
@endsection