@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                @include('chichi_theme.layout.filter')
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Barang</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Daftar Barang</a></li>
                    </ol>

                </div>
            </div>
<!-- END Page-Title -->

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Daftar Barang</h5>
                @can('buat_barang')
                    <div class="menu-header">
                        <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-plus"></i></span>
                            Tambah
                        </a>
                    </div>
                @endcan
            </div>
            <div class="card-body">
                {{-- @if($items->isEmpty())
                    <div class="alert alert-warning"> Tidak ada data. </div>
                @else --}}
                    @can('laporan_barang')
                        <div style="float: right; padding-right: 37px; padding-bottom: 10px;">
                            <a href="#" data-toggle="modal" data-target="#cetak" class="btn btn-primary btn-rounded waves-effect waves-light">
                                <span class="btn-label"><i class="fa fa-print"></i></span>
                                Cetak
                            </a>
                       </div>
                    @endcan
                   <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>No Barang</th>
                            <th>Barcode</th>
                            <th>Keterangan</th>
                            <th>Nama Singkat</th>
                            <th>Kuantitas</th>
                            <th>Satuan</th>
                            <th>Harga Satuan</th>
                            <th>Harga Rata-rata</th>
                            <th>Harga Terakhir</th>
                            {{-- <th>Kategori</th> --}}
                            <th>Tipe</th>
                            {{-- <th>Tipe Persediaan</th> --}}
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $key => $item)
                            <tr>
                                <td>{{ $key + $items->firstItem() }}</td>
                                <td>{{ $item->no_barang}}</td>
                                <td>{{ $item->barcode}}</td>
                                <td class="td-nama-barang">{{ $item->keterangan}}</td>
                                <td>{{ $item->nama_singkat}}</td>
                                <td>{!! $item->updated_qty !!}</td>
                                <td>{{ $item->unit}}</td>
                                <td class="mask td-harga-jual">{{ $item->harga_jual}}</td>
                                <td class="td-harga-modal">{!! !empty($item->saldoAwalBarang->last()->harga_modal) ? number_format($item->saldoAwalBarang->last()->harga_modal) : 0 !!}</td>
                                <td>{!! !empty($item->saldoAwalBarang->last()->harga_terakhir) ? number_format($item->saldoAwalBarang->last()->harga_terakhir) : 0 !!}</td>
                                {{-- <td>{{ $item->kategoriProduk->nama ?? null }}</td> --}}
                                <td>{!! $item->tipe_formatted !!}</td>
                                {{-- <td>{!! $item->tipe_persediaan_formatted !!}</td> --}}
                                <td>
                                    {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                            <div class="dropdown-menu">
                                                @if ( auth()->user()->hasPermissionTo('ubah_barang') || auth()->user()->hasPermissionTo('hapus_barang') )
                                                    @can('ubah_barang')
                                                        <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i>&emsp;Edit</a>
                                                    @endcan
                                                    @can('hapus_barang')
                                                        <a class="dropdown-item deleteBtn" href="#">
                                                            <i class="fa fa-trash"></i>&emsp;Delete
                                                        </a>
                                                    @endcan
                                                @endif
                                                <a class="btn btn-default dropdown-item show" href="{{ route($module_url->show, $item->id) }}">
                                                    <i class="fa fa-history"></i>&emsp;History
                                                </a>
                                                <a class="dropdown-item btn btn-info" href="#" onclick="marginHarga(this, {{ $item->id }})" data-toggle="modal" data-target="#modal-margin" style="margin-left: 5px;">
                                                    <i class="fa fa-money"></i>&emsp;Margin
                                                </a>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                </div>
            </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>

    <div class="modal fade" id="cetak" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel" style="text-align: center;">Cetak</h4>
                </div>
                {!! Form::open(['url' => url('akuntansi/laporan/barang/daftar-barang'), 'method' => 'get', 'target' => '_blank']) !!}
                <div class="modal-body">
                Tanggal :
                <hr>
                    <div class="form-group row">
                        {!! Form::label('Tanggal',null,['class' => 'col-3 col-form-label']) !!}
                        <div class="col-9">
                            {!! Form::date('one-date', null, ['class' => 'form-control','onchange' => '']) !!}          
                        </div>
                    </div>
                <hr>
                Gudang :
                <hr>
                    <div class="form-group row">
                        {!! Form::label('Gudang',null,['class' => 'col-3 col-form-label']) !!}
                        <div class="col-9">
                            {!! Form::select('gudang_id', $gudang, null,['placeholder' => '- Pilih -', 'class' => 'form-control select2']) !!}          
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Lanjutkan</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{-- Modal Margin --}}
        <div class="modal fade" id="modal-margin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" id="#harga-section">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Margin Harga Jual - <span id="nama-barang"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                {!! Form::open(['route' => 'akuntansi.barang.margin-harga-jual', 'files' => true ,'method' => 'POST', 'class' => 'form form-horizontal form-label-left form-submit-barang']) !!}
                <div class="form-group row">
                    <div class="col-3 col-form-label">Harga Modal</div>
                    <div class="col-9">
                        {!! Form::label('harga_modal', 0,['class' => 'col-10 col-form-label harga-modal','style' => 'text-align: right;']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-3 col-form-label">Margin</div>
                    <div class="col-8">
                        {!! Form::text('margin', 0,['class' => 'form-control','id' => '','onchange' => 'hargaJualMargin(this.value)','style' => 'text-align: right;'])!!}
                    </div>
                    <div class="col-1 col-form-label">%</div>
                </div>
                <div class="form-group row">
                    <div class="col-3 col-form-label">Harga Jual</div>
                    <div class="col-8">
                        {!! Form::text('harga_jual', 0,['class' => 'form-control harga-jual-margin','style' => 'text-align: right;']) !!}
                    </div>
                </div>
                {{ Form::hidden('id_barang', null,['id' => 'id-barang']) }}
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
    {{--  --}}

@endsection
@section('custom_js')
    <script type="text/javascript">

        {{-- set harga modal --}}

        function marginHarga(el, id) {
            var harga_modal_val = $(el).closest('tr').find('td.td-harga-modal').text();
            var harga_jual_val  = $(el).closest('tr').find('td.td-harga-jual').text();
            var nama_barang_val = $(el).closest('tr').find('td.td-nama-barang').text();
            $(".harga-modal").text(harga_modal_val);
            $(".harga-jual-margin").val(harga_jual_val);
            $("#id-barang").val(id);
            $("#nama-barang").text(nama_barang_val);
        }

        function changeMaskingToNumber(value) {
              if (value) {
                  if (typeof value == "string") {
                      return parseFloat(value.replace(/\,/gi, '').replace('.00', ''));                        
                  }else{
                      return value;
                  }
              }
        }

        function hargaJualMargin(val) {
            var harga_modal         = changeMaskingToNumber($(".harga-modal").text());
            var harga_jual_margin   = harga_modal + (harga_modal * (val/100));
            $(".harga-jual-margin").autoNumeric('set', harga_jual_margin);
        }

        $(document).ready(function() {
            $('.harga-jual-margin').autoNumeric('init', {
                aPad: false
            });   

        });

        $('.mask').autoNumeric('init',{aPad: false});
    </script>
@endsection