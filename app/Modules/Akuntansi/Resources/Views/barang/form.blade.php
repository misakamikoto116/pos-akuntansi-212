<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3);">
  <li class="nav-item">
    <a class="nav-link active" id="pills-umum-tab" data-toggle="pill" href="#pills-umum" role="tab" aria-controls="pills-umum" aria-selected="true">Umum</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-pp-tab" data-toggle="pill" href="#pills-pp" role="tab" aria-controls="pills-pp" aria-selected="true">Penjualan/Pembelian</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-akun-tab" data-toggle="pill" href="#pills-akun" role="tab" aria-controls="pills-akun" aria-selected="true">Akun-Akun</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-kontrol-tab" data-toggle="pill" href="#pills-kontrol" role="tab" aria-controls="pills-kontrol" aria-selected="true">Kontrol Persediaan</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-lain-tab" data-toggle="pill" href="#pills-lain" role="tab" aria-controls="pills-lain" aria-selected="true">Lain-lain</a>
  </li>
</ul>

<div class="tab-content" id="pills-tabContent">

  <div class="tab-pane fade show active" id="pills-umum" role="tabpanel" aria-labelledby="pills-umum-tab">
  	<div class="row" style="padding: 0px">
  		<div class="col-md-6" style="padding: 0px">
  			<div class="form-group row">
  		<div class="col-2 col-form-label">Tipe Barang</div>
  		<div class="col-10">
  			<div class="col-form-label">
  			<div class="form-check">
					{!! Form::radio('tipe_barang', 0, true, ['class' => 'form-check-input', 'id' => 'persediaan']) !!}
			  <label class="form-check-label" for="tipe-barang1">
			    Persediaan
			  </label>
			</div>

  			<div class="form-check">
					{!! Form::radio('tipe_barang', 1, false, ['class' => 'form-check-input', 'id' => 'nonpersediaan']) !!}
			  <label class="form-check-label" for="tipe-barang2">
			    Non Persediaan
			  </label>
			</div>
			<div class="form-check">
					{!! Form::radio('tipe_barang', 2, false, ['class' => 'form-check-input', 'id' => 'servis']) !!}
			  <label class="form-check-label" for="tipe-barang3">
			    Servis
			  </label>
			</div>
			</div>
			<div class="form-group row">
				<label class="col-12 col-form-label">Foto Barang</label>
				<div class="col-12">
          <input name="foto_produk" type="file" class="filestyle form-control" data-iconname="fa fa-cloud-upload">
				</div>
			</div>
  		</div>
  	</div>
  		</div>
  		<div class="col-md-6" style="padding: 0px">
  			<div class="form-group row">
  				<div class="col-2">Status</div>
  				<div class="col-4">
  					{!! Form::select('status',["0" => "Tidak Aktif","1" => "Aktif"], 1,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'status'])!!}
  				</div>
				</div>
				<div class="form-group row">
					<div class="col-2">Status UMKM</div>
					<div class="col-4">
						{!! Form::select('status_umkm',["0" => "Bukan", "1" => "Benar"], isset($item) ? $item->status_umkm : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'status_umkm'])!!}
					</div>
				</div>
  		</div>
  		<div class="col-md-4" style="padding: 0px">
  			<div class="form-group row">
  				<div class="col-3 col-form-label">No. Barang</div>
  				<div class="col-9">
  					{!! Form::text('no_barang',null,['class' => 'form-control','id' => 'no_barang'])!!}
  				</div>
  			</div>
        <div class="form-group row">
          <div class="col-3 col-form-label">No. Barcode</div>
          <div class="col-9">
            {!! Form::text('barcode',null,['class' => 'form-control','id' => 'barcode'])!!}
          </div>
        </div>
        <div class="form-group row">
          <div class="col-3 col-form-label">Nama Singkat</div>
          <div class="col-9">
            {!! Form::text('nama_singkat',null,['class' => 'form-control','id' => 'nama_singkat'])!!}
          </div>
        </div>
  		</div>
  		<div class="col-md-1" style="padding: 0px">
  			<div class="form-group row">
  				<div class="col-12">
  					<div class="form-check col-form-label" style="float: right;">
					  <input class="form-check-input" type="checkbox" value="" name="subitem" id="subitem" style="margin: 0 10px;">
					  <label class="form-check-label" for="subitem" style="padding: 0px;">
					    Sub Item
					  </label>
					</div>
				</div>
			</div>
  		</div>
		<div id="subitemdiv" class="col-md-7" style="display: none;">
			<div class="form-group row">
			<div class="col-1 col-form-label" style="padding-left: 0px; ">&nbsp;dari</div>
			<div class="col-9" style="padding-left: 0px;">
			{!! Form::select('',[],null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => ''])!!}
		</div>
		</div>
		</div>

		<div class="col-12" style="padding: 0px;">
			<div class="form-group row">
				<div class="col-1 col-form-label">Keterangan</div>
				<div class="col-11">
					{!! Form::textarea('keterangan',null,['class' => 'form-control','id' => '', 'rows' => '3'])!!}
				</div>
			</div>
		</div>
  	</div>

  	<!-- start persediaan -->
  	<div id="persediaandiv">
  	<hr>
  	<fieldset>
  		<legend>Informasi Produksi</legend>
  		<div class="row">
  			<div class="col-sm-5">
  				<div class="form-group row">
		  			<div class="col-3 col-form-label">Tipe Persediaan</div>
		  			<div class="col-8">
		  				{!! Form::select('tipe_persedian',['0'=> 'Bahan Baku','1'=> 'Bahan Baku Pembantu','2'=> 'Barang Setengah Jadi', '3'=> 'Barang Jadi','4'=> 'Barang lain-lain' ],null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => ''])!!}
		  			</div>
		  		</div>
  			</div>
  			<div class="col-sm-6">
  				<div class="form-group row">
  					<div class="col-sm-3 col-form-label">Harga Standar Def.</div>
		  			<div class="col-sm-5">
		  				{!! Form::text('harga_standar_def',null,['class' => 'form-control mask','id' => ''])!!}
		  			</div>
  				</div>
  			</div>
  		</div>
  	</fieldset>

  	<hr>
  	<fieldset>
  		<legend>Informasi Tambahan</legend>
  		<div class="row">
  			<div class="col-md-8">
  				<div class="form-group row">
  					<div class="col-2 col-form-label">Kategori</div>
  					<div class="col-8">
              {!! Form::select('kategori_produk_id',$kategori,null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'kategori_persediaan'])!!}
            </div>
          </div>
          <div class="form-group row">
            <div class="col-2 col-form-label">Gudang Default</div>
            <div class="col-4">
              {!! Form::select('gudang_def_id',$gudang,null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => ''])!!}
  					</div>
  				</div>
  			</div>
  			<div class="col-md-4">
  				<div class="form-group row">
  					<div class="col-4 col-form-label">Dept. Default</div>
  					<div class="col-8">
  						{!! Form::select('',[],null,['class' => 'form-control select2','id' => ''])!!}
  					</div>
  				</div>
  				<div class="form-group row">
  					<div class="col-4 col-form-label">Proyek Default</div>
  					<div class="col-8">
  						{!! Form::select('',[],null,['class' => 'form-control select2','id' => ''])!!}
  					</div>
  				</div>
  			</div>
  		</div>
  	</fieldset>
  	<div class="row">
  		<div class="col-sm-6">
  			<fieldset>
  				<legend>Saldo Awal</legend>
  				<div class="form-group row">
  					<div class="col-2 col-form-label">Kuantitas</div>
  					<div class="col-6">{!! Form::text('kuantitas', !empty($item) ? $item->sum_qty_awal : null,['class' => 'form-control kuantitas-gudang','id' => '', 'style' => 'text-align: right;','onchange' => 'embedToModal()'])!!}</div>
  				</div>
  				<div class="form-group row">
  					<div class="col-2 col-form-label">Unit</div>
  					<div class="col-2">
  						{!! Form::text('unit',null,['class' => 'form-control unit-change','id' => 'unit-change-persediaan'])!!}
  					</div>
  					<div class="col-3"><button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#rincian-unit">Rincian</button></div>

            <div class="unit-sections">
  					<div class="modal fade" id="rincian-unit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog modal-dialog-centered" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Rincian Unit</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					        <div class="form-group row">
					        	<div class="col-2 col-form-label">Unit 2</div>
					        	<div class="col-4">
					        		{!! Form::text('',null,['class' => 'form-control','id' => ''])!!}
					        	</div>
					        	<div class="col-1 col-form-label" align="center">=</div>
					        	<div class="col-4">
					        		{!! Form::text('',null,['class' => 'form-control','id' => ''])!!}
					        	</div>
					        	<div class="col-1 col-form-label" style="padding-left: 0px;">X <span class="unit-modal"></span></div>
					        </div>
					        <div class="form-group row">
					        	<div class="col-2 col-form-label">Unit 3</div>
					        	<div class="col-4">
					        		{!! Form::text('',null,['class' => 'form-control','id' => ''])!!}
					        	</div>
					        	<div class="col-1 col-form-label" align="center">=</div>
					        	<div class="col-4">
					        		{!! Form::text('',null,['class' => 'form-control','id' => ''])!!}
					        	</div>
					        	<div class="col-1 col-form-label" style="padding-left: 0px;">X <span class="unit-modal"></span></div>
					        </div>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-primary save-unit">Save</button>
					      </div>
					    </div>
					  </div>
					</div>
          </div>

  				</div>
  				<div class="form-group row">
  					<div class="col-2 col-form-label">Harga/Unit</div>
  					<div class="col-6">{!! Form::text('harga_unit', '0',['class' => 'form-control saldo-harga-unit mask','id' => 'saldo-harga-unit', 'onchange' => 'embedToModal()', 'style' => 'text-align: right;'])!!}</div>
  				</div>
  				<div class="form-group row">
  					<div class="col-2 col-form-label">Harga Pokok</div>
  					<div class="col-6 col-form-label" style="text-align: right;"><strong class="saldo-gudang-input">0</strong></div>
  				</div>
  			</fieldset>
  			<fieldset>
        <div class="gudang-sections">
  				<div class="form-group row">
  					<div class="col-2 col-form-label">Gudang</div>
  					<div class="col-6">
  						{!! Form::select('gudang_id',$gudang,null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'gudang_id','onchange' => 'embedToModal()'])!!}
  					</div>
  					<div class="col-3"><button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#rincian-gudang">Rincian</button></div>

            {{-- Modal Rincian Gudang --}}
          <div class="duplicate-gudang-sections">
  					<div class="modal fade" id="rincian-gudang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog modal-lg" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Rincian Gudang</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					        <table cellpadding="5px;" class="rincian-gudang" width="100%">
					        	<tr>
					        		<th width="20%">No. Faktur</th>
					        		<th width="23%">Tanggal</th>
					        		<th width="20%">Gudang</th>
					        		<th width="10%">Kuantitas</th>
					        		<th width="20%">Biaya</th>
					        		<th width="10%">SN</th>
					        		<th></th>
					        	</tr>
					        	<tbody class="purchaseGudang"></tbody>
					        </table>
					        <div><button type="button" class="add-gudang btn btn-primary">+</button></div>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-primary simpan-gudang">Save</button>
					      </div>
					    </div>
					  </div>
					</div>
        </div>
  				</div>
  				<div class="form-group row">
  					<div class="col-2 col-form-label">Per Tanggal</div>
  					<div class="col-6"><input class="form-control tanggal" type="" onchange="embedToModal()" name=""></div>
  				</div>
        </div>
  			</fieldset>

        <script id="table_gudang_section" type="text/template"/>
          <tr>
            <td>{!! Form::text('no_faktur_gudang[]',null,['class' => 'form-control no_faktur_gudang','id' => ''])!!}</td>
            <td>{!! Form::text('tanggal_gudang[]',null,['class' => 'form-control date-saldo tanggal_gudang','id' => ''])!!}</td>
            <td>
              {!! Form::select('multi_gudang_id[]',$gudang,null,['class' => 'form-control select2 gudang','id' => ''])!!}
            </td>
            <td>{!! Form::number('kuantitas_gudang[]', 0,['class' => 'form-control kuantitas_gudang','id' => ''])!!}</td>
            <td>{!! Form::text('biaya_gudang[]', 0,['class' => 'form-control biaya_gudang mask','id' => ''])!!}</td>
            <td>{!! Form::text('sn_gudang[]',null,['class' => 'form-control sn_gudang','id' => ''])!!}</td>
            <td><a href="#" class="remove-rincian-gudang btn btn-danger"><i class="fa fa-close"></i></a></td>
          </tr>
        </script>

  		</div>
  		<div class="offset-2 col-sm-4">
  			<fieldset>
  				<legend>Saldo Saat ini</legend>
  				<div class="form-group row">
  					<div class="col-4 col-form-label">Kuantitas</div>
  					<div class="col-6 col-form-label"><strong>0</strong></div>
  				</div>
  				<div class="form-group row">
  					<div class="col-4 col-form-label">Harga/Unit</div>
  					<div class="col-6 col-form-label"><strong>0</strong></div>
  				</div>
  				<div class="form-group row">
  					<div class="col-4 col-form-label">Harga Pokok</div>
  					<div class="col-6 col-form-label"><strong>0</strong></div>
  				</div>
  			</fieldset>
  		</div>
  	</div>
  	</div>
  	<!-- End Persediaan -->

  	<!-- start non-persediaan -->
  	<div id="nonpersediaandiv" style="display: none;">
  	<hr>
  	<fieldset>
  		<legend>Informasi Tambahan</legend>
  		<div class="row">
  			<div class="col-md-8">
  				<div class="form-group row">
  					<div class="col-1 col-form-label">Kategori</div>
  					<div class="col-9">
  						{!! Form::select('kategori_produk_non_id',$kategori,null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'kategori_non'])!!}


  					</div>
  				</div>
  				<div class="form-group row">
  					<div class="col-1 col-form-label">Unit</div>
  					<div class="col-4">
  						{!! Form::text('unit',null,['class' => 'form-control','id' => 'unit-change-non-persediaan', 'disabled' => ''])!!}
  					</div>
  				</div>
  			</div>
  			<div class="col-md-4">
  				<div class="form-group row">
  					<div class="col-4 col-form-label">Dept. Default</div>
  					<div class="col-8">
  						{!! Form::select('',[],null,['class' => 'form-control select2','id' => ''])!!}


  					</div>
  				</div>
  				<div class="form-group row">
  					<div class="col-4 col-form-label">Proyek Default</div>
  					<div class="col-8">
  						{!! Form::select('',[],null,['class' => 'form-control select2','id' => ''])!!}


  					</div>
  				</div>
  			</div>
  		</div>
  	</fieldset>
  	</div>
  	<!-- end non-persediaan -->

  </div>

  <div class="tab-pane fade show" id="pills-pp" role="tabpanel" aria-labelledby="pills-pp-tab">
  	<fieldset>
  		<legend>Informasi Penjualan</legend>
  		<div class="form-group row">
  			<div class="col-2 col-form-label">Harga Jual Barang</div>
  			<div class="col-2">
  				{!! Form::text('harga_jual',null,['class' => 'form-control','id' => 'harga_jual_barang','readonly'])!!}
  			</div>
  			<div class="col-2">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal">
  				  Rincian
			    </button>
			  </div>
        <div class="col-3">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#harga-jual-margin">
            Margin Harga Jual
          </button>
        </div>
  		</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" id="#harga-section">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Rincian Harga Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-2 col-form-label">Harga 1</div>
          <div class="col-10">{!! Form::text('tingkat_1',isset($item->tingkatHargaBarang) ? $item->tingkatHargaBarang->tingkat_1 : null,['class' => 'form-control','id' => 'harga_tingkat_1'])!!}</div>
        </div>
        <div class="form-group row">
          <div class="col-2 col-form-label">Harga 2</div>
          <div class="col-10">{!! Form::text('tingkat_2',isset($item->tingkatHargaBarang) ? $item->tingkatHargaBarang->tingkat_2 : null,['class' => 'form-control','id' => 'harga_tingkat_2'])!!}</div>
        </div>
        <div class="form-group row">
          <div class="col-2 col-form-label">Harga 3</div>
          <div class="col-10">{!! Form::text('tingkat_3',isset($item->tingkatHargaBarang) ? $item->tingkatHargaBarang->tingkat_3 : null,['class' => 'form-control','id' => 'harga_tingkat_3'])!!}</div>
        </div>
        <div class="form-group row">
          <div class="col-2 col-form-label">Harga 4</div>
          <div class="col-10">{!! Form::text('tingkat_4',isset($item->tingkatHargaBarang) ? $item->tingkatHargaBarang->tingkat_4 : null,['class' => 'form-control','id' => 'harga_tingkat_4'])!!}</div>
        </div>
        <div class="form-group row">
          <div class="col-2 col-form-label">Harga 5</div>
          <div class="col-10">{!! Form::text('tingkat_5',isset($item->tingkatHargaBarang) ? $item->tingkatHargaBarang->tingkat_5 : null,['class' => 'form-control','id' => 'harga_tingkat_5'])!!}</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save-tingkat-harga" data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
	<div class="col-2 col-form-label">Diskon Barang</div>
	<div class="col-1">{!! Form::text('diskon',null,['class' => 'form-control','id' => ''])!!}</div>
	<div class="col-1 col-form-label" style="padding-left: 0px;">%</div>
</div>

<div class="form-group row">
	<div class="col-2 col-form-label">Kode Pajak Penjualan</div>
	<div class="col-2">{!! Form::select('kode_pajak_penj_id',$kodePajak,null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => ''])!!}</div>
</div>

</fieldset>

<fieldset>
	<legend>Informasi Pembelian</legend>
<div class="form-group row">
	<div class="col-2 col-form-label">Pemasok Utama</div>
	<div class="col-4">
		{!! Form::select('pemasok_id',$pemasok,null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '']) !!}
	</div>
</div>
<div class="form-group row">
	<div class="col-2 col-form-label">Minimal Jumlah Reorder</div>
	<div class="col-2">{!! Form::text('min_jumlah_reorder',null,['class' => 'form-control','id' => ''])!!}</div>
</div>
<div class="form-group row">
	<div class="col-2 col-form-label">Kode Pajak Pembelian</div>
	<div class="col-2">{!! Form::select('kode_pajak_pemb_id',$kodePajak,null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => ''])!!}</div>
</div>
</fieldset>
  </div>
  <div class="tab-pane fade show" id="pills-akun" role="tabpanel" aria-labelledby="pills-akun-tab">
  	<div class="form-group row ak-persediaan">
  		<div class="col-2 col-form-label">Akun Persediaan</div>
  		<div class="col-sm-2"><input class="form-control" type="" name="akun_persedian_code" readonly="" value="{{ !empty($prefBarang) ? $prefBarang->first()->akunPersediaan->kode_akun : null }}"></div>
  		<div class="col-sm-6">
  			{!! Form::select('akun_persedian_id',$akun, !empty($prefBarang) ? $prefBarang[0]->akun_persediaan_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '','onchange' => 'getCodeAkun(this.value,"akun_persedian_code")'])!!}
  		</div>
  	</div>
    <div class="form-group row ak-beban">
      <div class="col-2 col-form-label">Akun Beban</div>
      <div class="col-sm-2"><input class="form-control" type="" name="akun_beban_code" readonly="" value="{{ !empty($prefBarang) ? $prefBarang->first()->akunBeban->kode_akun : null }}"></div>
      <div class="col-sm-6">
        {!! Form::select('akun_beban_id',$akun, !isset($item) ? $prefBarang[0]->akun_beban_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '','onchange' => 'getCodeAkun(this.value,"akun_beban_code")'])!!}
      </div>
    </div>
  	<div class="form-group row">
  		<div class="col-2 col-form-label">Akun Penjualan</div>
  		<div class="col-sm-2"><input class="form-control" type="" name="akun_penjualan_code" readonly="" value="{{ !empty($prefBarang) ? $prefBarang->first()->akunPenjualan->kode_akun : null }}"></div>
  		<div class="col-sm-6">
  			{!! Form::select('akun_penjualan_id',$akun, !isset($item) ? $prefBarang[0]->akun_penjualan_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '','onchange' => 'getCodeAkun(this.value, "akun_penjualan_code")'])!!}
  		</div>
  	</div>
  	<div class="form-group row ak-retur">
  		<div class="col-2 col-form-label">Akun Retur Penjualan</div>
  		<div class="col-sm-2"><input class="form-control" type="" name="akun_ret_penjualan_code" readonly="" value="{{ !empty($prefBarang) ? $prefBarang->first()->akunReturPenjualan->kode_akun : null }}"></div>
  		<div class="col-sm-6">
  			{!! Form::select('akun_ret_penjualan_id',$akun, !isset($item) ? $prefBarang[0]->akun_retur_penjualan_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '','onchange' => 'getCodeAkun(this.value, "akun_ret_penjualan_code")'])!!}
  		</div>
  	</div>
  	<div class="form-group row">
  		<div class="col-2 col-form-label">Akun Diskon Penjualan</div>
  		<div class="col-sm-2"><input class="form-control" type="" name="akun_disk_penjualan_code" readonly="" value="{{ !empty($prefBarang) ? $prefBarang->first()->akunDiskonBarang->kode_akun : null }}"></div>
  		<div class="col-sm-6">
  			{!! Form::select('akun_disk_penjualan_id',$akun, !isset($item) ? $prefBarang[0]->akun_diskon_barang_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '','onchange' => 'getCodeAkun(this.value, "akun_disk_penjualan_code")'])!!}
  		</div>
  	</div>
  	<div class="form-group row ak-terkirim">
  		<div class="col-2 col-form-label">Akun Barang Terkirim</div>
  		<div class="col-sm-2"><input class="form-control" type="" name="akun_barang_terkirim_code" readonly="" value="{{ !empty($prefBarang) ? $prefBarang->first()->akunBarangTerkirim->kode_akun : null }}"></div>
  		<div class="col-sm-6">
  			{!! Form::select('akun_barang_terkirim_id',$akun, !isset($item) ? $prefBarang[0]->akun_barang_terkirim_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '','onchange' => 'getCodeAkun(this.value, "akun_barang_terkirim_code")'])!!}
  		</div>
  	</div>
  	<div class="form-group row ak-hpp">
  		<div class="col-2 col-form-label">Akun HPP</div>
  		<div class="col-sm-2"><input class="form-control" type="" name="akun_hpp_code" readonly="" value="{{ !empty($prefBarang) ? $prefBarang->first()->akunHpp->kode_akun : null }}"></div>
  		<div class="col-sm-6">
  			{!! Form::select('akun_hpp_id',$akun, !isset($item) ? $prefBarang[0]->akun_hpp_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '','onchange' => 'getCodeAkun(this.value, "akun_hpp_code")'])!!}
  		</div>
  	</div>
  	<div class="form-group row ak-retur">
  		<div class="col-2 col-form-label">Akun Retur Pembelian</div>
  		<div class="col-sm-2"><input class="form-control" type="" name="akun_ret_pembelian_code" readonly="" value="{{ !empty($prefBarang) ? $prefBarang->first()->akunReturPembelian->kode_akun : null }}"></div>
  		<div class="col-sm-6">
  			{!! Form::select('akun_ret_pembelian_id',$akun, !isset($item) ? $prefBarang[0]->akun_retur_pembelian_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '','onchange' => 'getCodeAkun(this.value, "akun_ret_pembelian_code")'])!!}
  		</div>
  	</div>
  	<div class="form-group row ak-retur">
  		<div class="col-2 col-form-label">Akun Belum Tertagih</div>
  		<div class="col-sm-2"><input class="form-control" type="" name="akun_belum_tertagih_code" readonly="" value="{{ !empty($prefBarang) ? $prefBarang->first()->akunBelumTertagih->kode_akun : null }}"></div>
  		<div class="col-sm-6">
  			{!! Form::select('akun_belum_tertagih_id',$akun, !isset($item) ? $prefBarang[0]->akun_belum_tertagih_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '','onchange' => 'getCodeAkun(this.value, "akun_belum_tertagih_code")'])!!}
  		</div>
  	</div>
  </div>

  <div class="tab-pane fade show" id="pills-kontrol" role="tabpanel" aria-labelledby="pills-kontrol-tab">
  	<fieldset>
  		<legend>
  		<input class="form-check-input" type="checkbox" value="" name="nomor-seri" id="nomor-seri" style="margin: 0 10px;">
		<label class="form-check-label" for="nomor-seri" style="padding: 0px;">Pakai Nomor Seri</label>
		</legend>
		<div class="nomor-seri-div" style="display: none;">
			<div class="form-group">
				<input class="form-check-input" type="checkbox" style="margin: 0 10px;">
				<label class="form-check-label" style="padding: 0px;">Harus memilih Nomor Seri di Transaksi</label>
			</div>
			<div class="form-group">
				<input class="form-check-input" type="checkbox" style="margin: 0 10px;">
				<label class="form-check-label" style="padding: 0px;">Dapat mengeluarkan nomor seri meskipun tidak ada stok tersedia</label>
			</div>
			<div class="form-group row">
		  		<div class="col-1 col-form-label" style="margin-left: 25px;"><legend>Tipe Barang</legend></div>
		  		<div class="col-10">
		  			<div class="col-form-label">
		  			<div class="form-check">
					  <input class="form-check-input" type="radio" name="tipe-barang" checked>
					  <label class="form-check-label" for="tipe-barang1">
					    Nomor Unik
					  </label>
					</div>
		  			<div class="form-check">
					  <input class="form-check-input" type="radio" name="tipe-barang">
					  <label class="form-check-label" for="tipe-barang1">
					    Nomor Produksi
					  </label>
					</div>
					</div>
		  		</div>
		  	</div>
		  	<div class="form-group">
				<input class="form-check-input" type="checkbox" style="margin: 0 10px;">
				<label class="form-check-label" style="padding: 0px;">Pakai Tanggal Kadaluarsa</label>
			</div>
		  </div>
  </fieldset>
  </div>

  <div class="tab-pane fade show" id="pills-lain" role="tabpanel" aria-labelledby="pills-lain-tab">
  	<div class="form-group">
  		<div class="col-form-label">Catatan</div>
  		{!! Form::textarea('catatan',null,['class' => 'form-control','id' => '', 'rows' => '3'])!!}
  	</div>
  	<div class="row">
  		<div class="col-sm-4" style="border-right: 1px solid #f0f0f0;">
  			<div style="width: 100%; min-height: 200px; background: #f0f0f0"></div>
  			<div style="margin: 20px 0; float: right;">
  				<button class="btn btn-default"><i class="fa fa-upload"></i>&emsp;Ambil Gambar</button>
  				<button class="btn btn-danger"><i class="fa fa-recycle"></i>&emsp;Bersihkan Gambar</button>
  			</div>
  		</div>
  		<div class="col-sm-8">
  			<fieldset>
  				<legend>Dimensi Barang</legend>
  				<div class="form-group row">
  					<div class="col-2 col-form-label">Length (L)</div>
  					<div class="col-4">{!! Form::text('length',null,['class' => 'form-control','id' => ''])!!}</div>
  					<div class="col-5 col-form-label" style="padding-left: 0px">cm</div>
  				</div>
  				<div class="form-group row">
  					<div class="col-2 col-form-label">Width (W)</div>
  					<div class="col-4">{!! Form::text('width',null,['class' => 'form-control','id' => ''])!!}</div>
  					<div class="col-5 col-form-label" style="padding-left: 0px">cm</div>
  				</div>
  				<div class="form-group row">
  					<div class="col-2 col-form-label">Height (H)</div>
  					<div class="col-4">{!! Form::text('height',null,['class' => 'form-control','id' => ''])!!}</div>
  					<div class="col-5 col-form-label" style="padding-left: 0px">cm</div>
  				</div>
  			</fieldset>
  			<hr>
  			<fieldset>
  			<div class="form-group row">
  					<div class="col-2 col-form-label">Berat</div>
  					<div class="col-4">{!! Form::text('berat',null,['class' => 'form-control','id' => ''])!!}</div>
  					<div class="col-5 col-form-label" style="padding-left: 0px;">kg</div>
  			</div>
  			<div class="form-group row">
  					<div class="col-2 col-form-label">Lama Pengiriman</div>
  					<div class="col-4">{!! Form::text('lama_pengiriman',null,['class' => 'form-control','id' => ''])!!}</div>
  					<div class="col-5 col-form-label" style="padding-left: 0px;">hari</div>
  			</div>
  			</fieldset>
  		</div>
  	</div>
  </div>

</div> <!-- End Tab-Content -->