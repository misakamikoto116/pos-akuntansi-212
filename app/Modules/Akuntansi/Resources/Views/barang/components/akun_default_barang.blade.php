    
<div class="form-group row">
    <div class="col-4 col-form-label"><b>Akun Persediaan</b></div>
    <div class="col-8">
        {!! Form::select('akun_persediaan_id', $akun, $preferensi_barang->akun_persediaan_id ?? null, ['class' => 'form-control select2 akun_persediaan_id', 'required', 'placeholder' => 'Pilih Akun', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-4 col-form-label"><b>Akun Penjualan</b></div>
    <div class="col-8">
        {!! Form::select('akun_penjualan_id', $akun, $preferensi_barang->akun_penjualan_id ?? null, ['class' => 'form-control select2 akun_penjualan_id', 'required', 'placeholder' => 'Pilih Akun', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-4 col-form-label"><b>Akun Retur Penjualan</b></div>
    <div class="col-8">
        {!! Form::select('akun_retur_penjualan_id', $akun, $preferensi_barang->akun_retur_penjualan_id ?? null, ['class' => 'form-control select2 akun_retur_penjualan_id', 'required', 'placeholder' => 'Pilih Akun', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-4 col-form-label"><b>Akun Diskon Barang</b></div>
    <div class="col-8">
        {!! Form::select('akun_diskon_barang_id', $akun, $preferensi_barang->akun_diskon_barang_id ?? null, ['class' => 'form-control select2 akun_diskon_barang_id', 'required', 'placeholder' => 'Pilih Akun', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-4 col-form-label"><b>Akun Barang Terkirim</b></div>
    <div class="col-8">
        {!! Form::select('akun_barang_terkirim_id', $akun, $preferensi_barang->akun_barang_terkirim_id ?? null, ['class' => 'form-control select2 akun_barang_terkirim_id', 'required', 'placeholder' => 'Pilih Akun', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-4 col-form-label"><b>Akun HPP</b></div>
    <div class="col-8">
        {!! Form::select('akun_hpp_id', $akun, $preferensi_barang->akun_hpp_id ?? null, ['class' => 'form-control select2 akun_hpp_id', 'required', 'placeholder' => 'Pilih Akun', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-4 col-form-label"><b>Akun Retur Pembelian</b></div>
    <div class="col-8">
        {!! Form::select('akun_retur_pembelian_id', $akun, $preferensi_barang->akun_retur_pembelian_id ?? null, ['class' => 'form-control select2 akun_retur_pembelian_id', 'required', 'placeholder' => 'Pilih Akun', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-4 col-form-label"><b>Akun Beban</b></div>
    <div class="col-8">
        {!! Form::select('akun_beban_id', $akun, $preferensi_barang->akun_beban_id ?? null, ['class' => 'form-control select2 akun_beban_id', 'required', 'placeholder' => 'Pilih Akun', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-4 col-form-label"><b>Akun Belum Tertagih</b></div>
    <div class="col-8">
        {!! Form::select('akun_belum_tertagih_id', $akun, $preferensi_barang->akun_belum_tertagih_id ?? null, ['class' => 'form-control select2 akun_belum_tertagih_id', 'required', 'placeholder' => 'Pilih Akun', 'required' => 'required']) !!}
    </div>
</div>  
