@extends( auth()->user()->hasPermissionTo('ubah_barang') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 10px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
</style>

@endsection

@section('content')
    <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ url('akuntansi/barang') }}">Daftar Barang</a></li>
                    </ol>


                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left']) !!}
                @include($form)
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')
        <script type="text/javascript">
        var q = 0;
        // Rincian Gudang
        $(document).ready(function () {
            $('.duplicate-gudang-sections').on('click', '.add-gudang', function () {
                duplicateGudang();
            });
            $('.duplicate-gudang-sections').on('click', '.remove-rincian-gudang', function () {
                if ($(this).closest('.purchaseGudang').find('tr').length > 0) {
                    $(this).closest('tr').remove();
                }
            });
            ajaxDetailBarang( {{ $item->id }}, 'barang' );

            $("#harga_tingkat_1").autoNumeric('init', {aPad: false,
                aForm: false
            });

            $("#harga_tingkat_2").autoNumeric('init', {aPad: false,
                aForm: false
            });

            $("#harga_tingkat_3").autoNumeric('init', {aPad: false,
                aForm: false
            });

            $("#harga_tingkat_4").autoNumeric('init', {aPad: false,
                aForm: false
            });

            $("#harga_tingkat_5").autoNumeric('init', {aPad: false,
                aForm: false
            });

            $("#harga_jual_barang").autoNumeric('init', {aPad: false,
                aForm: false
            });

        });

        $('#subitem').change(function() {
            $('#subitemdiv').toggle();
        });

        $('#nomor-seri').change(function() {
            $('.nomor-seri-div').toggle();
        });

        $('#persediaan').change(function() {
            $('#persediaandiv').show();
            $('#nonpersediaandiv').hide();
            $('#pills-kontrol-tab').show();
            $('.ak-beban').hide();
            $('.ak-persediaan').show();
            $('.ak-terkirim').show();
            $('.ak-hpp').show();
            $('.ak-retur').show();
            $('#unit-change-persediaan').removeAttr('disabled');
            $('#unit-change-non-persediaan').attr('disabled','disabled');
            $('#kategori_persediaan').removeAttr('disabled');
            $('#kategori_non').attr('disabled','disabled');
        });

        $('#nonpersediaan').change(function() {
            $('#nonpersediaandiv').show();
            $('#persediaandiv').hide();
            $('#pills-kontrol-tab').hide();
            $('.ak-beban').show();
            $('.ak-persediaan').hide();
            $('.ak-terkirim').hide();
            $('.ak-hpp').hide();
            $('.ak-retur').show();
            $('#unit-change-persediaan').attr('disabled','disabled');
            $('#unit-change-non-persediaan').removeAttr('disabled');
            $('#kategori_persediaan').attr('disabled','disabled');
            $('#kategori_non').removeAttr('disabled');
        });

        $('#servis').change(function() {
            $('#nonpersediaandiv').show();
            $('#persediaandiv').hide();
            $('#pills-kontrol-tab').hide();
            $('.ak-beban').hide();
            $('.ak-persediaan').hide();
            $('.ak-terkirim').hide();
            $('.ak-hpp').hide();
            $('.ak-retur').hide();
            $('#unit-change-persediaan').attr('disabled','disabled');
            $('#unit-change-non-persediaan').removeAttr('disabled');
            $('#kategori_persediaan').attr('disabled','disabled');
            $('#kategori_non').removeAttr('disabled');
        });



        $('.unit-sections').on('click','.save-unit', function () {
            $(this).closest('.modal').modal('hide');
        });

        // Jumlah Unit Pada Modal
        $('.unit-change').keyup(function () {
           var unit_change = $('.unit-change').val();
           $('.unit-modal').html(unit_change);
        });

        // Perkalian Kuantitas kali Harga/unit
        $('.saldo-harga-unit').keyup(function () {
            var hasil_kuantitas_harga = parseFloat($('.kuantitas-gudang').val()) * parseFloat($('.saldo-harga-unit').autoNumeric('get'));
            $('.saldo-gudang-input').autoNumeric('init', {aPad: false,aForm:false});
            $('.saldo-gudang-input').autoNumeric('set', hasil_kuantitas_harga);
        });

        function duplicateGudang(){
            var row = $('#table_gudang_section').html();
            var clone = $(row).clone();
            // clear the values
            // clone.find('input[type=text]').val('');
            clone.find('.no_faktur_gudang').attr('name','no_faktur_gudang[]').attr('id','no_faktur_gudang'+q);
            clone.find('.tanggal_gudang').attr('name','tanggal_gudang[]').attr('id','tanggal_gudang'+q);
            clone.find('.gudang').attr('name','multi_gudang_id[]').attr('id','multi_gudang_id'+q);
            clone.find('.kuantitas_gudang').attr('name','kuantitas_gudang[]').attr('id','kuantitas_gudang'+q);
            clone.find('.biaya_gudang').attr('name','biaya_gudang[]').attr('id','biaya_gudang'+q);
            clone.find('.sn_gudang').attr('name','sn_gudang[]').attr('id','sn_gudang'+q);
            $(clone).appendTo($('.duplicate-gudang-sections').closest('.gudang-sections').find('.purchaseGudang'));
            $('#tanggal_gudang'+q).datepicker({format: 'dd MM yyyy', autoclose: true});
            $('.biaya_gudang').autoNumeric('init', {aPad: false,
                aForm: false,
            });
            q++;
        }

        function simpanGudang(param = 'load'){
            var totalSaldoGudang = 0;
            var totalUnitGudang = 0;
            var hasil_saldo_bagi_unit = 0;
            var itemSaldoGudang = $('.simpan-gudang').closest('.modal').find('.purchaseGudang tr');

                $(itemSaldoGudang).each(function(index, el) {
                  var saldoGudang = $(el).find('.biaya_gudang').autoNumeric('get');
                  var unitGudang = $(el).find('.kuantitas_gudang').val();
                  var totalSaldoUnit = parseFloat(saldoGudang) * parseFloat(unitGudang);
                  totalSaldoGudang += totalSaldoUnit;
                  totalUnitGudang += parseFloat(unitGudang);
                });

                if (!isNaN(totalSaldoGudang) && !isNaN(totalUnitGudang) && $(".no_faktur_gudang").val() !== "" ) {
                        var hasil_saldo_bagi_unit = 0;
                        var hasil_saldo_gudang = $('.saldo-gudang-input');
                        var hasil_unit_gudang = $('.kuantitas-gudang');

                        if (totalSaldoGudang || totalUnitGudang !== 0) {
                            hasil_saldo_bagi_unit = totalSaldoGudang / totalUnitGudang;
                        }else {
                            hasil_saldo_bagi_unit = 0;
                        }

                        $(hasil_saldo_gudang).autoNumeric('init', {aPad: false,aForm:false});
                        $(hasil_saldo_gudang).autoNumeric('set', totalSaldoGudang);
                        $('.saldo-harga-unit').autoNumeric('init', {aPad: false,aForm:true});
                        $('.saldo-harga-unit').autoNumeric('set', hasil_saldo_bagi_unit);
                        $(hasil_unit_gudang).val(totalUnitGudang);
                        $(hasil_unit_gudang).attr("readonly","readonly");
                        $('.saldo-harga-unit').attr("readonly","readonly");
                        if(param != 'onload'){
                          $('.simpan-gudang').closest('.modal').modal('hide');
                        }
                }else {
                        swal({
                            icon: "error",
                            text: "Kuantitas / No. Faktur / Harga tidak boleh kosong",
                        });
                }
        }

        // Perhitungan Rincian Gudang
        $('.duplicate-gudang-sections').on('click', '.simpan-gudang', function () {
            simpanGudang();
            
        });

          function getCodeAkun(id, name){
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-akun_id') }}",
                data: {id : id,
                _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                        $("input[name='"+name+"']").val(response.kode_akun);
                    }, error: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        function ajaxDetailBarang(id, type){
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-detail-barang') }}",
                data: {id : id, type : type, 
                _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                    insertDuplicatedGudang(response);
                    simpanGudang('onload');
                }, error: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        function insertDuplicatedGudang(response){
            $.each(response.barang, function(index, key){
                duplicateGudang();
                let ew = q - 1;
                $('#no_faktur_gudang' + ew).val(key.no_faktur);
                $('#tanggal_gudang' + ew).val(key.tanggal_formatted);
                $('#multi_gudang_id' + ew).val(key.gudang_id);
                $('#kuantitas_gudang' + ew).val(key.kuantitas);
                $('#biaya_gudang' + ew).autoNumeric('set', key.harga_terakhir);
                $('#sn_gudang' + ew).val(key.sn);
            });
        }

        $("#save-tingkat-harga").on('click', function(e) {
            e.preventDefault();
            $("#harga_jual_barang").val($("#harga_tingkat_1").val()).val();
        });  
        </script>
@endsection