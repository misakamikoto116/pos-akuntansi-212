<section style="overflow: hidden;">
    <div class="form-group clearfix">
    
    {!! Form::model($dataMataUang,['route' => [$module_url->update, 1],'method' => 'Put','class' => 'form-horizontal form-label-left']) !!}
    
    {!! Form::hidden('input_kategori', 'akun default') !!}
    
    <h1 class="title" align="center">Akun Default Mata Uang :</h1>
        <div class="isi-content">
            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Akun Hutang</b></div>
                <div class="col-2">
                    {!! Form::text('akun_hutang_nama', null, ['class' => 'form-control akun_hutang_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('akun_hutang_id', $akun, null, ['class' => 'form-control select2 akun_hutang_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "akun_hutang_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Akun Piutang</b></div>
                <div class="col-2">
                    {!! Form::text('akun_piutang_nama', null, ['class' => 'form-control akun_piutang_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('akun_piutang_id', $akun, null, ['class' => 'form-control select2 akun_piutang_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "akun_piutang_nama")']) !!}
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Uang Muka Pembelian</b></div>
                <div class="col-2">
                    {!! Form::text('uang_muka_pembelian_nama', null, ['class' => 'form-control uang_muka_pembelian_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('uang_muka_pembelian_id', $akun, null, ['class' => 'form-control select2 uang_muka_pembelian_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "uang_muka_pembelian_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Uang Muka Penjualan</b></div>
                <div class="col-2">
                    {!! Form::text('uang_muka_penjualan_nama', null, ['class' => 'form-control uang_muka_penjualan_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('uang_muka_penjualan_id', $akun, null, ['class' => 'form-control select2 uang_muka_penjualan_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "uang_muka_penjualan_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Diskon Penjualan</b></div>
                <div class="col-2">
                    {!! Form::text('diskon_penjualan_nama', null, ['class' => 'form-control diskon_penjualan_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('diskon_penjualan_id', $akun, null, ['class' => 'form-control select2 diskon_penjualan_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "diskon_penjualan_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Laba/Rugi Terealisir</b></div>
                <div class="col-2">
                    {!! Form::text('laba_rugi_terealisir_nama', null, ['class' => 'form-control laba_rugi_terealisir_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('laba_rugi_terealisir_id', $akun, null, ['class' => 'form-control select2 laba_rugi_terealisir_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "laba_rugi_terealisir_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Laba/Rugi Tak Terealisir</b></div>
                <div class="col-2">
                    {!! Form::text('laba_rugi_tak_terealisir_nama', null, ['class' => 'form-control laba_rugi_tak_terealisir_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('laba_rugi_tak_terealisir_id', $akun, null, ['class' => 'form-control select2 laba_rugi_tak_terealisir_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "laba_rugi_tak_terealisir_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Akun Penyesuaian </b></div>
                <div class="col-2">
                    {!! Form::text('akun_penyesuaian_nama', null, ['class' => 'form-control akun_penyesuaian_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('akun_penyesuaian_id', $akun, null, ['class' => 'form-control select2 akun_penyesuaian_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "akun_penyesuaian_nama")']) !!}
                </div>
            </div>

        </div>
        
        <div class="pull-right" style="margin-right:5px; margin-top:5px;">
            {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
            {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
        </div>

        {!! Form::close() !!}
        
    </div>
</section>