<section style="overflow: hidden;">
    <div class="form-group clearfix">
    
    {!! Form::model($dataBarang,['route' => [$module_url->update, 1],'method' => 'Put','class' => 'form-horizontal form-label-left']) !!}
    
    {!! Form::hidden('input_kategori', 'akun barang') !!}
    
    <h1 class="title" align="center">Akun Default Barang</h1>
        <div class="isi-content">
            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Akun Persediaan</b></div>
                <div class="col-2">
                    {!! Form::text('akun_persediaan_nama', null, ['class' => 'form-control akun_persediaan_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('akun_persediaan_id', $akun, null, ['class' => 'form-control select2 akun_persediaan_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "akun_persediaan_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Akun Penjualan</b></div>
                <div class="col-2">
                    {!! Form::text('akun_penjualan_nama', null, ['class' => 'form-control akun_penjualan_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('akun_penjualan_id', $akun, null, ['class' => 'form-control select2 akun_penjualan_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "akun_penjualan_nama")']) !!}
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Akun Retur Penjualan</b></div>
                <div class="col-2">
                    {!! Form::text('akun_retur_penjualan_nama', null, ['class' => 'form-control akun_retur_penjualan_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('akun_retur_penjualan_id', $akun, null, ['class' => 'form-control select2 akun_retur_penjualan_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "akun_retur_penjualan_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Akun Diskon Barang</b></div>
                <div class="col-2">
                    {!! Form::text('akun_diskon_barang_nama', null, ['class' => 'form-control akun_diskon_barang_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('akun_diskon_barang_id', $akun, null, ['class' => 'form-control select2 akun_diskon_barang_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "akun_diskon_barang_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Akun Barang Terkirim</b></div>
                <div class="col-2">
                    {!! Form::text('akun_barang_terkirim_nama', null, ['class' => 'form-control akun_barang_terkirim_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('akun_barang_terkirim_id', $akun, null, ['class' => 'form-control select2 akun_barang_terkirim_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "akun_barang_terkirim_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Akun HPP</b></div>
                <div class="col-2">
                    {!! Form::text('akun_hpp_nama', null, ['class' => 'form-control akun_hpp_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('akun_hpp_id', $akun, null, ['class' => 'form-control select2 akun_hpp_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "akun_hpp_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Akun Retur Pembelian</b></div>
                <div class="col-2">
                    {!! Form::text('akun_retur_pembelian_nama', null, ['class' => 'form-control akun_retur_pembelian_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('akun_retur_pembelian_id', $akun, null, ['class' => 'form-control select2 akun_retur_pembelian_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "akun_retur_pembelian_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Akun Beban</b></div>
                <div class="col-2">
                    {!! Form::text('akun_beban_nama', null, ['class' => 'form-control akun_beban_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('akun_beban_id', $akun, null, ['class' => 'form-control select2 akun_beban_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "akun_beban_nama")']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-3 col-form-label"><b>Akun Belum Tertagih</b></div>
                <div class="col-2">
                    {!! Form::text('akun_belum_tertagih_nama', null, ['class' => 'form-control akun_belum_tertagih_nama', 'required','readonly']) !!}
                </div>
                <div class="col-7">
                    {!! Form::select('akun_belum_tertagih_id', $akun, null, ['class' => 'form-control select2 akun_belum_tertagih_id', 'required', 'placeholder' => 'Pilih Akun', 'onchange' => 'changeNamaAkun(this.value, "akun_belum_tertagih_nama")']) !!}
                </div>
            </div>
        </div>
        
        <div class="pull-right" style="margin-right:5px; margin-top:5px;">
            {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit-default-barang']) !!}
            {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
        </div>

        {!! Form::close() !!}
        
    </div>
</section>