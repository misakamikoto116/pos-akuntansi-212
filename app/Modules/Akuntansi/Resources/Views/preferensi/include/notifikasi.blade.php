<section style="overflow: hidden;">
    <div class="form-group clearfix">
    
    {!! Form::model($dataBarang,['route' => [$module_url->update, 1],'method' => 'Put','class' => 'form-horizontal form-label-left']) !!}
    
    {!! Form::hidden('input_kategori', 'notifikasi') !!}
    
    <h1 class="title" align="center">Notifikasi Barang</h1>
        <div class="isi-content">
        	<div class="form-group row">
        	    <div class="col-3 col-form-label">
        	        <b>Minimal Stock</b>
        	    </div>
        	    <div class="col-9">
        	        {!! Form::number('minimal_stock', null,['class' => 'form-control','required','style' => 'text-align: right']) !!}
        	    </div>
        	</div>

        	<div class="form-group row">
        	    <div class="col-3 col-form-label">
        	        <b>Estimasi Kada Luarsa</b>
        	    </div>
        	    <div class="col-8">
        	        {!! Form::number('estimasi_kada_luarsa', null,['class' => 'form-control','required','style' => 'text-align: right']) !!}
        	    </div>
        	    <div class="col-1 col-form-label">
        	        <b>Hari</b>
        	    </div>
        	</div>
        </div>

    <div class="pull-right" style="margin-right:5px; margin-top:5px;">
        {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit-notifikasi']) !!}
        {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
    </div>

    {!! Form::close() !!}

    </div>
</section>