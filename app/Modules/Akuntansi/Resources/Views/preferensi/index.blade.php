@extends('chichi_theme.layout.app')

@section('custom_css')
    <style>
        .isi-content{
            background-color:#f2f8f9;
            padding: 20px;
            margin: 5px;
            border-radius: 3px;
        }
        .list-content li{
            background-color:#f2f8f9;
            border-color: solid 1px #ccc;
        }
    </style>
@endsection

@section('content')
<div class="form-group row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="title">Preferensi</h4>
            </div>
            <div class="card-body">
                <br>
                <div class="form-group row">
                    <div class="col-md-3">
                        <h1 class="title" align="center">Preferensi</h1>
                        <ul id="ul-group" class="nav nav-pills list-group list-content">
                            <li class="nav-item list-group-item">
                                <a id="tab-akun-default-mata-uang" data-toggle="pill" onclick="checkMataUang('mata-uang'); scrolltop();" href="#akun_default_mata_uang" >Akun Default Mata Uang</a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a id="tab-akun-default-barang" data-toggle="pill" onclick="checkMataUang('barang'); scrolltop();" href="#akun_default_barang">Akun Default Barang</a>
                            </li>
                            <li class="nav-item list-group-item">
                                <a id="tab-notifikasi" data-toggle="pill" onclick="scrolltop();" href="#notifikasi">Notifikasi Barang</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade" id="akun_default_mata_uang">
                                @include('akuntansi::preferensi.include.akun_default_mata_uang')
                            </div>
                            <div class="tab-pane fade" id="akun_default_barang">
                                @include('akuntansi::preferensi.include.akun_default_barang')
                            </div>
                            <div class="tab-pane fade" id="notifikasi">
                                @include('akuntansi::preferensi.include.notifikasi')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('custom_js')
    <script src="{{ asset('js/preferensi.js') }}"></script>
    <script>
        function scrolltop() {
            $("html, body").animate({ scrollTop: 125 }, 1000);
        }
    </script>
@endsection
