<div class="form-group row">
  <div class="col-1 col-form-label">Pelanggan</div>
  <div class="col-2">
  {!! Form::text('no_pelanggan',null,['class' => 'form-control','id' => 'no_pelanggan','readonly'])!!}
  </div>
  <div class="col-4">
  {!! Form::select('pelanggan_id',$pelanggan,null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'pelanggan_id'])!!}
  {!! Form::hidden('uang_muka',1,['class' => 'form-control','id' => ''])!!}
  </div>
  	<div class="col-3 form-inline">
  	  @if (isset($item))
  	  	<div class="form-check form-check-inline">
      		<input class="form-check-input taxable" type="checkbox" id="group1" name="taxable" onchange="calculate()"  value="1" {{ ($item->taxable == 1) ? 'checked' : '' }}>
      		<label class="form-check-label" for="group1"> Cust. is Taxable</label>
        </div>
        <div class="form-check form-check-inline">
      	    <input class="form-check-input in_tax group2" type="checkbox" name="in_tax" id="inlineCheckbox2" onchange="calculate()" value="1" {{ ($item->in_tax == 1) ? 'checked' : '' }}>
      		<label class="form-check-label" for="inlineCheckbox2"> Inclusive Tax</label>
        </div>
      @else
      	<div class="form-check form-check-inline">
      		<input class="form-check-input taxable" type="checkbox" id="group1" name="taxable" onchange="calculate()"  value="1">
      		<label class="form-check-label" for="group1"> Cust. is Taxable</label>
      	</div>
      	<div class="form-check form-check-inline">
      		<input class="form-check-input in_tax group2" type="checkbox" name="in_tax" id="inlineCheckbox2" onchange="calculate()" value="1" disabled="">
      		<label class="form-check-label" for="inlineCheckbox2"> Inclusive Tax</label>
      	</div>
  	  @endif
    </div>
</div>
{!! Form::hidden('total',isset($item) ? $item->total : null, ['class' => 'kolom_grandtotal']) !!}

<hr>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
	    <label for="">Tagihan Ke</label>
	    {!! Form::textarea('alamat_asal',null,['class' => 'form-control','id' => 'alamat_asal','readonly'])!!}
	  	</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
	    <label for="">Kirim Ke</label>
	    {!! Form::textarea('alamat_pengiriman',null,['class' => 'form-control','id' => 'alamat_pengiriman',''])!!}
	  	</div>
	</div>
	<div class="offset-sm-3 col-sm-3">
		<div class="row">
	      <div class="col-sm-6">
	            <div class="form-group">
	              <div class="tag">No. Faktur</div>
	              {!! Form::text('no_faktur',null,['class' => 'form-control','id' => '','required'])!!}
	              </div>
	      </div>
			<div class="col-sm-6">
				<div class="form-group">
			    <div class="tag">Invoice Date</div>
			    @if (isset($item))
			    	{!! Form::text('invoice_date',null,['class' => 'form-control tanggal_faktur','id' => '',''])!!}
			    @else
			    	{!! Form::text('invoice_date',null,['class' => 'form-control tanggal','id' => '',''])!!}
			    @endif
			  	</div>
			</div>
		</div>
	</div>
</div>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>

<div class="penawaran-sections" data-id="1">
<div class="duplicate-penawaran-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
    <div style="width: 1500px;">
  	<table class="table duplicate-sections">
  		<thead class="thead-light" style="border-collapse: collapse;">
  			<tr>
  				<th width="12%">Item</th>
  				<th width="12%">Item Description</th>
  				<th>Qty</th>
  				<th>Unit Price</th>
  				<th>Disc %</th>
  				<th>Tax</th>
  				<th>Amount</th>
  				<th>SN</th>
  			</tr>
  		</thead>
  		<tbody class="purchasePenawaran">
  			@if (isset($item))
  				@foreach ($item->barang as $itemBarang)
  					<tr>
					    <td style="display: none;">{!! Form::hidden('produk_id',$produk->id,['placeholder' => '-Pilih-','class' => 'form-control produk_id'])!!}</td>
					    <td>{!! Form::text('produk_no_barang',$produk->no_barang,['placeholder' => '-Pilih-','class' => 'form-control produk_id','disabled' => ''])!!}</td>
					    <td>{!! Form::text('item_deskripsi',$produk->keterangan,['class' => 'form-control keterangan_produk','id' => '','readonly' => ''])!!}</td>
					    <td>{!! Form::text('jumlah',1,['class' => 'form-control qty_produk','id' => 'qty_produk','readonly' => '','onchange'=>'sumQtyAndUnit()'])!!}</td>
					    <td>{!! Form::text('unit_price',$itemBarang->unit_price,['class' => 'form-control unit_harga_produk mask','id' => 'unit_harga_produk','onchange'=>'sumQtyAndUnit()'])!!}</td>
					    <td>{!! Form::text('diskon_produk',$itemBarang->diskon,['class' => 'form-control diskon_produk','id' => 'diskon_produk','onchange'=>'sumQtyAndUnit()','readonly'])!!}</td>
					    <td width="10%">
								{!! Form::select('kode_pajak_id[]',$pajak,[$itemBarang->kode_pajak_2_id, $itemBarang->kode_pajak_id],['class' => 'form-control select2 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => 'tax_produk','onchange'=>'sumQtyAndUnit()'])!!}
							</td>
					    <td>{!! Form::text('amount_produk',0,['class' => 'form-control amount_produk','id' => 'amount_produk','readonly' => ''])!!}</td>
					    <td>{!! Form::text('sn',null,['class' => 'form-control sn','id' => '',''])!!}</td>
					 </tr>
  				@endforeach
  			@else
  			<tr>
			    <td style="display: none;">{!! Form::hidden('produk_id',$produk->id,['placeholder' => '-Pilih-','class' => 'form-control produk_id'])!!}</td>
			    <td>{!! Form::text('produk_no_barang',$produk->no_barang,['placeholder' => '-Pilih-','class' => 'form-control produk_id','disabled' => ''])!!}</td>
			    <td>{!! Form::text('item_deskripsi',$produk->keterangan,['class' => 'form-control keterangan_produk','id' => '','readonly' => ''])!!}</td>
			    <td>{!! Form::text('jumlah',1,['class' => 'form-control qty_produk','id' => 'qty_produk','readonly' => '','onchange'=>'sumQtyAndUnit()'])!!}</td>
			    <td>{!! Form::text('unit_price',0,['class' => 'form-control unit_harga_produk mask','id' => 'unit_harga_produk','onchange'=>'sumQtyAndUnit()'])!!}</td>
			    <td>{!! Form::text('diskon_produk',$produk->diskon,['class' => 'form-control diskon_produk','id' => 'diskon_produk','onchange'=>'sumQtyAndUnit()','readonly'])!!}</td>
			    <td width="10%">
						{!! Form::select('kode_pajak_id[]',$pajak,[$produk->kode_pajak_penj_id],['class' => 'form-control select2 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => 'tax_produk','onchange'=>'sumQtyAndUnit()','disabled'])!!}
				</td>
			    <td>{!! Form::text('amount_produk',0,['class' => 'form-control amount_produk','id' => 'amount_produk','readonly' => ''])!!}</td>
			    <td>{!! Form::text('sn',null,['class' => 'form-control sn','id' => '',''])!!}</td>
			 </tr>
  			@endif
      </tbody>
    </table>
    </div>
  </div>

</div><!-- END PILLS -->
</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-4">
	 	<div class="form-group">
		    <label for="exampleFormControlTextarea1">Description</label>
		    {!! Form::textarea('catatan',null,['class' => 'form-control','id' => 'catatan'])!!}
		</div>
		<div class="form-group">
	      	<label for="exampleFormControlTextarea1">Akun Piutang</label>
	      	{!! Form::select('akun_piutang_id',$akun,!isset($item) && !empty($getAkunDp) ? $getAkunDp->akun_piutang_id : null,['placeholder'=>'--Pilih--','class' => 'form-control select2','id' => 'catatan'])!!}
	  	</div>
	  	<div class="form-group">
	      	<label for="exampleFormControlTextarea1">Akun DP</label>
	      	{!! Form::select('akun_dp_id',$akun, !isset($item) && !empty($getAkunDp) ? $getAkunDp->uang_muka_penjualan_id : null,['placeholder'=>'--Pilih--','class' => 'form-control select2','id' => 'catatan'])!!}
	  	</div>
    <hr>
    <div class="row">
    </div>
	</div>
	<div class="offset-sm-4 col-sm-4">
		<div class="form-group row">
		<div class="offset-2 col-3 col-form-label text-right"><strong>Sub Total</strong></div>
		<div class="col-7 text-right col-form-label" id="subtotal"><strong>0</strong></div>
		</div>
		
		<div class="form-group row">
			<div class="col-2 col-form-label">Discount:</div>
			<div class="col-3 col-form-label">{!! Form::text('diskon',isset($item) ? $item->diskon : 0,['class' => 'form-control','id' => 'diskonTotal', 'onchange' => 'calculate()','readonly'])!!}</div>
			<div class="col-2 col-form-label text-center">% =</div>
			<div class="col-5 col-form-label">{!! Form::text('',0,['class' => 'form-control','id' => 'totalPotonganRupiah','readonly' => ''])!!}</div>
		</div>
		<div class="form-group row">
			<div class="offset-1 col-4 col-form-label text-right"></div>
			<div class="col-7 text-right col-form-label" id="taxNaration"></div>
		</div>
		<div class="form-group row">
		<div class="offset-1 col-4 col-form-label text-right"><strong>Total Invoice</strong></div>
    <div class="col-7 text-right col-form-label grandtotal"><strong>0</strong></div>
		</div>
	</div>
</div>

