@extends('chichi_theme.layout.app')

@section('custom_css')
<link href="{{asset('assets/plugins/footable/css/footable.core.css')}}" rel="stylesheet">
<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788;
        color: white;
        text-align: center;
        position: relative;
        top: 4px;
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Penjualan</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Uang Muka Pelanggan</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Customer Down Payment / Uang Muka Pelanggan</h5>
            </div>
            <div class="card-body">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left','id' => 'formUangMuka']) !!}
                <div class="p-20">
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary' ]) !!}
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit',  'disabled' => 'disabled']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

        <!--FooTable-->
        <script src="{{ asset('assets/plugins/footable/js/footable.all.min.js')}}"></script>

        <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>

        <!--FooTable Example-->
        <script src="{{ asset('assets/pages/jquery.footable.js')}}"></script>

        <script type="text/javascript">
        $('#selectAll').click(function(e){
            var table= $(e.target).closest('table');
            $('th input:checkbox',table).prop('checked',this.checked);
        });
        </script>
        <script>
                $('.tanggal_faktur').datepicker({format: 'dd MM yyyy', autoclose: true});

                $('document').ready(function(){
                    // trigger cek satu2 pajak itemnya
                    dataPelanggan();
                    sumQtyAndUnit();
                });
        
                 $(function() {
                  enable_cb();
                  $("#group1").click(enable_cb);
                });

                function enable_cb() {
                  if ($("input#group1").prop('checked') == true) {
                    $("input.group2").removeAttr("disabled");
                  } else {
                    $("input.group2").attr("disabled", true).removeAttr("checked");
                  }
                }

                $( "#pelanggan_id" ).change(function() {
                    dataPelanggan();                    
                });

                function dataPelanggan() {
                    var pelanggan_id   = $('#pelanggan_id').val();
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-alamat-pelanggan') }}",
                        data: {id : pelanggan_id,
                            _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){   
                        $('#alamat_pengiriman').val(response.alamat);
                        $('#alamat_asal').val(response.alamat);
                        $('#no_pelanggan').val(response.no_pelanggan);
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                    sortPajakPelanggan(pelanggan_id);
                };

                function sortPajakPelanggan(id) {
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-sort-pajak') }}",
                        data: {
                            id : id, type: 'pelanggan',
                            _token: '{{ csrf_token() }}',
                        },
                        dataType: "json",
                        success: function (response) {
                            $('#tax_produk').find('option').remove().end();
                            $.each(response.kode_pajak, function (index, item){
                                $('#tax_produk').append( 
                                $("<option></option>")
                                .text(item.nilai+ "/" +item.nama)
                                .val(item.id)
                                );
                            });
                        }
                    });
                }

                $(document).ready( function() {
                    $("#btn-submit").click(function(e) {
                        e.preventDefault();
                        submitForm();
                    });
                });

                function submitForm() {
                    var unit_price = $("#unit_harga_produk").val();

                    if (unit_price <= 0) {
                        swal({
                            icon: 'error',
                            text: 'Tidak dapat menyimpan, karena harga unit masih 0',
                        });
                    }else {
                        $("#formUangMuka").submit();
                    }
                };

                function sumQtyAndUnit(){
                    if ($("#unit_harga_produk").val() == "") {
                        $("#unit_harga_produk").val(0);
                    }
                    
                    var amount  = 0;
                    let disc    = 0;

                    amount = parseInt($('#qty_produk').val()) * parseInt(changeMaskingToNumber($('#unit_harga_produk').val()));
                    disc = amount * parseFloat($('#diskon_produk').val()) / 100;            

                    $('#amount_produk').autoNumeric('init',{aPad: false}).autoNumeric('set', amount - disc);
                    calculate();
                }

            
        function calculate(){
            var subTotal    = 0;
            var grandtotal  = 0;
            var tax         = 0;
            var diskon      = 0;
            var in_tax      = 0;
            var amount      = [];
            var sorting     = [];
            $(".amount_produk").autoNumeric('init',{aPad: false});
            $('#taxNaration').html('');

            $(".amount_produk").each(function () {
                amount.push([$(this).autoNumeric('get')]);
            });

            $.each(amount, function (index, key) {
                var total_tax = 0;
                $('#tax_produk option:selected').each(function () {
                    var str = $(this).text();
                    var spl = str.split("/");
                    total_tax += parseFloat(spl[0].replace(/ [0-9]*/, ''));
                });
                $('#tax_produk option:selected').each(function () {
                    var str = $(this).text();
                    var spl = str.split("/");
                    let temp_tax = (100 / (100 + total_tax) * parseFloat(key));

                    var sort_temp = {
                        label: spl[1].replace(/ [0-9]*/, ''),
                        nilai: spl[0].replace(/ [0-9]*/, ''),
                        total: parseFloat(key) * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100,
                        total_in_tax: temp_tax * parseFloat(spl[0].replace(/ [0-9]*/, '')) / 100
                    }
                    sorting.push(sort_temp);
                });
            });

            var sorted = sorting.sort(function (a, b) {
                return a.nilai - b.nilai;
            })

            var sorted_temp = sorted;
            var unduplicateItem = [];
            for (var i = 0; i < sorted_temp.length; i++) {
                if (sorted_temp.length >= 2) {
                    if (sorted_temp[i + 1] != null) {
                        if (sorted_temp[i]['nilai'] != sorted_temp[i + 1]['nilai'] && sorted_temp[i]['label'] != sorted_temp[i + 1]['label']) {
                            unduplicateItem.push(sorted_temp[i]);
                        } else {
                            sorted_temp[i + 1]['total'] = sorted_temp[i]['total'] + sorted_temp[i + 1]['total'];
                            sorted_temp[i + 1]['total_in_tax'] = sorted_temp[i]['total_in_tax'] + sorted_temp[i + 1]['total_in_tax'];
                        }
                    } else {
                        unduplicateItem.push(sorted_temp[i]);
                    }
                } else {
                    unduplicateItem.push(sorted_temp[i]);
                }
            }

            $(".amount_produk").each(function () {
                subTotal += parseInt($(this).autoNumeric('get'));
                amount[$(this).index()].push($(this).autoNumeric('get'));
            });
            $.each(unduplicateItem, function (index, key) {
                if ($('input.in_tax').is(':checked')) {
                    tax += key['total_in_tax'];
                    $("#taxNaration").append(key['label'] + " " + key['nilai'] + "% = " + key['total_in_tax'].toFixed(2) + "<br>");
                } else {
                    tax += key['total'];
                    $("#taxNaration").append(key['label'] + " " + key['nilai'] + "% = " + key['total'].toFixed(2) + "<br>");
                }
            });

            if ($('input.taxable').is(':checked')) {
                $('.tax_produk').attr('disabled', false);
                $(".tax_produk").select2({
                    maximumSelectionLength: 2
                });
                tax = tax;
                $('#taxNaration').show();
                if ($('input.in_tax').is(':checked')) {
                    tax = 0;
                    //balik lagi 0 karena harga sudah sama ongkir
                }
            } else {
                $('.tax_produk').attr('disabled', true);
                $('#taxNaration').hide();
                tax = 0;
            }


            diskon = subTotal * $('#diskonTotal').val() / 100;
            grandtotal = subTotal - diskon + tax;

            $('#subtotal').html("<strong>"+ Number(subTotal).toLocaleString() +"</strong>");
            $('.grandtotal').html("<strong>"+Number(grandtotal).toLocaleString()+"</strong>");
            $('.kolom_grandtotal').val(grandtotal);
            $('#totalPotonganRupiah').val(diskon);
        }
        </script>
@endsection
