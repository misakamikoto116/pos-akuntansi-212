@extends( auth()->user()->hasPermissionTo('buat_faktur_penjualan') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
<link href="{{asset('assets/plugins/footable/css/footable.core.css')}}" rel="stylesheet">
<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788;
        color: white;
        text-align: center;
        position: relative;
        top: 4px;
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
    .warnatr{
        background: lightsteelblue;
        color: black;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Penjualan</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Faktur Penjualan</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Sales Invoice / Faktur Penjualan</h5>
            </div>
            <div class="card-body">
                {!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left','id' => 'form-faktur-penjualan']) !!}
                <div class="p-20">
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary', 'id' => 'btn-cetak']) !!}
                <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" id="btn-submit" disabled data-toggle="dropdown">
                Simpan <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            {!! Form::submit('Terima Bayaran', ['class' => 'btn btn-success btn-block','value' => '1', 'name' => 'lanjutkan']) !!}
                        </li>
                        <li>
                            {!! Form::submit('Simpan & Baru', ['class' => 'btn btn-info btn-block','value' => '0', 'name' => 'lanjutkan']) !!}
                        </li>
                        <li>
                            {!! Form::button('Simpan & Tutup',['type' => 'submit','class' => 'btn btn-warning btn-block']) !!}
                        </li>
                    </ul>
                </div>
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                
                </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

        <!--FooTable-->
        <script src="{{ asset('assets/plugins/footable/js/footable.all.min.js')}}"></script>

        <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>

        <!--FooTable Example-->
        <script src="{{ asset('assets/pages/jquery.footable.js')}}"></script>

        <script type="text/javascript">
        $('#selectAll').click(function(e){
            var table= $(e.target).closest('table');
            $('th input:checkbox',table).prop('checked',this.checked);
        });
        </script>
        <script>
        var q = 0;            
        var x = 0;   

        function GetURLParameter(sParam) {
                var sPageURL = window.location.search.substring(1);
                var sURLVariables = sPageURL.split('&');
                for (var i = 0; i < sURLVariables.length; i++) {
                    var sParameterName = sURLVariables[i].split('=');
                    if (sParameterName[0] == sParam) {
                        return sParameterName[1];
                    }
                }
        }

        $(function() {
            enable_cb();
            $("#group1").click(enable_cb);
            if(GetURLParameter('pelanggan_id') != null){
                $('#pelanggan_id').val(GetURLParameter('pelanggan_id'));
            }
            $("#pelanggan_id").select2({
                tags: true
            });
            @if(!old('pelanggan_id'))
                setDataPelanggan();              
            @endif
            $('#pelanggan_id').select2({
                tag: true,
            });
            calculate();
          });

            function counterDp(){
                var pengurang = 0;                    
                var sum_dp = 0;
                $('.total_uang_muka_dp').each(function(idx, itm){
                    sum_dp += parseFloat(changeMaskingToNumber(itm.value));
                });
                $('.faktur_uang_muka_pelanggan_id').each(function(){
                    let id = this.value;
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.check-dp') }}",
                        data: {id : id,
                            _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                           pengurang += response; 
                        //$('#counterDp').html(sum_dp - pengurang);
                        }
                    });
                });
                $('#counterDp').autoNumeric('init',{aPad: false}).autoNumeric('set', sum_dp);
                calculate();
            }
        
            function insertItemToForm(vale, item, target) {
                 var url = "{{ route('akuntansi.get-produk-by-val') }}";
                 var pelanggan_id = $("#pelanggan_id").val();                                    
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: {id : item, type : target, pelanggan_id : pelanggan_id,
                            _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                        $('#tax_produk'+vale).find('option').remove().end();                        
                        $('#gudang_id'+vale).find('option').remove().end();    
                        if(target == 'pengiriman'){
                            $('#barang_pengiriman_penjualan_id'+vale).val(response.barang_pengiriman_penjualan_id);
                        }                    
                        $('#no_produk'+vale).val(response.all.produk.no_barang);
                        $('#keterangan_produk'+vale).val(response.keterangan);
                        $('#ship_id').val(response.ship_id);
                        $('#delivery_date').val(response.delivery_date);
                        $('#term_id').val(response.term_id);
                        $('#po_no').val(response.po_no);
                        $('#fob').val(response.fob);
                        $('#qty_produk'+vale).val(response.jumlah);
                        $('#satuan_produk'+vale).val(response.satuan);
                        $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
                        $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0); 
                        $('#diskon_produk'+vale).val(response.diskon);
                        $('#barang_id'+vale).val(response.barang_id);
                        $('#terproses'+vale).val(response.terproses);
                        $('#no_so'+vale).val(response.no_so);
                        $('#no_do'+vale).val(response.no_do);
                        $("#diskonTotal").val(response.diskon_faktur);
                        $("#totalPotonganRupiah").autoNumeric('init',{aPad: false}).autoNumeric('set', response.jumlah_diskon_faktur);
                        $("#ongkir").autoNumeric('init',{aPad: false}).autoNumeric('set', response.ongkir_faktur);
                        $("#catatan").val(response.keterangan_faktur);
                        if(typeof(response.harga_modal) == 'object' && response.harga_modal !== null){
                            $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
                            $('#harga_terakhir'+vale).val(response.harga_modal.harga_terakhir);
                        }else{
                            $('#harga_modal'+vale).val(response.harga_modal);
                        }
                        $('#selisih_diskon_amount'+vale).val(0);
                        if(response.ditutup == 1){
                            $('#ditutup'+vale).prop('checked', true);
                        }
                        $.each(response.tax, function (index, item){
                            if(target == target){
                                if(response.kode_pajak_id == item.id || response.kode_pajak_2_id == item.id){
                                    $('#tax_produk'+vale).append( 
                                    $("<option></option>")
                                    .text(item.nilai+ " / " +item.nama)
                                    .val(item.id)
                                    .attr('selected','selected')
                                    );
                                }else{
                                    $('#tax_produk'+vale).append( 
                                    $("<option></option>") 
                                    .text(item.nilai+ " / " +item.nama)
                                    .val(item.id)
                                    );
                                }
                            }else{
                                $('#tax_produk'+vale).append( 
                                    $("<option></option>") 
                                   .text(item.nilai+ " / " +item.nama)
                                   .val(item.id)
                                );
                            }   
                            
                        });
                        $(response.multiGudang).each(function (val, text) {
                            $(text.gudang).each(function (index, item) {
                                $('#gudang_id'+vale).append(`<option value="${item.id}">${item.nama}</option>`);
                            });
                        });
                        if (response.taxable.taxable == 1) {
                            $("#group1").prop('checked', true);
                            $("#inlineCheckbox2").removeAttr('disabled');
                        }else if(response.taxable.taxable == 0 || response.taxable.taxable === null) {
                            $("#group1").prop('checked', false);
                            $("#inlineCheckbox2").attr('disabled','disabled');
                        }
                        if (response.taxable.in_tax == 1) {
                            $("#inlineCheckbox2").prop('checked', true);
                        }else if(response.taxable.in_tax == 0 || response.taxable.in_tax  ===null){
                            $("#inlineCheckbox2").prop('checked', false);
                        }
                        sumQtyAndUnit(vale);
                        $('.select3').select2();
                        for(var a = 0; a < q; a++){
                            callSelect2AjaxProduk(a, 'modal');
                        }
                        setAkun();
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
            }

        $( "#pelanggan_id" ).change(function() {
            setDataPelanggan();
        });
        
        function setDataPelanggan(){
            var pelanggan_id = $('#pelanggan_id').val();
            var sub = GetURLParameter('pengiriman_id');
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-alamat-pelanggan') }}",
                data: {id : pelanggan_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#table-list-penawaran').find('tr').remove().end();
                $('#table-list-pesanan').find('tr').remove().end();
                $('#table-list-pengiriman').find('tr').remove().end();
                $('#table-list-uangmuka').find('tr').remove().end();
                
                @if(!old('faktur_uang_muka_pelanggan_id'))
                    $('.dpList').find('tr').remove().end();   
                @endif
                $('.purchasePenawaran').find('tr').remove().end();   
                $('#counterDp').html(0);

                $('#alamat_pengiriman').val(response.alamat);
                $('#alamat_asal').val(response.alamat);
                $('#no_pelanggan').val(response.no_pelanggan);
                $("#tax_cetak0").val(null);
                $("#tax_cetak1").val(null);
                $("#tax_cetak_label0").val(null);
                $("#tax_cetak_label1").val(null);
                q = 0;
                if (response.penawaran) {
                    $.each(response.penawaran, function (index, item){
                        $('#table-list-penawaran').append('<tr><th><input type="checkbox" value="'+item.id+'" name="penawaran[]" id="penawaran[]"/></th><td>'+item.no_penawaran+'</td><td>'+item.tanggal+'</td></tr>');
                    });
                }
                if (response.pesanan) {
                    $.each(response.pesanan, function (index, item){
                        $('#table-list-pesanan').append('<tr><th><input type="checkbox" value="'+item.id+'" name="pesanan[]" id="pesanan[]"/></th><td>'+item.so_number+'</td><td>'+item.so_date+'</td><td>'+item.po_number+'</td></tr>');
                    });
                }
                if (response.pengiriman) {
                    $.each(response.pengiriman, function (index, item){
                        if(sub == item.id){ 
                            $('#table-list-pengiriman').append('<tr><th><input type="checkbox" checked value="'+item.id+'" name="pengiriman[]" class="pengiriman_item" id="pengiriman[]"/></th><td>'+item.po_no+'</td><td>'+item.delivery_date+'</td><td>'+item.delivery_no+'</td></tr>');
                            filterSelectedMethod('pengiriman');
                        }else{
                            $('#table-list-pengiriman').append('<tr><th><input type="checkbox" value="'+item.id+'" name="pengiriman[]" class="pengiriman_item" id="pengiriman[]"/></th><td>'+item.po_no+'</td><td>'+item.delivery_date+'</td><td>'+item.delivery_no+'</td></tr>');
                        }                   
                    });
                }
                if (response.uang_muka) {
                    $.each(response.uang_muka, function (index, item){
                        if (item.updated_uang_muka > 0) {
                            $('#table-list-uangmuka').append('<tr><th><input type="checkbox" value="'+item.id+'" name="uangmuka[]" class="uangmuka_item" id="uangmuka[]"/></th><td>'+item.no_faktur+'</td><td>'+item.invoice_date+'</td><td>'+item.updated_uang_muka+'</td></tr>');
                        }
                    });
                }
                if(typeof sub != 'string'){
                    checkAfterSetPelanggan(); 
                }else{
                    var form_old = "{{ old('pelanggan_id') }}";
                    if(form_old == ""){
                        callBackDp();
                    }else{
                        counterDp();
                    }
                }
                
            }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        /** Ketika dipilih salah satu dari pengiriman, pesanan dll maka eksekutornya disini **/
        function filterSelectedMethod(target){
            
            var checkCounted = $('input[name="'+target+'[]"]:checked').length;
            var input = $('input[name="'+target+'[]"]:checked');
            
            if(checkCounted > 0){
                input.each(function() {
                    var val = $(this).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-detail-barang') }}",
                        data: {id : val, type : target,
                        _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                            $.each(response.barang, function(index, item){
                                duplicateForm();
                                let countTemp = q - 1;
                                callSelect2AjaxProdukID(countTemp, item.produk_id);
                                $('#produk_id'+countTemp).val(item.produk_id); 
                                insertItemToForm(countTemp, item.id, target); //-1 karena diatas (diplicateForm) sdh di increment
                            });
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                });
              }
            }

        function filterUangMuka(){
            var checkCounted = $('input[name="uangmuka[]"]:checked').length;
            var input = $('input[name="uangmuka[]"]:checked');
            if(checkCounted > 0){
                input.each(function() {
                    var val = $(this).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-detail-informasi-pelanggan') }}",
                        data: {id : val,
                        _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                                duplicateDP();
                                let countTemp = x - 1;
                                $('#faktur_uang_muka_pelanggan_id'+countTemp).val(response.id);
                                $('#akun_id_dp'+countTemp).val(response.dp);
                                $('#penjelasan_dp'+countTemp).val(response.penjelasan);
                                $.each(response.tax, function(index, item){
                                    $('#pajak_dp'+countTemp).append( 
                                        $("<option></option>")
                                        .text(item.nilai+ "/" +item.nama)
                                        .val(item.id)
                                        );
                                });
                                $('#total_uang_muka_dp'+countTemp).autoNumeric('init',{aPad: false}).autoNumeric('set', response.saldo_awal);
                                $('#no_faktur_dp'+countTemp).val(response.no_faktur);
                                $('#no_pesanan_dp'+countTemp).val(response.no_pesanan);
                                $('#in_tax_dp'+countTemp).val(response.in_tax_texted);
                                counterDp();
                                setAkun();
                            }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                });
              }
        }

        
            
        @if (old('produk_id'))
            $(document).ready(function() {
                sumQtyAndUnit(q);
            })
        @endif

        function cekGudangStock(param) {
            var goSubmit = 0;
            var gudang_stock_id = $('#gudang_id'+ param).val();
            var produk_stock_id = $('#produk_id'+ param).val();
            var qty_stock_id    = $('#qty_produk'+ param).val();
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-gudang-stock') }}",
                data: {
                    gudang_id : gudang_stock_id, produk_id : produk_stock_id, qty_id: qty_stock_id , _token: '{{ csrf_token() }}'
                },
                dataType: "json",
                success: function (response) {
                    if (response.status === 0) {
                        swal({
                            icon: 'error',
                            text: 'Stock '+ $("#keterangan_produk"+ param).val() +' di '+ response.gudang +' tersisa '+ response.kuantitas +' harap masukkan jumlah yang sesuai !!!'  
                        });
                    }
                }, failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }

        // Fungsi button cetak di klik
        $("#btn-cetak").on("click", function(e) {
            e.preventDefault();
            btnCetak()
        });

        function btnCetak() {
            if ($("#pelanggan_id").val() == "" || $('.produk_id').val() == "" || $('.produk_id').val() == undefined) {
                swal({
                    icon: "warning",
                    text: "Pelanggan/Barang Masih Kosong"
                });
            }else {
                var form = $("#form-faktur-penjualan");
                form.attr('target','_blank');
                form.attr('action','{{ route('akuntansi.cetak-faktur-penjualan') }}');
                form.submit();
            }
        }

        // Fungsi button simpan tutup dan simpan lanjut di klik
        $(".btn-block").on("click", function () {
            var form = $("#form-faktur-penjualan");
            form.removeAttr('target');
            form.attr('action','{{ route($module_url->store) }}');
            $(".btn-block").on("submit", function () {
                form.submit();
            });
        });

        $( "#pelanggan_kirim_ke" ).change(function() {
            var pelanggan_kirim_ke_id   = $('#pelanggan_kirim_ke').val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-alamat-pelanggan-kirim-ke') }}",
                data: {id : pelanggan_kirim_ke_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#alamat_pengiriman').val(response.alamat);
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        });

        </script>
        <script src="{{ asset('js/faktur_penjualan.js') }}"></script>
        <script src="{{ asset('js/add-btn-tag-activity.js') }}"></script>
    @endsection
