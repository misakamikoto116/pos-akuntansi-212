<div class="form-group row">
  <div class="col-1 col-form-label">Pelanggan</div>
  <div class="col-2">
  {!! Form::text('no_pelanggan',null,['class' => 'form-control','id' => 'no_pelanggan','readonly'])!!}
  </div>
  <div class="col-4">
  {!! Form::select('pelanggan_id',$pelanggan,old('pelanggan_id'),['placeholder' => '--Pilih--','class' => 'form-control select3','id' => 'pelanggan_id'])!!}
</div>
  <div class="col-2">
  	<div class="btn-group">
        <button type="button" class="btn btn-sm btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-list"></i>&nbsp; Option <span class="caret"></span></button>
        <div class="dropdown-menu">
            <a class="dropdown-item btn-info" data-toggle="modal" data-target="#penawaran">Pilih Penawaran</a>
            <a class="dropdown-item btn-info" data-toggle="modal" data-target="#pesanan">Pilih Pesanan</a>
            <a class="dropdown-item btn-info" data-toggle="modal" data-target="#pengiriman">Pilih Pengiriman</a>
            <a class="dropdown-item btn-info" data-toggle="modal" data-target="#uang-muka">Pilih Uang Muka</a>
            <a class="dropdown-item btn-info" data-toggle="modal" data-target="#klaim">Pilih Klaim</a>
        </div>
    </div>
    {!! Form::hidden('total',null, ['class' => 'kolom_grandtotal']) !!}
    {!! Form::hidden('total_murni',null, ['class' => 'kolom_totalmurni']) !!}
    {!! Form::hidden('delivery_date',null, ['id' => 'delivery_date']) !!}

    <!-- Modal Penawaran -->
<div class="modal fade" id="penawaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Penawaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
            <tr>
                <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
                <th data-toggle="true">No. Transaksi</th>
                <th>Tanggal</th>
            </tr>
            </thead>
            <tbody id="table-list-penawaran">
            
            </tbody>
        </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="filterSelectedMethod('penawaran')" data-dismiss="modal" class="btn btn-default">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

 <!-- Modal Pesanan -->
<div class="modal fade" id="pesanan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pesanan Penjualan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Transaksi</th>
              <th>Tanggal</th>
              <th>PO No.</th>
          </tr>
          </thead>
          <tbody id="table-list-pesanan">
            
          </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="filterSelectedMethod('pesanan')" data-dismiss="modal" class="btn btn-default">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

 <!-- Modal Pengiriman -->
<div class="modal fade" id="pengiriman" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pengiriman Pesanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Transaksi</th>
              <th>Tanggal</th>
              <th>No. Pesanan</th>
          </tr>
          </thead>
          <tbody id="table-list-pengiriman">
          </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="filterSelectedMethod('pengiriman')" data-dismiss="modal" class="btn btn-default">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

 <!-- Modal Uang Muka -->
<div class="modal fade" id="uang-muka" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Uang Muka</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Transaksi</th>
              <th>Tanggal</th>
              <th>Nilai DP</th>
          </tr>
          </thead>
          <tbody id="table-list-uangmuka">
          </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="filterUangMuka()" data-dismiss="modal" class="btn btn-default">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

 <!-- Modal Klaim -->
<div class="modal fade" id="klaim" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">RMA</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. RMA</th>
              <th>Tanggal</th>
              <th>No. Barang</th>
          </tr>
          </thead>
          <tbody>
          <tr>
              <th><input type="checkbox" id="1"/></th>
              <td>Isidra</td>
              <td>Boudreaux</td>
              <td>4444444</td>
          </tr>
        </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

  </div>
  <div class="col-3 form-inline">
      <div class="form-check form-check-inline">
      @if (isset($item))
      {!! Form::checkbox('taxable', 1, $item->taxable == 1 ? true : false,['class' => 'form-check-input taxable','id' => 'group1','onchange' => 'calculate()']) !!}
      @else
      {!! Form::checkbox('taxable', 1, true,['class' => 'form-check-input taxable','id' => 'group1','onchange' => 'calculate()']) !!}
      @endif
      <label class="form-check-label" for="group1"> Cust. is Taxable</label>
    </div>
    <div class="form-check form-check-inline">
      @if (isset($item))
      {!! Form::checkbox('in_tax', 1, $item->in_tax == 1 ? true : false,['class' => 'form-check-input group2 in_tax', 'id' => 'inlineCheckbox2', 'onchange' => 'calculate()']) !!}
      @else
      {!! Form::checkbox('in_tax', 1, null,['class' => 'form-check-input group2 in_tax', 'id' => 'inlineCheckbox2', 'onchange' => 'calculate()']) !!}
      @endif
      <label class="form-check-label" for="inlineCheckbox2"> Inclusive Tax</label>
    </div>
    </div>
</div>

<hr>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
	    <label for="">Tagihan Ke</label>
	    {!! Form::textarea('alamat_tagihan',null,['class' => 'form-control','id' => 'alamat_asal','readonly'])!!}
	  	</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
	    <label for="">Kirim Ke</label>
	    {!! Form::textarea('alamat_pengiriman',null,['class' => 'form-control','id' => 'alamat_pengiriman'])!!}
	  	</div>
      <div>
        {!! Form::select('pelanggan_kirim_ke_id',$pelanggan,null,['class' => 'form-control select2','id' => 'pelanggan_kirim_ke','placeholder' => '- Pilih -'])!!}
      </div>
	</div>
	<div class="offset-sm-2 col-sm-4">
		<div class="row">
      <div class="col-sm-4">
            <div class="form-group">
              <div class="tag">No. Faktur</div>
              {!! Form::text('no_faktur',isset($item) ? $item->no_faktur : $idPrediction,['class' => 'form-control','id' => '','required'])!!}
              </div>
      </div>
      <div class="col-sm-4">
				<div class="form-group">
			    <div class="tag">PO. No.</div>
			    {!! Form::text('po_no',isset($item) ? $item->po_no : $idPrediction,['class' => 'form-control','id' => 'po_no'])!!}
			  	</div>
			</div>
      {{-- <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">Invoice No.</div>
          {!! Form::text('invoice_no',null,['class' => 'form-control','id' => '',''])!!}
          </div>
      </div> --}}

			<div class="col-sm-4">
				<div class="form-group">
			    <div class="tag">Invoice Date</div>
			    {!! Form::text('invoice_date', null,!empty($item->invoice_date) ? ['class' => 'form-control tanggal_faktur','id' => 'invoice_date','required']
                                                                          : ['class' => 'form-control tanggal','id' => 'invoice_date','required'])!!}
			  	</div>
			</div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">Ship Date</div>
          {!! Form::text('ship_date',null, !empty($item->ship_date) ? ['class' => 'form-control tanggal_faktur','id' => 'ship_date','required']
                                                                    : ['class' => 'form-control tanggal','id' => 'ship_date','required'])!!}
          </div>
      </div>
      <div class="col-sm-4">
          <div class="form-group">
            <div class="tag">FOB</div>
            {!! Form::select('fob',['0' => 'Shiping Point', '1' => 'Destination' ],null,['placeholder'=>'--Pilih--','class' => 'form-control select3','id' => 'fob'])!!}
            </div>
        </div>
        <div class="offset-4 col-sm-4">
          <div class="form-group">
            <div class="tag tag-terms">Terms</div>
            {!! Form::select('term_id',$termin,null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'term_id'])!!}
            </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <div class="tag tag-ships">Ship Via</div>
            {!! Form::select('ship_id',$pengiriman,null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'ship_id'])!!}
            </div>
        </div>
		</div>
	</div>
  <div class="offset-sm-2 col-sm-4">
    <div class="form-group row">
      <div class="col-2 col-form-label" for="">Proyek</div>
      <div class="col-10">
      {!! Form::select('',[],null,['class' => 'form-control select3','id' => 'alamat_asal'])!!}
      </div>
      </div>
      <div class="form-group row">
      <div class="col-2 col-form-label" for="">Termin</div>
      <div class="col-10">
      {!! Form::select('',[],null,['class' => 'form-control select3','id' => 'alamat_asal'])!!}
      </div>
      </div>
  </div>
</div>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-uang-tab" data-toggle="pill" href="#pills-uang" role="tab" aria-controls="pills-uang" aria-selected="true">Uang Muka | <strong id="counterDp"></strong></a>
  </li>
</ul>

<div class="penawaran-sections" data-id="1">
<div class="duplicate-penawaran-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
    <div style="width: 2000px;">
  	<table class="table duplicate-sections">
  		<thead class="thead-light" style="border-collapse: collapse;">
  			<tr>
  				<th width="12%">Item</th>
  				<th width="12%">Item Description</th>
  				<th>Qty</th>
  				<th>Item Unit</th>
  				<th>Unit Price</th>
  				<th>Disc %</th>
  				<th>Tax</th>
  				<th>Amount</th>
  				<th>Dept</th>
          <th>Proyek</th>
          <th>Gudang</th>
  				<th>SN</th>
          <th>No SO</th>
          <th>No DO</th>
  				<th></th>
  			</tr>
  		</thead>
  		<tbody class="purchasePenawaran">
          @if(old('produk_id') !== null)  
              @foreach(old('produk_id') as $row => $produk_id )
                @include('akuntansi::faktur_penjualan/components/item_faktur', [
                  'row'                   => $row,
                  'produk_id'             => $produk_id,
                  'no_produk'             => old('no_produk')[$row],
                  'keterangan_produk'     => old('keterangan_produk')[$row],
                  'qty_produk'            => old('qty_produk')[$row],
                  'satuan_produk'         => old('satuan_produk')[$row],
                  'unit_harga_produk'     => old('unit_harga_produk')[$row],
                  'diskon_produk'         => old('diskon_produk')[$row],
                  'kode_pajak_id'         => old('kode_pajak_id')[$row],
                  'amount_produk'         => old('amount_produk')[$row],
                  'dept_id'               => old('dept_id')[$row],
                  'proyek_id'             => old('proyek_id')[$row],
                  'gudang_id'             => old('gudang_id')[$row],
                  'sn'                    => old('sn')[$row],
                  'no_so'                 => old('no_so')[$row],
                  'no_do'                 => old('no_do')[$row],
                  'harga_modal'           => old('harga_modal')[$row],
                  'harga_terakhir'        => old('harga_terakhir')[$row],
                  'selisih_diskon_amount' => old('selisih_diskon_amount')[$row],
                  'barang_id'             => old('barang_id')[$row],
                  'barang_pengiriman_penjualan_id' => old('barang_pengiriman_penjualan_id')[$row],
                ])
              @endforeach      
          @endif
      </tbody>
    </table>
    </div>
    <a class="btn btn-info add-penawaran" ><i class="fa fa-plus"> Tambah</i></a>
  </div>

  <div class="tab-pane fade" id="pills-uang" role="tabpanel" aria-labelledby="pills-uang-tab">
    <div class="dp-sections" data-id="1">
      <div class="duplicate-dp-sections">
      <table class="table" width="100%">
          <thead class="thead-light">
            <tr>
              <th>Penjelasan</th>
              <th>Pajak</th>
              <th width="20%">Total Uang Muka</th>
              <th>No Faktur</th>
              <th>No Pesanan</th>
              <th>Termasuk Pajak</th>
              <th>Opsi</th>
            </tr>
          </thead>
          <tbody class="dpList">
              @if(old('faktur_uang_muka_pelanggan_id') !== null)  
                @foreach(old('faktur_uang_muka_pelanggan_id') as $row => $faktur_uang_muka )
                  @include('akuntansi::faktur_penjualan/components/item_dp_penjualan', [
                    'row'                                 => $row,
                    'faktur_uang_muka_pelanggan_id'       => $faktur_uang_muka,
                    'akun_id_dp'                          => old('akun_id_dp')[$row],
                    'penjelasan_dp'                       => old('penjelasan_dp')[$row],
                    'pajak_dp'                            => old('pajak_dp')[$row],
                    'total_uang_muka_dp'                  => old('total_uang_muka_dp')[$row],
                    'no_faktur_dp'                        => old('no_faktur_dp')[$row],
                    'no_pesanan_dp'                       => old('no_pesanan_dp')[$row],
                    'in_tax_dp'                           => old('in_tax_dp')[$row],
                  ])
                @endforeach      
            @endif
          </tbody>
        </table>
  </div>
  </div>
</div>

</div><!-- END PILLS -->
</div>
</div>

<script type="text/template" id="table_penawaran_section" data-id="">
    @include('akuntansi::faktur_penjualan/components/item_faktur')
</script>

<script type="text/template" id="table_dp_section" data-id=""/>
  <tr>
    <td style="display: none;">
      {!! Form::hidden('faktur_uang_muka_pelanggan_id[]',null, ['class' => 'faktur_uang_muka_pelanggan_id']) !!}
      {!! Form::hidden('akun_id_dp[]', null, ['class' => 'akun_id_dp']) !!}
    </td>
    <td>{!! Form::text('penjelasan_dp[]',null,['class' => 'form-control penjelasan_dp','id' => '',''])!!}</td>
    <td width="12%">
      {!! Form::select('pajak_dp[][]',[], null, ['class' => 'form-control select3 select2-multiple pajak_dp','multiple' => 'multiple','multiple','id' => '',''])!!}    
    </td>
    <td>{!! Form::text('total_uang_muka_dp[]',null,['class' => 'form-control total_uang_muka_dp','id' => '','onchange' => 'counterDP()'])!!}</td>
    <td>{!! Form::text('no_faktur_dp[]',null,['class' => 'form-control no_faktur_dp','id' => '','readonly'])!!}</td>
    <td>{!! Form::text('no_pesanan_dp[]',null,['class' => 'form-control no_pesanan_dp','id' => '','readonly'])!!}</td>
    <td>{!! Form::text('in_tax_dp[]',null,['class' => 'form-control col-form-label in_tax_dp','id' => '','readonly'])!!}</td>
    <td><button href="" class="btn btn-danger remove-kolom-dp" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>

<hr>
<div class="row">
	<div class="col-sm-4">
    
    <div class="form-group">
      <label for="exampleFormControlTextarea1">No FP Std</label>
      <div class="form-group row">
        <div class="col-3 col-form-label text-right">{!! Form::text('no_fp_std', old("no_fp_std"), ["class" => "form-control"]) !!}</div>
        <div class="col-4 col-form-label text-right">{!! Form::text('no_fp_std2', old("no_fp_std2"), ["class" => "form-control"]) !!}</div>
        <div class="col-5" style="margin-top:5px;">{!! Form::date('no_fp_std_date', isset($item) ? \Carbon\Carbon::parse($item->no_fp_std_date) : old('no_fp_std_date'), ['class' => 'form-control']) !!}</div>
      </div>
    </div>

	 	<div class="form-group">
	    <label for="exampleFormControlTextarea1">Description</label>
	    {!! Form::textarea('catatan',null,['class' => 'form-control','id' => 'catatan'])!!}
    </div>
    
    <div class="form-group akun_ongkir" >
      <label for="exampleFormControlTextarea1">Freight Account</label>
      <div class="form-group row">
        <div class="col-6 col-form-label text-right">{!! Form::text('akun_ongkir_kode', old("akun_ongkir_kode"), ["class" => "form-control akun_kode",'readonly']) !!}</div>
        <div class="col-6" style="margin-top:5px;">{!! Form::select('akun_ongkir_id', $akun, old('akun_ongkir_id'), ['class' => 'form-control akun_id','placeholder' => '-- Pilih Akun --']) !!}</div>
      </div>
    </div>

    <div class="form-group">
      <label for="exampleFormControlTextarea1">Akun Piutang</label>
      {!! Form::select('akun_piutang_id',$akun,!isset($item) ? $getAkunPiutang : null,['placeholder'=>'--Pilih--','class' => 'form-control akun_id','id' => 'catatan','required' => 'required'])!!}
    </div>
    <hr>
    <div class="row">
    <div class="col-5 col-form-label">Syarat Diskon</div>
    <div class="col-7 col-form-label"><strong>0</strong></div>
    </div>
	</div>
	<div class="offset-sm-4 col-sm-4">
		<div class="form-group row">
		<div class="offset-2 col-3 col-form-label text-right"><strong>Sub Total</strong></div>
		<div class="col-7 text-right col-form-label" id="subtotal"><strong>0</strong></div>
    {!! Form::hidden('subTotal',null, ['class' => 'kolom_subtotal']) !!}
		</div>
		<div class="form-group row">
			<div class="col-2 col-form-label">Discount:</div>
			<div class="col-3 col-form-label">{!! Form::text('diskon',isset($item) ? $item->diskon : 0,['class' => 'form-control','id' => 'diskonTotal', 'onchange' => 'calculate()'])!!}</div>
			<div class="col-2 col-form-label text-center">% =</div>
			<div class="col-5 col-form-label">{!! Form::text('jumlah_diskon_faktur', isset($item) ? $item->jumlah_diskon_faktur : 0,['class' => 'form-control mask','id' => 'totalPotonganRupiah','onchange' => 'calculate()'])!!}</div>
    </div>
    <div class="form-group row">
			<div class="col-5 col-form-label"></div>
			<div class="col-5 col-form-label" id="taxNaration"></div>
      {!! Form::hidden('tax_cetak0', null,['id' => 'tax_cetak0'])!!}
      {!! Form::hidden('tax_cetak1', null,['id' => 'tax_cetak1'])!!}
      {!! Form::hidden('tax_cetak_label0', null,['id' => 'tax_cetak_label0'])!!}
      {!! Form::hidden('tax_cetak_label1', null,['id' => 'tax_cetak_label1'])!!}
		</div>
		<div class="form-group row" style="margin-top: 50px;">
		<div class="offset-2 col-3 col-form-label text-right">Freight</div>
		<div class="col-7">{!! Form::text('ongkir',isset($item) ? $item->ongkir : 0,['class' => 'form-control mask','id' => 'ongkir','onchange'=>'calculate()','required'])!!}</div>
		</div>
		<div class="form-group row">
		<div class="offset-1 col-4 col-form-label text-right"><strong>Total Invoice</strong></div>
    <div class="col-7 text-right col-form-label grandtotal"><strong>0</strong></div>
		</div>
	</div>
</div>

@include('akuntansi::pesanan_modal/modal_barang')