@extends( auth()->user()->hasPermissionTo('ubah_faktur_penjualan') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
<link href="{{asset('assets/plugins/footable/css/footable.core.css')}}" rel="stylesheet">
<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788;
        color: white;
        text-align: center;
        position: relative;
        top: 4px;
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
    .warnatr{
        background: lightsteelblue;
        color: black;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Penjualan</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Faktur Penjualan</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Sales Invoice / Faktur Penjualan</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left','id' => 'form-penjualan-penawaran']) !!}
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary', 'id' => 'btn-cetak']) !!}
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" id="btn-submit" disabled data-toggle="dropdown">
                    Simpan <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            {!! Form::submit('Terima Bayaran', ['class' => 'btn btn-success btn-block','value' => '1', 'name' => 'lanjutkan']) !!}
                        </li>
                        <li>
                            {!! Form::button('Simpan & Tutup',['type' => 'submit','class' => 'btn btn-warning btn-block']) !!}
                        </li>
                    </ul>
                </div>
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger']) !!}
                
                </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

        <!--FooTable-->
        <script src="{{ asset('assets/plugins/footable/js/footable.all.min.js')}}"></script>

        <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>

        <!--FooTable Example-->
        <script src="{{ asset('assets/pages/jquery.footable.js')}}"></script>

        <script type="text/javascript">
        $('#selectAll').click(function(e){
            var table= $(e.target).closest('table');
            $('th input:checkbox',table).prop('checked',this.checked);
        });
        </script>
        <script>
            var x = 0;
                $('.tanggal_faktur').datepicker({format: 'dd MM yyyy', autoclose: true});
                var q = 0;

                $('document').ready(function(){
                    // checkbox taxable
                    enable_cb();
                    $("#group1").click(enable_cb);
                    $("#pelanggan_id").select2({
                        tags: true
                    });
                    // trigger cek satu2 pajak itemnya
                    let form_old = "{{ old('pelanggan_id') }}";
                    if(form_old == ""){
                        setDataPelanggan('edited');
                    }
                    cekPajak();
                });

            function counterDp(){
                var pengurang = 0;                    
                var sum_dp = 0;
                $('.faktur_uang_muka_pelanggan_id').each(function(){
                    let id = this.value;
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.check-dp') }}",
                        data: {id : id,
                            _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){ 
                            pengurang += response;
                            $('#counterDp').html(pengurang);
                        }
                    });
                });
            }
        
            function insertItemToForm(vale, item, target) {
                 var url = "{{ route('akuntansi.get-produk-by-val') }}";
                 var pelanggan_id = $("#pelanggan_id").val();                                    
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: {id : item, type : target, pelanggan_id : pelanggan_id,
                            _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                        
                        $('#tax_produk'+vale).find('option').remove().end();                        
                        $('#gudang_id'+vale).find('option').remove().end();                        
                        $('#keterangan_produk'+vale).val(response.keterangan);
                        $('#ship_id').val(response.ship_id);
                        $('#delivery_date').val(response.delivery_date);
                        $('#term_id').val(response.term_id);
                        $('#po_no').val(response.po_no);
                        $('#fob').val(response.fob);
                        $('#qty_produk'+vale).val(response.jumlah);
                        $('#satuan_produk'+vale).val(response.satuan);
                        $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
                        $('#amount_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0); 
                        $('#diskon_produk'+vale).val(response.diskon);
                        $('#terproses'+vale).val(response.terproses);
                        $('#no_so'+vale).val(response.no_so);
                        $('#no_do'+vale).val(response.no_do);
                        if(typeof(response.harga_modal) == 'object'){
                            $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
                            $('#harga_terakhir'+vale).val(response.harga_modal.harga_terakhir);
                        }else{
                            $('#harga_modal'+vale).val(response.harga_modal);
                        }
                        $('#selisih_diskon_amount'+vale).val(0);
                        if(response.ditutup == 1){
                            $('#ditutup'+vale).prop('checked', true);
                        }
                        $.each(response.tax, function (index, item){
                            if(target == target){
                                if(response.kode_pajak_id == item.id || response.kode_pajak_2_id == item.id){
                                    $('#tax_produk'+vale).append( 
                                    $("<option></option>")
                                    .text(item.nilai+ " / " +item.nama)
                                    .val(item.id)
                                    .attr('selected','selected')
                                    );
                                }else{
                                    $('#tax_produk'+vale).append( 
                                    $("<option></option>") 
                                    .text(item.nilai+ " / " +item.nama)
                                    .val(item.id)
                                    );
                                }
                            }else{
                                $('#tax_produk'+vale).append( 
                                    $("<option></option>") 
                                   .text(item.nilai+ " / " +item.nama)
                                   .val(item.id)
                                );
                            }   
                            
                        });
                        $(response.multiGudang).each(function (val, text) {
                            $(text.gudang).each(function (index, item) {
                                $('#gudang_id'+vale).append(`<option value="${item.id}">${item.nama}</option>`);
                            });
                        });
                        if (response.taxable.taxable == 1) {
                            $("#group1").prop('checked', true);
                            $("#inlineCheckbox2").removeAttr('disabled');
                        }else if(response.taxable.taxable == 0 || response.taxable.taxable === null) {
                            $("#group1").prop('checked', false);
                            $("#inlineCheckbox2").attr('disabled','disabled');
                        }
                        if (response.taxable.in_tax == 1) {
                            $("#inlineCheckbox2").prop('checked', true);
                        }else if(response.taxable.in_tax == 0 || response.taxable.in_tax  ===null){
                            $("#inlineCheckbox2").prop('checked', false);
                        }
                        sumQtyAndUnit(vale);
                        
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
            }
        $( "#pelanggan_id" ).change(function() {
            setDataPelanggan();
            
            $('.dpList').find('tr').remove().end();   
            $('.purchasePenawaran').find('tr').remove().end();   
            $('#counterDp').html(0);         
        });
        function test(vale) {
            var pelanggan_id = $('#pelanggan_id').val();
            var produk_id   = $('#produk_id'+vale).val();
                  $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-produk') }}",
                    data: {id : produk_id, pelanggan_id : pelanggan_id,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                    $('#tax_produk'+vale).find('option').remove().end();   
                    $('#gudang_id'+vale).find('option').remove().end();                                                                 
                    $('#keterangan_produk'+vale).val(response.keterangan);
                    $('#qty_produk'+vale).val('1');
                    $('#satuan_produk'+vale).val(response.satuan);
                    if (response.unitPrice) {
                        $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
                    }else {
                        $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', 0);
                    }
                    $('#diskon_produk'+vale).val('0');
                    $('#selisih_diskon_amount'+vale).val(0);
                    if (response.harga_modal) {
                        $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
                        $('#harga_terakhir'+vale).val(response.harga_modal.harga_terakhir);
                    }else {
                        $('#harga_modal'+vale).val('0');
                    }
                    if (response.tax) {
                        $.each(response.tax, function (index, item){
                            $('#tax_produk'+vale).append( 
                            $("<option></option>")
                            .text(item.nilai+ "/" +item.nama)
                            .val(item.id)
                            );
                        });
                    }
                    
                    $(response.multiGudang).each(function (val, text) {
                        $(text.gudang).each(function (index, item) {
                            $('#gudang_id'+vale).append(`<option value="${item.id}">${item.nama}</option>`);
                        });
                    });

                    sumQtyAndUnit(vale);
                    
                }, failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
        }

        /** Ketika dipilih salah satu dari pengiriman, pesanan dll maka eksekutornya disini **/
        function filterSelectedMethod(target){
            
            var checkCounted = $('input[name="'+target+'[]"]:checked').length;
            var input = $('input[name="'+target+'[]"]:checked');
            
            if(checkCounted > 0){
                input.each(function() {
                    var val = $(this).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-detail-barang') }}",
                        data: {id : val, type : target,
                        _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                            $.each(response.barang, function(index, item){
                                duplicateForm();
                                let countTemp = q - 1;
                                callSelect2AjaxProdukID(countTemp, item.produk_id);
                                $('#produk_id'+countTemp).val(item.produk_id); 
                                $('#produk_id_temp'+countTemp).val(item.produk_id); 
                                insertItemToForm(countTemp, item.id, target); //-1 karena diatas (diplicateForm) sdh di increment
                                $('.select3').select2();
                            });
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                });
              }
        }

        function filterUangMuka(){
            var checkCounted = $('input[name="uangmuka[]"]:checked').length;
            var input = $('input[name="uangmuka[]"]:checked');
            if(checkCounted > 0){
                input.each(function() {
                    var val = $(this).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-detail-informasi-pelanggan') }}",
                        data: {id : val,
                        _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                                duplicateDP();
                                let countTemp = x - 1;
                                $('#faktur_uang_muka_pelanggan_id'+countTemp).val(response.id);
                                $('#akun_id_dp'+countTemp).val(response.dp);
                                $('#penjelasan_dp'+countTemp).val(response.penjelasan);
                                $('#pajak_dp'+countTemp).val('0');
                                $('#total_uang_muka_dp'+countTemp).autoNumeric('init',{aPad: false}).autoNumeric('set', response.saldo_awal);
                                $('#no_pesanan_dp'+countTemp).val(response.no_pesanan);
                                $('#no_faktur_dp'+countTemp).val(response.no_faktur);
                                $('#in_tax_dp'+countTemp).val(response.in_tax_texted);
                                counterDp();
                            }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                });
              }
        }

        function cekPajak(){
            let param = {{ $item->barang->count() }};
                for(let i = 1; i <= param; i++){
                    var produk_id   = $('#produk_id'+i).val();
                    var pelanggan_id = $('#pelanggan_id').val();
                    var barang_faktur_id = $('#barang_faktur_id'+i).val();
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-produk') }}",
                        data: {id : produk_id, pelanggan_id : pelanggan_id, barang_faktur_id : barang_faktur_id,
                            _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                        if (response.tax) {
                            $.each(response.tax, function (index, item){
                                $.map(response.kode_pajak, function(val, key) {
                                    if (val.pajak_satu == item.id || val.pajak_dua == item.id) {
                                        $('#tax_produk'+i).append( 
                                            $("<option></option>") 
                                           .text(item.nilai+ " / " +item.nama)
                                           .val(item.id)
                                           .attr('selected','selected')
                                        );
                                    }else {
                                        $('#tax_produk'+i).append( 
                                            $("<option></option>") 
                                           .text(item.nilai+ " / " +item.nama)
                                           .val(item.id)
                                        );
                                    }
                                });
                            });
                        }
                        $(response.multiGudang).each(function (val, text) {
                            $(text.gudang).each(function (index, item) {
                                $('#gudang_id'+i).append(`<option value="${item.id}">${item.nama}</option>`);
                            });
                        });
                        sumQtyAndUnit(i);
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                }
        }

        function setDataPelanggan(param = null){
            var pelanggan_id = $('#pelanggan_id').val();
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-alamat-pelanggan') }}",
                data: {id : pelanggan_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#table-list-penawaran').find('tr').remove().end();
                $('#table-list-pesanan').find('tr').remove().end();
                $('#table-list-pengiriman').find('tr').remove().end();
                $('#table-list-uangmuka').find('tr').remove().end();

                $('#alamat_pengiriman').val(response.alamat);
                $('#alamat_asal').val(response.alamat);
                $('#no_pelanggan').val(response.no_pelanggan);
                $("#tax_cetak0").val(null);
                $("#tax_cetak1").val(null);
                $("#tax_cetak_label0").val(null);
                $("#tax_cetak_label1").val(null);
                if (response.penawaran) {
                    $.each(response.penawaran, function (index, item){
                        $('#table-list-penawaran').append('<tr><th><input type="checkbox" value="'+item.id+'" name="penawaran[]" id="penawaran[]"/></th><td>'+item.no_penawaran+'</td><td>'+item.tanggal+'</td></tr>');
                    });
                }
                if (response.pesanan) {
                    $.each(response.pesanan, function (index, item){
                        $('#table-list-pesanan').append('<tr><th><input type="checkbox" value="'+item.id+'" name="pesanan[]" id="pesanan[]"/></th><td>'+item.so_number+'</td><td>'+item.so_date+'</td><td>'+item.po_number+'</td></tr>');
                    });
                }
                if (response.pengiriman) {
                    $.each(response.pengiriman, function (index, item){
                        $('#table-list-pengiriman').append('<tr><th><input type="checkbox" value="'+item.id+'" name="pengiriman[]" class="pengiriman_item" id="pengiriman[]"/></th><td>'+item.po_no+'</td><td>'+item.delivery_date+'</td><td>'+item.delivery_no+'</td></tr>');
                    });
                }
                if (response.uang_muka) {
                    $.each(response.uang_muka, function (index, item){
                        if (item.updated_uang_muka > 0) {
                            $('#table-list-uangmuka').append('<tr><th><input type="checkbox" value="'+item.id+'" name="uangmuka[]" class="uangmuka_item" id="uangmuka[]"/></th><td>'+item.no_faktur+'</td><td>'+item.invoice_date+'</td><td>'+item.updated_uang_muka+'</td></tr>');
                        }
                    });
                }
                setDetailBarang({{ $item->id }})
                setUangMuka({{ $item->id }});
                    if(param == null){
                        checkAfterSetPelanggan();                    
                    }
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        function setUangMuka(id){
            var pelanggan_id = $("#pelanggan_id").val();
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-detail-barang') }}",
                data: {id : id, type : 'uang-muka', pelanggan_id : pelanggan_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $.each(response.barang, function(index, key){
                    duplicateDP();
                    let queue = index;
                    $('#faktur_uang_muka_pelanggan_id'+queue).val(key.faktur_uang_muka_pelanggan_id);
                    $('#penjelasan_dp'+queue).val('DOWN PAYMENT');
                    $('#total_uang_muka_dp'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set', key.jumlah);
                    $('#akun_id_dp'+queue).val(key.faktur_uang_muka_pelanggan.akun_dp_id); 
                    $('#no_faktur_dp'+queue).val(key.faktur_uang_muka_pelanggan.no_faktur); 
                    $('#no_pesanan_dp'+queue).val(key.faktur_uang_muka_pelanggan.po_no);
                    if (key.faktur_uang_muka_pelanggan_id.in_tax == 0 || key.faktur_uang_muka_pelanggan_id.in_tax == null) {
                        $("#in_tax_dp"+queue).val('Tidak');
                    }else {
                        $("#in_tax_dp"+queue).val('Ya');
                    }
                    $.each(response.kode_pajak, function (ind, item){
                        if(key.pajak_satu_id == item.id || response.pajak_dua_id == item.id){
                            $('#pajak_dp'+queue).append( 
                            $("<option></option>")
                            .text(item.nilai+ " / " +item.nama)
                            .val(item.id)
                            .attr('selected','selected')
                            );
                        }else{
                            $('#pajak_dp'+queue).append( 
                            $("<option></option>") 
                            .text(item.nilai+ " / " +item.nama)
                            .val(item.id)
                            );
                        }    
                    });
                    $('.select3').select2();
                    counterDp();
                });
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        function setDetailBarang(id){
            var pelanggan_id = $("#pelanggan_id").val();
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-detail-barang') }}",
                data: {id : id, type : 'faktur', pelanggan_id : pelanggan_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $.each(response.barang, function(index, key){
                    duplicateForm();
                    let queue = q - 1;
                    callSelect2AjaxProdukID(queue, key.produk.id);
                    $('#produk_id'+queue).val(key.produk.id);
                    $('#produk_id_temp'+queue).val(key.produk.id);
                    if (response.penawaran_id) {
                        $('#penawaran_id'+queue).val(response.penawaran_id.id);                        
                    }                        
                    $('#tax_produk'+queue).find('option').remove().end();                        
                    $('#keterangan_produk'+queue).val(key.produk.keterangan);
                    $('#qty_produk'+queue).val(key.jumlah);
                    $('#satuan_produk'+queue).val(key.item_unit);
                    var a = $('#unit_harga_produk'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set', key.unit_price);
                    $('#amount_produk'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set', key.harga_total); //karena defaultnya di * 1, maka cukup unitprice saja
                    $('#diskon_produk'+queue).val(key.diskon);
                    $('#barang_id'+queue).val(key.barang_pengiriman_penjualan_id);
                    $('#gudang_id'+queue).val(key.gudang_id);
                    $('#harga_modal'+queue).val(key.harga_modal);
                    $.each(response.kode_pajak, function (index, item){
                        if(item.id == key.kode_pajak_id || item.id == key.kode_pajak_2_id){
                            $('#tax_produk'+queue).append( 
                            $("<option></option>")
                            .text(item.nilai+ "/" +item.nama)
                            .val(item.id)
                            .attr('selected','selected')
                            );
                        }else {
                            $('#tax_produk'+queue).append( 
                            $("<option></option>")
                                .text(item.nilai+ "/" +item.nama)
                                .val(item.id)
                            );
                        }
                    });
                    $('.select3').select2();
                    sumQtyAndUnit(queue);
                });
                for(var a = 0; a < q; a++){
                    callSelect2AjaxProduk(a, 'modal');
                }
                setAkun();

                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        function cekGudangStock(param) {
            var goSubmit = 0;
            var gudang_stock_id = $('#gudang_id'+ param).val();
            var produk_stock_id = $('#produk_id'+ param).val();
            var qty_stock_id    = $('#qty_produk'+ param).val();
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-gudang-stock') }}",
                data: {
                    gudang_id : gudang_stock_id, produk_id : produk_stock_id, qty_id: qty_stock_id , _token: '{{ csrf_token() }}'
                },
                dataType: "json",
                success: function (response) {
                    if (response.status === 0) {
                        swal({
                            icon: 'error',
                            text: 'Stock '+ $("#keterangan_produk"+ param).val() +' di '+ response.gudang +' tersisa '+ response.kuantitas +' harap masukkan jumlah yang sesuai !!!'  
                        });
                    }
                }, failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }

        // Fungsi button cetak di klik
        $("#btn-cetak").on("click", function(e) {
            e.preventDefault();
            btnCetak()
        });

        function btnCetak() {
            if ($("#pelanggan_id").val() == "" || $('.produk_id').val() == "" || $('.produk_id').val() == undefined) {
                swal({
                    icon: "warning",
                    text: "Pelanggan/Barang Masih Kosong"
                });
            }else {
                var form = $("#form-penjualan-penawaran");
                form.attr('target','_blank');
                form.attr('action','{{ route('akuntansi.edit-cetak-faktur-penjualan') }}');
                form.submit();
            }
        }

        // Fungsi button simpan tutup dan simpan lanjut di klik
        $("#btn-submit").on("click", function () {
            var form = $("#form-penjualan-penawaran");
            form.removeAttr('target');
            form.attr('action','{{ route($module_url->update,$item->id) }}');
            $("#btn-submit").on("submit", function () {
                form.submit();
            });
        });

        $( "#pelanggan_kirim_ke" ).change(function() {
            var pelanggan_kirim_ke_id   = $('#pelanggan_kirim_ke').val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-alamat-pelanggan-kirim-ke') }}",
                data: {id : pelanggan_kirim_ke_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#alamat_pengiriman').val(response.alamat);
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        });

        </script>
        <script src="{{ asset('js/faktur_penjualan.js') }}"></script>
        <script src="{{ asset('js/add-btn-tag-activity.js') }}"></script>
@endsection
