<tr>
  <td>
    {!! Form::text('no_produk['.($row ?? 0).']', $no_produk ?? null ,['placeholder' => 'Cari Barang', 'class' => 'form-control no_produk','required', 'readonly', 'id' => 'no_produk'.($row ?? 0)])!!}
    {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id ?? null ,['class' => 'form-control produk_id','required','id' => 'produk_id'.($row ?? 0)])!!}
    {{-- {!! Form::select('produk_id['.($row ?? 0).']',[], $produk_id ?? null,['placeholder' => '-Pilih-','class' => 'form-control select3 produk_id','required','id' => 'produk_id'.($row ?? 0)])!!} --}}
    {!! Form::hidden('produk_id_temp['.($row ?? 0).']', $produk_id ?? null, ['class' => 'produk_id_temp','id' => 'produk_id_temp'.($row ?? 0)]) !!}
  </td>
  <td>{!! Form::text('keterangan_produk['.($row ?? 0).']', $keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}</td>
  <td>{!! Form::text('qty_produk['.($row ?? 0).']', $qty_produk ?? null,['class' => 'form-control qty_produk','id' => '','required'])!!}</td>
  <td>{!! Form::text('satuan_produk['.($row ?? 0).']', $satuan_produk ?? null,['class' => 'form-control satuan_produk','id' => '','required'])!!}</td>
  <td>{!! Form::text('unit_harga_produk['.($row ?? 0).']',$unit_harga_produk ?? 0,['class' => 'form-control unit_harga_produk mask','id' => '','required'])!!}</td>
  <td>{!! Form::text('diskon_produk['.($row ?? 0).']', $diskon_produk ?? 0,['class' => 'form-control diskon_produk','id' => '','required'])!!}</td>
  <td width="10%">
    {!! Form::select('kode_pajak_id['.($row ?? 0).'][]',$pajak, $kode_pajak_id ?? null,['class' => 'form-control select3 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => '',''])!!}
  </td>
  <td>{!! Form::text('amount_produk['.($row ?? 0).']',$amount_produk ?? 0,['class' => 'form-control amount_produk mask','id' => '',''])!!}</td>
  <td>{!! Form::select('dept_id['.($row ?? 0).']',[],$dept_id ?? null,['class' => 'form-control select3 dept_id','id' => '',''])!!}</td>
  <td>{!! Form::select('proyek_id['.($row ?? 0).']',[],$proyek_id ?? null,['class' => 'form-control select3 proyek_id','id' => '',''])!!}</td>
  <td>{!! Form::select('gudang_id['.($row ?? 0).']',$gudang,$gudang_id ?? null,['class' => 'form-control select3 gudang_id','id' => 'gudang_id',''])!!}</td>
  <td>{!! Form::text('sn['.($row ?? 0).']',$sn ?? null,['class' => 'form-control sn','id' => '',''])!!}</td>
  <td>{!! Form::text('no_so['.($row ?? 0).']',$no_so ?? null,['class' => 'form-control no_so','id' => '','readonly' => ''])!!}</td>
  <td>
    {!! Form::text('no_do['.($row ?? 0).']',$no_do ?? null,['class' => 'form-control no_do','id' => '','readonly' => ''])!!}
    {!! Form::hidden('harga_modal['.($row ?? 0).']',$harga_modal ?? null,['class' => 'form-control harga_modal'])!!}
    {!! Form::hidden('harga_terakhir['.($row ?? 0).']',$harga_terakhir ?? null,['class' => 'form-control harga_terakhir'])!!}
    {!! Form::hidden('selisih_diskon_amount['.($row ?? 0).']',$selisih_diskon_amount ?? null,['class' => 'form-control selisih_diskon_amount'])!!}
  </td>
  <td style="display: none;">{!! Form::hidden('barang_id['.($row ?? 0).']', $barang_id ?? null, ['class' => 'barang_id']) !!}</td>
  <td style="display: none;">{!! Form::hidden('barang_pengiriman_penjualan_id['.($row ?? 0).']', $barang_pengiriman_penjualan_id ?? null, ['class' => 'barang_pengiriman_penjualan_id']) !!}</td>
  <td><button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>