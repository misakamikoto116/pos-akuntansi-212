<tr>
  <td>
      {!! Form::hidden('faktur_uang_muka_pelanggan_id['.($row ?? 0).']', $faktur_uang_muka_pelanggan ?? null, ['class' => 'faktur_uang_muka_pelanggan_id','id' => 'faktur_uang_muka_pelanggan_id'.$row]) !!}
    {!! Form::hidden('akun_id_dp['.($row ?? 0).']', $akun_id_dp ?? null, ['class' => 'akun_id_dp','id' => 'akun_id_dp'.$row]) !!}
  {!! Form::text('penjelasan_dp['.($row ?? 0).']', $penjelasan_dp ?? null,['class' => 'form-control penjelasan_dp','id' => 'penjelasan_dp'.$row,''])!!}</td>
  <td width="12%">
    {!! Form::select('pajak_dp[]['.($row ?? 0).']', $pajak, $pajak_dp ?? null,['class' => 'form-control select2 select2-multiple pajak_dp','multiple' => 'multiple','multiple','id' => 'pajak_dp'.$row,'onchange' => 'calculate('.$row.')'])!!}    
  </td>
  <td>{!! Form::text('total_uang_muka_dp['.($row ?? 0).']', $total_uang_muka_dp ?? null,['class' => 'form-control total_uang_muka_dp','id' => 'total_uang_muka_dp'.$row,'onchange' => 'counterDp('.$row.')'])!!}</td>
  <td>{!! Form::text('no_faktur_dp['.($row ?? 0).']', $no_faktur_dp ?? null,['class' => 'form-control no_faktur_dp','id' => 'no_faktur_dp'.$row,'readonly'])!!}</td>
  <td>{!! Form::text('no_pesanan_dp['.($row ?? 0).']', $no_pesanan_dp ?? null,['class' => 'form-control no_pesanan_dp','id' => 'no_pesanan_dp'.$row,'readonly'])!!}</td>
  <td>{!! Form::text('in_tax_dp['.($row ?? 0).']', $in_tax_dp ?? null,['class' => 'form-control col-form-label in_tax_dp','id' => 'in_tax_dp'.$row,'readonly'])!!}</td>
  <td><button href="" class="btn btn-danger remove-kolom-dp" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>