@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')

<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ route('akuntansi.akun.index') }}">Daftar Penyelesaian Pesanan</a></li>
                      <li><a href="#">Edit Penyelesaian Pesanan</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left']) !!}
                @include($form)
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Perbarui',['type' => 'submit','class' => 'btn btn-default']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("custom_js")

    <!--Form Wizard-->
    <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

    <!--wizard initialization-->
    <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
    <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/starting.js') }}"></script>
    <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>
    <script src="{{ asset('js/penyelesaian_pesanan.js') }}"></script>

    <script type="text/javascript">
        $('.tanggal_edit').datepicker({format: 'dd MM yyyy', autoclose: true});
        $('.mask-edit').autoNumeric('init',{aPad: false});
        $('.select2').select2();
    </script>
@endsection