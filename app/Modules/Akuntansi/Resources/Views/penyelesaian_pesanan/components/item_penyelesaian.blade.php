@if (isset($row))
<tr>
    <td>
        {!! Form::text('no_produk['.($row ?? 0).']', $no_produk ?? null,['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required','id' => 'no_produk'.$row,'onclick' => 'awas('. $row .')', 'readonly'])!!}
        {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id ?? null,['class' => 'form-control produk_id','required','id' => 'produk_id'.$row])!!}
        {!! Form::hidden('produk_id_temp['.($row ?? 0).']', $produk_id ?? null, ['id' => 'produk_id_temp'.$row]) !!}
    </td>
    <td>{!! Form::text('keterangan_produk['.($row ?? 0).']',$keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.$row,'required'])!!}</td>
    <td>{!! Form::text('qty_produk['.($row ?? 0).']',$qty_produk ?? null,['class' => 'form-control qty_produk','id' => 'qty_produk'.$row,'onchange' => 'sumQtyAndUnit()','required'])!!}</td>
    <td>{!! Form::text('amount_produk['.($row ?? 0).']',$amount_produk ?? null,['class' => 'form-control mask amount_produk','id' => 'amount_produk'.$row,''])!!}</td>
    <td>{!! Form::text('alokasi_nilai['.($row ?? 0).']',$alokasi_nilai ?? null,['class' => 'form-control alokasi_nilai','id' => 'alokasi_nilai'.$row,'required'])!!}</td>
    <td>{!! Form::text('presentase_produk['.($row ?? 0).']',$presentase_produk ?? 0,['class' => 'form-control presentase_produk','id' => 'presentase_produk'.$row,'onchange' => 'sumQtyAndUnit()','required'])!!}</td>
    <td>{!! Form::select('gudang_id['.($row ?? 0).']',$gudang,$gudang_id ?? null,['class' => 'form-control select2 gudang_id','id' => 'gudang_id'.$row,''])!!}</td>
    <td>{!! Form::text('sn['.($row ?? 0).']',$sn ?? null,['class' => 'form-control sn','id' => 'sn'.$row,''])!!}</td>
    <td><button href="" class="remove btn btn-danger remove-rincian-item" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>
@else
<tr>
    <td>
        {!! Form::text('no_produk['.($row ?? 0).']', $no_produk ?? null,['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required','id' => 'no_produk', 'readonly'])!!}
        {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id ?? null,['class' => 'form-control produk_id','required','id' => ''])!!}
        {!! Form::hidden('produk_id_temp['.($row ?? 0).']', $produk_id ?? null, ['id' => '']) !!}
    </td>
    <td>{!! Form::text('keterangan_produk['.($row ?? 0).']',$keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('qty_produk['.($row ?? 0).']',$qty_produk ?? null,['class' => 'form-control qty_produk','id' => '','onchange' => 'sumQtyAndUnit()','required'])!!}</td>
    <td>{!! Form::text('amount_produk['.($row ?? 0).']',$amount_produk ?? null,['class' => 'form-control mask amount_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('alokasi_nilai['.($row ?? 0).']',$alokasi_nilai ?? null,['class' => 'form-control alokasi_nilai','id' => '','required'])!!}</td>
    <td>{!! Form::text('presentase_produk['.($row ?? 0).']',$presentase_produk ?? 0,['class' => 'form-control presentase_produk','id' => '','onchange' => 'sumQtyAndUnit()','required'])!!}</td>
    <td>{!! Form::select('gudang_id['.($row ?? 0).']',$gudang,$gudang_id ?? null,['class' => 'form-control select2 gudang_id','id' => 'gudang_id',''])!!}</td>
    <td>{!! Form::text('sn['.($row ?? 0).']',$sn ?? null,['class' => 'form-control sn','id' => '',''])!!}</td>

    <td><button href="" class="remove btn btn-danger remove-rincian-item" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>
@endif