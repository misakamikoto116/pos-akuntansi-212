<tr>
    <td>
        {!! Form::text('no_produk[]', $detail_penyelesaian->produk_id ?? null,['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required','id' => 'no_produk'.$key,'onclick' => 'awas('. $key .')', 'readonly'])!!}
        {!! Form::hidden('produk_id[]', $detail_penyelesaian->produk_id ?? null,['class' => 'form-control produk_id','required','id' => 'produk_id'.$key])!!}
        {!! Form::hidden('produk_id_temp[]', $detail_penyelesaian->produk_id ?? null, ['id' => 'produk_id_temp'.$key]) !!}
    </td>
    <td>{!! Form::text('keterangan_produk[]', $detail_penyelesaian->produk->keterangan ?? null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.$key,'required'])!!}</td>
    <td>{!! Form::text('qty_produk[]',$detail_penyelesaian->kuantitas ?? null,['class' => 'form-control qty_produk','id' => 'qty_produk'.$key,'onchange' => 'sumQtyAndUnit()','required'])!!}</td>
    <td>{!! Form::text('amount_produk[]',$detail_penyelesaian->biaya ?? null,['class' => 'form-control mask amount_produk mask-edit','id' => 'amount_produk'.$key,''])!!}</td>
    <td>{!! Form::text('alokasi_nilai[]',$detail_penyelesaian->alokasi_nilai ?? null,['class' => 'form-control alokasi_nilai mask-edit','id' => 'alokasi_nilai'.$key,'required'])!!}</td>
    <td>{!! Form::text('presentase_produk[]',$detail_penyelesaian->presentase ?? 0,['class' => 'form-control presentase_produk','id' => 'presentase_produk'.$key,'onchange' => 'sumQtyAndUnit()','required'])!!}</td>
    <td>{!! Form::select('gudang_id[]',$gudang,$detail_penyelesaian->gudang_id ?? null,['class' => 'form-control select2 gudang_id','id' => 'gudang_id'.$key,''])!!}</td>
    <td>{!! Form::text('sn[]',$detail_penyelesaian->sn ?? null,['class' => 'form-control sn','id' => 'sn'.$key,''])!!}</td>
    <td><button href="" class="remove btn btn-danger remove-rincian-item" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>