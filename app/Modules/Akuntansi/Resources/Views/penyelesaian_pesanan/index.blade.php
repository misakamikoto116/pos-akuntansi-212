@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')

<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Daftar Penyelesaian Pesanan</a></li>
                    </ol>

                </div>
            </div>
<!-- END Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Daftar Penyelesaian Pesanan</h5>
            <div class="menu-header">
            <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                <span class="btn-label"><i class="fa fa-plus"></i></span>
                Tambah
            </a>
            </div>
            </div>
            <div class="card-body">
                @if ($items->isEmpty())
                    <div class="alert alert-warning"> Tidak ada data. </div>
                @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No. Pesanan</th>
                                <th>Tanggal Selesai</th>
                                <th>Tipe Penyelesaiain</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->pembiayaanPesanan->batch_no }}</td>
                                    <td>{{ \Carbon\Carbon::parse($item->tgl_selesai)->format('d F Y') }}</td>
                                    <td>{!! $item->tipe_penyelesaian_formatted !!}</td>
                                    <td>
                                    {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                        <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i>&emsp;Edit</a>
                                            <a class="dropdown-item deleteBtn" href="#">
                                                <i class="fa fa-trash"></i>&emsp;Delete
                                            </a>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                <div class="pull-right">
                    {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                </div>
            </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection

@section('custom_js')
    <script type="text/javascript">
        $('.mask_duit').autoNumeric('init',{aSign: "Rp "});
    </script>
@endsection
