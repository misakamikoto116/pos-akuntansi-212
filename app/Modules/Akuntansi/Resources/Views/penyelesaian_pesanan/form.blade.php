<div class="form-group row">
	<div class="col-2 col-form-label">No. Pesanan</div>
	<div class="col-10">
		{!! Form::select('pembiayaan_pesanan_id',$pembiayaan, !empty($item) ? $item->pembiayaan_pesanan_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'pembiayaan_id','onchange' => 'getPembiayaan(this.value)'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('tanggal','Tanggal',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
		{!! Form::text('date',null,['class' => 'form-control tanggal','id' => 'date','readonly'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('akun_pembiayaan_pesanan','Akun Pembiayaan Pesanan',['class' => 'col-2 col-form-label']) !!}
	<div class="col-3">
		{!! Form::text('akun',null,['class' => 'form-control','id' => 'akun','readonly'])!!}
	</div>
	<div class="col-7">
		{!! Form::select('akun_pembiayaan_pesanan_id',[''=>'Pilih Akun']+$akun,null,['class' => 'form-control select2','id' => 'akun_id','onchange'=>'setNamaAkunMain()','readonly'])!!}
	</div>
</div>

<div class="form-group row">
	{!! Form::label('keterangan','Keterangan',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	    {!! Form::textarea('keterangan',null,['class' => 'form-control','id' => 'keterangan','readonly'])!!}
	</div>
</div>

<div class="form-group row">
	{!! Form::label('total','Total Pembiayaan Pesanan',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	    {!! Form::text('total',null,['class' => 'form-control','id' => 'total','readonly'])!!}
	</div>
</div>

<br>
<hr>
<br>

<div class="form-group row">
	{!! Form::label('tipe_penyelesaian','Tipe Penyelesaian',['class' => 'col-2 col-form-label']) !!}
	<div class="col-5">		
		<div class="radio radio-primary">
			{!! Form::radio('tipe_penyelesaian', 0, false,['id' => 'tipe_penyelesaian_account','onclick' => 'changeTab("account")']) !!}
			<label for="tipe_penyelesaian_account">
				To Account
			</label>
		</div>
		<div class="radio radio-primary">
			{!! Form::radio('tipe_penyelesaian', 1, false,['id' => 'tipe_penyelesaian_item','onclick' => 'changeTab("item")']) !!}
			<label for="tipe_penyelesaian_item">
				To Item
			</label>
		</div>
	</div>
	<div class="col-1">
		<label for="">Tgl Selesai</label>	
		<br>	
	</div>
	<div class="col-4">
		{!! Form::text('tgl_selesai',null, !empty($item) ? ['class' => 'form-control tanggal_edit','id' => 'tgl_selesai'] : ['class' => 'form-control tanggal','id' => 'tgl_selesai'])!!}
		<br>
	</div>
</div>
<br>
<div class="item-sections" data-id="1">
<div class="duplicate-item-sections tipe-item">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
  <div style="width: 2000px;">
  	<table width="100%" class="table">
  		<thead class="thead-light" style="border-collapse: collapse;">
  			<tr>
  				<th width="13%">Item</th>
  				<th width="13%">Item Description</th>
  				<th>Qty</th>
  				<th>Biaya</th>
  				<th>Alokasi nilai</th>
  				<th>%</th>
  				<th>Gudang</th>
  				<th>SN</th>
  				<th></th>
  			</tr>
  		</thead>
  		<tbody class="purchaseItem">
  			@if(old('produk_id') !== null)  
				@foreach(old('produk_id') as $row => $produk_id )
		          @include('akuntansi::penyelesaian_pesanan/components/item_penyelesaian',[
		          	'row' 				=> $row,
					'produk_id' 		=> $produk_id,
					'no_produk'			=> old('no_produk')[$row],
		            'keterangan_produk' => old('keterangan_produk')[$row],
		            'qty_produk' 		=> old('qty_produk')[$row],
		            'amount_produk' 	=> old('amount_produk')[$row],
		            'alokasi_nilai' 	=> old('alokasi_nilai')[$row],
		            'presentase_produk' => old('presentase_produk')[$row],
		            'gudang_id' 		=> old('gudang_id')[$row],
		            'sn' 				=> old('sn')[$row],
		          ])
		        @endforeach
		    @elseif(!empty($item->item))
		    	@foreach ($item->item as $key => $detail_penyelesaian)
		    		@include('akuntansi::penyelesaian_pesanan/components/item_penyelesaian_edit', [$detail_penyelesaian,$key])
    	      	@endforeach      
		    @endif
  		</tbody>
  	</table>
  </div>
  	<a class="btn btn-info add-item"><i class="fa fa-plus"> Tambah</i></a>
  </div>

</div><!-- END PILLS -->
</div>

<div class="tab-content tipe-account" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
		<table width="100%" class="table">
			<tr>
				<th width="50%">Kode Akun</th>
				<th>Nama Akun</th>
			</tr>
			<tr>
				<td>{!! Form::text('nama_akun_item', null, ['class' => 'nama_akun_item form-control','readonly']) !!}</td>
				<td>{!! Form::select('kode_akun_item',[''=>'Pilih Akun']+$akun, !empty($item->akun) ? $item->akun->detail_akun_id : null, ['class' => 'kode_akun_item form-control select2','onchange' => 'setNamaAkun()']) !!}</td>
			</tr>
		</table>
  </div>
</div>

</div>

<br>
<br>

@include('akuntansi::pesanan_modal.modal_barang')

<script type="text/template" id="table_item_section" data-id="">
  @include('akuntansi::penyelesaian_pesanan/components/item_penyelesaian')
</script>