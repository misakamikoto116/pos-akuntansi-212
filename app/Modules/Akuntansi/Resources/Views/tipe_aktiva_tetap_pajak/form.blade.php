<div class="form-group row">
	{!! Form::label('tipe_aktiva_tetap_pajak','Tipe Aktiva Tetap Pajak',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::text('tipe_aktiva_tetap_pajak',null,['class' => 'form-control','id' => 'tipe_aktiva_tetap_pajak'])!!}
</div>
</div>
<div class="form-group row">
	{!! Form::label('metode_penyusutan_id','Metode Penyusutan',['class' => 'col-2 col-form-label']) !!}
	<div class="col-md-10">
	{!! Form::select('metode_penyusutan_id',$listMetodePenyusutan,null,['class' => 'form-control target select2','id' => 'metode_penyusutan_id'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('estimasi_umur','Estimasi Umur',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::number('estimasi_umur',null,['class' => 'form-control test','id' => 'estimasi_umur'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('tarif_penyusutan','Tarif Penyusutan',['class' => 'col-2 col-form-label']) !!}
	<div class="col-2">
	{!! Form::number('tarif_penyusutan',null,['class' => 'form-control','id' => 'tarif_penyusutan','readonly'])!!}
	</div>
</div>
