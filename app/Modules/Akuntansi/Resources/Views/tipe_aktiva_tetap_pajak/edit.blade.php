@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ url('akuntansi/tipe-aktiva-tetap-pajak') }}">Daftar Tipe Aktiva Tetap Pajak</a></li>
                      <li><a href="#">Edit Tipe Aktiva Tetap Pajak</a></li>
                    </ol>

                </div>
            </div>
          @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Informasi</strong>
                    <ul class="list-unstyled">
                        @foreach($errors->all() as $error)
                            <li class="">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left']) !!}
                @include($form)
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Perbarui',['type' => 'submit','class' => 'btn btn-default']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script type="text/javascript">
    // ajax get kode_akun
$( ".test" ).keyup(function() {
            // alert('aas');
            var metode  = $('#metode_penyusutan_id').val();
            var umur = $('#estimasi_umur').val();
            var hasil = 0;
            if (metode == '1') {
                hasil = 100/umur ;
            }else if(metode == '2' ){
                hasil = 200/umur;
            }else{
                hasil = 0;
            }
            $('#tarif_penyusutan').val(hasil);
        });
$( ".target" ).change(function() {
            // alert('aas');
            var metode  = $('#metode_penyusutan_id').val();
            var umur = $('#estimasi_umur').val();
            var hasil = 0;
            if (metode == '1') {
                hasil = 100/umur ;
            }else if(metode == '2' ){
                hasil = 200/umur;
            }else{
                hasil = 0;
            }
            $('#tarif_penyusutan').val(hasil);
        });

</script>
@endsection