@extends('chichi_theme.layout.app')

@section('custom_css')
<link href="{{asset('assets/plugins/footable/css/footable.core.css')}}" rel="stylesheet">
<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788;
        color: white;
        text-align: center;
        position: relative;
        top: 4px;
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Pembelian</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Uang Muka Pemasok</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Suppliers Down Payment / Uang Muka Pemasok</h5>
            </div>
            <div class="card-body">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left','id' => 'formUangMuka']) !!}
                <div class="p-20">
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary' ]) !!}
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit',  'disabled' => 'disabled']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

        <!--FooTable-->
        <script src="{{ asset('assets/plugins/footable/js/footable.all.min.js')}}"></script>

        <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>

        <!--FooTable Example-->
        <script src="{{ asset('assets/pages/jquery.footable.js')}}"></script>

        <script type="text/javascript">
        $('#selectAll').click(function(e){
            var table= $(e.target).closest('table');
            $('th input:checkbox',table).prop('checked',this.checked);
        });
        </script>
        <script>
                var old_produk_id       = "{{ old('produk_id') ?? null }}";
                var old_kode_pajak_id   = {!! json_encode(old('kode_pajak_id') ?? null) !!};
                var edit_kode_pajak_id  = $("#tax_produk").val();

                $('.tanggal_faktur').datepicker({format: 'dd MM yyyy', autoclose: true});
                $('document').ready(function(){
                    // trigger cek satu2 pajak itemnya
                    dataPemasok();
                    sumQtyAndUnit();
                });

                if (old_produk_id !== '') {
                    $(document).ready(function() {
                        enable_cb();
                        sortPajakPemasok({{ old('pemasok_id') }});
                        sumQtyAndUnit();
                    });
                }
        
                $(function() {
                  enable_cb();
                  $("#group1").click(enable_cb);
                });

                $( "#pemasok_id" ).change(function() {
                    dataPemasok();                    
                });

                function dataPemasok() {
                    var pemasok_id   = $('#pemasok_id').val();
                    $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-alamat-pemasok') }}",
                        data: {id : pemasok_id,
                            _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){   
                        $('#alamat_pengiriman').val(response.alamat);
                        $('#alamat_asal').val(response.alamat);
                        $('#no_pemasok').val(response.no_pemasok);
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                    sortPajakPemasokEdit(pemasok_id);
                }

                function sortPajakPemasokEdit(id) {
                    $.ajax({
                        type: "GET",
                        url: "" + base_url + "/ajax/get-sort-pajak",
                        data: {
                            id : id, type: 'pemasok',
                            _token: token_,
                        },
                        dataType: "json",
                        success: function (response) {
                            $('#tax_produk').find('option').remove().end();
                            if (response.kode_pajak) {
                                $.each(response.kode_pajak, function (index, item){
                                    if (edit_kode_pajak_id !== null) {
                                        if (item.id == edit_kode_pajak_id[0] || item.id == edit_kode_pajak_id[1]) {
                                            $('#tax_produk').append( 
                                                $("<option></option>")
                                                .text(item.nilai+ "/" +item.nama)
                                                .val(item.id)
                                                .attr('selected','selected')
                                            );
                                        }else {
                                            $('#tax_produk').append( 
                                                $("<option></option>")
                                                .text(item.nilai+ "/" +item.nama)
                                                .val(item.id)
                                            );        
                                        }
                                    }else {
                                        $('#tax_produk').append( 
                                            $("<option></option>")
                                            .text(item.nilai+ "/" +item.nama)
                                            .val(item.id)
                                        );
                                    }
                                });
                            }
                        }
                    });
                }

                $(document).ready( function() {
                    $("#btn-submit").click(function(e) {
                        e.preventDefault();
                        submitForm();
                    });
                });

                function submitForm() {
                    var unit_price = $("#unit_harga_produk").val();

                    if (unit_price <= 0) {
                        swal({
                            icon: 'error',
                            text: 'Tidak dapat menyimpan, karena harga unit masih 0',
                        });
                    }else {
                        $("#formUangMuka").submit();
                    }
                };

        </script>
        <script src="{{ asset('js/uang-muka-pemasok.js') }}"></script>
@endsection
