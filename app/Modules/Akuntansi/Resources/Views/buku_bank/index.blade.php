@extends( auth()->user()->hasPermissionTo('buat_menampilkan_cetak_buku_bank') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Buku Bank</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Buku Bank</a></li>
                    </ol>

                </div>
            </div>   
<!-- END Page-Title -->   
<div class="card">
    <div class="card-header"><h5 class="title">Filter</h5>
        <div class="menu-header">
        </div>
    </div>
    <div class="card-body">
        <div class="form-group row">
            <div class="col-sm-1">
                {!! Form::label('bank','Bank',['class' => 'col-sm-2 col-form-label']) !!}
            </div>
            @if (isset($from_akun))
                <div class="col-sm-3" style="padding-left: 20px;">
                    {!! Form::label('bank_label', $listAkun ,['class' => 'form-control','id' => 'label_akun']) !!}
                    {!! Form::hidden('bank', $listAkunTarget, ['class' => 'form-control','id' => 'list_akun','onchange' => 'akunChange()']) !!}
                </div>
            @else
                <div class="col-sm-3" style="padding-left: 20px;">
                    {!! Form::select('bank',['default' => '- Pilih -']+$listAkun, null ,['class' => 'form-control select2','id' => 'list_akun','onchange' => 'akunChange()']) !!}
                </div>
            @endif
            <div class="col-sm-1">
                {!! Form::label('tanggal','Dari',['class' => 'col-sm-2 col-form-label']) !!}
            </div>
            <div class="col-sm-3">
                {!! Form::text('tanggal_dari',null,['placeholder'=>'Masukkan Tanggal','class' => 'form-control tanggal_bank input_tanggal','id' => 'tanggal_dari','onchange' => 'akunChange()'])!!}
            </div>
            <div class="col-sm-1">
                {!! Form::label('sampai','s/d',['class' => 'col-sm-2 col-form-label']) !!}
            </div>
            <div class="col-sm-3">
                {!! Form::text('tanggal_sampai',null,['placeholder'=>'Masukkan Tanggal','class' => 'form-control tanggal_bank input_tanggal','id' => 'tanggal_sampai','onchange' => 'akunChange()'])!!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Buku Bank</h5>
            <div class="menu-header">
            </div>
            </div>
            <div class="card-body">
                {{-- @if($items->isEmpty()) --}}
                    {{-- <div class="alert alert-warning"> Tidak ada data. </div> --}}
                {{-- @else --}}
                <table class="table" id="tblProducts">
                    <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>No Sumber</th>
                        <th>No Cek</th>
                        <th>Keterangan</th>
                        <th style="text-align: center;">Pemasukan (Dr)</th>
                        <th style="text-align: center;">Pengeluaran (Cr)</th>
                        <th style="text-align: center;">Saldo</th>
                        <th style="text-align: center;">Terekonsiliasi</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @if (isset($semua_transaksi))
                            @include('akuntansi::buku_bank.content')
                        @endif
                    </tbody>
                </table>
                {{-- @endif --}}
                {{-- <div class="pull-right"> --}}
                    {{-- {!! $items->links('vendor.pagination.bootstrap-4'); !!} --}}
                {{-- </div> --}}
            </div>
        </div>
    </div>
</div>  

@endsection

@section('custom_js')
<script type="text/javascript">
        var formattanggal={
            format: 'dd MM yyyy',
            orientation : "bottom",
            autoclose: true,
        };
        $('.tanggal_bank').datepicker(formattanggal);

        function akunChange() {
            var _token = $("input[name='_token']").val();
            var list_akun = $("#list_akun").val();
            $("#tblProducts tbody tr").remove();
            if ( "{{ isset($from_akun) }}" ) {
                var urlBuku = "{{ route('akuntansi.akun-buku-bank') }}"
            }else{
                var urlBuku = "{{ route('akuntansi.get_buku_bank_value') }}";
            }
            if (list_akun == 'default') {
                $('.tanggal_bank').val('');
            }else {
                $.ajax({
                    type: "GET",
                    url: urlBuku,
                    data: {
                        id: list_akun,
                        tanggal_dari : $("#tanggal_dari").val(),
                        tanggal_sampai : $("#tanggal_sampai").val(),
                        _token: _token
                    },
                    success: function(data) {
                        $('#tblProducts tbody').html(data);
                        $('.mask_duit').autoNumeric('init',{aPad: false});
                    },
                    failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }
</script>
@endsection