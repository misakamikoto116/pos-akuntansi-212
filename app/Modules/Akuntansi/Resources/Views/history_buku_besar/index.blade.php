@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')

<!-- Page-Title -->
            <div class="row">
                @if ($module_url !== null)
                    @include('chichi_theme.layout.filter')
                @else
                    @include('chichi_theme.layout.filter_history_buku_besar_custom')
                @endif
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Daftar History Buku Besar</a></li>
                    </ol>

                </div>
            </div>
<!-- END Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Daftar History Buku Besar</h5>
            <div class="menu-header">
            </div>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Sumber</th>
                            <th>No. Sumber</th>
                            <th>No. Akun</th>
                            <th>Nama Akun</th>
                            <th>Keterangan</th>
                            <th>Debit</th>
                            <th>Kredit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td>{{ $item->tanggal_formatted ?? $item['tanggal_formatted']}}</td>
                                <td>{{ $item->sumber ?? $item['sumber'] }}</td>
                                <td>{{ $item->no_transaksi ?? $item['no_transaksi'] }}</td>
                                <td>{{ $item['kode_akun'] ?? (empty($item->akun) ? "-" : $item->akun->kode_akun) }}</td>
                                <td>{{ $item['nama_akun'] ?? (empty($item->akun) ? "-" : $item->akun->nama_akun) }}</td>
                                <td>{{ $item->Keterangan ?? $item['keterangan'] }}</td>
                                <td>@if($item['status'] == 1 ?? $item->status == 1 ) {{ $item->nominal_formatted ?? $item['nominal'] }} @endif</td>
                                <td>@if($item['status'] == 0 ?? $item->status == 0 ) {{ $item->nominal_formatted ?? $item['nominal'] }} @endif</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @if ($module_url !== null)
                    <div class="pull-right">
                        {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                    </div>
                @endif
            </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection

@section('custom_js')
    <script type="text/javascript">
        $('.mask_duit').autoNumeric('init',{aSign: "Rp "});
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
          $('.checkbox-custom').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            increaseArea: '20%' // optional
          });
        });
    </script>
@endsection
