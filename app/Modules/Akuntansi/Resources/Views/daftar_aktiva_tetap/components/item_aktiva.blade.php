<tr>
	<td>{!! Form::select('akun_pengeluaran_aktiva_id['.($row ?? 0).']',$listAkun, $akun_aktiva_id ?? null,['placeholder' => '-Pilih-','class' => 'form-control select2 akun_id', 'required','onchange' => 'changeTab('. $row .')','id' => 'akun_id'. $row .''])!!}</td>
	<td>{!! Form::text('tanggal_pengeluaran['.($row ?? 0).']', $tanggal_pengeluaran ?? null,['class' => 'form-control tanggal_pengeluaran','id' => 'tanggal_pengeluaran'. $row .'',''])!!}</td>
	<td>{!! Form::text('keterangan_pengeluaran['.($row ?? 0).']', $keterangan_pengeluaran ?? null,['class' => 'form-control keterangan_pengeluaran','id' => 'keterangan_pengeluaran'. $row .''])!!}</td>
	<td>{!! Form::text('jumlah_pengeluaran['.($row ?? 0).']', $jumlah_pengeluaran ?? null,['class' => 'form-control jumlah_pengeluaran','id' => 'jumlah_pengeluaran'. $row .'','onblur' => 'checkJumlahPengeluaran('. $row .')'])!!}</td>
	<td><button href="" class="remove btn btn-danger remove-rincian-pengeluaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>