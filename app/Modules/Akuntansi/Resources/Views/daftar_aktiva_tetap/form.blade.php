<div class="form-group row">
	<small>
		<i style="color:crimson">Tanda * Wajib Diisi!</i>
	</small>	
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Kode Aktiva <i style="color:crimson">*</i></div>
  	<div class="col-3">
  		{!! Form::text('kode_aktiva',null,['class' => 'form-control','id' => 'kode_aktiva'])!!}
	  </div>
	  <div class="col-3 label-code-activa">
	</div>
</div>
<div class="form-group row">
  <div class="col-2 col-form-label">Tipe Aktiva <i style="color:crimson">*</i></div>
  <div class="col-3">
  		{!! Form::select('tipe_aktiva_id',$listTipeAktivaTetap, null,['placeholder' => '-Pilih-','class' => 'form-control select2', 'id' => 'tipe_aktiva_tetap_id'])!!}
  </div>
</div>
<div class="form-group row">
	<div class="col-2 col-form-label">Tanggal Beli <i style="color:crimson">*</i></div>
	<div class="col-3">
		@if (isset($item) || old('tgl_beli'))
			{!! Form::text('tgl_beli',null,['class' => 'form-control tanggal_daftar_aktiva', 'id'=>'tgl_beli'])!!}
		@else
			{!! Form::text('tgl_beli',null,['class' => 'form-control tanggal', 'id'=>'tgl_beli'])!!}
		@endif
	</div>
</div>
<div class="form-group row">
	<div class="col-2 col-form-label">Tanggal Pakai <i style="color:crimson">*</i></div>
	<div class="col-3">
		@if (isset($item) || old('tgl_pakai'))
			{!! Form::text('tgl_pakai',null,['class' => 'form-control tanggal_daftar_aktiva', 'id'=>'tgl_pakai'])!!}
		@else
			{!! Form::text('tgl_pakai',null,['class' => 'form-control tanggal', 'id'=>'tgl_pakai'])!!}
		@endif
	</div>
</div>
<hr>
<div class="form-group row">
	<div class="col-2 col-form-label">Keterangan <i style="color:crimson">*</i></div>
	<div class="col-3">
		{!! Form::textarea('keterangan',null,['class' => 'form-control', 'id' => 'keterangan'])!!}
	</div>
</div>
<div class="form-group row">
    <div class="col-2 col-form-label">Kuantitas <i style="color:crimson">*</i></div>
    <div class="col-3">
		{!! Form::number('qty', $item->qty ?? 1,['class' => 'form-control', 'id' => 'kuantitas','onchange' => 'changeQty(this.value)'])!!}
  	</div>
</div>
<div class="form-group row">
    <div class="col-2 col-form-label">Departemen</div>
    <div class="col-3">
		{!! Form::select('departement_id', [], null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'departement_id'])!!}
  	</div>
</div>
<hr>
<!-- Tab Menu -->
<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3);">
  <li class="nav-item">
    <a class="nav-link active" id="pills-alamat-tab" data-toggle="pill" href="#pills-alamat" role="tab" aria-controls="pills-alamat" aria-selected="true">Umum</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-pengeluaran-tab" data-toggle="pill" href="#pills-pengeluaran" role="tab" aria-controls="pills-pengeluaran" aria-selected="false">Pengeluaran</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-catatan-tab" data-toggle="pill" href="#pills-catatan" role="tab" aria-controls="pills-catatan" aria-selected="false">Catatan</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
	<!-- Tab Umum  -->
	<div class="tab-pane fade show active" id="pills-alamat" role="tabpanel" aria-labelledby="pills-alamat-tab">
 		<fieldset class="form-group">
    		{{-- <legend>
				Tgl Jurnal &nbsp;
				@if (isset($item))
					{!! Form::text('tgl_jurnal',null,['class' => 'col-2 col-form-label tanggal_daftar_aktiva', 'readonly']) !!}
				@else
					{!! Form::text('tgl_jurnal',null,['class' => 'col-2 col-form-label tanggal', 'readonly']) !!}
				@endif
			</legend> --}}
    		<div class="form-group row">
				<div class="col-2 col-form-label">Umur Bulan Aktiva <i style="color:crimson">*</i></div>
				<div class="col-4">
					{!! Form::text('tahun', $item->tahun ?? 0,['class' => 'form-control','id' => 'tahun','onchange' => 'calculateRasio()'])!!}
				</div>
				<div class="col-form-label">Tahun</div>
				<div class="col-2">
					{!! Form::number('bulan', $item->bulan ?? 0,['class' => 'form-control','id' => 'bulan','onchange' => 'calculateRasio()'])!!}
				</div>
				<div class="col-form-label">Bulan</div>
			</div>
			<div class="form-group row">
				<div class="col-2 col-form-label">Metode Penyusutan <i style="color:crimson">*</i></div>
				<div class="col-7">
					{!! Form::select('metode_penyusutan_id', $listMetodePenyusutan, null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'metode_penyusutan_id','onchange' => 'calculateRasio()'])!!}
				</div>
				<div class="col-form-label">Rasio</div>
				<div class="col-2">
					{!! Form::text('rasio',0,['class' => 'form-control','id' => 'rasio', 'readonly','style' => 'text-align: right;'])!!}
				</div>
				<div class="col-form-label">%</div>
			</div>
			<div class="form-group row">
				<div class="col-2 col-form-label">Akun Aktiva <i style="color:crimson">*</i></div>
				<div class="col-4">
					{!! Form::select('akun_aktiva_id', $listAkunAktivaTetap, null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'akun_aktiva_id'])!!}
				</div>
			</div>
			<div class="form-group row" id="div-akun-penyusutan">
				<div class="col-2 col-form-label">Akun Penyusutan <i style="color:crimson">*</i></div>
				<div class="col-4">
				{!! Form::select('akun_akum_penyusutan_id', $listAkunPenyusutan, null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'akun_akum_penyusutan_id'])!!}
				</div>
			</div>
			<div class="form-group row" id="div-akun-beban-penyusutan">
				<div class="col-2 col-form-label">Akun Beban Penyusutan <i style="color:crimson">*</i></div>
				<div class="col-4">
				{!! Form::select('akun_beban_penyusutan_id', $listAkun, null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'akun_beban_penyusutan_id'])!!}
				</div>
			</div>
		</fieldset>
		<div class="form-group row" id="div-tidak-berwujud">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			@if (isset($item))
				{!! Form::checkbox('aktiva_tidak_berwujud', 1, $item->aktiva_tidak_berwujud == 1 ? true : false,['id' => 'aktiva_tidak_berwujud','onclick' => 'checkboxChanged()'])!!}
			@else
				{!! Form::checkbox('aktiva_tidak_berwujud', 1, false,['id' => 'aktiva_tidak_berwujud','onclick' => 'checkboxChanged()'])!!}
			@endif
			<div class="col-3 col-form-label">Aktiva Tidak Berwujud</div>
		</div>
		<div class="form-group row" id="div-tetap-fisikal">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			@if (isset($item))
				{!! Form::checkbox('aktiva_tetap_fisikal', 1, $item->aktiva_tetap_fisikal == 1 ? true : false,['id' => 'aktiva_tetap_fisikal'])!!}
			@else	
				{!! Form::checkbox('aktiva_tetap_fisikal', 1, false,['id' => 'aktiva_tetap_fisikal'])!!}
			@endif
			<div class="col-3 col-form-label">Aktiva Tetap Fiskal</div>
		</div>
	</div>
<!-- Tab Pengeluaran -->
	<div class="tab-pane fade" id="pills-pengeluaran" role="tabpanel" aria-labelledby="pills-pengeluaran-tab">
    	<div class="form-group">
			<div class="pengeluaran-sections" data-id="1">
				<div class="duplicate-pengeluaran-sections">
					<table width="100%" class="table">
						<thead class="thead-light" style="border-collapse: collapse;">
							<tr>
								<th width="20%">No. Akun <i style="color:crimson">*</i></th>
								<th>Tanggal</th>
								<th>Keterangan</th>
								<th>Jumlah <i style="color:crimson">*</i></th>
								<th></th>
							</tr>
						</thead>
						<tbody class="tabel_pengeluaran">
							@if(isset($item))
								@foreach($item->pengeluaranAktiva as $row1 => $data)
									<tr data-id={{ $row1 }}>
										<td>
											{!! Form::select('akun_pengeluaran_aktiva_id[]',$listAkun,$data->akun_pengeluaran_aktiva_id,['placeholder' => '-Pilih-','class' => 'form-control select2 akun_id', 'required','onchange' => 'changeTab(' . $row1 . ')','id' => 'akun_id'. $row1 .''])!!}
										</td>
										<td>
											{!! Form::text('tanggal_pengeluaran[]',Carbon\Carbon::parse($data->tanggal)->format('d F Y'),['class' => 'form-control tanggal_daftar_aktiva','id' => 'tanggal_pengeluaran'. $row1 .''])!!}
										</td>
										<td>
											{!! Form::text('keterangan_pengeluaran[]',$data->keterangan,['class' => 'form-control keterangan_pengeluaran','id' => 'keterangan_pengeluaran'. $row1 .''])!!}
										</td>
										<td>
											{!! Form::text('jumlah_pengeluaran[]', $data->jumlah,['class' => 'form-control jumlah_pengeluaran','id' => 'jumlah_pengeluaran'. $row1 .'','onblur' => 'checkJumlahPengeluaran('. $row1 .')'])!!}
										</td>
										<td>
											<button href="" class="remove btn btn-danger remove-rincian-pengeluaran option-o" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button>
										</td>
									</tr>
								@endforeach				
							@endif

							@if (old('akun_pengeluaran_aktiva_id') !== null)
								@foreach (old('akun_pengeluaran_aktiva_id') as $row => $akun_aktiva_id)
									@include('akuntansi::daftar_aktiva_tetap/components/item_aktiva',[
											'row' 							=> $row,
											'akun_pengeluaran_aktiva_id'	=> $akun_aktiva_id,
											'tanggal_pengeluaran'			=> old('tanggal_pengeluaran')[$row],
											'keterangan_pengeluaran'		=> old('keterangan_pengeluaran')[$row],
											'jumlah_pengeluaran'			=> old('jumlah_pengeluaran')[$row],
										])
								@endforeach
							@endif
						</tbody>
					</table>
					<a class="btn btn-info add-pengeluaran option-o" ><i class="fa fa-plus"> Tambah</i></a>
				</div>
			</div>
			<script type="text/template" id="table_pengeluaran_section" data-id=""/>
				<tr>
					<td>{!! Form::select('akun_pengeluaran_aktiva_id[]',$listAkun,null,['placeholder' => '-Pilih-','class' => 'form-control select2 akun_id', 'required'])!!}</td>
					<td>{!! Form::text('tanggal_pengeluaran[]',null,['class' => 'form-control tanggal_pengeluaran','id' => '',''])!!}</td>
					<td>{!! Form::text('keterangan_pengeluaran[]',null,['class' => 'form-control keterangan_pengeluaran','id' => ''])!!}</td>
					<td>{!! Form::text('jumlah_pengeluaran[]',null,['class' => 'form-control jumlah_pengeluaran','id' => '',''])!!}</td>
					<td><button href="" class="remove btn btn-danger remove-rincian-pengeluaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
				</tr>
			</script>
			<div class="form-group" style="margin-top:150px">
				<div class="pengeluaran-perolehan-sections" data-id="1">
					<div class="duplicate-pengeluaran-perolehan-sections">
						<table width="100%" class="table">
							<thead class="thead-light" style="border-collapse: collapse;">
								<tr>
									<th>Harga Perolehan</th>
									<th>Aktiva Dihentikan</th>
									<th>Akum Penyusutan</th>
									<th>Nilai Buku Asuransi</th>
									<th>Nilai Sisa</th>
								</tr>
							</thead>
							
							<tbody class="tabel_pengeluaran_perolehan">
								<tr>
									<th>
										<span id="span-harga-perolehan">0</span>
										{{ Form::hidden('harga_perolehan', 0,['class' => 'form-control harga-perolehan']) }}
									</th>
									<th>
										<span id="span-aktiva-dihentikan">0</span>
										{{ Form::hidden('aktiva_dihentikan', 0,['class' => 'form-control aktiva-dihentikan']) }}
									</th>
									<th>
										<span id="span-akumulasi-penyusutan">0</span>
										{{ Form::hidden('akumulasi_penyusutan', 0,['class' => 'form-control akumulasi-penyusutan']) }}
									</th>
									<th>
										<span id="span-nilai-buku">0</span>
										{{ Form::hidden('nilai_buku',0,['class' => 'form-control nilai-buku']) }}
									</th>
									<th>{{ Form::text('sisa', !empty($item) ? number_format($item->sisa) : 0,['class' => 'form-control mask', 'id' => 'sisa']) }}</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
  		</div>
  	</div>
<!-- Tab Catatan -->
	  <div class="tab-pane fade" id="pills-catatan" role="tabpanel" aria-labelledby="pills-catatan-tab">
    	<div class="form-group">
    		<label for="exampleFormControlTextarea1">Catatan</label>
    		{!! Form::textarea('catatan',null,['class' => 'form-control','id' => 'catatan'])!!}
  		</div>
  	</div>
</div>