@extends( $permission['daftar'] ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
<!-- Page-Title -->
    <div class="row">
        @include('chichi_theme.layout.filter')
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">{{$title}}</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Daftar Aktiva Tetap</a></li>
            </ol>
        </div>
    </div>
<!-- END Page-Title -->

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">{{$title_document}}</h5>
                @can($permission['buat'])
                    <div class="menu-header">
                        <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-plus"></i></span>
                            Tambah
                        </a>
                    </div>
                @endcan
            </div>
            <div class="card-body" style="overflow-x:auto;">
                @if($items->isEmpty())
                    <div class="alert alert-warning"> Tidak ada data. </div>
                @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Aktiva</th>
                                <th>Keterangan</th>
                                <th>Tipe Aktiva</th>
                                <th>Akun Aktiva</th>
                                <th>Biaya Aktiva</th>
                                <th>Tgl Pakai</th>
                                <th>Tgl Beli</th>
                                <th>Qty</th>
                                <th>Umur Bulan Aktiva</th>
                                <th>% Penyusutan/Th</th>
                                <th>Metode Penyusutan</th>
                                <th>Departemen</th>
                                <th>Tidak Berwujud</th>
                                <th>Pajak</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $key => $item)
                                <tr>
                                    <td>{{ $key + $items->firstItem() }}</td>
                                    <td>{{ $item->kode_aktiva }}</td>
                                    <td>{{ $item->keterangan }}</td>
                                    <td>{{ $item->tipeAktivaTetap->tipe_aktiva_tetap }}</td>
                                    <td>{{ $item->akunAktiva->kode_akun }}</td>
                                    <td>{{ number_format($item->sum_pengeluaran_aktiva) }}</td>
                                    <td>{{ \Carbon\Carbon::parse($item->tgl_pakai)->format('d F Y') }}</td>
                                    <td>{{ \Carbon\Carbon::parse($item->tgl_beli)->format('d F Y') }}</td>
                                    <td>{{ $item->qty }}</td>
                                    <td>{{ $item->sum_month }}</td>
                                    <td>{{ $item->rasio }}</td>
                                    <td>{{ $item->metodePenyusutan->metode }}</td>
                                    <td>-</td>  
                                    <td>{{ $item->tidak_berwujud }}</td>
                                    <td>{{ $item->tetap_fisikal }}</td>
                                    <td>
                                        @if( $permission['lihat'] || $permission['ubah'] || $permission['hapus'] )
                                            {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                                    <div class="dropdown-menu">
                                                        @can($permission['lihat'])
                                                            <a class="dropdown-item jurnal" href="#">
                                                                <i class="fa fa-table"></i>&nbsp;Detail
                                                            </a>
                                                        @endcan
                                                        @if ($item->daftarAktivaTetapDetail->last()->periode == 0)
                                                            @can($permission['ubah'])
                                                                <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i> Edit</a>
                                                            @endcan
                                                        @else
                                                            @can($permission['lihat'])
                                                                <a class="dropdown-item btn-info" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-eye"></i> Lihat</a>
                                                            @endcan
                                                        @endif
                                                        @can($permission['hapus'])
                                                            <a class="dropdown-item deleteBtn" href="#">
                                                                <i class="fa fa-trash"></i> Delete
                                                            </a>
                                                        @endcan
                                                    </div>
                                                </div>
                                            {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
                <div class="pull-right">
                    {{-- {!! $items->links('vendor.pagination.bootstrap-4'); !!} --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
    <script type="text/javascript">
        $('.mask_index').autoNumeric('init');
    </script>
@endsection