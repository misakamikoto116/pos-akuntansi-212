@extends('chichi_theme.layout.app')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788; 
        color: white; 
        text-align: center; 
        position: relative; 
        top: 4px; 
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
        z-index: 100;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Pembelian</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Faktur Pembelian</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Purchase Invoice</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                 @include('pembelian.faktur.form')
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

        <script type="text/javascript">
            var q = 1;
            var w = 1;
            var e = 1;
            var r = 1;
            var t = 1;
            var y = 1;
            var u = 1;
            var i = 1;
            var o = 1;
            var p = 1;
            var a = 1;
            var s = 1;

             $(function() {
              enable_cb();
              $("#group1").click(enable_cb);
            });

            function enable_cb() {
              if (this.checked) {
                $("input.group2").removeAttr("disabled");
              } else {
                $("input.group2").attr("disabled", true).removeAttr("checked");
              }
            }

            $('#selectAll').click(function(e){
            var table= $(e.target).closest('table');
            $('th input:checkbox',table).prop('checked',this.checked);
            });

            $(document).ready(function () {
            $('.duplicate-penawaran-sections').on('click', '.add-penawaran', function () {
            var row = $('#table_penawaran_section').html();
            var clone = $(row).clone();
            clone.find('.produk_id').attr('name','produk_id['+q+']').attr('id','produk_id'+q).attr('onchange','test('+q+')').attr('class','select2 form-control');
            clone.find('.keterangan_produk').attr('name','keterangan_produk['+q+']').attr('id','keterangan_produk'+q);
            clone.find('.qty_produk').attr('name','qty_produk['+q+']').attr('id','qty_produk'+q);
            clone.find('.satuan_produk').attr('name','satuan_produk['+q+']').attr('id','satuan_produk'+q);
            clone.find('.unit_harga_produk').attr('name','unit_harga_produk['+q+']').attr('id','unit_harga_produk'+q);
            clone.find('.diskon_produk').attr('name','diskon_produk['+q+']').attr('id','diskon_produk'+q);
            clone.find('.tax_produk').attr('name','tax_produk['+q+']').attr('id','tax_produk'+q);
            clone.find('.amount_produk').attr('name','amount_produk['+q+']').attr('id','amount_produk'+q);
            clone.find('.dept_id').attr('name','dept_q['+q+']').attr('id','dept_id'+q);
            clone.find('.proyek_id').attr('name','proyek_q['+q+']').attr('id','proyek_id'+q);
            $(clone).appendTo($(this).closest('.penawaran-sections').find('.purchasePenawaran'));
            q++;
            });
        });

        $('.duplicate-penawaran-sections').on('click', '.remove-rincian-penawaran', function () {
            if ($(this).closest('.purchasePenawaran').find('tr').length > 0) {
                $(this).closest('tr').remove();
            }
        });

        $(document).ready(function () {
            $('.duplicate-beban-sections').on('click', '.add-beban', function () {
            var row = $('#table_beban_section').html();
            var clone = $(row).clone();
            clone.find('.produk_id').attr('name','produk_id['+q+']').attr('id','produk_id'+q).attr('onchange','test('+q+')').attr('class','select2 form-control');
            clone.find('.keterangan_produk').attr('name','keterangan_produk['+q+']').attr('id','keterangan_produk'+q);
            clone.find('.qty_produk').attr('name','qty_produk['+q+']').attr('id','qty_produk'+q);
            clone.find('.satuan_produk').attr('name','satuan_produk['+q+']').attr('id','satuan_produk'+q);
            clone.find('.unit_harga_produk').attr('name','unit_harga_produk['+q+']').attr('id','unit_harga_produk'+q);
            clone.find('.diskon_produk').attr('name','diskon_produk['+q+']').attr('id','diskon_produk'+q);
            clone.find('.tax_produk').attr('name','tax_produk['+q+']').attr('id','tax_produk'+q);
            clone.find('.amount_produk').attr('name','amount_produk['+q+']').attr('id','amount_produk'+q);
            clone.find('.dept_id').attr('name','dept_q['+q+']').attr('id','dept_id'+q);
            clone.find('.proyek_id').attr('name','proyek_q['+q+']').attr('id','proyek_id'+q);
            $(clone).appendTo($(this).closest('.beban-sections').find('.purchaseBeban'));
            q++;
            });
        });

        $('.duplicate-beban-sections').on('click', '.remove-rincian-beban', function () {
            if ($(this).closest('.purchaseBeban').find('tr').length > 0) {
                $(this).closest('tr').remove();
            }
        });
        </script>

@endsection
