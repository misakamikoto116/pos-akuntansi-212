<div class="form-group row">
  <div class="col-1 col-form-label">Vendor</div>
  <div class="col-2">
  {!! Form::text('',null,['class' => 'form-control','id' => '','readonly'])!!}
  </div>
  <div class="col-4">
  {!! Form::select('',[],null,['placeholder' => '','class' => 'form-control select2','id' => ''])!!}
  </div>
  <div class="col-2">
    <div class="btn-group">
        <button type="button" class="btn btn-sm btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-list"></i>&nbsp; Option <span class="caret"></span></button>
        <div class="dropdown-menu">
            <a class="dropdown-item btn-info" data-toggle="modal" data-target="#penawaran">Pilih Pesanan</a>
            <a class="dropdown-item btn-info" data-toggle="modal" data-target="#pesanan">Pilih Penerimaan</a>
            <a class="dropdown-item btn-info" data-toggle="modal" data-target="#pengiriman">Pilih Uang Muka</a>
        </div>
    </div>
       <!-- Modal Penawaran -->
<div class="modal fade" id="penawaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pesanan Pembelian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Transaksi</th>
              <th>Tanggal</th>
          </tr>
          </thead>
          <tbody>
          <tr>
              <th><input type="checkbox" id="1"/></th>
              <td>Isidra</td>
              <td>Boudreaux</td>
          </tr>
        </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

 <!-- Modal Pesanan -->
<div class="modal fade" id="pesanan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Penerimaan Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Form</th>
              <th>No. Penerimaan</th>
              <th>PO No.</th>
              <th>Tanggal</th>
          </tr>
          </thead>
          <tbody>
          <tr>
              <th><input type="checkbox" id="1"/></th>
              <td>Isidra</td>
              <td>Boudreaux</td>
              <td>99999</td>
              <td>99999</td>
          </tr>
        </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

 <!-- Modal Pengiriman -->
<div class="modal fade" id="pengiriman" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Uang Muka</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Transaksi</th>
              <th>Tanggal</th>
              <th>Nilai Faktur</th>
          </tr>
          </thead>
          <tbody>
          <tr>
              <th><input type="checkbox" id="1"/></th>
              <td>Isidra</td>
              <td>Boudreaux</td>
              <td>000000</td>
          </tr>
        </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

  </div>
  <div class="col-3 form-inline">
    <div class="form-check form-check-inline">
    <input class="form-check-input" type="checkbox" id="group1" value="option1">
    <label class="form-check-label" for="group1"> Vendor. is Taxable</label>
  </div>
  <div class="form-check form-check-inline">
    <input class="form-check-input group2" type="checkbox" id="inlineCheckbox2" value="option2" disabled="">
    <label class="form-check-label" for="inlineCheckbox2"> Inclusive Tax</label>
  </div>
  </div>
</div>

<div class="row">
  <div class="offset-sm-3 col-sm-3">
    <div class="form-group">
      {!! Form::textarea('alamat_pengiriman',null,['class' => 'form-control','id' => 'alamat_pengiriman',''])!!}
      </div>
  </div>
  <div class="offset-sm-2 col-sm-4">
    <div class="row">
      <div class="offset-sm-4 col-sm-4">
        <div class="form-group">
          <div class="tag">Form No</div>
          {!! Form::text('',null,['class' => 'form-control','id' => '',''])!!}
          </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">Ship Date</div>
          {!! Form::text('',null,['class' => 'form-control tanggal','id' => '',''])!!}
          </div>
      </div>
      <div class="offset-sm-4 col-sm-4">
        <div class="form-group">
          <div class="tag">Invoice No.</div>
          {!! Form::text('',null,['class' => 'form-control','id' => '',''])!!}
          </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">Invoice Date</div>
          {!! Form::text('',null,['class' => 'form-control tanggal','id' => '',''])!!}
          </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">FOB</div>
          {!! Form::select('',[],null,['class' => 'form-control select2','id' => '',''])!!}
          </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">Terms</div>
          {!! Form::select('',[],null,['class' => 'form-control select2','id' => '',''])!!}
          </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <div class="tag">Ship Via</div>
          {!! Form::select('',[],null,['class' => 'form-control select2','id' => '',''])!!}
          </div>
      </div>
      <div class="col-sm-12">
        <div class="form-group row">
          <div class="col-4 col-form-label text-right">Amount</div>
          <div class="col-4">{!! Form::text('',null,['class' => 'form-control','id' => '',''])!!}</div>
           <div class="col-4"><button class="btn btn-default btn-sm"><i class="fa fa-calculator"></i></button></div>
          </div>
      </div>
    </div>
</div>
</div>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-beban-tab" data-toggle="pill" href="#pills-beban" role="tab" aria-controls="pills-beban" aria-selected="true">Beban</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-uang-tab" data-toggle="pill" href="#pills-uang" role="tab" aria-controls="pills-uang" aria-selected="true">Uang Muka</a>
  </li>
</ul>


<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab">
    <div class="penawaran-sections" data-id="1">
    <div class="duplicate-penawaran-sections">
    <table width="100%" class="table duplicate-sections">
      <thead class="thead-light" style="border-collapse: collapse;">
        <tr>
          <th width="12%">Item</th>
          <th width="12%">Item Description</th>
          <th>Qty</th>
          <th>Item Unit</th>
          <th>Unit Price</th>
          <th>Disc %</th>
          <th>Tax</th>
          <th>Amount</th>
          <th width="10%">Dept</th>
          <th width="10%">Proyek</th>
          <th width="10%">Gudang</th>
          <th>SN</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="purchasePenawaran">
      </tbody>
    </table>
    <a class="btn btn-info add-penawaran" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
  </div>
</div>

  <div class="tab-pane fade" id="pills-beban" role="tabpanel" aria-labelledby="pills-beban-tab">
    <div class="beban-sections" data-id="1">
    <div class="duplicate-beban-sections">
    <table width="100%" class="table duplicate-sections">
      <thead class="thead-light" style="border-collapse: collapse;">
        <tr>
          <th width="13%">Account No.</th>
          <th width="17%">Account Name</th>
          <th>Amount</th>
          <th width="17%">Notes</th>
          <th>Department</th>
          <th>Project</th>
          <th>Alokasi ke barang</th>
          <th>Ke Pemasok lain</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="purchaseBeban">
      </tbody>
    </table>
    <a class="btn btn-info add-beban" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
  </div>
</div>

  <div class="tab-pane fade" id="pills-uang" role="tabpanel" aria-labelledby="pills-uang-tab">
    <table class="table" width="100%">
      <thead class="thead-light">
        <tr>
          <th width="30%">Keterangan</th>
          <th>Pajak</th>
          <th width="20%">Total Uang Muka</th>
          <th>No Faktur</th>
          <th>No PO</th>
          <th>Termasuk Pajak</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{!! Form::text('',null,['class' => 'form-control','id' => '',''])!!}</td>
          <td>{!! Form::text('',null,['class' => 'form-control','id' => '',''])!!}</td>
          <td>{!! Form::text('',null,['class' => 'form-control','id' => '',''])!!}</td>
          <td>{!! Form::text('',null,['class' => 'form-control','id' => '','','readonly'])!!}</td>
          <td>{!! Form::text('',null,['class' => 'form-control','id' => '','','readonly'])!!}</td>
          <td>{!! Form::text('',null,['class' => 'form-control','id' => '','','readonly'])!!}</td>
        </tr>
      </tbody>
    </table>
  </div>

</div><!-- END PILLS -->

<script type="text/template" id="table_penawaran_section" data-id="">
  <tr>
    <td>
    {!! Form::select('produk_id',[],null,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id'])!!}</td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('unit_harga_produk[]',null,['class' => 'form-control unit_harga_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('diskon_produk[]',null,['class' => 'form-control diskon_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('tax_produk[]',null,['class' => 'form-control tax_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('amount_produk[]',null,['class' => 'form-control amount_produk','id' => '',''])!!}</td>
    <td>{!! Form::select('dept_id[]',[],null,['class' => 'form-control select2 dept_id','id' => '',''])!!}</td>
    <td>{!! Form::select('proyek_id[]',[],null,['class' => 'form-control select2 proyek_id','id' => '',''])!!}</td>
    <td>{!! Form::select('gudang_id[]',[],null,['class' => 'form-control select2 gudang_id','id' => 'gudang_id',''])!!}</td>
    <td>{!! Form::text('sn',null,['class' => 'form-control sn','id' => '','readonly'])!!}</td>
    <td><button href="" class="remove btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>

<script type="text/template" id="table_beban_section" data-id="">
  <tr>
    <td>
    {!! Form::select('produk_id',[],null,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id'])!!}</td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('unit_harga_produk[]',null,['class' => 'form-control unit_harga_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('diskon_produk[]',null,['class' => 'form-control diskon_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('tax_produk[]',null,['class' => 'form-control tax_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('amount_produk[]',null,['class' => 'form-control amount_produk','id' => '',''])!!}</td>
    <td><button href="" class="remove btn btn-danger remove-rincian-beban" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>

<hr>
<div class="row">
  <div class="col-sm-4">
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Description</label>
      {!! Form::textarea('catatan',null,['class' => 'form-control','id' => 'catatan'])!!}
    </div>
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Akun Hutang</label>
      {!! Form::select('akun_hutang_id',[],null,['class' => 'form-control select2','id' => 'catatan'])!!}
    </div>
    <hr>
    <div class="row">
    <div class="col-5 col-form-label">Syarat Diskon</div>
    <div class="col-7 col-form-label"><strong>0</strong></div>
    </div>
  </div>
  <div class="offset-sm-4 col-sm-4">
    <div class="form-group row">
    <div class="offset-2 col-3 col-form-label text-right"><strong>Sub Total</strong></div>
    <div class="col-7 text-right col-form-label"><strong>0</strong></div>
    </div>
    <div class="form-group row">
      <div class="col-2 col-form-label">Discount:</div>
      <div class="col-3 col-form-label">{!! Form::text('',null,['class' => 'form-control','id' => ''])!!}</div>
      <div class="col-2 col-form-label text-center">% =</div>
      <div class="col-5 col-form-label">{!! Form::text('',null,['class' => 'form-control','id' => ''])!!}</div>
    </div>
    <div class="form-group row">
    <div class="offset-1 col-4 col-form-label text-right"><strong>Total</strong></div>
    <div class="col-7 text-right col-form-label"><strong>0</strong></div>
    </div>
  </div>
</div>
