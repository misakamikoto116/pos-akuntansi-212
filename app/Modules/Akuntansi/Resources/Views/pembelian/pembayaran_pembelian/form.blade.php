<div class="form-group row">
  <div class="col-1 col-form-label">Vendor</div>
  <div class="col-2">
  {!! Form::text('no_pemasok',null,['class' => 'form-control','id' => 'no_pemasok','readonly'])!!}
  </div>
  <div class="col-4">
  {!! Form::select('pemasok_id',$pemasok,null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => 'pemasok_id'])!!}
  </div>
</div>
<div class="row">
  <div class="offset-sm-3 col-sm-3">
    {!! Form::textarea('alamat_asal',null,['class' => 'form-control','id' => 'alamat_asal','readonly'])!!}
  </div>
  <div class="offset-sm-2 col-sm-4">
    <div class="form-group row">
      <div class="col-4 col-form-label">Form no.</div>
      <div class="col-8">
        {!! Form::text('form_no',null,['class' => 'form-control','id' => 'form_no'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Payment Date</div>
      <div class="col-8">
        {!! Form::text('payment_date',null,['class' => 'form-control tanggal','id' => 'payment_date'])!!}
      </div>
    </div>
  </div>
</div>

<hr>

<div class="row">
  <div class="offset-sm-2 col-sm-4">
    <div class="form-group row">
      <div class="col-3 col-form-label">Bank</div>
      <div class="col-5">
        {!! Form::text('akun',null,['class' => 'form-control','id' => 'akun'])!!}
      </div>
      <div class="col-4">
        {!! Form::select('akun_bank_id',[''=>'Pilih Akun']+$akun,null,['class' => 'form-control select2','id' => 'akun_id','onchange'=>'setNamaAkun(this.value)'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Rate</div>
      <div class="col-3">
        {!! Form::text('rate',null,['class' => 'form-control','id' => ''])!!}
      </div>
      <div class="col-6 col-form-label">
        Currency&emsp;<strong>IDR</strong>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Department</div>
      <div class="col-9">
        {!! Form::select('departement',[],null,['class' => 'form-control select2','id' => 'departement'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Project</div>
      <div class="col-9">
        {!! Form::select('project',[],null,['class' => 'form-control select2','id' => 'project'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-3 col-form-label">Memo</div>
      <div class="col-9">
        {!! Form::textarea('memo',null,['class' => 'form-control','id' => 'memo'])!!}
      </div>
    </div>
  </div>

  <div class="offset-sm-2 col-sm-4">
    <div class="offset-sm-4">
    <div class="form-group row">
      <div class="col-2"><input class="form-control" type="checkbox" id="group1" value="1" name="void_cheque"></div>
      <div class="col-10 col-form-label">Void Cheque</div>
    </div>
    <div class="form-group row">
      <div class="col-2"><input class="form-control group2" type="checkbox" id="group2" value="1" name="fiscal_payment"></div>
      <div class="col-10 col-form-label">Fiscal Payment</div>
    </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Cheque No</div>
      <div class="col-8">
        {!! Form::text('cheque_no',null,['class' => 'form-control','id' => 'cheque_no'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Cheque Date</div>
      <div class="col-8">
        {!! Form::text('cheque_date',null,['class' => 'form-control tanggal','id' => 'cheque_date'])!!}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-4 col-form-label">Cheque Amount</div>
      <div class="col-8">
        {!! Form::text('cheque_amount',null,['class' => 'form-control','id' => 'cheque_amount','readonly'])!!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th width="20%">Invoice No.</th>
          <th>Date</th>
          <th>Due</th>
          <th>Amount</th>
          <th>Owing</th>
          <th>Payment Amount</th>
          <th width="5%">Pay</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{!! Form::text('invoice_no[]',null,['class' => 'form-control invoice_no','id' => '','readonly'])!!}</td>
          <td>{!! Form::text('date[]',null,['class' => 'form-control date','id' => '','readonly'])!!}</td>
          <td>{!! Form::text('amount[]',null,['class' => 'form-control amount','id' => '','readonly'])!!}</td>
          <td>{!! Form::text('owing[]',null,['class' => 'form-control owing','id' => '','readonly'])!!}</td>
          <td>{!! Form::text('payment_amount[]',null,['class' => 'form-control payment_amount','id' => '','readonly'])!!}</td>
          <td>{!! Form::text('total_disc[]',null,['class' => 'form-control total_disc','id' => ''])!!}</td>
          <th><input class="form-control" type="checkbox pay" id="" value="1" name="pay[]"></th>
          <td>{!! Form::date('discount_date[]',null,['class' => 'form-control discount_date','id' => ''])!!}</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<hr>

<div class="row">
  <div class="col-sm-6">
    <div class="row">
      <div class="col-3 text-center">Total Owing : 0</div>
      <div class="col-3 text-center">Total Payment : 0</div>
      <div class="col-3 text-center">Total Discount : 0</div>
    </div>
  </div>
</div>