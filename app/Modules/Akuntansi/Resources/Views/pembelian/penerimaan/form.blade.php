<div class="form-group row">
  <div class="col-1 col-form-label">Vendor</div>
  <div class="col-2">
  {!! Form::text('',null,['class' => 'form-control','id' => '','readonly'])!!}
  </div>
  <div class="col-4">
  {!! Form::select('',[],null,['placeholder' => '--Pilih--','class' => 'form-control select2','id' => ''])!!}
  </div>
  <div class="col-2">
    <button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-list"></i>&nbsp; Pilih Pesanan</button>

    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pesanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="demo-foo-filtering" class="table table-bordered">
          <thead>
          <tr>
              <th data-sort-ignore="true"><input type="checkbox" id="selectAll"/></th>
              <th data-toggle="true">No. Transaksi</th>
              <th>Tanggal</th>
          </tr>
          </thead>
          <tbody>
          <tr>
              <th><input type="checkbox" id="1"/></th>
              <td>Isidra</td>
              <td>Boudreaux</td>
          </tr>
          <tr>
              <th><input type="checkbox" id="1"/></th>
              <td>rIsidra</td>
              <td>uBoudreaux</td>
          </tr>
          <tr>
              <th><input type="checkbox" id="1"/></th>
              <td>qIsidra</td>
              <td>gBoudreaux</td>
          </tr>
        </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default">Save changes</button>
      </div>
    </div>
  </div>
</div><!-- END Modal -->

  </div>
</div>

<hr>

<div class="row">
  <div class="offset-sm-3 col-sm-3">
    <div class="form-group">
      {!! Form::textarea('alamat_pengiriman',null,['class' => 'form-control','id' => 'alamat_pengiriman','readonly'])!!}
      </div>
  </div>

  <div class="offset-sm-3 col-sm-3">
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">Receipt No.</div>
          {!! Form::text('',null,['class' => 'form-control','id' => '',''])!!}
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">Receipt Date</div>
          {!! Form::text('',null,['class' => 'form-control tanggal','id' => '',''])!!}
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">Form No.</div>
          {!! Form::text('',null,['class' => 'form-control date','id' => '',''])!!}
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">FOB</div>
          {!! Form::select('',[],null,['class' => 'form-control select2','id' => '',''])!!}
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">Ship Date</div>
          {!! Form::text('',null,['class' => 'form-control tanggal','id' => '',''])!!}
          </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="tag">Ship Via</div>
          {!! Form::select('',[],null,['class' => 'form-control select2','id' => '',''])!!}
          </div>
      </div>
    </div>
  </div>
</div>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>
<div class="penawaran-sections" data-id="1">
<div class="duplicate-penawaran-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab">
    <table width="100%" class="table">
      <thead class="thead-light" style="border-collapse: collapse;">
        <tr>
          <th width="15%">Item</th>
          <th width="15%">Description</th>
          <th width="5%">Qty</th>
          <th>Satuan</th>
          <th width="15%">Dept</th>
          <th width="15%">Proyek</th>
          <th width="15%">Gudang</th>
          <th>SN</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="purchasePenawaran">
      </tbody>
    </table>
    <a class="btn btn-info add-penawaran" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
</div><!-- END PILLS -->
</div>
</div>

<script type="text/template" id="table_penawaran_section" data-id="">
  <tr>
     <td>
    {!! Form::select('produk_id0',[],null,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id'])!!}</td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '',''])!!}</td>
    <td>{!! Form::select('dept_id[]',[],null,['class' => 'form-control select2 dept_id','id' => '',''])!!}</td>
    <td>{!! Form::select('proyek_id[]',[],null,['class' => 'form-control select2 proyek_id','id' => '',''])!!}</td>
    <td>{!! Form::select('gudang_id[]',[],null,['class' => 'form-control select2 proyek_id','id' => '',''])!!}</td>
    <td>{!! Form::text('sn',null,['class' => 'form-control diskon_produk','id' => '',''])!!}</td>
    <td><button href="" class="remove btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>

<hr>
<div class="row">
  <div class="col-sm-4">
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Description</label>
      {!! Form::textarea('catatan',null,['class' => 'form-control','id' => 'catatan'])!!}
    </div>
  </div>
</div>