<div class="form-group row">
  <div class="col-1 col-form-label">Vendor</div>
  <div class="col-2">
  {!! Form::text('no_pelanggan',null,['class' => 'form-control','id' => 'no_pelanggan','readonly'])!!}
  </div>
  <div class="col-6">
  {!! Form::select('pelanggan_id',[],null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'pelanggan_id'])!!}
  </div>
</div>

<div class="row">
  <div class="offset-sm-3 col-sm-3">
    <div class="form-group">
      {!! Form::textarea('',null,['class' => 'form-control','id' => '','readonly'])!!}
      </div>
  </div>
  </div>

  <div class="row">
    <div class="col-sm-6">
      <div class="form-group row">
      <div class="col-6 col-form-label text-right">Receipt / Inv. No.</div>
      <div class="col-6">{!! Form::select('',[],null,['class' => 'form-control','id' => ''])!!}</div>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="form-group row">
      <div class="col-2" style="padding: 0px"><input class="form-control" type="checkbox" id="group1" value="1" name="taxable"></div>
      <div class="col-10 col-form-label" style="padding: 4px 0px;">Vendor is Taxable</div>
    </div>
    <div class="form-group row">
      <div class="col-2" style="padding: 0px"><input class="form-control group2" type="checkbox" id="group2" value="1" name="taxable"></div>
      <div class="col-10 col-form-label" style="padding: 4px 0px;">Inclusive Tax</div>
    </div>
    </div>
    <div class="col-sm-3">
      <div class="row">
        <div class="col-6">
          <div class="form-group">
          <div class="tag">Return No.</div>
          {!! Form::text('',null,['class' => 'form-control','id' => '',''])!!}
          </div>
        </div>
        <div class="col-6">
          <div class="form-group">
          <div class="tag">Date</div>
          {!! Form::text('',null,['class' => 'form-control tanggal','id' => '',''])!!}
          </div>
        </div>
        <div class="col-6 tax" style="display: none;">
          <div class="form-group">
          <div class="tag">No. Fiscal</div>
          {!! Form::text('',null,['class' => 'form-control','id' => '',''])!!}
          </div>
        </div>
        <div class="col-6 tax" style="display: none;">
          <div class="form-group">
          <div class="tag">Tgl Fiscal</div>
          {!! Form::text('',null,['class' => 'form-control tanggal','id' => '',''])!!}
          </div>
        </div>
        <div class="col-12">
          <div class="form-group row">
            <div class="col-6 col-form-label">Nilai Tukar</div>
            <div class="col-6">{!! Form::text('',null,['class' => 'form-control date','id' => '','readonly'])!!}</div>
          </div>
          <div class="form-group row">
            <div class="col-6 col-form-label">Nilai Tukar Pajak</div>
            <div class="col-6">{!! Form::text('',null,['class' => 'form-control date','id' => '','readonly'])!!}</div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="form-group">
    <label>Description :</label>
    {!! Form::textarea('',null,['class' => 'form-control','id' => '',''])!!}
  </div>


<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>

<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab">
      <table class="table">
      <thead class="thead-dark">
        <tr>
          <th width="12%">Item</th>
          <th width="12%">Description</th>
          <th>Qty</th>
          <th>Satuan</th>
          <th>Unit Price</th>
          <th>Disc %</th>
          <th>Tax</th>
          <th>Amount</th>
          <th width="10%">Dept</th>
          <th width="10%">Proyek</th>
          <th width="10%">Gudang</th>
          <th>SN</th>
          <!-- <th></th> -->
        </tr>
      </thead>
      <tbody>
        <tr>
        <td>{!! Form::select('produk_id',[],null,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id'])!!}</td>
        <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '',''])!!}</td>
        <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '',''])!!}</td>
        <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '',''])!!}</td>
        <td>{!! Form::text('unit_harga_produk[]',null,['class' => 'form-control unit_harga_produk','id' => '',''])!!}</td>
        <td>{!! Form::text('diskon_produk[]',null,['class' => 'form-control diskon_produk','id' => '',''])!!}</td>
        <td>{!! Form::text('tax_produk[]',null,['class' => 'form-control tax_produk','id' => '',''])!!}</td>
        <td>{!! Form::text('amount_produk[]',null,['class' => 'form-control amount_produk','id' => '',''])!!}</td>
        <td>{!! Form::select('dept_id[]',[],null,['class' => 'form-control select2 dept_id','id' => '',''])!!}</td>
        <td>{!! Form::select('proyek_id[]',[],null,['class' => 'form-control select2 proyek_id','id' => '',''])!!}</td>
        <td>{!! Form::select('terproses[]',[],null,['class' => 'form-control terproses','id' => '',''])!!}</td>
        <td>{!! Form::text('',null,['class' => 'form-control','id' => '',''])!!}</td>
        <!-- <td><button href="" class="remove btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td> -->
      </tr>
      </tbody>
    </table>
  </div>
</div>

<hr>
<div class="row">
<div class="offset-sm-8 col-sm-4">
    <div class="form-group row">
    <div class="offset-1 col-4 col-form-label text-right"><strong>Sub Total</strong></div>
    <div class="col-7 text-right col-form-label"><strong>0</strong></div>
    </div>
    <div class="form-group row">
    <div class="offset-1 col-4 col-form-label text-right"><strong>Total Retur</strong></div>
    <div class="col-7 text-right col-form-label"><strong>0</strong></div>
    </div>
  </div>
</div>