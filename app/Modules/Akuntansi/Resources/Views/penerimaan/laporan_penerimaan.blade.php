@extends('akuntansi::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan={{ count($item->first()) }}>
                            <p><h4> {{ $perusahaan }}   </h4></p>
                            <p><h6> {{ $title }}        </h6></p>
                            <p><b>  {{ $subTitle  }}   </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>No. Voucher</th>
                        <th>Deposit Ke Bank</th>
                        <th>Keterangan</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    @if($item->isEmpty())
                        <tr>
                            <td colspan={{ count($item->first()) }} align='center'>
                                <div class="alert alert-warning">
                                    Tidak ada data {{ $title }} yang tersedia <br>
                                    {{  $subTitle  }}.
                                </div>
                            </td>
                        </tr>
                    @else
                        @foreach($item as $item_key => $dataPembayaran)
                            <tr>
                                <td>{{ $dataPembayaran['tanggal']               }}</td>
                                <td>{{ $dataPembayaran['no_faktur']             }}</td>
                                <td>{{ $dataPembayaran['ke_bank']             }}</td>
                                <td>{{ $dataPembayaran['keterangan']            }}</td>
                                <td>{{ number_format($dataPembayaran['jumlah']) }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('custom_js')
    
@endsection