@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Jurnal Transaksi Penerimaan</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ route('akuntansi.penerimaan.index') }}">Daftar Penerimaan</a></li>
                      <li><a href="#">Jurnal Transaksi Penerimaan</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Jurnal Transaksi Penerimaan</h5>
            </div>
            <div class="card-body">
                <div class="p-20">

                <div class="row">

                <div class="col-md-6">

                <div class="form-group row">
                {!! Form::label('no_faktur','No.Faktur',['class' => 'col-sm-2 col-form-label']) !!}
                <div class="col-sm-10" style="padding-left: 20px;">
                    <label class="col-sm-2 col-form-label">{{$buku_masuk->no_faktur}}</label>
                </div>
                </div>

                <div class="form-group row">
                {!! Form::label('tanggal','Tanggal',['class' => 'col-sm-2 col-form-label']) !!}
                <div class="col-sm-10" style="padding-left: 20px;">
                    <label class="col-sm-10 col-form-label">{{ \Helpers\IndonesiaDate::indonesiaDate($buku_masuk->tanggal) }}</label>
                </div>
                </div>

                <div class="form-group row">
                {!! Form::label('keterangan','Keterangan',['class' => 'col-sm-2 col-form-label']) !!}
                <div class="col-sm-10" style="padding-left: 20px;">
                    <label class="col-sm-10 col-form-label">{{$buku_masuk->keterangan}}</label>
                </div>
                </div>

                </div>

                </div>

                <div>
                <div>
                <table class="table" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No. Akun</th>
                                <th>Nama Akun</th>
                                <th>Debet</th>
                                <th>Kredit</th>
                                <th>Catatan</th>
                            </tr>
                        </thead>
                        <tbody id="example">
                            <tr>
                                <td>{{$buku_masuk->akun->kode_akun}}</td>
                                <td>{{$buku_masuk->akun->nama_akun}}</td>
                                <td><span class="jurnal_mask">{{$buku_masuk->nominal}}</span></td>
                                <td></td>
                                <td>{{$buku_masuk->keterangan}}</td>
                            </tr>
                            @foreach($detail_buku_masuk as $detail)
                            <tr>
                                <td>{{$detail->akun->kode_akun}}</td>
                                <td>{{$detail->akun->nama_akun}}</td>
                                <td></td>
                                <td><span class="jurnal_mask">{{$detail->nominal}}</span></td>
                                <td>{{$detail->catatan}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
            </div>


            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
    <script type="text/javascript">
        $('.jurnal_mask').autoNumeric('init',{aPad: false,aSign: "Rp "});
    </script>
@endsection