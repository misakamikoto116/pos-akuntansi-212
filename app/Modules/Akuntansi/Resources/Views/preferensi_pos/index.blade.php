@extends('chichi_theme.layout.app')

@section('custom_css')
    <style>
        .isi-content{
            background-color:#f2f8f9;
            padding: 20px;
            margin: 5px;
            border-radius: 3px;
        }
        .list-content li{
            background-color:#f2f8f9;
            border-color: solid 1px #ccc;
        }
    </style>
@endsection

@section('content')
<div class="form-group row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="title">Preferensi</h4>
            </div>
            <div class="card-body">
                <br>
                <div class="form-group row">
                    <div class="col-md-3">
                        <h1 class="title" align="center">Preferensi</h1>
                        <ul id="ul-group" class="nav nav-pills list-group list-content">
                            <li class="nav-item list-group-item">
                                <a id="tab-akun-default-pos" data-toggle="pill" onclick="scrolltop();" href="#akun_default_pos" >Akun Default POS</a>
                            </li>

                            <li class="nav-item list-group-item">
                                <a id="tab-akun-default-pajak" data-toggle="pill" onclick="scrolltop();" href="#akun_defaultpajak" >Akun Default Pajak</a>
                            </li>

                            <li class="nav-item list-group-item">
                                <a id="tab-akun-default-modal" data-toggle="pill" onclick="scrolltop();" href="#akun_defaultmodal" >Default Modal</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade" id="akun_default_pos">
                                <section style="overflow: hidden;">
                                    <div class="form-group clearfix">
                                    
                                    {!! Form::open(['route' => 'akuntansi.preferensi.pos.store', 'method' => 'POST','class' => 'form-horizontal form-label-left']) !!}
        
                                    
                                        <h1 class="title" align="center">Akun Default POS</h1>

                                        <div class="isi-content">
                                            <div class="form-group row">
                                                <div class="col-3 col-form-label"><b>Akun Cash</b></div>
                                                <div class="col-2">
                                                    {!! Form::text('akun_cash_name', $preferensi_pos->akunCash->kode_akun ?? null, ['class' => 'form-control akun_name', 'required','readonly']) !!}
                                                </div>
                                                <div class="col-7">
                                                    {!! Form::select('akun_cash_id', $akun, $preferensi_pos->akun_cash_id ?? null, ['class' => 'form-control select2 akun_select', 'id' => 'cash_id','required', 'placeholder' => 'Pilih Akun', 'required']) !!}
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-3 col-form-label"><b>Akun Piutang</b></div>
                                                <div class="col-2">
                                                    {!! Form::text('akun_hutang_name', $preferensi_pos->akunHutang->kode_akun ?? null, ['class' => 'form-control akun_name', 'required','readonly']) !!}
                                                </div>
                                                <div class="col-7">
                                                    {!! Form::select('akun_hutang_id', $akun, $preferensi_pos->akun_hutang_id ?? null, ['class' => 'form-control select2 akun_select', 'id' => 'hutang_id', 'required', 'placeholder' => 'Pilih Akun', 'required']) !!}
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-3 col-form-label"><b>Akun Diskon Penjualan</b></div>
                                                <div class="col-2">
                                                    {!! Form::text('diskon_penjualan_name', $preferensi_pos->diskonPenjualan->kode_akun ?? null, ['class' => 'form-control akun_name', 'required','readonly']) !!}
                                                </div>
                                                <div class="col-7">
                                                    {!! Form::select('diskon_penjualan_id', $akun, $preferensi_pos->diskon_penjualan_id ?? null, ['class' => 'form-control select2 akun_select', 'id' => 'diskon_penjualan_id', 'required', 'placeholder' => 'Pilih Akun', 'required']) !!}
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-3 col-form-label"><b>Akun Bank</b></div>
                                                <div class="col-2">
                                                    {!! Form::text('akun_bank_nama', $preferensi_pos->akunBank->kode_akun ?? null, ['class' => 'form-control akun_name', 'required','readonly']) !!}
                                                </div>
                                                <div class="col-7">
                                                    {!! Form::select('akun_bank_id', $akunBank, $preferensi_pos->akun_bank_id ?? null, ['class' => 'form-control select2 akun_select', 'id' => 'bank_id', 'required', 'placeholder' => 'Pilih Akun']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="pull-right" style="margin-right:5px; margin-top:5px;">
                                            {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit-default-pos']) !!}
                                        </div>

                                        {!! Form::close() !!}
                                        
                                    </div>
                                </section>
                            </div>

                            <div class="tab-pane fade" id="akun_defaultpajak">
                                <section style="overflow: hidden;">
                                    <div class="form-group clearfix">
                                    
                                    {!! Form::open(['route' => 'akuntansi.preferensi.pos.store.pajak', 'method' => 'POST','class' => 'form-horizontal form-label-left']) !!}
        
                                    
                                        <h1 class="title" align="center">Akun Default Pajak</h1>

                                        <div class="isi-content">
                                            <div class="form-group row">
                                                <div class="col-3 col-form-label"><b>Kode Pajak</b></div>
                                                <div class="col-2">
                                                    <label for="">Status</label>
                                                    
                                                    {{ Form::checkbox('status', 1, ( $preferensi_pos->status ?? false ) == 1 ? true : false, ['class' => 'akun_status']) }}
                                                </div>
                                                <div class="col-7">
                                                    {!! Form::select('kode_pajak_id', $pajak, $preferensi_pos->kode_pajak_id ?? null, ['class' => 'form-control select2 pajak_select', 'id' => 'pajak_id', 'required', 'placeholder' => 'Pilih Pajak', 'required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="pull-right" style="margin-right:5px; margin-top:5px;">
                                            {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit-default-pajak']) !!}
                                        </div>

                                        {!! Form::close() !!}
                                        
                                    </div>
                                </section>
                            </div>

                            <div class="tab-pane fade" id="akun_defaultmodal">
                                <section style="overflow: hidden;">
                                    <div class="form-group clearfix">
                                    
                                    {!! Form::open(['route' => 'akuntansi.preferensi.pos.store.modal', 'method' => 'POST','class' => 'form-horizontal form-label-left']) !!}
        
                                    
                                        <h1 class="title" align="center">Default Modal</h1>

                                        <div class="isi-content">
                                            <div class="form-group row">
                                                <div class="col-3 col-form-label"><b>Modal Awal</b></div>
                                                <div class="col-7">
                                                    {!! Form::text('uang_modal',$preferensi_pos->uang_modal ?? null, ['class' => 'form-control mask', 'required', 'required','id' => 'uang_modal']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-3 col-form-label">
                                                    <b>Tanggal Custom</b>
                                                </div>
                                                <div class="col-7">
                                                    {{ Form::checkbox('tanggal_custom', 1, $preferensi_pos->tanggal_custom === 1 ? true : false, ['class' => 'tanggal_custom']) }}
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="pull-right" style="margin-right:5px; margin-top:5px;">
                                            {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit-default-modal']) !!}
                                        </div>

                                        {!! Form::close() !!}
                                        
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('custom_js')
    <script>
        function scrolltop() {
            $("html, body").animate({ scrollTop: 125 }, 1000);
        }

        jQuery(document).ready(function () {

            var uang_modal = $("#uang_modal").autoNumeric('init').autoNumeric('get');
            $("#uang_modal").autoNumeric('set', uang_modal);

            $('body').on('change', '.akun_select', function (e) {

                var el = $(this);

                $(el).closest('.row').find('.akun_name').val('');

                if ($(el).val() != '') {
                    
                    $.ajax({
                        url: '{{ route("akuntansi.preferensi.pos.akun") }}',
                        data: {
                            id: $(el).val()
                        },
                        dataType: 'JSON',
                        type: 'GET',
                        success: function (result) {
                            if (result.status) {
                                $(el).closest('.row').find('.akun_name').val(result.data.kode_akun);
                            }
                        }
                    });
                }

            });

        });

    </script>
@endsection
