<!-- Form Tab -->
<ul class="nav nav-pills" role="tablist" style="background: #f3f3f3 !important; border-bottom: 1px solid rgba(97,135,136,0.3);">
	<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab-umum">Umum</a></li>
	<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab-pajak">Pajak</a></li>
	<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab-cabang">ID Cabang</a></li>
	<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab-logo-perusahaan">Logo Perusahaan</a></li>
</ul>
<div class="tab-content">
	<!-- Tab Umum -->
	<div class="tab-pane show active" id="tab-umum" style="padding: 20px">
		@include('akuntansi::identitas.components.tab_umum')
	</div>
	<!-- Tab Pajak -->
	<div class="tab-pane fade" id="tab-pajak" style="padding: 20px">
		@include('akuntansi::identitas.components.tab_pajak')
	</div>
	<!-- Tab Cabang -->
	<div class="tab-pane fade" id="tab-cabang" style="padding: 20px">
		@include('akuntansi::identitas.components.tab_cabang')
	</div>
	<!-- Tab Logo Perusahaan -->
	<div class="tab-pane fade" id="tab-logo-perusahaan" style="padding: 20px">
		@include('akuntansi::identitas.components.tab_logo_perusahaan')
	</div>
</div>
