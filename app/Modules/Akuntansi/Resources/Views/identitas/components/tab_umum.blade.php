<div class="form-group row">
    <label class="col-2 col-form-label">Nama Perusahaan <span class="red-star">*</span></label>
    <div class="col-10">
    {!! Form::text('nama_perusahaan',null,['class' => 'form-control','id' => 'nama_perusahaan'])!!}
    </div>
</div>
<div class="form-group row">
    <label class="col-2 col-form-label">Tanggal Tutup Buku <span class="red-star">*</span></label>
    <div class="col-10">
    {!! Form::text('tanggal_mulai',null,['class' => 'form-control tanggal_identitas','id' => 'tanggal_mulai'])!!}
    </div>
</div>
<div class="form-group row">
    <label class="col-2 col-form-label">Alamat <span class="red-star">*</span></label>
    <div class="col-10">
    {!! Form::text('alamat',null,['class' => 'form-control','id' => 'alamat'])!!}
    </div>
</div>
<div class="form-group row">
    <label class="col-2 col-form-label">Kepala Toko <span class="red-star">*</span></label>
    <div class="col-10">
    {!! Form::text('kepala_toko',null,['class' => 'form-control','id' => 'kepala_toko'])!!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('kodePos','Kode Pos', ['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
        {!! Form::text('kode_pos', null, ['class' => 'form-control','id' => 'kode_pos'])!!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('noTelp','No Telepon', ['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
        {!! Form::text('no_telepon', null, ['class' => 'form-control','id' => 'no_telepon'])!!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('negara','Negara', ['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
        {!! Form::text('negara', null, ['class' => 'form-control','id' => 'negara'])!!}
    </div>
</div>
<div class="form-group row">
    <label class="col-2 col-form-label">Mata Uang <span class="red-star">*</span></label>
    <div class="col-10">
        {!! Form::select('mata_uang_id',$mataUang,null,['class' => 'form-control select2','id' => 'mata_uang_id','placeholder' => '- Pilih -'])!!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('footer1','Pesan Footer',['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
        {!! Form::text('footer_1', !empty($item->pesan) ? $item->pesan : null, ['class' => 'form-control','id' => ''])!!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('footer2','Slogan Footer',['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
        {!! Form::text('footer_2', !empty($item->slogan) ? $item->slogan : null, ['class' => 'form-control','id' => ''])!!}
    </div>
</div>