<div class="form-group row">
    {!! Form::label('nama','Nama Perusahaan',['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
    {!! Form::text('nama_perusahaan',null,['class' => 'form-control','id' => 'nama_perusahaan_pajak', 'disabled'])!!}
</div>
</div>
<div class="form-group row">
    {!! Form::label('alamat','Alamat',['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
    {!! Form::text('alamat',null,['class' => 'form-control','id' => 'alamat_pajak','disabled'])!!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('noSeriPajak','No Seri Faktur Pajak',['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
    {!! Form::text('no_seri_faktur_pajak', isset($item) ? $item->identitasPajak['no_seri_faktur_pajak'] : null, ['class' => 'form-control no_seri_faktur_pajak','id' => 'no_seri_faktur_pajak', 'maxlength' => '20' ])!!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('npwps','No Pokok Wajib Pajak', ['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
        {!! Form::text('npwp', isset($item) ? $item->identitasPajak['npwp'] : null, ['class' => 'form-control','id' => 'npwp', 'maxlength' => '20' ])!!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('noPengukuhanPKP','No Pengukuhan PKP', ['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
        {!! Form::text('no_pengukuhan_pkp', isset($item) ? $item->identitasPajak['no_pengukuhan_pkp'] : null, ['class' => 'form-control','id' => 'no_pengukuhan_pkp', 'maxlength' => '30'])!!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('tanggalPengukuhan','Tanggal Pengukuhan PKP',['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
    {!! Form::text('tanggal_pengukuhan_pkp',isset($item) ? $item->identitasPajak['tanggal_pengukuhan_pkp'] : null,['class' => 'form-control tanggal_identitas','id' => 'tanggal_pengukuhan_pkp'])!!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('kodeCabang','Kode Cabang',['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
    {!! Form::text('kode_cabang_pajak',isset($item) ? $item->identitasPajak['kode_cabang_pajak'] : null,['class' => 'form-control','id' => 'kode_cabang', 'maxlength' => '30'])!!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('jenisUsaha','Jenis Usaha',['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
    {!! Form::text('jenis_usaha',isset($item) ? $item->identitasPajak['jenis_usaha'] : null,['class' => 'form-control','id' => 'jenis_usaha', 'maxlength' => '80'])!!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('kluspt','KLU SPT',['class' => 'col-2 col-form-label']) !!}
    <div class="col-10">
    {!! Form::text('klu_spt',isset($item) ? $item->identitasPajak['klu_spt'] : null,['class' => 'form-control','id' => 'klu_spt', 'maxlength' => '10'])!!}
    </div>
</div>