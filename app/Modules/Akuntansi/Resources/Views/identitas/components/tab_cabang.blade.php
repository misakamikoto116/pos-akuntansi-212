<div class="cabang-tab" data-id="1">
    <div class="duplicate-cabang-tab">
        <table width="100%" class="table">
            <thead class="thead-light" style="border-collapse: collapse;">
                <tr>
                    <th width="45%">ID Cabang</th>
                    <th width="45%">Nama Cabang</th>
                    <th width="10%"></th>
                </tr>
            </thead>
            <tbody class="cabang">
                @if(isset($item))
                    @foreach ($item->identitasCabang as $data)
                        <tr>
                            <td>{!! Form::text('kode_cabang[]', $data['kode_cabang'], [ 'placeholder' => '* Wajib di Isi!', 'class' => 'form-control kode_cabang','id' => '', 'required'])!!}</td>
                            <td>{!! Form::text('nama_cabang[]', $data['nama_cabang'], [ 'placeholder' => '* Wajib di Isi!', 'class' => 'form-control nama_cabang', 'required'])!!}</td>
                            <td><button href="" class="remove btn btn-danger remove-cabang" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <a class="btn btn-info add-cabang" ><i class="fa fa-plus"> Tambah</i></a>
    </div>
</div>
<script type="text/template" id="table_cabang_section" data-id="">
    <tr>
        <td>{!! Form::text('kode_cabang[]', null, [ 'placeholder' => '* Wajib di Isi!', 'class' => 'form-control kode_cabang','id' => '', 'required'])!!}</td>
        <td>{!! Form::text('nama_cabang[]', null, [ 'placeholder' => '* Wajib di Isi!', 'class' => 'form-control nama_cabang', 'required'])!!}</td>
        <td><button href="" class="remove btn btn-danger remove-cabang" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
    </tr>
</script>