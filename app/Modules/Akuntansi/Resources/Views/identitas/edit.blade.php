@extends('chichi_theme.layout.app')

@section('custom_css')
<style type="text/css">
    .red-star {
        color: red;
    }
</style>
@endsection

@section('content')
<!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">{{$title}}</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="{{ route('akuntansi.identitas.index') }}">Daftar Informasi Perusahaan</a></li>
                <li><a href="#">Edit Informasi Perusahaan</a></li>
            </ol>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">{{ $title_document }}</h5>
                </div>
                <div class="card-body">
                    <div class="p-20">
                        <div class="form-group row">
                            <label class="col-4 col-form-label"><b>INPUTAN YANG BERTANDA</b> (<span class="red-star"> * </span>) <b>TIDAK BOLEH KOSONG</b></label>
                        </div>
                        <hr>
                    {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left','enctype' => 'multipart/form-data']) !!}
                    @include($form)
                    <div class="submit">
                    {!! Form::button('<i class="fa fa-check"></i> Perbarui',['type' => 'submit', 'id' => 'btn-submit-identitas', 'class' => 'btn btn-default']) !!}
                    {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function () {
            var x = 0;
            $('.tanggal_identitas').datepicker({format: 'dd MM yyyy', autoclose: true});

            function duplicateCabang(){
                var row = $('#table_cabang_section').html();
                var clone = $(row).clone();
                clone.find('.kode_cabang').attr('name','kode_cabang[]').attr('id','kode_cabang'+x);
                clone.find('.nama_cabang').attr('name','nama_cabang[]').attr('id','nama_cabang'+x);
                $(clone).appendTo($('.duplicate-cabang-tab').closest('.cabang-tab').find('.cabang'));
                x++;
            }

            $('.duplicate-cabang-tab').on('click', '.add-cabang', function () {
                duplicateCabang();
            });

            $('.duplicate-cabang-tab').on('click', '.remove-cabang', function () {
                if ($(this).closest('.cabang').find('tr').length > 0) {
                    $(this).closest('tr').remove();
                }
            });

            $('#nama_perusahaan').on('change', function(e){
                e.preventDefault();
                var ini = $('#nama_perusahaan').val();
                $('#nama_perusahaan_pajak').val(ini);
            });

            $('#alamat').on('change', function(e){
                e.preventDefault();
                var ini = $('#alamat').val();
                $('#alamat_pajak').val(ini);
            });
        });
    </script>
@endsection