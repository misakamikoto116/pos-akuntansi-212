@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')

<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Daftar Nomor Pajak</a></li>
                      <li><a href="#">Edit Pajak</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left']) !!}
                @include($form)
                <div class="form-group row">
                {!! Form::label('status','Aktiv',['class' => 'col-2 col-form-label']) !!}
                <div class="col-10">
                {!! Form::hidden('status',0,['class' => 'form-control','id' => 'status'])!!}
                {!! Form::checkbox('status',1,$item->status == 1 ? true : false,['class' => 'form-control','id' => 'status'])!!}
                </div>
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Perbarui',['type' => 'submit','class' => 'btn btn-default']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("custom_js")

@endsection