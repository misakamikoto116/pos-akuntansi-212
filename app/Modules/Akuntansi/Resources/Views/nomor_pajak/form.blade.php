<div class="form-group row">
	{!! Form::label('dari','Dari',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
		{!! Form::text('dari',null,['class' => 'form-control','id' => 'dari'])!!}
		</div>
</div>

<div class="form-group row">
	{!! Form::label('sd','S/D',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
       {!! Form::text('sd',null,['class' => 'form-control','id' => 'sd'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('tetap','Tetap',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
       {!! Form::text('tetap',null,['class' => 'form-control','id' => 'tetap'])!!}
	</div>
</div>