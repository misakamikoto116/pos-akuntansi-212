<div class="form-group row">
  <div class="col-1 col-form-label">Customer</div>
  <div class="col-2">
  {!! Form::text('no_pelanggan',$item->pelanggan->no_pelanggan,['class' => 'form-control','id' => 'no_pelanggan','readonly'])!!}
  </div>
  <div class="col-6">
  {!! Form::select('pelanggan_id',$pelanggan,$item->pelanggan->id,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'pelanggan_id'])!!}
  </div>
</div>

<div class="row">
  <div class="offset-sm-3 col-sm-3">
    <div class="form-group">
      {!! Form::textarea('alamat_asal',$item->pelanggan->alamat,['class' => 'form-control','id' => 'alamat_asal','readonly'])!!}
      </div>
  </div>
  </div>

  {!! Form::hidden('total',null, ['class' => 'kolom_grandtotal']) !!}  

  <div class="row">
    <div class="col-sm-6">
      <div class="form-group row">
      <div class="col-6 col-form-label text-right">Sales Inv. No.</div>
      <div class="col-6">{!! Form::select('list-sales-invoice',$item->pelanggan->fakturPenjualan->pluck('no_faktur','id'),$item->faktur_penjualan_id,['class' => 'form-control list-sales-invoice select2','id' => 'list-sales-invoice'])!!}</div>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="form-group row">
      <div class="col-2" style="padding: 0px"><input class="form-control" type="checkbox" onchange="calculate()" id="group1" value="1" {{ ($item->taxable == 1) ? 'checked' : '' }} name="taxable"></div>
      <div class="col-10 col-form-label" style="padding: 4px 0px;">Cust. is Taxable</div>
    </div>
    <div class="form-group row">
      <div class="col-2" style="padding: 0px"><input class="form-control group2" type="checkbox" onchange="calculate()" id="group2" value="1" {{ ($item->intax == 1) ? 'checked' : '' }}  name="intax"></div>
      <div class="col-10 col-form-label" style="padding: 4px 0px;">Inclusive Tax</div>
    </div>
    </div>
    <div class="col-sm-3">
      <div class="row">
        <div class="col-6">
          <div class="form-group">
          <div class="tag">SR No.</div>
          {!! Form::text('sr_no',$item->sr_no,['class' => 'form-control','id' => '',''])!!}
          </div>
        </div>
        <div class="col-6">
          <div class="form-group">
          <div class="tag">Date</div>
          {!! Form::text('tanggal',$item->tanggal,['class' => 'form-control tanggal_retur','id' => '',''])!!}
          </div>
        </div>
        <div class="col-6 tax">
          <div class="form-group">
          <div class="tag">No. Fiscal</div>
          {!! Form::text('no_fiscal',$item->no_fiscal,['class' => 'form-control','id' => '',''])!!}
          </div>
        </div>
        <div class="col-6 tax" >
          <div class="form-group">
          <div class="tag">Tgl Fiscal</div>
          {!! Form::text('tgl_fiscal',$item->tgl_fiscal,['class' => 'form-control tanggal_retur','id' => '',''])!!}
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="form-group">
    <label>Description :</label>
    {!! Form::textarea('keterangan',$item->keterangan,['class' => 'form-control','id' => '',''])!!}
  </div>


<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>

<div class="faktur-sections" data-id="1">
  <div class="duplicate-faktur-sections">
  <div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab">
      <table width="100%" class="table duplicate-sections">
        <thead class="thead-light" style="border-collapse: collapse;">
          <tr>
            <th width="12%">Item</th>
            <th width="12%">Item Description</th>
            <th>Qty</th>
            <th>Item Unit</th>
            <th>Unit Price</th>
            <th>Disc %</th>
            <th>Tax</th>
            <th>Amount</th>
            <th width="10%">Dept</th>
            <th width="10%">Proyek</th>
            <th width="10%">Gudang</th>
            <th>SN</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="purchaseFaktur">
        
        </tbody>
      </table>
    </div>
  
  </div><!-- END PILLS -->
  </div>
  </div>

<hr>
<div class="row">
  <div class="offset-sm-8 col-sm-4">
		<div class="form-group row">
		<div class="offset-2 col-3 col-form-label text-right"><strong>Sub Total</strong></div>
		<div class="col-7 text-right col-form-label" id="subtotal"><strong>0</strong></div>
    </div>
    
    <div class="form-group row">
			<div class="offset-1 col-4 col-form-label text-right"></div>
			<div class="col-7 text-right col-form-label" id="taxNaration"></div>
    </div>
    
		<div class="form-group row">
			<div class="col-2 col-form-label">Discount:</div>
			<div class="col-3 col-form-label">{!! Form::text('diskon', isset($item->diskon) ? $item->diskon : 0 ,['class' => 'form-control','id' => 'diskonTotal', 'onchange' => 'calculate()'])!!}</div>
			<div class="col-2 col-form-label text-center">% =</div>
			<div class="col-5 col-form-label">{!! Form::text('jumlah_diskon_retur',0,['class' => 'form-control','id' => 'totalPotonganRupiah'])!!}</div>
    </div>
		<div class="form-group row">
		<div class="offset-1 col-4 col-form-label text-right"><strong>Total Invoice</strong></div>
    <div class="col-7 text-right col-form-label grandtotal"><strong>0</strong></div>
		</div>
	</div>
</div>

<script type="text/template" id="table_faktur_section" data-id="">
  <tr>
    <td>
        {!! Form::hidden('barang_faktur_penjualan_id[]', null, ['class' => 'barang_faktur_penjualan_id']) !!}
        {!! Form::select('produk_id[]',[],null,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id'])!!}
    </td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('unit_harga_produk[]',null,['class' => 'form-control unit_harga_produk','id' => '',''])!!}</td>
    <td>{!! Form::text('diskon_produk[]',null,['class' => 'form-control diskon_produk','id' => '',''])!!}</td>
    <td width="10%">
			{!! Form::select('tax_produk[][]',$kode_pajak,null,['class' => 'form-control select2 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => '',''])!!}
    </td>
    <td>{!! Form::text('amount_produk[]',null,['class' => 'form-control amount_produk','id' => '',''])!!}</td>
    <td>{!! Form::select('dept_id[]',[],null,['class' => 'form-control select2 dept_id','id' => '',''])!!}</td>
    <td>{!! Form::select('proyek_id[]',[],null,['class' => 'form-control select2 proyek_id','id' => '',''])!!}</td>
    <td>{!! Form::select('gudang_id[]',[],null,['class' => 'form-control select2 gudang_id','id' => 'gudang_id',''])!!}</td>
    <td>
        {!! Form::text('sn[]',null,['class' => 'form-control sn','id' => '',''])!!}
        {!! Form::hidden('harga_dgn_pajak[]',null,['class' => 'form-control harga_dgn_pajak','id' => '',''])!!}
        {!! Form::hidden('harga_modal[]',null,['class' => 'form-control harga_modal','id' => '',''])!!}
    </td>
    <td><button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>