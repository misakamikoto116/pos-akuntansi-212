<tr>
    <td>
        {!! Form::hidden('barang_faktur_penjualan_id['.($row ?? 0).']', $barang_faktur_penjualan_id ?? null, ['class' => 'barang_faktur_penjualan_id','id' => 'barang_faktur_penjualan_id'.$row]) !!}
        {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id ?? null,['placeholder' => '-Pilih-','class' => 'form-control produk_id','id' => 'produk_id'.$row])!!}
        {!! Form::text('no_produk['.($row ?? 0).']', $no_produk ?? null,['class' => 'form-control no_produk','id' => 'no_produk', 'readonly'])!!}
    </td>
    <td>{!! Form::text('keterangan_produk['.($row ?? 0).']', $keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.$row])!!}</td>
    <td>{!! Form::text('qty_produk['.($row ?? 0).']', $qty_produk ?? null,['class' => 'form-control qty_produk','id' => 'qty_produk'.$row,'onchange' => 'sumQtyAndUnit('. $row .')'])!!}</td>
    <td>{!! Form::text('satuan_produk['.($row ?? 0).']', $satuan_produk ?? null,['class' => 'form-control satuan_produk','id' => 'satuan_produk'.$row])!!}</td>
    <td>{!! Form::text('unit_harga_produk['.($row ?? 0).']', $unit_harga_produk ?? null,['class' => 'form-control unit_harga_produk mask','id' => 'unit_harga_produk'.$row,'onchange' => 'sumQtyAndUnit('. $row .')'])!!}</td>
    <td>{!! Form::text('diskon_produk['.($row ?? 0).']', $diskon_produk ?? null,['class' => 'form-control diskon_produk','id' => 'diskon_produk'.$row,'onchange' => 'sumQtyAndUnit('. $row .')'])!!}</td>
    <td width="10%">
			{!! Form::select('tax_produk['.($row ?? 0).'][]',$kode_pajak, $tax_produk ?? null,['class' => 'form-control select2 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => 'tax_produk','onchange' => 'calculate('. $row .')'])!!}
    </td>
    <td>{!! Form::text('amount_produk['.($row ?? 0).']', $amount_produk ?? null,['class' => 'form-control amount_produk mask','id' => 'amount_produk'.$row,'readonly'])!!}</td>
    <td>{!! Form::select('dept_id['.($row ?? 0).']',[], $dept_id ?? null,['class' => 'form-control select2 dept_id','id' => 'dept_id'.$row,''])!!}</td>
    <td>{!! Form::select('proyek_id['.($row ?? 0).']',[], $proyek_id ?? null,['class' => 'form-control select2 proyek_id','id' => 'proyek_id'.$row,''])!!}</td>
    <td>{!! Form::select('gudang_id['.($row ?? 0).']',isset($item) ? $gudang : [], $gudang_id ?? null,['class' => 'form-control select2 gudang_id','id' => 'gudang_id'.$row,''])!!}</td>
    <td>
        {!! Form::text('sn['.($row ?? 0).']', $sn ?? null,['class' => 'form-control sn','id' => 'sn'.$row,''])!!}
        {!! Form::hidden('harga_modal['.($row ?? 0).']', $harga_modal ?? null,['class' => 'form-control harga_modal','id' => 'harga_modal'.$row,''])!!}
        {!! Form::hidden('harga_terakhir['.($row ?? 0).']', $harga_terakhir ?? null,['class' => 'form-control harga_terakhir','id' => 'harga_terakhir'.$row,''])!!}
        {!! Form::hidden('harga_dgn_pajak['.($row ?? 0).']', $harga_dgn_pajak ?? null,['class' => 'form-control harga_dgn_pajak','id' => 'harga_dgn_pajak'.$row,''])!!}
    </td>
    <td><button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>