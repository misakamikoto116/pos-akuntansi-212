@extends( auth()->user()->hasPermissionTo('buat_rekonsiliasi_bank') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Rekonsiliasi Bank</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Rekonsiliasi Bank</a></li>
                    </ol>

                </div>
            </div>   
<!-- END Page-Title -->
{!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left','id' => 'form-rekonsiliasi']) !!}
<div class="card form-rekonsiliasi-head">
    <div class="card-header"><h5 class="title">Rekonsiliasi Bank</h5>
    <div class="menu-header">
    </div>
    </div>
    <div class="card-body form-rekonsiliasi-body">
        <div class="col-md-12">
            <div class="form-group row">
                {!! Form::label('akun_id','Akun Bank',['class' => 'col-1 col-form-label']) !!}
            <div class="col-md-2">
                {!! Form::text('kode_akun', null,['class' => 'form-control mask_rekon','id' => 'akun_id' , 'readonly'])!!}
            </div>
            <div class="col-md-7">
                {{ Form::select('akun_id', $akun, null,['class' => 'form-control target select2 form-rekonsiliasi-required','id' => 'nama_akun','placeholder' => '- Pilih -','onchange' => 'setAkun()']) }}
            </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row">
            {!! Form::label('saldo_rekening_koran','Saldo Rekening Koran',['class' => 'col-sm-2 col-form-label']) !!}
            <div class="col-sm-3">
            {!! Form::text('saldo_rekening_koran', 0,['class' => 'form-control mask_rekon saldo_rekening','id' => '', 'style' => 'text-align: right;'])!!}
            </div>
            {!! Form::label('mata_uang','Mata Uang',['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-2">
            {!! Form::text('mata_uang', null,['class' => 'form-control','id' => '','readonly' => '']) !!}
            </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row">
            {!! Form::label('kalkulasi','Kalkulasi Saldo',['class' => 'col-sm-2 col-form-label']) !!}
            <div class="col-sm-3" >
            {!! Form::text('kalkulasi_saldo', 0,['class' => 'form-control mask_rekon kalkulasi_saldo','id' => '', 'readonly' => '', 'style' => 'text-align: right;']) !!}
            </div>
            {!! Form::label('tanggal','Tanggal Rekonsil',['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-2" >
            {!! Form::text('tanggal_terakhir_rekonsil', null,['class' => 'form-control tanggal_rekonsil form-rekonsiliasi-required', 'required' => '','id' => 'tanggal_rekonsil_id']) !!}
            </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row">
            {!! Form::label('selisih','Selisih Saldo',['class' => 'col-sm-2 col-form-label']) !!}
            <div class="col-sm-3" >
            {!! Form::text('selisih_saldo', 0,['class' => 'form-control mask_rekon selisih_saldo','id' => '', 'readonly' => '', 'style' => 'text-align: right;']) !!}
            </div>
            {!! Form::label('tanggal','Tanggal Terakhir Rekonsil',['class' => 'col-sm-3 col-form-label']) !!}
            <div class="col-sm-2" >
            {!! Form::text('tanggal_terakhir_rekonsil_disabled', null,['class' => 'form-control tanggal_terakhir_rekonsil', 'readonly' => '']) !!}
            </div>
            </div>
        </div>
    </div>
</div>  
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Rekonsiliasi Bank</h5>
            <div class="menu-header">
            </div>
            </div>
            <div class="card-body">
                <table class="table" id="tblProducts">
                    <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>No Sumber</th>
                        <th>No Cek</th>
                        <th>Keterangan</th>
                        <th>Nilai Tukar</th>
                        <th>Terima (Dr)</th>
                        <th>Keluar (Cr)</th>
                        <th>Beres</th>
                        <th>Rekonsil Pada</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <hr>
                <div align="right" style="padding: 0 10px">
                   <strong>Total yg Sesuai :</strong>&emsp; <span id="total" class="mask_rekon total_sesuai">0</span>
                </div>
                <div align="right" style="padding: 0 10px">
                   <strong>Total Beredar   :</strong>&emsp; <span id="total" class="mask_rekon total_beredar">0</span>
                </div>
                <hr>
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default' ,'id' => 'btnSubmitRekonsiliasi']) !!}
                </div>
            </div>
        </div>
    </div>
</div>  
{!! Form::close() !!}
@endsection

@section('custom_js')
<script type="text/javascript">

$('#btnSubmitRekonsiliasi').on('click', function (e) {
    e.preventDefault();
    var sectionRekonsiliasi = $('#form-rekonsiliasi').find('.form-rekonsiliasi-body .form-rekonsiliasi-required');
    var nextRekonsiliasi;
    for(var i = 0; i < sectionRekonsiliasi.length; i++){
        if (!sectionRekonsiliasi[i].validity.valid) {
            nextRekonsiliasi = false;
        } 
    }
    if (nextRekonsiliasi == false) {
        swal({
            icon: "error",
            text: "Tolong lengkapi inputan anda"
        });
    } else {
        var form = $('#form-rekonsiliasi');
        swal({
          title: "Simpan Data",
          text: 'Simpan Data Rekonsiliasi',
          icon: "info",
          buttons: true,
        })
        .then((willSave) => {
          if (willSave) {
            form.submit();
          } else {
            swal("Data batal di Simpan", {
                icon: "success",
            });
          }
        });
    }
});

$(".tanggal_rekonsil").datepicker({
    format: 'dd MM yyyy', 
    autoclose: true
});
$('.tanggal_rekonsil').datepicker('setDate', new Date());

$('.tanggal_rekonsil').change(function() {
    setAkun();
});

{{-- tampilkan data ke dalam table --}}
function rekonsiliasiBank(nama_akun, tanggal) {
    $("#tblProducts tbody tr").remove();
    $.ajax({
        type: "GET",
        url: "{{ route('akuntansi.get.rekonsiliasi_bank.value') }}",
        data: {
            id: nama_akun,
            tanggal: tanggal,
            _token: '{{ csrf_token() }}'
        },
        success: function(data) {
        $('#tblProducts tbody').html(data);
        var total_beredar = 0;
        var total_sesuai  = 0;
        $("#tblProducts tbody tr").each(function (index, val) {
            var id = $(this).data("id");
            var data_model = $(this).data("model");
            var nominal_penerimaan_beredar = parseFloat($(this).find(".nominal_penerimaan").text());
            var nominal_pembayaran_beredar = parseFloat($(this).find(".nominal_pembayaran").text());

            if ($(this).find(`input[name='transaksi_id[${id}]']`).prop('checked')==true) {
                var nominal_penerimaan_beredar = parseFloat($(this).find(".nominal_penerimaan").text());
                var nominal_pembayaran_beredar = parseFloat($(this).find(".nominal_pembayaran").text());

                if (isNaN(nominal_penerimaan_beredar)) {
                    nominal_penerimaan_beredar = 0;
                }

                if (isNaN(nominal_pembayaran_beredar)) {
                    nominal_pembayaran_beredar = 0;
                }

                if (data_model == "1") {
                    total_sesuai   += nominal_penerimaan_beredar;
                    total_beredar  -= nominal_penerimaan_beredar;
                }else if (data_model == "0") {
                    total_sesuai   -= nominal_pembayaran_beredar;
                    total_beredar  += nominal_pembayaran_beredar;
                }
            }
        });
        $('.total_beredar').autoNumeric("set",total_beredar);
        $('.total_sesuai').autoNumeric("set",total_sesuai);

        $('.mask_duit').autoNumeric('init',{aPad: false});
        
        },failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

{{-- get akun tampilke input disable kode akun --}}
function setAkun() {
    var nama_akun   = $('#nama_akun').val();
    var tanggal     = $('.tanggal_rekonsil').val();
      $.ajax({
        type: "GET",
        url: "{{ route('akuntansi.get.akun_id.rekonsiliasi_bank') }}",
        data: {id : nama_akun,
            _token: '{{ csrf_token() }}'},
        dataType: "json",
        success: function(response){
        $('#akun_id').val(response.kode_akun);
        if (response.tanggal_terakhir_rekonsil) {
            $(".tanggal_terakhir_rekonsil").val(response.tanggal_terakhir_rekonsil);
            $(".saldo_rekening").autoNumeric('set', response.saldo_rekening_koran);
            $(".kalkulasi_saldo").autoNumeric('set', response.kalkulasi_saldo);
            $(".selisih_saldo").autoNumeric('set', response.selisih_saldo);
        }else {
            refreshField();
        }

        rekonsiliasiBank(nama_akun, tanggal);
        },fail: function(errMsg) {
            alert(errMsg);
        }
    });
}

function refreshField() {
    $(".saldo_rekening").autoNumeric('set', 0);
    $(".kalkulasi_saldo").autoNumeric('set', 0);
    $(".selisih_saldo").autoNumeric('set', 0);
    $(".tanggal_terakhir_rekonsil").val('');
    $('.total_sesuai').html(0);
    $('.total_beredar').html(0);
}

{{-- get rekonsiliasi data --}}
function rekonsiliasiBankData(nama_akun) {
      $.ajax({
        type: "GET",
        url: "{{ route('akuntansi.get.rekonsiliasi-bank.data') }}",
        data: {id : nama_akun,
            _token: '{{ csrf_token() }}'},
        dataType: "json",
        success: function(response){
            if (response.rekonsiliasi != null) {
                $.each(response, function (index, element) {
                    $(".saldo_rekening").autoNumeric('init',{aPad: false}).autoNumeric('set',element.saldo_rekening_koran);
                    $(".kalkulasi_saldo").autoNumeric('init',{aPad: false}).autoNumeric('set',element.kalkulasi_saldo);
                    $(".selisih_saldo").autoNumeric('init',{aPad: false}).autoNumeric('set',element.selisih_saldo);

                    var event = new Date(element.tanggal_rekonsil);
                    var options = { year: 'numeric', month: 'long', day: 'numeric' };
                   
                    $(".tanggal_terakhir_rekonsil").val(event.toLocaleDateString('IND', options));
                });
            }
            else {
                $(".saldo_rekening").val(0);
                $(".kalkulasi_saldo").val(0);
                $(".selisih_saldo").val(0);
                $(".total_sesuai").html(0);
                $(".tanggal_terakhir_rekonsil").val(null);
            }
        },
        fail: function(errMsg) {
            alert(errMsg);
        }
    });
}


// Fungsi check dan perhitungan
$('#tblProducts tbody').on('ifChanged','.beres_check',function(event) {

        if ($(this).prop('checked')==true) {
           $.each($(this).iCheck('check').closest("td"),
                  function () {
                        
                        var nominal_penerimaan = $(this).siblings("td").find('.nominal_penerimaan');
                        var nominal_pembayaran = $(this).siblings("td").find('.nominal_pembayaran');

                        if ($(nominal_penerimaan).length  ) {
                            // Formating Saldo Rekening
                            var saldo_rekening = $('.saldo_rekening').autoNumeric('get');
                            var saldo_rekening_formatted = parseFloat(saldo_rekening);

                            // Formating Kalkulasi
                            var kalkulasi = parseFloat($(".kalkulasi_saldo").autoNumeric('get'));

                            // Formating nominal_penerimaan
                            var nominal_penerimaan_nformatted = parseFloat($(nominal_penerimaan).autoNumeric('get'));

                            // Perhitungan total kalkulasi saldo & dimasukan ke total sesuai dan kalkulasi saldo
                            var kalkulasi_saldo = kalkulasi + nominal_penerimaan_nformatted; 
                            $(".kalkulasi_saldo").autoNumeric('set',kalkulasi_saldo);
                            $(".total_sesuai").autoNumeric('set', kalkulasi_saldo);


                            var total_beredar = parseFloat($(".total_beredar").autoNumeric("get")) - nominal_penerimaan_nformatted;
                            $(".total_beredar").autoNumeric("set",total_beredar);

                        }else {
                            var saldo_rekening = $('.saldo_rekening').autoNumeric('get');
                            var saldo_rekening_formatted = parseFloat(saldo_rekening);

                            var kalkulasi = parseFloat($(".kalkulasi_saldo").autoNumeric('get'));

                            var nominal_pembayaran_nformatted = parseFloat($(nominal_pembayaran).autoNumeric('get'));

                            var kalkulasi_saldo = kalkulasi - nominal_pembayaran_nformatted; 
                            $(".kalkulasi_saldo").autoNumeric('set',kalkulasi_saldo);
                            $(".total_sesuai").autoNumeric('set', kalkulasi_saldo);

                            var total_beredar = parseFloat($(".total_beredar").autoNumeric("get")) + nominal_pembayaran_nformatted;
                            $(".total_beredar").autoNumeric("set",total_beredar);
                        }
                        var total_saldo_rekening = saldo_rekening_formatted - kalkulasi_saldo;
                        $('.selisih_saldo').autoNumeric('set',total_saldo_rekening);
                  });

        }
        else {
            $.each($(this).iCheck('uncheck').closest("td"),
              function () {
                    
                    var nominal_penerimaan = $(this).siblings("td").find('.nominal_penerimaan');
                    var nominal_pembayaran = $(this).siblings("td").find('.nominal_pembayaran');

                    if ($(nominal_penerimaan).length  ) {
                        // Formating Saldo Rekening
                        var saldo_rekening = $('.saldo_rekening').autoNumeric('get');
                        var saldo_rekening_formatted = parseFloat(saldo_rekening);

                        // Formating Kalkulasi
                        var kalkulasi = parseFloat($(".kalkulasi_saldo").autoNumeric('get'));

                        // Formating nominal_penerimaan
                        var nominal_penerimaan_nformatted = parseFloat($(nominal_penerimaan).autoNumeric('get'));

                        // Perhitungan total kalkulasi saldo & dimasukan ke total sesuai dan kalkulasi saldo
                        var kalkulasi_saldo = kalkulasi - nominal_penerimaan_nformatted; 
                        $(".kalkulasi_saldo").autoNumeric('set',kalkulasi_saldo);
                        $(".total_sesuai").autoNumeric('set', kalkulasi_saldo);

                        var total_beredar = parseFloat($(".total_beredar").autoNumeric("get")) + nominal_penerimaan_nformatted;
                        $(".total_beredar").autoNumeric("set",total_beredar);    

                    }else {
                        var saldo_rekening = $('.saldo_rekening').autoNumeric('get');
                        var saldo_rekening_formatted = parseFloat(saldo_rekening);

                        var kalkulasi = parseFloat($(".kalkulasi_saldo").autoNumeric('get'));

                        var nominal_pembayaran_nformatted = parseFloat($(nominal_pembayaran).autoNumeric('get'));

                        var kalkulasi_saldo = kalkulasi + nominal_pembayaran_nformatted; 
                        $(".kalkulasi_saldo").autoNumeric('set',kalkulasi_saldo);
                        $(".total_sesuai").autoNumeric('set', kalkulasi_saldo);

                        var total_beredar = parseFloat($(".total_beredar").autoNumeric("get")) - nominal_pembayaran_nformatted;
                        $(".total_beredar").autoNumeric("set",total_beredar);                        
                    }
                    var total_saldo_rekening = saldo_rekening_formatted - kalkulasi_saldo;
                    $('.selisih_saldo').autoNumeric('set',total_saldo_rekening);
              });
        }
});

// Ubah saldo
    $( ".saldo_rekening" ).keyup(function() {
        $('.mask_rekon').autoNumeric('init',{aPad: false, mDec: 0});
        var saldo_rek = parseInt($( this ).autoNumeric('get'));
        var saldo_kal = parseInt($( ".kalkulasi_saldo" ).autoNumeric('get'));
        var jumlah_saldo = saldo_rek - saldo_kal;
        $('.selisih_saldo').autoNumeric('set',jumlah_saldo);
      })
      .keyup();
</script>
@endsection