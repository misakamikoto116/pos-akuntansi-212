@foreach ($semua_transaksi as $key => $data)
	<tr data-id="{{ $data['id'] }}" data-model="{{ $data['status'] }}">
		<td>{{ $data['tanggal'] }}</td>
		<td>{{ $data['no_faktur'] }}</td>
		<td>{{ $data['no_cek'] }}</td>
		<td>{{ $data['keterangan'] }}</td>
		<td>{{ $data['rate'] }}</td>
		@if ($data['debet'] !== 0)
			<td><span class="mask_duit nominal_penerimaan">{{ $data['debet'] ?? 0 }}</span></td>
			<td>0</td>
		@elseif($data['kredit'] !== 0)
			<td>0</td>
			<td><span class="mask_duit nominal_pembayaran">{{ $data['kredit'] ?? 0 }}</span></td>
		@endif
		<td>
			<input type="checkbox" name="transaksi_id[{{ $data['id'] }}]" class="beres_check" value="1" {{ $data['status_checked'] }}>
		</td>
		@if ($data['status_checked'] == "checked")
			<td>{{ $data['rekonsil_pada'] }}</td>
		@else
			<td></td>
		@endif
	</tr>
@endforeach

<script type="text/javascript">
	$(document).ready(function(){
	  $('.beres_check').iCheck({
	    checkboxClass: 'icheckbox_flat-green',
	    increaseArea: '20%' // optional
	  });
	});
</script>