<tr>
    <td>
        {!! Form::text('no_produk['.($row ?? 0).']', null, ['placeholder' => 'Cari Barang','class' => 'form-control no_produk', 'readonly', 'required', 'id' => 'no_produk'.($row ?? 0)])!!}
        {!! Form::hidden('produk_id['.($row ?? 0).']', null,['class' => 'form-control produk_id', 'required', 'id' => 'produk_id'.($row ?? 0)])!!}
    </td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.($row ?? 0)])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk', 'id' => 'qty_produk'.($row ?? 0)])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk', 'id' => 'satuan_produk'.($row ?? 0)])!!}</td>
    <td>{!! Form::text('nomor_serial[]',null,['class' => 'form-control nomor_serial', 'id' => 'nomor_serial'.($row ?? 0)])!!}</td>
    <td><button href="" class="remove btn btn-danger remove-rincian-serial-barang" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>