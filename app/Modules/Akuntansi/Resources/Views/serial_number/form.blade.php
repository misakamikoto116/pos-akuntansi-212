<div class="form-group row">
  <div class="col-2 col-form-label">Tipe Transaksi</div>
  	<div class="col-3">
  		{!! Form::select('tipe_transaksi',array('0' => 'Debet', '1' => 'Kredit'),null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'tipe_transaksi'])!!}
  	</div>
</div>
<div class="form-group row">
	<div class="col-2 col-form-label">Nomor Transaksi</div>
	<div class="col-3">
		{!! Form::select('no_transaksi',$transaksi, null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'no_transaksi'])!!}
	</div>
	<div class="col-3">
		{!! Form::hidden('tgl_transaksi',null,['class' => 'form-control col-form-label','id' => 'tgl_transaksi'])!!}
	</div>
</div>
<hr>
<div class="form-group row">
	<div class="col-2 col-form-label">Input Nomor</div>
	<div class="col-3">
		{!! Form::text('no_pengisian',null,['class' => 'form-control', 'id' => 'input_nomor'])!!}
	</div>
</div>
<div class="form-group row">
	<div class="col-2 col-form-label">Tanggal</div>
	<div class="col-3">
		@if (isset($item))
			{!! Form::text('tgl_pengisian',null,['class' => 'form-control tanggal_pencatatan_serial'])!!}
		@else
			{!! Form::text('tgl_pengisian',null,['class' => 'form-control tanggal tanggal_pencatatan_serial'])!!}
		@endif
	</div>
</div>
<div class="form-group row">
    <div class="col-2 col-form-label">Dipersiapkan Oleh</div>
    <div class="col-3">
		{!! Form::text('persiapan',null,['class' => 'form-control', 'id' => 'persiapan'])!!}
  	</div>
</div>
<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>
<div class="serial-barang-sections" data-id="1">
<div class="duplicate-serial-barang-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab">
  	<table width="100%" class="table">
  		<thead class="thead-light" style="border-collapse: collapse;">
  			<tr>
  				<th width="20%">Item</th>
  				<th>Item Description</th>
  				<th>Qty</th>
  				<th>Satuan</th>
  				<th>Serial Number</th>
  				<th></th>
  			</tr>
  		</thead>
  		<tbody class="nomorserialbarang">
				@if(isset($item))
					@if(old('produk_id') !== null)  
							@foreach(old('produk_id') as $row => $produk_id )
								@include('akuntansi::serial_number/component/item_serial_number',[
									'row'                                 => $row, 
									'produk_id'                           => $produk_id,
									'no_produk'			                  => old('no_produk')[$row],
									'keterangan_produk'                   => old('keterangan_produk')[$row],
									'qty_produk'                          => old('qty_produk')[$row],
									'satuan_produk'                       => old('satuan_produk')[$row],
									'nomor_serial'                        => old('nomor_serial')[$row],
								])
							@endforeach      
					@else
						@foreach($item->detailSerialNumber as $key => $data)
							<tr>
								<td>
									{!! Form::text('no_produk[]', $data->produk->no_barang, ['placeholder' => 'Cari Barang', 'class' => 'form-control no_produk', 'required', 'readonly', 'id' => 'no_produk'.($key)])!!}
									{!! Form::hidden('produk_id[]', $data->produk_id, ['class' => 'form-control produk_id', 'required'])!!}
								</td>
								<td>{!! Form::text('keterangan_produk[]',$data->item_deskripsi,['class' => 'form-control keterangan_produk','id' => ''])!!}</td>
								<td>{!! Form::number('qty_produk[]',$data->jumlah,['class' => 'form-control qty_produk','id' => ''])!!}</td>
								<td>{!! Form::text('satuan_produk[]',$data->satuan,['class' => 'form-control satuan_produk','id' => ''])!!}</td>
								<td>{!! Form::text('nomor_serial[]',$data->serial_number,['class' => 'form-control nomor_serial','id' => ''])!!}</td>
								<td><button href="" class="remove btn btn-danger remove-rincian-serial-barang" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
							</tr>
						@endforeach
					@endif
				@elseif(old('produk_id') !== null)  
					@foreach(old('produk_id') as $row => $produk_id )
						@include('akuntansi::serial_number/component/item_serial_number',[
							'row'                                 => $row, 
							'produk_id'                           => $produk_id,
							'no_produk'			                  => old('no_produk')[$row],
							'keterangan_produk'                   => old('keterangan_produk')[$row],
							'qty_produk'                          => old('qty_produk')[$row],
							'satuan_produk'                       => old('satuan_produk')[$row],
							'nomor_serial'                        => old('nomor_serial')[$row],
						])
					@endforeach      
				@endif
  		</tbody>
  	</table>
  	<a class="btn btn-info add-serial-barang" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
  </div>
</div>
</div>

@include('akuntansi::pesanan_modal/modal_barang')

<script type="text/template" id="table_serial_barang_section" data-id="">
	<tr>
		<td>
			{!! Form::text('no_produk[]', null,['placeholder' => 'Cari Barang','class' => 'form-control no_produk', 'readonly', 'required'])!!}
			{!! Form::hidden('produk_id[]', null, ['class' => 'form-control produk_id', 'required'])!!}
		</td>
		<td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}</td>
		<td>{!! Form::number('qty_produk[]',null,['class' => 'form-control qty_produk','required'])!!}</td>
		<td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','required'])!!}</td>
		<td>{!! Form::text('nomor_serial[]',null,['class' => 'form-control nomor_serial'])!!}</td>
		<td><button href="" class="remove btn btn-danger remove-rincian-serial-barang" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
	</tr>
</script>
	