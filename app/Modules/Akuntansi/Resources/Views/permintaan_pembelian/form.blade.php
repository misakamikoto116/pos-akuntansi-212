<div class="row">
	<div class="col-sm-4">
		<div class="form-group row">
			<div class="col-3 col-form-label">Request No.</div>
			<div class="col-6">
				{!! Form::text('request_no',(empty($item->request_no) ? $idPrediction : $item->request_no),['class' => 'form-control','id' => ''])!!}
			</div>
		</div>
		<div class="form-group row">
			<div class="col-3 col-form-label">Request Date</div>
			<div class="col-6">
        @if (isset($item) || old('request_date') !== null)
				{!! Form::text('request_date',null,['class' => 'form-control tanggal1 format-tanggal','id' => ''])!!}
        @else
        {!! Form::text('request_date',null,['class' => 'form-control tanggal1 tanggal_permintaan','id' => ''])!!}
        @endif
			</div>
		</div>
	</div>
	<div class="offset-sm-6 col-sm-2">
		<div class="form-group row" style="position: relative; top: 50%;">
	      <div class="col-2" style="padding: 0px">
          @if (isset($item->status))
            <input class="form-control" type="checkbox" id="group1" value="1" name="status" {{ ($item->status == 1) ? 'checked' : '' }}>
          @else
            <input class="form-control" type="checkbox" id="group1" value="1" name="status">
          @endif  
        </div>
	      <div class="col-10 col-form-label" style="padding: 4px 0px;">Ditutup</div>
	    </div>
	</div>
</div>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>
<div class="penawaran-sections" data-id="1">
<div class="duplicate-penawaran-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
    <div style="width: 1800px;">
      <table width="100%" class="table">
        <thead class="thead-light" style="border-collapse: collapse;">
          <tr>
            <th width="12%">Item</th>
            <th width="12%">Description</th>
            <th width="5%">Qty</th>
            <th>Item Unit</th>
            <th width="10%">Required Date</th>
            <th width="10%">Notes</th>
            <th>Qty Ordered</th>
            <th>Qty Received</th>
            <th width="10%">Dept</th>
            <th width="10%">Proyek</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="purchasePenawaran">

          @if (old('produk_id') !== null)
            @foreach (old('produk_id') as $row => $produk_id)
              @include('akuntansi::permintaan_pembelian/components/item_permintaan_pembelian',[
                  'row'                           => $row,
                  'produk_id'                     => $produk_id,
                  'no_produk'                     => old('no_produk')[$row],
                  'keterangan_produk'             => old('keterangan_produk')[$row],
                  'qty_produk'                    => old('qty_produk')[$row],
                  'satuan_produk'                 => old('satuan_produk')[$row],
                  'required_date'                 => old('required_date')[$row],
                  'notes'                         => old('notes')[$row],
                  'qty_ordered'                   => old('qty_ordered')[$row],
                  'qty_received'                  => old('qty_received')[$row],
                  'dept_id'                       => old('dept_id')[$row],
                  'proyek_id'                     => old('proyek_id')[$row],
                ])
            @endforeach
          @endif

          @if (!old('produk_id'))
            @if (isset($item->barangPermintaanPembelian))
              @php $looping = $item->barangPermintaanPembelian->count() + 1; @endphp
              @foreach ($item->barangPermintaanPembelian as $itemBarang)
                <tr class="tr-class" data-id="{{ $looping }}">
                  <td>
                    {!! Form::text('no_produk[]', $itemBarang->produk->no_barang, ['placeholder' => 'Cari Barang','class' => 'form-control no_produk','id' => 'no_produk'.$loop->iteration, 'onclick' => 'awas('.$loop->iteration.')','required', 'readonly'])!!}
                    {!! Form::hidden('produk_id[]', $itemBarang->produk_id, ['placeholder' => '-Pilih-','class' => 'form-control produk_id','id' => 'produk_id'.$loop->iteration, 'required'])!!}
                    {!! Form::hidden('produk_id_temp[]', $itemBarang->produk_id, ['id' => 'produk_id_temp'.$loop->iteration,'class' => 'produk_id_temp']) !!}
                  </td>
                  <td>{!! Form::text('keterangan_produk[]',$itemBarang->produk->keterangan,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.$loop->iteration,'required'])!!}</td>
                  <td>{!! Form::text('qty_produk[]',$itemBarang->jumlah,['class' => 'form-control qty_produk','id' => 'qty_produk'.$loop->iteration,'required','onchange' => 'cekQtyandPrice('. $loop->iteration .')'])!!}</td>
                  <td>{!! Form::text('satuan_produk[]',$itemBarang->item_unit,['class' => 'form-control satuan_produk','id' => 'satuan_produk'.$loop->iteration,'required'])!!}</td>
                  <td>{!! Form::text('required_date[]',$itemBarang->required_date_formatted,['class' => 'form-control required_date format-tanggal','id' => 'required_date'.$loop->iteration,'required'])!!}</td>
                  <td>{!! Form::text('notes[]',$itemBarang->notes,['class' => 'form-control notes','id' => 'notes'.$loop->iteration,''])!!}</td>
                  <td>{!! Form::text('qty_ordered[]',$itemBarang->qty_ordered === null ? 0 : $itemBarang->qty_ordered,['class' => 'form-control qty_ordered','id' => 'qty_ordered'.$loop->iteration,'readonly'])!!}</td>
                  <td>{!! Form::text('qty_received[]',$itemBarang->qty_received === null ? 0 : $itemBarang->qty_received,['class' => 'form-control qty_received','id' => 'qty_received'.$loop->iteration,'readonly'])!!}</td>
                  <td>{!! Form::select('dept_id[]',[],null,['class' => 'form-control select2 dept_id','id' => 'dept_id'.$loop->iteration,''])!!}</td>
                  <td>{!! Form::select('proyek_id[]',[],null,['class' => 'form-control select2 proyek_id','id' => 'proyek_id'.$loop->iteration,''])!!}</td>
                  <td><button href="" class="remove btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
                </tr>
              @endforeach
            @endif
          @endif
        </tbody>
      </table>
    </div>
  	<a class="btn btn-info add-penawaran" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
</div><!-- END PILLS -->
</div>
</div>

<script type="text/template" id="table_penawaran_section" data-id=""/>
  <tr>
    <td>
      {!! Form::text('no_produk[]', null, ['placeholder' => 'Cari Barang', 'class' => 'form-control no_produk', 'required', 'readonly'])!!}
      {!! Form::hidden('produk_id[]', null, ['class' => 'form-control produk_id','required'])!!}
    </td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('required_date[]',null,['class' => 'form-control tanggal1 required_date','id' => '','required'])!!}</td>
    <td>{!! Form::text('notes[]',null,['class' => 'form-control notes','id' => '',''])!!}</td>
    <td>{!! Form::text('qty_ordered[]',null,['class' => 'form-control qty_ordered','id' => '','readonly'])!!}</td>
    <td>{!! Form::text('qty_received[]',null,['class' => 'form-control qty_received','id' => '','readonly'])!!}</td>
    <td>{!! Form::select('dept_id[]',[],null,['class' => 'form-control select2 dept_id','id' => '',''])!!}</td>
    <td>{!! Form::select('proyek_id[]',[],null,['class' => 'form-control select2 proyek_id','id' => '',''])!!}</td>
    <td><button href="" class="remove btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>

<hr>
<div class="row">
	<div class="col-sm-6">
	 	<div class="form-group">
	    <label for="exampleFormControlTextarea1">Description</label>
	    {!! Form::textarea('catatan',null,['class' => 'form-control','id' => 'keterangan'])!!}
	  </div>
	</div>
</div>

@include('akuntansi::pesanan_modal/modal_barang')