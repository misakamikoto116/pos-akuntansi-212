<tr>
    <td>
        {!! Form::text('no_produk['.($row ?? 0).']', $no_produk ?? null,['placeholder' => 'Cari Barang', 'class' => 'form-control no_produk','required', 'id' => 'no_produk'.$row, 'onclick' => 'awas('.$row.')', 'readonly'])!!}
        {!! Form::hidden('produk_id_temp['.($row ?? 0).']', $produk_id, ['class' => 'produk_id_temp','id' => 'produk_id_temp'.$row]) !!}
        {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id,['class' => 'form-control produk_id','id' => 'produk_id'.$row, 'required'])!!}</td>
    <td>{!! Form::text('keterangan_produk['.($row ?? 0).']',$keterangan_produk,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.$row,'required'])!!}</td>
    <td>{!! Form::text('qty_produk['.($row ?? 0).']',$qty_produk,['class' => 'form-control qty_produk','id' => 'qty_produk'.$row,'required','onchange' => 'cekQtyandPrice('. $row .')'])!!}</td>
    <td>{!! Form::text('satuan_produk['.($row ?? 0).']',$satuan_produk,['class' => 'form-control satuan_produk','id' => 'satuan_produk'.$row,'required'])!!}</td>
    <td>{!! Form::text('required_date['.($row ?? 0).']',$required_date,['class' => 'form-control required_date format-tanggal','id' => 'required_date'.$row,'required'])!!}</td>
    <td>{!! Form::text('notes['.($row ?? 0).']',$notes,['class' => 'form-control notes','id' => 'notes'.$row,''])!!}</td>
    <td>{!! Form::text('qty_ordered['.($row ?? 0).']',$qty_ordered === null ? 0 : $qty_ordered,['class' => 'form-control qty_ordered','id' => 'qty_ordered'.$row,'readonly'])!!}</td>
    <td>{!! Form::text('qty_received['.($row ?? 0).']',$qty_received === null ? 0 : $qty_received,['class' => 'form-control qty_received','id' => 'qty_received'.$row,'readonly'])!!}</td>
    <td>{!! Form::select('dept_id['.($row ?? 0).']',[],null,['class' => 'form-control select2 dept_id','id' => 'dept_id'.$row,''])!!}</td>
    <td>{!! Form::select('proyek_id['.($row ?? 0).']',[],null,['class' => 'form-control select2 proyek_id','id' => 'proyek_id'.$row,''])!!}</td>
    <td><button href="" class="remove btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>