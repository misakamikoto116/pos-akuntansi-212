<div class="row" style="padding: 0px">
    <div class="col-md-6" style="padding: 0px">
        <div class="form-group row">
            <div class="col-2 col-form-label">No. Grop</div>
                <div class="col-10">
                    {!! Form::text('no_grop', null, ['class' => 'form-control', 'id' => 'no_grop']) !!}
                </div>
            </div>
  	    </div>
    </div>
    <div class="row" style="padding: 0px">
        <div class="col-md-12" style="padding: 0px">
            <div class="form-group row">
                <div class="col-1 col-form-label">keterangan</div>
                    <div class="col-10">
                        {!! Form::textarea('keterangan', null, ['class' => 'form-control', 'id' => 'keterangan']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <div class="col-md-12" style="padding: 0px">
        <div class="form-group row">
            <div class="col-2 col-form-label">Harga Jual Barang</div>
  			    <div class="col-2">
  				    {!! Form::text('harga_jual',null,['class' => 'form-control','id' => 'harga_jual_barang'])!!}
  			    </div>
  			    <div class="col-1">
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal">Rincian</button>
			    </div>
            </div>
            <div class="form-group row">
                <div class="col-2 col-form-label">Status</div>
                <div class="col-2">{!! Form::select('status',["0" => "Tidak Aktif","1" => "Aktif"], 1,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'status'])!!}</div>
                <div class="col-2 col-form-label">Kode Pajak Penjualan</div>
                <div class="col-2">{!! Form::select('kode_pajak_penj_id',$kodePajak,null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => ''])!!}</div>
                <div class="col-1 col-form-label">Unit</div>
                <div class="col-2">{!! Form::text('unit',null,['class' => 'form-control unit-change','id' => 'unit-change-persediaan'])!!}</div>
            </div>
  		    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content" id="#harga-section">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Rincian Harga Barang</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-2 col-form-label">Harga 1</div>
                                <div class="col-10">{!! Form::text('tingkat_1',isset($item) ? $item->tingkatHargaBarang->tingkat_1 ?? 0 : null,['class' => 'form-control','id' => 'harga_tingkat_1'])!!}</div>
                            </div>
                            <div class="form-group row">
                                <div class="col-2 col-form-label">Harga 2</div>
                                <div class="col-10">{!! Form::text('tingkat_2',isset($item) ? $item->tingkatHargaBarang->tingkat_2 ?? 0 : null,['class' => 'form-control','id' => ''])!!}</div>
                            </div>
                            <div class="form-group row">
                                <div class="col-2 col-form-label">Harga 3</div>
                                <div class="col-10">{!! Form::text('tingkat_3',isset($item) ? $item->tingkatHargaBarang->tingkat_3 ?? 0 : null,['class' => 'form-control','id' => ''])!!}</div>
                            </div>
                            <div class="form-group row">
                                <div class="col-2 col-form-label">Harga 4</div>
                                <div class="col-10">{!! Form::text('tingkat_4',isset($item) ? $item->tingkatHargaBarang->tingkat_4 ?? 0 : null,['class' => 'form-control','id' => ''])!!}</div>
                            </div>
                            <div class="form-group row">
                                <div class="col-2 col-form-label">Harga 5</div>
                                <div class="col-10">{!! Form::text('tingkat_5',isset($item) ? $item->tingkatHargaBarang->tingkat_5 ?? 0 : null,['class' => 'form-control','id' => ''])!!}</div>
                            </div>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="save-tingkat-harga" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
    <div class="col-md-12" style="padding: 0px">
        <div class="form-group row">
            <div class="col-2 col-form-label">Akun Penjualan</div>
            <div class="col-sm-3"><input class="form-control" type="" name="akun_penjualan_code" readonly="" value="{{ !isset($item) && !empty($prefBarang) ? $prefBarang->first()->akunPenjualan->kode_akun : null }}"></div>
            <div class="col-sm-6">
                {!! Form::select('akun_penjualan_id',$akun, !isset($item) ? $prefBarang[0]->akun_penjualan_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '','onchange' => 'getCodeAkun(this.value, "akun_penjualan_code")'])!!}
            </div>
  	    </div>
        <div class="form-group row ak-retur">
            <div class="col-2 col-form-label">Akun Retur Penjualan</div>
            <div class="col-sm-3"><input class="form-control" type="" name="akun_ret_penjualan_code" readonly="" value="{{ !isset($item) && !empty($prefBarang) ? $prefBarang->first()->akunReturPenjualan->kode_akun : null }}"></div>
            <div class="col-sm-6">
                {!! Form::select('akun_ret_penjualan_id',$akun, !isset($item) ? $prefBarang[0]->akun_retur_penjualan_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '','onchange' => 'getCodeAkun(this.value, "akun_ret_penjualan_code")'])!!}
            </div>
  	    </div>
        <div class="form-group row">
            <div class="col-2 col-form-label">Akun Diskon Penjualan</div>
            <div class="col-sm-3"><input class="form-control" type="" name="akun_disk_penjualan_code" readonly="" value="{{ !isset($item) && !empty($prefBarang) ? $prefBarang->first()->akunDiskonBarang->kode_akun : null }}"></div>
            <div class="col-sm-6">
                {!! Form::select('akun_disk_penjualan_id',$akun, !isset($item) ? $prefBarang[0]->akun_diskon_barang_id : null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => '','onchange' => 'getCodeAkun(this.value, "akun_disk_penjualan_code")'])!!}
            </div>
        </div>
    </div>
</div> <!-- End Tab-Content -->
<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>
<div class="grop-barang-sections" data-id="1">
<div class="duplicate-grop-barang-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab">
  	<table width="100%" class="table">
  		<thead class="thead-light" style="border-collapse: collapse;">
  			<tr>
  				<th width="20%">Nomor Barang</th>
  				<th>Deskripsi Barang</th>
  				<th>Kuantitas</th>
  				<th></th>
  			</tr>
  		</thead>
  		<tbody class="gropbarang">
            
  		</tbody>
  	</table>
  	<a class="btn btn-info add-grop-barang" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
  </div>
</div>
</div>
<script type="text/template" id="table_grop_barang_section" data-id="">
	<tr>
		<td>{!! Form::select('produk_id[]',$produk ?? [], null,['placeholder' => '-Pilih Barang-','class' => 'form-control select2 produk_id', 'required'])!!}</td>
		<td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}</td>
		<td>{!! Form::number('qty_produk[]',null,['class' => 'form-control qty_produk','required'])!!}</td>
		<td><button href="" class="remove btn btn-danger remove-rincian-grop-barang" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
	</tr>
</script>