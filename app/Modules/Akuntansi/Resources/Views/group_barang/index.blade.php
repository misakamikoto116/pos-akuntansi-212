@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Barang</h4>
        <ol id="breadcrumb">
            <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
            <li><a href="#">Daftar Barang</a></li>
        </ol>

    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Daftar Group Barang</h5>
            <div class="menu-header">
            <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                <span class="btn-label"><i class="fa fa-plus"></i></span>
                Tambah
            </a>
            </div>
            </div>
            <div class="card-body">
                   <table class="table">
                    <thead>
                        <tr>
                        <th>No</th>
                        <th>No Barang</th>
                        <th>Keterangan</th>
                        <th>Kuantitas</th>
                        <th>Satuan</th>
                        <th>Harga Satuan</th>
                        <th>Kategori</th>
                        <th>Tipe</th>
                        <th>Tipe Persediaan</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->no_barang}}</td>
                                <td>{{ $item->keterangan}}</td>
                                <td>{!! $item->updated_qty !!}</td>
                                <td>{{ $item->unit}}</td>
                                <td class="mask">{{ $item->harga_jual}}</td>
                                <td>{{ $item->kategoriProduk->nama ?? null }}</td>
                                <td>{!! $item->tipe_formatted !!}</td>
                                <td>{!! $item->tipe_persediaan_formatted !!}</td>
                                <td>
                                {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i>&emsp;Edit</a>
                                        <a class="dropdown-item deleteBtn" href="#">
                                            <i class="fa fa-trash"></i>&emsp;Delete
                                        </a>
                                        <a class="btn btn-default dropdown-item show" href="{{ route($module_url->show, $item->id) }}"><i class="fa fa-history"></i>&emsp;History</a>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection