@extends('chichi_theme.layout.app')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 10px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .disabled-select {
        background-color:#d5d5d5;
        opacity:0.5;
        border-radius:3px;
        cursor:not-allowed;
        position:absolute;
        top:0;
        bottom:0;
        right:0;
        left:0;
    }
</style>

@endsection

@section('content')
    <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ url('akuntansi/grop-barang') }}">Daftar Group</a></li>
                    </ol>


                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id], 'method' => 'Put','class' => 'form-horizontal form-label-left', 'id' => 'form-pencatatan-nomor-serial']) !!}
                 @include($form)
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')
        <script type="text/javascript">
        var q = 0;
        
        function duplicateGropBarang(){
            var row = $('#table_grop_barang_section').html();
            var clone = $(row).clone();
            clone.find('.produk_id').attr('name','produk_id[]').attr('id','produk_id'+q).attr('onchange','test('+q+')').attr('class','select2 form-control');
            clone.find('.keterangan_produk').attr('name','keterangan_produk[]').attr('id','keterangan_produk'+q);
            clone.find('.qty_produk').attr('name','qty_produk[]').attr('id','qty_produk'+q);
            $(clone).appendTo($('.duplicate-grop-barang-sections').closest('.grop-barang-sections').find('.gropbarang'));
            q++;
            $('.select2').select2();
        }

        $(document).ready(function () {
            $('.duplicate-grop-barang-sections').on('click', '.add-grop-barang', function () {
                duplicateGropBarang();
            });
        });

        function test(vale) {
            var produk_id   = $('#produk_id'+vale).val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-produk') }}",
                    data: {id : produk_id,
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                    $('#keterangan_produk'+vale).val(response.keterangan);
                    $('#qty_produk'+vale).val('1');
                    }, failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
        }

        $('.duplicate-grop-barang-sections').on('click', '.remove-rincian-grop-barang', function () {
            if ($(this).closest('.gropbarang').find('tr').length > 0) {
                $(this).closest('tr').remove();
            }
        });

        function getCodeAkun(id, name){
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-akun_id') }}",
                data: {id : id,
                _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                        $("input[name='"+name+"']").val(response.kode_akun);
                    }, error: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        $("#save-tingkat-harga").on('click', function(e) {
            e.preventDefault();
            $("#harga_jual_barang").val($("#harga_tingkat_1").val()).val();
        });
        </script>
@endsection