@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ url('akuntansi.kode-pajak') }}">Daftar Kode Pajak</a></li>
                      <li><a href="#">Tambah Kode Pajak</a></li>
                    </ol>

                </div>
            </div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left']) !!}

                @include($form)
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')
<script type="text/javascript">
    // ajax get kode_akun
$( ".target2" ).change(function() {
            // alert('aas');
            var nama_akun   = $('#akun_pajak_pembelian_id').val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-akun_id') }}",
                data: {id : nama_akun,
                     _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#kode_akun_pembelian').val(response.kode_akun)
                },
                failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        });

$( ".target1" ).change(function() {
            var nama_akun   = $('#akun_pajak_penjualan_id').val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-akun_id') }}",
                data: {id : nama_akun,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#kode_akun_penjualan').val(response.kode_akun)
                },
                failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        });

</script>
@endsection