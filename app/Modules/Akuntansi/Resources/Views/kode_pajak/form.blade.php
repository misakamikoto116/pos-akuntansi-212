<div class="form-group row">
	{!! Form::label('nama','Nama',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::text('nama',null,['class' => 'form-control','id' => 'nama'])!!}
</div>
</div>
<div class="form-group row">
	{!! Form::label('nilai','% Nilai',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::number('nilai',null,['class' => 'form-control','id' => 'nilai'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('kode','Kode',['class' => 'col-2 col-form-label']) !!}
	<div class="col-2">
	{!! Form::text('kode',null,['class' => 'form-control','id' => 'kode'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('keterangan','Keterangan',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
        {!! Form::text('keterangan',null,['class' => 'form-control','id' => 'keterangan'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('akun_pajak_penjualan_id','Akun Pajak Penjualan',['class' => 'col-2 col-form-label']) !!}
	<div class="col-3">
	@if (isset($item))
		{!! Form::text('kode_akun_penjualan',$item->akunpajakpenjualan->kode_akun,['class' => 'form-control','id' => 'kode_akun_penjualan','readonly'])!!}
        @else
		{!! Form::text('kode_akun_penjualan',null,['class' => 'form-control','id' => 'kode_akun_penjualan','readonly'])!!}
        @endif
	</div>
	<div class="col-md-7">
		{{-- <select class="form-control target1 select2" name="akun_pajak_penjualan_id" id="akun_pajak_penjualan_id">
			<option value="">- Pilih -</option>
			@foreach ($listAkun as $key => $akun)
			<option value="{{$key}}">{{$akun}}</option>
			@endforeach
		</select> --}}
		{!! Form::select('akun_pajak_penjualan_id',$listAkun,null,['class' => 'form-control target1 select2','id' => 'akun_pajak_penjualan_id'])!!}
		</div>
</div>
<div class="form-group row">
	{!! Form::label('akun_pajak_pembelian_id','Akun Pajak Pembelian',['class' => 'col-2 col-form-label']) !!}
	<div class="col-3">
	@if (isset($item))
		{!! Form::text('kode_akun_pembelian',$item->akunpajakpembelian->kode_akun,['class' => 'form-control','id' => 'kode_akun_pembelian','readonly'])!!}
        @else
		{!! Form::text('kode_akun_pembelian',null,['class' => 'form-control','id' => 'kode_akun_pembelian','readonly'])!!}
        @endif
	</div>
	<div class="col-md-7">
{{-- 		<select class="form-control target2 select2" name="akun_pajak_pembelian_id" id="akun_pajak_pembelian_id">
			<option value="">- Pilih -</option>
			@foreach ($listAkun as $key => $akun)
			<option value="{{$key}}">{{$akun}}</option>
			@endforeach
		</select> --}}
		{!! Form::select('akun_pajak_pembelian_id',$listAkun,null,['class' => 'form-control target2 select2','id' => 'akun_pajak_pembelian_id'])!!}
		</div>
</div>
