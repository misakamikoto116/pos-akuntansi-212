@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Pemberitahuan / notification</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Kirim Pemberitahuan</h5>
                </div>

                <div class="card-body">
                    {{ Form::open(['url' => route('akuntansi.pemberitahuan.kirim.post'), 'method' => 'post']) }}
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Tujuan</label>
                        {!! Form::select('tujuan', $tujuan,null, ['placeholder' => '- Pilih -','class' => 'form-control select2','id' => 'tujuan'])!!}
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Pesan yang dikirim</label>
                        {!! Form::textarea('pesan',null,['class' => 'form-control','id' => 'pesan'])!!}
                    </div>
                    <div class="form-group">
                        <button class="btn btn-default">Kirim</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Daftar Pemberitahuan</h5>
                </div>
                <div class="card-body">
                    @if(!$data->isEmpty())
                        <table class="table table-bordered ">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th width="10%">Tujuan</th>
                                <th>Isi Pemberitahuan</th>
                                <th>Options</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($data as $row)
                                <tr id="{{ $row->id }}">
                                    <td style="width: 5%">{{ $loop->iteration }}</td>
                                    <td>{{ $row->tujuan }}</td>
                                    <td>{{ $row->isi_pemberitahuan }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item edit" href="{{ route('akuntansi.iklan-manage.edit.get', $row->id) }}"><i class="fa fa-pencil"></i>&emsp;Kirim Ulang</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-warning"> Tidak ada data. </div>
                    @endif
                    <div class="pull-right">
                        {!! $data->links('vendor.pagination.bootstrap-4'); !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script src="{{ asset('assets/plugins/notifyjs/js/notify.js') }}"></script>
    <script src="{{ asset('assets/plugins/notifications/notify-metro.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $('.select2').select2();
    </script>

@endsection