<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3);">
  <li class="nav-item">
    <a class="nav-link active" id="pills-alamat-tab" data-toggle="pill" href="#pills-alamat" role="tab" aria-controls="pills-alamat" aria-selected="true">Alamat</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-termin-tab" data-toggle="pill" href="#pills-termin" role="tab" aria-controls="pills-termin" aria-selected="false">Termin, dll</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-penjualan-tab" data-toggle="pill" href="#pills-penjualan" role="tab" aria-controls="pills-penjualan" aria-selected="false">Penjualan</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-kontak-tab" data-toggle="pill" href="#pills-kontak" role="tab" aria-controls="pills-kontak" aria-selected="false">Kontak</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-catatan-tab" data-toggle="pill" href="#pills-catatan" role="tab" aria-controls="pills-catatan" aria-selected="false">Catatan</a>
  </li>
</ul>

<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-alamat" role="tabpanel" aria-labelledby="pills-alamat-tab">
    <i style="color:crimson; margin:auto -10px;">Tanda * Wajib Diisi</i>
    <div class="form-group row">
        <label class="col-2 col-form-label">No. Pelanggan <i style="color:crimson">*</i></label>
  <div class="col-10">
  {!! Form::text('no_pelanggan',null,['class' => 'form-control','id' => 'no_pelanggan'])!!}
</div>
</div>
<div class="form-group row">
    <div class="col-6">
      <div class="form-group row">
        <label class="col-4 col-form-label">Nama Pelanggan <i style="color:crimson">*</i></label>
        <div class="col-8">
            {!! Form::text('nama',null,['class' => 'form-control','id' => 'nama'])!!}
        </div>
      </div>
      <div class="form-group row">
        <label class="col-4 col-form-label">NIK Pelanggan <i style="color:crimson">*</i></label>
        <div class="col-8">
          {!! Form::text('nik',null,['class' => 'form-control','id' => 'nama'])!!}
        </div>
      </div>
      <div class="form-group row">
          <label class="col-4 col-form-label">Alamat <i style="color:crimson">*</i></label>
        <div class="col-8">
        {!! Form::textarea('alamat',null,['class' => 'form-control','id' => 'alamat'])!!}
        </div>
      </div>
      <div class="form-group row">
        {!! Form::label('alamat_pajak','Alamat Pajak',['class' => 'col-4 col-form-label']) !!}
        <div class="col-8">
        {!! Form::textarea('alamat_pajak',null,['class' => 'form-control','id' => 'alamat_pajak'])!!}
        </div>
      </div>
    </div>
    <div class="col-sm-3" style="border-right: 1px solid #f0f0f0;">
      <label for="">Foto Pelanggan</label>
      <div style="width: 100%; min-height: 200px; background: #f0f0f0">
        <img src="{{ ( !empty($item) == true ) ? asset('storage/pelanggan/'.$item->foto_pelanggan) : null  }}" alt="" id="img_pelanggan" style="width: 100%; height: 200px;">
      </div>
      <div style="width: 100%; margin: 20px 0; float: right;">
        <input name="foto_pelanggan" type="file" class="image_foto_pelanggan filestyle form-control" data-iconname="fa fa-cloud-upload">
      </div>
    </div>
    <div class="col-sm-3" style="border-right: 1px solid #f0f0f0;">
      <label for="">Foto NIK Pelanggan</label>
      <div style="width: 100%; min-height: 200px; background: #f0f0f0">
        <img src="{{ ( !empty($item) == true ) ? asset('storage/pelanggan/'.$item->foto_nik_pelanggan) : null  }}" alt="" id="img_nik_pelanggan" style="width: 100%; height: 200px;">
      </div>
      <div style="width: 100%; margin: 20px 0; float: right;">
        <input name="foto_nik_pelanggan" type="file" class="image_foto_nik_pelanggan filestyle form-control" data-iconname="fa fa-cloud-upload">
      </div>
    </div>
</div>
<div class="form-group row">
  {!! Form::label('kota','Kota/Prop/Kode Pos',['class' => 'col-2 col-form-label']) !!}
  <div class="col-4">
  {!! Form::text('kota',null,['class' => 'form-control','id' => 'kota'])!!}
  </div>
  <div class="col-3">
  {!! Form::text('prop',null,['class' => 'form-control','id' => 'prop'])!!}
  </div>
  <div class="col-3">
  {!! Form::text('kode_pos',null,['class' => 'form-control','id' => 'kode_pos'])!!}
  </div>
</div>
<div class="form-group row">
  {!! Form::label('negara','Negara',['class' => 'col-2 col-form-label']) !!}
  <div class="col-10">
  {!! Form::text('negara',null,['class' => 'form-control','id' => 'negara'])!!}
  </div>
</div>
<div class="form-group row">
  {!! Form::label('telepon','Telepon',['class' => 'col-2 col-form-label']) !!}
  <div class="col-10">
  {!! Form::text('telepon',null,['class' => 'form-control','id' => 'telepon'])!!}
  </div>
</div>
<div class="form-group row">
  {!! Form::label('personal_kontak','Personal Kontak',['class' => 'col-2 col-form-label']) !!}
  <div class="col-10">
  {!! Form::text('personal_kontak',null,['class' => 'form-control','id' => 'personal_kontak'])!!}
  </div>
</div>
<div class="form-group row">
  {!! Form::label('email','Email',['class' => 'col-2 col-form-label']) !!}
  <div class="col-10">
  {!! Form::text('email',null,['class' => 'form-control','id' => 'email'])!!}
  </div>
</div>
<div class="form-group row">
  {!! Form::label('web','Halaman Web',['class' => 'col-2 col-form-label']) !!}
  <div class="col-10">
  {!! Form::text('halaman_web',null,['class' => 'form-control','id' => 'web'])!!}
  </div>
</div>
{{-- <div class="form-group row">
  {!! Form::label('mataUang','Mata Uang',['class' => 'col-2 col-form-label']) !!}
  <div class="col-10">
        {!! Form::select('mata_uang_id',$mataUang,null,['class' => 'form-control','id','select2' => 'mata_uang_id'])!!}
  </div>
</div> --}}

  </div>
  <div class="tab-pane fade" id="pills-termin" role="tabpanel" aria-labelledby="pills-termin-tab">
    <fieldset>
      <legend>Termin</legend>
      <div class="form-group row">
      <div class="col-1 col-form-label">Termin</div>
    <div class="col-6 drop">
    {!! Form::select('syarat_pembayaran_id',$termin,null,['placeholder' => '- Pilih -','class' => 'form-control select2','id' => 'syarat_pembayaran_id'])!!}
    </div>
    </div>

  <div class="row">
    <div class="col-md-2 offset-md-1">% Diskon &emsp; <big id="diskon_termin">0</big></div>
    <div class="col-md-2">Hari Disk &emsp; <big id="hari_diskon_termin">0</big></div>
    <div class="col-md-3">Jatuh Tempo &emsp; <big id="jatuh_tempo_termin">0</big></div>
  </div>
    </fieldset>

    <fieldset>
      <legend>Batasan Piutang</legend>
      <div class="form-group row">
        <div class="col-3 col-form-label">
          Jika tidak ada faktur beredar melebihi
        </div>
        <div class="col-1">
          {!! Form::number('batasan_piutang_hari',null,['class' => 'form-control','id' => 'batasan_piutang_hari'])!!}
        </div>
        <div class="col-2 col-form-label">hari yang lalu</div>
        <div class="col-4 col-form-label warn"><i class="fa fa-warning"></i> Set nol jika pelanggaran tidak ada batasan kredit</div>
      </div>
      <div class="form-group row">
        <div class="col-3 col-form-label">
          Jika piutang tidak melebihi dari
        </div>
        <div class="col-3">
          {!! Form::number('batasan_piutang_uang',null,['class' => 'form-control','id' => 'batasan_piutang_uang'])!!}
        </div>
      </div>
    </fieldset>

    <fieldset>
      <legend>Lain</legend>
        <div class="form-group row">
        <div class="col-1 col-form-label">Mata Uang</div>
        <div class="col-10">
          {!! Form::select('mata_uang_id',$mataUang,null,['placeholder' => '- Pilih -','class' => 'form-control select2','id' => 'mata_uang'])!!}
        </div>
        </div>
        <div class="saldo-sections">
          <div class="form-group row">
            <div class="col-1 col-form-label">Saldo Awal</div>
            <div class="col-3">
              {!! Form::text('saldo_awal',null,['class' => 'form-control saldo-awal-input','id' => 'saldo_awal', 'readonly'])!!}
            </div>
            <div class="col-2"><button type="button" class="btn btn-default waves-effect waves-light tombol_pelanggan" id="" data-toggle="modal" data-target="#rincian">Rincian Saldo</button>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-1 col-form-label">Pesan</div>
            <div class="col-11">
              {!! Form::textarea('pesan',null,['class' => 'form-control','id' => 'pesan'])!!}
            </div>
          </div>
          {{-- Modal --}}
        <div class="duplicate-saldo-sections saldo_sections">
          <div class="modal fade" id="rincian" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h4 class="modal-title mt-0" id="myModalLabel">Saldo Awal</h4>
                      </div>
                      <div class="modal-body">
                          <table align="center" class="table">
                              <thead>
                                  <tr>
                                      <td style="text-align: center;">No. Faktur</td>
                                      <td style="text-align: center;">Tanggal</td>
                                      <td style="text-align: center;">Saldo Awal</td>
                                      <td style="text-align: center;">Syarat Pembayaran</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                  </tr>
                              </thead>
                              <tbody class="purchaseItemsSaldo">
                              </tbody>
                          </table>
                          <a class="btn btn-primary btn-rounded add" href="#" role="button" style="margin-bottom: 30px;">
                          <span class="btn-label"><i class="fa fa-plus"></i></span>
                          Tambah Saldo Awal</a>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-primary waves-effect waves-light simpan-saldo">Save changes</button>
                      </div>
                  </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
              </div>
        </div>
    </fieldset>



  </div>

  <script id="table_saldo_section" type="text/template">
        <tr>
            <td>
                <input type="text" name="no_faktur_saldo[]" value=""  class="form-control no_faktur_saldo" id="">
            </td>
            <td>
                <input type="text" name="tanggal_saldo[]" value="" class="form-control date-saldo tanggal_saldo" id="">
            </td>
            <td>
                <input type="text" name="saldo_awal[]" value="0"  class="form-control mask saldo_awal" id="">
            </td>
            <td>
               {!! Form::select('syarat_pembayaran_saldo_id[]',$termin,null,['class' => 'form-control select2','id' => 'syarat_pembayaran_saldo_id','placeholder' => '- Pilih -'])!!}
            </td>
            <td>
                <button type="button" class="removeRow btn btn-danger btn-sm">-</button>
            </td>
        </tr>
    </script>

  <div class="tab-pane fade" id="pills-penjualan" role="tabpanel" aria-labelledby="pills-penjualan-tab">

    <fieldset>
      <legend>Pajak</legend>

      <div class="form-group row">
      <div class="col-2 col-form-label">Pajak 1</div>
    <div class="col-6 drop">
    {!! Form::select('pajak_satu_id',$kodePajak,null,['placeholder' => '- Pilih -','class' => 'form-control select2','id' => 'pajak_satu_id'])!!}
    </div>
    </div>

    <div class="form-group row">
      <div class="col-2 col-form-label">Pajak 2</div>
    <div class="col-6 drop">
    {!! Form::select('pajak_dua_id',$kodePajak,null,['placeholder' => '- Pilih -','class' => 'form-control select2','id' => 'pajak_dua_id'])!!}
    </div>
    </div>

    <div class="row">
      <div class="offset-2 col-10">
        <div class="form-check">
      {!! Form::checkbox('pajak_faktur',1, (!empty($item) ? $item->pajak_faktur == 1 ? true : '' : ''), ['id' => 'pajak_faktur'])!!}
      <label class="col-form-label" for="exampleCheck1">Default Pajak Faktur "Termasuk Pajak"</label>
    </div>
    </div>
    </div>

    <div class="form-group row">
      <div class="col-2 col-form-label">NPWP Pelanggan</div>
      <div class="col-sm-10">
       {!! Form::text('npwp',null,['class' => 'form-control','id' => 'npwp'])!!}
      </div>
    </div
    ><div class="form-group row">
      <div class="col-2 col-form-label">NPPKP</div>
      <div class="col-sm-10">
       {!! Form::text('nppkp',null,['class' => 'form-control','id' => 'nppkp'])!!}
      </div>
    </div>

    <div class="form-group row">
      <div class="col-2 col-form-label">Tipe Pajak</div>
    <div class="col-4 drop">
    {!! Form::select('tipe_pajak_id',$tipePajak,null,['placeholder' => '- Pilih -','class' => 'form-control select2','id' => 'tipe_pajak_id'])!!}
    </div>
    </div>

    </fieldset>

    <fieldset>
      <legend>Lain</legend>

      <div class="form-group row">
      <div class="col-2 col-form-label">Tipe Pelanggan</div>
    <div class="col-6 drop">
    {!! Form::select('tipe_pelanggan_id',$tipePelanggan,null,['placeholder' => '- Pilih -','class' => 'form-control select2','id' => 'tipe_pelanggan_id'])!!}
    </div>
    </div>

    <div class="form-group row">
      <div class="col-2 col-form-label">Tingkatan Harga Jual</div>
    <div class="col-4 drop">
    {!! Form::select('tingkatan_harga_jual',['1' => '1',
                                             '2' => '2',
                                             '3' => '3',
                                             '4' => '4',
                                             '5' => '5'],null,['placeholder' => '- Pilih -','class' => 'form-control select2','id' => 'tipe_pajak_id'])!!}
    </div>
    </div>

    <div class="form-group row">
      <div class="col-2 col-form-label">Default Disk. Penjualan</div>
      <div class="col-sm-10">
        {!! Form::text('disk_default',null,['class' => 'form-control','id' => 'disk_default'])!!}
      </div>
    </div>

    </fieldset>

  </div>

  <script id="table_kontak" type="text/template">
      <tr class="form-section">
            <td style="padding: 5px;"><input type="text" class="form-control nama_kontak" name="nama_kontak[]"></td>
            <td style="padding: 5px;"><input type="text" class="form-control nama_depan_kontak" name="nama_depan_kontak[]"></td>
            <td style="padding: 5px;"><input type="text" class="form-control jabatan_kontak" name="jabatan_kontak[]"></td>
            <td style="padding: 5px;"><input type="text" class="form-control telepon_kontak" name="telepon_kontak[]"></td>
            <td style="padding: 5px;"><a href="#" class="remove btn btn-danger btn-hapus-kontak"><i class="fa fa-times"></i></a></td>
        </tr>
  </script>

  <div class="tab-pane fade" id="pills-kontak" role="tabpanel" aria-labelledby="pills-kontak-tab">
    <div id="div1">
        <table class="duplicate-sections" cellspacing="7px;">
          <tr>
              <td style="padding: 5px;">Nama</td>
              <td style="padding: 5px;">Nama Depan</td>
              <td style="padding: 5px;">Title Jabatan</td>
              <td style="padding: 5px;">Telepon</td>
              <td style="padding: 5px;"></td>
          </tr>
          <tbody class="bodyKontak">

          </tbody>
          <tr class="form-section">
              <td style="padding: 5px;"><input type="text" class="form-control nama_kontak" name="nama_kontak[]"></td>
              <td style="padding: 5px;"><input type="text" class="form-control nama_depan_kontak" name="nama_depan_kontak[]"></td>
              <td style="padding: 5px;"><input type="text" class="form-control jabatan_kontak" name="jabatan_kontak[]"></td>
              <td style="padding: 5px;"><input type="text" class="form-control telepon_kontak" name="telepon_kontak[]"></td>
              <td style="padding: 5px;"><a href="#" class="remove btn btn-danger btn-hapus-kontak"><i class="fa fa-times"></i></a></td>
          </tr> <!-- End .form-section -->
          <tr>
              <td colspan="6"></td>
          </tr>
        </table> <!-- End .duplicate-section -->
      <a class="btn btn-info btn-duplicate addsection" href="#" role="button" data-section='0'><i class="fa fa-plus"> Tambah</i></a>
      <hr>
      </div> <!-- End Show up -->
  </div>

  <div class="tab-pane fade" id="pills-catatan" role="tabpanel" aria-labelledby="pills-catatan-tab">
    <div class="form-group">
    <label for="exampleFormControlTextarea1">Catatan</label>
    {!! Form::textarea('catatan',null,['class' => 'form-control','id' => 'catatan'])!!}
  </div>
  </div>
</div>