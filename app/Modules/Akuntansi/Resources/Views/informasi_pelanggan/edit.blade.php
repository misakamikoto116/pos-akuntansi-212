@extends( auth()->user()->hasPermissionTo('ubah_data_pelanggan') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 10px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Daftar Pelanggan</a></li>
                      <li><a href="#">Edit Pelanggan</a></li>
                    </ol>

                </div>
            </div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left', 'files' => true]) !!}
                @include($form)
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Perbarui',['type' => 'submit','class' => 'btn btn-default']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
    <!--Form Wizard-->
    <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

    <!--wizard initialization-->
    <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
    <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/starting.js') }}"></script>
    <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

    <script type="text/javascript">
    var sectionsCount = 0;   
        var q = 0;         
    
    $(document).ready(function () {
        getDetail( {{ $item->id }} );
        $('.duplicate-saldo-sections').on('click', '.add', function () {
            duplicateSaldoForm();
        });
        $('.duplicate-saldo-sections').on('click', '.removeRow', function () {
            if ($(this).closest('.purchaseItemsSaldo').find('tr').length > 0) {
                $(this).closest('tr').remove();
            }
        });
    });

    function simpanSaldo(param = null){
        var totalSaldo = 0;
        var itemSaldo = $('.simpan-saldo').closest('.modal').find('.purchaseItemsSaldo tr');

        $(itemSaldo).each(function(index, el) {
          var saldoPelanggan = $(el).find('.saldo_awal').autoNumeric('get');
          totalSaldo += parseFloat(saldoPelanggan);
        });
          var hasil_saldo = $('.saldo-awal-input');
          $(hasil_saldo).autoNumeric('init', {aPad: false, aForm:false});
          $(hasil_saldo).autoNumeric('set', totalSaldo);
        
          if(param == null){
              $('.simpan-saldo').closest('.modal').modal('hide');                  
          }
    }
    $('#syarat_pembayaran_id').on('change',function(){
        var str = $(this).find(":selected").text();
        var spl = str.split("/");
        $('#diskon_termin').html(spl[0]);
        $('#hari_diskon_termin').html(spl[1]);
        $('#jatuh_tempo_termin').html(spl[2].replace('n', ''));
    });
    $('.duplicate-saldo-sections').on('click', '.simpan-saldo', function () {
        simpanSaldo();
      });

      function duplicateKontakForm(){
            var row = $('#table_kontak').html();
            var clone = $(row).clone();

            clone.find('.nama_kontak').attr('name','nama_kontak[]').attr('id','nama_kontak'+sectionsCount);
            clone.find('.nama_depan_kontak').attr('name','nama_depan_kontak[]').attr('id','nama_depan_kontak'+sectionsCount);
            clone.find('.jabatan_kontak').attr('name','jabatan_kontak[]').attr('id','jabatan_kontak'+sectionsCount);
            clone.find('.telepon_kontak').attr('name','telepon_kontak[]').attr('id','telepon_kontak'+sectionsCount);
            clone.find('.btn-hapus-kontak');
            $(clone).appendTo($('.duplicate-sections').find('.bodyKontak'));
            sectionsCount++;
      }

      function duplicateSaldoForm(){
            var row = $('#table_saldo_section').html();
            var clone = $(row).clone();

            // clear the values
            clone.find('input[type=text]').val('');

            clone.find('.no_faktur_saldo').attr('name','no_faktur_saldo[]').attr('id','no_faktur_saldo'+q);
            clone.find('.tanggal_saldo').attr('name','tanggal_saldo[]').attr('id','tanggal_saldo'+q);
            clone.find('.saldo_awal').attr('name','saldo_awal[]').attr('id','saldo_awal'+q);
            clone.find('.syarat_pembayaran_saldo_id').attr('name','syarat_pembayaran_saldo_id[]').attr('id','syarat_pembayaran_saldo_id'+q);
            $(clone).appendTo($('.add').closest('.saldo-sections').find('.purchaseItemsSaldo'));

           $('.date-saldo').datepicker({format: 'dd MM yyyy', autoclose: true});
            $('.mask').autoNumeric('init', {aPad: false, aForm:false});
            q++;
      }

      function getDetail(id){
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-edit-informasi-pelanggan') }}",
                data: {id : id,
                _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                    $.each(response.data.kontak_pelanggan, function(index, key){
                    console.log(key);
                        duplicateKontakForm();
                        let temp = sectionsCount - 1;
                        $('#nama_kontak' + temp).val(key.nama);
                        $('#nama_depan_kontak' + temp).val(key.nama_depan);
                        $('#jabatan_kontak' + temp).val(key.jabatan);
                        $('#telepon_kontak' + temp).val(key.telepon_kontak);
                    });

                    $.each(response.data.detail_informasi_pelanggan, function(index, key){
                        duplicateSaldoForm();
                        let temp = q - 1;
                        $('#no_faktur_saldo' + temp).val(key.no_faktur);
                        $('#tanggal_saldo' + temp).val(key.tanggal);
                        $('#saldo_awal' + temp).val(key.saldo_awal);
                        $('#syarat_pembayaran_saldo_id' + temp).val(key.syarat_pembayaran_id);
                    });
                    simpanSaldo('triggered');
                    }, error: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        $('.image_foto_pelanggan').change(function(){
            var input = this;
            var url	  = $(this).val();
            var ext		= url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                    $('#img_pelanggan').attr('src', e.target.result);
                            }
                    reader.readAsDataURL(input.files[0]);
            }else{
                    $('#img_pelanggan').attr('src', '');
            }
        });

        $('.image_foto_nik_pelanggan').change(function(){
            var input = this;
            var url	  = $(this).val();
            var ext		= url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                    $('#img_nik_pelanggan').attr('src', e.target.result);
                            }
                    reader.readAsDataURL(input.files[0]);
            }else{
                    $('#img_nik_pelanggan').attr('src', '');
            }
        });
    </script>
@endsection