@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Jurnal Transaksi Pembayaran</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ route('pembayaran.index') }}">Daftar Pelanggan</a></li>
                      <li><a href="#">Detail Pelanggan</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Daftar Detail Data Pelanggan {{$detail_pelanggan->first()->informasipelanggan->nama}}</h5>
            <div class="menu-header">
                <a href="" class="btn btn-default btn-rounded waves-effect waves-light">
                <span class="btn-label"><i class="fa fa-plus"></i></span>
                Tambah
                </a>
            </div>
            </div>
            <div class="card-body">
                <div>
                <table class="table" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No. Faktur</th>
                                <th>Tanggal</th>
                                <th>Saldo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="example">
                            @foreach($detail_pelanggan as $detail)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$detail->no_faktur}}</td>
                                <td>{{$detail->tanggal}}</td>
                                <td>{{$detail->saldo_awal}}</td>
                                <td>{!! Form::open(['route' => ['akuntansi.delete.detail_pelanggan', $detail->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                            <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item edit" href="{{ route('akuntansi.edit.detail_pelanggan', $detail->id) }}"><i class="fa fa-pencil"></i>&emsp;Edit</a>
                                <a class="dropdown-item deleteBtn" href="#">
                                    <i class="fa fa-trash"></i>&emsp;Delete
                                </a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
            </div>


            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
    {{-- Masking Nominal --}}
    <script type="text/javascript">
        $('.jurnal_mask').autoNumeric('init',{aSign: "Rp "});
    </script>
@endsection