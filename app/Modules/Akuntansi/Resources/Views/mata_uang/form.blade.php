<div class="form-group row">
	{!! Form::label('nama','Nama',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::text('nama',null,['class' => 'form-control','id' => 'nama'])!!}
</div>
</div>
<div class="form-group row">
	{!! Form::label('negara','Negara',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::text('negara',null,['class' => 'form-control','id' => 'negara'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('simbol','Simbol',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	{!! Form::text('kode_simbol',null,['class' => 'form-control','id' => 'simbol'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('satuan','Satuan',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
        {!! Form::text('kode',null,['class' => 'form-control','id' => 'satuan'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('nilaiTukar','Nilai Tukar',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
        {!! Form::text('nilai_tukar',null,['class' => 'form-control mask','id' => 'nilai_tukar'])!!}
	</div>
</div>
