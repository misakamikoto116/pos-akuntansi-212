@extends('chichi_theme.layout.app')

@section('custom_css')
@endsection

@section('content')

<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Detail Promosi</a></li>
                    </ol>

                </div>
            </div>
<!-- END Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Detail Promosi</h5>
            <div class="menu-header">
            </div>
            </div>
            <div class="card-body">     
                <table class="table table-bordered" width="100%">
                    <tr>
                        <th width="15%">Nama Promosi</th>
                        <td>{{ $item->title}}</td>
                    </tr>
                    <tr>
                        <th>Berlaku dari</th>
                        <td>{{ $item->active_date_formatted}}</td>
                    </tr>
                    <tr>
                        <th>Berlaku s/d</th>
                        <td>{{ $item->expired_date_formatted}}</td>
                    </tr>
                </table>
                <hr>
                @if($item->sebab->count() != null)
                <b>Sebab Promosi</b>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <th>No</th>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>Total</th>
                            <th>Tipe Promosi</th>
                            <th>Tipe Barang Promosi</th>
                            <th>Kelipatan</th>
                        </tr>
                    @foreach ($item->sebab as $itemSebab)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $itemSebab->produk->keterangan ?? "-" }}</td>
                            <td>{{ $itemSebab->qty }}</td>
                            <td>{{ $itemSebab->total_formatted }}</td>
                            <td>{!! $itemSebab->tipe_formatted !!}</td>
                            <td>{!! $itemSebab->tipe_barang_formatted !!}</td>
                            <td>{!! $itemSebab->kelipatan_formatted !!}</td>
                        </tr>
                        <tr style="background:#f9fdff;">
                            <th rowspan="{{ $itemSebab->akibat->count() + 1 }}" colspan="2"><center>Akibat Promosi</center></th>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>Presentase</th>
                            <th>Total</th>
                        </tr>
                        @foreach($itemSebab->akibat as $itemAkibat)
                        <tr style="background:#f9fdff;">
                            <td>{{ $itemAkibat->produk->keterangan ?? "-" }}</td>
                            <td>{{ $itemAkibat->qty }}</td>
                            <td>{{ $itemAkibat->presentase }}</td>
                            <td>{{ $itemAkibat->total_formatted }}</td>
                        </tr>
                        @endforeach
                    @endforeach
                    </table>
                <hr>
                @endif
                @if($item->akibat->count() != null)
                <b>Akibat Promosi</b>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <th>No</th>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>Presentase</th>
                            <th>Total</th>
                        </tr>
                    @foreach ($item->akibat as $itemAkibat)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $itemAkibat->produk->keterangan ?? "-"}}</td>
                            <td>{{ $itemAkibat->qty }}</td>
                            <td>{{ $itemAkibat->presentase }}</td>
                            <td>{{ $itemAkibat->total_formatted }}</td>
                        </tr>
                        <tr style="background:#f9fdff;">
                            <th rowspan="{{ $itemAkibat->sebab->count() + 1 }}" colspan="2"><center>Sebab Promosi</center></th>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>Total</th>
                            <th>Tipe Promosi</th>
                            <th>Tipe Barang Promosi</th>
                            <th>Kelipatan</th>
                        </tr>
                        @foreach($itemAkibat->sebab as $itemSebab)
                        <tr style="background:#f9fdff;">
                            <td>{{ $itemSebab->produk->keterangan ?? "-"}}</td>
                            <td>{{ $itemSebab->qty }}</td>
                            <td>{{ $itemSebab->total_formatted }}</td>
                            <td>{!! $itemSebab->tipe_formatted !!}</td>
                            <td>{!! $itemSebab->tipe_barang_formatted !!}</td>
                            <td>{!! $itemSebab->kelipatan_formatted !!}</td>
                        </tr>
                        @endforeach
                    @endforeach
                    </table>
                @endif

            </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection

@section('custom_js')
    <script type="text/javascript">
        $('.mask_duit').autoNumeric('init',{aSign: "Rp "});
    </script>
@endsection
