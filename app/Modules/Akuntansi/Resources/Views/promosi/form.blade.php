<div class="form-group row">
	{!! Form::label('title','Judul Promosi',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
		{!! Form::text('title',null,['class' => 'form-control','id' => 'title'])!!}
		</div>
</div>
<div class="form-group row">
	{!! Form::label('active_date','Tanggal Mulai / Tanggal Selesai',['class' => 'col-2 col-form-label']) !!}
		<div class="col-4">
		{!! Form::text('active_date',null,['class' => 'form-control tanggal','id' => 'active_date'])!!}
		</div>
		<div class="col-2">
			<center>
				<b>Sampai Dengan</b>
			</center>
		</div>
		<div class="col-4">
		{!! Form::text('expired_date',null,['class' => 'form-control tanggal','id' => 'expired_date'])!!}
		</div>
</div>
<hr>
<a class="btn btn-primary" id="add-sebab"> <i class="fa fa-plus"></i> Tambah Sebab</a> &nbsp;
<a class="btn btn-primary" id="add-akibat"> <i class="fa fa-plus"></i> Tambah Akibat</a> &nbsp;
<a class="btn btn-danger" id="reset"> <i class="fa fa-trash"></i> Reset Promosi</a>
<div class="form-group row" id="duplicate_sebab">
	  
		<table width="100%" style="margin-top:10px" class="table table-bordered table-sebab">
			<thead>
				<tr class="success">
					<th>Item</th>
					<th width="10%">Qty</th>
					<th width="10%">Total</th>
					<th width="15%">Tipe Promosi</th>
					<th width="10%">Tipe Barang Promosi</th>
					<th width="20%">Kelipatan</th>
					<th width="10%">Option</th>
				</tr>
			</thead>
			<tbody id="table_sebab">
				
			</tbody>
		</table>
</div>

<script type="text/template" id="template_sebab" />
	<tr>
		<td>
	        {!! Form::select('produk_id[]',[],null,['placeholder' => '-Pilih-','class' => 'form-control produk_id urutan0','onchange'=>'checkItem(this)'])!!}
		</td>
		<td>
			{!! Form::number('qty[]', null, ['class' => 'form-control qty urutan0']) !!}
		</td>
		<td>
			{!! Form::number('total[]', null, ['class' => 'form-control total urutan0','required']) !!}
		</td>
		<td>
			{!! Form::select('type_sebab[]', ['0' => 'Produk', '1' => 'Akumulasi Belanja'], 0, ['placeholder' => '-Pilih-','class' => 'form-control type_sebab select3 urutan0']) !!}
		</td>
		<td>
			{!! Form::select('tipe_barang[]', ['0' => 'Non Food', '1' => 'Food'], null, ['placeholder' => '-Pilih-','class' => 'form-control tipe_barang select3 urutan0']) !!}
		</td>
		<td>
			<div class="row">
				<div class="col col-md-1">
					{!! Form::radio('kelipatan[]', '0', true, ['class' => 'form-control kelipatan urutan0']) !!}
				</div>
				<div class="col col-md-3">
					{!! Form::label('Tidak', null, ['class' => 'form label_kelipatan_tidak urutan0', 'style' => 'padding : 5px']) !!}
				</div>
				<div class="col col-md-4">
					{!! Form::radio('kelipatan[]', '1', false, ['class' => 'form-control kelipatan urutan0']) !!}
				</div>
				<div class="col col-md-3">
					{!! Form::label('Ya', null, ['class' => 'form label_kelipatan_ya urutan0', 'style' => 'padding : 5px']) !!}
				</div>
			</div>
		</td>
		<td>
			<a class="btn btn-success add-sebab-field urutan0" onclick="addColAkibat(0)"> <i class="fa fa-plus"></i> Tambah Akibat</a>
		</td>
	</tr>
	<tr>
		<th colspan="7" style="background-color: #81c868 !important; color: whitesmoke;"><center>Akibat</center></th>
	</tr>
	<tr>
		<td colspan="7">
			<table class="table">
				<thead>
					<tr>
						<th colspan="3"></th>
						<th width="228px">Item</th>
						<th width="10%">Qty</th>
						<th width="10%">Presentase</th>
						<th width="15%">Total</th>
					</tr>
				</thead>
				<tbody class="rowAkibat">
					
				</tbody>
			</table>
		</td>
	</tr>
</script>

<script type="text/template" id="template_akibat" />
	<tr>
		<td colspan="3">
		</td>
		<td>
			{!! Form::select('produk_id_akibat[][]',[],null,['placeholder' => '-Pilih-','class' => 'form-control produk_id urutan0','onchange'=>'checkItem(this)'])!!}
		</td>
		<td>
			{!! Form::number('qty_akibat[][]', null, ['class' => 'form-control qty_akibat urutan0']) !!}
		</td>
		<td>
			{!! Form::number('presentase_akibat[][]', 100, ['class' => 'form-control presentase_akibat urutan0']) !!}
		</td>
		<td>
			{!! Form::number('total_akibat[][]', null, ['class' => 'form-control total_akibat urutan0','required']) !!}
		</td>
	</tr>
</script>

{{-- Awal Form Akibat --}}
<div class="form-group row" id="duplicate_akibat">
	<table width="100%" style="margin-top:10px" class="table table-bordered table-akibat">
		<thead>
			<tr class="success">
				<th width="25%">Item</th>
				<th width="10%">Qty</th>
				<th width="10%">Presentase</th>
				<th width="10%">Total</th>
				<th width="10%">Tipe Promosi</th>
				<th width="10%">Tipe Barang Promosi</th>
				<th width="25%">Kelipatan</th>
				<th>Option</th>
			</tr>
		</thead>
		<tbody id="table_akibat">
			
		</tbody>
	</table>
</div>

<script type="text/template" id="template_akibat_promosi" />
	<tr>
		<td>
			{!! Form::select('produk_id_akibat_promosi[]',[],null,['placeholder' => '-Pilih-','class' => 'form-control produk_id urutan0','onchange'=>'checkItem(this)'])!!}
		</td>
		<td>
			{!! Form::number('qty_akibat_promosi[]', null, ['class' => 'form-control qty_akibat_promosi urutan0']) !!}
		</td>
		<td>
			{!! Form::number('presentase_akibat_promosi[]', 100, ['class' => 'form-control presentase_akibat_promosi urutan0']) !!}
		</td>
		<td>
			{!! Form::number('total_akibat_promosi[]', null, ['class' => 'form-control total_akibat_promosi urutan0','required']) !!}
		</td>
		<td>
			{!! Form::select('type_sebab_promosi[]', ['0' => 'Produk', '1' => 'Akumulasi Belanja'], 0, ['placeholder' => '-Pilih-','class' => 'form-control type_sebab_promosi select3 urutan0']) !!}
		</td>
		<td>
			{!! Form::select('tipe_barang_[]', ['0' => 'Non Food', '1' => 'Food'], null, ['placeholder' => '-Pilih-','class' => 'form-control tipe_barang select3 urutan0']) !!}
		</td>
		<td>
			<div class="row">
				<div class="col col-md-1">
					{!! Form::radio('kelipatan_sebab_promosi[]', '0', true, ['class' => 'form-control kelipatan_sebab_promosi urutan0']) !!}
				</div>
				<div class="col col-md-3">
					{!! Form::label('Tidak', null, ['class' => 'form label_kelipatan_sebab_promosi_tidak urutan0', 'style' => 'padding : 5px']) !!}
				</div>
				<div class="col col-md-4">
					{!! Form::radio('kelipatan_sebab_promosi[]', '1', false, ['class' => 'form-control kelipatan_sebab_promosi urutan0']) !!}
				</div>
				<div class="col col-md-3">
					{!! Form::label('Ya', null, ['class' => 'form label_kelipatan_sebab_promosi_ya urutan0', 'style' => 'padding : 5px']) !!}
				</div>
			</div>
		</td>
		<td>
			<a class="btn btn-success add-akibat-field urutan0" onclick="addColSebab(0)"> <i class="fa fa-plus"></i> Tambah Sebab</a>
		</td>
	</tr>
	<tr>
		<th colspan="7" style="background-color: #81c868 !important; color: whitesmoke;"><center>Sebab</center></th>
	</tr>
	<tr>
		<td colspan="7">
			<table class="table">
				<thead>
					<tr>
						<th>Item</th>
						<th width="10%">Qty</th>
						<th width="15%">Total</th>
					</tr>
				</thead>
				<tbody class="rowSebab">
					
				</tbody>
			</table>
		</td>
	</tr>
</script>

<script type="text/template" id="template_sebab_promosi" />
	<tr>
		<td>
			{!! Form::select('produk_id_sebab_promosi[][]',[],null,['placeholder' => '-Pilih-','class' => 'form-control produk_id urutan0','onchange'=>'checkItem(this)'])!!}
		</td>
		<td>
			{!! Form::number('qty_sebab_promosi[][]', null, ['class' => 'form-control qty_sebab_promosi urutan0']) !!}
		</td>
		<td>
			{!! Form::number('total_sebab_promosi[][]', null, ['class' => 'form-control total_sebab_promosi urutan0','required']) !!}
		</td>
	</tr>
</script>