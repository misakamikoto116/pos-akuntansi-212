@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Stiker Harga</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">        
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Upload Header</h5>
                </div>

                <div class="card-body">
                    {{ Form::open(['url' => route('akuntansi.upload.stiker.upload'), 'method' => 'post', 'files' => true]) }}
                        <div class="row">
                            <div class="col-md-2">
                                <input type="file" name="header">
                                <star>Disarankan ukuran ****</star>
                            </div>
                            <div class="col-md-2">
                                {{ Form::submit('Upload', ['class' => 'btn btn-success btn-fill']) }}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                   {{ $active == null ? 'Peringatan Stiker Harga Tidak Ada Yang Aktif' : '' }}
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th width="20%">Foto Stiker</th>
                                <th>Status</th>
                                <th>Options</th>
                            </tr>                            
                        </thead>

                        <tbody>
                            @foreach ($stickers as $sticker)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><image src="{{ asset('storage/sticker/header/'.$sticker->nama_stiker ) }}"></image></td>
                                    <td>{!! $sticker->aktif ? '<button class="btn btn-primary btn-fill">aktif</button>' : '<button class="btn btn-warning btn-fill">tidak aktif</button>' !!}</td>
                                    
                                    <td>
                                        {{ Form::open(['url' => route('akuntansi.upload.stiker.status', $sticker->id), 'method' => 'post']) }}
                                            @if(!$sticker->aktif)                                                                                    
                                                <button class="btn btn-fill btn-success">Aktifkan</button>
                                            @else                                        
                                                <button class="btn btn-fill btn-warning">Nonaktifkan</button>                                                                                        
                                            @endif                                                                                    
                                        {{ Form::close() }}
                                        {{ Form::open(['url' => route('akuntansi.upload.stiker.delete', $sticker->id), 'method' => 'post']) }}
                                            <button class="btn btn-danger btn-fill">Hapus</button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
