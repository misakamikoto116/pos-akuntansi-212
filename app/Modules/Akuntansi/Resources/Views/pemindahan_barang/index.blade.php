@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
    <div class="row">
        @include('chichi_theme.layout.filter')
        <div class="col-sm-12">
            <h4 class="page-title"></h4>
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Pemindahan Barang</a></li>
            </ol>
        </div>
    </div>   

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">Pemindahan Barang</h5>
                    @can('buat_pindah_barang')
                        <div class="menu-header">
                            <a href="{{ url('akuntansi/pemindahan-barang/create') }}" class="btn btn-default btn-rounded waves-effect waves-light">
                                <span class="btn-label"><i class="fa fa-plus"></i></span>
                                Tambah
                            </a>    
                        </div>
                    @endcan
                </div>
                <div class="card-body">
                    <table class="table">
                    @if($items->isEmpty())
                        <div class="alert alert-warning"> Tidak ada data. </div>
                    @else
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Transfer</th>
                                <th>Tanggal</th>
                                <th>Keterangan</th>
                                <th>Dari</th>
                                <th>s/d</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $key => $item)
                                <tr>
                                    <td>{{ $key + $items->firstItem() }}</td>
                                    <td>{{ $item->no_transfer }}</td>
                                    <td>{{ \Helpers\IndonesiaDate::indonesiaDate($item->tanggal) }}</td>
                                    <td>{{ $item->keterangan }}</td>
                                    <td>{{ $item->dataDariGudang->nama }}</td>
                                    <td>{{ $item->dataKeGudang->nama }}</td>
                                    <td>
                                        @if(auth()->user()->hasPermissionTo('ubah_pindah_barang') || auth()->user()->hasPermissionTo('hapus_pindah_barang') )
                                            {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                                    <div class="dropdown-menu">
                                                        @can('ubah_pindah_barang')
                                                            <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i> Edit</a>
                                                        @endcan
                                                        @can('hapus_pindah_barang')
                                                            <a class="dropdown-item deleteBtn" href="#">
                                                                <i class="fa fa-trash"></i>&emsp;Delete
                                                            </a>
                                                        @endcan
                                                    </div>
                                                </div>
                                            {!! Form::close() !!}
                                        @endif
                                    </td> 
                                </tr>
                            @endforeach
                        </tbody>
                    @endif
                    </table>
                    <div class="pull-right">
                        {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                    </div>
                </div>
            </div>   
        </div> <!-- container -->
    </div> <!-- content -->
@endsection

@section('custom_js')

@endsection
