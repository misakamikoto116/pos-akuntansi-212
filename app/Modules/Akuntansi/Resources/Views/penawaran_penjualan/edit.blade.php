@extends( auth()->user()->hasPermissionTo('ubah_penawaran_penjualan') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9{
        padding: 0 15px;
    }

    .warnatr{
        background: lightsteelblue;
        color: black;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Penjualan</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Penawaran Penjualan</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Sales Quotation / Penawaran Penjualan</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left','id' => 'form-penjualan-penawaran']) !!}
                 @include($form)
                </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary', 'id' => 'btn-cetak']) !!}
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" id="btn-submit" disabled data-toggle="dropdown">
                    Simpan <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        @if ( auth()->user()->hasPermissionTo('buat_pesanan_penjualan') )
                            <li>
                                {!! Form::submit('Pesanan Penjualan', ['class' => 'btn btn-success btn-block','value' => '1', 'name' => 'lanjutkan']) !!}
                            </li>
                        @endif
                        <li>
                            {!! Form::button('Simpan & Tutup',['type' => 'submit','class' => 'btn btn-warning btn-block']) !!}
                        </li>
                    </ul>
                </div>
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger']) !!}
                
                </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

        <script type="text/javascript">
            $('.tanggal_penawaran').datepicker({format: 'dd MM yyyy', autoclose: true});
            var q               = 0;
            var old_produk_id   = "{{ !empty(old('produk_id')) ? count(old('produk_id')) : null }}";
            
            $(function() {
              enable_cb();
              $("#group1").click(enable_cb);
              let form_old = "{{ old('pelanggan_id') }}";
              if(form_old == ""){
                setDataPelanggan('with-penawaran');
              }
              calculate();
            });

            if (old_produk_id !== null) {
                $(document).ready(function () {
                    z = 0;
                    q = $(".produk_id").length;
                    sumQtyAndUnit(z);
                    removeDelButton();
                });
            }

            function enable_cb() {
              if ($("input#group1").is(':checked')) {
                $("input.group2").removeAttr("disabled");
              } else {
                $("input.group2").attr("disabled", true).removeAttr("checked");
              }
            }

            $( "#pelanggan_id" ).change(function() {
                setDataPelanggan();
            });

            function setDataPelanggan(param = null){
                var pelanggan_id   = $('#pelanggan_id').val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.get-alamat-pelanggan') }}",
                    data: {id : pelanggan_id, type : 'edit',
                        _token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function(response){
                    $('.purchasePenawaran').find('tr').remove().end();                           
                    $('#alamat_pengiriman').val(response.alamat);
                    $('#alamat_asal').val(response.alamat);
                    $('#no_pelanggan').val(response.no_pelanggan);
                    setDetailBarang({{ $item->id }});
                    $('#no_pelanggan').attr('readonly','readonly');
                    $('#alamat_asal').attr('readonly','readonly');
                    $("#tax_cetak0").val(null);
                    $("#tax_cetak1").val(null);
                    $("#tax_cetak_label0").val(null);
                    $("#tax_cetak_label1").val(null);
                    q = 0;
                    }, error: function(errMsg) {
                        $('#no_pelanggan').removeAttr('readonly');
                        $('#alamat_asal').removeAttr('readonly');
                    }
                });
            }

        function setDetailBarang(id){
            var type = "penawaran";
            var pelanggan_id = $("#pelanggan_id").val();
            $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-detail-barang') }}",
                data: {id : id, type : type, pelanggan_id : pelanggan_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $.each(response.barang, function(index, key){
                    duplicateForm();
                    let queue = q - 1;
                    callSelect2AjaxProdukID(queue, key.produk.id);
                    $('#no_produk'+queue).val(key.produk.no_barang+' | '+key.produk.keterangan);
                    $('#produk_id'+queue).val(key.produk.id);
                    $('#tax_produk'+queue).find('option').remove().end();                        
                    $('#keterangan_produk'+queue).val(key.produk.keterangan);
                    $('#qty_produk'+queue).val(key.jumlah);
                    $('#satuan_produk'+queue).val(key.satuan);
                    $('#unit_harga_produk'+queue).autoNumeric('init',{aPad: false}).autoNumeric('set', key.harga);
                    $('#amount_produk'+queue).val(key.harga_total); //karena defaultnya di * 1, maka cukup unitprice saja
                    $('#diskon_produk'+queue).val(key.diskon);
                    $('#harga_modal'+queue).val(key.harga_modal);
                    $.each(response.kode_pajak, function (index, item){
                        if(item.id == key.kode_pajak_id || item.id == key.kode_pajak_2_id){
                            $('#tax_produk'+queue).append( 
                            $("<option></option>")
                            .text(item.nilai+ "/" +item.nama)
                            .val(item.id)
                            .attr('selected','selected')
                            );
                        }else {
                            $('#tax_produk'+queue).append( 
                            $("<option></option>")
                                .text(item.nilai+ "/" +item.nama)
                                .val(item.id)
                            );
                        }
                    });
                    sumQtyAndUnit(queue);
                    $('.select2').select2();
                });
                for(var a = 0; a < q; a++){
                    callSelect2AjaxProduk(a, 'modal');
                }
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        }

        $('#selectAll').click(function(e){
            var table= $(e.target).closest('table');
            $('th input:checkbox',table).prop('checked',this.checked);
        });
        function saveToSession()
        {
            $.get("{{ route('akuntansi.get-session-master-penawaran') }}", $("#form-penjualan-penawaran").serialize());
        }
        $( "#pelanggan_id" ).change(function() {
            var pelanggan_id   = $('#pelanggan_id').val();
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-alamat-pelanggan') }}",
                data: {id : pelanggan_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#alamat_pengiriman').val(response.alamat)
                $('#alamat_asal').val(response.alamat)
                $('#no_pelanggan').val(response.no_pelanggan)
                }, failure: function(errMsg) {
                    alert(errMsg);
                }
            });
            saveToSession();
        });

        function test(vale) {
                var produk_id   = $('#produk_id'+vale).val();
                var pelanggan_id   = $('#pelanggan_id').val();
                      $.ajax({
                        type: "GET",
                        url: "{{ route('akuntansi.get-produk') }}",
                        data: {id : produk_id, pelanggan_id : pelanggan_id,
                            _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                        $('#tax_produk'+vale).find('option').remove().end();                        
                        $('#keterangan_produk'+vale).val(response.keterangan);
                        $('#qty_produk'+vale).val('1');
                        $('#satuan_produk'+vale).val(response.satuan);
                        $('#unit_harga_produk'+vale).autoNumeric('init',{aPad: false}).autoNumeric('set', response.unitPrice);
                        $('#amount_produk'+vale).val(response.unitPrice); //karena defaultnya di * 1, maka cukup unitprice saja
                        $('#diskon_produk'+vale).val('0');
                        if (response.harga_modal) {
                            $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
                        }else {
                            $('#harga_modal'+vale).val('0');
                        }
                        if (response.tax) {
                            $.each(response.tax, function (index, item){
                                    $('#tax_produk'+vale).append( 
                                    $("<option></option>")
                                    .text(item.nilai+ "/" +item.nama)
                                    .val(item.id)
                                    );                                        
                            });
                        }
                        sumQtyAndUnit(vale);            
                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
            }

            $("#pelanggan_id").select2({
                tags: true
            });

            // Fungsi button cetak di klik
            $("#btn-cetak").on("click", function(e) {
                e.preventDefault();
                btnCetak()
            });

            function btnCetak() {
                if ($("#pelanggan_id").val() == "" || $('.produk_id').val() == "" || $('.produk_id').val() == undefined) {
                    swal({
                        icon: "warning",
                        text: "Pelanggan/Barang Masih Kosong"
                    });
                }else {
                    var form = $("#form-penjualan-penawaran");
                    form.attr('target','_blank');
                    form.attr('action','{{ route('akuntansi.edit-cetak-penawaran-penjualan') }}');
                    form.submit();
                }
            }

            // Fungsi button simpan tutup dan simpan lanjut di klik
            $("#btn-submit").on("click", function () {
                var form = $("#form-penjualan-penawaran");
                form.removeAttr('target');
                form.attr('action','{{ route($module_url->update,$item->id) }}');
                $("#btn-submit").on("submit", function () {
                    form.submit();
                });
            });

        </script>
        <script src="{{ asset('js/penawaran-penjualan.js') }}"></script>
        
        @endsection