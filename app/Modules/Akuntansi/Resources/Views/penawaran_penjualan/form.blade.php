<div class="form-group row">
  <div class="col-1 col-form-label">Order By</div>
  <div class="col-2">
		<?php //$from_session['no_pelanggan'] ?>
  {!! Form::text('no_pelanggan',null,['class' => 'form-control','id' => 'no_pelanggan','readonly'])!!}
  </div>
  <div class="col-6">
		<?php //$from_session['master_pelanggan_id'] ?>
  {!! Form::select('pelanggan_id',$pelanggan, null,['placeholder' => '-Pilih-','class' => 'form-control select2','id' => 'pelanggan_id', 'data-tags'=>'true'])!!}
  </div>
  <div class="col-3 form-inline">
  	<div class="form-check form-check-inline">
	  @if (isset($item))
	  {!! Form::checkbox('taxable', 1, $item->taxable == 1 ? true : false,['class' => 'form-check-input taxable','id' => 'group1','onchange' => 'calculate()']) !!}
	  @else
	  {!! Form::checkbox('taxable', 1, true,['class' => 'form-check-input taxable','id' => 'group1','onchange' => 'calculate()']) !!}
		@endif
				
	  <label class="form-check-label" for="group1">Cust. is Taxable</label>
	</div>
	<div class="form-check form-check-inline">
	  @if (isset($item))
	  {!! Form::checkbox('in_tax', 1, $item->in_tax == 1 ? true : false,['class' => 'form-check-input group2 in_tax', 'id' => 'inlineCheckbox2', 'onchange' => 'calculate()']) !!}
	  @else
	  {!! Form::checkbox('in_tax', 1, null,['class' => 'form-check-input group2 in_tax', 'id' => 'inlineCheckbox2', 'onchange' => 'calculate()']) !!}
	  @endif
	  <label class="form-check-label" for="inlineCheckbox2">Inclusive Tax</label>
	</div>
  </div>
</div>

<hr>
{{-- Pajak --}}
{{ Form::hidden("nilai_pajak", null,['id' => 'nilai_pajak_id','disabled']) }}
{{ Form::hidden("subTotal", null,['class' => 'sub-total-cetak']) }}
<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
			<label for="">Alamat</label>
			<?php //isset($from_session['alamat_pengiriman']) ? $from_session['alamat_pengiriman'] :null ?>
	    {!! Form::textarea('alamat_asal',null,['class' => 'form-control','id' => 'alamat_asal','readonly'])!!}
	  	</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
	    <label for="">Quote To</label>
					<?php //isset($from_session['alamat_pengiriman']) ? $from_session['alamat_pengiriman'] :null ?>
	    {!! Form::textarea('alamat_pengiriman',null,['class' => 'form-control','id' => 'alamat_pengiriman',''])!!}
	  	</div>
	</div>
	<div class="offset-sm-3 col-sm-3">
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label style="text-align: center; width: 100%;">Quote Number</label>
				    {!! Form::text('no_penawaran', !empty($item->no_penawaran) ? $item->no_penawaran : $idPrediction,['class' => 'form-control','id' => '','required' => 'required'])!!}
			  	</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
			    <label style="text-align: center; width: 100%;" >Quote Date</label>
			    @if (old('tanggal'))
			    {!! Form::text('tanggal', null,['class' => 'form-control tanggal_penawaran'])!!}
			    @elseif(isset($item))
			    {!! Form::text('tanggal', $item->tanggal_formatted,['class' => 'form-control tanggal_penawaran'])!!}
			    @else
			    {!! Form::text('tanggal', null,['class' => 'form-control tanggal'])!!}
			    @endif
			  	</div>
			</div>
		</div>
	</div>
</div>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-catatan-tab" data-toggle="pill" href="#pills-catatan" role="tab" aria-controls="pills-catatan" aria-selected="false">Catatan</a>
  </li>
</ul>
<div class="penawaran-sections" data-id="1">
<div class="duplicate-penawaran-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
  <div style="width: 2000px;">
  	<table width="100%" class="table">
			<thead class="thead-light" style="border-collapse: collapse;">
  			<tr>
  				<th width="13%">Item</th>
  				<th width="13%">Item Description</th>
  				<th>Qty</th>
  				<th>Satuan</th>
  				<th>Unit Price</th>
  				<th>Disc %</th>
  				<th>Tax</th>
  				<th>Amount</th>
  				<th width="10%">Dept</th>
  				<th width="10%">Proyek</th>
  				<th>Terproses</th>
  				<th>Ditutup</th>
  				<th></th>
  			</tr>
  		</thead>
  		<tbody class="purchasePenawaran">
  			@if(old('produk_id') !== null)  
				@foreach(old('produk_id') as $row => $produk_id )
		          @include('akuntansi::penawaran_penjualan/components/item_penawaran',[
		          	'row' => $row,
								'produk_id' => $produk_id,
								'no_produk' => old('no_produk')[$row],
		            'keterangan_produk' => old('keterangan_produk')[$row],
		            'qty_produk' => old('qty_produk')[$row],
		            'satuan_produk' => old('satuan_produk')[$row],
		            'unit_harga_produk' => old('unit_harga_produk')[$row],
		            'diskon_produk' => old('diskon_produk')[$row],
		            'kode_pajak_id' => old('kode_pajak_id')[$row],
		            'amount_produk' => old('amount_produk')[$row],
		            'dept_id' => old('dept_id')[$row],
		            'proyek_id' => old('proyek_id')[$row],
		            'terproses' => old('terproses')[$row],
		            'ditutup' => old('ditutup')[$row],
		            'harga_modal' => old('harga_modal')[$row],
		          ])
		        @endforeach      
		    @endif
  		</tbody>
  	</table>
  </div>
  	<a class="btn btn-info add-penawaran"><i class="fa fa-plus"> Tambah</i></a>
  </div>

  <div class="tab-pane fade" id="pills-catatan" role="tabpanel" aria-labelledby="pills-catatan-tab">
  	<div class="form-group">
    <label for="exampleFormControlTextarea1">Catatan</label>
    {!! Form::textarea('catatan',null,['class' => 'form-control','id' => 'catatan'])!!}
  </div>
  </div>

</div><!-- END PILLS -->
</div>
</div>

<script type="text/template" id="table_penawaran_section" data-id="">
  @include('akuntansi::penawaran_penjualan/components/item_penawaran')
</script>

<hr>
<div class="row">
	<div class="col-sm-4">
	 	<div class="form-group">
	    <label for="exampleFormControlTextarea1">Description</label>
	    {!! Form::textarea('keterangan',null,['class' => 'form-control','id' => 'keterangan'])!!}
	  </div>
	</div>
	<div class="offset-sm-4 col-sm-4">
		<div class="form-group row">
		<div class="offset-2 col-3 col-form-label text-right"><strong>Sub Total</strong></div>
		<div class="col-7 text-right col-form-label" id="subtotal"><strong>0</strong></div>
		</div>
		<div class="form-group row">
			<div class="col-2 col-form-label">Discount:</div>
			<div class="col-3 col-form-label">{!! Form::text('diskon',(empty($item) ? 0 : $item->diskon),['class' => 'form-control','id' => 'diskonTotal','onchange' => 'calculate()','required'])!!}</div>
			<div class="col-2 col-form-label text-center">% =</div>
			<div class="col-5 col-form-label">
				{!! Form::text('total_potongan_rupiah', (empty($item) ? 0 : $item->total_potongan_rupiah),['class' => 'form-control mask','id' => 'totalPotonganRupiah','onchange' => 'calculate()','required'])!!}
			</div>
		</div>
		<div class="form-group row">
			<div class="col-5 col-form-label"></div>
			<div class="col-5 col-form-label" id="taxNaration"></div>
			{!! Form::hidden('tax_cetak0', null,['id' => 'tax_cetak0'])!!}
			{!! Form::hidden('tax_cetak1', null,['id' => 'tax_cetak1'])!!}
			{!! Form::hidden('tax_cetak_label0', null,['id' => 'tax_cetak_label0'])!!}
			{!! Form::hidden('tax_cetak_label1', null,['id' => 'tax_cetak_label1'])!!}
		</div>
		<div class="form-group row" style="margin-top: 50px;">
		<div class="offset-2 col-3 col-form-label text-right">Freight</div>
		<div class="col-7">{!! Form::text('ongkir',(empty($item) ? 0 : $item->ongkir),['class' => 'form-control mask','id' => 'ongkir','onchange'=>'calculate()','required'])!!}</div>
		</div>
		<div class="form-group row">
		<div class="offset-1 col-4 col-form-label text-right"><strong>Total Invoice</strong></div>
		<div class="col-7 text-right col-form-label grandtotal"><strong>0</strong></div>
		{!! Form::hidden('grandtotal', null,['class' => 'grand_tot'])!!}
		{!! Form::hidden('total', null,['class' => 'grand_tot'])!!}
		</div>
	</div>
</div>

<div id="modalBarang" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title">Barang</h2>
      </div>
      <div class="modal-body">
				<div class="form-group row">
					{!! Form::label('kriteria', 'Kriteria', ['class' => 'col-1', 'style' => 'col-form-label']) !!}
					<div class="col col-3">
						{!! Form::select('kategori_barang', [ 'Semua', 'Kategori' ], 0, ['class' => 'form-control select2', 'id' => 'kategori_barang']) !!}
					</div>
					<div class="col col-6">
						{!! Form::select('searchByKategori', [], null, ['class' => 'form-control select2 searchByKategori', 'id' => 'searchByKategori', 'disabled']) !!}
					</div>
					<div class="col col-2">
						<a href="{{ route('akuntansi.barang.create') }}" target="_blank" class="btn btn-success form-control"><i class="fa fa-plus"></i> Barang</a>
					</div>
				</div>
				<div class="modal-sections" data-id="1">
						<div class="duplicate-modal-sections">
							<table class="table modal_table_barang" width="100%" id="modal-table-barang">
								<thead id="modal_thead" class="modal_thead" style="display:table; width:100%; table-layout:fixed;">	
								</thead>
								<tbody class="modal_tbody" style="display:table; width:100%; table-layout:fixed; display:block; overflow:auto; height: 190px;">
								</tbody>
							</table>
						</div>
				</div>
			</div>
			<div class="modal-footer modal_footer_gudang">
				<div class="form-group row mr-auto" style="width: 100%;">
					<div class="col-4">
						{!! Form::label('label_tampilkan_gudang', 'Tampilkan Qty di Gudang : ', ['class' => 'col-form-label']) !!}
					</div>
					<div class="col-8">
						{!! Form::select('modal_gudang', ['Semua Gudang'], null ,['class' => 'form-control select2', 'id' => 'modal_gudang']) !!}
						{!! Form::hidden('modal_gudang_temp', null ,['class' => 'form-control', 'id' => 'modal_gudang_temp']) !!}
					</div>
				</div>
			</div>
			<div class="modal-footer modal_footer_qty">
				<div class="form-group row mr-auto" style="width: 100%;">
					<div class="col-4">
						{!! Form::label('label_qty_gudang', 'Kuantitas : ', ['class' => 'col-form-label']) !!}
					</div>
					<div class="col-8">
						{!! Form::label('label_qty_gudang_footer', ' ', ['class' => 'col-form-label qty_gudang_footer', 'id' => 'qty_gudang_footer']) !!}
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default modal_btn_batal" onclick="barangBatal()" data-dismiss="modal">Close</button>
			</div>
    </div>
  </div>
</div>

<script type="text/template" id="modal_thead_barang" data-id="">
	<tr class="modal_thead_barang_tr">
		<td width="26%">
			{!! Form::text('modal_kode_barang[]', null, ['placeholder' => 'Cari Berdasarkan No Barang','class' => 'form-control modal_kode_barang']) !!}
		</td>
		<td width="3%"></td>
		<td width="55%">
			{!! Form::text('modal_nama_barang[]', null, ['placeholder' => 'Cari Berdasarkan Nama Barang', 'class' => 'form-control modal_nama_barang']) !!}
		</td>
	</tr>
</script>

<script type="text/template" id="modal_footer_barang" data-id="">
	<button type="button" class="btn btn-success modal_btn_simpan" onclick="simpan()">Simpan</button>
</script>