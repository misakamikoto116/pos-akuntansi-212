<div class="form-group row">
	{!! Form::label('no_pesanan','No. Pesanan',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
		{!! Form::text('batch_no',null,['class' => 'form-control','id' => 'batch_no'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('tanggal','Tanggal',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
		{!! Form::text('date',null,['class' => 'form-control tanggal','id' => 'date'])!!}
	</div>
</div>
<div class="form-group row">
	{!! Form::label('akun_pembiayaan_pesanan_id','Job Cost Account',['class' => 'col-2 col-form-label']) !!}
	<div class="col-3">
		{!! Form::text('akun', !empty($item) ? $item->akun->kode_akun : null,['class' => 'form-control','id' => 'akun','readonly'])!!}
	</div>
	<div class="col-7">
		{!! Form::select('akun_pembiayaan_pesanan_id',[''=>'Pilih Akun']+$akun,null,['class' => 'form-control select2','id' => 'akun_id','onchange'=>'setNamaAkunMain(this.value)'])!!}
	</div>
</div>

<div class="form-group row">
	{!! Form::label('keterangan','Keterangan',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
	    {!! Form::textarea('deskripsi',null,['class' => 'form-control','id' => 'deskripsi'])!!}
	</div>
</div>

<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang <strong id="total-amount-barang"></strong></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-beban-tab" data-toggle="pill" href="#pills-beban" role="tab" aria-controls="pills-beban" aria-selected="true">Beban <strong id="total-amount-beban"></strong></a>
  </li>
</ul>
{!! Form::hidden('grandtotal', null, ['id' => 'grandtotal']) !!}
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
    <div class="item-sections" data-id="1">
    <div class="duplicate-item-sections" style="width: 2000px;">
    <table width="100%" class="table duplicate-sections">
      <thead class="thead-light" style="border-collapse: collapse;">
        <tr>
          <th width="12%">Item</th>
          <th width="6%">Date</th>
          <th width="12%">Item Description</th>
          <th>Qty</th>
          <th>Item Unit</th>
          <th>Cost</th>
          <th>SN</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="purchaseItem">
        @if(old('produk_id') !== null)
            @foreach(old('produk_id') as $row => $produk_id)
              @include('akuntansi::pembiayaan_pesanan/components/item_pembiayaan',[
                'row'                   => $row, 
                'produk_id'             => $produk_id,
                'tanggal_produk'       	=> old('tanggal_produk')[$row],
                'no_produk'             => old('no_produk')[$row],
                'keterangan_produk'     => old('keterangan_produk')[$row],
                'qty_produk'           	=> old('qty_produk')[$row],
                'unit_produk'           => old('unit_produk')[$row],
                'harga_modal'           => old('harga_modal')[$row],
                'amount_produk'         => old('amount_produk')[$row],
                'sn'                    => old('sn')[$row],
              ])
            @endforeach     

        @elseif (!empty($item->detail))
          @foreach ($item->detail as $key => $detail)
              @include('akuntansi::pembiayaan_pesanan/components/item_detail', compact('key', 'detail'))
          @endforeach
        @endif
      </tbody>
    </table>
    <a class="btn btn-info add-item" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
  </div>
</div>

  <div class="tab-pane fade" id="pills-beban" role="tabpanel" aria-labelledby="pills-beban-tab" style="width: 100%; overflow: auto;">
    <div class="beban-sections" data-id="1">
    <div class="duplicate-beban-sections" style="width: 2000px;">
    <table width="100%" class="table duplicate-sections">
      <thead class="thead-light" style="border-collapse: collapse;">
        <tr>
          <th width="13%">Account No.</th>
          <th>Tanggal</th>
          <th width="17%">Nama Akun</th>
          <th>Notes</th>
          <th>Amount</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="purchaseBeban">
        @if (old('akun_beban_id') !== null)
          @foreach (old('akun_beban_id') as $row => $akun_beban_id)
            @include('akuntansi::pembiayaan_pesanan/components/item_beban_old',[
              'row'                => $row,
              'akun_beban_id'      => $akun_beban_id,
              'tanggal_beban'      => old('tanggal_beban')[$row] ?? null,
              'akun_beban_nama'    => old('akun_beban_nama')[$row] ?? null,
              'notes_beban'        => old('notes_beban')[$row] ?? null,
              'amount_beban'       => old('amount_beban')[$row] ?? null,
            ])
          @endforeach

        @elseif (!empty($item->beban))
        @foreach ($item->beban as $row =>  $dataBeban)
          @include('akuntansi::pembiayaan_pesanan/components/item_beban', compact('row', 'dataBeban'))
        @endforeach
      @endif
      </tbody>
    </table>
    <a class="btn btn-info add-beban" ><i class="fa fa-plus"> Tambah</i></a>
  </div>
  </div>
</div>

</div><!-- END PILLS -->

<script type="text/template" id="table_item_section" data-id=""/>
  <tr>
    <td>
      {!! Form::text('no_produk[]', null, ['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required', 'readonly'])!!}
      {!! Form::hidden('produk_id[]', null, ['class' => 'form-control produk_id','required'])!!}
    </td>
    <td>
		{!! Form::text('tanggal_produk[]',null,['class' => 'form-control required_date format-tanggal tanggal_produk','id' => 'tanggal_produk'])!!}
    </td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('unit_produk[]',null,['class' => 'form-control unit_produk','id' => '','required'])!!}</td>
    <td>
      {!! Form::hidden('harga_modal[]', null, ['class' => 'harga_modal']) !!}
      {!! Form::text('amount_produk[]',null,['class' => 'form-control amount_produk','id' => '','readonly'])!!}
    </td>
    <td>{!! Form::text('sn[]',null,['class' => 'form-control sn','id' => ''])!!}</td>
    <td><button href="" class="btn btn-danger remove-rincian-item" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>

<script type="text/template" id="table_beban_section" data-id=""/>
  <tr>
    <td>
		{!! Form::select('akun_beban_id[]',[],null,['placeholder' => '-Pilih-','class' => 'form-control akun_id akun_beban_id','required'])!!}
	</td><td>
		{!! Form::text('tanggal_beban[]',null,['class' => 'form-control required_date format-tanggal','id' => 'tanggal_beban'])!!}
	</td>
    <td>{!! Form::text('akun_beban_nama[]',null,['class' => 'form-control akun_beban_nama','id' => '',''])!!}</td>
    <td>{!! Form::text('notes_beban[]',null,['class' => 'form-control notes_beban','id' => '',''])!!}</td>
    <td>{!! Form::text('amount_beban[]',null,['class' => 'form-control amount_beban','id' => '','required'])!!}</td>
    <td><button href="" class="btn btn-danger remove-rincian-beban"  type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>

<hr>
<div class="row">
	<div class="offset-sm-12 col-sm-12">
		<div class="form-group row">
		<div class="offset-1 col-9 col-form-label text-right"><strong>Total Job Cost</strong></div>
    	<div class="col-2 text-right col-form-label grandtotal"><strong>0</strong></div>
		</div>
	</div>
</div>

@include('akuntansi::pesanan_modal/modal_barang')