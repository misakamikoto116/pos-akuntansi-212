<tr>
    <td>
    	{!! Form::hidden('akun_beban_temp', $dataBeban->akun_bbn_pembiayaan_pesanan_id ?? null,['id' => 'akun_beban_temp' . $row]) !!}
    	{!! Form::select('akun_beban_id[' .($row ?? 0). ']',[], $dataBeban->akun_bbn_pembiayaan_pesanan_id,['placeholder' => '-Pilih-','class' => 'form-control select2 akun_id akun_beban_id','required','id' => 'akun_beban_id'.($row ?? 0), 'onchange' => 'setNamaAkun(' .($row ?? 0). ')'])!!}
    </td>
    <td>
		{!! Form::text('tanggal_beban[' .($row ?? 0). ']', \Carbon\Carbon::parse($dataBeban->tanggal_beban)->format('d F Y') ?? null,['class' => 'form-control tanggal-edit format-tanggal','id' => 'tanggal_beban'.($row ?? 0)])!!}
	</td>
    <td>
    	{!! Form::text('akun_beban_nama[' .($row ?? 0). ']', $dataBeban->akun->nama_akun ?? null,['class' => 'form-control akun_beban_nama','id' => 'akun_beban_nama' .($row ?? 0),''])!!}
    </td>
    <td>
    	{!! Form::text('notes_beban[' .($row ?? 0). ']', $dataBeban->catatan ?? null,['class' => 'form-control notes_beban','id' => 'notes_beban'.($row ?? 0)])!!}
    </td>
    <td>
    	{!! Form::text('amount_beban[' .($row ?? 0). ']', $dataBeban->amount,['class' => 'form-control mask-harga amount_beban','id' => 'amount_beban'.($row ?? 0),'required'])!!}
    </td>
    <td>
    	<button href="" class="btn btn-danger remove-rincian-beban" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button>
    </td>
  </tr>