<tr>
    <td>{!! Form::select('akun_beban_id[' .($row ?? 0). ']',$akun, $akun_beban_id ?? null,['placeholder' => '-Pilih-','class' => 'form-control select2 akun_beban_id','required','id' => 'akun_beban_id'.($row ?? 0), 'onchange' => 'setNamaAkun(' .($row ?? 0). ')'])!!}</td>
    <td>
		{!! Form::text('tanggal_beban[' .($row ?? 0). ']',$tanggal_beban ?? null,['class' => 'form-control required_date format-tanggal','id' => 'tanggal_beban'.($row ?? 0)])!!}
	</td>
    <td>{!! Form::text('akun_beban_nama[' .($row ?? 0). ']',$akun_beban_nama ?? null,['class' => 'form-control akun_beban_nama','id' => 'akun_beban_nama' .($row ?? 0),''])!!}</td>
    <td>{!! Form::text('notes_beban[' .($row ?? 0). ']', $notes_beban ?? null,['class' => 'form-control notes_beban','id' => 'notes_beban'.($row ?? 0)])!!}</td>
    <td>{!! Form::text('amount_beban[' .($row ?? 0). ']', $amount_beban ?? null,['class' => 'form-control amount_beban','id' => 'amount_beban'.($row ?? 0),'required'])!!}</td>
    <td><button href="" class="btn btn-danger remove-rincian-beban" onclick="checkSesamaFungsi()" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>