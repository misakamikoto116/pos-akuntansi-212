@if (isset($row))
<tr>
    <td>
      {!! Form::hidden('produk_id_temp[]', $produk_id ?? null, ['id' => 'produk_id_temp' . $row]) !!}
      {!! Form::text('no_produk['.($row ?? 0).']', null, ['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required' ,'id' => 'no_produk'.$row,'onclick' => 'awas('.$row.')', 'readonly'])!!}
      {!! Form::hidden('produk_id['.($row ?? 0).']', $produk_id ?? null,['class' => 'form-control produk_id','required' ,'id' => 'produk_id'.$row])!!}
    </td>
    <td>
		{!! Form::text('tanggal_produk['.($row ?? 0).']',$tanggal_produk,['class' => 'form-control required_date format-tanggal','id' => 'tanggal_produk'.$row])!!}
    </td>
    <td>
      {!! Form::text('keterangan_produk['.($row ?? 0).']',$keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk'.$row,'required'])!!}
    </td>
    <td>{!! Form::text('qty_produk['.($row ?? 0).']',$qty_produk ?? 0,['class' => 'form-control qty_produk','id' => 'qty_produk'.$row,'required'])!!}</td>
    <td>
        {!! Form::text('unit_produk['.($row ?? 0).']',$unit_produk ?? 0,['class' => 'form-control unit_produk','id' => 'unit_produk'.$row,'required'])!!}
    </td>
    <td>
      {!! Form::hidden('harga_modal['.($row ?? 0).']', $harga_modal, ['class' => 'harga_modal','id' => 'harga_modal'.$row]) !!}
      {!! Form::text('amount_produk['.($row ?? 0).']',$amount_produk ?? 0,['class' => 'form-control amount_produk mask','id' => 'amount_produk'.$row,'readonly' => ''])!!}
    </td>
    <td>
        {!! Form::text('sn['.($row ?? 0).']',$sn ?? null,['class' => 'form-control sn','id' => 'sn'.$row])!!}
    </td>
    <td><button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
@else
<tr>
    <td>
      {!! Form::hidden('produk_id_temp[]', $produk_id ?? null, ['id' => 'produk_id_temp' . $row]) !!}
      {!! Form::text('no_produk[]', null, ['placeholder' => 'Cari Barang','class' => 'form-control no_produk','required' ,'id' => 'no_produk'.$row,'onclick' => 'awas('.$row.')', 'readonly'])!!}
      {!! Form::hidden('produk_id[]', $produk_id ?? null,['class' => 'form-control produk_id','required' ,'id' => 'produk_id'.$row])!!}
    </td>
    <td>
		{!! Form::text('tanggal_produk['.($row ?? 0).']',$tanggal_produk,['class' => 'form-control required_date format-tanggal','id' => 'tanggal_produk'])!!}
    </td>
    <td>
      {!! Form::text('keterangan_produk['.($row ?? 0).']',$keterangan_produk ?? null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk','required'])!!}
    </td>
    <td>{!! Form::text('qty_produk['.($row ?? 0).']',$qty_produk ?? 0,['class' => 'form-control qty_produk','id' => 'qty_produk','required'])!!}</td>
    <td>
        {!! Form::text('unit_produk['.($row ?? 0).']',$unit_produk ?? 0,['class' => 'form-control unit_produk','id' => 'unit_produk','required'])!!}
    </td>
    <td>
      {!! Form::hidden('harga_modal['.($row ?? 0).']', $harga_modal, ['class' => 'harga_modal','id' => 'harga_modal'.$row]) !!}
        {!! Form::text('amount_produk['.($row ?? 0).']',$amount_produk ?? 0,['class' => 'form-control amount_produk mask','id' => 'amount_produk','readonly' => ''])!!}
    </td>
    <td>
        {!! Form::text('sn['.($row ?? 0).']',$sn ?? null,['class' => 'form-control sn','id' => 'sn'])!!}
    </td>
    <td><button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
@endif
