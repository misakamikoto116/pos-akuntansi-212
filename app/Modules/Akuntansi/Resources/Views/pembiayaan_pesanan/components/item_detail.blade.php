<tr>
  <td>
      {!! Form::hidden('produk_id_temp[]', $detail->produk_id ?? null, ['id' => 'produk_id_temp' . $key]) !!}
      {!! Form::hidden('produk_id[]', null, ['class' => 'form-control produk_id','id' => 'produk_id' . $key, 'required'])!!}
      {!! Form::text('no_produk[]', null, ['placeholder' => 'Cari Barang','class' => 'form-control no_produk','id' => 'no_produk' . $key, 'onclick' => 'awas('. $key .')', 'readonly', 'required'])!!}
  </td>
  <td>
      {!! Form::text('tanggal_produk[]', \Carbon\Carbon::parse($detail->tanggal)->format('d F Y') ?? null,['class' => 'form-control tanggal-edit format-tanggal','id' => 'tanggal_produk'])!!}
  </td>
  <td>
      {!! Form::text('keterangan_produk[]', $detail->produk->keterangan ?? null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}
  </td>
  <td>
      {!! Form::text('qty_produk[]', $detail->qty ?? null,['class' => 'form-control qty_produk','id' => 'qty_produk' . $key,'onchange' => 'sumQtyAndUnit(' . $key . ')','required'])!!}
  </td>
  <td>
      {!! Form::text('unit_produk[]', $detail->unit ?? null,['class' => 'form-control unit_produk','id' => 'unit_produk' . $key,'required'])!!}
  </td>
  <td>
    {!! Form::hidden('harga_modal[]', $detail->harga_modal ?? null, ['class' => 'harga_modal','id' => 'harga_modal' . $key]) !!}
    {!! Form::text('amount_produk[]', $detail->cost ?? null,['class' => 'form-control amount_produk mask-harga','id' => 'amount_produk' . $key,'readonly'])!!}
  </td>
  <td>
      {!! Form::text('sn[]',null,['class' => 'form-control sn','id' => 'sn' . $key])!!}
  </td>
  <td>
      <button href="" class="btn btn-danger remove-rincian-item" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button>
  </td>
</tr>