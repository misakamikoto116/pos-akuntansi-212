<td class="td_id">{{ $cart->options->akun }}</td>
<td class="td_nama">{{ $cart->name }}</td>
<td><span class="duit">{{ $cart->options->debet }}</span></td>
<td><span class="duit">{{ $cart->options->kredit }}</span></td>
<td>{{ $cart->options->memo }}</td>
<td>{{ $cart->options->ledger }}</td>
<td>{{ $cart->options->department }}</td>
<td>{{ $cart->options->project }}</td>
<td>
    <a href="#" class="btn btn-warning edit_cart" data-id="{{ $cart->rowId }}" data-link=""><i class="fa fa-pencil"></i> </a>
    <a href="#" class="btn btn-danger remove_cart" data-id="{{ $cart->rowId }}" data-link="{{ route('akuntansi.jurnal_umum.delete_cart') }}"> <i class="fa fa-trash"></i></a>
</td>