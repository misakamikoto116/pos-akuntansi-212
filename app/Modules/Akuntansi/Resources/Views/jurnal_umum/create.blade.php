@extends( auth()->user()->hasPermissionTo('buat_bukti_jurnal_umum') ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
        <style type="text/css">
            .scale *{
                font-size: 11px;
            }
            .card{
                padding-top: 15px;
            }
            .card-header h5{
                font-size: 13px !important;
            }
            .form-control{
                height: auto;
            }
            .select2-container .select2-selection--single{
                height: 30px !important;
            }
            .select2-container .select2-selection--single .select2-selection__rendered{
                line-height: 27px !important;
            }
        </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">{{$title}}</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="{{ route('akuntansi.pembayaran.index') }}">Daftar Pembayaran</a></li>
                <li><a href="#">Tambah Pembayaran</a></li>
            </ol>

        </div>
    </div>

    <div class="scale">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">{{$title_document}}</h5>
                    </div>
                    <div class="card-body">
                        <div class="p-20">
                        {!! Form::open(['route' => $module_url->store, 'method' => 'POST', 'class' => 'form form-horizontal form-label-left myForm', 'id' => 'myForm']) !!}
                            @include($form)
                        </div>
                        <div class="col-sm-12">
                            {{-- <div class="menu-header"> --}}
                            {{-- <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                                <span class="btn-label"><i class="fa fa-plus"></i></span>
                                Tambah
                            </a> --}}
                            {{-- </div> --}}
                            <div class="title" style="margin-bottom: 10px; font-size: 12px; font-weight: 1000;">Merincikan Jurnal Umum
                            </div>
                            {{-- @if($items->isEmpty())
                            <div class="alert alert-warning"> Tidak ada data. </div>
                                @else --}}
                            <div class="form form-horizontal form-label-left bersih">
                                <table id="myTable" class="table table-bordered" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="17%">No. Akun</th>
                                            <th width="30%">Nama Akun</th>
                                            <th class="debet debet-label">Debet</th>
                                            <th class="kredit kredit-label">Kredit</th>
                                            <th>Memo</th>
                                            <th>Subsidiary Ledger</th>
                                            <th>Departement</th>
                                            <th>Project</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tbody_rincian">
                                        <tr>
                                            <td>{!! Form::text('akun_detail_id',null,['class' => 'form-control','id' => 'detail_akun_id','readonly', 'required' => ''])!!}</td>
                                            <td><select class="form-control nama_akun_ubah select2 rincian_pembayaran_required" name="nama_akun" id="detail_nama_akun">
                                                    <option value="">- Pilih Akun -</option>
                                                    @foreach ($listAkun as $akun)
                                                    <option value="{{$akun}}">{{$akun}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td class="debet debet-form">
                                                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                                    <div class="input-group-addon">Rp</div>
                                                    {!! Form::text('debet',null,['class' => 'form-control mask rincian_pembayaran_required detail_debet','id' => 'detail_debet'])!!}
                                                </div>
                                            </td>
                                            <td class="kredit kredit-form">
                                                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                                    <div class="input-group-addon">Rp</div>
                                                    {!! Form::text('kredit',null,['class' => 'form-control mask rincian_pembayaran_required detail_kredit','id' => 'detail_kredit'])!!}
                                                </div>
                                            </td>
                                            <td>{!! Form::text('memo',null,['class' => 'form-control rincian_pembayaran_required','id' => 'detail_memo', 'placeholder' => '-'])!!}</td>
                                            <td>{!! Form::text('subsidiary_ledger',null,['class' => 'form-control rincian_pembayaran_required','id' => 'detail_ledger', 'placeholder' => '-'])!!}</td>
                                            <td>{!! Form::text('departement',null,['class' => 'form-control rincian_pembayaran_required','id' => 'detail_departement', 'placeholder' => '-'])!!}</td>
                                            <td>{!! Form::text('project',null,['class' => 'form-control rincian_pembayaran_required','id' => 'detail_project', 'placeholder' => '-'])!!}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="float: left" class="block-hide">

                            </div>

                            <div class="hint" style="float: right; color: #618788;">
                                <small>Tekan "TAB" pada keyboard untuk menambah rincian</small>
                            </div>
                        </div>
                    </div>

                    <div class="card-header" style="z-index: 0;position: relative; border-radius: 8px; margin-top: 40px;">
                        <h5 class="title">Rincian Jurnal Umum</h5>
                    </div>

                    <div class="card-body">
                        {{-- @if($items->isEmpty())
                            <div class="alert alert-warning"> Tidak ada data. </div>
                        @else --}}
                        <table class="table" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No. Akun</th>
                                    <th>Nama Akun</th>
                                    <th>Debet</th>
                                    <th>Kredit</th>
                                    <th>Memo</th>
                                    <th>Subsidiary Ledger</th>
                                    <th>Departement</th>
                                    <th>Project</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="example">
                                @foreach (Cart::instance('jurmum' . auth()->user()->id)->content() as $row)
                                    <tr id="{{ $row->rowId }}">
                                        <td class="td_id">{{ $row->options->akun }}</td>
                                        <td class="td_nama">{{ $row->name }}</td>
                                        <td><span class="duit">{{ $row->options->debet }}</span></td>
                                        <td><span class="duit">{{ $row->options->kredit }}</span></td>
                                        <td>{{ $row->options->memo }}</td>
                                        <td>{{ $row->options->ledger }}</td>
                                        <td>{{ $row->options->department }}</td>
                                        <td>{{ $row->options->project }}</td>
                                        <td>
                                            <a href="#" class="btn btn-warning edit_cart" data-id="{{ $row->rowId }}" data-link=""><i class="fa fa-pencil"></i> </a>
                                            <a href="#" class="btn btn-danger remove_cart" data-id="{{ $row->rowId }}" data-link="{{ route('akuntansi.jurnal_umum.delete_cart') }}"> <i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <hr>
                        <div align="right" style="padding: 0 10px">
                            <strong>TOTAL :</strong>&emsp; <span id="total" class="duit">{{ $totalCart }}</span>
                        </div>
                        <hr>
                        <div class="submit">
                            {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default' ,'id' => 'btnSubmit']) !!}
                        </div>
                        {!! Form::button('<i class="fa fa-print"></i> Cetak',['type' => 'button','class' => 'btn btn-primary','style' => 'float : right', 'id' => 'btn-cetak']) !!}
                        {!! Form::close() !!}
                    </div>

                </div>
                <br>
            </div>
        </div>
    </div>


@endsection

@section('custom_js')

<script type="text/template" id="btnUpdate">
    <button type="button" class="btn btn-warning btn-md" id="btnBatal">Batal</button>
</script>

<script type="text/javascript">
    $('.mask').autoNumeric('init', {aPad: false, aForm:false});
    $('.duit').autoNumeric('init', {aPad: false, aSign: "Rp "});

    $("#btnSubmit").on("click", function () {
        var form = $("#myForm");
        form.removeAttr('target');
        form.attr('action','{{ route($module_url->store) }}');
        $("#btnSubmit").on("submit", function () {
            form.submit();
        });
    });

    $("#btn-cetak").on("click", function(e) {
        e.preventDefault();
        btnCetak();
    });

    function btnCetak() {
        var _token = $("input[name='_token']").val();
        var kredit = $("input[name='kredit']").autoNumeric('get');
        var debet = $("input[name='debet']").autoNumeric('get');
        var rincian = $("#example").find(".td_id");
        var type = 'jurmum{{auth()->user()->id}}';
        $.ajax({
            url: "{{ route('akuntansi.validate-jurnal_umum') }}",
            type:'POST',
            data: {_token:_token, debet:debet,kredit:kredit, type: type},
            success: function(data) {
                if (rincian.length > 0) {
                    if($.isEmptyObject(data.error)){
                        var form = $('.myForm');
                        form.attr('target','_blank');
                        form.attr('action','{{ route('akuntansi.cetak-jurnal-umum') }}');
                        form.submit();
                    }else{
                        swal({
                            icon: "error",
                            text: data.error
                        });
                    }
                }else {
                    swal({
                        icon: "error",
                        text: "Rincian Jurnal Masih Kosong"
                    });
                }
            }
        });
    }
</script>

<script type="text/javascript">

var createData = new Boolean(true);
var updateRowId = null;

var dk = null;
var vHide = null;

// ajax akun di cart
$( ".nama_akun_ubah" ).change(function() {
    var nama_akun   = $('#detail_nama_akun').val();
      $.ajax({
        type: "GET",
        url: "{{ route('akuntansi.get_akun_in_cart') }}",
        data: {id : nama_akun,
            _token: '{{ csrf_token() }}'},
        dataType: "json",
        success: function(response){
        $('#detail_akun_id').val(response.kode_akun)
        },
        fail: function(errMsg) {
            alert(errMsg);
        }
    });
});


// ajax session bawah
    $('#myForm').on('change',function(){
        var no_faktur_jurmum    = $('#no_faktur_jurmum').val();
        var tanggal_jurmum       = $('#tanggal_jurmum').val();
        var description_jurmum   = $('#description_jurmum').val();

$.ajax({
    type: "POST",
    url: "{{ route('akuntansi.ajax-jurnal_umum') }}",
    data: { description_jurmum: description_jurmum,
            no_faktur_jurmum: no_faktur_jurmum,
            tanggal_jurmum: tanggal_jurmum,
            _token: '{{ csrf_token() }}'
          },
    dataType: "json",
    success: function(data){
    },
    failure: function(errMsg) {
        alert(errMsg);
    }
});
});

// edit row
$('#example').on('click', '.edit_cart', function (e) {
    e.preventDefault();
    var id = $(this).closest('tr').attr('id');

    $.ajax({
        type: 'GET',
        url: '{{ route("akuntansi.jurnal_umum.get_cart") }}',
        dataType: 'JSON',
        data: {
            id:  id,
        },
        success: function (result) {
            editableCart();

            createData = false;
            updateRowId = id;

            $('#detail_akun_id').val(result.view.options.akun);
            $('#detail_nama_akun').val(result.view.name).trigger('change');

            var type = result.view.options.type == 'debet'
                        ? 'kredit'
                        : 'debet';
            var nominal = result.view.options.type == 'debet'
                        ? result.view.options.debet
                        : result.view.options.kredit;

            openUp(type, result.view.options.type, nominal);

            $('#detail_memo').val(result.view.options.memo);
            $('#detail_ledger').val(result.view.options.ledger);
            $('#detail_departement').val(result.view.options.department);
            $('#detail_project').val(result.view.options.project);

            $('.block-hide').fadeIn();
            $('.block-hide').html(
                                $('#btnUpdate').html()
                            );
            $('#detail_nama_akun').focus();
        },
        fail: function () {
            alert('Terjadi kesalahan saat edit');
        }
    });
});

// tampilkan tombol batal
$('.block-hide').on('click', '#btnBatal', function (e) {
    e.preventDefault();

    editableCart();

    $('.bersih').find("input[type=text]").val("");
    $('#detail_nama_akun').val("").trigger('change');

    updateRowId = null;
    createData = true;
    dk = null;
    vHide = null;

    $('.block-hide').fadeOut();
    $('.block-hide').empty();
});

// delete row
$('#example').on('click','.remove_cart', function (e) {
    e.preventDefault();

    if (updateRowId != null) {
        if ($(this).data('id') == updateRowId) {
            return false;
        }
    }
     swal({
          title: "Hapus Data",
          text: "Apakah Anda yakin?",
          icon: "error",
          buttons: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            var route = $(this).data('link');
            var row = $(this);
            $.ajax({
                type: "GET",
                url: route,
                data: {id: row.data('id')},
                success: function (result) {
                    console.log(result);
                    $('#total').autoNumeric('destroy').empty();
                    row.parents('tr').fadeOut(300,function() {
                        $(this).remove();
                    });

                    $('#total').autoNumeric('init', {aPad: false, aSign: "Rp "});
                    $('#total').autoNumeric('set', result.total);
                },
                fail: function(errMsg) {
                    alert(errMsg);
                }
            })
          } else {
            swal("Data batal di Dihapus", {
                icon: "warning",
            });
          }
        });
});

// ajax cart
$('#detail_project').on('keydown',function (event) {
    var keyCode = event.keyCode || event.which;
    if ( keyCode == 9 ) {
        event.preventDefault();
        var itemPembayaran = $('.tbody_rincian').find('tr');
        var sectionsPembayaran = $(itemPembayaran).find('.rincian_pembayaran_required');
        var nextToPembayaran;
        for(var i = 0; i < sectionsPembayaran.length; i++) {
            if (!sectionsPembayaran[i].validity.valid) {
                nextToPembayaran = false;
            }
        }
        if (nextToPembayaran == false) {
                swal({
                    icon: "error",
                    text: "Tolong Lengkapi Inputan Rincian Pembayaran Anda"
                });
        } else {

            var kredit_detail = $('#detail_kredit').length == 0
                                ? ''
                                : $('#detail_kredit').autoNumeric('get');

            var debet_detail = $('#detail_debet').length == 0
                                ? ''
                                : $('#detail_debet').autoNumeric('get');

            if (createData == true) {
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.jurnal_umum.post.add_cart') }}",
                    data: { akun_detail_id: $('#detail_akun_id').val(),
                            nama_detail_akun: $('#detail_nama_akun').val(),
                            debet_detail: debet_detail,
                            kredit_detail: kredit_detail,
                            memo_detail: $('#detail_memo').val(),
                            departement_detail: $('#detail_departement').val(),
                            ledger_detail: $('#detail_ledger').val(),
                            project_detail: $('#detail_project').val(),
                            type: dk,
                            _token: '{{ csrf_token() }}'
                          },
                    dataType: "json",
                    success: function(resp){
                        console.log(resp);
                        $('#total').autoNumeric('destroy').empty();
                        $('#example').append(resp.view);
                        //     $('#'+resp.rowId+'').find('.duit').text(resp.price).autoNumeric('init');
aPad: false,                         //     // var formatted_harga = $('.harga').text().replace(/\,/gi,'').replace('.00','');
                        //     var sum = 0;
                        //     $('.harga').each(function(){
                        //       sum += parseFloat($(this).text().replace(/\,/gi,'').replace('.00',''));
                        //     });
                        $('#total').autoNumeric('init', {aPad: false, aSign: "Rp "});
                        $('#total').autoNumeric('set', resp.total);

                        editableCart();

                        updateRowId = null;
                        createData = true;
                        dk = null;
                        vHide = null;

                        $('.bersih').find("input[type=text]").val("");
                        $('#detail_nama_akun').val("").trigger('change');

                        $('.duit').autoNumeric('init', {aPad: false, aSign: "Rp "});
                    },
                    fail: function() {
                        alert('Gagal');
                    }
                });
            } else {
                $.ajax({
                    type: "GET",
                    url: "{{ route('akuntansi.jurnal_umum.post.update_cart') }}",
                    data: { rowId: updateRowId,
                            akun_detail_id: $('#detail_akun_id').val(),
                            nama_detail_akun: $('#detail_nama_akun').val(),
                            debet_detail: debet_detail,
                            kredit_detail: kredit_detail,
                            memo_detail: $('#detail_memo').val(),
                            departement_detail: $('#detail_departement').val(),
                            ledger_detail: $('#detail_ledger').val(),
                            project_detail: $('#detail_project').val(),
                            type: dk,
                            _token: '{{ csrf_token() }}'
                          },
                    dataType: "json",
                    success: function (resp) {
                        if (resp.sukses) {
                            $('#total').autoNumeric('destroy').empty();
                            var eleId = $('tr#' + updateRowId);

                            eleId.html(resp.view);

                            eleId.attr('id', resp.data.rowId);

                            // var sum = 0;
                            // $(".harga").each(function(){
                            //   sum += parseFloat($(this).text().replace(/\,/gi,'').replace('.00',''));
                            // });
                            // $('#total').text(sum).autoNumeric('init', {aPad: false, aSign: "Rp "});

                            editableCart();

                            $('#total').autoNumeric('init', {aPad: false, aSign: "Rp "});
                            $('#total').autoNumeric('set', resp.total);

                            $('.bersih').find("input[type=text]").val("");
                            $('#detail_nama_akun').val("").trigger('change');

                            updateRowId = null;
                            createData = true;
                            dk = null;
                            vHide = null;

                            $('.block-hide').fadeOut();
                            $('.block-hide').empty();
                            $('.duit').autoNumeric('init', {aPad: false, aSign: "Rp "});
                        }
                    },
                    fail: function(errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        return false;
    }
});

// ajax get kode_akun
$( ".target" ).change(function() {
            var nama_akun   = $('#nama_akun').val();
            console.log(nama_akun);
              $.ajax({
                type: "GET",
                url: "{{ route('akuntansi.get-akun_id') }}",
                data: {id : nama_akun,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                $('#akun_id').val(response.kode_akun)
                },
                failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        });


//ajax validate nominal
$(document).ready(function() {
  $("#btnSubmit").click(function(e){
    e.preventDefault();
    var _token = $("input[name='_token']").val();
    var kredit = $("input[name='kredit']").autoNumeric('get');
    var debet = $("input[name='debet']").autoNumeric('get');
    var rincian = $("#example").find(".td_id");
    var type = 'jurmum{{auth()->user()->id}}';
    $.ajax({
        url: "{{ route('akuntansi.validate-jurnal_umum') }}",
        type:'POST',
        data: {_token:_token, debet:debet,kredit:kredit, type: type},
        success: function(data) {
            if (rincian.length > 0) {
                if($.isEmptyObject(data.error)){
                     var form = $('.myForm');
                    swal({
                      title: "Simpan Data",
                      text: data.success,
                      icon: "info",
                      buttons: true,
                    })
                    .then((willSave) => {
                      if (willSave) {
                        form.submit();
                      } else {
                        swal("Data batal di Simpan", {
                            icon: "success",
                        });
                      }
                    });
                }else{
                   swal({
                        icon: "error",
                        text: data.error
                        });
                }
            }else {
                swal({
                    icon: "error",
                    text: "Rincian Jurnal Masih Kosong"
                });
            }
        }
    });
  });


  $('#balancing').on('click', function (e) {
    e.preventDefault();
    $.ajax({
        url: '{{ route("akuntansi.ajax.getTotal") }}',
        type: 'GET',
        data: {
            type: 'pembayaran'
        },
        dataType: 'JSON',
        success: function (result) {
            if (result.success) {
                $('#nominal').autoNumeric('set', result.data.total);
            } else {
                swal({
                    icon: 'Error',
                    text: 'Error Saat Menyeimbangkan Jumlah'
                });
            }
        },
        error: function () {
            swal({
                icon: 'Error',
                text: 'Error Saat Menyeimbangkan Jumlah'
            });
        }
    });

  });
});

    $('.debet-form').on('keyup', '.detail_debet', function (e) {
        e.preventDefault();
        openKeyUp($(this), 'kredit', 'debet');
    });

    $('.kredit-form').on('keyup', '.detail_kredit', function (e) {
        e.preventDefault();
        openKeyUp($(this), 'debet', 'kredit');
    });

    function openKeyUp(event, valueHide, valueShow)
    {
        if ($(event).val().length > 0) {
            $('.' + valueHide).empty();
            $('.' + valueHide).hide();
            vHide = valueHide;
            dk = valueShow;
        } else {
            $('.' + valueHide + '-form').fadeIn().html(
                $('#form-' + valueHide).html()
            );

            $('.' + valueHide + '-label').fadeIn().html(
                $('#label-' + valueHide).html()
            );

            $('.' + valueHide + '-form').show();
            $('.mask').autoNumeric('init', {aPad: false, aForm:false});

            vHide = null;
            dk = null;
        }
    }

    function openUp(valueHide, valueShow, nominal)
    {
        dk = valueShow;
        vHide = valueHide;
        $('.' + valueHide).empty();
        $('.' + valueHide).hide();

        $('#detail_' + valueShow).val(nominal);
    }

    function editableCart()
    {
        if (vHide != null) {
            $('.' + vHide + '-form').fadeIn().html(
                $('#form-' + vHide).html()
            );

            $('.' + vHide + '-label').fadeIn().html(
                $('#label-' + vHide).html()
            );

            $('.' + vHide + '-form').show();
            $('.mask').autoNumeric('init', {aPad: false, aForm:false});
        }
    }

</script>

    <script type="text/template" id="label-kredit">
        Kredit
    </script>
    <script type="text/template" id="form-kredit">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <div class="input-group-addon">Rp</div>
                {!! Form::text('kredit',null,['class' => 'form-control mask rincian_pembayaran_required detail_kredit','id' => 'detail_kredit', 'required' => ''])!!}
            </div>
        </div>
    </script>

    <script type="text/template" id="label-debet">
        Debet
    </script>
    <script type="text/template" id="form-debet">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <div class="input-group-addon">Rp</div>
            {!! Form::text('debet',null,['class' => 'form-control mask rincian_pembayaran_required detail_debet','id' => 'detail_debet', 'required' => ''])!!}
        </div>
    </script>
@endsection