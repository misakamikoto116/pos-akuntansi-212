<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			{!! Form::label('no_faktur','No.Faktur',['class' => 'col-sm-2 col-form-label']) !!}
			<div class="col-sm-10" style="padding-left: 20px;">
				{!! Form::text('no_faktur_jurmum',session()->get('no_faktur_jurmum'),['class' => 'form-control','id' => 'no_faktur_jurmum'])!!}
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group row">
			{{-- {{ dd($item) }} --}}
			{!! Form::label('tanggal','Tanggal',['class' => 'col-sm-2 col-form-label']) !!}
			<div class="col-sm-10" style="padding-left: 20px;">
				@if (isset($item))
				{!! Form::text('tanggal_jurmum',$item->tanggal,['class' => 'form-control tanggal_jurnal'])!!}
				@else
				{!! Form::text('tanggal_jurmum',session()->get('tanggal_jurmum'),['class' => 'form-control tanggal','id' => 'tanggal_jurmum'])!!}
				@endif
			</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="form-group row">
			{!! Form::label('keterangan','Description',['class' => 'col-sm-1 col-form-label']) !!}
			<div class="col-sm-11" style="padding-left: 20px;">
				{!! Form::textarea('description_jurmum',session()->get('description_jurmum'),['class' => 'form-control','id' => 'description_jurmum','rows' => '3'])!!}
			</div>
		</div>
	</div>

</div>


