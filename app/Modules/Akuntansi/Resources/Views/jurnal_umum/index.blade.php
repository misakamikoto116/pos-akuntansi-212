@extends( $permission['daftar'] ? 'chichi_theme.layout.app' : 'exception.error')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
    <style>
        th {
            cursor: pointer;
        }
</style>
@endsection

@section('content')
<!-- Page-Title -->
    <div class="row">
        @include('chichi_theme.layout.filter')
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">{{$title}}</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Jurnal Umum</a></li>
            </ol>
        </div>
    </div>
<!-- END Page-Title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">{{$title_document}}</h5>
                    @can($permission['buat'])
                        <div class="menu-header">
                            <a href="{{ route($module_url->create) }}" class="btn btn-default btn-rounded waves-effect waves-light">
                                <span class="btn-label"><i class="fa fa-plus"></i></span>
                                Tambah
                            </a>
                        </div>
                    @endcan
                </div>
                <div class="card-body">
                    @if($items->isEmpty())
                        <div class="alert alert-warning"> Tidak ada data. </div>
                    @else
                    <table class="table" id="tableJurnal">
                        <thead>
                            <tr>
                                <th onclick="sortTable(0)">No</th>
                                <th onclick="sortTable(1)">No. Faktur</th>
                                <th onclick="sortTable(2)">Tanggal</th>
                                <th onclick="sortTable(3)">Keterangan</th>
                                <th onclick="sortTable(4)">Nominal</th>
                                @include('chichi_theme.layout.created_updated_deleted_th')
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $key => $item)
                                <tr>
                                    <td>{{$key + $items->firstItem()}}</td>
                                    <td>{{$item->no_faktur}}</td>
                                    <td>{{ \Helpers\IndonesiaDate::indonesiaDate($item->tanggal) }}</td>
                                    <td>{{$item->description}}</td>
                                    <td class='mask_index'>{{( $item->nominalJurnal() )}}</td>
                                    @include('chichi_theme.layout.created_updated_deleted_td', compact($item))
                                    <td>
                                        @if($permission['buat'] || $permission['ubah'] || $permission['hapus'])
                                            {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                                    <div class="dropdown-menu">
                                                        @can($permission['buat'])
                                                            <a class="dropdown-item jurnal" href="#">
                                                                <i class="fa fa-table"></i>&emsp;Jurnal
                                                            </a>
                                                        @endcan
                                                        @can($permission['ubah'])
                                                            <a class="dropdown-item hey" href="{{ route($module_url->edit, $item->id) }}"></a>
                                                        @endcan
                                                        @can($permission['hapus'])
                                                            <a class="dropdown-item deleteBtn" href="#">
                                                                <i class="fa fa-trash"></i>&emsp;Delete
                                                            </a>
                                                        @endcan
                                                    </div>
                                                </div>
                                            {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                    <div class="pull-right">
                        {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script type="text/javascript">
        function sortTable(n){
            var table, rows, triggerSwitch, i, x, y, aksiSwitch, sortBy, tandaSwitch = 0;

            table = $('#tableJurnal')[0];
            triggerSwitch = true;
            sortBy = "asc";
            while (triggerSwitch) {
                triggerSwitch = false;
                rows = table.rows;
                console.log('s'); 
                console.log(table);
                
                for (i = 1; i < (rows.length - 1); i++) {
                    aksiSwitch = false;
                    x = rows[i].getElementsByTagName("TD")[n];
                    y = rows[i + 1].getElementsByTagName("TD")[n];
                    x = $(x).html();
                    y = $(y).html();
                    if (sortBy == "asc") {
                        if (n == 4) {
                            if (Number(x) > Number(y)) {
                                aksiSwitch= true;
                                break;
                            }
                        }else{
                            if (x.toLowerCase() > y.toLowerCase()) {
                                aksiSwitch= true;
                                break;
                            }
                        }
                    } else if (sortBy == "desc") {
                        if (n == 4) {
                            if (Number(x) < Number(y)) {
                                aksiSwitch= true;
                                break;
                            }
                        }else{
                            if (x.toLowerCase() < y.toLowerCase()) {
                                aksiSwitch= true;
                                break;
                            }
                        }
                    }
                }
                if (aksiSwitch) {
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    triggerSwitch = true;
                    tandaSwitch ++;      
                } else {
                    if (tandaSwitch == 0 && sortBy == "asc") {
                        sortBy = "desc";
                        triggerSwitch = true;
                    }
                }
            }
        }

        $('.mask_index').autoNumeric('init',{aPad: false});
        @if(!$items->isEmpty())
            @foreach ($items as $item)
                @if( $item->status == 2 )
                    $('.hey').addClass('btn-info');
                    $('.hey').html('<i class="fa fa-eye"></i>&emsp;Lihat');
                    $('.deleteBtn').hide();
                @else
                    $('.hey').addClass('edit');
                    $('.hey').html('<i class="fa fa-pencil"></i>&emsp;Edit');
                @endif
            @endforeach
        @endif
    </script>
@endsection