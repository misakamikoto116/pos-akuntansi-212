@php
	$total_saldo = 0; 
@endphp
@foreach ($semua_transaksi as $key => $data)
	<tr class="tr_penerimaan" id="tr_penerimaan">
		@if($data->status == "1")
		@php
			$total_saldo = $total_saldo + $data->nominal;
		@endphp
		<td>{{ \Helpers\IndonesiaDate::indonesiaDate($data->tanggal) }}</td>
		<td>{{ $data->item->no_faktur }}</td>
		<td>{{ $data->item->no_cek or ''}}</td>
		<td>{{ $data->item->keterangan or '' }}</td>
		<td style="text-align: right;"><span class="mask_duit nominal_penerimaan">{{ $data->nominal  or ''}}</span></td>
		<td><span class="mask_duit nominal_pembayaran"></span></td>
		<td style="text-align: right;"><span class="mask_duit total_saldo">{{ $total_saldo }}</span></td>
		@elseif($data->status == "0")
		@php
			$total_saldo = $total_saldo - $data->nominal;
		@endphp
		<td>{{ \Helpers\IndonesiaDate::indonesiaDate($data->tanggal) }}</td>
		<td>{{ $data->item->no_faktur or '' }}</td>
		<td>{{ $data->item->no_cek or ''}}</td>
		<td>{{ $data->item->keterangan or '' }}</td>
		<td><span class="mask_duit nominal_penerimaan"></span></td>
		<td style="text-align: right;"><span class='mask_duit nominal_pembayaran'>{{ $data->nominal  or ''}}</span></td>
		<td style="text-align: right;"><span class="mask_duit total_saldo">{{ $total_saldo }}</span></td>
		@elseif ($data->status == null)
		@php
			$total_saldo = $total_saldo + $data->nominal;
		@endphp
		<td colspan="6"></td>
		<td><span class="mask_duit total_saldo">{{ $total_saldo }}</span></td>
		@endif
		<td></td>
	</tr>
@endforeach