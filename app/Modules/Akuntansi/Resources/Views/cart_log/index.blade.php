@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                @include('chichi_theme.layout.filter')
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Cart Log</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Cart Log</a></li>
                    </ol>

                </div>
            </div>   
<!-- END Page-Title -->   

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Cart Log</h5>
            <div class="menu-header">
            </div> 
            </div>
            <div class="card-body">
                @if($items->isEmpty())
                    <div class="alert alert-warning"> Tidak ada data. </div>
                @else
                <table class="table table-bordered" id="tblProducts">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Petugas</th>
                            <th>Aksi</th>
                            <th>Barang</th>
                            <th>Qty</th>
                            <th>Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 0; @endphp
                        @foreach($items as $key => $item)
                        @if($key > 0)
                            @if($items[$key - 1]->no_transaksi != $item->no_transaksi)
                                @php $i++; @endphp    
                            @endif
                            
                            @php $mod = fmod($i, 2) @endphp
                            
                            @if($mod == 1)
                                <tr style="background:#ecf0f1">
                            @else 
                                <tr style="background:#fff">
                            @endif
                        @endif
                                <td>{{ $item->no_transaksi }}</td>
                                <td>{{ $item->created_at }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>{!! $item->status_formatted !!}</td>
                                <td>{{ $item->produk->keterangan }}</td>
                                <td>{{ $item->qty }}</td>
                                <td>{{ $item->harga_formatted }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
                <div class="pull-right">
                    {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                </div>
            </div>
        </div>
    </div>
</div>  

@endsection

@section('custom_js')
<script type="text/javascript">
        
</script>
@endsection