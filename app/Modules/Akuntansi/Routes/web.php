<?php

Route::group(['prefix' => 'akuntansi', 'as' => 'akuntansi.'], function () {
    // Route Tampilan saja
    Route::get('/form', function () {
        return view('chichi_theme.dashboard.home.formexample');
    });
    // Route::get('/penerimaan-penjualan', function () {
    //     return view ('akuntansi::penerimaan_penjualan.create');
    // });
    // Route::get('/retur-penjualan', function () {
    //     return view ('akuntansi::retur_penjualan.create');
    // });
    // Route::get('/penerimaan-barang', function () {
    //     return view ('akuntansi::pembelian.penerimaan.create');
    // });
    // Route::get('/faktur-pembelian', function () {
    //     return view ('akuntansi::pembelian.faktur.create');
    // });
    // Route::get('/faktur-pembelian', function () {
    //     return view ('akuntansi::pembelian.faktur.create');
    // });

    // Akun => Buku Bank
    Route::get('akun/buku-bank', 'AkunController@getBukuBank')->name('akun-buku-bank');

    // Laporan Pembayaran dan Penerimaan
    Route::get('pembayaran/laporan/', 'PembayaranController@getLaporanPembayaran');
    Route::get('penerimaan/laporan/', 'PenerimaanController@getLaporanPenerimaan');

    /* Informasi History Buku Besar Pelanggan */
    Route::get('informasi-pelanggan/history-buku-besar/{type}/{id}', 'InformasiPelangganController@getHistoryBukuBesar')->name('pelanggan.history-buku-besar');

    /* Informasi History Buku Besar Pemasok */
    Route::get('informasi-pemasok/history-buku-besar/{type}/{id}', 'InformasiPemasokController@getHistoryBukuBesar')->name('pemasok.history-buku-besar');


    Route::resource('serial-number', 'SerialNumberController');
    Route::resource('penyesuaian-persediaan', 'PenyesuaianPersediaanController');
    Route::resource('barang-pergudang', 'BarangPergudangController');
    Route::resource('pemindahan-barang', 'PemindahanBarangController');
    Route::resource('penyesuaian-harga-jual', 'PenyesuaianHargaJualController');
    Route::resource('daftar-aktiva-tetap', 'DaftarAktivaTetapController');
    Route::resource('retur-pembelian', 'ReturPembelianController');
    Route::resource('preferensi', 'PreferensiController', ['only' => ['index', 'update']]);
    Route::resource('data-karyawan', 'DataKaryawanController');
    Route::resource('group-barang', 'GroupBarangController');


    Route::resource('preferensi-pos', 'PreferensiPosController');
    Route::post('preferensi-pos/store', 'PreferensiPosController@storePos')->name('preferensi.pos.store');
    Route::post('preferensi-pos/store/modal', 'PreferensiPosController@storeModal')->name('preferensi.pos.store.modal');
    Route::post('preferensi-pos/pajak/store', 'PreferensiPosController@storePajak')->name('preferensi.pos.store.pajak');
    Route::get('preferensi-pos/ajax/get/akun', 'PreferensiPosController@getAkun')->name('preferensi.pos.akun');


    // Route Core
    Route::get('', 'HomeController@index')->name('home');
    Route::get('harga-jual-nol', 'HomeController@indexHargaJualNol')->name('home.harga-jual-nol');
    Route::get('minimal-stock', 'HomeController@indexMinimalStock')->name('home.minimal-stock');
    Route::get('harga-modal', 'HomeController@indexHargaModal')->name('home.harga-modal');
    Route::get('kada-luarsa', 'HomeController@indexKadaLuarsa')->name('home.kada-luarsa');
    Route::get('edit-password', 'DataPerusahaanController@showPassword');
    Route::get('test', 'AkunController@test');
    Route::post('edit-password/save', 'DataPerusahaanController@editPassword')->name('edit.password');
    Route::resource('tipe-akun', 'TipeAkunController');
    Route::resource('mata-uang', 'MataUangController');
    Route::resource('gudang', 'GudangController');
    Route::resource('akun', 'AkunController');
    Route::resource('barang', 'BarangController');
    Route::resource('kategori-produk', 'KategoriProdukController');
    Route::resource('syarat-pembayaran', 'SyaratPembayaranController');
    Route::resource('nomor-pajak', 'NomorPajakController');
    Route::resource('kode-pajak', 'KodePajakController');
    Route::resource('jasa-pengiriman', 'JasaPengirimanController');
    Route::resource('tipe-aktiva-tetap-pajak', 'TipeAktivaTetapPajakController');
    Route::resource('tipe-aktiva-tetap', 'TipeAktivaTetapController');
    Route::resource('tipe-pelanggan', 'TipePelangganController');
    Route::resource('pembayaran', 'PembayaranController');
    Route::resource('faktur-penjualan', 'FakturPenjualanController');
    Route::resource('penawaran-penjualan', 'PenawaranPenjualanController');
    Route::resource('pesanan-penjualan', 'PesananPenjualanController');
    Route::resource('pengiriman-penjualan', 'PengirimanPenjualanController');
    Route::resource('penerimaan-penjualan', 'PenerimaanPenjualanController');
    Route::resource('retur-penjualan', 'ReturPenjualanController');
    Route::resource('penerimaan', 'PenerimaanController');
    Route::resource('jurnal-umum', 'JurnalUmumController');
    Route::resource('identitas', 'IdentitasController');
    Route::resource('informasi-pelanggan', 'InformasiPelangganController');
    Route::resource('informasi-pemasok', 'InformasiPemasokController');
    Route::resource('uang-muka-pelanggan', 'UangMukaPelangganController');
    Route::resource('uang-muka-pemasok', 'UangMukaPemasokController');
    Route::resource('rekonsiliasi-bank', 'RekonsiliasiBankController');
    Route::resource('type-edc', 'TypeEdcController');

    // Pembelian
    Route::resource('permintaan-pembelian', 'PermintaanPembelianController');
    Route::resource('pesanan-pembelian', 'PesananPembelianController');
    Route::resource('penerimaan-barang', 'PenerimaanBarangController');
    Route::resource('faktur-pembelian', 'FakturPembelianController');
    Route::resource('pembayaran-pembelian', 'PembayaranPembelianController');

    Route::get('detail/pelanggan/{id}', 'InformasiPelangganController@detail')->name('detail.pelanggan');
    Route::get('delete/detail-pelanggan/{id}', 'InformasiPelangganController@delete')->name('delete.detail_pelanggan');
    Route::get('edit/detail-pelanggan/{id}', 'InformasiPelangganController@edit')->name('edit.detail_pelanggan');

    Route::get('pembayaran/jurnal/{id}', 'PembayaranController@jurnal')->name('show.jurnal_pembayaran');
    Route::get('penerimaan/jurnal/{id}', 'PenerimaanController@jurnal')->name('show.jurnal_penerimaan');
    
    Route::get('buku-bank', 'BukuBankController@index')->name('buku_bank.show');

    // Rekonsiliasi
    // Route::get('rekonsiliasi-bank', 'RekonsiliasiBank1Controller@index')->name('rekonsiliasi_bank.show');
    // Route::post('rekonsiliasi-save', 'RekonsiliasiBankController@save')->name('save.rekonsiliasi_bank');

    // Penyelesaian Pesanan
    Route::resource('pembiayaan-pesanan', 'PembiayaanPesananController');
    Route::resource('penyelesaian-pesanan', 'PenyelesaianPesananController');
    Route::resource('promosi', 'PromosiController');
    // Buku Besar
    Route::resource('history-buku-besar', 'HistoryBukuBesarController');
    Route::resource('saldo-akun', 'SaldoAkunController');

    Route::prefix('ajax')->group(function () {
        Route::get('get-session-master-nomor-serial', 'AjaxController@setMasterDetailSession')->name('get-session-master-nomor-serial');
        Route::get('get-alamat-gudang', 'AjaxController@getAlamatGudang')->name('get-alamat-gudang');
        Route::get('get-tanggal-transaksi', 'AjaxController@getTanggalTransaksi')->name('get-tanggal-transaksi');

        /* Get List Terms and Ships */
        Route::get('get-list-terms', 'AjaxController@getListTerms')->name('get-list-terms');
        Route::get('get-list-ships', 'AjaxController@getListShips')->name('get-list-ships');

        /*Penjualan*/
        Route::get('get-alamat-pelanggan', 'AjaxController@getAlamatPelanggan')->name('get-alamat-pelanggan');
        Route::get('get-alamat-pelanggan-kirim-ke', 'AjaxController@getAlamatPelangganKirimKe')->name('get-alamat-pelanggan-kirim-ke');
        Route::get('get-session-master-penawaran', 'AjaxController@setMasterDetailSession')->name('get-session-master-penawaran');
        // Route::get('get-detail-pesanan-penjualan','AjaxController@getDetailPesananPenjualan')->name('get-detail-pesanan-penjualan');
        // Route::get('get-detail-pengiriman-penjualan','AjaxController@getDetailPengirimanPenjualan')->name('get-detail-pengiriman-penjualan');
        // Route::get('get-produk-pengiriman-penjualan','AjaxController@getProdukPengirimanPenjualan')->name('get-produk-pengiriman-penjualan');

        /*Pembelian*/
        Route::get('get-alamat-pemasok', 'AjaxController@getAlamatPemasok')->name('get-alamat-pemasok');
        Route::get('get-produk', 'AjaxController@getProduk')->name('get-produk');
        // Route::get('get-produk-pesanan-penjualan','AjaxController@getProdukPesananPenjualan')->name('get-produk-pesanan-penjualan');

        /*aktiva*/
        Route::get('get-aktiva-tetap', 'AjaxController@getAktiva')->name('get-aktiva-tetap');

        /*akun*/
        Route::get('get-akun-tipe', 'AjaxController@getAkunTipe')->name('get-akun-tipe');
        Route::get('get-akun-kode', 'AjaxController@getAkunKode')->name('get-akun-kode');

        /*pembayaran*/
        Route::post('ajax-master-pembayaran', 'AjaxController@master')->name('ajax-pembayaran');
        Route::get('get-akun_id', 'AjaxController@getAkun')->name('get-akun_id');
        Route::get('get-akun-in-cart', 'AjaxController@getAkunInCart')->name('get_akun_in_cart');
        Route::get('/add-cart', 'PembayaranController@sessionApi')->name('pembayaran.post.add_cart');
        Route::get('/delete-cart', 'PembayaranController@sessionDelete')->name('pembayaran.delete_cart');
        Route::post('validate-pembayaran', 'AjaxController@validateNominal')->name('validate-pembayaran');
        Route::get('/edit-cart', 'PembayaranController@sessionUpdate')->name('pembayaran.post.update_cart');

        /*penerimaan*/
        Route::post('ajax-master-penerimaan', 'AjaxController@master')->name('ajax-penerimaan');
        Route::get('get-akun_id_penerimaan', 'AjaxController@getAkun')->name('get-akun_id');
        Route::get('get-akun-in-cart_penerimaan', 'AjaxController@getAkunInCart')->name('get_akun_in_cart');
        Route::post('/add-cart_penerimaan', 'PenerimaanController@sessionApi')->name('post.add_cart');
        Route::get('/delete-cart_penerimaan', 'PenerimaanController@sessionDelete')->name('delete_cart');
        Route::get('/edit-cart_penerimaan', 'PenerimaanController@sessionUpdate')->name('post.update_cart_penerimaan');

        /*jurnal umum*/
        Route::post('ajax-master-jurnal_umum', 'AjaxController@masterJurnalUmum')->name('ajax-jurnal_umum');
        Route::get('jurnal_umum/add-cart', 'JurnalUmumController@sessionApi')->name('jurnal_umum.post.add_cart');
        Route::get('jurnal_umum/delete-cart', 'JurnalUmumController@sessionDelete')->name('jurnal_umum.delete_cart');
        Route::post('validate-jurnal_umum', 'AjaxController@validateNominalJurnalUmum')->name('validate-jurnal_umum');
        Route::get('jurnal_umum/edit-cart', 'JurnalUmumController@sessionUpdate')->name('jurnal_umum.post.update_cart');
        Route::get('jurnal_umum/get-cart', 'JurnalUmumController@getSession')->name('jurnal_umum.get_cart');

        /*buku bank*/
        Route::get('buku-bank-value', 'BukuBankController@getBukuBank')->name('get_buku_bank_value');

        /*rekonsiliasi bank*/
        // Route::get('get-akun_id', 'RekonsiliasiBankController@getAkun')->name('get.akun_id.rekonsiliasi_bank');
        // Route::get('rekonsiliasi-bank-value', 'RekonsiliasiBankController@getRekonsiliasiBank')->name('get.rekonsiliasi_bank.value');
        // Route::get('rekonsiliasi-bank-data', 'RekonsiliasiBankController@getRekonsiliasiBankData')->name('get.rekonsiliasi-bank.data');
        // Route::get('rekonsiliasi-status', 'RekonsiliasiBankController@getCheckedStatus')->name('get.rekonsiliasi-status');

        Route::get('/getTotal', 'AjaxController@getTotal')->name('ajax.getTotal');
        Route::post('validatepassword', 'DataPerusahaanController@validatePasslama')->name('validate.passlama');

        /* Barang */
        Route::get('get-detail-barang', 'AjaxController@getDetailBarang')->name('get-detail-barang');
        Route::get('get-produk-by-val', 'AjaxController@getProdukByVal')->name('get-produk-by-val');
        Route::get('get-returned-item', 'AjaxController@getReturnedItem')->name('get-returned-item');
        Route::get('get-nama-produk', 'AjaxController@getDaftarNamaProduk')->name('get-nama-produk');
        Route::get('get-id-produk', 'AjaxController@getDaftarIdProduk')->name('get-id-produk');
        Route::get('get-nama-akun', 'AjaxController@getDaftarNamaAkun')->name('get-nama-akun');
        Route::get('get-id-akun', 'AjaxController@getDaftarIdAkun')->name('get-id-akun');
        Route::get('get-nama-produk-detail', 'AjaxController@getDaftarNamaProdukDetail')->name('get-nama-produk-detail');

        /* List Transaksi dari Pelanggan atau Pemasok */
        Route::POST('get-list-history-price-item', 'AjaxController@getListHistoryPriceItem')->name('get-list-history-price-item');

        /* List Kategori Barang */
        Route::get('get-all-kategori-produk', 'AjaxController@getListKategoriProduk')->name('get-all-kategori-produk');

        /* Informasi Pelanggan */
        Route::get('get-detail-informasi-pelanggan', 'AjaxController@getDetailPelanggan')->name('get-detail-informasi-pelanggan');

        /*	Cek DP */
        Route::get('check-dp', 'AjaxController@checkDp')->name('check-dp');
        Route::get('check-dp-pembelian', 'AjaxController@checkDpPembelian')->name('check-dp-pembelian');

        /*  Cek Termin */
        Route::get('check-termin', 'AjaxController@checkTermin')->name('check-termin');

        Route::get('get-faktur-pembelian', 'AjaxController@getFakturPembelian')->name('get-faktur-pembelian');

        Route::get('get-sort-pajak', 'AjaxController@sortPajak')->name('get-sort-pajak');
        Route::get('get-detail-pemasok', 'AjaxController@getDetailPemasok')->name('get-detail-pemasok');
        Route::get('get-edit-informasi-pelanggan', 'AjaxController@getEditInformasiPelanggan')->name('get-edit-informasi-pelanggan');

        /*  Cek Stock di gudang */
        Route::get('get-gudang-stock', 'AjaxController@cekGudangStock')->name('get-gudang-stock');
        Route::get('get-qty-gudang-stock', 'AjaxController@getSumQtyGudangProdukDetail')->name('get-qty-gudang-stock');
        Route::get('get-list-gudang', 'AjaxController@getListGudang')->name('get-list-gudang');

        /* Cek Retur */
        Route::get('get-retur', 'AjaxController@cekRetur')->name('cek-retur');

        // Rekonsiliasi Bank Data
        Route::get('get-akun_id', 'AjaxController@getAkunRekonsiliasi')->name('get.akun_id.rekonsiliasi_bank');
        Route::get('rekonsiliasi-bank-value', 'AjaxController@getRekonsiliasiBank')->name('get.rekonsiliasi_bank.value');
        Route::get('rekonsiliasi-bank-data', 'AjaxController@getRekonsiliasiBankData')->name('get.rekonsiliasi-bank.data');

        Route::get('get-pembiayaan', 'AjaxController@getPembiayaan')->name('get-pembiayaan');

        /* Cek Retur */
        Route::get('get-cek-preferensi','AjaxController@cekPreferensiPenyesuaian')->name('get.cek.preferensi');

    });

    /* Sticker */
    Route::get('upload/sticker', 'StikerController@index')->name('upload.stiker.index');
    Route::post('upload/sticker', 'StikerController@upload')->name('upload.stiker.upload');
    Route::post('upload/sticker/{id}/status', 'StikerController@status')->name('upload.stiker.status');
    Route::post('upload/sticker/{id}/delete', 'StikerController@delete')->name('upload.stiker.delete');

    /* Pemberitahuan */
    Route::get('pemberitahuan', 'PemberitahuanController@index')->name('pemberitahuan.kirim');
    Route::post('pemberitahuan', 'PemberitahuanController@kirimPesanPost')->name('pemberitahuan.kirim.post');

    /* iklan */
    Route::get('display-iklan', 'DisplayIklanController@index')->name('iklan-manage.get');
    Route::get('display-iklan/create', 'DisplayIklanController@add')->name('iklan-manage.add.get');
    Route::post('display-iklan/create', 'DisplayIklanController@post')->name('iklan-manage.add.post');
    Route::get('display-iklan/update/{iklan_id}', 'DisplayIklanController@update')->name('iklan-manage.edit.get');
    Route::post('display-iklan/update/{iklan_id}', 'DisplayIklanController@postUpdate')->name('iklan-manage.edit.post');
    /*ajax*/
    Route::delete('display-iklan/delete', 'DisplayIklanController@deleteIklan')->name('iklan-manage.delete');

    // Rekap Transaksi
    Route::get('penjualan/rekap/transaksi', 'RekapTransaksiController@index')->name('penjualan.rekap.transaksi');
    Route::get('penjualan/rekap/transaksi-detail/{id}','RekapTransaksiController@detailTransaksi')->name('penjualan.rekap.detail-transaksi');
    Route::get('penjualan/rekap/detail-barang/{id}','RekapTransaksiController@detailBarang')->name('penjualan.rekap.detail-barang');
    Route::get('penjualan/rekap/kasir', 'RekapTransaksiController@rekapKasir')->name('penjualan.rekap.kasir');

    /*upload produk*/
    Route::get('upload-produk','BarangController@getUploadProduk')->name('get.produk-upload');
    Route::post('upload-produk', 'BarangController@postUploadProduk')->name('post.produk-upload');

    /*upload produk by akun*/
    Route::get('upload-produk-by-akun','BarangController@getUploadProdukByAkun')->name('get.produk-upload-by-akun');
    Route::post('upload-produk-by-akun/{type}','BarangController@postUploadProdukByAkun')->name('post.produk-upload-by-akun');

    
    /*upload pelanggan*/
    Route::get('upload-pelanggan','InformasiPelangganController@getUploadPelanggan')->name('get.pelanggan-upload');
    Route::post('upload-pelanggan', 'InformasiPelangganController@postUploadPelanggan')->name('post.pelanggan-upload');

    /*upload pemasok*/
    Route::get('upload-pemasok','InformasiPemasokController@getUploadPemasok')->name('get.pemasok-upload');
    Route::post('upload-pemasok', 'InformasiPemasokController@postUploadPemasok')->name('post.pemasok-upload');

    /*upload akun*/
    Route::get('upload-akun','AkunController@getUploadAkun')->name('get.akun-upload');
    Route::post('upload-akun', 'AkunController@postUploadAkun')->name('post.akun-upload');

    /*upload jasa pengiriman*/
    Route::get('upload-jasa-pengiriman','JasaPengirimanController@getUploadJasaPengiriman')->name('get.jasa-pengiriman-upload');
    Route::post('upload-jasa-pengiriman', 'JasaPengirimanController@postUploadJasaPengiriman')->name('post.jasa-pengiriman-upload');

    /*upload syarat pembayaran*/
    Route::get('upload-syarat-pembayaran','SyaratPembayaranController@getUploadSyaratPembayaran')->name('get.syarat-pembayaran-upload');
    Route::post('upload-syarat-pembayaran', 'SyaratPembayaranController@postUploadSyaratPembayaran')->name('post.syarat-pembayaran-upload');

    /*upload barcode*/
    Route::get('import-barcode','ImportBarcodeController@getUploadBarcode')->name('get.import-barcode');
    Route::post('import-barcode', 'ImportBarcodeController@postUploadBarcode')->name('post.import-barcode');

    Route::get('get-kode-aktiva', 'DaftarAktivaTetapController@reqKodeAktiva')->name('get-kode-aktiva');

    Route::resource('cart-log', 'CartLogController');

    // Margin harga jual
    Route::post('margin-harga-jual','BarangController@marginHargaJual')->name('barang.margin-harga-jual');
});
