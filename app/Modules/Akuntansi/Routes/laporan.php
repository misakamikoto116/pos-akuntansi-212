<?php

use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\TipePelanggan;
use App\Modules\Akuntansi\Models\User;

Route::group(['prefix' => 'akuntansi', 'as' => 'akuntansi.'], function () {
    Route::get('/laporan', function () {
        $formFilter         = 'akuntansi::laporan.include_laporan.detail_laporan.laporan_keuangan.laporan_keuangan_form_filter';
        $listUserKasir      = User::whereHas('roles', function ($query)
                                {
                                    $query->where('name','pos');
                                })->pluck('name','id');
        $tipePelanggan      = TipePelanggan::pluck('nama','id');
        $tipePelanggan      = TipePelanggan::with('pelanggan')->get();

        $data_pelanggan = [];
        foreach ($tipePelanggan as $key => $tipe_pelanggan) {
            $data_pelanggan[$tipe_pelanggan->nama] = $tipe_pelanggan->pelanggan->pluck('nama_formatted','id')->toArray();
        }

        return view('akuntansi::laporan.index')->with([
                                                        'formFilter'        => $formFilter,
                                                        'listUserKasir'     => $listUserKasir,
                                                        'tipePelanggan'     => $tipePelanggan,
                                                        'pelanggan'         => array_filter($data_pelanggan),
                                                     ]);
    });

    // Pembelian
    // Route::get('/laporan/cetak-permintaan-barang', function () {
    //     return view('akuntansi::laporan.cetak_laporan.aktivitas_pembelian.cetak_permintaan_barang');
    // });
    // Route::get('/laporan/cetak-pesanan-pembelian', function () {
    //     return view('akuntansi::laporan.cetak_laporan.aktivitas_pembelian.cetak_pesanan_pembelian');
    // });
    // Route::get('/laporan/cetak-penerimaan-barang', function () {
    //     return view('akuntansi::laporan.cetak_laporan.aktivitas_pembelian.cetak_penerimaan_barang');
    // });
    // Route::get('/laporan/cetak-faktur-pembelian', function () {
    //     return view('akuntansi::laporan.cetak_laporan.aktivitas_pembelian.cetak_faktur_pembelian');
    // });
    // Route::get('/laporan/cetak-pembayaran-pembelian', function () {
    //     return view('akuntansi::laporan.cetak_laporan.aktivitas_pembelian.cetak_pembayaran_pembelian');
    // });
    // Penjualan
    // Route::get('/laporan/cetak-barang', function () {
    //     return view('akuntansi::laporan.cetak_laporan.cetak_barang');
    // });

    Route::get('/laporan/cetak-pengiriman-barang', function () {
        return view('akuntansi::laporan.cetak_laporan.cetak_pengiriman_barang');
    });
    Route::get('/laporan/cetak-pesanan-penjualan', function () {
        return view('akuntansi::laporan.cetak_laporan.cetak_pesanan_penjualan');
    });
    Route::get('/laporan/cetak-faktur', function () {
        return view('akuntansi::laporan.cetak_laporan.cetak_faktur');
    });
    Route::get('/laporan/cetak-penjualan-penerimaan-pelanggan', function () {
        return view('akuntansi::laporan.cetak_laporan.cetak_penjualan_penerimaan_pelanggan');
    });
    // Laporan Keuangan - Neraca
    Route::get('laporan/keuangan/neraca', 'LaporanKeuanganController@laporan_neraca');
    Route::get('laporan/keuangan/neraca-induk-skontro', 'LaporanKeuanganController@laporan_neraca_induk_skontro');
    Route::get('laporan/keuangan/neraca-multi-periode', 'LaporanKeuanganController@laporan_neraca_multi_periode');
    Route::get('laporan/keuangan/neraca-perbandingan-bulan', 'LaporanKeuanganController@laporan_neraca_perbandingan_bulan');
    Route::get('laporan/keuangan/neraca-anggaran-periode', 'LaporanKeuanganController@laporan_neraca_anggaran_periode');
    Route::get('laporan/keuangan/neraca-perbandingan-anggaran', 'LaporanKeuanganController@laporan_neraca_perbandingan_anggaran');
    Route::get('laporan/keuangan/neraca-perbandingan-anggaran-periode', 'LaporanKeuanganController@laporan_neraca_perbandingan_anggaran_periode');
    Route::get('laporan/keuangan/neraca-ukuran-umum', 'LaporanKeuanganController@laporan_neraca_ukuran_umum');
    Route::get('laporan/keuangan/neraca-konsolidasi', 'LaporanKeuanganController@laporan_neraca_konsolidasi');
    // Laporan Keuangan - Laba Rugi
    Route::get('laporan/keuangan/laba-rugi-standar', 'LaporanKeuanganController@laporan_laba_rugi_standar');
    Route::get('laporan/keuangan/laba-rugi-multi-periode', 'LaporanKeuanganController@laporan_laba_rugi_multi_periode');
    Route::get('laporan/keuangan/laba-rugi-perbandingan-periode', 'LaporanKeuanganController@laporan_laba_rugi_perbandingan_periode');
    Route::get('laporan/keuangan/laba-rugi-anggaran-periode', 'LaporanKeuanganController@laporan_laba_rugi_anggaran_periode');
    Route::get('laporan/keuangan/laba-rugi-perbandingan-anggaran', 'LaporanKeuanganController@laporan_laba_rugi_perbandingan_anggaran');
    Route::get('laporan/keuangan/laba-rugi-perbandingan-anggaran-periode', 'LaporanKeuanganController@laporan_laba_rugi_perbandingan_anggaran_periode');
    Route::get('laporan/keuangan/laba-rugi-konsolidasi', 'LaporanKeuanganController@laporan_laba_rugi_konsolidasi');
    // Laporan Keuangan - Grafik Keuangan
    Route::get('laporan/keuangan/grafik-perbandingan-nilai-akun', 'LaporanKeuanganController@laporan_grafik_perbandingan_nilai_akun');
    // Laporan Buku Besar - Ringkasan Jurnal
    Route::get('laporan/buku-besar/daftar-histori-gl', 'LaporanBukuBesarController@daftar_histori_gl');
    Route::get('laporan/buku-besar/keseluruhan-jurnal', 'LaporanBukuBesarController@keseluruhan_jurnal');
    Route::get('laporan/buku-besar/ringkasan-buku-besar', 'LaporanBukuBesarController@ringkasan_buku_besar');
    Route::get('laporan/buku-besar/buku-besar-rinci', 'LaporanBukuBesarController@buku_besar_rinci');
    Route::get('laporan/buku-besar/neraca-saldo', 'LaporanBukuBesarController@neraca_saldo');
    Route::get('laporan/buku-besar/neraca-saldo-klasik', 'LaporanBukuBesarController@neraca_saldo_klasik');
    Route::get('laporan/buku-besar/untung-rugi-realisir', 'LaporanBukuBesarController@untung_rugi_realisir');
    Route::get('laporan/buku-besar/untung-rugi-nonrealisir', 'LaporanBukuBesarController@untung_rugi_nonrealisir');
    Route::get('laporan/buku-besar/bukti-jurnal-umum', 'LaporanBukuBesarController@bukti_jurnal_umum');
    // Laporan Kas & Bank - kas & bank
    Route::get('laporan/kas-dan-bank/daftar-akun-kas-dan-bank', 'LaporanKasBankController@daftar_akun_kas_dan_bank');
    Route::get('laporan/kas-dan-bank/buku-bank', 'LaporanKasBankController@buku_bank');
    Route::get('laporan/kas-dan-bank/rekonsili-bank', 'LaporanKasBankController@rekonsili_bank');
    Route::get('laporan/kas-dan-bank/histori-rekonsili-bank', 'LaporanKasBankController@histori_rekonsili_bank');
    Route::get('laporan/kas-dan-bank/ringkasan-proyeksi-arus-kas', 'LaporanKasBankController@ringkasan_proyeksi_arus_kas');
    Route::get('laporan/kas-dan-bank/rincian-proyeksi-arus-kas', 'LaporanKasBankController@rincian_proyeksi_arus_kas');
    Route::get('laporan/kas-dan-bank/arus-kas-per-akun', 'LaporanKasBankController@arus_kas_per_akun');
    Route::get('laporan/kas-dan-bank/giro-mundur-belum-jatuh-tempo', 'LaporanKasBankController@giro_mundur_belum_jatuh_tempo');
    // Laporan Kas & Bank - Rincian Pembayaran dan Penerimaan
    Route::get('laporan/kas-dan-bank/rincian-daftar-pembayaran', 'LaporanKasBankController@rincian_daftar_pembayaran');
    Route::get('laporan/kas-dan-bank/ringkasan-pembayaran-per-bank', 'LaporanKasBankController@ringkasan_pembayaran_per_bank');
    Route::get('laporan/kas-dan-bank/rincian-pembayaran-per-bank', 'LaporanKasBankController@rincian_pembayaran_per_bank');
    Route::get('laporan/kas-dan-bank/rincian-daftar-penerimaan', 'LaporanKasBankController@rincian_daftar_penerimaan');
    Route::get('laporan/kas-dan-bank/ringkasan-penerimaan-per-bank', 'LaporanKasBankController@ringkasan_penerimaan_per_bank');
    Route::get('laporan/kas-dan-bank/rincian-penerimaan-per-bank', 'LaporanKasBankController@rincian_penerimaan_per_bank');
    // Laporan Akun Piutang dan Pelanggan - Rincian Piutang
    Route::get('laporan/akun-piutang-pelanggan/faktur-belum-lunas', 'LaporanAkunPiutangPelangganController@faktur_belum_lunas');
    Route::get('laporan/akun-piutang-pelanggan/ringkasan-umur-piutang', 'LaporanAkunPiutangPelangganController@ringkasan_umur_piutang');
    Route::get('laporan/akun-piutang-pelanggan/rincian-umur-piutang', 'LaporanAkunPiutangPelangganController@rincian_umur_piutang');
    Route::get('laporan/akun-piutang-pelanggan/ringkasan-buku-besar-pembantu-piutang', 'LaporanAkunPiutangPelangganController@ringkasan_buku_besar_pembantu_piutang');
    Route::get('laporan/akun-piutang-pelanggan/rincian-buku-besar-pembantu-piutang', 'LaporanAkunPiutangPelangganController@rincian_buku_besar_pembantu_piutang');
    Route::get('laporan/akun-piutang-pelanggan/histori-piutang-pelanggan', 'LaporanAkunPiutangPelangganController@histori_piutang_pelanggan');
    Route::get('laporan/akun-piutang-pelanggan/ringkasan-pembayaran-faktur', 'LaporanAkunPiutangPelangganController@ringkasan_pembayaran_faktur');
    Route::get('laporan/akun-piutang-pelanggan/rincian-pembayaran-faktur', 'LaporanAkunPiutangPelangganController@rincian_pembayaran_faktur');
    Route::get('laporan/akun-piutang-pelanggan/laporan-pelanggan', 'LaporanAkunPiutangPelangganController@laporan_pelanggan');
    Route::get('laporan/akun-piutang-pelanggan/penagihan-per-pelanggan', 'LaporanAkunPiutangPelangganController@penagihan_per_pelanggan');
    Route::get('laporan/akun-piutang-pelanggan/penagihan-per-tipe-pelanggan', 'LaporanAkunPiutangPelangganController@penagihan_per_tipe_pelanggan');
    // Laporan Akun Piutang dan Pelanggan - Data Pelanggan
    Route::get('laporan/akun-piutang-pelanggan/daftar-pelanggan', 'LaporanAkunPiutangPelangganController@daftar_pelanggan');
    Route::get('laporan/akun-piutang-pelanggan/ringkasan-penerimaan-penjualan', 'LaporanAkunPiutangPelangganController@ringkasan_penerimaan_penjualan');
    Route::get('laporan/akun-piutang-pelanggan/rincian-penerimaan-penjualan', 'LaporanAkunPiutangPelangganController@rincian_penerimaan_penjualan');
    // Laporan Penjualan - Rincian Penjualan
    Route::get('laporan/penjualan/penjualan-per-pelanggan', 'LaporanPenjualanController@penjualan_per_pelanggan')->name('get-laporan-penjualan-per_pelanggan');
    Route::get('laporan/penjualan/rincian-penjualan-per-pelanggan', 'LaporanPenjualanController@rincian_penjualan_per_pelanggan');
    Route::get('laporan/penjualan/penjualan-per-barang-per-pelanggan', 'LaporanPenjualanController@penjualan_perbarang_perpelanggan');
    Route::get('laporan/penjualan/rincian-penjualan-per-tipe-pelanggan', 'LaporanPenjualanController@rincian_penjualan_per_tipe_pelanggan');
    Route::get('laporan/penjualan/penjualan-perbarang-omset', 'LaporanPenjualanController@penjualan_perbarang_omset');
    Route::get('laporan/penjualan/penjualan-perbarang-kuantitas', 'LaporanPenjualanController@penjualan_perbarang_kuantitas');
    Route::get('laporan/penjualan/penjualan-perbarang', 'LaporanPenjualanController@penjualan_perbarang')->name('get-laporan-penjualan-perbarang');
    Route::get('laporan/penjualan/rincian-penjualan-perbarang', 'LaporanPenjualanController@rincian_penjualan_perbarang');
    Route::get('laporan/penjualan/penjualan-perbarang-perkasir', 'LaporanPenjualanController@penjualan_perbarang_per_kasir')->name('get-laporan-penjualan-perbarang-perkasir');
    Route::get('laporan/penjualan/penjualan-pertransaksi-perkasir', 'LaporanPenjualanController@penjualan_pertransaksi_per_kasir')->name('get-laporan-penjualan-pertransaksi-perkasir');
    Route::get('laporan/penjualan/penjualan-perbarang-pos', 'LaporanPenjualanController@penjualan_perbarang_pos')->name('get-laporan-penjualan-perbarang-pos');
    Route::get('laporan/penjualan/penjualan-pelanggan-perbarang', 'LaporanPenjualanController@penjualan_pelanggan_perbarang');
    Route::get('laporan/penjualan/penjualan-barang-per-pelanggan', 'LaporanPenjualanController@penjualan_perbarang_pelanggan');
    Route::get('laporan/penjualan/retur-penjualan-per-pelanggan', 'LaporanPenjualanController@retur_penjualan_per_pelanggan');
    Route::get('laporan/penjualan/retur-penjualan-perbarang', 'LaporanPenjualanController@retur_penjualan_perbarang');
    Route::get('laporan/penjualan/penjualan-tahunan', 'LaporanPenjualanController@penjualan_tahunan');
    Route::get('laporan/penjualan/grafik-prestasi-penjualan-barang', 'LaporanPenjualanController@grafik_prestasi_penjualan_barang');
    Route::get('laporan/penjualan/grafik-prestasi-penjualan-pelanggan', 'LaporanPenjualanController@grafik_prestasi_penjualan_pelanggan');
    // Laporan Penjualan - Rincian Penawaran
    Route::get('laporan/penjualan/penawaran-penjualan-belum-terproses', 'LaporanPenjualanController@penawaran_penjualan_belum_terproses');
    Route::get('laporan/penjualan/penawaran-penjualan-per-barang-belum-terproses', 'LaporanPenjualanController@penawaran_penjualan_per_barang_belum_terproses');
    Route::get('laporan/penjualan/pesanan-penjualan-per-pelanggan-belum-terprosesi', 'LaporanPenjualanController@pesanan_penjualan_per_pelanggan_belum_terprosesi');
    Route::get('laporan/penjualan/pesanan-penjualan-per-barang-belum-terprosesi', 'LaporanPenjualanController@pesanan_penjualan_per_barang_belum_terprosesi');
    Route::get('laporan/penjualan/uang-muka-pesanan-penjualan', 'LaporanPenjualanController@uang_muka_pesanan_penjualan');
    Route::get('laporan/penjualan/uang-muka-per-faktur', 'LaporanPenjualanController@uang_muka_per_faktur');
    Route::get('laporan/penjualan/histori-penawaran-penjualan', 'LaporanPenjualanController@histori_penawaran_penjualan');
    Route::get('laporan/penjualan/histori-pesanan-penjualan', 'LaporanPenjualanController@histori_pesanan_penjualan');
    Route::get('laporan/penjualan/histori-pengirim-pesanan', 'LaporanPenjualanController@histori_pengirim_pesanan');
    // Laporan Penjualan - Rincian Penawaran Tagihan Dan Pendapatan RMA
    Route::get('laporan/penjualan/tagihan-rma', 'LaporanPenjualanController@tagihan_rma');
    Route::get('laporan/penjualan/pendapatan-rma', 'LaporanPenjualanController@pendapatan_rma');
    // Laporan Akun Hutang dan Pemasok - Rincian Akun Hutang
    Route::get('laporan/akun-hutang-pemasok/hutang-beredar', 'LaporanAkunHutangPemasokController@hutang_beredar');
    Route::get('laporan/akun-hutang-pemasok/ringkasan-umur-hutang', 'LaporanAkunHutangPemasokController@ringkasan_umur_hutang');
    Route::get('laporan/akun-hutang-pemasok/rincian-umur-hutang', 'LaporanAkunHutangPemasokController@rincian_umur_hutang');
    Route::get('laporan/akun-hutang-pemasok/ringkasan-buku-besar-pembantu-hutang', 'LaporanAkunHutangPemasokController@ringkasan_buku_besar_pembantu_hutang');
    Route::get('laporan/akun-hutang-pemasok/rincian-buku-besar-pembantu-hutang', 'LaporanAkunHutangPemasokController@rincian_buku_besar_pembantu_hutang');
    Route::get('laporan/akun-hutang-pemasok/histori-hutang-pelanggan', 'LaporanAkunHutangPemasokController@histori_hutang_pelanggan');
    Route::get('laporan/akun-hutang-pemasok/ringkasan-pembayaran-faktur', 'LaporanAkunHutangPemasokController@ringkasan_pembayaran_faktur');
    Route::get('laporan/akun-hutang-pemasok/rincian-pembayaran-faktur', 'LaporanAkunHutangPemasokController@rincian_pembayaran_faktur');
    Route::get('laporan/akun-hutang-pemasok/laporan-pemasok', 'LaporanAkunHutangPemasokController@laporan_pemasok');
    // Laporan Akun Hutang dan Pemasok - Rincian Pemasok
    Route::get('laporan/akun-hutang-pemasok/daftar-pemasok', 'LaporanAkunHutangPemasokController@daftar_pemasok');
    Route::get('laporan/akun-hutang-pemasok/ringkasan-pembayaran-pembelian', 'LaporanAkunHutangPemasokController@ringkasan_pembayaran_pembelian');
    Route::get('laporan/akun-hutang-pemasok/rincian-pembayaran-pembelian', 'LaporanAkunHutangPemasokController@rincian_pembayaran_pembelian');
    // Laporan Pembelian - Rincian Pembelian
    Route::get('laporan/pembelian/pembelian-per-pemasok', 'LaporanPembelianController@pembelian_per_pemasok');
    Route::get('laporan/pembelian/rincian-pembelian-per-pemasok', 'LaporanPembelianController@rincian_pembelian_per_pemasok');
    Route::get('laporan/pembelian/pembelian-per-barang-total', 'LaporanPembelianController@pembelian_per_barang_total');
    Route::get('laporan/pembelian/pembelian-per-barang-kuantitas', 'LaporanPembelianController@pembelian_per_barang_kuantitas');
    Route::get('laporan/pembelian/pembelian-per-barang', 'LaporanPembelianController@pembelian_per_barang');
    Route::get('laporan/pembelian/rincian-pembelian-per-barang', 'LaporanPembelianController@rincian_pembelian_per_barang');
    Route::get('laporan/pembelian/pembelian-per-biaya', 'LaporanPembelianController@pembelian_per_biaya');
    Route::get('laporan/pembelian/pengembalian-pembelian-per-pemasok', 'LaporanPembelianController@pengembalian_pembelian_per_pemasok');
    Route::get('laporan/pembelian/pengembalian-pembelian-per-barang', 'LaporanPembelianController@pengembalian_pembelian_per_barang');
    // Laporan Pembelian - Rincian Pesanan dan Pembelian
    Route::get('laporan/pembelian/permintaan-pembelian-belum-proses', 'LaporanPembelianController@permintaan_pembelian_belum_proses');
    Route::get('laporan/pembelian/pesanan-pembelian-per-pemasok-belum-proses', 'LaporanPembelianController@pesanan_pembelian_per_pemasok_belum_proses');
    Route::get('laporan/pembelian/pesanan-pembelian-per-barang', 'LaporanPembelianController@pesanan_pembelian_per_barang');
    Route::get('laporan/pembelian/histori-permintaan-pembelian', 'LaporanPembelianController@histori_permintaan_pembelian');
    Route::get('laporan/pembelian/histori-pesanan-pembelian', 'LaporanPembelianController@histori_pesanan_pembelian');
    Route::get('laporan/pembelian/histori-penerimaan-barang', 'LaporanPembelianController@histori_penerimaan_barang');
    // Laporan Pembelian - Rincian Uang Muka Faktur dan Pesanan
    Route::get('laporan/pembelian/uang-muka-faktur-pembelian', 'LaporanPembelianController@uang_muka_faktur_pembelian');
    Route::get('laporan/pembelian/uang-muka-pesanan-pembelian', 'LaporanPembelianController@uang_muka_pesanan_pembelian');
    // Laporan Pembiayaan Pesanan - Rincian Pembiayaan Pesanan
    Route::get('laporan/pembiayaan-pesanan/pembiayaan-pesanan-per-barang', 'LaporanPembiayaanPesananController@pembiayaan_pesanan_per_barang');
    Route::get('laporan/pembiayaan-pesanan/pembiayaan-pesanan-per-beban', 'LaporanPembiayaanPesananController@pembiayaan_pesanan_per_beban');
    Route::get('laporan/pembiayaan-pesanan/pembiayaan-pesanan-per-departemen', 'LaporanPembiayaanPesananController@pembiayaan_pesanan_per_departemen');
    Route::get('laporan/pembiayaan-pesanan/pembiayaan-pesanan-per-proyek', 'LaporanPembiayaanPesananController@pembiayaan_pesanan_per_proyek');
    Route::get('laporan/pembiayaan-pesanan/pembiayaan-pesanan-yang-telah-selesai', 'LaporanPembiayaanPesananController@pembiayaan_pesanan_yang_telah_selesai');
    // Laporan Pembiayaan Pesanan - Rincian Penawaran
    Route::get('laporan/pembiayaan-pesanan/rincian-jurnal-pembiayaan-pesanan', 'LaporanPembiayaanPesananController@rincian_jurnal_pembiayaan_pesanan');
    Route::get('laporan/pembiayaan-pesanan/ringkasan-histori-pembiayaan-pesanan', 'LaporanPembiayaanPesananController@ringkasan_histori_pembiayaan_pesanan');
    // Laporan Aktiva Tetap - Rincian Aktiva Tetap
    Route::get('laporan/aktiva-tetap/tipe-aktiva-tetap-pajak', 'LaporanAktivaTetapController@tipe_aktiva_tetap_pajak');
    Route::get('laporan/aktiva-tetap/daftar-aktiva-tetap-per-aktiva-tetap', 'LaporanAktivaTetapController@daftar_aktiva_tetap_per_aktiva_tetap');
    Route::get('laporan/aktiva-tetap/daftar-aktiva-tetap-per-aktiva-pajak', 'LaporanAktivaTetapController@daftar_aktiva_tetap_per_aktiva_pajak');
    Route::get('laporan/aktiva-tetap/histori-aktiva-tetap', 'LaporanAktivaTetapController@histori_aktiva_tetap');
    Route::get('laporan/aktiva-tetap/rincian-jurnal-aktiva-tetap', 'LaporanAktivaTetapController@rincian_jurnal_aktiva_tetap');
    Route::get('laporan/aktiva-tetap/daftar-penyusutan-aktiva-tetap', 'LaporanAktivaTetapController@daftar_penyusutan_aktiva_tetap');
    Route::get('laporan/aktiva-tetap/perbedaan-penyusutan-sementara', 'LaporanAktivaTetapController@perbedaan_penyusutan_sementara');
    Route::get('laporan/aktiva-tetap/histori-perubahan-asset-tetap', 'LaporanAktivaTetapController@histori_perubahan_asset_tetap');
    // Laporan Barang
    Route::get('laporan/barang/daftar-barang', 'LaporanBarangController@daftar_barang');

    // Laporan Penawaran Penjualan
    Route::post('laporan/cetak-penawaran-penjualan', 'LaporanAktivitasPenjualanController@penawaranPenjualan')->name('cetak-penawaran-penjualan');
    Route::put('laporan/edit-cetak-penawaran-penjualan', 'LaporanAktivitasPenjualanController@penawaranPenjualan')->name('edit-cetak-penawaran-penjualan');
    // Laporan Pesanan Penjualan
    Route::post('laporan/cetak-pesanan-penjualan', 'LaporanAktivitasPenjualanController@pesananPenjualan')->name('cetak-pesanan-penjualan');
    Route::put('laporan/edit-cetak-pesanan-penjualan', 'LaporanAktivitasPenjualanController@pesananPenjualan')->name('edit-cetak-pesanan-penjualan');
    // Laporan Pengiriman Penjualan
    Route::post('laporan/cetak-pengiriman-penjualan', 'LaporanAktivitasPenjualanController@pengirimanPenjualan')->name('cetak-pengiriman-penjualan');
    Route::put('laporan/edit-cetak-pengiriman-penjualan', 'LaporanAktivitasPenjualanController@pengirimanPenjualan')->name('edit-cetak-pengiriman-penjualan');
    // Laporan Faktur Penjualan
    Route::post('laporan/cetak-faktur-penjualan', 'LaporanAktivitasPenjualanController@fakturPenjualan')->name('cetak-faktur-penjualan');
    Route::put('laporan/edit-cetak-faktur-penjualan', 'LaporanAktivitasPenjualanController@fakturPenjualan')->name('edit-cetak-faktur-penjualan');
    // Laporan Penerimaan Penjualan
    Route::post('laporan/cetak-penerimaan-penjualan', 'LaporanAktivitasPenjualanController@penerimaanPenjualan')->name('cetak-penerimaan-penjualan');
    Route::put('laporan/edit-cetak-penerimaan-penjualan', 'LaporanAktivitasPenjualanController@penerimaanPenjualan')->name('edit-cetak-penerimaan-penjualan');

    // Laporan Permintann Pembelian
    Route::post('laporan/cetak-permintaan-pembelian', 'LaporanAktivitasPembelianController@permintaanPembelian')->name('cetak-permintaan-pembelian');
    Route::put('laporan/edit-cetak-permintaan-pembelian', 'LaporanAktivitasPembelianController@permintaanPembelian')->name('edit-cetak-permintaan-pembelian');
    // Laporan Pesanan Pembelian
    Route::post('laporan/cetak-pesanan-pembelian', 'LaporanAktivitasPembelianController@pesananPembelian')->name('cetak-pesanan-pembelian');
    Route::put('laporan/edit-cetak-pesanan-pembelian', 'LaporanAktivitasPembelianController@pesananPembelian')->name('edit-cetak-pesanan-pembelian');
    // Laporan Penerimaan Pembelian
    Route::post('laporan/cetak-penerimaan-pembelian', 'LaporanAktivitasPembelianController@penerimaanPembelian')->name('cetak-penerimaan-pembelian');
    Route::put('laporan/edit-cetak-penerimaan-pembelian', 'LaporanAktivitasPembelianController@penerimaanPembelian')->name('edit-cetak-penerimaan-pembelian');
    // Laporan Faktur Pembelian
    Route::post('laporan/cetak-faktur-pembelian', 'LaporanAktivitasPembelianController@fakturPembelian')->name('cetak-faktur-pembelian');
    Route::put('laporan/edit-cetak-faktur-pembelian', 'LaporanAktivitasPembelianController@fakturPembelian')->name('edit-cetak-faktur-pembelian');

    // Laporan Pembayaran Pembelian
    Route::post('laporan/cetak-pembayaran-pembelian', 'LaporanAktivitasPembelianController@PembayaranPembelian')->name('cetak-pembayaran-pembelian');
    Route::put('laporan/edit-cetak-pembayaran-pembelian', 'LaporanAktivitasPembelianController@PembayaranPembelian')->name('edit-cetak-pembayaran-pembelian');

    // Cetak Pembayaran
    Route::post('laporan/cetak-pembayaran', 'PembayaranController@getCetakPembayaran')->name('cetak-pembayaran');
    Route::put('laporan/edit-cetak-pembayaran', 'PembayaranController@getCetakPembayaran')->name('edit-cetak-pembayaran');

    // Cetak Penerimaan
    Route::post('laporan/cetak-penerimaan', 'PenerimaanController@getCetakPenerimaan')->name('cetak-penerimaan');
    Route::put('laporan/edit-cetak-penerimaan', 'PenerimaanController@getCetakPenerimaan')->name('edit-cetak-penerimaan');

    // Cetak Jurnal Umum
    Route::post('laporan/cetak-jurnal-umum', 'JurnalUmumController@getCetakJurnalUmum')->name('cetak-jurnal-umum');
    Route::put('laporan/edit-cetak-jurnal-umum', 'JurnalUmumController@getCetakJurnalUmum')->name('edit-cetak-jurnal-umum');

    // Laporan Lain-lain
    Route::get('laporan/laporan-lain-lain/daftar-barang', 'LaporanLainLainController@daftar_barang');
    Route::get('laporan/laporan-lain-lain/daftar-mata-uang', 'LaporanLainLainController@daftar_mata_uang');
    Route::get('laporan/laporan-lain-lain/daftar-rincian-mata-uang', 'LaporanLainLainController@rincian_daftar_mata_uang');
    Route::get('laporan/laporan-lain-lain/daftar-pajak', 'LaporanLainLainController@daftar_pajak');
    Route::get('laporan/laporan-lain-lain/daftar-syarat-pembayaran', 'LaporanLainLainController@daftar_syarat_pembayaran');
    Route::get('laporan/laporan-lain-lain/daftar-pengiriman', 'LaporanLainLainController@daftar_pengiriman');
    Route::get('laporan/laporan-lain-lain/daftar-tipe-pelanggan', 'LaporanLainLainController@daftar_tipe_pelanggan');
    Route::get('laporan/laporan-lain-lain/daftar-batasan-komisi', 'LaporanLainLainController@daftar_batasan_komisi');
    Route::get('laporan/laporan-lain-lain/ringkasan-daftar-pengguna', 'LaporanLainLainController@ringkasan_daftar_pengguna');

    // Gudang
    Route::get('laporan/gudang/daftar-gudang', 'LaporanGudangController@daftar_gudang');
    Route::get('laporan/gudang/rincian-daftar-perpindahan-barang', 'LaporanGudangController@rincian_daftar_perpindahan_barang');
    Route::get('laporan/gudang/mutasi-perbarang-pergudang', 'LaporanGudangController@mutasi_perbarang_pergudang');
    Route::get('laporan/gudang/mutasi-pergudang-perbarang', 'LaporanGudangController@mutasi_pergudang_perbarang');
    Route::get('laporan/gudang/ringkasan-mutasi-gudang-perbarang', 'LaporanGudangController@ringkasan_mutasi_gudang_perbarang');
    Route::get('laporan/gudang/kuantitas-barang-per-daftar-gudang', 'LaporanGudangController@kuantitas_barang_per_daftar_gudang');
    Route::get('laporan/gudang/lembar-perhitungan-stok-fisik-barang', 'LaporanGudangController@lembar_perhitungan_stok_fisik_barang');

    // Persediaan
    Route::get('laporan/persediaan/ringkasan-valuasi-persediaan', 'LaporanPersediaanController@ringkasan_valuasi_persediaan');
    Route::get('laporan/persediaan/rincian-valuasi-persediaan', 'LaporanPersediaanController@rincian_valuasi_persediaan');
    Route::get('laporan/persediaan/ringkasan-analisis-persediaan', 'LaporanPersediaanController@ringkasan_analisis_persediaan');
    Route::get('laporan/persediaan/rekap-umur-persediaan', 'LaporanPersediaanController@rekap_umur_persediaan');
    Route::get('laporan/persediaan/rincian-umur-persediaan', 'LaporanPersediaanController@rincian_umur_persediaan');
    Route::get('laporan/persediaan/kartu-stok-persediaan', 'LaporanPersediaanController@kartu_stok_persediaan');
    Route::get('laporan/persediaan/histori-penyesuaian-harga-jual-barang', 'LaporanPersediaanController@histori_penyesuaian_harga_jual_barang');
    Route::get('laporan/persediaan/histori-barang', 'LaporanPersediaanController@histori_barang');
    Route::get('laporan/persediaan/rincian-daftar-penyesuaian-persediaan', 'LaporanPersediaanController@rincian_daftar_penyesuaian_persediaan');
    Route::get('laporan/persediaan/group-barang', 'LaporanPersediaanController@group_barang');
    Route::get('laporan/persediaan/jurnal-persediaan', 'LaporanPersediaanController@jurnal_persediaan');

    // Laporan Pembiayaan
    Route::get('laporan/pembiayaan/pembiayaan-pesanan-per-barang', 'LaporanPembiayaanPesananController@pembiayaan_pesanan_per_barang');
    Route::get('laporan/pembiayaan/pembiayaan-pesanan-per-beban', 'LaporanPembiayaanPesananController@pembiayaan_pesanan_per_beban');
    Route::get('laporan/pembiayaan/pembiayaan-pesanan-yang-telah-selesai', 'LaporanPembiayaanPesananController@pembiayaan_pesanan_yang_telah_selesai');
    
});
