<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use Generator\Interfaces\RepositoryInterface;

class BarangPergudangRepository implements RepositoryInterface
{
    public function __construct(Produk $model, Gudang $gudang)
    {
        $this->model = $model;
        $this->gudang = $gudang;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        $produk         =   Produk::get(['id','no_barang','keterangan']);
        $gudang         =   Gudang::pluck('nama','id');
        $dataSaldo      =   SaldoAwalBarang::with('gudang','produk')
                            ->whereIn('produk_id',$produk->pluck('id'))
                            ->whereIn('gudang_id',$gudang->keys())
                            ->get();

        return $this->KuantitasBarangGudang($dataSaldo, $gudang, $produk);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->find($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->findItem($id)->fill($data);
        $model->save();

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $model = $this->model->fill($data);
        $model->save();

        return $model;
    }

    /**e
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }

    public function KuantitasBarangGudang($dataSaldo, $gudang, $produk)
    {
        $output = collect()->push($gudang->values()->prepend('Nama Barang')->prepend('No Barang'));
        $items = $dataSaldo->groupBy('produk_id')->map(function ($produk){
            return $produk->groupBy('gudang_id')->map(function ($gudang)
            {
                return $gudang->sum('kuantitas');
            });
        });
        $noBarang = $produk->pluck('no_barang','id');
        $pordukBarang = $produk->pluck('keterangan','id');
        foreach ($items as $produkID => $gudangItem) {
            $temp = collect();
            $temp->push($noBarang[$produkID]);
            $temp->push($pordukBarang[$produkID]);
            foreach ($gudang->keys() as $gudangID) {
                $temp->push($gudangItem[$gudangID] ?? 0);
            }
            $output->push($temp);
        }
        return $output;
    }
}
