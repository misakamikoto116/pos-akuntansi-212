<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\DetailBukuKeluar;
use App\Modules\Akuntansi\Models\Pembayaran;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\Identitas;
use Carbon\Carbon;
use Cart;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
use Session;
use Helpers\TransaksiHelper;
use App\Modules\Akuntansi\Http\Controllers\TraitLaporan; 

class PembayaranRepository implements RepositoryInterface
{
    use TraitLaporan; 

    public function __construct(Pembayaran $model, Akun $akun, TransaksiHelper $transaksi, Identitas $identitas)
    {
        $this->model     = $model;
        $this->akun      = $akun;
        $this->transaksi = $transaksi;
        // untuk laporan
        $this->identitas = $identitas;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {

        return $this->model->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $edit_pembayaran = $this->model->find($id);
        $this->formattingField($edit_pembayaran);
        return $edit_pembayaran;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_pembayaran_lain')){
            return abort(403);
        }

        $model = $this->findItem($id);
        $this->transaksi->ExecuteTransactionSingle('delete', $model);
        return $model->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $convert = str_replace(["Rp ",".00",","],"", $data['nominal']);
        $data = [
            'akun_id' => $data['akun_id'],
            'no_faktur' => $data['no_faktur'],
            'no_cek'    => $data['no_cek'],
            'keterangan' => $data['keterangan'],
            'tanggal' => Carbon::parse($data['tanggal'])->format('Y-m-d H:i:s'),
            'nominal' => $convert,
        ];
        $model = $this->findItem($id)->fill($data);
        if($model->save()){
            $this->transaksi->ExecuteTransactionSingle('delete', $model);
            DetailBukuKeluar::where(['buku_keluar_id' => $model->id])->delete();
            $this->InsertFromCart($model, $convert);
        }
        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $convert = str_replace(["Rp ",".00",","],"", $data['nominal']);
        $input = [
            'akun_id' => $data['akun_id'],
            'no_cek' => $data['no_cek'],
            'no_faktur' => $data['no_faktur'],
            'keterangan' => $data['keterangan'],
            'tanggal' => Carbon::parse($data['tanggal'])->format('Y-m-d H:i:s'),
            'peruntukan' => $data['peruntukan'],
            'nominal' => $convert,
            ];

        $model = $this->model->fill($input);;
        if($model->save()){
            $this->InsertFromCart($model, $convert);
        }
        return $model;
    }

    public function InsertFromCart($model, $convert)
    {
        $arrKredit = [];
        $arrDebit  = [];
        $i = 0;
        foreach (Cart::instance('pembayaran')->content() as $value) {
            $i++;
            $akun = Akun::where('kode_akun',$value->options->akun_id)->pluck('id');
            $convert1 = str_replace(".0","", $value->price);
            $detail_buku_keluar = new DetailBukuKeluar;

            $arrDebit[] = [
                'nominal'   => $convert1,
                'tanggal'   => $model->tanggal,
                'dari'      => 'Pembayaran Detail '.$akun->first().' '.$i,
                'akun'      => $akun->first()
            ];

            $detail_buku_keluar->akun_id = $akun->first();
            $detail_buku_keluar->buku_keluar_id = $model->id;
            $detail_buku_keluar->nominal = $convert1;
            $detail_buku_keluar->catatan = $value->options->catatan_detail;
            $detail_buku_keluar->save();
        }

        $arrKredit = [
            'nominal'   => $convert,
            'tanggal'   => $model->tanggal,
            'dari'      => 'Pembayaran',
            'akun'      => $model['akun_id']
        ];

        $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrKredit, 0, $model->no_faktur);
        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrDebit, 1, $model->no_faktur);

        Cart::instance('pembayaran')->destroy();
        Session::forget([
              'akun_id',
              'nama_akun',
              'no_cek',
              'no_faktur',
              'tanggal',
              'keterangan',
              'nominal',
              'peruntukan',
            ]);
        return true;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(10);
    }
    public function listAkun()
    {
        return $this->akun->whereDoesntHave('childAkun')->whereHas('tipeAkun', function ($query)
        {
            $query->whereNotIn('title',['Kas/Bank']);
        })->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;
            return $output;
        }, []);
    }
    public function listAkunId()
    {
        return $this->akun->whereHas('tipeAkun', function ($query)
        {
            $query->where('title','Kas/Bank');
        })->where('parent_id','!=',null)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function formattingField($model)
    {
        if (!empty($model->tanggal)) {
            $model->tanggal = Carbon::parse($model->tanggal)->format('d F Y');
        }
        if (!empty($model->nominal)) {
            $model->nominal = number_format($model->nominal);
        }
    }

    public function laporanPembayaran($request)
    {
        $title = 'Ringkasan Daftar Pembayaran';

        $dataPembayaran = $this->model->ReportPembayaran($request)->get();
        
        $itemPembayaran = $dataPembayaran->map(function($query){
            return $arrayPembayaran = [
                'tanggal'   => Carbon::parse($query->tanggal)->format('d F Y'),
                'no_faktur' => $query->no_faktur,
                'dari_bank' => $query->akun->nama_akun,
                'keterangan'=> $query->keterangan,
                'jumlah'    => $query->nominal
            ];
        });

        $view = [
            'title'         =>  $title,
            'subTitle'      =>  $this->generateSubTitleDMY($request),
            'perusahaan'    =>  $this->identitasPerushaan(),
            'item'          =>  $itemPembayaran,
        ];

        return view('akuntansi::pembayaran.laporan_pembayaran')->with($view);
    }

    public function cetakPembayaran($data)
    {
        $cart = Cart::instance('pembayaran')->content();
        $items              = $data->all();
        $identitas          = $this->identitas->first();
        $akun_paid          = $this->akun->where('id', $items['akun_id'])->first();
        unset($items['akun_id'], $items['ww']);
        $items['kode_akun'] = $akun_paid['kode_akun'];
        $items['nama_akun'] = $akun_paid['nama_akun'];
        $view = [
            'items'             => $items,
            'identitas'         => $identitas,
            'cart'              => $cart,
        ];
        return view('akuntansi::laporan.cetak_laporan.cetak_pembayaran')->with($view);
    }
}
