<?php

namespace App\Modules\Akuntansi\Repositories;

use Carbon\carbon;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\PenerimaanPembelian;
use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use App\Modules\Akuntansi\Models\BebanFakturPembelian;
use App\Modules\Akuntansi\Models\BarangFakturPembelian;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\TransaksiUangMukaPemasok;
use App\Modules\Akuntansi\Models\BarangPengirimanPenjualan;
use App\Modules\Akuntansi\Models\BantuanPenerimaanPembelian;

trait TraitFakturPembelianRepository
{
    public function insertSubItem($data, $model, $type)
    {
        $data['invoice_date'] = (empty($data['invoice_date'])) ? $data['invoice_date'] = date('Y-m-d H:i:s') : $data['invoice_date'] = $this->date->IndonesiaToSql($data['invoice_date']);

        $serviceProduct = $this->produk->whereIn('id', $data['produk_id'])->whereIn('tipe_barang', [2, 4])->pluck('id');

        $sumPajak = 0;
        $sumPajak1 = 0;
        $harga_terakhir = 0;
        $harga_modal = 0;
        $totalNilaiBeban = 0;
        $totalNilaiBebanForPengiriman = 0;
        $items = [];
        $dp = [];
        $produkDetail = [];
        $arrayKredit = [];
        // $arrayPengirimanKredit = [];
        $arrayDebit = [];
        $arrayBelumTertagih = [];
        $arrayBeban = [];

        $penerimaan_id = $this->barang_penerimaan->whereIn('id', $data['barang_penerimaan_pembelian_id'])->groupBy('penerimaan_pembelian_id')->pluck('penerimaan_pembelian_id');
        $penerimaan = $this->penerimaan->whereIn('id', $penerimaan_id)->get();
        // dd($penerimaan);
        if (!$penerimaan->isEmpty()) {
            //Harga modal masuk : ke tb produk_detail
            $getTransaksi = Transaksi::where([
                'item_id' => $penerimaan->first()->id,
                'item_type' => get_class($penerimaan->first()),
                // 'dari' => 'Total Belum Tertagih Fakur Pembelian',
            ])->pluck('id');
            // foreach ($getTransaksi as $gt) {
            //     // mencari hasil similiar
            //     SaldoAwalBarang::where([
            //         'status' => 1,
            //         'status_transaksi' => 5,
            //         'transaksi_id' => $gt, ])->delete();
            // }

            foreach ($penerimaan as $receive) {
                $this->transaksi->ExecuteTransactionSingle('delete', $receive);
            }
        }

        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        $count = collect($data['produk_id'])->count();
        $count_dp = (empty($data['faktur_dp_id'])) ? 0 : collect($data['faktur_dp_id'])->count();
        $count_beban = (empty($data['akun_beban_id'])) ? 0 : collect($data['akun_beban_id'])->count();
        $total_amount = array_sum($data['amount_produk']);

        $detailProdukArr    = $this->sortAkunProduk($data);
        $nilai_pajak_akhir  = null;

        if (!empty($data['kode_pajak_id'])) {
            $nilai_pajak_akhir = $this->findPajak($data, $model);
        }

        // T R A N S A K S I
        // B E B A N
        // dd($pemasok_beban_id);
        if (!empty($data['akun_beban_id'])) {
            for ($i = 0; $i < $count_beban; ++$i) {
                $txt_alokasi_ke_barang = (empty($data['txt_alokasi_ke_barang'][$i])) ? null : $data['txt_alokasi_ke_barang'][$i];
                $pemasok_beban_id = (empty($data['pemasok_beban_id_real'][$i])) ? null : $data['pemasok_beban_id_real'][$i];
                if ($txt_alokasi_ke_barang == 0) { //kosong alias 0
                    $arrayDebit[] = [ // beban dengan kondisi 1* => debit
                        'nominal' => $data['amount_beban'][$i],
                        'tanggal' => $data['invoice_date'],
                        'dari' => 'Beban Faktur Pembelian Debit',
                        'akun' => $data['akun_beban_id'][$i],
                    ];
                    $bebanNotAlokasi = $data['amount_beban'][$i];
                    $arrayKredit[] = [ // Sub Total => kredit
                        'nominal' => $bebanNotAlokasi,
                        'tanggal' => $data['invoice_date'],
                        'dari' => 'Total yg ke akun hutang',
                        'akun' => $data['akun_hutang_id'],
                    ];
                } elseif ($txt_alokasi_ke_barang == 1 && $pemasok_beban_id === null) {
                    // tambahkan nilai ke masing2 barang
                    // dengan catatan pemasok kosong && dia dialokasikan kebarang
                    // beban dengan kondisi ke 2* => debit (lsng masuk ke barang)
                    $bebanNotAlokasi = $data['amount_beban'][$i];
                    $nilaiBeban = $data['amount_beban'][$i] / collect($data['produk_id'])->count();
                    $totalNilaiBeban += $data['amount_beban'][$i];
                    $totalNilaiBebanForPengiriman += $nilaiBeban;
                    foreach ($data['produk_id'] as $key => $bt) {
                        // $arrayBelumTertagih['nominal'] += $nilaiBeban;
                        if ($penerimaan->isEmpty()) {
                            $arrayDebit[] = [ // akun belum persediaan => debit
                                'nominal' => $nilaiBeban,
                                'tanggal' => $data['invoice_date'],
                                'dari' => 'Total Belum Tertagih Fakur Pembelian',
                                'produk' => $bt,
                                'akun' => $detailProdukArr[$key]['akun_persedian_id'],
                            ];
                        } else {
                            $arrayDebit[] = [ // akun belum tertagih => debit
                                'nominal' => $nilaiBeban,
                                'tanggal' => $data['invoice_date'],
                                'dari' => 'Total Belum Tertagih Fakur Pembelian',
                                'produk' => $bt,
                                'akun' => $detailProdukArr[$key]['akun_belum_tertagih_id'],
                            ];
                        }
                    }
                    $arrayKredit[] = [ // Sub Total => kredit
                        'nominal' => $bebanNotAlokasi,
                        'tanggal' => $data['invoice_date'],
                        'dari' => 'Total yg ke akun hutang',
                        'akun' => $data['akun_hutang_id'],
                        'id' => $data['no_faktur'],
                    ];
                } elseif ($txt_alokasi_ke_barang == 1 && $pemasok_beban_id != null) {
                    $arrayKredit[] = [ // beban dengan kondisi 3* => kredit
                        'nominal' => $data['amount_beban'][$i],
                        'tanggal' => $data['invoice_date'],
                        'dari' => 'Nilai Beban Faktur Pembelian Kondisi ke 3 Kredit',
                        'akun' => $data['akun_beban_id'][$i],
                    ];
                    $nilaiBeban = $data['amount_beban'][$i] / collect($data['produk_id'])->count();
                    $totalNilaiBeban += $data['amount_beban'][$i];
                    $totalNilaiBebanForPengiriman += $nilaiBeban;
                    foreach ($data['produk_id'] as $key => $bt) {
                        // $arrayBelumTertagih['nominal'] += $nilaiBeban;
                        if ($penerimaan->isEmpty()) {
                            // dd('a');
                            $arrayDebit[] = [ // akun belum persediaan => debit
                                'nominal' => $nilaiBeban,
                                'tanggal' => $data['invoice_date'],
                                'dari' => 'Total Belum Tertagih Fakur Pembelian',
                                'produk' => $bt,
                                'akun' => $detailProdukArr[$key]['akun_persedian_id'],
                            ];
                        } else {
                            // dd($nilaiBeban);
                            $arrayDebit[] = [ // akun belum tertagih => debit
                                'nominal' => $nilaiBeban,
                                'tanggal' => $data['invoice_date'],
                                'dari' => 'Total Belum Tertagih Fakur Pembelian',
                                'produk' => $bt,
                                'akun' => $detailProdukArr[$key]['akun_belum_tertagih_id'],
                            ];
                        }
                    }
                    $bebanNotAlokasi = 0;
                }
                // dd('stop');
                // Insert Beban
                $pemasok_id = (empty($data['pemasok_id'][$i])) ? null : $data['pemasok_id'][$i];
                $arrayBeban[] = [
                    'akun_beban_id' => $data['akun_beban_id'][$i],
                    'faktur_pembelian_id' => $model->id,
                    'amount' => $data['amount_beban'][$i],
                    'notes' => $data['notes_beban'][$i],
                    'alokasi_ke_barang' => $data['txt_alokasi_ke_barang'][$i],
                    'pemasok_id' => $pemasok_id,
                    'created_at' => $jam_sekarang,
                    'updated_at' => $jam_sekarang,
                ];
            }
            BebanFakturPembelian::insert($arrayBeban);
            $arrayKredit[] = [ // Sub Total => kredit
                'nominal' => $data['subtotal'] - $data['total_diskon_faktur'],
                'tanggal' => $data['invoice_date'],
                'dari' => 'Total yg ke akun hutang',
                'akun' => $data['akun_hutang_id'],
            ];
        } else {
            $arrayKredit[] = [ // Sub Total => kredit
                'nominal' => $data['subtotal'] - $data['total_diskon_faktur'] - round($data['total_pajak'], 2),
                'tanggal' => $data['invoice_date'],
                'dari' => 'Sub total dikurang diskon faktur',
                'akun' => $data['akun_hutang_id'],
            ];
        }

        foreach ($data['produk_id'] as $i => $produkID) {
            $pajak1 = null;
            $pajak2 = null;
            $nilai_pajak_barang     = !empty($data['in_tax']) ? $nilai_pajak_akhir[$i]['sum_pajak'] : 0;
            $barang                 = $data['unit_harga_produk'][$i] * $data['qty_produk'][$i];
            $total_diskon           = (($barang * $data['diskon_produk'][$i]) / 100);
            $persen_barang          = $total_amount == 0 ? $data['amount_produk'][$i] : $data['amount_produk'][$i] / $total_amount;
            $total_diskon_master    = ($data['total_diskon_faktur'] * $persen_barang);
            $total_barang           = $barang - $total_diskon - $total_diskon_master;
            $total_pembagian_beban  = $totalNilaiBeban * $persen_barang;
            $total_barang_beban     = ($total_barang - $nilai_pajak_barang) + $total_pembagian_beban;
            $nilai_pajak_harga_modal = !empty($data['in_tax']) ? ($nilai_pajak_akhir[$i]['sum_pajak'] / $data['qty_produk'][$i]) : 0;

            // $produk_detail           = SaldoAwalBarang::where(['produk_id' => $data['produk_id'][$i], 'status_transaksi' => 2, 'status_transaksi' => 3])->orderBy('id', 'desc')->first();
            // $produk_detail_kuantitas = SaldoAwalBarang::where(['produk_id' => $data['produk_id'][$i]])->orderBy('id', 'desc')->get()->sum('kuantitas');
            // $produk_detail_modal = SaldoAwalBarang::where(['produk_id' => $data['produk_id'][$i]])->orderBy('id', 'desc')->get();
            // if ($serviceProduct->contains($produkID)) {
            //     $harga_terakhir = 0;
            //     $harga_modal = 0;
            // } else {
            //     $modal = $produk_detail_modal->first()->harga_modal;
            //     $sumQTy = $produk_detail_kuantitas;
            //     $harga_terakhir = $total_barang_beban / $data['qty_produk'][$i];
            //     $harga_modal_1 = $total_barang_beban + ($modal * $sumQTy);
            //     $harga_modal = ($harga_modal_1 / ($data['qty_produk'][$i] + $sumQTy));
            // }

            if (!empty($data['kode_pajak_id'][$i]) && !empty($data['taxable'])) {
                if (!empty($data['in_tax'])) {
                    $total_pajak1 = $nilai_pajak_akhir[$i]['pajak_1']; // res : float
                } else {
                    $total_pajak1 = ($total_barang * $nilai_pajak_akhir[$i]['nilai_pajak_1']->nilai) / 100; // res : float
                }

                $sumPajak1 += $total_pajak1;

                $arrayDebit[] = [ // pajak debit
                    'nominal'   => $total_pajak1,
                    'tanggal'   => $data['invoice_date'],
                    'produk'    => $data['produk_id'][$i],
                    'dari'      => 'Pajak'.$nilai_pajak_akhir[$i]['nilai_pajak_1']->akun_pajak_pembelian_id.'per item Fakur Pembelian Debit',
                    'akun'      => $nilai_pajak_akhir[$i]['nilai_pajak_1']->akun_pajak_pembelian_id,
                ];
                if (collect($data['kode_pajak_id'][$i])->count() > 1) {
                    if (!empty($data['in_tax'])) {
                        $total_pajak2 = $nilai_pajak_akhir[$i]['pajak_2']; // res : float
                    } else {
                        $total_pajak2 = ($total_barang * $nilai_pajak_akhir[$i]['nilai_pajak_2']->nilai) / 100; // res : float
                    }
                    $sumPajak += $total_pajak2;

                    $arrayDebit[] = [ // pajak debit
                        'nominal'   => $total_pajak2,
                        'tanggal'   => $data['invoice_date'],
                        'produk'    => $data['produk_id'][$i],
                        'dari'      => 'Pajak'.$nilai_pajak_akhir[$i]['nilai_pajak_2']->akun_pajak_pembelian_id.'per item Fakur Pembelian Debit',
                        'akun'      => $nilai_pajak_akhir[$i]['nilai_pajak_2']->akun_pajak_pembelian_id,
                    ];

                    $arrayKredit[] = [ // pajak kredit
                        'nominal'   => $total_pajak2,
                        'tanggal'   => $data['invoice_date'],
                        'produk'    => $data['produk_id'][$i],
                        'dari'      => 'Pajak per item Fakur pajak2 Pembelian Kredit',
                        'akun'      => $data['akun_hutang_id'],
                    ];
                }
            }

            $items[] = [
                'produk_id' => $data['produk_id'][$i],
                'faktur_pembelian_id' => $model->id,
                'item_deskripsi' => $data['keterangan_produk'][$i],
                'item_unit' => $data['satuan_produk'][$i],
                'jumlah' => $data['qty_produk'][$i],
                'barang_penerimaan_pembelian_id' => !empty($data['barang_penerimaan_pembelian_id']) ? $data['barang_penerimaan_pembelian_id'][$i] : null,
                'unit_price' => $data['unit_harga_produk'][$i],
                'harga_final' => $total_barang_beban / $data['qty_produk'][$i],
                'bagian_diskon_master' => $total_diskon_master,
                'diskon' => $data['diskon_produk'][$i],
                'harga_modal' => $data['amount_old_modal_produk'][$i],
                'gudang_id' => !empty($data['gudang_id']) ? $data['gudang_id'][$i] : null,
                'expired_date' => $data['expired_date'][$i] = (empty($data['expired_date'][$i])) ? $data['expired_date'][$i] = date('Y-m-d H:i:s') : $data['expired_date'][$i] = $this->date->IndonesiaToSql($data['expired_date'][$i]),
                'kode_pajak_id' => $nilai_pajak_akhir[$i]['nilai_pajak_1']->id ?? null,
                'kode_pajak_2_id' => $nilai_pajak_akhir[$i]['nilai_pajak_2']->id ?? null,
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];

            // Transaksi per item (Fix this later)
            if (!$serviceProduct->contains($produkID)) {
                if (!$penerimaan->isEmpty()) {
                    $arrayBelumTertagih = [ // akun belum tertagih => debit
                        'nominal' => $total_barang_beban,
                        'tanggal' => $data['invoice_date'],
                        'produk' => $data['produk_id'][$i],
                        'dari' => 'Total Belum Tertagih Fakur Pembelian',
                        'akun' => $detailProdukArr[$i]['akun_belum_tertagih_id'],
                    ];
                    $blmTertagih = $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrayBelumTertagih, 1, $data['no_faktur']);
                }
            }

            if ($penerimaan->count() < 1) {
                // ga ono.. ya masuk debit
                $cek_transaksi = Transaksi::withTrashed()->where(['item_id' => $receive->id, 'item_type' => get_class($this->penerimaan)])->first();
                $produk_detail_kuantitas = SaldoAwalBarang::where('produk_id', $data['produk_id'][$i])
                    ->whereHas('transaksi', function ($query) use ($penerimaan) {
                        $query->withTrashed();
                        $query->whereNotIn('item_id', $penerimaan->pluck('id'));
                    })->where('id', '<', $cek_transaksi->id)->orderBy('id', 'desc')->get()->sum('kuantitas');
                $produk_detail_modal = $this->barang_penerimaan->where('id', $receive->id)->pluck('harga_modal');
                // dd($produk_detail_kuantitas);
                if ($serviceProduct->contains($produkID)) {
                    $harga_terakhir = 0;
                    $harga_modal = 0;
                } else {
                    $modal = $produk_detail_modal->first() ?? 0;
                    $sumQTy = $produk_detail_kuantitas < 0 ? 0 : $produk_detail_kuantitas;
                    // dd($modal, $cek_transaksi_receive->id);
                }
                $harga_terakhir = $total_barang_beban / $data['qty_produk'][$i];
                $harga_modal_1 = $total_barang_beban + ($modal * $sumQTy);
                $harga_modal = ($harga_modal_1 / ($data['qty_produk'][$i] + $sumQTy));
                // dd($harga_modal);
                $arrayBelumTertagih = [
                    'nominal' => $total_barang_beban, //awalnya cuma $total_barang di ganti $total_barang_beban
                    'tanggal' => $data['invoice_date'],
                    'produk' => $data['produk_id'][$i],
                    'dari' => 'Total Belum Tertagih Fakur Pembelian',
                    'akun' => $detailProdukArr[$i]['akun_persedian_id'],
                ];
                $blmTertagih = $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrayBelumTertagih, 1, $data['no_faktur']);
            } else {
                // ada !! hapus dlu yg lama dan masukkin yg baru
                foreach ($penerimaan as $receive) {
                    $cek_transaksi_receive = Transaksi::withTrashed()->where(['item_id' => $receive->id, 'item_type' => get_class($this->penerimaan), 'produk_id' => $data['produk_id'][$i]])->orderBy('id', 'desc')->first();
                    if (!empty($cek_transaksi_receive)) {
                        if ($cek_transaksi_receive->desc_dev == 'PenerimaanBarang |'.$receive->id) {
                            // Ini Kemarin Masalah Harga Modal Diserver Dan Local Berbeda Pada Malam Tahun Baru Yang Besoknya UAT INTERNAL dan Besoknya Lagi UAT Di Tempat Mas Eky
                            // $produk_detail_kuantitas = SaldoAwalBarang::where('produk_id', $data['produk_id'][$i])
                            //     ->whereHas('transaksi', function ($query) use ($penerimaan) {
                            //         $query->withTrashed();
                            //         $query->whereNotIn('item_id', $penerimaan->pluck('id'));
                            //     })->orderBy('id', 'desc')->get()->sum('kuantitas');

                            // $produk_detail_modal = SaldoAwalBarang::where('produk_id', $data['produk_id'][$i])
                            //     ->whereHas('transaksi', function ($query) use ($penerimaan) {
                            //         $query->withTrashed();
                            //         $query->whereNotIn('item_id', $penerimaan->pluck('id'));
                            //     })->orderBy('id', 'desc')->pluck('harga_modal');

                            $produk_detail_kuantitas = SaldoAwalBarang::where('produk_id', $data['produk_id'][$i])
                                ->whereHas('transaksi')->orderBy('id', 'desc')->get()->sum('kuantitas');

                            $produk_detail_modal = SaldoAwalBarang::where('produk_id', $data['produk_id'][$i])
                                ->whereHas('transaksi')->orderBy('id', 'desc')->pluck('harga_modal');

                            // $produk_detail_modal = $this->barang_penerimaan->where('id', $receive->id)->pluck('harga_modal');

                            if ($serviceProduct->contains($produkID)) {
                                $harga_terakhir = 0;
                                $harga_modal = 0;
                            } else {
                                $modal = $produk_detail_modal->first() ?? 0;
                                $sumQTy = $produk_detail_kuantitas < 0 ? 0 : $produk_detail_kuantitas;
                            }

                            // dd($totalNilaiBeban, $persen_barang, $data['qty_produk'][$i]);
                            $total_pembagian_beban = ($totalNilaiBeban * $persen_barang) / $data['qty_produk'][$i];
                            $barang = $data['unit_harga_produk'][$i];
                            $total_diskon_master = ($data['total_diskon_faktur'] * $persen_barang) / $data['qty_produk'][$i];
                            $total_diskon = (($barang * $data['diskon_produk'][$i]) / 100);
                            $persen_barang = $total_amount == 0 ? $data['amount_produk'][$i] : $data['amount_produk'][$i] / $total_amount;
                            $total_barang = $barang - $total_diskon - $total_diskon_master;
                            foreach ($receive->barang as $itemPenerimaan) {
                                if ($itemPenerimaan->produk_id == $data['produk_id'][$i]) {
                                    $total_barang_beban = ($total_barang - $nilai_pajak_harga_modal  + $total_pembagian_beban) * $itemPenerimaan->jumlah;
                                    $harga_modal_1 = $total_barang_beban + ($modal * $sumQTy);
                                    $harga_terakhir = $total_barang_beban / $itemPenerimaan->jumlah;
                                    $harga_modal = ($harga_modal_1 / ($itemPenerimaan->jumlah + $sumQTy));
                                }
                                // dd($itemPenerimaan->jumlah);
                            }
                        } else {
                            $cek_transaksi = Transaksi::withTrashed()->where(['item_id' => $receive->id, 'item_type' => get_class($this->penerimaan), 'produk_id' => $data['produk_id'][$i]])->first();
                            $produk_detail_kuantitas = SaldoAwalBarang::where('produk_id', $data['produk_id'][$i])
                                ->whereHas('transaksi', function ($query) use ($penerimaan) {
                                    $query->withTrashed();
                                    $query->whereNotIn('item_id', $penerimaan->pluck('id'));
                                })->where('id', '<', $cek_transaksi->id)->orderBy('id', 'desc')->get()->sum('kuantitas');
                            $produk_detail_modal = SaldoAwalBarang::where('produk_id', $data['produk_id'][$i])
                                ->whereHas('transaksi', function ($query) use ($penerimaan) {
                                    $query->withTrashed();
                                    $query->whereNotIn('item_id', $penerimaan->pluck('id'));
                                })->orderBy('id', 'desc')->pluck('harga_modal');
                            // $produk_detail_modal = $this->barang_penerimaan->where('id', $receive->id)->orderBy('id', 'desc')->pluck('harga_modal');
                            // dd($produk_detail_kuantitas);
                            if ($serviceProduct->contains($produkID)) {
                                $harga_terakhir = 0;
                                $harga_modal = 0;
                            } else {
                                $modal = $produk_detail_modal->first() ?? 0;
                                $sumQTy = $produk_detail_kuantitas < 0 ? 0 : $produk_detail_kuantitas;
                                // dd($modal, $cek_transaksi_receive->id);
                            }
                            // cari di faktur
                            $old_transaction = Transaksi::withTrashed()
                                    ->where(['item_id' => $receive->id, 'item_type' => get_class($this->penerimaan), 'produk_id' => $data['produk_id']])
                                    ->where('desc_dev', 'LIKE', '%'.$receive->id)
                                    ->where('desc_dev', 'LIKE', 'FakturPembelian |%')
                                    ->where('status', 1)->get();
                            $harga_total = 0;
                            $jumlah_total = 0;
                            foreach ($old_transaction as $ot) {
                                $array = explode('|', $ot->desc_dev);
                                $fp = FakturPembelian::find($array[1]); //id faktur

                                if (!empty($fp)) {
                                    foreach ($fp->barang as $barangs) {
                                        if ($data['produk_id'][$i] == $barangs->produk_id) {
                                            $harga_total += $barangs->jumlah * $barangs->harga_final;
                                            $jumlah_total += $barangs->jumlah;
                                        }
                                    }
                                }
                            }

                            $temp = ($total_barang_beban + $harga_total) / ($jumlah_total + $data['qty_produk'][$i]);
                            foreach ($receive->barang as $itemPenerimaan) {
                                if ($itemPenerimaan->produk_id == $data['produk_id'][$i]) {
                                    $total_barang_beban = $temp * $itemPenerimaan->jumlah;
                                    $harga_modal_1 = $total_barang_beban + ($modal * $sumQTy);
                                    $harga_terakhir = $total_barang_beban / $itemPenerimaan->jumlah;
                                    $harga_modal = ($harga_modal_1 / ($itemPenerimaan->jumlah + $sumQTy));
                                }
                            }
                            // dd($total_barang_beban);
                            // dd($total_barang_beban, $harga_total, $jumlah_total, $data['qty_produk'][$i], $temp, $modal, $sumQTy);
                        }
                    }
                    // dd($modal, $harga_terakhir, $harga_modal_1, $total_barang_beban, $sumQTy);
                    if ($totalNilaiBeban > 0) {
                        $arrDebit = [
                            'nominal' => $total_barang_beban,
                            'tanggal' => $data['invoice_date'],
                            'produk' => $data['produk_id'][$i],
                            'dari' => 'Persediaan Fakur Pembelian',
                            'desc' => 'FakturPembelian |'.$model->id.'|'.$receive->id,
                            // dari faktur ... dan ini untuk penerimaan ...
                            'akun' => $detailProdukArr[$i]['akun_persedian_id'],
                        ];

                        $arrKredit = [
                            'nominal' => $total_barang_beban,
                            'tanggal' => $data['invoice_date'],
                            'produk' => $data['produk_id'][$i],
                            'dari' => 'Total Belum Tertagih Fakur Pembelian',
                            'desc' => 'FakturPembelian |'.$model->id.'|'.$receive->id,
                            'akun' => $detailProdukArr[$i]['akun_belum_tertagih_id'],
                        ];
                    } else {
                        $arrDebit = [
                            'nominal' => $total_barang_beban,
                            'tanggal' => $data['invoice_date'],
                            'produk' => $data['produk_id'][$i],
                            'dari' => 'Persediaan Fakur Pembelian',
                            'desc' => 'FakturPembelian |'.$model->id.'|'.$receive->id,
                            'akun' => $detailProdukArr[$i]['akun_persedian_id'],
                        ];

                        $arrKredit = [
                            'nominal' => $total_barang_beban,
                            'tanggal' => $data['invoice_date'],
                            'produk' => $data['produk_id'][$i],
                            'dari' => 'Total Belum Tertagih Fakur Pembelian',
                            'desc' => 'FakturPembelian |'.$model->id.'|'.$receive->id,
                            'akun' => $detailProdukArr[$i]['akun_belum_tertagih_id'],
                        ];
                    }
                    $dataTransaksiSaldo = $this->transaksi->ExecuteTransactionSingle('insert', $receive, $arrDebit, 1, $receive->receipt_no);
                    $this->transaksi->ExecuteTransactionSingle('insert', $receive, $arrKredit, 0, $receive->receipt_no);
                    
                    SaldoAwalBarang::where([
                        'item_type' => get_class($this->penerimaan),
                        'item_id' => $receive->id,
                        'no_faktur' => $receive->receipt_no,
                        'produk_id' => $dataTransaksiSaldo->produk_id
                    ])->update(['transaksi_id' => $dataTransaksiSaldo->id]);

                    foreach ($receive->barang as $rb) {
                        $this->syncPenjualan($rb, $data, $i, $model, $harga_modal);
                    }
                }
            }
            if (!$serviceProduct->contains($produkID)) {
                $produkDetail[] = [
                    'no_faktur' => $data['form_no'],
                    'tanggal' => $data['receive_date'] !== null ? $data['receive_date'] : $data['invoice_date'],
                    'kuantitas' => 0,
                    'harga_modal' => $harga_modal,
                    'harga_terakhir' => $harga_terakhir,
                    'gudang_id' => !empty($data['gudang_id']) ? $data['gudang_id'][$i] : null,
                    'transaksi_id' => $blmTertagih->id,
                    'produk_id' => $data['produk_id'][$i],
                    'sn' => $data['sn'][$i],
                    'status' => 1,
                    'item_type' => get_class(new FakturPembelian()),
                    'item_id'   => $model->id,
                    'created_at' => $jam_sekarang,
                ];
            }
        }
        BarangFakturPembelian::insert($items);

        // save dp ke transaksi uang muka pemasok
        if (!empty($data['faktur_dp_id'])) {
            //cek apakah DPnya cukup? ato kelebihan?
            $this->cekDPpemasok($count_dp, $data);

            $sumPajakDp = 0;
            for ($i = 0; $i < $count_dp; ++$i) {
                $in_tax = (empty($data['in_tax_dp'][$i])) ? 0 : $data['in_tax_dp'][$i];

                // Manyimpan kode pajak dan nilai pajak
                $pajak_dp1 = null;
                $pajak_dp2 = null;
                $total_pajak_dp1 = null;
                $total_pajak_dp2 = null;
                if (!empty($data['pajak_dp'][$i])) {
                    // data ada.. walau hanya satu
                    if (!empty($data['pajak_dp'][$i][0])) {
                        $pajak_dp1 = $data['pajak_dp'][$i][0];
                        $data_pajak_dp1 = KodePajak::find($pajak_dp1);
                        if (!empty($data['in_tax'])) {
                            $total_pajak_dp1 = ($data['total_dp'][$i] * $data_pajak_dp1->nilai) / 100 + $data_pajak_dp1->nilai;
                        } else {
                            $total_pajak_dp1 = ($data['total_dp'][$i] * $data_pajak_dp1->nilai) / 100;
                        }
                        $sumPajakDp += $total_pajak_dp1;
                    }
                    if (collect($data['pajak_dp'][$i])->count() > 1) {
                        if (!empty($data['pajak_dp'][$i][1])) {
                            $pajak_dp2 = $data['pajak_dp'][$i][1];
                            $data_pajak_dp2 = KodePajak::find($pajak_dp2);
                            if (!empty($data['in_tax'])) {
                                $total_pajak_dp2 = ($data['total_dp'][$i] * $data_pajak_dp2->nilai) / 100 + $data_pajak_dp2->nilai;
                            } else {
                                $total_pajak_dp2 = ($data['total_dp'][$i] * $data_pajak_dp2->nilai) / 100;
                            }
                            $sumPajakDp += $total_pajak_dp2;
                        }
                    }
                }
                $dp[] = [
                    'faktur_uang_muka_pemasok_id' => $data['faktur_dp_id'][$i],
                    'faktur_pembelian_id' => $model->id,
                    'jumlah' => $data['total_dp'][$i],
                    'pajak_satu_id' => $pajak_dp1,
                    'pajak_dua_id' => $pajak_dp2,
                    'total_pajak1' => $total_pajak_dp1,
                    'total_pajak2' => $total_pajak_dp2,
                    'created_at' => $jam_sekarang,
                    'updated_at' => $jam_sekarang,
                ];
                $arrayKredit[] = [ // Uang muka per item => kredit
                    'nominal' => $data['total_dp'][$i],
                    'tanggal' => $data['invoice_date'],
                    'dari' => 'Total DP per item',
                    'akun' => $data['akun_dp_id'][$i],
                ];
            }
            TransaksiUangMukaPemasok::insert($dp);

            // $arrayDebit[] = [ // Uang Muka (Gak per item) => debit
            //     'nominal' => array_sum($data['total_dp']),
            //     'dari' => 'Total DP',
            //     'akun' => $data['akun_hutang_id'],
            // ];
        }

        $cekBalanceDP = 0 - $sumPajak1;

        if (!empty($data['total_dp'])) {
            if (array_sum($data['total_dp'])) {
                $cekBalanceDP = array_sum($data['total_dp']) - $sumPajak1;
            }
        }

        if ($cekBalanceDP < 0) {
            $arrayKredit[] = [
                'nominal' => abs($cekBalanceDP),
                'tanggal' => $data['invoice_date'],
                'dari' => 'Hasil Pengurangan Pajak',
                'akun' => $data['akun_hutang_id'],
            ];
        } else {
            $arrayDebit[] = [
                'nominal' => abs($cekBalanceDP),
                'tanggal' => $data['invoice_date'],
                'dari' => 'Hasil Pengurangan Pajak',
                'akun' => $data['akun_hutang_id'],
            ];
        }

        if (!empty($produkDetail)) {
            SaldoAwalBarang::insert($produkDetail);
        }

        // masukkan ke array debit
        // foreach($arrayBelumTertagih as $bt){
        // $arrayDebit[] = $arrayBelumTertagih;
        // }
        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayDebit, 1, $data['no_faktur']);
        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayKredit, 0, $data['no_faktur']);
    }

    public function deleteSubItem($model, $faktur_id = null)
    {
        $getTransaksi = Transaksi::where([
            'item_id' => $model->id,
            'item_type' => get_class($model),
            // 'dari' => 'Total Belum Tertagih Fakur Pembelian',
        ])->pluck('id');

        foreach ($getTransaksi as $gt) {
            // mencari hasil similiar
            SaldoAwalBarang::where([
                'status' => 1,
                'item_type' => get_class(new FakturPembelian()),
                'transaksi_id' => $gt, ])->delete();
        }
        // current transaction
        $getOldTransactionPenerimaan = Transaksi::withTrashed()->select('desc_dev', 'id')->where('desc_dev', 'Like', 'FakturPembelian |'.$model->id.'|%')->orderBy('id', 'desc');
        $penerimaan_id = [];
        $faktur_pembelian_id = [];
        foreach ($getOldTransactionPenerimaan->get() as $receive) {
            $array = explode('|', $receive->desc_dev);
            array_push($penerimaan_id, end($array));
            Transaksi::withTrashed()->find($receive->id)->update(['desc_dev' => 'BekasFakturPembelian |'.$array[1].'|'.end($array)]);
            // Transaksi::withTrashed()->find($receive->id)->delete();
        }
        //restore old transaction. but check it first, does the old shit is not deleted yet?;
        foreach (array_unique($penerimaan_id) as $id) {
            $total_barang = 0;
            $faktur = Transaksi::withTrashed()
                ->where('desc_dev', 'LIKE', '%'.$id)
                ->where('desc_dev', 'LIKE', 'FakturPembelian |%')->orderBy('id', 'desc')->get();
            $bekas = Transaksi::withTrashed()
                ->where('desc_dev', 'LIKE', '%'.$id)
                ->where('desc_dev', 'LIKE', 'BekasFakturPembelian |%')->orderBy('id', 'desc')->get();
            // loop barang penerimaannya & sesuaikan dengan bekas & faktur
            $penerimaan = PenerimaanPembelian::find($id);

            foreach ($penerimaan->barang as $item) {
                $harga_total_faktur = 0;
                $harga_total_bekas = 0;
                $jumlah_total_faktur = 0;
                $jumlah_total_bekas = 0;

                foreach ($faktur as $invoice) {
                    $array = explode('|', $invoice->desc_dev);
                    $fp = FakturPembelian::withTrashed()->find($array[1]); //id faktur

                    foreach ($fp->barang as $fp_item) {
                        // jika itemnya sama, maka ...
                        if ($fp_item->produk_id == $item->produk_id &&
                        $fp_item->barang_penerimaan_pembelian_id == $item->id) {
                            $harga_total_faktur += $fp_item->harga_final * $fp_item->jumlah;
                            $jumlah_total_faktur += $fp_item->jumlah;
                        }
                    }
                }

                foreach ($bekas as $invoice) {
                    $array = explode('|', $invoice->desc_dev);
                    $fp = FakturPembelian::withTrashed()->find($array[1]); //id faktur

                    foreach ($fp->barang as $fp_item) {
                        // jika itemnya sama, maka ...
                        if ($fp_item->produk_id == $item->produk_id &&
                        $fp_item->barang_penerimaan_pembelian_id == $item->id) {
                            $harga_total_bekas += $fp_item->harga_final * $fp_item->jumlah;
                            $jumlah_total_bekas += $fp_item->jumlah;
                        }
                    }
                }
                // dd($jumlah_total_bekas);
                if ($harga_total_faktur > 0) {
                    $temp_faktur = ($harga_total_faktur / $jumlah_total_faktur) + ($harga_total_bekas / $jumlah_total_bekas);
                    $temp_bekas = ($harga_total_bekas / $jumlah_total_bekas);

                    $total_barang = ($temp_faktur - $temp_bekas) * $item->jumlah;
                    // buat transaksi baru;
                    // dd($total_barang, 'a');
                    Transaksi::withTrashed()
                    ->where('desc_dev', 'LIKE', '%'.$id)
                    ->orderBy('id', 'desc')->update(['nominal' => $total_barang]);
                } else {
                    // faktur yang masih hidup udah habis.. jadi di balikkan ke pengiriman
                    // $total_barang = ($item->jumlah * $item->harga_modal) * $item->jumlah;
                    // dd($total_barang, 'b');
                    // Transaksi::withTrashed()
                    // ->where('desc_dev', 'LIKE', '%'.$id)
                    // ->orderBy('id', 'desc')->update(['nominal' => $total_barang]);
                }

                Transaksi::withTrashed()
                    ->where('desc_dev', 'LIKE', '%'.$id)
                    ->where('status', 0)
                    ->orderBy('id', 'desc')->first()->restore();

                Transaksi::withTrashed()
                    ->where('desc_dev', 'LIKE', '%'.$id)
                    ->where('status', 1)
                    ->orderBy('id', 'desc')->first()->restore();

                /*
                 * Section Update Produk Detail
                 */
                $this->syncPenjualan($item, null, null, $model, null);
            }
        }

        // foreach($old_transaction as $ot){
        //     $array = explode("|",$ot->desc_dev);
        //     $fp = FakturPembelian::find($array[1]); //id faktur
        //     if(!empty($fp) && $ot->deleted_at != null){
        //         Transaksi::withTrashed()
        //         ->where('desc_dev', "FakturPembelian |".$fp->id."|".$id)->restore(); //memastikan ulang
        //     }
        // }

        $this->transaksi->recalculateTransaction($model, $this->barang);

        $this->transaksi->ExecuteTransactionSingle('delete', $model);
        BarangFakturPembelian::where('faktur_pembelian_id', $model->id)->delete();
        TransaksiUangMukaPemasok::where('faktur_pembelian_id', $model->id)->delete();
        BebanFakturPembelian::where('faktur_pembelian_id', $model->id)->delete();
    }

    /**
     * @param array barang penerimaan pembelian
     * @param array data utama
     * @param int parameter
     * @param collection
     */
    public function syncPenjualan($item, $data = null, $i, $model, $harga_modal)
    {
        $bantuan = BantuanPenerimaanPembelian::where([
            'produk_id' => $item->produk_id,
            'brg_penerimaan_pembelian_id' => $item->id,
        ]);
        $brg_faktur = BarangFakturPembelian::where([
            'produk_id' => $item->produk_id,
            'barang_penerimaan_pembelian_id' => $item->id,
        ])->sum('jumlah');

        // jumlahkan semuanya. dan compare
        $currentQty = ($data != null) ? $data['qty_produk'][$i] : 0;
        if ($item->jumlah <= $brg_faktur + $currentQty) {
            if (!empty($bantuan->get())) {
                $bantuan->delete();
            }
        }

        if (!empty($bantuan)) {
            // Result : get item bantuan penerimaan pembelian
            foreach ($bantuan->get() as $bpp) {
                // dd($bpp);       // Result : get item bantuan pengiriman penjualan
                foreach ($bpp->bantuPengirimanPenjualan as $bantuPenjualan) {
                    // Cari Barang Pengiriman Penjualan
                    $brg_pengiriman_penj = BarangPengirimanPenjualan::find($bantuPenjualan->brg_pengiriman_penjualan_id)
                    ->where('produk_id', $item->produk_id);

                    // Cari barang faktur penjualan
                    $brg_faktur_penj = BarangFakturPenjualan::where([
                        'barang_pengiriman_penjualan_id' => $bantuPenjualan->brg_pengiriman_penjualan_id,
                        'produk_id' => $item->produk_id,
                    ]);

                    // update transaksi barang pengiriman penjualan
                    // foreach ($brg_pengiriman_penj->get() as $item_pengiriman) {
                    //     $nominal = $item_pengiriman->jumlah * $harga_modal;
                    //     // dd($nominal, $harga_modal, $item_pengiriman->jumlah);
                    //     Transaksi::where([
                    //         'item_id' => $item_pengiriman->pengiriman_penjualan_id,
                    //         'item_type' => get_class(new PengirimanPenjualan()),
                    //         'produk_id' => $data['produk_id'][$i],
                    //         'akun_id' => $detailProdukArr[$i]['akun_barang_terkirim_id'],
                    //     ])->update(['nominal' => $nominal]);

                    //     Transaksi::where([
                    //         'item_id' => $item_pengiriman->pengiriman_penjualan_id,
                    //         'item_type' => get_class(new PengirimanPenjualan()),
                    //         'produk_id' => $data['produk_id'][$i],
                    //         'akun_id' => $detailProdukArr[$i]['akun_persedian_id'],
                    //     ])->update(['nominal' => $nominal]);
                    // }

                    // // update transaksi barang faktur penjualan
                    // foreach ($brg_faktur_penj->get() as $item_faktur) {
                    //     $nominal = $item_faktur->jumlah * $harga_modal;

                    //     Transaksi::where([
                    //         'item_id' => $item_faktur->faktur_penjualan_id,
                    //         'item_type' => get_class(new FakturPenjualan()),
                    //         'produk_id' => $data['produk_id'][$i],
                    //         'akun_id' => $detailProdukArr[$i]['akun_hpp_id'],
                    //     ])->update(['nominal' => $nominal]);

                    //     Transaksi::where([
                    //         'item_id' => $item_faktur->faktur_penjualan_id,
                    //         'item_type' => get_class(new FakturPenjualan()),
                    //         'produk_id' => $data['produk_id'][$i],
                    //         'akun_id' => $detailProdukArr[$i]['akun_barang_terkirim_id'],
                    //     ])->update(['nominal' => $nominal]);
                    // }

                    foreach ($brg_pengiriman_penj->get() as $item_pengiriman) {
                        $nominal = $item_pengiriman->jumlah * $harga_modal;
                        // dd($nominal, $harga_modal, $item_pengiriman->jumlah);
                        Transaksi::where([
                            'item_id' => $item_pengiriman->pengiriman_penjualan_id,
                            'item_type' => get_class(new PengirimanPenjualan()),
                            'produk_id' => $item_pengiriman->produk_id,
                            'akun_id' => $item_pengiriman->produk->akun_barang_terkirim_id,
                        ])->update(['nominal' => $nominal]);

                        Transaksi::where([
                            'item_id' => $item_pengiriman->pengiriman_penjualan_id,
                            'item_type' => get_class(new PengirimanPenjualan()),
                            'produk_id' => $item_pengiriman->produk_id,
                            'akun_id' => $item_pengiriman->produk->akun_persedian_id,
                        ])->update(['nominal' => $nominal]);
                    }

                    // update transaksi barang faktur penjualan
                    foreach ($brg_faktur_penj->get() as $item_faktur) {
                        $nominal = $item_faktur->jumlah * $harga_modal;

                        Transaksi::where([
                            'item_id' => $item_faktur->faktur_penjualan_id,
                            'item_type' => get_class(new FakturPenjualan()),
                            'produk_id' => $item_faktur->produk_id,
                            'akun_id' => $item_faktur->produk->akun_hpp_id,
                        ])->update(['nominal' => $nominal]);

                        Transaksi::where([
                            'item_id' => $item_faktur->faktur_penjualan_id,
                            'item_type' => get_class(new FakturPenjualan()),
                            'produk_id' => $item_faktur->produk_id,
                            'akun_id' => $item_faktur->produk->akun_barang_terkirim_id, //aslinya
                        ])->update(['nominal' => $nominal]);
                    }

                    // update di barangnya
                    $brg_faktur_penj->update(['harga_modal' => $brg_faktur_penj->sum('harga_modal')]);
                    $brg_pengiriman_penj->update(['harga_modal' => $brg_faktur_penj->sum('harga_modal')]);
                }
            }
        }
    }

    public function formattingAllData($data)
    {
        // data faktur
        $data['amount'] = floatval(preg_replace('/[^\d.]/', '', $data['amount']));

        // Total Diskon Faktur
        $data['total_diskon_faktur'] = floatval(preg_replace('/[^\d.]/', '', $data['total_diskon_faktur']));

        // data produk
        if (!empty($data['produk_id'])) {
            for ($i = 0; $i < collect($data['produk_id'])->count(); ++$i) {
                $data['unit_harga_produk'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['unit_harga_produk'][$i]));
                $data['amount_produk'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['amount_produk'][$i]));
            }
        }

        // data uang muka
        if (!empty($data['faktur_dp_id'])) {
            for ($y = 0; $y < collect($data['faktur_dp_id'])->count(); ++$y) {
                $data['total_dp'][$y] = floatval(preg_replace('/[^\d.]/', '', $data['total_dp'][$y]));
            }
        }

        // data beban
        if (!empty($data['akun_beban_id'])) {
            for ($y = 0; $y < collect($data['akun_beban_id'])->count(); ++$y) {
                $data['amount_beban'][$y] = floatval(preg_replace('/[^\d.]/', '', $data['amount_beban'][$y]));
            }
        }

        return $data;
    }
}
