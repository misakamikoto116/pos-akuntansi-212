<?php

namespace App\Modules\Akuntansi\Repositories;

use Carbon\carbon;
use Helpers\TransaksiHelper;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\Transaksi;
use Generator\Interfaces\RepositoryInterface;
use App\Modules\Akuntansi\Models\KategoriProduk;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\PreferensiBarang;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\TingkatHargaBarang;
use App\Modules\Akuntansi\Models\PenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\DetailPenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\SaldoAwalBarang as ProdukDetail;
use Intervention\Image\Facades\Image;
use File;
use DB;

class BarangRepository implements RepositoryInterface
{
    public function __construct(
        Produk $model,
        KategoriProduk $kategori,
        Gudang $gudang,
        Akun $akun,
        InformasiPemasok $pemasok,
        KodePajak $kode_pajak,
        TingkatHargaBarang $tingkat_harga_barang,
        PreferensiBarang $preferensi_barang,
        TransaksiHelper $transaksi
    ) {
        $this->model = $model;
        $this->kategori = $kategori;
        $this->gudang = $gudang;
        $this->akun = $akun;
        $this->pemasok = $pemasok;
        $this->kode_pajak = $kode_pajak;
        $this->tingkat_harga_barang = $tingkat_harga_barang;
        $this->preferensi_barang = $preferensi_barang;
        $this->transaksi = $transaksi;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(50);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $model = $this->model->with(['tingkatHargaBarang','saldoAwalBarang'])->findOrFail($id);
           
        $model = $this->formattingNominalFromDatabase($model);

        return $model;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_barang')){
            return abort(403);
        }

        $model = $this->model->find($id);
        if ($model->delete()) {
            $this->DeleteTingkatHargaBarang($model);
            $this->updatePenyesuaianPersediaan($model);
        }

        return $model;
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        if($data['status_umkm'] === null){
            $data['status_umkm'] = 0;
        }
        $arrayParentData = $data;
        $filename = null;
        if(request()->has('foto_produk')){
            if($data['foto_produk'] == null){
                unset($arrayParentData['foto_produk']);
            }else{
                // path foto
                $location = storage_path().'/app/public/produk/';

                // delete foto produk yg lama
                $model = $this->model->where('id', $id)->first();
                File::delete($location.$model['foto_produk']);

                // request foto produk baru
                $file = request()->file('foto_produk');
                $extension = $file->getClientOriginalExtension();
                $filename = md5($file->getFilename()).'.'.$extension;
                $locationImage  =   $location.$filename;
                    
                // simpan foto produk yg baru
                Image::make($file)->resize(200, 150)->save($locationImage);
                $data['foto_produk'] = $filename;
                $arrayParentData['foto_produk'] = $filename;
            }
        }

        unset($arrayParentData['harga_unit']);

        $arrayParentData['harga_jual']          = $this->formattingNominalField($arrayParentData['harga_jual']);
        $arrayParentData['harga_standar_def']   = $this->formattingNominalField($arrayParentData['harga_standar_def']);

        $model = $this->findItem($id)->fill($arrayParentData);
        
        $this->DeleteTingkatHargaBarang($model);
        $this->updatePenyesuaianPersediaan($model);

        if ($data['tipe_barang'] == 2) {
            if ($model->save()) {
                $this->InsertTingkatHargaBarang($data, $model);
            }
        }else if($data['kuantitas'] <= 0){
            if ($model->save()) {
                $this->InsertTingkatHargaBarang($data, $model);
            }
        }else {
            if ($model->save()) {
                $this->InsertTingkatHargaBarang($data, $model);
                $this->InsertDetailBarang($data, $model);
            }
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        if($data['status_umkm'] === null){
            $data['status_umkm'] = 0;
        }
        $arrayParentData = $data;
        $filename = null;
        if(request()->has('foto_produk')){
            $file = request()->file('foto_produk');
            $extension = $file->getClientOriginalExtension();
            $filename = md5($file->getFilename()).'.'.$extension;

            $location = storage_path().'/app/public/produk/'.$filename;
            Image::make($file)->resize(200, 150)->save($location);
            $data['foto_produk'] = $filename;
            $arrayParentData['foto_produk'] = $filename;
        }

        unset($arrayParentData['harga_unit']);

        $arrayParentData['harga_jual']          = $this->formattingNominalField($arrayParentData['harga_jual']);
        $arrayParentData['harga_standar_def']   = $this->formattingNominalField($arrayParentData['harga_standar_def']);


        $model = $this->model->fill($arrayParentData);

        if ($data['tipe_barang'] == 2) {
            if ($model->save()) {
                $this->InsertTingkatHargaBarang($data, $model);
            }
        }else if($data['kuantitas'] <= 0){
            if ($model->save()) {
                $this->InsertTingkatHargaBarang($data, $model);
            }
        }else {
            if ($model->save()) {
                $this->InsertTingkatHargaBarang($data, $model);
                $this->InsertDetailBarang($data, $model);
            }
        }

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(20);
    }

    public function listKategoriProduk()
    {
        return $this->kategori->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listGudang()
    {
        return $this->gudang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listAkun()
    {
        return $this->akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function listPemasok()
    {
        return $this->pemasok->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listKodePajak()
    {
        return $this->kode_pajak->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->kode;

            return $output;
        }, []);
    }

    public function listPrefBarang()
    {
        return $this->preferensi_barang->get();
    }

    public function InsertTingkatHargaBarang($data, $model)
    {
        $tingkat_harga_barang = $this->tingkat_harga_barang->create([
            'tingkat_1' => $this->formattingNominalField($data['tingkat_1']),
            'tingkat_2' => $this->formattingNominalField($data['tingkat_2']),
            'tingkat_3' => $this->formattingNominalField($data['tingkat_3']),
            'tingkat_4' => $this->formattingNominalField($data['tingkat_4']),
            'tingkat_5' => $this->formattingNominalField($data['tingkat_5']),
            'produk_id' => $model->id,
        ]);

        return $tingkat_harga_barang;
    }

    public function InsertDetailBarang($data, $model)
    {
        $data_penyesuaian   = [];
        $arrayFakturBarang  = [];
        $jam_sekarang       = Carbon::now()->format('Y-m-d H:i:s');
        $preferensi         = PreferensiMataUang::first();

        $data['harga_unit'] = $this->formattingNominalField($data['harga_unit']);

        foreach ($data['kuantitas_gudang'] as $key => $kuantitas_gudang) {

            $tanggal_gudang             = Carbon::parse($data['tanggal_gudang'][$key])->format('Y-m-d H:i:s');
            $data['biaya_gudang'][$key] = $this->formattingNominalField($data['biaya_gudang'][$key]);

            $temp_data = [
                'no_penyesuaian'    => $data['no_faktur_gudang'][$key] ?? $this->makeNoPenyesuaian($tanggal_gudang),
                'tgl_penyesuaian'   => $tanggal_gudang,
                'akun_penyesuaian'  => $data['akun_persedian_id'],
                'kode_akun_id'      => $data['akun_persedian_id'],
                'status'            => 0,
            ]; 

            $tempDataPenyesuaian    = PenyesuaianPersediaan::create($temp_data);
            $tempDetailPenyesuaian  = $this->insertDetailPenyesuaian($tempDataPenyesuaian, $data, $key,$model);

            $data_penyesuaian[]         = $tempDataPenyesuaian;
        }

        foreach ($data_penyesuaian as $key => $penyesuaian) {

            $arrDebitPersediaan = [
                'nominal'       => $penyesuaian->detailPenyesuaian->sum('nilai_skrg'),
                'tanggal'       => $penyesuaian->tgl_penyesuaian,
                'dari'          => 'Akun Persediaan',
                'akun'          => $penyesuaian->akun_penyesuaian,
            ];


            $arrDebit = [
                'nominal'       => $penyesuaian->detailPenyesuaian->sum('nilai_skrg'),
                'tanggal'       => $penyesuaian->tgl_penyesuaian,
                'dari'          => 'Akun Penyesuaian',
                'akun'          => $this->akunOpening()->id,
            ];

            $first_transaksi_persediaan     = $this->transaksi->ExecuteTransactionSingle('insert', $penyesuaian, $arrDebitPersediaan, 1, $data['no_barang']);
            $second_transaksi_penyesuaian   = $this->transaksi->ExecuteTransactionSingle('insert', $penyesuaian, $arrDebit, 0, $data['no_barang']);

            $arrayFakturBarang[] = [
                'no_faktur'         => $data['no_faktur_gudang'][$key] ?? $this->makeNoFakturProdukDetail(Carbon::parse($data['tanggal_gudang'][$key])->format('d-m-Y'), $data['no_barang']),
                'tanggal'           => Carbon::parse($data['tanggal_gudang'][$key])->format('Y-m-d H:i:s'),
                'harga_modal'       => $data['harga_unit'],
                'harga_terakhir'    => $data['biaya_gudang'][$key],
                'kuantitas'         => $data['kuantitas_gudang'][$key],
                'sn'                => $data['sn_gudang'][$key],
                'transaksi_id'      => $first_transaksi_persediaan->id,
                'status'            => 0,
                'gudang_id'         => $data['multi_gudang_id'][$key],
                'produk_id'         => $model->id,
                'created_at'        => $jam_sekarang,
                'updated_at'        => $jam_sekarang,
                'item_type'         => get_class($penyesuaian),
                'item_id'           => $penyesuaian->id,
            ];          
        }

        return ProdukDetail::insert($arrayFakturBarang);
    }

    public function insertDetailPenyesuaian($dataPenyesuaian, $data, $key, $model)
    {
        $detail_penyesuaian = DetailPenyesuaianPersediaan::create([
            'penyesuaian_persediaan_id'     => $dataPenyesuaian->id,
            'produk_id'                     => $model->id,
            'keterangan_produk'             => $model->keterangan,
            'satuan_produk'                 => $model->unit,
            'qty_produk'                    => $data['kuantitas_gudang'][$key],
            'qty_baru'                      => $data['kuantitas_gudang'][$key],
            'nilai_skrg'                    => $data['kuantitas_gudang'][$key] * $data['biaya_gudang'][$key],
            'nilai_baru'                    => $data['biaya_gudang'][$key],
            'gudang_id'                     => $data['multi_gudang_id'][$key],
        ]);

        return $detail_penyesuaian;
    }

    public function DeleteTingkatHargaBarang($model)
    {
        return TingkatHargaBarang::where('produk_id', $model->id)->delete();
    }


    public function updatePenyesuaianPersediaan($model)
    {
        $penyesuaian = $this->getPenyesuaianPersediaan($model);

        if ($penyesuaian->isNotEmpty()) {
            $this->delTransactionProdukDetail($penyesuaian);
        }

        return true;
    }

    public function delTransactionProdukDetail($model)
    {
        $detail = [];

        $id_penyesuaian = [];

        foreach ($model as $penyesuaian) {

            ProdukDetail::where([
                'item_id' => $penyesuaian->id,
                'item_type' => get_class($penyesuaian),
            ])->delete();

            $this->transaksi->ExecuteTransactionSingle("delete", $penyesuaian);

            $id_penyesuaian[] = $penyesuaian->id;

            $detail = array_merge($detail, $penyesuaian->detailPenyesuaian->pluck('id')->toArray());

        }

        DetailPenyesuaianPersediaan::whereIn('id',$detail)->delete();

        PenyesuaianPersediaan::whereIn('id', $id_penyesuaian)->delete();

        return true;
    }

    public function getPenyesuaianPersediaan($model)
    {
        return PenyesuaianPersediaan::with('detailPenyesuaian')->where('status', 0)
                                                                       ->whereHas('detailPenyesuaian', function ($query) use ($model)
                                                                       {
                                                                           $query->where('produk_id', $model->id);
                                                                       })->get();
    }

    public function formattingNominalField($nominal)
    {
        return str_replace(['Rp ', '.00', ','], '', $nominal);   
    }

    public function formattingNominalFromDatabase($model)
    {
        if (!empty($model->harga_jual)) {
            $model->harga_jual = number_format($model->harga_jual);
        }

        if (!empty($model->harga_standar_def)) {
            $model->harga_standar_def = number_format($model->harga_standar_def);
        }

        if (!empty($model->tingkatHargaBarang->tingkat_1)) {
            $model->tingkatHargaBarang->tingkat_1 = number_format($model->tingkatHargaBarang->tingkat_1);
        }

        if (!empty($model->tingkatHargaBarang->tingkat_2)) {
            $model->tingkatHargaBarang->tingkat_2 = number_format($model->tingkatHargaBarang->tingkat_2);
        }

        if (!empty($model->tingkatHargaBarang->tingkat_3)) {
            $model->tingkatHargaBarang->tingkat_3 = number_format($model->tingkatHargaBarang->tingkat_3);
        }

        if (!empty($model->tingkatHargaBarang->tingkat_4)) {
            $model->tingkatHargaBarang->tingkat_4 = number_format($model->tingkatHargaBarang->tingkat_4);
        }

        if (!empty($model->tingkatHargaBarang->tingkat_5)) {
            $model->tingkatHargaBarang->tingkat_5 = number_format($model->tingkatHargaBarang->tingkat_5);
        }

        return $model;
    }

    public function makeNoPenyesuaian($tanggal)
    {
        $penyesuaian = PenyesuaianPersediaan::get();

        if ($penyesuaian->isEmpty()) {
            $no_penyesuaian = sprintf('Saldo-Awal-Penyesuaian/%s/%s', $tanggal, '1');
        }else {
            $last_penyesuaian = PenyesuaianPersediaan::orderBy('id','DESC')->first();
            $no_penyesuaian = sprintf('Saldo-Awal-Penyesuaian/%s/%s', $tanggal, $last_penyesuaian->id + 1);
        }

        return $no_penyesuaian;
    }

    public function akunOpening()
    {
        return $this->akun->where('nama_akun','Opening Balance Equity')->first();
    }

    public function makeNoFakturProdukDetail($tanggal, $no_barang)
    {
        $no_faktur = sprintf('Saldo-Awal-Produk/%s/%s', $tanggal, $no_barang);

        return $no_faktur;
    }

    public function marginHargaJual($data)
    {
        try {
                
            DB::beginTransaction();

            $barang = $this->model->findOrFail($data['id_barang']);

            $barang->update([
                'harga_jual'        => $this->formattingNominalField($data['harga_jual']),
            ]);

            if (!empty($barang->tingkatHargaBarang)) {
                $barang->tingkatHargaBarang->update([
                    'tingkat_1'        => $this->formattingNominalField($data['harga_jual']),
                ]);
            }

            DB::commit();

            return [
                'type'      => 'success',
                'message'   => 'Berhasil Menambah/Memperbarui data',
            ];

        } catch (Exception $e) {
            
            DB::rollback();

            return [
                'type'      => 'errors',
                'message'   => $e->getMessage(),
            ]; 
        }
    }
}
