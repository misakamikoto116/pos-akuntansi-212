<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\TipeAkun;
use Generator\Interfaces\RepositoryInterface;


class TipeAkunRepository implements RepositoryInterface
{
    public function __construct(TipeAkun $model)
    {
        $this->model = $model;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->orderBy('id','DESC')->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $this->deletedByUser($id);
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data  = $this->updatedByUser($data);
        $model = $this->findItem($id)->fill($data);
        $model->save();

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data  = $this->createdByUser($data);
        $model = $this->model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }

    public function createdByUser($data)
    {
        $data['created_by'] = auth()->user()->id;

        return $data;
    }

    public function updatedByUser($data)
    {
        $data['updated_by'] = auth()->user()->id;

        return $data;
    }

    public function deletedByUser($id)
    {
        $data = [];

        $data['deleted_by'] = auth()->user()->id;

        $this->model->findOrFail($id)->update($data);

        return true;
    }
}