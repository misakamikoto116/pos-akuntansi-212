<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\BantuanPenerimaanPembelian;
use App\Modules\Akuntansi\Models\BarangPenerimaanPembelian;
use App\Modules\Akuntansi\Models\BarangPesananPembelian;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\JasaPengiriman;
use App\Modules\Akuntansi\Models\PenerimaanPembelian;
use App\Modules\Akuntansi\Models\PesananPembelian;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use Carbon\Carbon;
use Generator\Interfaces\RepositoryInterface;
use Helpers\CodeHelper;
use Helpers\IndonesiaDate as DateHelper;
use Helpers\TransaksiHelper;

class PenerimaanBarangRepository implements RepositoryInterface
{
    public function __construct(PenerimaanPembelian $model,
                                InformasiPemasok $pemasok,
                                Produk $produk,
                                JasaPengiriman $pengiriman,
                                Gudang $gudang,
                                CodeHelper $code,
                                DateHelper $date,
                                TransaksiHelper $transaksi,
                                BarangPenerimaanPembelian $barang,
                                BarangPesananPembelian $barang_pesanan_pembelian,
                                PesananPembelian $pesanan_pembelian
                               ) {
        $this->model = $model;
        $this->pemasok = $pemasok;
        $this->produk = $produk;
        $this->pengiriman = $pengiriman;
        $this->gudang = $gudang;
        $this->code = $code;
        $this->date = $date;
        $this->transaksi = $transaksi;
        $this->barang = $barang;
        $this->barang_pesanan_pembelian = $barang_pesanan_pembelian;
        $this->pesanan_pembelian = $pesanan_pembelian;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $penerimaan_barang = $this->model->findOrFail($id);
        $penerimaan_barang->receive_date = Carbon::parse($penerimaan_barang->receive_date)->format('d F Y');
        $penerimaan_barang->ship_date = Carbon::parse($penerimaan_barang->ship_date)->format('d F Y');

        return $penerimaan_barang;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_penerimaan_pembelian')){
            return abort(403);
        }

        $model = $this->model->findOrFail($id);
        $this->deleteSubItem($id, $model);

        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->findItem($id)->fill($this->dateChecker($data));
        $this->transaksi->checkGudang($data);
        if ($model->save()) {
            $this->deleteSubItem($id, $model);
            BarangPenerimaanPembelian::where('penerimaan_pembelian_id', $model->id)->delete();
            $this->transaksi->ExecuteTransactionSingle('delete', $model);
            $this->insertSubItem($data, $model);
        }

        if (!empty($data['lanjutkan'])) {
            if ($data['lanjutkan'] == 'Simpan & Baru') {
                $model['continue_stat'] = 2;
            } else {
                $model['continue_stat'] = 1;
            }
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data['receipt_no'] = $this->code->autoGenerate($this->model, 'receipt_no', $data['receipt_no']);
        $model = $this->model->fill($this->dateChecker($data));
        $this->transaksi->checkGudang($data);
        if ($model->save()) {
            $this->insertSubItem($data, $model);
        }

        if (!empty($data['lanjutkan'])) {
            if ($data['lanjutkan'] == 'Simpan & Baru') {
                $model['continue_stat'] = 2;
            } else {
                $model['continue_stat'] = 1;
            }
        }

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(10);
    }

    public function listPemasok()
    {
        return $this->pemasok->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listProduk()
    {
        return $this->produk->where('tipe_barang', '!==', 3)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->no_barang.' | '.$item->keterangan;

            return $output;
        }, []);
    }

    public function listJasaPengiriman()
    {
        return $this->pengiriman->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listGudang()
    {
        return $this->gudang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function dateChecker($data)
    {
        $data['ship_date'] = (empty($data['ship_date'])) ? $data['ship_date'] = date('Y-m-d H:i:s') : $data['ship_date'] = $this->date->IndonesiaToSql($data['ship_date']);
        $data['receive_date'] = (empty($data['receive_date'])) ? $data['receive_date'] = date('Y-m-d H:i:s') : $data['receive_date'] = $this->date->IndonesiaToSql($data['receive_date']);

        return $data;
    }

    public function idPrediction()
    {
        $data = $this->model->orderBy('id', 'desc')->first();
        if ($data) {
            return $data->id + 1;
        }

        return 1;
    }

    public function reSortingDetailProduk(array $data): array
    {
        $detailProduk = Produk::whereIn('id', $data['produk_id'])->get()->toArray();
        $detailProdukArr = [];

        foreach ($data['produk_id'] as $produk) { //cari itemnya.. samakan indexnya..
            $i = 0;
            $x = 0;
            while ($x < 1) {
                if ($produk == $detailProduk[$i]['id']) {
                    $detailProdukArr[] = [
                        'akun_persedian_id' => $detailProduk[$i]['akun_persedian_id'],
                        'akun_belum_tertagih_id' => $detailProduk[$i]['akun_belum_tertagih_id'],
                    ];
                    $x = 1;
                }
                ++$i;
            }
        }

        return $detailProdukArr;
    }

    public function insertSubItem($data, $model)
    {
        $items = [];
        $transaction_purpose = [];
        $arrDebit = [];
        $arrKredit = [];
        $produkDetail = [];

        $pajak          = $this->findPajak($data, $model)['pajak'];
        $price_sum      = $this->findPajak($data, $model)['sum_unit_price'];
        $diskon_master  = $this->findPajak($data, $model)['total_diskon_faktur'];
        $data['receive_date'] = (empty($data['receive_date'])) ? $data['receive_date'] = date('Y-m-d H:i:s') : $data['receive_date'] = $this->date->IndonesiaToSql($data['receive_date']);
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        $count = count($data['produk_id']);

        $detailProdukArr = $this->reSortingDetailProduk($data);

        for ($i = 0; $i < $count; ++$i) {
            $barang                     = $data['unit_price'][$i] * $data['qty_produk'][$i];
            $total_diskon               = ($barang *  ($data['diskon_produk'][$i] / 100));
            $persen_barang              = $data['total_produk'][$i] / $price_sum;
            $total_diskon_master        = $diskon_master * $persen_barang; 
            $total_barang               = ($barang - $total_diskon - $total_diskon_master) - (!empty($pajak[$i]['sum_pajak']) ? $pajak[$i]['sum_pajak'] : 0);
            $produk_detail_kuantitas    = SaldoAwalBarang::where(['produk_id' => $data['produk_id'][$i]])->orderBy('id', 'desc')->get()->sum('kuantitas');
            $produk_detail_modal        = SaldoAwalBarang::where(['produk_id' => $data['produk_id'][$i]])->orderBy('id', 'desc')->get();
            $modal                      = $produk_detail_modal->first()->harga_modal ?? 0;
            $sumQTy                     = $produk_detail_kuantitas;
            $harga_terakhir             = $total_barang / $data['qty_produk'][$i];
            $harga_modal_1              = $total_barang + ($modal * $sumQTy);
            $harga_modal                = ($harga_modal_1 / ($data['qty_produk'][$i] + $sumQTy));
            $pesanan_pembelian          = (empty($data['barang_pesanan_pembelian_id'][$i])) ? null : $data['barang_pesanan_pembelian_id'][$i];

            $items = [
                'produk_id' => $data['produk_id'][$i],
                'penerimaan_pembelian_id' => $model->id,
                'barang_pesanan_pembelian_id' => $pesanan_pembelian,
                'jumlah' => $data['qty_produk'][$i],
                'satuan' => $data['satuan_produk'][$i],
                'gudang_id' => $data['gudang_id'][$i],
                'harga_modal' => $data['harga_modal'][$i],
                'sn' => $data['sn'][$i],
                // 'status' => 1,
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];  
            // $total_barang_setelah_pajak = $total_barang - (array_key_exists('sum_pajak', $pajak) ? $pajak['sum_pajak'] : 0);

            $arrDebit = [
                'nominal'   => $total_barang ?? 0,
                'tanggal'   => $data['receive_date'],
                'produk'    => $data['produk_id'][$i],
                'dari'      => 'Total Penerimaan Barang Debit',
                'desc'      => 'PenerimaanBarang |'.$model->id,
                'akun'      => $detailProdukArr[$i]['akun_persedian_id'],
            ];

            $arrKredit[] = [
                'nominal'   => $total_barang ?? 0,
                'tanggal'   => $data['receive_date'],
                'produk'    => $data['produk_id'][$i],
                'dari'      => 'Total Penerimaan Barang Kredit',
                'desc'      => 'PenerimaanBarang |'.$model->id,
                'akun'      => $detailProdukArr[$i]['akun_belum_tertagih_id'],
            ];

            $persedian = $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrDebit, 1, $data['receipt_no']);
            $produkDetail[] = [
                    'no_faktur' => $data['form_no'],
                    'tanggal' => $data['receive_date'],
                    'kuantitas' => $data['qty_produk'][$i],
                    'harga_modal' => $harga_modal,
                    'harga_terakhir' => $harga_terakhir,
                    'gudang_id' => !empty($data['gudang_id']) ? $data['gudang_id'][$i] : null,
                    'transaksi_id' => $persedian->id,
                    'produk_id' => $data['produk_id'][$i],
                    'sn' => $data['sn'][$i],
                    'status' => 1,
                    'item_type' => get_class($this->model),
                    'item_id' => $model->id,
                    'created_at' => $jam_sekarang,
            ];

            $brg_penerimaan_pembelian = BarangPenerimaanPembelian::create($items);

            $bantu[] = [
                'produk_id' => $data['produk_id'][$i],
                'brg_penerimaan_pembelian_id' => $brg_penerimaan_pembelian->id,
            ];
        }

        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrKredit, 0, $data['receipt_no']);

        BantuanPenerimaanPembelian::insert($bantu);
        SaldoAwalBarang::insert($produkDetail);

        return true;
    }

    public function findPajak($data, $model)
    {
        $sorting            = [];
        $sum_pajak          = 0;
        $pesanan_pembelian  = $this->findPesananPembelian($data);
        $sum_unit_price     = $this->sumUnitPrice($data, $pesanan_pembelian)['sum_unit_price'];
        $array_unit_price   = $this->sumUnitPrice($data, $pesanan_pembelian)['array_unit_price'];
        $diskon             = $this->hitungDiskon($pesanan_pembelian, $sum_unit_price);
        if (!empty($pesanan_pembelian)) {
            if ($pesanan_pembelian->in_tax !== 0) {
                foreach ($array_unit_price as $key => $unit_price) {
                    $total_tax             = $this->totalTax($pesanan_pembelian, $key);
                    $after_dikurang_diskon = $this->afterDikurangDiskon($unit_price, $sum_unit_price, $diskon);
                    $sorting[]             = $this->calculateSortedPajak($pesanan_pembelian, $key, $total_tax, $unit_price, $after_dikurang_diskon);

                }
            }
        }
        return [
            'pajak'                 => $sorting,
            'sum_unit_price'        => $sum_unit_price,
            'total_diskon_faktur'   => $diskon,
        ];
    }

    // public function sumUnitPrice($data)
    // {
    //     $array_unit_price   = [];

    //     foreach($data['unit_harga_produk'] as $key => $unit_price) {
    //         $sum_qty_price                      = $unit_price * $data['qty_produk'][$key];
    //         $sum_qty_price_with_diskon          = $sum_qty_price - (($data['diskon_produk'][$key] / 100 ) * $sum_qty_price);
    //         $array_unit_price[]                 = $sum_qty_price_with_diskon;
    //     }
        
    //     return $array_unit_price;
    // }

    public function findPesananPembelian($data)
    {
        $pesanan_pembelian = $this->pesanan_pembelian->with('barang')->whereHas('barang', function ($query) use ($data)
        {
            $query->whereIn('id', $data['barang_pesanan_pembelian_id']);
        })->first();

        return $pesanan_pembelian;
    }

    public function afterDikurangDiskon($unit_price, $sum_unit_price, $diskon)
    {
        return ($unit_price / $sum_unit_price) * $diskon;
    }

    public function calculateSortedPajak($pesanan_pembelian, $key, $total_tax, $unit_price, $after_dikurang_diskon)
    {
        $temp_tax           = (100 / (100 + $total_tax) * ($unit_price - $after_dikurang_diskon));
        $total_in_tax1      = ($temp_tax * ($pesanan_pembelian->barang[$key]->pajak ? $pesanan_pembelian->barang[$key]->pajak->nilai : 0) / 100);
        $total_in_tax2      = ($temp_tax * ($pesanan_pembelian->barang[$key]->pajak2 ? $pesanan_pembelian->barang[$key]->pajak2->nilai : 0) / 100);
        $sum_in_tax         = round($total_in_tax1, 2) + round($total_in_tax2, 2);

        return [
            'pajak_1'       => round($total_in_tax1, 2),
            'pajak_2'       => round($total_in_tax2, 2),
            'sum_pajak'     => $sum_in_tax,
        ];
    }

    public function sumUnitPrice($data, $pesanan_pembelian)
    {
        $array_unit_price   = [];
        $sum_unit_price     = 0;
        foreach ($data['unit_price'] as $key => $unit_price) {
            $sum_qty_price              = $unit_price * $data['qty_produk'][$key];
            $sum_unit_price_with_diskon = $sum_qty_price - (($pesanan_pembelian->barang[$key]->diskon / 100) * $sum_qty_price);
            $sum_unit_price            += $sum_unit_price_with_diskon;
            $array_unit_price[]         = $sum_unit_price_with_diskon;
        }

        return [
            'sum_unit_price'        => $sum_unit_price,
            'array_unit_price'      => $array_unit_price,
        ];
    }

    public function hitungDiskon($pesanan_pembelian, $sum_unit_price)
    {
        $diskon_untuk_pajak = 0;

        if ($pesanan_pembelian->diskon > 0) {
            $diskon_untuk_pajak = $sum_unit_price * ($pesanan_pembelian->diskon / 100);
        }

        return $diskon_untuk_pajak;
    }

    public function totalTax($pesanan_pembelian, $key)
    {
        $total_tax = ($pesanan_pembelian->barang[$key]->pajak != null ? $pesanan_pembelian->barang[$key]->pajak->nilai : 0) + 
                     ($pesanan_pembelian->barang[$key]->pajak2 != null ? $pesanan_pembelian->barang[$key]->pajak2->nilai : 0); 

        return $total_tax;
    }

    public function deleteSubItem($id, $model)
    {
        $this->transaksi->ExecuteTransactionSingle('delete', $model);
        SaldoAwalBarang::where([
                    'item_id' => $id,
                    'item_type' => get_class($model),
        ])->delete();

        $this->transaksi->recalculateTransaction($model, $this->barang);
    }
}
