<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\BarangReturPembelian;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\ReturPembelian;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\Transaksi;
use Carbon\Carbon;
use Generator\Interfaces\RepositoryInterface;
use Helpers\CodeHelper;
use Helpers\IndonesiaDate as DateHelper;
use Helpers\TransaksiHelper;

class ReturPembelianRepository implements RepositoryInterface
{
    public function __construct(ReturPembelian $model,
                                CodeHelper $code,
                                InformasiPemasok $pemasok,
                                KodePajak $kode_pajak,
                                DateHelper $date,
                                TransaksiHelper $transaksi,
                                Produk $produk,
                                Gudang $gudang,
                                BarangReturPembelian $barang,
                                FakturPembelian $fakturPembelian,
                                SaldoAwalBarang $saldoAwalBarang)
    {
        $this->model = $model;
        $this->pemasok = $pemasok;
        $this->date = $date;
        $this->code = $code;
        $this->kode_pajak = $kode_pajak;
        $this->transaksi = $transaksi;
        $this->produk = $produk;
        $this->gudang = $gudang;
        $this->barang = $barang;
        $this->fakturPembelian = $fakturPembelian;
        $this->saldoAwalBarang = $saldoAwalBarang;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $retur_pembelian = $this->model->findOrFail($id);
        if (!empty($retur_pembelian->tanggal)) {
            $retur_pembelian->tanggal = Carbon::parse($retur_pembelian->tanggal)->format('d F Y');
        }
        if (!empty($retur_pembelian->tgl_fiscal)) {
            $retur_pembelian->tgl_fiscal = Carbon::parse($retur_pembelian->tgl_fiscal)->format('d F Y');
        }

        return $retur_pembelian;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if( !auth()->user()->hasPermissionTo('hapus_retur_pembelian') ){
            return abort(403);
        }

        $model = $this->model->findOrFail($id);
        $this->deleteSubItem($id, $model);
        // $this->transaksi->ExecuteTransactionSingle('delete', $model);
        $this->transaksi->ExecuteTransactionSingle('delete', $model);

        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $checkData = $this->model->where('return_no', $data['return_no'])
                    ->where('id', '!=', $id)->first();

        if ($checkData) {
            throw new \Exception('Nomor Retur terduplikat!');
        }
        $data = $this->formattingAllData($data);
        $data['taxable'] = (empty($data['taxable'])) ? 0 : 1;
        $data['in_tax'] = (empty($data['in_tax'])) ? 0 : 1;
        $data = $this->dateChecker($data);
        $model = $this->findItem($id)->fill($data);
        $akun = $this->produk->whereIn('id', $data['produk_id'])->pluck('akun_ret_pembelian_id');
        $this->transaksi->checkGudang($data);
        if ($model->save()) {
            $this->deleteSubItem($id, $model);
            $this->insertSubItem($data, $model);
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->formattingAllData($data);
        $data['return_no'] = $this->code->autoGenerate($this->model, 'return_no', $data['return_no']);
        $data['taxable'] = (empty($data['taxable'])) ? 0 : 1;
        $data['in_tax'] = (empty($data['in_tax'])) ? 0 : 1;
        $data = $this->dateChecker($data);
        $model = $this->model->fill($data);
        $akun = $this->produk->whereIn('id', $data['produk_id'])->pluck('akun_ret_pembelian_id');
        $this->transaksi->checkGudang($data);
        if ($model->save()) {
            $this->insertSubItem($data, $model);
        }
        if (!empty($data['lanjutkan'])) {
            $model['continue_stat'] = 2;
        }

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(10);
    }

    public function listPemasok()
    {
        return $this->pemasok->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listGudang()
    {
        return $this->gudang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function idPrediction()
    {
        $data = $this->model->orderBy('id', 'desc')->first();
        if ($data) {
            return $data->id + 1;
        }

        return 1;
    }

    public function dateChecker($data)
    {
        $data['tanggal'] = (empty($data['tanggal'])) ? $data['tanggal'] = null : $data['tanggal'] = $this->date->IndonesiaToSql($data['tanggal']);
        $data['tgl_fiscal'] = (empty($data['tgl_fiscal'])) ? $data['tgl_fiscal'] = null : $data['tgl_fiscal'] = $this->date->IndonesiaToSql($data['tgl_fiscal']);

        return $data;
    }

    public function insertSubItem($data, $model)
    {
        $items = [];
        $produkDetail = [];
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        $count = count($data['produk_id']);
        for ($i = 0; $i < $count; ++$i) {
            $pajak1 = null;
            $pajak2 = null;
            if (!empty($data['tax_produk'][$i])) {
                // data ada.. walau hanya satu
                $pajak1 = $data['tax_produk'][$i][0];
                if (count($data['tax_produk'][$i]) > 1) {
                    $pajak2 = $data['tax_produk'][$i][1];
                }
            }
            $items[] = [
                'produk_id' => $data['produk_id'][$i],
                'barang_faktur_pembelian_id' => $data['barang_faktur_pembelian_id'][$i],
                'retur_pembelian_id' => $model->id,
                'item_deskripsi' => $data['keterangan_produk'][$i],
                'jumlah' => $data['qty_produk'][$i],
                'harga' => $data['unit_harga_produk'][$i],
                'satuan' => $data['satuan_produk'][$i],
                'gudang_id' => (empty($data['gudang_id'][$i])) ? null : $data['gudang_id'][$i],
                'kode_pajak_id' => $pajak1,
                'kode_pajak_2_id' => $pajak2,
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];
        }
        $this->insertTransaction($data, $model);

        return BarangReturPembelian::insert($items);
    }

    public function deleteSubItem($id, $model)
    {
        SaldoAwalBarang::where([
                    'item_id' => $id,
                    'item_type' => get_class($model),
        ])->delete();
        $this->transaksi->recalculateTransaction($model, $this->barang);
    }

    public function checkNoSr($data)
    {
        // $data['sr_no'] = $this->code->autoGenerate($this->model, "sr_no", $data['sr_no']);
    }

    public function listKodePajak()
    {
        return $this->kode_pajak->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->kode_pajak_formatted;

            return $output;
        }, []);
    }

    public function insertTransaction($data, $model)
    {
        $detailProdukArr = $this->getProdukArr($data);
        $arrDebit = [];
        $arrKredit = [];
        $count = count($data['produk_id']);
        $arrReturPembelian = [];
        $akun_produk = $this->produk->whereIn('id', $data['produk_id'])->pluck('akun_ret_pembelian_id');
        $data_faktur = FakturPembelian::find($data['faktur_pembelian_id']);

        // if (!empty($data['tax_produk'])) {
        $this->hitungPajak($data, $data_faktur, $model);
        // } else {
        //     for ($i = 0; $i < $count; ++$i) {
        //         $total_hpp = ($data['harga_modal'][$i] * $data['qty_produk'][$i]) - ($data['unit_harga_produk'][$i] * $data['qty_produk'][$i]);
        //         // dd($total_hpp);
        //         if ($total_hpp < 0) {
        //             $arrKredit[] = [
        //                 'nominal' => abs($total_hpp),
        //                 'dari' => 'Akun HPP',
        //                 'produk' => $data['produk_id'][$i],
        //                 'akun' => $detailProdukArr[$i]['akun_hpp_id'],
        //             ];
        //         } else {
        //             $arrDebit[] = [
        //                 'nominal' => abs($total_hpp),
        //                 'dari' => 'Akun HPP',
        //                 'produk' => $data['produk_id'][$i],
        //                 'akun' => $detailProdukArr[$i]['akun_hpp_id'],
        //             ];
        //         }
        //     }
        //     if ($total_hpp < 0) {
        //         $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrKredit, 0, $data['return_no']); //i(kredit)
        //     } else {
        //         $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrDebit, 1, $data['return_no']); //i(kredit)
        //     }
        // }

        $detailProdukArr = $this->getProdukArr($data);

        for ($i = 0; $i < count($data['produk_id']); ++$i) {
            $arrReturPembelian = [
                'nominal'   => $data['harga_modal'][$i] * $data['qty_produk'][$i],
                'tanggal'   => $data['tanggal'],
                'dari'      => 'Akun Retur Pembelian',
                'produk'    => $data['produk_id'][$i],
                'akun'      => $detailProdukArr[$i]['akun_ret_pembelian_id'],
            ];

            $modelRetur = $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrReturPembelian, 0, $data['return_no']); //i(kredit)

            $harga_modal_akhir = $this->cariHargaModal($data, $i);

            $produkDetail[] = [
                'no_faktur' => $data['return_no'],
                'tanggal' => date('Y-m-d H:i:s'),
                'kuantitas' => -$data['qty_produk'][$i],
                'harga_modal' => $harga_modal_akhir,
                'harga_terakhir' => $data['harga_terakhir'][$i],
                'gudang_id' => !empty($data['gudang_id']) ? $data['gudang_id'][$i] : null,
                'transaksi_id' => $modelRetur->id, //harusnya pake $model->id
                'sn' => null,
                'status' => 1,
                'item_type' => get_class($model),
                'item_id' => $model->id,
                'produk_id' => $data['produk_id'][$i],
                'created_at' => date('Y-m-d H:i:s'),
            ];
            /*
             * Q : apakah harga modal terakhir = $harga_terakhir ?
             * Q : yang seperti apa itu selisih untuk akun HPP? dan masuk kredit/debit ?
             */
        }

        SaldoAwalBarang::insert($produkDetail);

        return true;
    }

    public function hitungPajak($data, $data_faktur, $model)
    {
        // dd($data);
        $detailProdukArr = $this->getProdukArr($data);
        $count = count($data['produk_id']);
        $arrayPajak = [];
        $grandTotalBarang = 0;
        $totalPajak1 = 0;
        $totalPajak2 = 0;
        for ($i = 0; $i < $count; ++$i) {
            $pajak1 = null;
            $pajak2 = null;
            $barang = $data['unit_harga_produk'][$i] * $data['qty_produk'][$i];
            // $total_diskon = (($barang * $data['diskon_produk'][$i]) / 100);
            // $total_diskon_master = ($data['amount_produk'][$i] * $data['diskon'] / 100);
            $total_barang = $barang; //- $total_diskon - $total_diskon_master;
            $grandTotalBarang += $total_barang;
            if (!empty($data['tax_produk'][$i])) {
                // data ada.. walau hanya satu
                $pajak1 = $data['tax_produk'][$i][0];
                $data_pajak1 = KodePajak::find($pajak1); // res : collection
                if (!empty($data['in_tax'])) {
                    $total_pajak1 = ($total_barang * $data_pajak1->nilai) / 100 + $data_pajak1->nilai; // res : float
                } else {
                    $total_pajak1 = ($total_barang * $data_pajak1->nilai) / 100; // res : float
                }
                $arrayPajak[] = [
                        'nominal'   => $total_pajak1,
                        'tanggal'   => $data['tanggal'],
                        'produk'    => $data['produk_id'][$i],
                        'akun'      => $data_pajak1->akun_pajak_pembelian_id,
                        'dari'      => 'Pajak 1 dan barang',
                    ];
                $grandTotalBarang += $total_pajak1;
                $totalPajak1 += $total_pajak1;
                if (count($data['tax_produk'][$i]) > 1) {
                    $pajak2 = $data['tax_produk'][$i][1];
                    $data_pajak2 = KodePajak::find($pajak2); // res : collection
                    if (!empty($data['in_tax'])) {
                        $total_pajak2 = ($total_barang * $data_pajak2->nilai) / 100 + $data_pajak2->nilai; // res : float
                    } else {
                        $total_pajak2 = ($total_barang * $data_pajak2->nilai) / 100; // res : float
                    }
                    $arrayPajak[] = [ // pajak pure
                            'nominal'   => $total_pajak2,
                            'tanggal'   => $data['tanggal'],
                            'produk'    => $data['produk_id'][$i],
                            'akun'      => $data_pajak2->akun_pajak_pembelian_id,
                            'dari'      => 'Pajak 2 dan barang',
                        ];
                    $grandTotalBarang += $total_pajak2;
                    $totalPajak2 += $total_pajak2;
                }
            }
        }

        for ($i = 0; $i < $count; ++$i) {
            $sum_pajak = $totalPajak1 + $totalPajak2;
            $persen_barang = $data['qty_produk'][$i] / array_sum($data['qty_produk']);
            $pajak_persen = $persen_barang * $sum_pajak;
            $nilai_hpp = $persen_barang * $grandTotalBarang;
            $total_hpp = ($data['harga_modal'][$i] * $data['qty_produk'][$i]) - ($data['unit_harga_produk'][$i] * $data['qty_produk'][$i]);
            // dd($total_hpp);
            if ($total_hpp < 0) {
                $arrKredit[] = [
                    'nominal'   => abs($total_hpp),
                    'tanggal'   => $data['tanggal'],
                    'dari'      => 'Akun HPP',
                    'produk'    => $data['produk_id'][$i],
                    'akun'      => $detailProdukArr[$i]['akun_hpp_id'],
                ];
            } else {
                $arrDebit[] = [
                        'nominal'   => abs($total_hpp),
                        'tanggal'   => $data['tanggal'],
                        'dari'      => 'Akun HPP',
                        'produk'    => $data['produk_id'][$i],
                        'akun'      => $detailProdukArr[$i]['akun_hpp_id'],
                    ];
            }
        }

        $arrayBarangDanPajak = [
            'nominal'   => $grandTotalBarang,
            'tanggal'   => $data['tanggal'],
            'akun'      => $data_faktur->akun_hutang_id,
            'dari'      => 'Total dan Pajak',
        ];
        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayPajak, 0, $data['return_no']); //insert pajak per item (kredit)
        $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrayBarangDanPajak, 1, $data['return_no']); //insert pajak per item (kredit)
        if ($total_hpp < 0) {
            $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrKredit, 0, $data['return_no']); //i(kredit)
        } else {
            $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrDebit, 1, $data['return_no']); //i(kredit)
        }

        return true;
    }

    public function getProdukArr($data): array
    {
        $detailProduk = Produk::whereIn('id', $data['produk_id'])->get()->toArray();
        $detailProdukArr = []; // y I do dis.. kalau 10 barang brti 10x query.. itu berat mz

        foreach ($data['produk_id'] as $produk) { //cari itemnya.. samakan indexnya..
            $i = 0;
            $x = 0;
            while ($x < 1) {
                if ($produk == $detailProduk[$i]['id']) {
                    $detailProdukArr[] = [
                        'akun_hpp_id' => $detailProduk[$i]['akun_hpp_id'],
                        'akun_ret_pembelian_id' => $detailProduk[$i]['akun_ret_pembelian_id'],
                    ];
                    $x = 1;
                }
                ++$i;
            }
        }

        return $detailProdukArr;
    }

    public function formattingAllData($data)
    {
        // data produk
        if (!empty($data['produk_id'])) {
            for ($i = 0; $i < count($data['produk_id']); ++$i) {
                $data['unit_harga_produk'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['unit_harga_produk'][$i]));
                $data['amount_produk'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['amount_produk'][$i]));
            }
        }

        return $data;
    }

    public function cariHargaModal($data, $i)
    {
        // Mencari Harga Qty Ter Update dan mengkalikan dengan harga modal yang dikirim dari request
        $harga_modal_dari_faktur = $this->saldoAwalBarang->where('item_type', get_class($this->fakturPembelian))
                                                           ->where('item_id', $data['faktur_pembelian_id'])
                                                           ->where('produk_id', $data['produk_id'][$i])
                                                           ->orderBy('id','DESC')->first();
        $updated_qty            = $this->produk->findOrFail($data['produk_id'][$i])->updated_qty;
        $harga_modal_1          =  $harga_modal_dari_faktur->harga_modal * $updated_qty;

        // Mencari harga final yang diambil dari barang faktur pembelian yang berkaitan dengan retur
        $faktur_pembelian       = $this->fakturPembelian->with('barang')->findOrFail($data['faktur_pembelian_id']);
        $barang_faktur          = $faktur_pembelian->barang->where('produk_id', $data['produk_id'][$i])->first();
        $harga_modal_2          = $barang_faktur->jumlah * $barang_faktur->harga_final;

        $total_harga_modal      = $harga_modal_1 - $harga_modal_2;
        $qty_harga_modal        = $updated_qty - $barang_faktur->jumlah;

        $harga_modal_pertama    = $total_harga_modal / $qty_harga_modal;

        $harga_final_retur          = ($barang_faktur->jumlah - $data['qty_produk'][$i]) * $barang_faktur->harga_final;
        $harga_paling_final_retur   = $harga_final_retur + $total_harga_modal;
        $qty_final_retur            = ($barang_faktur->jumlah - $data['qty_produk'][$i]) + $qty_harga_modal;

        $harga_modal_akhir          = $harga_paling_final_retur / $qty_final_retur; 

        return $harga_modal_akhir;
    }
}
