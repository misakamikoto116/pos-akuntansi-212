<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\KategoriProduk;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\SaldoAwalBarang as ProdukDetail;
use App\Modules\Akuntansi\Models\TingkatHargaBarang;
use App\Modules\Akuntansi\Models\PreferensiBarang;
use Carbon\carbon;
use Generator\Interfaces\RepositoryInterface;


class GroupBarangRepository implements RepositoryInterface
{
    public function __construct(Produk $model, Akun $akun, KodePajak $kode_pajak, TingkatHargaBarang $tingkat_harga_barang, PreferensiBarang $preferensi_barang)
    {
        $this->model = $model;
        $this->akun = $akun;
        $this->kode_pajak = $kode_pajak;
        $this->tingkat_harga_barang = $tingkat_harga_barang;
        $this->preferensi_barang = $preferensi_barang;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->where('tipe_barang', 4)->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);
        return $this->$model;
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->findItem($id);
        $model->save();
        return $this->$model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $model->save();
        return $this->$model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(20);
    }
    public function listAkun()
    {
        return $this->akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }
    public function listKodePajak()
    {
        return $this->kode_pajak->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->kode;

            return $output;
        }, []);
    }
    public function listPrefBarang()
    {
        return $this->preferensi_barang->get();
    }
}