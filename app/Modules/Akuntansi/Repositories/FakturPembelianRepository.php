<?php
namespace App\Modules\Akuntansi\Repositories;

use Carbon\carbon;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\Produk;
use Helpers\IndonesiaDate as DateHelper;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\Transaksi;
use Generator\Interfaces\RepositoryInterface;
use App\Modules\Akuntansi\Models\JasaPengiriman;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\PenerimaanPembelian;
use App\Modules\Akuntansi\Models\BebanFakturPembelian;
use App\Modules\Akuntansi\Models\BarangFakturPembelian;
use App\Modules\Akuntansi\Models\TransaksiUangMukaPemasok;
use App\Modules\Akuntansi\Models\BarangPenerimaanPembelian;
use App\Modules\Akuntansi\Models\BantuanPenerimaanPembelian;

class FakturPembelianRepository implements RepositoryInterface
{
	public function __construct(FakturPembelian $model,
								InformasiPemasok $pemasok,
								Produk $produk,
								JasaPengiriman $pengiriman,
								Gudang $gudang,
								Akun $akun,
								SyaratPembayaran $termin,
								KodePajak $kode_pajak,
				DateHelper $date, CodeHelper $code, TransaksiHelper $transaksi,
				Transaksi $transaksiModel,
								PreferensiMataUang $preferensi,
								BebanFakturPembelian $beban_faktur_pembelian,
								PenerimaanPembelian $penerimaan,
								BarangFakturPembelian $barang,
								BarangPenerimaanPembelian $barang_penerimaan,
								TransaksiUangMukaPemasok $transaksiUangPemasok
							   ) {
		$this->model          = $model;
		$this->pemasok        = $pemasok;
		$this->produk         = $produk;
		$this->pengiriman     = $pengiriman;
		$this->termin         = $termin;
		$this->gudang         = $gudang;
		$this->date           = $date;
		$this->code           = $code;
		$this->akun           = $akun;
		$this->kode_pajak     = $kode_pajak;
		$this->transaksi      = $transaksi;
		$this->transaksiModel = $transaksiModel;

		$this->preferensi               = $preferensi;
		$this->beban_faktur_pembelian   = $beban_faktur_pembelian;
		$this->penerimaan               = $penerimaan;
		$this->barang_penerimaan        = $barang_penerimaan;
		$this->barang                   = $barang;
		$this->transaksiUangMukaPemasok = $transaksiUangPemasok;
	}

	use TraitFakturPembelianRepository;

	/**
	 * ini untuk mengambil data keseluruhan
	 * user di data repositori.
	 *
	 * @return Collection data list user
	 */
	public function getItems()
	{
		$data          = $this->query()->orderBy('id', 'DESC')->paginate(20);
		$transaksiSums = $this->transaksiModel->whereIn('item_id', $data->getCollection()->pluck('id'))
	->where('item_type', 'App\Modules\Akuntansi\Models\FakturPembelian')
	->selectRaw('sum(nominal) as sum_nominal')
	->addSelect('item_id')
	->groupBy('item_id')
	->pluck('sum_nominal', 'item_id');
		$data->getCollection()->transform(function (FakturPembelian $item) use ($transaksiSums) {
			$item->uang_muka_formatted = $transaksiSums[$item->id] ?? 0;

			return $item;
		});

		return $data;
	}

	/**
	 * ini untuk mencari user berdasarkan id yang dicari.
	 *
	 * @param int $id
	 *
	 * @return object
	 */
	public function findItem($id)
	{
		$faktur_pembelian = $this->model->findOrFail($id);

		if (!empty($faktur_pembelian->syaratPembayaran)) {
			$faktur_pembelian->total_syarat_diskon = $faktur_pembelian->total * ($faktur_pembelian->syaratPembayaran->akan_dapat_diskon / 100);
		} else {
			$faktur_pembelian->total_syarat_diskon = 0;
		}
		$faktur_pembelian->invoice_date = Carbon::parse($faktur_pembelian->invoice_date)->format('d F Y');
		$faktur_pembelian->ship_date    = Carbon::parse($faktur_pembelian->ship_date)->format('d F Y');

		return $faktur_pembelian;
	}

	/**
	 * ini untuk menghapus data berdasarkan id.
	 *
	 * @param [type] $id [description]
	 *
	 * @return [type] [description]
	 */
	public function delete($id)
	{
		if (!auth()->user()->hasPermissionTo('hapus_faktur_pembelian')) {
			return abort(403);
		}

		$model = $this->model->findOrFail($id);
		$this->deleteSubItem($model, $id);

		$this->transaksiUangMukaPemasok->where('faktur_pembelian_id', $id)->delete();
		// $this->transaksi->ExecuteTransaction('delete', $model); # T E R H A P U S
		return $this->model->findOrFail($id)->delete();
	}

	/**
	 * update data berdasarkan id dan data
	 * didapat dari variable request.
	 *
	 * @param [type] $id   [description]
	 * @param [type] $data [description]
	 *
	 * @return [type] [description]
	 */
	public function update($id, $data)
	{
		$data              = $this->formattingAllData($data);
		$data['no_faktur'] = $this->code->autoGenerate($this->model, 'no_faktur', $data['no_faktur']);
		$data['taxable']   = (empty($data['taxable'])) ? 0 : 1;
		$data['in_tax']    = (empty($data['in_tax'])) ? 0 : 1;
		$model             = $this->model->findOrFail($id)->fill($this->dateChecker($data));
		$this->transaksi->checkGudang($data);

		if ($model->save()) {
			$this->deleteSubItem($model, $id);
			$this->insertSubItem($data, $model, 'update');
		}
		$this->buatFakturDariBeban($data);

		if (!empty($data['lanjutkan'])) {
			if ('Simpan & Baru' == $data['lanjutkan']) {
				$model['continue_stat'] = 2;
			} else {
				$model['continue_stat'] = 1;
			}
		}

		return $model;
	}

	/**
	 * menambahkan data berdasarkan request.
	 *
	 * @param [type] $request [description]
	 *
	 * @return [type] [description]
	 */
	public function insert($data)
	{
		$data               = $this->formattingAllData($data);
		$data['no_faktur']  = $this->code->autoGenerate($this->model, 'no_faktur', $data['no_faktur']);
		$data['akun_dp_id'] = $this->preferensi->pluck('uang_muka_pembelian_id')->first();
		$data['receipt_no'] = $data['no_faktur'];
		$modelPenerimaan    = $this->penerimaan->fill($this->dateChecker($data));
		if ($modelPenerimaan->save()) {
			$dataPenerimaan                         = $this->insertSubItemPenerimaan($modelPenerimaan, $data);
			$data['barang_penerimaan_pembelian_id'] = $dataPenerimaan;
			$modelFaktur                            = $this->model->fill($this->dateChecker($data));
			if ($modelFaktur->save()) {
				$this->insertSubItem($data, $modelFaktur, 'insert');
			}
			$this->transaksi->checkGudang($data);
			$this->buatFakturDariBeban($data);
		}

		if (!empty($data['lanjutkan'])) {
			if ('Simpan & Baru' == $data['lanjutkan']) {
				$model['continue_stat'] = 2;
			} else {
				$model['continue_stat'] = 1;
			}
		}

		return $model;
	}

	public function buatFakturDariBeban($data)
	{
		// dd($data);
		$newFaktur = collect();
		if (isset($data['txt_alokasi_ke_barang'])) {
			foreach ($data['txt_alokasi_ke_barang'] as $key => $isAlocation) {
				$vendor = $this->pemasok->find($data['pemasok_beban_id_real'][$key]);
				// dd(null === $vendor);
				if (null === $vendor) {
					continue;
				}
				if ('1' === $isAlocation) {
					$produk     = $this->produk->where('tipe_barang', 4)->firstOrFail();
					$tempFaktur = $newFaktur->firstWhere('pemasok_id', $vendor->id);
					if (null === $tempFaktur) {
						$tempFaktur = array_only($data, [
							'form_no',
							'ship_date',
							'no_faktur',
							'invoice_date',
							'fob',
							'term_id',
							'ship_id',
							'akun_hutang_id',
						]);

						$tempFaktur['no_pemasok']                       = $vendor->no_pemasok;
						$tempFaktur['pemasok_id']                       = $vendor->id;
						$tempFaktur['alamat_pengiriman']                = $vendor->alamat;
						$tempFaktur['total_diskon_faktur']              = 0;
						$tempFaktur['produk_id'][]                      = $produk->id;
						$tempFaktur['keterangan_produk'][]              = $data['notes_beban'][$key];
						$tempFaktur['qty_produk'][]                     = 1;
						$tempFaktur['satuan_produk'][]                  = 'Jasa';
						$tempFaktur['unit_harga_produk'][]              = $data['amount_beban'][$key];
						$tempFaktur['diskon_produk'][]                  = '0';
						$tempFaktur['amount_produk'][]                  = $data['amount_beban'][$key];
						$tempFaktur['amount_modal_produk'][]            = 0;
						$tempFaktur['amount_old_modal_produk'][]        = 0;
						$tempFaktur['sn'][]                             = null;
						$tempFaktur['gudang_id'][]                      = null;
						$tempFaktur['barang_penerimaan_pembelian_id'][] = null;
						$newFaktur->push($tempFaktur);
					} else {
						$tempFaktur['produk_id'][]                      = $produk->id;
						$tempFaktur['keterangan_produk'][]              = $data['notes_beban'][$key];
						$tempFaktur['qty_produk'][]                     = 1;
						$tempFaktur['satuan_produk'][]                  = 'Jasa';
						$tempFaktur['unit_harga_produk'][]              = $data['amount_beban'][$key];
						$tempFaktur['diskon_produk'][]                  = '0';
						$tempFaktur['amount_produk'][]                  = $data['amount_beban'][$key];
						$tempFaktur['amount_modal_produk'][]            = 0;
						$tempFaktur['amount_old_modal_produk'][]        = 0;
						$tempFaktur['sn'][]                             = null;
						$tempFaktur['gudang_id'][]                      = null;
						$tempFaktur['barang_penerimaan_pembelian_id'][] = null;
					}
				}
			}
			// dd($data);
			if ($newFaktur->count() > 0) {
				$newFaktur->each(function ($faktur) {
					$faktur['amount'] = $faktur['subtotal'] = array_sum($faktur['amount_produk']);
					$fakturModel = $this->model->newInstance($this->dateChecker($faktur));
					if ($fakturModel->save()) {
						$this->insertSubItem($faktur, $fakturModel, 'insert', true);
					}
				});
			}
		}
	}

	/**
	 * ini berfungisi untuk melakukan filter terhadap
	 * data yang akan diambil dan ditampilkan kepada
	 * user nantinya.
	 *
	 * @param array $data
	 */
	public function filter($data)
	{
		$data          = $this->query()->filter($data)->paginate(10);
		$transaksiSums = $this->transaksiModel->whereIn('item_id', $data->getCollection()->pluck('id'))
	->where('item_type', 'App\Modules\Akuntansi\Models\FakturPembelian')
	->selectRaw('sum(nominal) as sum_nominal')
	->addSelect('item_id')
	->groupBy('item_id')
	->pluck('sum_nominal', 'item_id');
		$data->getCollection()->transform(function (FakturPembelian $item) use ($transaksiSums) {
			$item->uang_muka_formatted = $transaksiSums[$item->id] ?? 0;

			return $item;
		});

		return $data;
	}

	public function query()
	{
		return $this->model->where('uang_muka', 0)->with('pemasok');
	}

	public function getProdukTipeBarang($produkIDs)
	{
		return $this->model->whereIn('id', $produkIDs)->pluck('tipe_barang');
	}

	public function listPemasok()
	{
		return $this->pemasok->pluck('nama', 'id')->toArray();
	}

	public function listProduk($id = null)
	{
		$canSelected = [0, 1, 2];
		if (null !== $id) {
			// dd($this->model->with('barang.produk')->find($id));
			$canSelected = $this->produk->whereHas('fakturPembelian', function ($query) use ($id) {
				$query->where('faktur_pembelian.id', $id);
			})->pluck('tipe_barang');
		}

		return $this->produk->whereIn('tipe_barang', $canSelected)->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->no_barang . ' | ' . $item->keterangan;

			return $output;
		}, []);
	}

	public function listJasaPengiriman()
	{
		return $this->pengiriman->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama;

			return $output;
		}, []);
	}

	public function listGudang()
	{
		return $this->gudang->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama;

			return $output;
		}, []);
	}

	public function listAkun()
	{
		return $this->akun->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama_akun;

			return $output;
		}, []);
	}

	public function listAkunKode()
	{
		return $this->akun->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->kode_akun . ' | ' . $item->nama_akun;

			return $output;
		}, []);
	}

	public function listTermin()
	{
		return $this->termin->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->akan_dapat_diskon . '/' . $item->jika_membayar_antara . '/n' . $item->jatuh_tempo;

			return $output;
		}, []);
	}

	public function listPajak()
	{
		return $this->kode_pajak->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->kode_pajak_formatted;

			return $output;
		}, []);
	}

	public function getAkunHutang()
	{
		return $this->preferensi->pluck('akun_hutang_id')->first();
	}

	public function dateChecker($data)
	{
		$data['invoice_date'] = (empty($data['invoice_date'])) ? $data['invoice_date'] = date('Y-m-d H:i:s') : $data['invoice_date'] = $this->date->IndonesiaToSql($data['invoice_date']);
		$data['ship_date']    = (empty($data['ship_date'])) ? $data['ship_date']    = date('Y-m-d H:i:s') : $data['ship_date']    = $this->date->IndonesiaToSql($data['ship_date']);

		return $data;
	}

	public function idPrediction(): array
	{
		$data = $this->model->orderBy('id', 'desc')->first()->toArray();
		if ($data) {
			$data = [
				'form_no'   => $this->code->autoGenerate($data, 'form_no'),
				'no_faktur' => $this->code->autoGenerate($data, 'no_faktur'),
			];

			return $data;
		}

		return ['form_no' => 1, 'no_faktur' => 1];
	}

	public function cekDPpemasok($count_dp, $data)
	{
		// for ($y = 0; $y < $count_dp; ++$y) {
		//     $cek_transaksi = TransaksiUangMukaPemasok::where('faktur_uang_muka_pemasok_id', $data['faktur_dp_id'][$y])->get();
		//     if (!empty($cek_transaksi)) {
		//         $angka_yang_sudah_di_dp = $cek_transaksi->sum('jumlah');
		//         $cek_detail = $this->model->find($data['faktur_dp_id'][$y]);
		//         dd($cek_detail);
		//         if ($cek_detail->saldo_awal < $angka_yang_sudah_di_dp) {
		//             throw new \Exception('Maaf, DP yang dipakai berlebihan');
		//         }
		//     }
		// }
	}

	public function getPajakAkun($data)
	{
		$array = [];

		$count = collect($data['produk_id'])->count();
		for ($i = 0; $i < $count; ++$i) {
			if (!empty($data['tax_produk'][$i])) {
				$pajak1   = KodePajak::find($data['tax_produk'][$i][0]);
				$nominal1 = ($data['unit_harga_produk'][$i] * $data['qty_produk'][$i]) * $pajak1->nilai / 100;
				array_push($array, ['nominal' => $nominal1, 'akun' => $pajak1->akun_pajak_pembelian_id]);

				if (collect($data['tax_produk'][$i])->count() > 1) {
					$pajak2   = KodePajak::find($data['tax_produk'][$i][1]);
					$nominal2 = ($data['unit_harga_produk'][$i] * $data['qty_produk'][$i]) * $pajak2->nilai / 100;
					array_push($array, ['nominal' => $nominal2, 'akun' => $pajak2->akun_pajak_pembelian_id]);
				}
			}
		}

		return $array;
	}

	public function sortAkunProduk(array $data): array
	{
		$detailProduk    = Produk::whereIn('id', $data['produk_id'])->get()->toArray();
		$detailProdukArr = [];

		foreach ($data['produk_id'] as $produk) { //cari itemnya.. samakan indexnya..
			$i = 0;
			$x = 0;
			while ($x < 1) {
				if ($detailProduk[$i]['id'] == $produk) {
					$detailProdukArr[] = [
						'akun_barang_terkirim_id' => $detailProduk[$i]['akun_barang_terkirim_id'],
						'akun_belum_tertagih_id'  => $detailProduk[$i]['akun_belum_tertagih_id'],
						'akun_persedian_id'       => $detailProduk[$i]['akun_persedian_id'],
						'akun_hpp_id'             => $detailProduk[$i]['akun_hpp_id'],
					];
					$x = 1;
				}
				++$i;
			}
		}

		return $detailProdukArr;
	}

	public function calculateSortedPajak($unit_price, $total_tax, $array_nilai_pajak, $after_dikurang_diskon)
	{
		$nilai_pajak_1 = !empty($array_nilai_pajak['pajak_1']) ? $array_nilai_pajak['pajak_1'] : null;
		$nilai_pajak_2 = !empty($array_nilai_pajak['pajak_2']) ? $array_nilai_pajak['pajak_2'] : null;

		$temp_tax      = (100 / (100 + $total_tax) * ($unit_price - $after_dikurang_diskon));
		$total_in_tax1 = $temp_tax * ((null != $nilai_pajak_1 ? $nilai_pajak_1->nilai : 0) / 100);
		$total_in_tax2 = $temp_tax * ((null != $nilai_pajak_2 ? $nilai_pajak_2->nilai : 0) / 100);
		$sum_in_tax    = round($total_in_tax1, 2) + round($total_in_tax2, 2);

		$all_pajak = [
			'pajak_1'       => round($total_in_tax1, 2),
			'pajak_2'       => round($total_in_tax2, 2),
			'nilai_pajak_1' => $nilai_pajak_1,
			'nilai_pajak_2' => $nilai_pajak_2,
			'sum_pajak'     => $sum_in_tax,
		];

		return $all_pajak;
	}

	public function sumUnitPrice($data)
	{
		$array_unit_price = [];

		foreach ($data['unit_harga_produk'] as $key => $unit_price) {
			$sum_qty_price             = $unit_price * $data['qty_produk'][$key];
			$sum_qty_price_with_diskon = $sum_qty_price - (($data['diskon_produk'][$key] / 100) * $sum_qty_price);
			$array_unit_price[]        = $sum_qty_price_with_diskon;
		}

		return $array_unit_price;
	}

	public function totalTax($data_pajak)
	{
		$total_pajak = (!empty($this->checkNilaiPajak($data_pajak)['nilai_pajak_1']) ? $this->checkNilaiPajak($data_pajak)['nilai_pajak_1']->nilai : null) + (!empty($this->checkNilaiPajak($data_pajak)['nilai_pajak_2']) ? $this->checkNilaiPajak($data_pajak)['nilai_pajak_2']->nilai : null);

		return $total_pajak;
	}

	public function checkNilaiPajak($data_pajak)
	{
		$nilai_pajak_1 = null;
		$nilai_pajak_2 = null;

		if (array_key_exists('pajak_1', $data_pajak)) {
			if (!empty($data_pajak['pajak_1'])) {
				$nilai_pajak_1 = $data_pajak['pajak_1'];
			}
		}

		if (array_key_exists('pajak_2', $data_pajak)) {
			if (!empty($data_pajak['pajak_2'])) {
				$nilai_pajak_2 = $data_pajak['pajak_2'];
			}
		}

		return [
			'nilai_pajak_1' => $nilai_pajak_1,
			'nilai_pajak_2' => $nilai_pajak_2,
		];
	}

	public function findNilaiPajak($data_pajak)
	{
		$pajak_1 = null;
		$pajak_2 = null;

		if (!empty($data_pajak)) {
			$pajak_1 = KodePajak::find($data_pajak[0]);
			if (collect($data_pajak)->count() > 1) {
				$pajak_2 = KodePajak::find($data_pajak[1]);
			}
		}

		return [
			'pajak_1' => $pajak_1,
			'pajak_2' => $pajak_2,
		];
	}

	public function findPajak($data, $model)
	{
		$nilai_pajak_akhir = [];
		// rumus mencari pajak in tax
		// mencari array price per unit
		$array_unit_price = $this->sumUnitPrice($data);

		foreach ($array_unit_price as $key => $unit_price) {
			// cek kode pajak
			$kode_pajak_id = array_key_exists('kode_pajak_id', $data) ? $data['kode_pajak_id'] : null;
			// mencari array nilai pajak
			$array_nilai_pajak = $this->findNilaiPajak($kode_pajak_id);
			// mencari total pajak
			$total_tax             = $this->totalTax($array_nilai_pajak);
			$after_dikurang_diskon = $this->afterDikurangDiskon($unit_price, $array_unit_price, $data['total_diskon_faktur']);
			// mencari nilai pajak akhir
			$nilai_pajak_akhir[] = $this->calculateSortedPajak($unit_price, $total_tax, $array_nilai_pajak, $after_dikurang_diskon);
		}

		return $nilai_pajak_akhir;
	}

	public function afterDikurangDiskon($unit_price, $array_unit_price, $diskon)
	{
		return ($unit_price / array_sum($array_unit_price)) * $diskon;
	}

	public function insertSubItemPenerimaan($model, $data)
	{
		$items               = [];
		$transaction_purpose = [];
		$arrDebit            = [];
		$arrKredit           = [];
		$produkDetail        = [];

		$pajak                = $this->findPajak($data, $model);
		$price_sum            = $this->sumUnitPrice($data);
		$data['receive_date'] = (empty($data['receive_date'])) ? $data['receive_date'] = date('Y-m-d H:i:s') : $data['receive_date'] = $this->date->IndonesiaToSql($data['receive_date']);
		$jam_sekarang         = Carbon::now()->format('Y-m-d H:i:s');
		$count                = count($data['produk_id']);

		$detailProdukArr = $this->reSortingDetailProduk($data);

		for ($i = 0; $i < $count; ++$i) {
			$diskon_untuk_pajak = 0;
			if ($data['diskon_produk'][$i] > 0) {
				$diskon_master = $sum_unit_price * ($data['diskon_produk'][$i] / 100);
			} else {
				$diskon_master = 0;
			}
			$barang                  = $data['unit_harga_produk'][$i] * $data['qty_produk'][$i];
			$total_diskon            = ($barang * ($data['diskon_produk'][$i] / 100));
			$persen_barang           = $data['amount_produk'][$i] / $price_sum[$i];
			$total_diskon_master     = $diskon_master * $persen_barang;
			$total_barang            = ($barang - $total_diskon - $total_diskon_master) - (!empty($pajak[$i]['sum_pajak']) ? $pajak[$i]['sum_pajak'] : 0);
			$produk_detail_kuantitas = SaldoAwalBarang::where(['produk_id' => $data['produk_id'][$i]])->orderBy('id', 'desc')->get()->sum('kuantitas');
			if ($produk_detail_kuantitas <= 0) {
				$produk_detail_kuantitas = 0;
			}
			$produk_detail_modal     = SaldoAwalBarang::where(['produk_id' => $data['produk_id'][$i]])->orderBy('id', 'desc')->get();
			$modal                   = $produk_detail_modal->first()->harga_modal ?? 0;
			$sumQTy                  = $produk_detail_kuantitas;
			$harga_terakhir          = $total_barang / $data['qty_produk'][$i];
			if ($produk_detail_kuantitas <= 0) {
				$harga_modal_1           = $total_barang + $modal;
			}else {
				$harga_modal_1           = $total_barang + ($modal * $sumQTy);
			}
			$harga_modal             = ($harga_modal_1 / ($data['qty_produk'][$i] + $sumQTy));
			$pesanan_pembelian       = (empty($data['barang_pesanan_pembelian_id'][$i])) ? null : $data['barang_pesanan_pembelian_id'][$i];

			$items = [
				'produk_id'                   => $data['produk_id'][$i],
				'penerimaan_pembelian_id'     => $model->id,
				'barang_pesanan_pembelian_id' => $pesanan_pembelian,
				'jumlah'                      => $data['qty_produk'][$i],
				'satuan'                      => $data['satuan_produk'][$i],
				'gudang_id'                   => $data['gudang_id'][$i],
				'harga_modal'                 => $data['amount_produk'][$i],
				'sn'                          => $data['sn'][$i],
				// 'status' => 1,
				'created_at' => $jam_sekarang,
				'updated_at' => $jam_sekarang,
			];
			// $total_barang_setelah_pajak = $total_barang - (array_key_exists('sum_pajak', $pajak) ? $pajak['sum_pajak'] : 0);

			$arrDebit = [
				'nominal' => $total_barang ?? 0,
				'tanggal' => $data['receive_date'],
				'produk'  => $data['produk_id'][$i],
				'dari'    => 'Total Penerimaan Barang Debit',
				'desc'    => 'PenerimaanBarang |' . $model->id,
				'akun'    => $detailProdukArr[$i]['akun_persedian_id'],
			];

			$arrKredit[] = [
				'nominal' => $total_barang ?? 0,
				'tanggal' => $data['receive_date'],
				'produk'  => $data['produk_id'][$i],
				'dari'    => 'Total Penerimaan Barang Kredit',
				'desc'    => 'PenerimaanBarang |' . $model->id,
				'akun'    => $detailProdukArr[$i]['akun_belum_tertagih_id'],
			];

			$persedian      = $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrDebit, 1, $data['receipt_no']);
			$produkDetail[] = [
				'no_faktur'      => $data['form_no'],
				'tanggal'        => $data['receive_date'],
				'kuantitas'      => $data['qty_produk'][$i],
				'harga_modal'    => $harga_modal,
				'harga_terakhir' => $harga_terakhir,
				'gudang_id'      => !empty($data['gudang_id']) ? $data['gudang_id'][$i] : null,
				'transaksi_id'   => $persedian->id,
				'produk_id'      => $data['produk_id'][$i],
				'sn'             => $data['sn'][$i],
				'status'         => 1,
				'item_type'      => get_class($this->model),
				'item_id'        => $model->id,
				'created_at'     => $jam_sekarang,
			];

			$brg_penerimaan_pembelian = BarangPenerimaanPembelian::create($items);

			$bantu[] = [
				'produk_id'                   => $data['produk_id'][$i],
				'brg_penerimaan_pembelian_id' => $brg_penerimaan_pembelian->id,
			];
			$dataPenerimaanId[] = $brg_penerimaan_pembelian->id;
		}

		$this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrKredit, 0, $data['receipt_no']);

		BantuanPenerimaanPembelian::insert($bantu);
		SaldoAwalBarang::insert($produkDetail);

		return $dataPenerimaanId;
	}

	public function reSortingDetailProduk(array $data): array
	{
		$detailProduk    = Produk::whereIn('id', $data['produk_id'])->get()->toArray();
		$detailProdukArr = [];

		foreach ($data['produk_id'] as $produk) { //cari itemnya.. samakan indexnya..
			$i = 0;
			$x = 0;
			while ($x < 1) {
				if ($produk == $detailProduk[$i]['id']) {
					$detailProdukArr[] = [
						'akun_persedian_id'      => $detailProduk[$i]['akun_persedian_id'],
						'akun_belum_tertagih_id' => $detailProduk[$i]['akun_belum_tertagih_id'],
					];
					$x = 1;
				}
				++$i;
			}
		}

		return $detailProdukArr;
	}
}
