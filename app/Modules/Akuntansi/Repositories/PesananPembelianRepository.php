<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\BarangPesananPembelian;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\JasaPengiriman;
use App\Modules\Akuntansi\Models\KategoriProduk;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\PesananPembelian;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use Carbon\carbon;
use Generator\Interfaces\RepositoryInterface;
use Helpers\CodeHelper;
use Helpers\IndonesiaDate as DateHelper;

class PesananPembelianRepository implements RepositoryInterface
{
    public function __construct(PesananPembelian $model, KategoriProduk $kategori, Gudang $gudang, Akun $akun,InformasiPemasok $pemasok, SyaratPembayaran $termin, Produk $produk,JasaPengiriman $pengiriman, CodeHelper $code, DateHelper $date, PreferensiMataUang $preferensi, KodePajak $kode_pajak)
    {
        $this->produk = $produk;
        $this->model = $model;
        $this->kategori = $kategori;
        $this->gudang = $gudang;
        $this->akun = $akun;
        $this->pemasok = $pemasok;
        $this->termin = $termin;
        $this->pengiriman = $pengiriman;
        $this->code = $code;
        $this->date = $date;
        $this->preferensi = $preferensi;
        $this->kode_pajak = $kode_pajak;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $pesanan_pembelian                  = $this->model->findOrFail($id);
        $pesanan_pembelian->po_date         = Carbon::parse($pesanan_pembelian->po_date)->format('d F Y');
        $pesanan_pembelian->expected_date   = Carbon::parse($pesanan_pembelian->expected_date)->format('d F Y');
        $pesanan_pembelian                  = $this->formattingToNumeric($pesanan_pembelian);
        return $pesanan_pembelian;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_pesanan_pembelian')){
            return abort(403);
        }
        
        $model = $this->model->findOrFail($id);
        BarangPesananPembelian::where('pesanan_pembelian_id',$model->id)->delete();        
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $checkData = PesananPembelian::where('po_number', $data['po_number'])
                    ->where('id', '!=', $id)->first();
        
        if($checkData){
			throw new \Exception('Nomor PO terduplikat!');            
        }
        
        $data = $this->formattingAllData($data);
        $data['po_number'] = $this->code->autoGenerate($this->model, "po_number", $data['po_number']);
        
        $data['taxable'] = (empty($data['taxable'])) ? 0 : 1 ;
        $data['in_tax'] = (empty($data['in_tax'])) ? 0 : 1 ;
        $model = $this->findItem($id)->fill($this->dateChecker($data));
        BarangPesananPembelian::where('pesanan_pembelian_id',$model->id)->delete();        
        if($model->save()){
            $this->insertSubItem($model, $data);
        }    
        
        if(!empty($data['lanjutkan'])){
            if($data['lanjutkan'] == 'Simpan & Baru'){
                $model['continue_stat'] = 2;                
            }else{
                $model['continue_stat'] = 1;
            }
        }        
        
        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->formattingAllData($data);
        $data['po_number'] = $this->code->autoGenerate($this->model, "po_number", $data['po_number']);
        $data['taxable'] = (empty($data['taxable'])) ? 0 : 1 ;
        $data['in_tax'] = (empty($data['in_tax'])) ? 0 : 1 ;
        $model = $this->model->fill($this->dateChecker($data));
        if($model->save()){
            $this->insertSubItem($model, $data);
        }

        if(!empty($data['lanjutkan'])){
            if($data['lanjutkan'] == 'Simpan & Baru'){
                $model['continue_stat'] = 2;                
            }else{
                $model['continue_stat'] = 1;
            }
		}        

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(10);
    }

    public function dateChecker($data)
    {
        $data['po_date'] = (empty($data['po_date'])) ? $data['po_date'] = date('Y-m-d H:i:s') : $data['po_date'] = $this->date->IndonesiaToSql($data['po_date']); ;
        $data['expected_date'] = (empty($data['expected_date'])) ? $data['expected_date'] = date('Y-m-d H:i:s') : $data['expected_date'] = $this->date->IndonesiaToSql($data['expected_date']); ;
        return $data;
    }

    public function listKategoriProduk()
    {
        return $this->kategori->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }
    public function listGudang()
    {
        return $this->gudang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }
    public function listAkun()
    {
        return $this->akun->whereDoesntHave('childAkun')->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }
    public function listPemasok()
    {
        return $this->pemasok->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }
    public function listProduk()
    {
        return $this->produk->where('tipe_barang','!==',3)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->no_barang." | ".$item->keterangan;

            return $output;
        }, []);
    }
    public function listTermin()
    {
        return $this->termin->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->akan_dapat_diskon.'/'.$item->jika_membayar_antara.'/n'.$item->jatuh_tempo ;

            return $output;
        }, []);
    }
    public function listJasaPengiriman()
    {
        return $this->pengiriman->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listPajak()
    {
        return $this->kode_pajak->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->kode_pajak_formatted;

            return $output;
        }, []);
    }

    public function getAkunDp()
    {
        return $this->preferensi->pluck('uang_muka_pembelian_id')->first();
    }
    
    public function idPrediction()
    {
        $data = $this->model->orderBy('id','desc')->first();
        if($data){
            return $data->id + 1;
        }
        return 1;
    }
    
    public function insertSubItem($model, $data)
    {
        $items = [];
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        $count = count($data['produk_id']);
        for ($i=0; $i < $count; $i++) {
                $ditutup = (empty($data['ditutup'][$i])) ? 0 : $data['ditutup'][$i];
                $permintaan_pembelian = (empty($data['barang_permintaan_pembelian_id'][$i])) ? null : $data['barang_permintaan_pembelian_id'][$i];
                $pajak1     = null;
                $pajak2     = null;

                if(!empty($data['kode_pajak_id'][$i])){
                    // data ada.. walau hanya satu
                    $pajak1 = $data['kode_pajak_id'][$i][0];
                    if(count($data['kode_pajak_id'][$i]) > 1){
                        $pajak2 = $data['kode_pajak_id'][$i][1];
                    }
                }

                $items[] = [
                    'produk_id' => $data['produk_id'][$i],
                    'pesanan_pembelian_id' => $model->id,
                    'barang_permintaan_pembelian_id' => $permintaan_pembelian,
                    'item_deskripsi' => $data['keterangan_produk'][$i],
                    'harga' => $data['unit_harga_produk'][$i],
                    'harga_modal' => $data['harga_modal'][$i],
                    'satuan' => $data['satuan_produk'][$i],
                    'diskon' => $data['diskon_produk'][$i],
                    'jumlah' => $data['qty_produk'][$i],
                    'kode_pajak_id' => $pajak1,                
                    'kode_pajak_2_id' => $pajak2,
                    'harga' => $data['unit_harga_produk'][$i],
                    'ditutup' => $ditutup,
                    'created_at' => $jam_sekarang,
                    'updated_at' => $jam_sekarang,
                ];
            }
            // dd($items);
            BarangPesananPembelian::insert($items);
    }

    public function formattingAllData($data)
    {
        // data faktur
        $data['ongkir']                 = floatval(preg_replace('/[^\d.]/', '', $data['ongkir']));
        $data['total_potongan_rupiah']  = floatval(preg_replace('/[^\d.]/', '', $data['total_potongan_rupiah']));
   
        // data produk
        if (!empty($data['produk_id'])) {
            for ($i=0; $i < count($data['produk_id']); $i++) { 
                $data['unit_harga_produk'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['unit_harga_produk'][$i]));
                $data['amount_produk'][$i]     = floatval(preg_replace('/[^\d.]/', '', $data['amount_produk'][$i]));
            }
        }
        return $data;
    }

    public function formattingToNumeric($data)
    {
        if (!empty($data->ongkir)) {
            $data->ongkir = number_format($data->ongkir); 
        }
        if (!empty($data->total_potongan_rupiah)) {
            $data->total_potongan_rupiah  = number_format($data->total_potongan_rupiah); 
        }
        return $data;
    }
}