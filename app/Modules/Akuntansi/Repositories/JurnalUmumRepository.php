<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\DetailJurnalUmum;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\JurnalUmum;
use App\Modules\Akuntansi\Models\Transaksi;
use Helpers\TransaksiHelper;
use Carbon\carbon;
use Cart;
use Generator\Interfaces\RepositoryInterface;
use Session;

class JurnalUmumRepository implements RepositoryInterface
{
    public function __construct(JurnalUmum $model, Akun $akun, TransaksiHelper $transaksi_helper, Identitas $identitas)
    {
        $this->model = $model;
        $this->akun = $akun;
        $this->identitas = $identitas;
        $this->transaksi_helper = $transaksi_helper;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $model = $this->model->findOrFail($id);
        $this->formattingField($model);

        $this->removeSession('update', $model);

        return $model;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if( !auth()->user()->hasPermissionTo('hapus_bukti_jurnal_umum') ){
            return abort(403);
        }

        // Deleted By Jurnal Umum
        $this->deletedByUser($id);

        $model = $this->findItem($id);

        // Deleted BY Detail jurnal Umum
        $this->updateDeletedByDetailJurnalUmum($model);
        
        // Deleted By Transaksi
        $this->updateDeletedByTransaksi($model);
        
        $this->transaksi_helper->ExecuteTransactionSingle('delete', $model);
        $model->detailJurnalUmum()->delete();
        return $model->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data  = $this->updatedByUser($data);
        $input = [
            'no_faktur' => $data['no_faktur_jurmum'],
            'tanggal' => Carbon::parse($data['tanggal_jurmum'])->format('Y-m-d H:i:s'),
            'description' => $data['description_jurmum'],
            'updated_by'    => $data['updated_by'],
        ];

        $model = $this->findItem($id)->fill($input);
        if ($model->save()) {
            $this->transaksi_helper->ExecuteTransactionSingle('delete', $model);
            $model->detailJurnalUmum()->delete();

            $this->saveCart($data, $model);
        }

        return $model;
    }

    public function saveCart($data, $model)
    {
        $cart = $this->getCart()->content();

        $index = 1;
        foreach ($cart as $value) {
            $akunIds = $this->akun->where('kode_akun', $value->options->akun)->pluck('id');

            $this->saveTransaksi($value, $model, $index, $akunIds,'update');

            $newDetailJurnal = DetailJurnalUmum::create([
                'akun_id' => $akunIds->first(),
                'jurnal_umum_id' => $model->id,
                'debit' => $value->options->debet,
                'kredit' => $value->options->kredit,
                'memo' => $value->options->memo,
                'department' => $value->options->departement,
                'subsidiary_ledger' => $value->options->ledger,
                'project' => $value->options->project,
                'updated_by' => auth()->user()->id,
                'created_by'    => $model->created_by,
            ]);
            ++$index;
        }

        $this->getCart()->destroy();

        $this->flushSessionDataUpdate();
        $this->flushSessionCartUpdate();
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data  = $this->createdByUser($data);
        $input = [
            'no_faktur' => $data['no_faktur_jurmum'],
            'tanggal' => Carbon::parse($data['tanggal_jurmum'])->format('Y-m-d H:i:s'),
            'description' => $data['description_jurmum'],
            'created_by'  => $data['created_by'],
            ];
        $cart = cart::instance('jurmum'.auth()->user()->id)->content();

        $model = $this->model->fill($input);

        if ($model->save()) {
            $index = 1;
            foreach ($cart as $value) {
                $akun = Akun::where('kode_akun', $value->options->akun)->pluck('id');
                $jurnal_umum = new DetailJurnalUmum();
                $jurnal_umum->akun_id = $akun->first();
                $jurnal_umum->jurnal_umum_id = $model->id;
                $jurnal_umum->debit = $value->options->debet;
                $jurnal_umum->kredit = $value->options->kredit;
                $jurnal_umum->memo = $value->options->memo;
                $jurnal_umum->departement = $value->options->departement;
                $jurnal_umum->subsidiary_ledger = $value->options->ledger;
                $jurnal_umum->project = $value->options->project;
                $jurnal_umum->created_by = auth()->user()->id;
                $jurnal_umum->save();
                $this->saveTransaksi($value, $model, $index, $akun,'create');
                ++$index;
            }
            Cart::instance('jurmum'.auth()->user()->id)->destroy();
            Session::forget([
                  'description_jurmum',
                  'no_faktur_jurmum',
                  'tanggal_jurmum',
                  'create',
                ]);
        }

        return $model;
    }

    public function saveTransaksi($value, $model, $index, $akun, $type)
    {
        $transaksi_detail = new Transaksi();
        $nama_kelas = (new \ReflectionClass(get_class($model)))->getShortName();

        if ($value->options->kredit) {
            $transaksi_detail->nominal = $value->options->kredit;
            $transaksi_detail->status = 0;
        } else {
            $transaksi_detail->nominal = $value->options->debet;
            $transaksi_detail->status = 1;
        }

        $transaksi_detail->akun_id = $akun->first();
        $transaksi_detail->tanggal = $model->tanggal;
        $transaksi_detail->item_id = $model->id;
        $transaksi_detail->item_type = get_class($model);
        $transaksi_detail->no_transaksi = $model->no_faktur;
        $transaksi_detail->dari = 'Jurnal Umum '.$index;
        $transaksi_detail->sumber = $this->transaksi_helper->fromCamelCase($nama_kelas, ' ');
        $transaksi_detail->keterangan = $this->transaksi_helper->fromCamelCase($nama_kelas, ' ').' '.$model->no_faktur;
        $transaksi_detail = $this->saveCreatedOrUpdated($transaksi_detail, $type,$model);

        return $transaksi_detail->save();
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(10);
    }

    public function listAkun()
    {
        return $this->akun->whereDoesntHave('childAkun')->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function totalCart()
    {
        $items = $this->getCart()->content();

        $total = 0;

        $items->each(function ($item) use (&$total) {
            if ($item->options->type === 'debet') {
                $total = $total + $item->options->debet;
            } elseif ($item->options->type === 'kredit') {
                $total = $total - $item->options->kredit;
            }
        });

        return $total;
    }

    public function getCart()
    {
        return session()->has('update') && session()->get('update') === true
                ? Cart::instance('jurmum-edit'.auth()->user()->id)
                : Cart::instance('jurmum'.auth()->user()->id);
    }

    public function removeSession($type, $model)
    {
        if ($type === 'create') {
            $this->setSessionCreate();
        } else {
            $this->setSessionUpdate($model);
        }
    }

    public function setSessionCreate()
    {
        if (!session()->has('create') && session()->get('create') !== true) {
            session([
                'create' => true,
            ]);
        }

        if (session()->has('update') && session()->get('update') === true) {
            $this->flushSessionDataUpdate();
            $this->flushSessionCartUpdate();
        }
    }

    public function flushSessionDataUpdate()
    {
        session()->forget([
            'update',
            'data-id',
            'description_jurmum',
            'no_faktur_jurmum',
            'tanggal_jurmum',
        ]);
    }

    public function flushSessionCartUpdate()
    {
        Cart::instance('jurmum-edit'.auth()->user()->id)->destroy();
    }

    public function setSessionUpdate($model)
    {
        if (!session()->has('update') && session()->get('update') !== true) {
            session([
                'update' => true,
            ]);

            $this->setSessionUpdateData($model);
        } else {
            if (session()->get('data-id') !== $model->id) {
                $this->flushSessionDataUpdate();
                $this->flushSessionCartUpdate();

                $this->setSessionUpdate($model);
            }
        }
    }

    public function setSessionUpdateData($model)
    {
        $this->setSessionCartUpdate($model);
        $this->setSessionDataUpdate($model);
    }

    public function setSessionCartUpdate($model)
    {
        $model->detailJurnalUmum->each(function ($item) {
            $type = $this->getTypeJurnal($item);

            $uniqueId = Cart::instance('jurmum-edit'.auth()->user()->id)
                              ->content()
                              ->pluck('id')
                              ->toArray();

            $newUniqueId = uniqid();
            while (in_array($newUniqueId, $uniqueId)) {
                $newUniqueId = uniqid();
            }

            Cart::instance('jurmum-edit'.auth()->user()->id)->add([
                'id' => $newUniqueId,
                'name' => $item->akun->nama_akun,
                'qty' => 1,
                'price' => 1,
                'options' => [
                    'akun' => $item->akun->kode_akun,
                    'type' => $type,
                    'debet' => $item->debit,
                    'kredit' => $item->kredit,
                    'memo' => $item->memo,
                    'department' => $item->departement,
                    'ledger' => $item->subsidiary_ledger,
                    'project' => $item->project,
                ],
            ]);
        });
    }

    public function getTypeJurnal($item)
    {
        return $item->debit === null
               ? 'kredit'
               : 'debet';
    }

    public function setSessionDataUpdate($model)
    {
        session([
            'update' => true,
            'data-id' => $model->id,
            'description_jurmum' => $model->description,
            'no_faktur_jurmum' => $model->no_faktur,
            'tanggal_jurmum' => date('d-m-Y', strtotime($model->tanggal)),
        ]);
    }

    public function formattingField($model)
    {
        if (!empty($model->tanggal)) {
            $model->tanggal = Carbon::parse($model->tanggal)->format('d F Y');
        }
    }

    public function createdByUser($data)
    {
        $data['created_by'] = auth()->user()->id;

        return $data;
    }

    public function updatedByUser($data)
    {
        $data['updated_by'] = auth()->user()->id;

        return $data;
    }

    public function deletedByUser($id)
    {
        $data = [];

        $data['deleted_by'] = auth()->user()->id;

        $this->model->findOrFail($id)->update($data);

        return true;
    }

    public function saveCreatedOrUpdated($transaksi_detail,$type, $model)
    {
        if ($type == 'create') {
            $transaksi_detail->created_by = auth()->user()->id;
        }else {
            $transaksi_detail->created_by = $model->created_by;
            $transaksi_detail->updated_by = auth()->user()->id;
        }

        return $transaksi_detail;
    }

    public function updateDeletedByDetailJurnalUmum($model)
    {
        $update = DetailJurnalUmum::where('jurnal_umum_id', $model->id)->update([
            'deleted_by'    => auth()->user()->id,
        ]);

        return true;
    }

    public function updateDeletedByTransaksi($model)
    {
        $update = Transaksi::where(['item_id' => $model->id, 'item_type' => get_class($model)])->update([
            'deleted_by'    => auth()->user()->id,
        ]);

        return true;
    }

    public function cetakJurnalUmum($data)
    {
        $cart = $this->getCart()->content();
        $debit = 0; $kredit = 0;
        foreach ($cart as $cartPembayaran){
            $debit += $cartPembayaran->options->debet ?? 0;
            $kredit += $cartPembayaran->options->kredit ?? 0 ;
        }
        $items = $data->all();
        $identitas = $this->identitas->first();
        $view = [
            'items' => $items,
            'identitas' => $identitas,
            'cart' => $cart,
            'debit' => number_format($debit),
            'kredit' => number_format($kredit),
        ];
        return view('akuntansi::laporan.cetak_laporan.cetak_jurnal_umum')->with($view);
    }
}
