<?php

namespace App\Modules\Akuntansi\Repositories;

use Generator\Interfaces\RepositoryInterface;
use Carbon\Carbon;
use App\Modules\Pos\Models\PreferensiPos;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\KodePajak;
use DB;

class PreferensiPosRepositories implements RepositoryInterface 
{
    public function __construct(PreferensiPos $preferensiPos,
                                Akun $akun,
                                KodePajak $kodePajak)
    {
        $this->preferensiPos = $preferensiPos;
        $this->akun = $akun;
        $this->kodePajak = $kodePajak;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->findItem($id)->fill($data);
        $model->save();

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = request()->all();

        $preferensiPos = $this->preferensiPos->first();

        if ($preferensiPos === null) {
            $preferensiPos = $this->preferensiPos->create($data);
        } else {
            $preferensiPos->update($data);
        }

        return $preferensiPos;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }

    public function listAkun()
    {
        return $this->akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function listPajak()
    {
        return $this->kodePajak->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama . ' - ' . $item->nilai . '%';

            return $output;
        }, []);
    }

    public function listAkunBank()
    {
        return $this->akun->where('tipe_akun_id', 1)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function getAkun($request)
    {
        try {
            $akun = $this->akun->find($request->id);

            if ($akun === null) {
                $response = [
                    'status' => false
                ];
            } else {
                $response = [
                    'status' => true,
                    'data' => $akun
                ];
            }

        } catch (Exception $e) {
            $response = [
                'status' => false
            ];
        }

        return response()->json($response);
    }

    public function listPreferensiPos()
    {
        return $this->preferensiPos->with(['akunCash', 'akunHutang', 'kodePajak', 'akunBank'])->first();
    }

    public function storePos($request)
    {
        DB::beginTransaction();
        
        try {
            $data = $request->all();

            $preferensiPos = $this->preferensiPos->first();

            if ($preferensiPos === null) {
                $preferensiPos = $this->preferensiPos->create($data);
            } else {
                $preferensiPos->update($data);
            }

            DB::commit();

            return redirect()->route('akuntansi.preferensi-pos.index')->withMessage('Berhasil Menambah/Memperbarui data');
        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->withInput()->withErrors([
                'message' => $e->getMessage()
            ]);
        }

        return redirect()->back();
    }

    public function storePajak($request)
    {
        DB::beginTransaction();
        
        try {
            $data = $request->all();

            if ( ( $data['status'] ?? null ) === null) {
                $data['status'] = 0;
            }

            $preferensiPos = $this->preferensiPos->first();

            if ($preferensiPos === null) {
                $preferensiPos = $this->preferensiPos->create($data);
            } else {
                $preferensiPos->update($data);
            }

            DB::commit();

            return redirect()->route('akuntansi.preferensi-pos.index')->withMessage('Berhasil Menambah/Memperbarui data');
        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->withInput()->withErrors([
                'message' => $e->getMessage()
            ]);
        }

        return redirect()->back();
    }

    public function storeModal($request)
    {
        DB::beginTransaction();

        try {

            $data = $request->all();

            $data['uang_modal'] = floatval(preg_replace('/[^\d.]/', '', $data['uang_modal']));

            if ( ( $data['tanggal_custom'] ?? null ) === null) {
                    $data['tanggal_custom'] = 0;
            }

            $preferensiPos = $this->preferensiPos->first();

            if ($preferensiPos === null) {
                $preferensiPos = $this->preferensiPos->create($data);
            } else {
                $preferensiPos->update($data);
            }

            DB::commit();

            return redirect()->route('akuntansi.preferensi-pos.index')->withMessage('Berhasil Menambah/Memperbarui data');

        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->withInput()->withErrors([
                'message' => $e->getMessage()
            ]);
        }

        return redirect()->back();
    }
}