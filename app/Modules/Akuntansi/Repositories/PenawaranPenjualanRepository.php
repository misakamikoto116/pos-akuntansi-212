<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\PenawaranPenjualan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\KategoriProduk;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\BarangPenawaranPenjualan;
use Carbon\carbon;
use Helpers\IndonesiaDate as DateHelper;
use Helpers\CodeHelper;
use Generator\Interfaces\RepositoryInterface;
use DB;
use Validator;

class PenawaranPenjualanRepository implements RepositoryInterface
{
    public function __construct(PenawaranPenjualan $model, CodeHelper $code, KategoriProduk $kategori, Gudang $gudang, Akun $akun,InformasiPelanggan $pelanggan, KodePajak $kode_pajak, Produk $produk, DateHelper $date)
    {
        $this->produk = $produk;
        $this->model = $model;
        $this->kategori = $kategori;
        $this->gudang = $gudang;
        $this->akun = $akun;
        $this->pelanggan = $pelanggan;
        $this->kode_pajak = $kode_pajak;
        $this->date = $date;
        $this->code = $code;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $penawaran_penjualan = $this->model->findOrFail($id);
        $penawaran_penjualan->tanggal_formatted = $penawaran_penjualan->tanggal->format('d F Y');
        $penawaran_penjualan->ongkir = number_format($penawaran_penjualan->ongkir, 2, ".", ","); 
        $penawaran_penjualan->total_potongan_rupiah = number_format($penawaran_penjualan->total_potongan_rupiah, 2, ".", ","); 
        return $penawaran_penjualan;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_penawaran_penjualan')){
            return abort(403);
        }
        $check = $this->model->findOrFail($id);
        BarangPenawaranPenjualan::where('penawaran_penjualan_id',$check->id)->delete();
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $checkData = PenawaranPenjualan::where('no_penawaran', $data['no_penawaran'])
                    ->where('id', '!=', $id)->first();
        
        if($checkData){
			throw new \Exception('Nomor Penawaran terduplikat!');            
        }

        $data = $this->formattingSingleData($data);
        $data['no_penawaran'] = $this->code->autoGenerate($this->model, "no_penawaran", $data['no_penawaran']);

        if(empty($data['tanggal'])){
            $data['tanggal'] = date('Y-m-d');
        }else{
            $data['tanggal'] = $this->date->IndonesiaToSql($data['tanggal']);
        }

        $data['taxable'] = (empty($data['taxable'])) ? 0 : 1 ;
        $data['in_tax'] = (empty($data['in_tax'])) ? 0 : 1 ;

        # mau pake firstOrCreate tapi nama ga bisa dari pelanggan_id kk
        $pelanggan = InformasiPelanggan::find($data['pelanggan_id']);
        if(empty($pelanggan)){
            $pelanggan = InformasiPelanggan::create([
                'nama' => $data['pelanggan_id'],
                'no_pelanggan' => $data['no_pelanggan'],
                'alamat' => $data['alamat_asal']
            ]);
        }

        $data['pelanggan_id'] = $pelanggan->id;

        $model = $this->model->findOrFail($id)->fill($data);
            if($model->save()){
                BarangPenawaranPenjualan::where('penawaran_penjualan_id',$model->id)->delete();        
                $this->InsertBarangPenawaranPenjualan($data, $model);
            }

        if(!empty($data['lanjutkan'])){
            if($data['lanjutkan'] == 'Simpan & Baru'){
                $model['continue_stat'] = 2;                
            }else{
                $model['continue_stat'] = 1;
            }
        }   

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->formattingSingleData($data);
        $data['no_penawaran'] = $this->code->autoGenerate($this->model, "no_penawaran", $data['no_penawaran']);
        
        if(empty($data['tanggal'])){
            $data['tanggal'] = date('Y-m-d');
        }else{
            $data['tanggal'] = $this->date->IndonesiaToSql($data['tanggal']);
        }
        # mau pake firstOrCreate tapi nama ga bisa dari pelanggan_id kk
        $pelanggan = InformasiPelanggan::find($data['pelanggan_id']);
        if(empty($pelanggan)){
            $pelanggan = InformasiPelanggan::create([
                'nama' => $data['pelanggan_id'],
                'no_pelanggan' => $data['no_pelanggan'],
                'alamat' => $data['alamat_asal']
            ]);
        }
        $data['pelanggan_id'] = $pelanggan->id;
        $model = $this->model->fill($data);
        
        if($model->save()){
            $this->InsertBarangPenawaranPenjualan($data, $model);
        }
        if(!empty($data['lanjutkan'])){
            if($data['lanjutkan'] == 'Simpan & Baru'){
                $model['continue_stat'] = 2;                
            }else{
                $model['continue_stat'] = 1;
            }
        }        

        return $model;
        
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(10);
    }
    public function listKategoriProduk()
    {
        return $this->kategori->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }
    public function listGudang()
    {
        return $this->gudang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }
    public function listAkun()
    {
        return $this->akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }
    public function listPelanggan()
    {
        return $this->pelanggan->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }
    public function listProduk()
    {
        return $this->produk->where('tipe_barang','!==',3)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->no_barang." | ".$item->keterangan;

            return $output;
        }, []);
    }

    public function listKodePajak($kode_pelanggan = null)
    {
        return $this->kode_pajak->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->kode_pajak_formatted;

            return $output;
        }, []);
    }

    public function idPrediction()
    {
        $data = $this->model->orderBy('id','desc')->first();
        if($data){
            return $this->code->autoGenerate($this->model, "no_penawaran");
        }
        return 1;
    }

    public function InsertBarangPenawaranPenjualan($data, $model){
        $count = count($data['produk_id']);        
        $items = [];
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');

        for ($i=0; $i < $count; $i++) {
            
            $ditutup    = (empty($data['ditutup'][$i])) ? 0 : $data['ditutup'][$i];
            $terproses  = (empty($data['terproses'][$i])) ? 0 : $data['terproses'][$i];

            $data = $this->formattingArrayData($data, $i);
            $items[] = [
                'produk_id' => $data['produk_id'][$i],
                'penawaran_penjualan_id' => $model->id,
                'item_deskripsi' => $data['keterangan_produk'][$i],
                'jumlah' => $data['qty_produk'][$i],
                'harga' => $data['unit_harga_produk'][$i],
                'satuan' => $data['satuan_produk'][$i],
                'diskon' => $data['diskon_produk'][$i],
                'harga_modal' => $data['harga_modal'][$i],
                'kode_pajak_id' => $data['kode_pajak_id'][$i][0] ?? null,                
                'kode_pajak_2_id' => $data['kode_pajak_id'][$i][1] ?? null,                
                'terproses' => $terproses,
                'ditutup' => $ditutup,
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];
                        
        }

        return BarangPenawaranPenjualan::insert($items);
    }

    public function formattingSingleData($data)
    {
        $data['ongkir']                 = floatval(preg_replace('/[^\d.]/', '', $data['ongkir']));
        $data['total_potongan_rupiah']  = floatval(preg_replace('/[^\d.]/', '', $data['total_potongan_rupiah']));

        return $data;
    }

    public function formattingArrayData($data, $i)
    {
       $data['unit_harga_produk'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['unit_harga_produk'][$i]));
       return $data;
    }
}