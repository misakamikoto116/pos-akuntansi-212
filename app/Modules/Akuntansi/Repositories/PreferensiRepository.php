<?php

namespace App\Modules\Akuntansi\Repositories;

use Generator\Interfaces\RepositoryInterface;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\PreferensiBarang;

class PreferensiRepository implements RepositoryInterface
{
    public function __construct(Produk $model, Akun $akun, PreferensiMataUang $mataUang, PreferensiBarang $prefBarang)
    {
        $this->akun = $akun;
        $this->model = $model;
        $this->mataUang = $mataUang;
        $this->prefBarang = $prefBarang;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->find($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        switch ($data['input_kategori']) {
            case 'akun default':
                $this->insertAkunDefault($data);                
            break;
            case 'akun barang':
                $this->insertAkunBarang($data);                
            break;
            case 'notifikasi':
                $this->notifikasiBarang($data);
            break;
        }

        return true;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $model = $this->model->fill($data);
        $model->save();

        return $model;
    }

    /**e
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }

    public function listAkun()
    {
        return $this->akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function dataMataUang()
    {
        return $this->mataUang->find(1);
    }

    public function dataBarang()
    {
        return $this->prefBarang->find(1);
    }

    public function dataAkunBank()
    {
        return $this->prefAkunBank->find(1);
    }

    public function insertAkunDefault($data)
    {
        $this->mataUang->updateOrCreate(
            [
                'id' => 1
            ],
            [
                'akun_hutang_id'              => $data['akun_hutang_id'],
                'akun_piutang_id'             => $data['akun_piutang_id'],
                'uang_muka_pembelian_id'      => $data['uang_muka_pembelian_id'],
                'uang_muka_penjualan_id'      => $data['uang_muka_penjualan_id'],
                'diskon_penjualan_id'         => $data['diskon_penjualan_id'],
                'laba_rugi_terealisir_id'     => $data['laba_rugi_terealisir_id'],
                'laba_rugi_tak_terealisir_id' => $data['laba_rugi_tak_terealisir_id'],
                'akun_penyesuaian_id'         => $data['akun_penyesuaian_id']
            ]
        );
        return true;
    }

    public function insertAkunBarang($data)
    {
        $this->prefBarang->updateOrCreate(
            [
                'id' => 1
            ],
            [
                'akun_persediaan_id'                => $data['akun_persediaan_id'],
                'akun_penjualan_id'                 => $data['akun_penjualan_id'],
                'akun_retur_penjualan_id'           => $data['akun_retur_penjualan_id'],
                'akun_diskon_barang_id'             => $data['akun_diskon_barang_id'],
                'akun_barang_terkirim_id'           => $data['akun_barang_terkirim_id'],
                'akun_hpp_id'                       => $data['akun_hpp_id'],
                'akun_retur_pembelian_id'           => $data['akun_retur_pembelian_id'],
                'akun_beban_id'                     => $data['akun_beban_id'],
                'akun_belum_tertagih_id'            => $data['akun_belum_tertagih_id'],
            ]
        );
        return true;
    }

    public function notifikasiBarang($data)
    {
        $this->prefBarang->updateOrCreate(
            [
                'id'    => 1
            ],
            [
               'minimal_stock'                     => $data['minimal_stock'],
                'estimasi_kada_luarsa'              => $data['estimasi_kada_luarsa']
            ]
        );
        return true;
    }
}
