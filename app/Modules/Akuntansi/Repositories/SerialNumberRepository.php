<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\SerialNumber;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\DetailSerialNumber;
use Generator\Interfaces\RepositoryInterface;
use Carbon\carbon;
use Helpers\IndonesiaDate as DateHelper;

class SerialNumberRepository implements RepositoryInterface
{
    public function __construct(SerialNumber $model, Produk $produk, Transaksi $transaksi, DetailSerialNumber $detailserial, DateHelper $date)
    {
        $this->model = $model;
        $this->produk = $produk;
        $this->transaksi = $transaksi;
        $this->detailserial = $detailserial;
        $this->date = $date;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $model = $this->model->findOrFail($id);
        $this->formattingField($model);

        return $model;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $check = $this->model->findOrFail($id);
        DetailSerialNumber::where('serial_number_id', $check->id)->delete();

        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        if (empty($data['tgl_pengisian'])) {
            $data['tgl_pengisian'] = date('Y-m-d');
        } else {
            $data['tgl_pengisian'] = $this->date->IndonesiaToSql($data['tgl_pengisian']);
        }
        $model = $this->findItem($id)->fill($data);
        if ($model->save()) {
            DetailSerialNumber::where('serial_number_id', $model->id)->delete();
            $this->insertDetailSerialNumber($data, $model);
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        if (empty($data['tgl_pengisian']) && empty($data['tgl_transaksi'])) {
            $data['tgl_pengisian'] = date('Y-m-d');
        } else {
            $data['tgl_pengisian'] = $this->date->IndonesiaToSql($data['tgl_pengisian']);
        }

        $model = $this->model->fill($data);
        if ($model->save()) {
            $this->insertDetailSerialNumber($data, $model);
        }

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }

    public function formattingField($model)
    {
        if (!empty($model->tgl_pengisian)) {
            $model->tgl_pengisian = Carbon::parse($model->tgl_pengisian)->format('d F Y');
        }

        return true;
    }

    public function insertDetailSerialNumber($data, $model)
    {
        $count = count($data['produk_id']);
        $items = [];
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');

        for ($i = 0; $i < $count; ++$i) {
            $items[] = [
                'serial_number_id' => $model->id,
                'produk_id' => $data['produk_id'][$i],
                'item_deskripsi' => $data['keterangan_produk'][$i],
                'jumlah' => $data['qty_produk'][$i],
                'satuan' => $data['satuan_produk'][$i],
                'serial_number' => $data['nomor_serial'][$i],
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];
        }
        DetailSerialNumber::insert($items);
    }

    public function listTransaksi()
    {
        return $this->transaksi->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->id;

            return $output;
        }, []);
    }

    public function listProduk()
    {
        return $this->produk->where('tipe_barang', '!==', 3)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->no_barang.' | '.$item->keterangan;

            return $output;
        }, []);
    }
}
