<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\PembayaranPembelian;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\InvoicePembayaranPembelian;
use Carbon\Carbon;
use Cart;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;
use Helpers\CodeHelper as Code;
use Helpers\IndonesiaDate as DateHelper;
use Helpers\TransaksiHelper;
use Session;

class PembayaranPembelianRepository implements RepositoryInterface
{
    public function __construct(PembayaranPembelian $model, 
                                Code $code, 
                                InformasiPemasok $pemasok,
                                Akun $akun,
                                DateHelper $date,
                                TransaksiHelper $transaksi
                                )
    {
        $this->model = $model;
        $this->code = $code;
        $this->date = $date;
        $this->akun = $akun;
        $this->pemasok = $pemasok;
        $this->transaksi = $transaksi;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {

        return $this->model->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
       $edit_pembayaran_pembelian = $this->model->findOrFail($id);
        $this->formattingField($edit_pembayaran_pembelian);
        return $edit_pembayaran_pembelian;        
        
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_pembayaran_pembelian')){
            return abort(403);
        }

        $model = $this->model->findOrFail($id);
        $this->transaksi->ExecuteTransactionSingle('delete', $model);            
        InvoicePembayaranPembelian::where('pembayaran_pembelian_id',$model->id)->delete();        
        return $model->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $checkData = $this->model->where('form_no', $data['form_no'])
                    ->where('id', '!=', $id)->first();
        
        if($checkData){
			throw new \Exception('Nomor Form terduplikat!');            
        }
        
        $data = $this->formattingAllData($data);
        $data['form_no'] = $this->code->autoGenerate($this->model, "form_no", $data['form_no']);
        $model = $this->findItem($id)->fill($this->dateChecker($data));
        InvoicePembayaranPembelian::where('pembayaran_pembelian_id',$model->id)->delete();        
        if($model->save()){
            $this->transaksi->ExecuteTransactionSingle('delete', $model);            
            $this->insertSubItem($data, $model);
        }    
        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
   //      $checkData = $this->model->where('form_no', $data['form_no'])
   //                  ->where('id', '!=', $id)->first();
        
   //      if($checkData){
			// throw new \Exception('Nomor Form terduplikat!');            
   //      }

        $data = $this->formattingAllData($data);
        $data['form_no'] = $this->code->autoGenerate($this->model, "form_no", $data['form_no']);
        $model = $this->model->fill($this->dateChecker($data));
        if($model->save()){
            $this->insertSubItem($data, $model);
        }

        if(!empty($data['lanjutkan'])){
            $model['continue_stat'] = 2;                
        }    
          
        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(10);
    }

    public function listPemasok()
    {
        return $this->pemasok->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }
   
    public function listAkun()
    {
        return $this->akun->whereHas('tipeAkun', function($item){
            $item->where('title','Kas/Bank');
        })->get()->reduce(function ($output, $item) {
                $output[$item->id] = $item->nama_akun;

                return $output;
            }, []);
    }

    public function dateChecker($data)
    {
        $data['cheque_date'] = (empty($data['cheque_date'])) ? $data['cheque_date'] = date('Y-m-d H:i:s') : $data['cheque_date'] = $this->date->IndonesiaToSql($data['cheque_date']); ;
        $data['payment_date'] = (empty($data['payment_date'])) ? $data['payment_date'] = date('Y-m-d H:i:s') : $data['payment_date'] = $this->date->IndonesiaToSql($data['payment_date']); ;
        
        return $data;
    }

    public function insertSubItem($data, $model)
    {
        $data['cheque_date'] = (empty($data['cheque_date'])) ? $data['cheque_date'] = date('Y-m-d H:i:s') : $data['cheque_date'] = $this->date->IndonesiaToSql($data['cheque_date']);
        $items = [];
        $arrayKredit = []; $arrayDebit = [];
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        $count = is_array($data['faktur_id']) ? count($data['faktur_id']) : 1;
        for ($i=0; $i < $count; $i++) {
            $akun_faktur = FakturPembelian::select('akun_hutang_id')->findOrFail($data['faktur_id'][$i]);
            if ($data['payment_amount'][$i] > 0 && $data['pay'][$i] == 1) {
                $cheque_date = Carbon::parse($data['cheque_date'])->format('Y-m-d H:i:s');
                $items[] = [
                    'faktur_id' => $data['faktur_id'][$i],
                    'pembayaran_pembelian_id' => $model->id,
                    'payment_amount' => $data['payment_amount'][$i],
                    'last_owing'     => $data['owing'][$i],
                    'tanggal'        => $cheque_date,
                    'created_at' => $jam_sekarang,
                    'updated_at' => $jam_sekarang,
                ];
            }
            $arrayKredit[] = [ 
                'nominal'   => $data['payment_amount'][$i],
                'tanggal'   => $data['cheque_date'],
                'dari'      => 'dp amount kredit',
                'akun'      => $data['akun_bank_id']
            ];

            $arrayDebit[] = [ 
                'nominal'   => $data['payment_amount'][$i],
                'tanggal'   => $data['cheque_date'],
                'dari'      => 'dp amount debit',
                'akun'      => $akun_faktur->akun_hutang_id
            ];
        }
        InvoicePembayaranPembelian::insert($items);
        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayKredit, 0, $data['form_no']);            
        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayDebit, 1, $data['form_no']);            
        
    }

    public function idPrediction()
    {
        $data = $this->model->orderBy('id','desc')->first();
        if($data){
            return $data->id + 1;
        }
        return 1;
    }

    public function formattingField($model)
    {
        if (!empty($model->cheque_date)) {
            $model->cheque_date = Carbon::parse($model->cheque_date)->format('d F Y');
        }
        if (!empty($model->payment_date)) {
            $model->payment_date = Carbon::parse($model->payment_date)->format('d F Y');
        }
    }

    public function formattingAllData($data)
    {
        // data faktur
        $data['cheque_amount']               = floatval(preg_replace('/[^\d.]/', '', $data['cheque_amount']));
        
        // data produk
        if (!empty($data['faktur_id'])) {
            for ($i=0; $i < count($data['faktur_id']); $i++) { 
                $data['amount'][$i]         = floatval(preg_replace('/[^\d.]/', '', $data['amount'][$i]));
                $data['owing'][$i]          = floatval(preg_replace('/[^\d.]/', '', $data['owing'][$i]));
                $data['payment_amount'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['payment_amount'][$i]));
            }
        }
        return $data;
    }
}