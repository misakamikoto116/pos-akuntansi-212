<?php

namespace App\Modules\Akuntansi\Repositories;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\TipeAkun;
use Generator\Interfaces\RepositoryInterface;
use Helpers\CodeHelper;
use Helpers\LayoutHelper;

class SaldoAkunRepository implements RepositoryInterface
{
    public function __construct(Transaksi $model, LayoutHelper $layout, Akun $akun, TipeAkun $tipeAkun)
    {
        $this->model = $model;
        $this->layout = $layout;
        $this->akun = $akun;
        $this->tipeAkun = $tipeAkun;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        if (!isset($_REQUEST)) {
            return $this->calculate();
        }else{
            return $this->filter($_REQUEST);
        }
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->find($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->findItem($id)->fill($data);
        $model->save();

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $model = $this->model->fill($data);
        $model->save();
            
        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->calculate($data);
    }

    public function calculate($request = null)
    {
        if(!empty($request['search'])){
            $data = $this->model->where('akun_id','!=','null')->whereYear('tanggal',$request['search'])->get();
        }else if(!empty($request['tipe'])){
            if(isset($request['search']) === false){
                $request['search'] = date('Y');
            }
            $data = $this->model->where('akun_id', $request['tipe'])->whereYear('tanggal',$request['search'])->get();
        }else{
            $data = $this->model->where('akun_id','!=','null')->get();
        }
        
        $data = $data->unique('akun_id')->transform(function($item, $key){
            $item['saldo_awal'] = $item->where('akun_id',$item->akun_id)->where('desc_dev','Saldo Awal')->sum('nominal');
            $item['periode_1'] = $item->where('akun_id',$item->akun_id)->whereMonth('tanggal','01')->sum('nominal');
            $item['periode_2'] = $item->where('akun_id',$item->akun_id)->whereMonth('tanggal','02')->sum('nominal');
            $item['periode_3'] = $item->where('akun_id',$item->akun_id)->whereMonth('tanggal','03')->sum('nominal');
            $item['periode_4'] = $item->where('akun_id',$item->akun_id)->whereMonth('tanggal','04')->sum('nominal');
            $item['periode_5'] = $item->where('akun_id',$item->akun_id)->whereMonth('tanggal','05')->sum('nominal');
            $item['periode_6'] = $item->where('akun_id',$item->akun_id)->whereMonth('tanggal','06')->sum('nominal');
            $item['periode_7'] = $item->where('akun_id',$item->akun_id)->whereMonth('tanggal','07')->sum('nominal');
            $item['periode_8'] = $item->where('akun_id',$item->akun_id)->whereMonth('tanggal','08')->sum('nominal');
            $item['periode_9'] = $item->where('akun_id',$item->akun_id)->whereMonth('tanggal','09')->sum('nominal');
            $item['periode_10'] = $item->where('akun_id',$item->akun_id)->whereMonth('tanggal','10')->sum('nominal');
            $item['periode_11'] = $item->where('akun_id',$item->akun_id)->whereMonth('tanggal','11')->sum('nominal');
            $item['periode_12'] = $item->where('akun_id',$item->akun_id)->whereMonth('tanggal','12')->sum('nominal');
            return $item;
        });

        if(isset($request['tipe'])){
            $tipeAkun = $request['tipe'];
            $akun = $this->akun->whereHas('tipeAkun', function ($query) use($tipeAkun) {
                $query->where('id', $tipeAkun);
            })->orderBy('id')->get();
        }else{
            $akun = $this->akun->orderBy('id')->get();
        }
        foreach($data as $transaksi){
            $x = 0;
            $i = 0;
            while($x < 1){
                if($data[$x]['akun_id'] === $akun[$i]->id){
                    $akun[$i]['saldo_awal'] = $transaksi->saldo_awal;
                    $akun[$i]['periode_1']  = $transaksi->periode_1;
                    $akun[$i]['periode_2']  = $transaksi->periode_2;
                    $akun[$i]['periode_3']  = $transaksi->periode_3;
                    $akun[$i]['periode_4']  = $transaksi->periode_4;
                    $akun[$i]['periode_5']  = $transaksi->periode_5;
                    $akun[$i]['periode_6']  = $transaksi->periode_6;
                    $akun[$i]['periode_7']  = $transaksi->periode_7;
                    $akun[$i]['periode_8']  = $transaksi->periode_8;
                    $akun[$i]['periode_9']  = $transaksi->periode_9;
                    $akun[$i]['periode_10'] = $transaksi->periode_10;
                    $akun[$i]['periode_11'] = $transaksi->periode_11;
                    $akun[$i]['periode_12'] = $transaksi->periode_12;
                    $akun[$i]['tahun']      = $transaksi->tahun;
                    $x = 1; // STAPH
                }
                $i++;
            }
        }
        
        return $this->layout->paginate($akun, 10)->withPath('saldo-akun');
    }

    public function listTahun()
    {
        return $this->layout->generateTahun();
    }

    public function tipeAkun()
    {
		return $this->tipeAkun->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->title;

			return $output;
		}, []);
    }
}