<?php
namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use App\Modules\Akuntansi\Models\PesananPenjualan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\KategoriProduk;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use App\Modules\Akuntansi\Models\JasaPengiriman;
use App\Modules\Akuntansi\Models\BarangPengirimanPenjualan;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\PenawaranPenjualan;
use App\Modules\Akuntansi\Models\BarangPesananPenjualan;
use App\Modules\Akuntansi\Models\BarangPenawaranPenjualan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\BantuanPengirimanPenjualan;
use App\Modules\Akuntansi\Models\BantuanPenerimaanPembelian;
use Carbon\carbon;
use Helpers\IndonesiaDate as DateHelper;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;
use Generator\Interfaces\RepositoryInterface;

class PengirimanPenjualanRepository implements RepositoryInterface
{
	public function __construct(PengirimanPenjualan $model,
				KategoriProduk $kategori,
				Gudang $gudang,
				Akun $akun,
				InformasiPelanggan $pelanggan,
				SyaratPembayaran $termin,
				Produk $produk,
				JasaPengiriman $pengiriman,
				DateHelper $date,
				CodeHelper $code,
				TransaksiHelper $transaksi,
				PesananPenjualan $pesanan,
				PenawaranPenjualan $penawaran)
	{
		$this->produk     = $produk;
		$this->model      = $model;
		$this->kategori   = $kategori;
		$this->gudang     = $gudang;
		$this->akun       = $akun;
		$this->pelanggan  = $pelanggan;
		$this->termin     = $termin;
		$this->pengiriman = $pengiriman;
		$this->penawaran  = $penawaran;
		$this->pesanan    = $pesanan;
		$this->date       = $date;
		$this->code       = $code;
		$this->transaksi  = $transaksi;
	}

	/**
	 * ini untuk mengambil data keseluruhan
	 * user di data repositori.
	 *
	 * @return Collection data list user
	 */
	public function getItems()
	{
		return $this->model->paginate(20);
	}

	/**
	 * ini untuk mencari user berdasarkan id yang dicari.
	 *
	 * @param int $id
	 *
	 * @return object
	 */
	public function findItem($id)
	{
		$pengiriman_penjualan                = $this->model->findOrFail($id);
		$pengiriman_penjualan->delivery_date = Carbon::parse($pengiriman_penjualan->delivery_date)->format('d F Y');

		return $pengiriman_penjualan;
	}

	/**
	 * ini untuk menghapus data berdasarkan id.
	 *
	 * @param [type] $id [description]
	 *
	 * @return [type] [description]
	 */
	public function delete($id)
	{
		if(!auth()->user()->hasPermissionTo('hapus_pengiriman_penjualan')){
			return abort(403);
		}
		$model   = $this->model->findOrFail($id);
		$subItem = BarangPengirimanPenjualan::where('pengiriman_penjualan_id', $model->id);
		$this->deleteSubItem($model);
		$this->transaksi->ExecuteTransactionSingle('delete', $model);
		$this->transaksi->UpdateParentStatus($this->pesanan, $subItem, 'available', 'pesanan');
		$subItem->delete();

		return $this->findItem($id)->delete();
	}

	/**
	 * update data berdasarkan id dan data
	 * didapat dari variable request.
	 *
	 * @param [type] $id   [description]
	 * @param [type] $data [description]
	 *
	 * @return [type] [description]
	 */
	public function update($id, $data)
	{
		if (empty($data['delivery_date'])) {
			$data['delivery_date'] = date('Y-m-d');
		} else {
			$data['delivery_date'] = $this->date->IndonesiaToSql($data['delivery_date']);
		}
		$model = $this->findItem($id)->fill($data);
		$this->transaksi->checkGudang($data);
		if ($model->save()) {
			$subItem = BarangPengirimanPenjualan::where('pengiriman_penjualan_id', $model->id);
			$this->deleteSubItem($model);
			$this->transaksi->UpdateParentStatus($this->pesanan, $subItem, 'delete', 'pesanan_penjualan_id');
			$subItem->delete();

			$this->transaksi->ExecuteTransactionMultiple('delete', $model); // delete all
			// update yang diupdate
			$this->transaksi->ExecuteTransactionSingle('delete', $model);
			$this->deleteSubItem($model);
			$this->InsertBarangPengirimanPenjualan($data, $model);
			$this->transaksi->UpdateParentStatus($this->pesanan, $model, 'update', 'pesanan', $data);
		}

		if (!empty($data['lanjutkan'])) {
			if ('Simpan & Baru' == $data['lanjutkan']) {
				$model['continue_stat'] = 2;
			} else {
				$model['continue_stat'] = 1;
			}
		}

		return $model;
	}

	/**
	 * menambahkan data berdasarkan request.
	 *
	 * @param [type] $request [description]
	 *
	 * @return [type] [description]
	 */
	public function insert($data)
	{
		$data['delivery_no'] = $this->code->autoGenerate($this->model, 'delivery_no', $data['delivery_no']);
		if (empty($data['delivery_date'])) {
			$data['delivery_date'] = date('Y-m-d');
		} else {
			$data['delivery_date'] = $this->date->IndonesiaToSql($data['delivery_date']);
		}
		$model = $this->model->fill($data);
		$this->transaksi->checkGudang($data);
		$akun = $this->produk->whereIn('id', $data['produk_id'])->pluck('akun_persedian_id');
		if ($model->save()) {
			$this->InsertBarangPengirimanPenjualan($data, $model);
		}

		if (!empty($data['lanjutkan'])) {
			if ('Simpan & Baru' == $data['lanjutkan']) {
				$model['continue_stat'] = 2;
			} else {
				$model['continue_stat'] = 1;
			}
		}

		return $model;
	}

	/**
	 * ini berfungisi untuk melakukan filter terhadap
	 * data yang akan diambil dan ditampilkan kepada
	 * user nantinya.
	 *
	 * @param array $data
	 */
	public function filter($data)
	{
		return $this->model->filter($data)->paginate(10);
	}

	public function listKategoriProduk()
	{
		return $this->kategori->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama;

			return $output;
		}, []);
	}

	public function listGudang()
	{
		return $this->gudang->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama;

			return $output;
		}, []);
	}

	public function listAkun()
	{
		return $this->akun->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama_akun;

			return $output;
		}, []);
	}

	public function listPelanggan()
	{
		return $this->pelanggan->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama;

			return $output;
		}, []);
	}

	public function listProduk()
	{
		return $this->produk->where('tipe_barang', '!==', 3)->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->no_barang . ' | ' . $item->keterangan;

			return $output;
		}, []);
	}

	public function listTermin()
	{
		return $this->termin->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->akan_dapat_diskon . '/' . $item->jika_membayar_antara . '/n' . $item->jatuh_tempo;

			return $output;
		}, []);
	}

	public function listJasaPengiriman()
	{
		return $this->pengiriman->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama;

			return $output;
		}, []);
	}

	public function idPrediction()
	{
		$data = $this->model->orderBy('id', 'desc')->first();
		if ($data) {
			return $data->id + 1;
		}

		return 1;
	}

	/**
	 * inserting sub item.
	 */
	public function InsertBarangPengirimanPenjualan($data, $model)
	{
		$items             = [];
		$produkDetail      = [];
		$jam_sekarang      = Carbon::now()->format('Y-m-d H:i:s');
		$count             = count($data['produk_id']);
		$getProduk         = $this->produk->whereIn('id', $data['produk_id']);
		$akunPersediaan    = $getProduk->pluck('akun_persedian_id', 'id')->toArray();
		$akunBarangTerkrim = $getProduk->pluck('akun_barang_terkirim_id', 'id')->toArray();

		foreach ($data['produk_id'] as $i => $produk_id) {
			// $getDetailProduk = Produk::select('akun_barang_terkirim_id')->find($data['produk_id'][$i]);
			$items = [
				'item_deskripsi'              => $data['keterangan_produk'][$i],
				'produk_id'                   => $data['produk_id'][$i],
				'pengiriman_penjualan_id'     => $model->id,
				'jumlah'                      => $data['qty_produk'][$i],
				'item_unit'                   => $data['satuan_produk'][$i],
				'barang_pesanan_penjualan_id' => !empty($data['barang_id']) ? $data['barang_id'][$i] : null,
				'gudang_id'                   => $data['gudang_id'][$i],
				'harga_modal'                 => $data['harga_modal'][$i],
				'no_so'                       => $data['no_so'][$i],
				'created_at'                  => $jam_sekarang,
				'updated_at'                  => $jam_sekarang,
			];

			$arrayBarangTerkirim = [
				'nominal' => $data['harga_modal'][$i] * $data['qty_produk'][$i],
				'tanggal' => $data['delivery_date'],
				'produk'  => $data['produk_id'][$i],
				'akun'    => $akunBarangTerkrim[$produk_id],
				'dari'    => 'Barang Terkirim',
			];

			$transaksi_barang_terkirim = $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrayBarangTerkirim, 1, $data['po_no']); // piutang usaha => debit
			
			$produkDetail[] = [
				'no_faktur'        => $data['po_no'],
				'tanggal'          => $data['delivery_date'],
				'kuantitas'        => -$data['qty_produk'][$i],
				'harga_modal'      => $data['harga_modal'][$i],
				'harga_terakhir'   => $data['harga_terakhir'][$i],
				'gudang_id'        => !empty($data['gudang_id']) ? $data['gudang_id'][$i] : null,
				'transaksi_id'     => $transaksi_barang_terkirim->id, //harusnya pake $model->id
				'sn'               => null,
				'status'           => 1,
				'item_type' 	   => get_class($this->model), // dari pengiriman
				'item_id' 		   => $model->id, // dari pengiriman
				'produk_id'        => $data['produk_id'][$i],
				'created_at'       => date('Y-m-d H:i:s'),
			];

			$arrayPersediaan[] = [
				'nominal' => $data['harga_modal'][$i] * $data['qty_produk'][$i],
				'tanggal' => $data['delivery_date'],
				'produk'  => $data['produk_id'][$i],
				'akun'    => $akunPersediaan[$produk_id],
				'dari'    => 'Persediaan',
			];

			$bantuan_pembelian = BantuanPenerimaanPembelian::where('produk_id', $data['produk_id'][$i])->orderBy('id')->first();
			$brg_pengiriman = BarangPengirimanPenjualan::create($items);
			if(!empty($bantuan_pembelian)){
				BantuanPengirimanPenjualan::insert([
					'brg_pengiriman_penjualan_id' => $brg_pengiriman->id,
					'bnt_penerimaan_pembelian_id' => $bantuan_pembelian->id
				]);	
			}
		}

		$this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayPersediaan, 0, $data['po_no']); // piutang usaha => debit
		SaldoAwalBarang::insert($produkDetail);

		$this->checkIfAllItemWasUsed($data, $model);

		return true;
	}


	public function deleteSubItem($model)
	{
			SaldoAwalBarang::where([
				'status' => 1,
				'item_type' => get_class($this->model),
				'item_id'     => $model->id, ])->delete();

		return true;
	}

	public function checkIfAllItemWasUsed($data, $model)
	{
		$valid_pesanan   = 1;
		$valid_penawaran = 1;

		$barang_pesanan   = [];
		$barang_penawaran = [];
		$pesanan_id       = [];
		$penawaran_id     = [];
		foreach ($data['produk_id'] as $key => $val) { // key means index
			if (!empty($data['barang_id'])) {
				array_push($barang_pesanan, $data['barang_id'][$key]);
			}
		}

		if (empty($barang_pesanan)) {
			return true; // abort mission
		}

		$getBarangPesanan = BarangPesananPenjualan::whereIn('id', $barang_pesanan);
		foreach ($getBarangPesanan->get() as $index => $gbp) {
			// cek apakah semua barang di pesanan sudah di pakai
			// first what index ?
			$i = array_search($gbp->produk_id, $data['produk_id']);
			// second, if we dont get the index, it's mean not all of the item are used
			if (false === $i) {
				// which is not valid
				$valid_pesanan = 0;
			} else {
				// third, does the value is same or more?
				if ($data['qty_produk'][$i] < $gbp->jumlah) {
					// if it's not equals/more than it's not valid
					$valid_pesanan = 0;
				}
				// fourth. well all is valid tho
				array_push($pesanan_id, $gbp->pesanan_penjualan_id);
				// insert penawaran id
				if (!empty($gbp->barang_penawaran_penjualan_id)) {
					array_push($barang_penawaran, $gbp->barang_penawaran_penjualan_id);
				}
			}
		}
		if (1 == $valid_pesanan && null != $pesanan_id) {
			$this->transaksi->UpdateParentStatusDua($this->pesanan, '2', $pesanan_id); //update status pesanan
		} else {
			return true; //abort mission
		}
		// ==================================================================

		$getBarangPenawaran = BarangPenawaranPenjualan::whereIn('id', $barang_penawaran);
		foreach ($getBarangPenawaran->get() as $index => $gbp) {
			// cek apakah semua barang di penawaran sudah di pakai
			// first what index ?
			$i = array_search($gbp->produk_id, $data['produk_id']);
			// second, if we dont get the index, it's mean not all of the item are used
			if (false === $i) {
				// which is not valid
				$valid_penawaran = 0;
			} else {
				// third, does the value is same or more?
				if ($data['qty_produk'][$i] < $gbp->jumlah) {
					// if it's not equals/more than it's not valid
					$valid_penawaran = 0;
				}
				// fourth. well all is valid tho
				array_push($penawaran_id, $gbp->penawaran_penjualan_id);
			}
		}
		if (1 == $valid_penawaran && null != $penawaran_id) {
			$this->transaksi->UpdateParentStatusDua($this->penawaran, '3', $penawaran_id); //tutup status pesanan
		} else {
			return true; //abort mission
		}
	}
}
