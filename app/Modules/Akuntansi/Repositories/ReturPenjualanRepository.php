<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\ReturPenjualan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\BarangReturPenjualan;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\ReturPembelian;
use App\Modules\Akuntansi\Models\FakturPembelian;
use Generator\Interfaces\RepositoryInterface;
use Helpers\IndonesiaDate as DateHelper;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;
use Carbon\Carbon;

class ReturPenjualanRepository implements RepositoryInterface
{
    public function __construct(ReturPenjualan $model, CodeHelper $code, TransaksiHelper $transaksi, InformasiPelanggan $pelanggan, DateHelper $date, KodePajak $kode_pajak, Akun $akun, Produk $produk, Gudang $gudang, PreferensiMataUang $preferensi)
    {
        $this->model = $model;
        $this->pelanggan = $pelanggan;
        $this->date = $date;
        $this->kode_pajak = $kode_pajak;
        $this->code = $code;
        $this->transaksi = $transaksi;
        $this->akun = $akun;
        $this->produk = $produk;
        $this->gudang = $gudang;
        $this->preferensi = ($preferensi->find(1)) ? $preferensi->find(1) : null;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $retur_penjualan = $this->model->findOrFail($id);
        if (!empty($retur_penjualan->tanggal)) {
            $retur_penjualan->tanggal = Carbon::parse($retur_penjualan->tanggal)->format('d F Y');
        }
        if (!empty($retur_penjualan->tgl_fiscal)) {
            $retur_penjualan->tgl_fiscal = Carbon::parse($retur_penjualan->tgl_fiscal)->format('d F Y');
        }

        return $retur_penjualan;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_retur_penjualan')){
            return abort(403);
        }
        $model = $this->findItem($id);
        $getTransaksi = Transaksi::where(['item_id' => $model->id, 'item_type' => get_class($model)])->pluck('id');
        foreach ($getTransaksi as $gt) {
            // mencari hasil similiar
            SaldoAwalBarang::where([
                    'status' => 1,
                    'sn' => null,
                    'item_type' => get_class($model),
                    'transaksi_id' => $gt, ])->delete();
        }
        $this->transaksi->ExecuteTransactionSingle('delete', $model);
        $this->deleteSubItem($model);

        return $model->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data = $this->formattingAllData($data);
        
        $this->checkNoSr($data);
        $data['faktur_penjualan_id'] = $data['list-sales-invoice'];
        $data['taxable'] = (empty($data['taxable'])) ? 0 : 1;
        $data['intax'] = (empty($data['intax'])) ? 0 : 1;

        $model = $this->findItem($id)->fill($this->dateChecker($data));
        $this->transaksi->checkGudang($data);

        if ($model->save()) {
            $this->deleteSubItem($model);
            $getTransaksi = Transaksi::where(['item_id' => $model->id, 'item_type' => get_class($model)])->pluck('id');
            foreach ($getTransaksi as $gt) {
                // mencari hasil similiar
                SaldoAwalBarang::where([
                    'status' => 1,
                    'sn' => null,
                    'harga_terakhir' => 0,
                    'item_type' => get_class($model),
                    'transaksi_id' => $gt, ])->delete();
            }
            $this->insertSubItem($data, $model);
            $this->transaksi->ExecuteTransactionSingle('delete', $model);

            $this->insertTransaction('insert', $data, $model);
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->formattingAllData($data);
        $this->checkNoSr($data);
        $data['faktur_penjualan_id'] = $data['list-sales-invoice'];
        $data['taxable'] = (empty($data['taxable'])) ? 0 : 1;
        $data['intax'] = (empty($data['intax'])) ? 0 : 1;

        $model = $this->model->fill($this->dateChecker($data));
        $this->transaksi->checkGudang($data);
        if ($model->save()) {
            $this->insertSubItem($data, $model);
            $this->insertTransaction('insert', $data, $model);
        }

        if (!empty($data['lanjutkan'])) {
            $model['continue_stat'] = 2;
        }

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(20);
    }

    public function listPelanggan()
    {
        return $this->pelanggan->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listGudang()
    {
        return $this->gudang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listProduk()
    {
        return $this->produk->where('tipe_barang', '!==', 3)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->no_barang.' | '.$item->keterangan;

            return $output;
        }, []);
    }

    public function idPrediction()
    {
        $data = $this->model->orderBy('id', 'desc')->first();
        if ($data) {
            return $data->id + 1;
        }

        return 1;
    }

    public function dateChecker($data)
    {
        $data['tanggal'] = (empty($data['tanggal'])) ? $data['tanggal'] = date('Y-m-d H:i:s') : $data['tanggal'] = $this->date->IndonesiaToSql($data['tanggal']);
        $data['tgl_fiscal'] = (empty($data['tgl_fiscal'])) ? $data['tgl_fiscal'] = date('Y-m-d H:i:s') : $data['tgl_fiscal'] = $this->date->IndonesiaToSql($data['tgl_fiscal']);

        return $data;
    }

    public function insertSubItem($data, $model)
    {
        $items = [];
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        $count = count($data['produk_id']);
        for ($i = 0; $i < $count; ++$i) {
            $pajak1 = null;
            $pajak2 = null;
            if (!empty($data['tax_produk'][$i])) {
                // data ada.. walau hanya satu
                $pajak1 = $data['tax_produk'][$i][0];
                if (count($data['tax_produk'][$i]) > 1) {
                    $pajak2 = $data['tax_produk'][$i][1];
                }
            }

            $items[] = [
                'produk_id' => $data['produk_id'][$i],
                'barang_faktur_penjualan_id' => $data['barang_faktur_penjualan_id'][$i],
                'retur_penjualan_id' => $model->id,
                'item_deskripsi' => $data['keterangan_produk'][$i],
                'jumlah' => $data['qty_produk'][$i],
                'harga' => $data['unit_harga_produk'][$i],
                'satuan' => $data['satuan_produk'][$i],
                'diskon' => $data['diskon_produk'][$i],
                'gudang_id' => $data['gudang_id'][$i],
                'harga_modal' => $data['harga_modal'][$i],
                'kode_pajak_id' => $pajak1,
                'kode_pajak_2_id' => $pajak2,
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];
        }

        return BarangReturPenjualan::insert($items);
    }

    public function deleteSubItem($model)
    {
        return BarangReturPenjualan::where('retur_penjualan_id', $model->id)->delete();
    }

    public function checkNoSr($data)
    {
        $data['sr_no'] = $this->code->autoGenerate($this->model, 'sr_no', $data['sr_no']);
    }

    public function listKodePajak()
    {
        return $this->kode_pajak->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->kode_pajak_formatted;

            return $output;
        }, []);
    }

    public function insertTransaction($param, $data, $model)
    {
        // dd($data);
        $data['tanggal'] = (empty($data['tanggal'])) ? $data['tanggal'] = date('Y-m-d H:i:s') : $data['tanggal'] = $this->date->IndonesiaToSql($data['tanggal']);
        $akun_produk = $this->produk->whereIn('id', $data['produk_id'])->pluck('akun_ret_penjualan_id');
        $data_faktur = FakturPenjualan::find($data['faktur_penjualan_id']);

        if (!empty($data['tax_produk'])) {
            $arrayPajak = $this->hitungPajak($data);
            $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayPajak, 1); //insert pajak per item (debit)
            $arrayPajakDanBarang = $this->hitungPajakDanBarang($data, $data_faktur);
            $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayPajakDanBarang); //insert pajak dan barang (kredit)
        } else {
            $arrayPajakDanBarang = $this->hitungPajakDanBarang($data, $data_faktur);
            $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayPajakDanBarang); //insert pajak dan barang (kredit)
        }
        // dd($data_faktur);
        $count = count($data['produk_id']);

        $arrayReturPenjualan = [];
        $arrayHpp = [];
        $arrayDiskon = [];
        $arrayPersediaan = [];
        $arrayDebit = [];
        $arrayKredit = [];
        $array_total_barang = [];

        $detailProduk = Produk::whereIn('id', $data['produk_id'])->get()->toArray();
        $detailProdukArr = []; // y I do dis.. kalau 10 barang brti 10x query.. itu berat mz

        foreach ($data['produk_id'] as $produk) { //cari itemnya.. samakan indexnya..
            $i = 0;
            $x = 0;
            while ($x < 1) {
                if ($produk == $detailProduk[$i]['id']) {
                    $detailProdukArr[] = [
                        // 'akun_penjualan_id' => $detailProduk[$i]['akun_penjualan_id'],
                        'akun_hpp_id' => $detailProduk[$i]['akun_hpp_id'],
                        'akun_disk_penjualan_id' => $detailProduk[$i]['akun_disk_penjualan_id'],
                        'akun_persedian_id' => $detailProduk[$i]['akun_persedian_id'],
                    ];
                    $x = 1;
                }
                ++$i;
            }
        }

        // for ($i = 0; $i < $count; ++$i) {
        // $total_barang = $data['unit_harga_produk'][$i] * $data['qty_produk'][$i];

        // $arrayReturPenjualan[] = [
        //     'nominal' => $total_barang,
        //     'produk' => $data['produk_id'][$i],
        //     'akun'  => $detailProdukArr[$i]['akun_penjualan_id'],
        //     'id'    => $data['sr_no']
        // ];

        // }

        foreach ($data['produk_id'] as $i => $produk_id) {
            $total_barang = $data['unit_harga_produk'][$i] * $data['qty_produk'][$i];
            $total_diskon = $total_barang * $data['diskon_produk'][$i] / 100; // res : float

            $persen_barang = $data['amount_produk'][$i] / array_sum($data['amount_produk']);
            $total_diskon_faktur = (empty($data['jumlah_diskon_faktur'])) ? 0 : $data['jumlah_diskon_faktur'];
            $total_diskon_master = ($total_diskon_faktur * $persen_barang);

            $produk_detail = SaldoAwalBarang::where(['produk_id' => $produk_id, 'item_type' => get_class(new FakturPembelian()), 'item_type' => get_class(new ReturPembelian())])->orderBy('id', 'desc')->first();
            $produk_detail_kuantitas = SaldoAwalBarang::where(['produk_id' => $produk_id])->orderBy('id', 'desc')->get()->sum('kuantitas');
            $produk_detail_modal = SaldoAwalBarang::where(['produk_id' => $produk_id])->orderBy('id', 'desc')->get();

            $modal = $produk_detail_modal->first()->harga_modal;
            $sumQTy = $produk_detail_kuantitas;
            $harga_barang = $modal * $data['qty_produk'][$i];
            // $harga_modal_1  = ($total_barang * $data['qty_produk'][$i]);
            $harga_terakhir = $harga_barang / $data['qty_produk'][$i];
            $harga_modal_1 = $harga_barang + ($modal * $sumQTy);
            $harga_modal = ($harga_modal_1 / ($data['qty_produk'][$i] + $sumQTy));
            // dd($harga_terakhir);

            $array_total_barang = [ // akun retur penjualan => debit
                'nominal' => $total_barang,
                'tanggal' => $data['tanggal'],
                'dari' => 'Total Retur Penjualan',
                'produk' => $produk_id,
                'akun' => $akun_produk[$i] ?? 0,
                'id' => $data['sr_no'],
            ];

            $model_total = $this->transaksi->ExecuteTransactionSingle($param, $model, $array_total_barang, 1); //penjualan diskon => kredit

            $arrayDiskon[] = [
                'nominal' => $total_diskon,
                'tanggal' => $data['tanggal'],
                'produk' => $produk_id,
                'dari' => 'Diskon Retur Penjualan',
                'akun' => $detailProdukArr[$i]['akun_disk_penjualan_id'],
                'id' => $data['sr_no'],
            ];
            $arrayKredit[] = [ //penjualan hpp => kredit
                'nominal' => $harga_modal * $data['qty_produk'][$i],
                'tanggal' => $data['tanggal'],
                'dari' => 'HPP Penjualan',
                'produk' => $produk_id,
                'akun' => $detailProdukArr[$i]['akun_hpp_id'],
                'id' => $data['sr_no'],
            ];

            $arrayPersediaan[] = [
                'nominal' => $harga_modal * $data['qty_produk'][$i],
                'tanggal' => $data['tanggal'],
                'dari' => 'Persediaan Retur Penjualan',
                'produk' => $produk_id,
                'akun' => $detailProdukArr[$i]['akun_persedian_id'],
                'id' => $data['sr_no'],
            ];

            $produkDetail = [
                'no_faktur' => $data['sr_no'],
                'tanggal' => date('Y-m-d H:i:s'),
                'kuantitas' => $data['qty_produk'][$i],
                'harga_modal' => $harga_modal,
                'harga_terakhir' => $harga_terakhir,
                'gudang_id' => !empty($data['gudang_id']) ? $data['gudang_id'][$i] : null,
                'transaksi_id' => $model_total->id, //harusnya pake $model->id
                'sn' => null,
                'status' => 1,
                'item_type' => get_class($model),
                'produk_id' => $produk_id,
                'item_id' => $model->id,
                'created_at' => date('Y-m-d H:i:s'),
            ];

            SaldoAwalBarang::insert($produkDetail);
        }

        $arrayKredit[] = [ //penjualan diskon => kredit | $akumulasi_diskon_item
            'nominal' => $data['jumlah_diskon_retur'],
            'tanggal' => $data['tanggal'],
            'dari' => 'Diskon Retur Penjualan',
            'akun' => ($this->preferensi == null) ? null : $this->preferensi->diskon_penjualan_id,
            'id' => $data['sr_no'],
        ];

        // $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayReturPenjualan, 1); #return penjualan  => debit
        $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayDiskon); //penjualan diskon => kredit
        $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayPersediaan, 1); //persediaan => debit

        $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayKredit);
        $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayDebit, 1);

        return true;
    }

    public function hitungPajak($data)
    {
        $count = count($data['produk_id']);
        $arrayPajak = [];
        for ($i = 0; $i < $count; ++$i) {
            $pajak1 = null;
            $pajak2 = null;
            $barang = $data['unit_harga_produk'][$i] * $data['qty_produk'][$i];
            $total_diskon = (($barang * $data['diskon_produk'][$i]) / 100);
            $persen_barang = $data['amount_produk'][$i] / array_sum($data['amount_produk']);
            $total_diskon_retur = (empty($data['jumlah_diskon_retur'])) ? 0 : $data['jumlah_diskon_retur'];
            $total_diskon_master = ($total_diskon_retur * $persen_barang);
            $total_barang = $barang - $total_diskon - $total_diskon_master;

            if (!empty($data['tax_produk'][$i])) {
                // data ada.. walau hanya satu
                $pajak1 = $data['tax_produk'][$i][0];
                $data_pajak1 = KodePajak::find($pajak1); // res : collection
                if (!empty($data['in_tax'])) {
                    $total_pajak1 = ($total_barang * $data_pajak1->nilai) / 100 + $data_pajak1->nilai; // res : float
                } else {
                    $total_pajak1 = ($total_barang * $data_pajak1->nilai) / 100; // res : float
                }
                $arrayPajak[] = [
                        'nominal' => $total_pajak1,
                        'tanggal' => $data['tanggal'],
                        'produk' => $data['produk_id'][$i],
                        'akun' => $data_pajak1->akun_pajak_penjualan_id,
                        'dari' => 'Pajak '.$data_pajak1->akun_pajak_penjualan_id.' dan barang',
                        'id' => $data['sr_no'],
                    ];

                if (count($data['tax_produk'][$i]) > 1) {
                    $pajak2 = $data['tax_produk'][$i][1];
                    $data_pajak2 = KodePajak::find($pajak2); // res : collection
                    if (!empty($data['in_tax'])) {
                        $total_pajak2 = ($total_barang * $data_pajak2->nilai) / 100 + $data_pajak2->nilai; // res : float
                    } else {
                        $total_pajak2 = ($total_barang * $data_pajak2->nilai) / 100; // res : float
                    }
                    $arrayPajak[] = [ // pajak pure
                            'nominal' => $total_pajak2,
                            'tanggal' => $data['tanggal'],
                            'produk' => $data['produk_id'][$i],
                            'akun' => $data_pajak2->akun_pajak_penjualan_id,
                            'dari' => 'Pajak '.$data_pajak2->akun_pajak_penjualan_id.' dan barang',
                            'id' => $data['sr_no'],
                        ];
                }
            }
        }

        return $arrayPajak;
    }

    public function hitungPajakDanBarang($data, $data_faktur)
    {
        $count = count($data['produk_id']);
        $arrayPajak = [];
        for ($i = 0; $i < $count; ++$i) {
            $pajak1 = null;
            $pajak2 = null;
            $barang = $data['unit_harga_produk'][$i] * $data['qty_produk'][$i];
            $total_diskon = (($barang * $data['diskon_produk'][$i]) / 100);
            $persen_barang = $data['amount_produk'][$i] / array_sum($data['amount_produk']);
            $total_diskon_retur = (empty($data['jumlah_diskon_retur'])) ? 0 : $data['jumlah_diskon_retur'];
            $total_diskon_master = ($total_diskon_retur * $persen_barang);
            $total_barang = $barang - $total_diskon - $total_diskon_master;

            $arrayPajak[] = [ // total barang (per row) + pajak
                'nominal' => $total_barang,
                'tanggal' => $data['tanggal'],
                'produk' => null,
                'akun' => $data_faktur->akun_piutang_id,
                'id' => $data['sr_no'],
            ];

            if (!empty($data['tax_produk'][$i])) {
                // data ada.. walau hanya satu
                $pajak1 = $data['tax_produk'][$i][0];
                $data_pajak1 = KodePajak::find($pajak1); // res : collection
                if (!empty($data['in_tax'])) {
                    $total_pajak1 = ($total_barang * $data_pajak1->nilai) / 100 + $data_pajak1->nilai; // res : float
                } else {
                    $total_pajak1 = ($total_barang * $data_pajak1->nilai) / 100; // res : float
                }
                $arrayPajak[] = [ // total barang (per row) + pajak
                        'nominal' => $total_pajak1,
                        'tanggal' => $data['tanggal'],
                        'produk' => $data['produk_id'][$i],
                        'akun' => $data_faktur->akun_piutang_id,
                        'id' => $data['sr_no'],
                    ];

                if (count($data['tax_produk'][$i]) > 1) {
                    $pajak2 = $data['tax_produk'][$i][1];
                    $data_pajak2 = KodePajak::find($pajak2); // res : collection
                    if (!empty($data['in_tax'])) {
                        $total_pajak2 = ($total_barang * $data_pajak2->nilai) / 100 + $data_pajak2->nilai; // res : float
                    } else {
                        $total_pajak2 = ($total_barang * $data_pajak2->nilai) / 100; // res : float
                    }
                    $arrayPajak[] = [ // total barang (per row) + pajak
                            'nominal' => $total_pajak2,
                            'tanggal' => $data['tanggal'],
                            'produk' => $data['produk_id'][$i],
                            'akun' => $data_faktur->akun_piutang_id,
                            'id' => $data['sr_no'],
                        ];
                }
            }
        }
        return $arrayPajak;
    }

    public function formattingAllData($data)
    {
        // data produk
        if (!empty($data['produk_id'])) {
            for ($i = 0; $i < count($data['produk_id']); ++$i) {
                $data['unit_harga_produk'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['unit_harga_produk'][$i]));
                $data['amount_produk'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['amount_produk'][$i]));
            }
        }

        if (!empty($data['jumlah_diskon_retur'])) {
            $data['jumlah_diskon_retur'] = floatval(preg_replace('/[^\d.]/', '', $data['jumlah_diskon_retur'])); 
        }

        return $data;
    }
}
