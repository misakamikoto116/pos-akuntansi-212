<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\User;
use PDF;
use Carbon\carbon;
use Generator\Interfaces\RepositoryInterface;

class RekapTransaksiRepository implements RepositoryInterface
{
    public function __construct(FakturPenjualan $model, BarangFakturPenjualan $barang, Identitas $identitas, User $user)
    {
        $this->model        = $model;
        $this->barang       = $barang;
        $this->identitas    = $identitas;
        $this->user         = $user;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $model = $this->model->findOrFail($id);

        return $model;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->findItem($id)->fill($data);

        return $model->save();
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $model = $this->model->fill($data);

        return $model->save();
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }

     // Rekap Transaksi
    public function getRekapTransaksi()
    {
        $items = [];
        $temp  = $this->getTransaksiModal()->orderBy('invoice_date','asc')->get()->map(function($item) use(&$items){
            if(!isset($items[Carbon::parse($item->invoice_date)->format('Y_m_d')])){
                $items[Carbon::parse($item->invoice_date)->format('Y_m_d')] = [
                    'tanggal'           => Carbon::parse($item->invoice_date)->format('d F Y'),
                    'sum_transaksi'     => 0,
                    'harga_modal'       => 0,
                    'laba'              => 0,
                    'tanggal_formatted' => Carbon::parse($item->invoice_date)->format('Y-m-d')
                ];
            }

            $items[Carbon::parse($item->invoice_date)->format('Y_m_d')]['sum_transaksi'] += $item->transaksi->where('dari', 'Penjualan dengan Tax & Disc')->sum('nominal');
            $items[Carbon::parse($item->invoice_date)->format('Y_m_d')]['harga_modal'] += $item->transaksi->where('dari', 'Penjualan HPP')->sum('nominal');
            $items[Carbon::parse($item->invoice_date)->format('Y_m_d')]['laba'] += $items[Carbon::parse($item->invoice_date)->format('Y_m_d')]['sum_transaksi'] - $items[Carbon::parse($item->invoice_date)->format('Y_m_d')]['harga_modal'];

        });
        $view = [
            'items'     => $items,
            'filter'    => [
                'tanggal_dari_sampai_rekap',
            ],
            'module_url' => (Object)[
                'index'  => 'akuntansi.penjualan.rekap.transaksi', 
            ],
        ];

        return $view;
    }

    public function getDetailTransaksi($tanggal)
    {
        $transaksi = $this->getTransaksiModal()->whereDate('invoice_date', $tanggal)->get();

        $data      = $this->dataCekOut($transaksi);

        $view      = [
            'title'     => 'Detail Transaksi',
            'items'     => $data,
        ];

        return view('akuntansi::rekap_transaksi.detail_transaksi')->with($view);
    }

    public function getDetailBarang($id)
    {
        $barang = $this->barang->with('produk.saldoAwalBarang')->where('faktur_penjualan_id', $id)->get();

        $view = [
            'title'     => 'Detail Barang',
            'items'     => $barang
        ];

        return view('akuntansi::rekap_transaksi.detail_barang')->with($view);
    }

    public function getRekapKasir($request)
    {
        $data = $request->all();

        $nama_perusahaan    = $this->identitas->first()->nama_perusahaan ?? null;

        $nomor_rekap_kasir  = sprintf('RekapKasir-%s-%s',Carbon::parse($data['tanggal_operasional'])->format('d-m-Y'),Carbon::parse($data['tanggal_setoran'])->format('d-m-Y'));

        $sum_per_kasir      = $this->user->with(['fakturPenjualan' => function ($query) use($data) {
            $query->whereDate('invoice_date', Carbon::parse($data['tanggal_operasional'])->format('Y-m-d'));
        }])->whereHas('fakturPenjualan')->get();

        return PDF::setOptions(['dpi' => 203, 'defaultFont' => 'sans-serif'])->loadView('akuntansi::rekap_transaksi.rekap_kasir', [
                'items'                 => $this->detailRekapTransaksiKasir($sum_per_kasir),
                'sum_setoran'           => $this->detailRekapTransaksiKasir($sum_per_kasir)->sum('sum_transaksi'),
                'nama_perusahaan'       => $nama_perusahaan,
                'tanggal_operasional'   => Carbon::parse($data['tanggal_operasional'])->format('d F Y'),
                'tanggal_setoran'       => Carbon::parse($data['tanggal_setoran'])->format('d F Y'),
            ])
            ->setPaper('a4','landscape')
            ->save(
                    storage_path('app/rekap_kasir/' . $nomor_rekap_kasir . '.pdf')
                )
            ->stream($nomor_rekap_kasir.'.pdf');
    }

    public function getTransaksi()
    {
        return  $this->model->with(['transaksi' => function ($query) {
            $query->where('dari', 'Penjualan dengan Tax & Disc');
        }, 'kasirMesin', 'barang.produk'])->has('transaksi')->where('status_modul', 1)->whereNotNull('kasir_mesin_id');
    }

    public function getTransaksiModal()
    {
        $request = request();

        return $this->model->filter($request)->with(['transaksi', 'kasirMesin', 'barang.produk'])->has('transaksi')->where('status_modul', 1)->whereNotNull('kasir_mesin_id');   
    }

    public function dataCekOut($data)
    {
        return $data->map(function ($item_data)
        {
            $total_transaksi = $item_data->transaksi->where('dari', 'Penjualan dengan Tax & Disc')->sum('nominal');
            $harga_modal     = $item_data->transaksi->where('dari','Penjualan HPP')->sum('nominal');
            $laba            = $total_transaksi - $harga_modal;
            return [
                'id'                => $item_data->id,
                'no_transaksi'      => $item_data->no_faktur,
                'tanggal'           => Carbon::parse($item_data->invoice_date)->format('d F Y'),
                'jumlah_item'       => $item_data->barang->count(),
                'kasir'             => $item_data->user->name,
                'total_transaksi'   => $total_transaksi,
                'harga_modal'       => $harga_modal,
                'laba'              => $laba,
            ];
        });
    }

    public function detailRekapTransaksiKasir($kasir)
    {
        return $kasir->map(function ($item_kasir)
        {
            return [
                'nama_kasir'    => $item_kasir->name,
                'sum_transaksi' => $item_kasir->fakturPenjualan->sum('total'),
            ];
        });
    }
}
