<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\DetailInformasiPemasok;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\BarangFakturPembelian;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use Helpers\IndonesiaDate as DateHelper;
use Carbon\Carbon;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;
use Generator\Interfaces\RepositoryInterface;


class UangMukaPemasokRepository implements RepositoryInterface
{
    public function __construct(FakturPembelian $model, InformasiPemasok $pemasok, DetailInformasiPemasok $detail_pemasok, SyaratPembayaran $termin, DateHelper $date, BarangFakturPembelian $barang_faktur_pembelian, CodeHelper $code, Produk $produk, KodePajak $kode_pajak, Akun $akun, TransaksiHelper $transaksi, PreferensiMataUang $preferensi)
    {
        $this->model = $model;
        $this->barang_faktur_pembelian = $barang_faktur_pembelian;
        $this->pemasok = $pemasok;
        $this->detail_pemasok = $detail_pemasok;
        $this->termin = $termin;
        $this->date = $date;
        $this->code = $code;
        $this->produk = $produk;
        $this->kode_pajak = $kode_pajak;
        $this->akun = $akun;        
        $this->transaksi = $transaksi;        
        $this->preferensi = ($preferensi->find(1)) ? $preferensi->find(1) : null ;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->where('uang_muka', 1)->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $uang_muka_pemasok = $this->model->findOrFail($id);
        $uang_muka_pemasok->invoice_date = Carbon::parse($uang_muka_pemasok->invoice_date)->format('d F Y');
        $uang_muka_pemasok->barang->first()->unit_price    = number_format($uang_muka_pemasok->barang->first()->unit_price);
        return $uang_muka_pemasok;
    }

    /**-
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $check = $this->model->findOrFail($id);
        $this->transaksi->ExecuteTransactionSingle('delete', $check); # T E R H A P U S

        BarangFakturPembelian::where('faktur_pembelian_id', $check->id)->delete();  
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $checkData = $this->model->where('no_faktur', $data['no_faktur'])
                    ->where('id', '!=', $id)->first();
        
        if($checkData){
			throw new \Exception('Nomor Faktur terduplikat!');            
        }

        $data = $this->formattingAllData($data);
        $data['no_faktur'] = $this->code->autoGenerate($this->model, "no_faktur", $data['no_faktur']);
        
        $data['taxable'] = (empty($data['taxable'])) ? 0 : $data['taxable'];
        $data['in_tax'] = (empty($data['in_tax'])) ? 0 : $data['in_tax'];
        $uang_muka_pemasok = $this->findItem($id);
        $detail_uang_muka = $this->barang_faktur_pembelian->where('faktur_pembelian_id',$uang_muka_pemasok->id)->first();
        $model = $uang_muka_pemasok->fill($this->dateChecker($data));
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        if($model->save()){
            // $this->transaksi->ExecuteTransaction('update', $this->model, array('nominal' => $data['total']), $data['akun_uang_muka_id']);             
            $this->transaksi->ExecuteTransactionSingle('delete', $model); # T E R H A P U S
            $this->insertTransaction($data, $model);

            $pajak1     = null;
            $pajak2     = null;
            if(!empty($data['kode_pajak_id'])){
                $pajak1 = $data['kode_pajak_id'][0];
                if(count($data['kode_pajak_id']) > 1){
                    $pajak2 = $data['kode_pajak_id'][1];
                }
            }

            $detail_uang_muka->produk_id = $data['produk_id'];
            $detail_uang_muka->faktur_pembelian_id = $uang_muka_pemasok->id;
            $detail_uang_muka->item_deskripsi = $data['item_deskripsi'];
            $detail_uang_muka->jumlah = $data['jumlah'];
            $detail_uang_muka->unit_price = $data['unit_price'];
            $detail_uang_muka->diskon = $data['diskon_produk'];
            $detail_uang_muka->kode_pajak_id = $pajak1;
            $detail_uang_muka->kode_pajak_2_id = $pajak2;
            $detail_uang_muka->update();
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->formattingAllData($data);
        $data['no_faktur'] = $this->code->autoGenerate($this->model, "no_faktur", $data['no_faktur']);
        $model = $this->model->fill($this->dateChecker($data));
        if($model->save()){
            $this->insertSubItem($data, $model);
            $this->insertTransaction($data, $model);
            // $this->transaksi->ExecuteTransaction('insert', $this->model, array('nominal' => $data['total']), $data['akun_uang_muka_id']); 
        }
        if(!empty($data['lanjutkan'])){
            $model['continue_stat'] = 2;                
        }    
        return $model;
    }

    public function insertSubItem($data, $model)
    {
        $pajak1     = null;
        $pajak2     = null;
        if(!empty($data['kode_pajak_id'])){
            $pajak1 = $data['kode_pajak_id'][0];
            if(count($data['kode_pajak_id']) > 1){
                $pajak2 = $data['kode_pajak_id'][1];
            }
        }
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
            $items = [
                'produk_id' => $data['produk_id'],
                'faktur_pembelian_id' => $model->id,
                'item_deskripsi' => $data['item_deskripsi'],
                'jumlah' => $data['jumlah'],
                'unit_price' => $data['unit_price'],
                'diskon' => $data['diskon_produk'],
                'kode_pajak_id' => $pajak1,
                'kode_pajak_2_id' => $pajak2,
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];
        BarangFakturPembelian::insert($items);
    }
    
    /** 
     * 
     */

    public function insertTransaction($data, $model)
    {
        $data['invoice_date'] = (empty($data['invoice_date'])) ? $data['invoice_date'] = date('Y-m-d H:i:s') : $data['invoice_date'] = $this->date->IndonesiaToSql($data['invoice_date']);
        $arrayDebit = [];
        $arrayKredit = [];
       
        $arrayKredit[] = [
            'produk'    => $data['produk_id'],
            'tanggal'   => $data['invoice_date'],
            'nominal'   => $data['unit_price'],
            'dari'      => 'Total DP Kredit',
            'akun'      => ($data['akun_hutang_id'] == null) ? $this->preferensi->akun_hutang_id : $data['akun_hutang_id']
        ];   

        $arrayDebit[] = [
            'produk'    => $data['produk_id'],
            'tanggal'   => $data['invoice_date'],
            'nominal'   => $data['unit_price'],
            'dari'      => 'Total DP Debit',
            'akun'      => ($data['akun_uang_muka_id'] == null) ? $this->preferensi->uang_muka_pembelian_id : $data['akun_uang_muka_id']
        ];   

        if(!empty($data['kode_pajak_id'])){
            $arrayPajak = $this->hitungPajak($data, $model);
        }

        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayDebit, 1, $data['no_faktur']);
        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayKredit, 0, $data['no_faktur']);
       
        return true;         
    }

    /** 
     * 
     * 
     */

    public function hitungPajak($data, $model)
    {
        $arrayPajak = [];    
        $arrayHutang = [];    

        $pajak1     = null;
        $pajak2     = null;
        $total_barang = $data['unit_price'];

        if(!empty($data['kode_pajak_id'])){
            // data ada.. walau hanya satu
            $pajak1 = $data['kode_pajak_id'][0];
            $data_pajak1  = KodePajak::find($pajak1); // res : collection
            $total_pajak1 = ($total_barang  * $data_pajak1->nilai) / 100; // res : float
            
            $arrayPajak[] = [
                'nominal'   => $total_pajak1,
                'tanggal'   => $data['invoice_date'],
                'produk'    => $data['produk_id'],
                'dari'      => 'Pajak DP Pembelian',
                'akun'      => $data_pajak1->akun_pajak_pembelian_id,
                'id'        => $data['no_faktur']
            ]; 
            
            $arrayHutang[] = [
                'nominal'   => $total_pajak1,
                'tanggal'   => $data['invoice_date'],
                'produk'    => $data['produk_id'],
                'dari'      => 'Pajak DP',
                'akun'      => $data['akun_hutang_id'],
                'id'        => $data['no_faktur']
            ];

            if(count($data['kode_pajak_id']) > 1){
                $pajak2 = $data['kode_pajak_id'][1];
                $data_pajak2  = KodePajak::find($pajak2); // res : collection
                $total_pajak2 = ($total_barang * $data_pajak2->nilai) / 100; // res : float

                $arrayPajak[] = [
                    'nominal'   => $total_pajak2,
                    'tanggal'   => $data['invoice_date'],
                    'produk'    => $data['produk_id'],
                    'dari'      => 'Pajak DP Pembelian',
                    'akun'      => $data_pajak2->akun_pajak_pembelian_id,
                    'id'        => $data['no_faktur']
                ];

                $arrayHutang[] = [
                    'nominal'   => $total_pajak2,
                    'tanggal'   => $data['invoice_date'],
                    'produk'    => $data['produk_id'],
                    'dari'      => 'Pajak DP',
                    'akun'      => $data['akun_hutang_id'],
                    'id'        => $data['no_faktur']
                ];
            }
        
        }
        
        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayPajak, 1);             
        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayHutang);            

        return true;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->where('uang_muka', 1)->filter($data)->paginate(20);
    }

    public function dateChecker($data)
    {
        $data['invoice_date'] = (empty($data['invoice_date'])) ? $data['invoice_date'] = date('Y-m-d H:i:s') : $data['invoice_date'] = $this->date->IndonesiaToSql($data['invoice_date']); ;
        return $data;
    }

    public function listPemasok()
    {
        return $this->pemasok->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listPajak()
    {
        return $this->kode_pajak->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->kode_pajak_formatted;

            return $output;
        }, []);
    }

    public function listAkun()
    {
        return $this->akun->whereDoesntHave('childAkun')->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function getAkunDp()
    {
        return $this->preferensi->pluck('uang_muka_pembelian_id')->first();
    }

    public function getAkunHutang()
    {
        return $this->preferensi->pluck('akun_hutang_id')->first();
    }

    public function getDPFromProduk()
    {
        return $this->produk->where('tipe_barang', 3)->first();
    }

    public function idPrediction()
    {
        $data = $this->model->orderBy('id','desc')->first();
        if($data){
            return $this->code->autoGenerate($this->model, "no_faktur");
        }
        return 1;
    }

    public function ExecuteTransactionMultiple($type, $model, $class, $data = null, $dk = 0)
    {
        $new_data = [];
        $nama_kelas = (new \ReflectionClass(get_class($class)))->getShortName();   

        foreach($data as $item){
            if($item['nominal'] != 0){
                $new_data[] = [
                    'nominal'             => $item['nominal'],
                    'tanggal'             => $item['tanggal'] !== null ? $item['tanggal'] : date('Y-m-d H:i:s'),
                    'akun_id'             => $item['akun'],
                    'produk_id'           => (!empty($item['produk'])) ? $item['produk'] : null ,
                    'item_id'             => $model->id,
                    'item_type'           => get_class($class),
                    'status'              => $dk,
                    'no_transaksi'        => $item['id'],                
                    'sumber'              => $this->transaksi->fromCamelCase($nama_kelas,' '),
                    'keterangan'          => $this->transaksi->fromCamelCase($nama_kelas,' ')." ".$item['id'],
                    'dari'                => (!empty($item['dari'])) ? $item['dari'] : null,
                    'status_rekonsiliasi' => null,
                    'created_at'          => date('Y-m-d H:i:s')  
                ];
            }
        }
        if($type == 'insert'){
            $transaksi = Transaksi::insert($new_data);   
        }else{
            $transaksi = Transaksi::where(['item_id' => $model->id, 'item_type' => get_class($model)])->delete();
        }
        return $transaksi;
    }

    public function formattingAllData($data)
    {        
        // data produk
        if (!empty($data['produk_id'])) {
            $data['unit_price'] = floatval(preg_replace('/[^\d.]/', '', $data['unit_price']));
        }
        return $data;
    }
}