<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\AkunPenyelesaianPesanan;
use App\Modules\Akuntansi\Models\DetailPenyelesaianPesanan;
use App\Modules\Akuntansi\Models\DetailPenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\PembiayaanPesanan;
use App\Modules\Akuntansi\Models\PenyelesaianPesanan;
use App\Modules\Akuntansi\Models\PenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\Transaksi;
use Carbon\Carbon;
use Generator\Interfaces\RepositoryInterface;
use Helpers\TransaksiHelper;

class PenyelesaianPesananRepository implements RepositoryInterface
{
    public function __construct(
        PenyelesaianPesanan $model,
        Gudang $gudang,
        Akun $akun,
        PembiayaanPesanan $pembiayaan,
        DetailPenyelesaianPesanan $pembiayaanItem,
        AkunPenyelesaianPesanan $pembiayaanAkun,
        SaldoAwalBarang $produk_detail,
        Produk $produk,
        PenyesuaianPersediaan $penyesuaian,
        DetailPenyesuaianPersediaan $detailPenyesuaian,
        TransaksiHelper $transaksi
    ) {
        $this->model = $model;
        $this->akun = $akun;
        $this->gudang = $gudang;
        $this->pembiayaan = $pembiayaan;
        $this->pembiayaanItem = $pembiayaanItem;
        $this->pembiayaanAkun = $pembiayaanAkun;
        $this->produk = $produk;
        $this->produk_detail = $produk_detail;
        $this->penyesuaian = $penyesuaian;
        $this->detailPenyesuaian = $detailPenyesuaian;
        $this->transaksi = $transaksi;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->with(['pembiayaanPesanan'])->orderBy('id','DESC')->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $model = $this->model->find($id);
        $model->tgl_selesai = Carbon::parse($model->tgl_selesai)->format('d F Y');
        return $model;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);
        if ($model->delete()) {
            $this->deleteSubItem($model, $id);
        }

        return $model;
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data = $this->formattingArrayData($data);
        $model = $this->findItem($id)->fill($data);
        if ($model->save()) {
            $this->deleteSubItem($model, $id);
            $this->insertSubItem($model, $data);
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->formattingArrayData($data);
        $model = $this->model->fill($data);
        if ($model->save()) {
            $this->insertSubItem($model, $data);
        }

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }

    public function listGudang()
    {
        return $this->gudang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listAkun()
    {
        return $this->akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function listPembiayaan()
    {
        return $this->pembiayaan->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->batch_no;

            return $output;
        }, []);
    }

    public function formattingArrayData($data): array
    {
        $data['tgl_selesai'] = date('Y-m-d H:i:s', strtotime($data['tgl_selesai']));
        if (array_key_exists('produk_id', $data)) {
            foreach ($data['produk_id'] as $idx => $produk_id) {
                $data['amount_produk'][$idx] = floatval(preg_replace('/[^\d.]/', '', $data['amount_produk'][$idx]));
                $data['alokasi_nilai'][$idx] = floatval(preg_replace('/[^\d.]/', '', $data['alokasi_nilai'][$idx]));
            }
        }

        return $data;
    }

    public function insertSubItem(PenyelesaianPesanan $model, array $data)
    {
        $pembiayaan = $this->pembiayaan->find($data['pembiayaan_pesanan_id']);
        $arrKredit = [];
        $arrDebit = [];
        $masterPenyesuaian = [];
        $detailPenyesuaian = [];

        try {
            $item = [];
            if ($data['tipe_penyelesaian'] == 1) {
                $akun = $this->produk->whereIn('id', $data['produk_id'])->get();

                $masterPenyesuaian = [
                    'no_penyesuaian' => $pembiayaan->batch_no,
                    'tgl_penyesuaian' => $pembiayaan->date,
                    'akun_penyesuaian' => $pembiayaan->akun_pembiayaan_pesanan_id,
                    'kode_akun_id' => $pembiayaan->akun_pembiayaan_pesanan_id,
                    'keterangan' => $pembiayaan->deskripsi,
                ];

                $dataPenyesuaian = $this->penyesuaian->create($masterPenyesuaian);

                foreach ($data['produk_id'] as $idx => $produk_id) {
                    $itemRecord = $akun->where('id', $produk_id)->first();
                    $produk_detail_sum_qty = $this->produk_detail->where('produk_id', $produk_id)->sum('kuantitas');
                    $latest_harga_modal = $this->produk_detail->where('produk_id', $produk_id)->orderBy('id','DESC')->first();
                    $latest_harga_modal = $latest_harga_modal !== null ? $latest_harga_modal->harga_modal : 0;
                    $harga_modal = (($produk_detail_sum_qty * $latest_harga_modal) + $data['alokasi_nilai'][$idx]) / ($produk_detail_sum_qty + $data['qty_produk'][$idx]);
                    
                    $item[] = [
                        'produk_id' => $produk_id,
                        'penyelesaian_pesanan_id' => $model->id,
                        'kuantitas' => $data['qty_produk'][$idx],
                        'biaya' => $data['amount_produk'][$idx],
                        'harga_modal' => $harga_modal,
                        'alokasi_nilai' => $data['alokasi_nilai'][$idx],
                        'presentase' => $data['presentase_produk'][$idx],
                        'gudang_id' => $data['gudang_id'][$idx],
                        'sn' => $data['sn'][$idx],
                    ];

                    $arrDebit = [
                        'nominal' => $data['alokasi_nilai'][$idx],
                        'dari' => 'Barang Persediaan',
                        'produk' => $produk_id,
                        'akun' => $itemRecord->akun_persedian_id,
                        'tanggal' => $data['tgl_selesai'],
                    ];

                    $transaksiItem = $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrDebit, 1, $pembiayaan->batch_no);

                    $produkDetail[] = [
                        'no_faktur' => $pembiayaan->batch_no,
                        'tanggal' => $data['tgl_selesai'],
                        'kuantitas' => $data['qty_produk'][$idx],
                        'harga_modal' => $harga_modal,
                        'harga_terakhir' => $data['amount_produk'][$idx],
                        'gudang_id' => $data['gudang_id'][$idx],
                        'transaksi_id' => $transaksiItem->id,
                        'sn' => $data['sn'][$idx],
                        'status' => 1,
                        'item_type' => get_class($model),
                        'item_id' => $model->id,
                        'produk_id' => $produk_id,
                        'created_at' => $data['tgl_selesai'],
                    ];

                    $detailPenyesuaian[] = [
                        'penyesuaian_persediaan_id' => $dataPenyesuaian->id,
                        'produk_id' => $produk_id,
                        'keterangan_produk' => $data['keterangan_produk'][$idx],
                        'satuan_produk' => $itemRecord->unit,
                        'qty_produk' => $data['qty_produk'][$idx],
                        'qty_baru' => $data['qty_produk'][$idx],
                        'nilai_skrg' => $latest_harga_modal * $produk_detail_sum_qty,
                        'nilai_baru' => $harga_modal * $produk_detail_sum_qty,
                        'gudang_id' => $data['gudang_id'][$idx],
                        'serial_number' => $data['sn'][$idx],
                    ];
                }

                $this->detailPenyesuaian->insert($detailPenyesuaian);
                $this->produk_detail->insert($produkDetail);
                $this->pembiayaanItem->insert($item);

                $arrKredit = [
                    'nominal' => $pembiayaan->total_pembiayaan,
                    'dari' => 'Total Pembiayaan',
                    'akun' => $pembiayaan->akun_pembiayaan_pesanan_id,
                    'tanggal' => $data['tgl_selesai'],
                ];

                $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrKredit, 0, $pembiayaan->batch_no);
            } else {
                $item = [
                    'penyelesaian_pesanan_id'   => $model->id,
                    'detail_akun_id'            => $data['kode_akun_item'],
                    'created_at'                => Carbon::now(),
                    'updated_at'                => Carbon::now(),
                ];
                $this->pembiayaanAkun->insert($item);
            }
        } catch (Exception $e) {
            return $e;
        }
    }

    public function deleteSubItem($model, $id)
    {
        try {
            $this->produk_detail->where(['item_id' => $model->id, 'item_type' => get_class($model)])->delete();
            $this->pembiayaanItem->where('penyelesaian_pesanan_id', $id)->delete();
            $this->pembiayaanAkun->where('penyelesaian_pesanan_id', $id)->delete();
            Transaksi::where(['item_id' => $model->id, 'item_type' => get_class($model)])->delete();

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }
}
