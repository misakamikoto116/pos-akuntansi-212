<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\IdentitasPajak;
use App\Modules\Akuntansi\Models\IdentitasCabang;
use App\Modules\Akuntansi\Models\MataUang;
use Generator\Interfaces\RepositoryInterface;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use File;


class IdentitasRepository implements RepositoryInterface
{
    public function __construct(Identitas $model, MataUang $mata_uang, IdentitasPajak $identitasPajak, IdentitasCabang $identitasCabang)
    {
        $this->model = $model;
        $this->mata_uang = $mata_uang;
        $this->identitasPajak = $identitasPajak;
        $this->identitasCabang = $identitasCabang;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $model = $this->model->findOrFail($id);
        
        $this->formattingField($model);
        
        $model = $this->formattinToArray($model);

        return $model;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $json_footer = $this->formattingToJson($data);

        $file_foto = $this->saveLogoPerusahaan($data, $id);
        $identitas = [
            'nama_perusahaan' => $data['nama_perusahaan'],
            'tanggal_mulai' => Carbon::parse($data['tanggal_mulai'])->format('Y-m-d H:i:s'),
            'alamat' => $data['alamat'],
            'kepala_toko' => $data['kepala_toko'],
            'mata_uang_id' => $data['mata_uang_id'],
            'kode_pos' => $data['kode_pos'],
            'no_telepon' => $data['no_telepon'],
            'negara' => $data['negara'],
            'footer_struk' => $json_footer,
            'logo_perusahaan'   => $file_foto,
        ];
        $model = $this->model->findOrFail($id)->fill($identitas);

        if($model->save()){
            $this->identitasPajak($data, $model);
            $this->identitasCabang($data, $model);
        }
        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }

    public function listMataUang()
    {
        return $this->mata_uang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->kode;

            return $output;
        }, []);
    }

    public function formattingField($model)
    {
        if (!empty($model->tanggal_mulai)) {
            $model->tanggal_mulai = Carbon::parse($model->tanggal_mulai)->format('d F Y');
        }
    }

    public function identitasPajak($data, $identitas)
    {
        $dataPajak = $this->identitasPajak->where('identitas_id', $identitas->id);

        if(!$dataPajak->get()->isEmpty()){
            $dataPajak->delete();
        }

        return $this->identitasPajak->create([
            'identitas_id' => $identitas['id'],
            'no_seri_faktur_pajak' => $data['no_seri_faktur_pajak'],
            'npwp' => $data['npwp'],
            'no_pengukuhan_pkp' => $data['no_pengukuhan_pkp'],
            'tanggal_pengukuhan_pkp' => $data['tanggal_pengukuhan_pkp'],
            'kode_cabang_pajak' => $data['kode_cabang_pajak'],
            'jenis_usaha' => $data['jenis_usaha'],
            'klu_spt' => $data['klu_spt'],
        ]);
    }

    public function identitasCabang($data, $identitas)
    {
        $dataCabang = $this->identitasCabang->where('identitas_id', $identitas->id);

        if(!$dataCabang->get()->isEmpty()){
            $dataCabang->delete();
        }
        if(isset($data['kode_cabang'])){
            for ($i=0; $i < count($data['kode_cabang']); $i++) { 
                $this->identitasCabang->create([
                    'identitas_id' => $identitas['id'],
                    'kode_cabang' => $data['kode_cabang'][$i],
                    'nama_cabang' => $data['nama_cabang'][$i],
                ]);
            }
        }
        return true;
    }

    public function formattingToJson($data)
    {
        $json = null;
        if (!empty($data['footer_1']) || !empty($data['footer_2'])) {
            $json = json_encode(['pesan' => $data['footer_1'], 'slogan' => $data['footer_2']]);
        }

        return $json;
    }

    public function formattinToArray($model)
    {
        if (!empty($model->footer_struk)) {
            $json           = json_decode($model->footer_struk);
            $model->pesan   = $json->pesan;
            $model->slogan  = $json->slogan;
        }

        return $model;
    }

    public function saveLogoPerusahaan($data, $id)
    {
        $filename = null;
        if(request()->has('logo_perusahaan')){
            if($data['logo_perusahaan'] != null){
                // path foto
                $location = storage_path().'/app/public/produk/';

                // delete foto produk yg lama
                $model = $this->model->where('id', $id)->first();
                File::delete($location.$model['logo_perusahaan']);

                // request foto produk baru
                $file = request()->file('logo_perusahaan');
                $extension = $file->getClientOriginalExtension();
                $filename = md5($file->getFilename()).'.'.$extension;
                $locationImage  =   $location.$filename;
                    
                // simpan foto produk yg baru
                Image::make($file)->resize(200, 150)->save($locationImage);
            }
        }

        return $filename;
    }
}