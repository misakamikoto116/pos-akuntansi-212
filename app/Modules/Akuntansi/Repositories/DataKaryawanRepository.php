<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\DataKaryawan;
use Generator\Interfaces\RepositoryInterface;
use Helpers\IndonesiaDate as DateHelper;
use Carbon\carbon;

class DataKaryawanRepository implements RepositoryInterface
{
    public function __construct(DataKaryawan $model, DateHelper $date)
    {
        $this->model = $model;
        $this->date = $date;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $model = $this->model->findOrFail($id);
        $this->formattingField($model);

        return $model;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        if (empty($data['tgl_lahir'])) {
            $data['tgl_lahir'] = date('Y-m-d');
        } else {
            $data['tgl_lahir'] = $this->date->IndonesiaToSql($data['tgl_lahir']);
        }
        $model = $this->findItem($id)->fill($data);

        return $model->save();
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        if (empty($data['tgl_lahir'])) {
            $data['tgl_lahir'] = date('Y-m-d');
        } else {
            $data['tgl_lahir'] = $this->date->IndonesiaToSql($data['tgl_lahir']);
        }
        $model = $this->model->fill($data);

        return $model->save();
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }

    public function formattingField($model)
    {
        if (!empty($model->tgl_lahir)) {
            $model->tgl_lahir = Carbon::parse($model->tgl_lahir)->format('d F Y');
        }

        return true;
    }
}
