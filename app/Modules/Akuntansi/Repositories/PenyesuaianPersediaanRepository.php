<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\TipeAkun;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\PenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\DetailPenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\Transaksi;
use Generator\Interfaces\RepositoryInterface;
use Helpers\IndonesiaDate as DateHelper;
use Helpers\TransaksiHelper;
use Carbon\carbon;

class PenyesuaianPersediaanRepository implements RepositoryInterface
{
    public function __construct(
        PenyesuaianPersediaan $model,
        Produk $produk,
        Gudang $gudang,
        Akun $akun,
        TipeAkun $tipeakun,
        DateHelper $date,
        SaldoAwalBarang $produk_detail,
        TransaksiHelper $transaksi,
        Transaksi $transaksi_model,
        DetailPenyesuaianPersediaan $detailPenyesuaianPersediaan
    ) {
        $this->model = $model;
        $this->produk = $produk;
        $this->akun = $akun;
        $this->tipe_akun = $tipeakun;
        $this->date = $date;
        $this->produk_detail = $produk_detail;
        $this->transaksi = $transaksi;
        $this->transaksi_model = $transaksi_model;
        $this->gudang = $gudang;
        $this->detailPenyesuaianPersediaan = $detailPenyesuaianPersediaan;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->with('detailPenyesuaian')->orderBy('id','DESC')->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->with('akun')->find($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if( !auth()->user()->hasPermissionTo('hapus_penyesuaian_persediaan')){
            return abort(403);
        }

        $check = $this->model->findOrFail($id);
        DetailPenyesuaianPersediaan::where('penyesuaian_persediaan_id', $check->id)->delete();

        Transaksi::where([
            'item_id' => $check->id,
            'item_type' => get_class($check),
        ])->delete();

        $sab = SaldoAwalBarang::where([
            'item_id' => $check->id,
            'item_type' => get_class($check),
        ])->delete();

        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data['value_penyesuaian'] = (empty($data['value_penyesuaian'])) ? 0 : 1;

        if (empty($data['tgl_penyesuaian'])) {
            $data['tgl_penyesuaian'] = date('Y-m-d');
        } else {
            $data['tgl_penyesuaian'] = $this->date->IndonesiaToSql($data['tgl_penyesuaian']);
        }
        $model = $this->findItem($id)->fill($data);
        if ($model->save()) {
            $this->detailPenyesuaianPersediaan->where('penyesuaian_persediaan_id', $model->id)->delete();
            $this->transaksi_model->where('item_id', $model->id)->where('item_type', get_class($model))->delete();
            $this->insertDetailPenyesuaianPersediaan($data, $model);
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data['value_penyesuaian'] = (empty($data['value_penyesuaian'])) ? 0 : 1;

        if (empty($data['tgl_penyesuaian'])) {
            $data['tgl_penyesuaian'] = date('Y-m-d');
        } else {
            $data['tgl_penyesuaian'] = $this->date->IndonesiaToSql($data['tgl_penyesuaian']);
        }
        $model = $this->model->fill($data);
        if ($model->save()) {
            $this->insertDetailPenyesuaianPersediaan($data, $model);
        }

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->with('detailPenyesuaian')->orderBy('id','DESC')->filter($data)->paginate(10);
    }

    public function insertDetailPenyesuaianPersediaan($data, $model)
    {
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        $arrDebit = [];
        $arrKredit = [];
        $produkDetail = [];
        $temp_array = [];
        $total_temp_array = [];
        $totalPenyesuaian = 0;
        $getProdukDetail = $this->produk_detail->whereIn('id', $data['produk_detail_id'])->get();
        $detailProdukArr = $this->sortAkunProduk($data);

        // $summed = $getProdukDetail->groupBy('id')
        //     ->transform(function ($item, $key) {
        //         $item['summed_qty'] = $item->sum('kuantitas');

        //         return $item['summed_qty'];
        //     })->sortBy('id')->toArray();
        // dd($data);

        $getProdukDetail = $getProdukDetail->sortBy('id')->toArray();
        foreach ($data['produk_id'] as $i => $val) {
            $data = $this->formatNominal($data, $i);
            if ($data['value_penyesuaian'] == 1) {
                $harga_modal = $data['nilai_baru'][$i] / array_key_exists('qty_baru', $data) ? $data['qty_baru'][$i] : $data['qty_diff'][$i];
                $counted_result = -$data['nilai_baru'][$i];
                $counted_result_penyesuaian = $data['nilai_baru'][$i];
                $harga_terakhir = $getProdukDetail[$i]['harga_terakhir'];
            } else {
                $harga_modal = $getProdukDetail[$i]['harga_modal'];
                $harga_terakhir = $getProdukDetail[$i]['harga_terakhir'];

                $counted_result = ($harga_modal) * (array_key_exists('qty_baru', $data) || array_key_exists('qty_produk', $data) ? $data['qty_baru'][$i] - $data['qty_produk'][$i] : $data['qty_diff'][$i]);
                $counted_result_penyesuaian = (-$harga_modal) * (array_key_exists('qty_baru', $data) || array_key_exists('qty_produk', $data) ? $data['qty_baru'][$i] - $data['qty_produk'][$i] : $data['qty_diff'][$i]);
            }

            if (array_key_exists('qty_baru', $data) ? $data['qty_baru'][$i] : $data['qty_diff'][$i]) {
                $temp_array = [
                    'nominal' => abs($counted_result),
                    'tanggal' => $data['tgl_penyesuaian'],
                    'dari' => 'Akun Persediaan',
                    'akun' => $detailProdukArr[$i]['akun_persedian_id'],
                ];

                if ($counted_result < 0) {
                    $insertedTransaction = $this->transaksi->ExecuteTransactionSingle('insert', $model, $temp_array, 0, $data['no_penyesuaian']);
                } else {
                    $insertedTransaction = $this->transaksi->ExecuteTransactionSingle('insert', $model, $temp_array, 1, $data['no_penyesuaian']);
                }

                $totalPenyesuaian += $counted_result_penyesuaian;

                $items[] = [
                    'penyesuaian_persediaan_id' => $model->id,
                    'produk_id' => $data['produk_id'][$i],
                    'keterangan_produk' => $data['keterangan_produk'][$i],
                    'satuan_produk' => $data['satuan_produk'][$i],
                    'qty_produk' => array_key_exists('qty_produk', $data) ? $data['qty_produk'][$i] : 0,
                    'qty_baru' => array_key_exists('qty_baru', $data) ? $data['qty_baru'][$i] : $data['qty_diff'][$i],
                    'qty_diff'  => (array_key_exists('qty_baru', $data) || array_key_exists('qty_produk', $data) ? $data['qty_baru'][$i] - $data['qty_produk'][$i] : $data['qty_diff'][$i]),
                    'nilai_skrg' => ($data['value_penyesuaian'] == 1 ? $data['nilai_skrg'][$i] : null),
                    'nilai_baru' => ($data['value_penyesuaian'] == 1 ? $data['nilai_baru'][$i] : null),
                    'departemen' => $data['departemen'][$i],
                    'gudang_id' => $data['gudang_id'][$i],
                    'proyek' => $data['proyek'][$i],
                    'serial_number' => $data['serial_number'][$i],
                    'created_at' => $jam_sekarang,
                    'updated_at' => $jam_sekarang,
                ];
            }

            $produkDetail[] = [
                'no_faktur' => $data['no_penyesuaian'],
                'tanggal' => $data['tgl_penyesuaian'],
                'kuantitas' => (array_key_exists('qty_baru', $data) || array_key_exists('qty_produk', $data) ? $data['qty_baru'][$i] - $data['qty_produk'][$i] : $data['qty_diff'][$i]),
                'harga_modal' => $harga_modal,
                'harga_terakhir' => $harga_terakhir,
                'gudang_id' => $data['gudang_id'][$i],
                'transaksi_id' => $insertedTransaction->id ?? null,
                'produk_id' => $data['produk_id'][$i],
                'sn' => $data['serial_number'][$i],
                'status' => 0,
                'item_type' => get_class($model),
                'item_id' => $model->id,
                'created_at' => $jam_sekarang,
            ];
        }

        $total_temp_array = [
            'nominal' => abs($totalPenyesuaian),
            'tanggal' => $data['tgl_penyesuaian'],
            'dari' => 'Akun Penyesuaian',
            'akun' => $data['akun_penyesuaian'],
        ];

        if ($totalPenyesuaian < 0) {
            $arrKredit[] = $total_temp_array;
        } else {
            $arrDebit[] = $total_temp_array;
        }

        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrKredit, 0, $data['no_penyesuaian']);
        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrDebit, 1, $data['no_penyesuaian']);
        DetailPenyesuaianPersediaan::insert($items);

        $this->produk_detail->where('item_id', $model->id)->where('item_type', get_class($model))->delete();

        SaldoAwalBarang::insert($produkDetail);
    }

    public function listProduk()
    {
        return $this->produk->where('tipe_barang', '!==', 3)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->no_barang.' | '.$item->keterangan;

            return $output;
        }, []);
    }

    public function listAkun()
    {
        return $this->akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function listTipeAkun()
    {
        return $this->tipe_akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->title;

            return $output;
        }, []);
    }

    public function listGudang()
    {
        return $this->gudang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function sortAkunProduk(array $data): array
    {
        $detailProduk = Produk::whereIn('id', $data['produk_id'])->orderBy('id', 'asc')->get()->toArray();
        $detailProdukArr = [];
        foreach ($data['produk_id'] as $i => $produk) {
            $detailProdukArr[] = [
                'akun_persedian_id' => $detailProduk[$i]['akun_persedian_id'],
            ];
        }

        return $detailProdukArr;
    }

    public function formatNominal($data, $i)
    {
        if (!empty($data['nilai_skrg'][$i])) {
            $data['nilai_skrg'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['nilai_skrg'][$i]));
        }

        if (!empty($data['nilai_baru'][$i])) {
            $data['nilai_baru'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['nilai_baru'][$i]));
        }

        return $data;
    }
}
