<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\BarangFakturPembelian;
use App\Modules\Akuntansi\Models\DetailInformasiPemasok;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\KontakPelanggan;
use App\Modules\Akuntansi\Models\MataUang;
use App\Modules\Akuntansi\Models\PembayaranPembelian;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\ReturPembelian;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use App\Modules\Akuntansi\Models\TipePajak;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Repositories\HistoryBukuBesarRepository;
use Carbon\carbon;
use File;
use Generator\Interfaces\RepositoryInterface;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;
use Intervention\Image\Facades\Image;

class InformasiPemasokRepository implements RepositoryInterface
{
    public function __construct(
            InformasiPemasok $model,
            SyaratPembayaran $termin,
            MataUang $mata_uang,
            KodePajak $kode_pajak,
            TipePajak $tipe_pajak,
            CodeHelper $code,
            TransaksiHelper $transaksi,
            FakturPembelian $fakturPembelian,
            ReturPembelian $returPembelian,
            PembayaranPembelian $pembayaranPembelian,
            Transaksi $isTransaksi,
            HistoryBukuBesarRepository $historyBukuBesarRepository
    ) {
        $this->model = $model;
        $this->termin = $termin;
        $this->mata_uang = $mata_uang;
        $this->kode_pajak = $kode_pajak;
        $this->tipe_pajak = $tipe_pajak;
        $this->code = $code;
        $this->transaksi = $transaksi;
        $this->isTransaksi = $isTransaksi;
        $this->fakturPembelian = $fakturPembelian;
        $this->returPembelian = $returPembelian;
        $this->pembayaranPembelian = $pembayaranPembelian;
        $this->historyBukuBesarRepository = $historyBukuBesarRepository; 
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->with(['kontakPemasok','mataUang'])->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if( !auth()->user()->hasPermissionTo('hapus_data_pemasok') ){
            return abort(403);
        }
        $model = $this->findItem($id);
        if ($model->delete()) {
            $this->deleteSubItem($model);
        }

        return $model;
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $location = storage_path().'/app/public/pemasok/';
        $model = $this->model->where('id', $id)->first();

        $filenamePemasok = null;
        $filenameNIKPemasok = null;
        if(empty($data['foto_pemasok'])){
            unset($data['foto_pemasok']);
        }else{
            File::delete($location.$model['foto_pemasok']);
            $fileNIKPemasok = request()->file('foto_pemasok');
            $extensionNIKPemasok = $fileNIKPemasok->getClientOriginalExtension();
            $filenameNIKPemasok = md5($fileNIKPemasok->getFilename()).'.'.$extensionNIKPemasok;
            $locationImageNIKPemasok  =   $location.$filenameNIKPemasok;
            Image::make($fileNIKPemasok)->resize(200, 150)->save($locationImageNIKPemasok);
            $data['foto_pemasok'] = $filenameNIKPemasok;
        }

        if(empty($data['foto_nik_pemasok'])){
            unset($data['foto_nik_pemasok']);
        }else{
            File::delete($location.$model['foto_nik_pemasok']);
            $fileNIKPemasok = request()->file('foto_nik_pemasok');
            $extensionNIKPemasok = $fileNIKPemasok->getClientOriginalExtension();
            $filenameNIKPemasok = md5($fileNIKPemasok->getFilename()).'.'.$extensionNIKPemasok;
            $locationImageNIKPemasok  =   $location.$filenameNIKPemasok;
            Image::make($fileNIKPemasok)->resize(200, 150)->save($locationImageNIKPemasok);
            $data['foto_nik_pemasok'] = $filenameNIKPemasok;
        }
        
        $model = $model->fill($data);
        if ($model->save()) {
            $this->deleteSubItem($model);
            $this->insertSubItem($data, $model);
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $location = storage_path().'/app/public/pemasok/';
        $filenamePemasok = null;
        $filenameNIKPemasok = null;
        if(request()->has('foto_pemasok')){
            $filePemasok = request()->file('foto_pemasok');
            $extensionPemasok = $filePemasok->getClientOriginalExtension();
            $filenamePemasok = md5($filePemasok->getFilename()).'.'.$extensionPemasok;
            $locationImagePemasok  =   $location.$filenamePemasok;
            Image::make($filePemasok)->resize(200, 150)->save($locationImagePemasok);
            $data['foto_pemasok'] = $filenamePemasok;
        }
        if(request()->has('foto_nik_pemasok')){
            $fileNIKPemasok = request()->file('foto_nik_pemasok');
            $extensionNIKPemasok = $fileNIKPemasok->getClientOriginalExtension();
            $filenameNIKPemasok = md5($fileNIKPemasok->getFilename()).'.'.$extensionNIKPemasok;
            $locationImageNIKPemasok  =   $location.$filenameNIKPemasok;
            Image::make($fileNIKPemasok)->resize(200, 150)->save($locationImageNIKPemasok);
            $data['foto_nik_pemasok'] = $filenameNIKPemasok;
        }

        $model = $this->model->fill($data);
        if ($model->save()) {
            $this->insertSubItem($data, $model);
        }

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->with(['kontakPemasok','mataUang'])->filter($data)->paginate(10);
    }

    public function listMataUang()
    {
        return $this->mata_uang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->kode;

            return $output;
        }, []);
    }

    public function listTermin()
    {
        return $this->termin->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->akan_dapat_diskon.'/'.$item->jika_membayar_antara.'/n'.$item->jatuh_tempo;

            return $output;
        }, []);
    }

    public function listKodePajak()
    {
        return $this->kode_pajak->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->keterangan;

            return $output;
        }, []);
    }

    public function listTipePajak()
    {
        return $this->tipe_pajak->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->kode.'  '.$item->nama;

            return $output;
        }, []);
    }

    public function deleteSubItem($model)
    {
        $saldo_awal = Produk::where('tipe_barang', 5)->first();
        DetailInformasiPemasok::where('informasi_pemasok_id', $model->id)->delete();
        $faktur     = FakturPembelian::with('barang')->where('pemasok_id', $model->id)
                                                 ->whereHas('barang', function ($query) use ($saldo_awal)
                                                 {
                                                     $query->where('produk_id', $saldo_awal->id);
                                                 });
        $dataFaktur = $faktur->pluck('id');
        Transaksi::whereIn('item_id', $dataFaktur)->where('item_type', get_class(new FakturPembelian()))->delete();
        $faktur     = $faktur->delete();
        BarangFakturPembelian::whereIn('faktur_pembelian_id', $dataFaktur)->delete();
        KontakPelanggan::where('pemasok_id', $model->id)->delete();

        return true;
    }

    public function insertSubItem($data, $model)
    {
        $arrSaldo = [];
        $transaksiDebit = [];
        $transaksiKredit = [];
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        $akun = PreferensiMataUang::all();
        $akun_balance = Akun::where('nama_akun', 'Opening Balance Equity')->pluck('id');
        $saldo_awal = Produk::where('tipe_barang', 5)->first();

        if (!empty($data['no_faktur_saldo'])) {
            foreach ($data['no_faktur_saldo'] as $key => $faktur) {
                $data['saldo_awal'][$key] = str_replace(['Rp ', '.00', ','], '', $data['saldo_awal'][$key]);
                $data['tanggal_saldo'][$key] = carbon::parse($data['tanggal_saldo'][$key])->format('Y-m-d H:i:s');
                
                $arrSaldoAwalPurpose = [
                    'no_faktur' => $this->code->autoGenerate(new FakturPembelian(), 'no_faktur', $data['no_faktur_saldo'][$key]),
                    'pemasok_id' => $model->id,
                    'form_no' => '0',
                    'invoice_date' => $data['tanggal_saldo'][$key],
                    'no_fp_std' => '.',
                    'no_fp_std2' => '.',
                    'no_fp_std_date' => '.',
                    'keterangan' => 'Saldo Awal',
                    'catatan' => 'Saldo Awal',
                    'uang_muka' => '0',
                    'total' => $data['saldo_awal'][$key],
                    'term_id'   => $data['syarat_pembayaran_saldo_id'][$key],
                ];

                $fakturPembelian = FakturPembelian::create($arrSaldoAwalPurpose);

                $arrSaldo[] = [
                    'no_faktur' => $faktur,
                    'tanggal' => $data['tanggal_saldo'][$key],
                    'saldo_awal' => $data['saldo_awal'][$key],
                    'syarat_pembayaran_id' => $data['syarat_pembayaran_saldo_id'][$key],
                    'informasi_pemasok_id' => $model->id,
                    'created_at' => $jam_sekarang,
                    'updated_at' => $jam_sekarang,
                ];

                $arrItemSaldoAwalPurpose[] = [
                    'produk_id'             => $saldo_awal->id,
                    'faktur_pembelian_id'   => $fakturPembelian->id,
                    'item_deskripsi'        => $saldo_awal->keterangan,
                    'item_unit'             => $saldo_awal->unit,
                    'jumlah'                => $saldo_awal->kuantitas,
                    'unit_price'            => $data['saldo_awal'][$key],
                    'diskon'                => $saldo_awal->diskon,
                    'harga_modal'           => $data['saldo_awal'][$key],
                    'harga_final'           => $data['saldo_awal'][$key],
                    'created_at'            => $jam_sekarang,
                    'updated_at'            => $jam_sekarang,
                ];

                $transaksiKredit = [
                    'nominal' => $data['saldo_awal'][$key],
                    'tanggal' => $data['tanggal_saldo'][$key],
                    'id' => $faktur,
                    'dari' => 'Saldo Awal per Item '.$key,
                    'akun' => $akun->first()->akun_hutang_id,
                    'item_id' => $fakturPembelian->id,
                ];
                $transaksiDebit = [
                    'nominal' => $data['saldo_awal'][$key],
                    'tanggal' => $data['tanggal_saldo'][$key],
                    'dari' => 'Saldo Awal per Item balance '.$key,
                    'id' => $faktur,
                    'akun' => $akun_balance[0],
                    'item_id' => $fakturPembelian->id,
                ];

                $this->transaksi->ExecuteTransactionSingle('insert', $fakturPembelian, $transaksiKredit); //kredit
                $this->transaksi->ExecuteTransactionSingle('insert', $fakturPembelian, $transaksiDebit, 1); //debit
            }    // code...

            DetailInformasiPemasok::insert($arrSaldo);
            BarangFakturPembelian::insert($arrItemSaldoAwalPurpose);
        }

        $this->saveKontak($model, $data);

        return true;
    }

    public function saveKontak($model, $data)
    {
        $arrKontak = [];
        if (!empty($data['nama_kontak'])) {
            foreach ($data['nama_kontak'] as $x => $kontak) {
                if (!empty($kontak)) {
                    $arrKontak[] = [
                        'pemasok_id'        => null,
                        'pemasok_id'        => $model->id,
                        'nama'              => $data['nama_kontak'][$x],
                        'nama_depan'        => $data['nama_depan_kontak'][$x],
                        'jabatan'           => $data['jabatan_kontak'][$x],
                        'telepon_kontak'    => $data['telepon_kontak'][$x],
                        'created_at'        => $jam_sekarang,
                        'updated_at'        => $jam_sekarang,
                    ];
                }
            }
            KontakPelanggan::insert($arrKontak);
        }

        return true;
    }
    
    public function historyBukuBesar($request, $type, $id)
    {
        $pemasok = $this->model->find($id);

        $data = $request->all();

        if($pemasok === null){
            return abort(404);
        }
        
        $title = 'History Buku Besar Pemasok : '.$pemasok->nama;
        $ini = $this;

        $transaksiPemasok =   $this->isTransaksi
                                ->where(function ($query) use (&$ini, &$pemasok) {
                                    $query->where('status', 0)
                                    ->whereHas('akun', function($query) {
                                        $query->whereHas('tipeAkun', function($queries) {
                                            $queries->where('title', 'Akun Hutang');
                                        });
                                    })
                                    ->where('item_type', get_class($this->fakturPembelian))
                                    ->whereIn('item_id', $ini->fakturPembelian->where('pemasok_id', $pemasok->id)->pluck('id'));
                                })
                                ->orWhere(function ($query) use (&$ini, &$pemasok) {
                                    $query->where('status', 1)
                                    ->whereHas('akun', function($query) {
                                        $query->whereHas('tipeAkun', function($queries) {
                                            $queries->where('title', 'Akun Hutang');
                                        });
                                    })
                                    ->where('item_type', get_class($this->pembayaranPembelian))
                                    ->whereIn('item_id', $ini->pembayaranPembelian->where('pemasok_id', $pemasok->id)->pluck('id'));
                                })
                                ->orWhere(function ($query) use (&$ini, &$pemasok) {
                                    $query->where('status', 1)
                                    ->whereHas('akun', function($query) {
                                        $query->whereHas('tipeAkun', function($queries) {
                                            $queries->where('title', 'Akun Hutang');
                                        });
                                    })
                                    ->where('item_type', get_class($this->returPembelian))
                                    ->whereIn('item_id', $ini->returPembelian->where('pemasok_id', $pemasok->id)->pluck('id'));
                                })
                                ->filter($data)
                                ->get();

        $items = $transaksiPemasok->map(function($query){
            return [
                'tanggal_formatted' => $query->tanggal_formatted,
                'sumber' => $query->sumber,
                'no_transaksi' => $query->no_transaksi,
                'kode_akun' => $query->akun['kode_akun'],
                'nama_akun' => $query->akun['nama_akun'],
                'keterangan' => $query->keterangan,
                'nominal' => $query->nominal_formatted,
                'status' => $query->status
            ];
        });

        $view = [
            'akun'          => $this->historyBukuBesarRepository->listAkun(),
            'tipe_akun'     => $this->historyBukuBesarRepository->listTipeAkun(),
            'name_space'    => $this->historyBukuBesarRepository->listNameSpace(),
            'items'         => $items,
            'title'         => $title,
            'module_url'    => null,
            'id'            => $id,
            'type'          => $type,
            'filter'        => [
                                    'no_transaksi',
                                    'no_akun',
                                    'tanggal_dari_sampai',
                                    'akun_id',
                                    'tipe_akun_id',
                                    'filter_name_space',
                               ],
        ];
        return view('akuntansi::history_buku_besar.index')->with($view);
    }
}
