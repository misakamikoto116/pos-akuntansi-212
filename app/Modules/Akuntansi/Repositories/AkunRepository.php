<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\DetailJurnalUmum;
use App\Modules\Akuntansi\Models\JurnalUmum;
use App\Modules\Akuntansi\Models\MataUang;
use App\Modules\Akuntansi\Models\Repositories\has_childish;
use App\Modules\Akuntansi\Models\TipeAkun;
use App\Modules\Akuntansi\Models\Transaksi;
use Carbon\Carbon;
use Generator\Interfaces\RepositoryInterface;
use Helpers\TransaksiHelper;
use Helpers\has_child;
use Illuminate\Support\Facades\DB;

class AkunRepository implements RepositoryInterface
{
    protected $tipe_akun;
    public function __construct(Akun $model, TipeAkun $tipe_akun, MataUang $mataUang, TransaksiHelper $transaksi, Transaksi $isTransaksi)
    {
        $this->model     = $model;
        $this->tipe_akun = $tipe_akun;
        $this->mataUang  = $mataUang;
        $this->transaksi = $transaksi;
        $this->isTransaksi = $isTransaksi;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        # Naikkan Memory Limit Karena data terlalu Berat
        ini_set('memory_limit', '4095M'); 
        $data   = $this->model->with(['parentAkun','childAkun','tipeAkun'])->orderBy('kode_akun')->get();
        $model  = $this->getAkun($data);
        # Turunkan Kembali Memory Limit Ketika Proses Query sudah di jalankan
        # Untuk mengembalikan Performa Server seperti sediakala
        ini_set('memory_limit', '512M');
        return $model;
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if( !auth()->user()->hasPermissionTo('hapus_daftar_akun') ){
            return abort(403);
        }

        $model = $this->model->with('detailJurnalUmum')->findOrFail($id);
        $this->delJurnalUmum($model);
        return $model->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data = $this->formattingAllData($data);

        $data['money_function_transaction'] = $data['money_function'];
        $data['money_function']             = 0;

        $data                               = $this->checkData($data);
        if(!empty($data['parent_id'])){
            // cek parentnya.. sdh pernah punya anak / belum
            $parent = Akun::where('id',$data['parent_id'])->first();
            // wah blm punya anak
            $child = Akun::where('parent_id', $parent->id)->get();
            if(count($child) <= 0 && $parent->parent_id == null && $parent->tipe_akun_id == $data['tipe_akun_id']){
                // cloning dirinya (parent)
                $clone = [
                    "kode_akun"      => $parent->kode_akun.".01",
                    "nama_akun"      => $parent->nama_akun,
                    "tipe_akun_id"   => $parent->tipe_akun_id,
                    "parent_id"      => $parent->id,
                    "mata_uang_id"   => $parent->mata_uang_id,
                    "money_function" => $parent->money_function,
                    "tanggal_masuk"  => $parent->tanggal_masuk,
                ];
                Akun::insert($clone);
            }
        }
        $model = $this->findItem($id)->fill($data);
        if ($model->save()) {
            $this->insertTransaction($model, $data, "update");
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {   
        $data   = $this->createdByUser($data);
        $data   = $this->formattingAllData($data);

        $data['money_function_transaction'] = $data['money_function'];
        $data['money_function']             = 0;

        $data = $this->checkData($data);

        if(!empty($data['parent_id'])){
            // cek parentnya.. sdh pernah punya anak / belum
            $parent = Akun::where('id',$data['parent_id'])->first();
            // wah blm punya anak
            $child = Akun::where('parent_id', $parent->id)->get();
            if(count($child) <= 0 && $parent->parent_id == null && $parent->tipe_akun_id == $data['tipe_akun_id']){
                // cloning dirinya (parent)
                $clone = [
                    "kode_akun"      => $parent->kode_akun.".01",
                    "nama_akun"      => $parent->nama_akun,
                    "tipe_akun_id"   => $parent->tipe_akun_id,
                    "parent_id"      => $parent->id,
                    "mata_uang_id"   => $parent->mata_uang_id,
                    "money_function" => $parent->money_function,
                    "tanggal_masuk"  => $parent->tanggal_masuk,
                    'created_by'     => auth()->user()->id
                ];
                Akun::insert($clone);
            }
        }

        $model = $this->model->fill($data);
        $model->save();
        if ($data['money_function_transaction'] !== "0" && $data['money_function_transaction'] !== null) {
                $this->insertTransaction($model, $data,"insert");
        }
        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($request)
    {
        $data   = $this->model->with(['parentAkun','childAkun','tipeAkun'])->orderBy('kode_akun')->filter($request)->get();
        $model  = $this->getAkun($data);
        return $model;
    }

    public function getAkun($data)
    {
        $data_arr = [];
        $saldo = 0;
        $parent_stat = 0; 
        $akun = $this->model;

        // $data = $this->model->orderBy('kode_akun')->paginate(10);
        $data->transform(function($item, $key) use (&$data_arr, &$saldo, &$parent_stat, $akun){
            $saldo = 0;
            $parent_stat = 0; 

            $saldo_kredit   = $item->transaksi->where('status', 0)->sum('nominal');          

            $saldo_debit    = $item->transaksi->where('status', 1)->sum('nominal');

            $saldo = $saldo_debit - $saldo_kredit;

            if(count($item->childAkun) > 0){
                $parent_stat++;
                $saldo = $akun->recrusiveAkun($item->childAkun, $saldo, $parent_stat);                  
            }
            $data_arr = [
                'id'             => $item->id,
                'parent_id'      => $item->parent_id,
                'kode_akun'      => $item->kode_akun,
                'nama_akun'      => $item->nama_akun,
                'money_function' => $item->money_function,
                'tipe_akun'      => $item->tipeAkun->title,
                'childAkun'      => $item->childAkun,
                'saldo'          => $saldo,
                'parent_stat'    => $parent_stat,
                'created_by'     => $item->user_created,
                'updated_by'     => $item->user_updated,
                'created_at'     => Carbon::parse($item->created_at)->format('d F Y'),
                'updated_at'     => Carbon::parse($item->updated_at)->format('d F Y')
            ];

            return $data_arr;
        });

        return $data;
    }

    public function listTipeAkun()
    {
        return $this->tipe_akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->title;

            return $output;
        }, []);
    }
    public function listAkun()
    {
        return $this->model->get()->reduce(function ($output, $item) {
            $output[$item->kode_akun][$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function listMataUang()
    {
        return $this->mataUang->pluck('kode','id');
    }

    public function checkData($data) : array
    {
        /** sesuatu HARUS kosong ketika tipe_akun_id == ??
         *   'mata_uang_id',
         *   'fiksal',
         *   'alokasi_ke_produksi',
         *   'money_function',
         */
        if (array_key_exists('tanggal_masuk', $data)) {
            $data['tanggal_masuk'] = Carbon::parse($data['tanggal_masuk'])->format('Y-m-d H:i:s');
        }

         if($data['tipe_akun_nama'] === 'Kas/Bank' 
        || $data['tipe_akun_nama'] === 'Aktiva Lancar' 
        || $data['tipe_akun_nama'] === 'Aktiva Lainnya' 
        || $data['tipe_akun_nama'] === 'Hutang Lancar Lainnya'){
            /** Mata Uang & Saldo Awal */
            unset($data['fiksal']);
            unset($data['alokasi_ke_produksi']);

        } elseif ($data['tipe_akun_nama'] === 'Akun Piutang' 
        || $data['tipe_akun_nama'] === 'Akun Hutang') {
            /** Mata Uang*/
            unset($data['fiksal']);
            unset($data['alokasi_ke_produksi']);
            unset($data['money_function']);
            // unset($data['tanggal_masuk']);
        
        } elseif ($data['tipe_akun_nama'] === 'Persediaan' 
        || $data['tipe_akun_nama'] === 'Ekuitas') {
            /** Saldo Awal*/
            unset($data['fiksal']);
            unset($data['alokasi_ke_produksi']);
            unset($data['mata_uang_id']);
        } elseif ($data['tipe_akun_nama'] === 'Aktiva Tetap'
        || $data['tipe_akun_nama'] === 'Akumulasi Penyusutan'
        || $data['tipe_akun_nama'] === 'Hutang Jangka Panjang'
        || $data['tipe_akun_nama'] === 'Beban'
        || $data['tipe_akun_nama'] === 'Beban lain-lain'){
            /** Tidak ada tambahan */
            unset($data['money_function']);
            unset($data['fiksal']);
            unset($data['alokasi_ke_produksi']);
            // unset($data['tanggal_masuk']);
            unset($data['mata_uang_id']);
        } elseif ($data['tipe_akun_nama'] === 'Pendapatan'){
            /** Fiksal & Saldo Awal */
            unset($data['alokasi_ke_produksi']);
            unset($data['mata_uang_id']);
        } elseif ($data['tipe_akun_nama'] === 'Harga Pokok Penjualan') {
            /** Fiksal, Dialokasikan, & Saldo Awal */
            unset($data['mata_uang_id']);
        }else if ($data['tipe_akun_nama'] === 'Hutang Jangka Panjang') {
            /** Saldo awal & mata uang */
            unset($data['fiksal']);
            unset($data['alokasi_ke_produksi']);
        } else if ($data['tipe_akun_nama'] === 'Beban' || $data['tipe_akun_nama'] === 'Beban lain-lain') {
            unset($data['mata_uang_id']);
        } 
       
        if(empty($data['use_parent'])){
            unset($data['parent_id']);
        }else{
            unset($data['use_parent']);
        }

        unset($data['tipe_akun_nama']);

        return $data;
    }

    public function get_all_sub_caseTypeId($parent){

        $branch = [];

        $branch['model'] = $parent;
        
        $query = $this->model->where('parent_id', $parent->id)->get();
        
        foreach ($query as $key => $rows) {
            $branch['child'][$rows->id] = [
                'model' => $rows
            ];

            if ($this->has_child($rows->id)) {
                $branch['child'][$rows->id] = $this->get_all_sub_caseTypeId($rows);
            }
        }
        
        return $branch;
    
    }

    public function has_child($parent_id){
        $akun = $this->model->where('parent_id', $parent_id)->get();

        if ($akun->isEmpty()) {
            return false;
        }

        return true;
    }  

    public function insertTransaction($model, $data, $type)
    {
        $tanggal_sekarang = date("Y-m-d H:i:s");
        if (array_key_exists('tanggal_masuk', $data)) {

            if ($type === "insert") {
                $modelJurnalUmum = $this->insertJurnalUmum($data);
            }else if($type === "update") {
                $modelJurnalUmum = $this->updateJurnalUmum($data, $model);
            }

            if ($modelJurnalUmum->count() > 0) {
                if (!is_bool($modelJurnalUmum)) {
                    if ($data['tipe_akun_id'] === "10"
                       || $data['tipe_akun_id'] === "9"
                       || $data['tipe_akun_id'] === "11"
                       || $data['tipe_akun_id'] === "12"
                       || $data['tipe_akun_id'] === "15") {

                        $detail[] = [
                            'akun_id'        => $model->id,
                            'jurnal_umum_id' => $modelJurnalUmum->id,
                            'debit'          => 0,
                            'kredit'         => $data['money_function_transaction'],
                            'created_at'     => $tanggal_sekarang,
                            'updated_at'     => $tanggal_sekarang,
                            'created_by'     => auth()->user()->id,
                        ];

                        $detail[] = [
                            'akun_id'        => $this->akunOpening()->id,
                            'jurnal_umum_id' => $modelJurnalUmum->id,
                            'debit'          => $data['money_function_transaction'],
                            'kredit'         => 0,
                            'created_at'     => $tanggal_sekarang,
                            'updated_at'     => $tanggal_sekarang,
                            'created_by'     => auth()->user()->id,
                        ];

                        DetailJurnalUmum::insert($detail);

                        # T R A N S A K S I 

                        $transaksi = [
                            'nominal'       => $data['money_function_transaction'],
                            'tanggal'       => $data['tanggal_masuk'],
                            'created_by'    => auth()->user()->id,
                        ];

                        $kredit             = $transaksi;
                        $debit              = $transaksi;
                        $kredit['akun']     = $model->id;
                        $debit['akun']      = $this->akunOpening()->id;
                        $kredit['dari']     = 'Saldo Awal Akun per item '.$modelJurnalUmum->id;
                        $debit['dari']      = 'Saldo Awal Akun per item balance '.$modelJurnalUmum->id;

                        $this->transaksi->ExecuteTransactionSingle('insert', $modelJurnalUmum, $debit, 1, $this->generateNoFaktur($data['tanggal_masuk']));
                        $this->transaksi->ExecuteTransactionSingle('insert', $modelJurnalUmum, $kredit, 0,$this->generateNoFaktur($data['tanggal_masuk']));    

                    }else {

                        $detail[] = [
                            'akun_id'        => $model->id,
                            'jurnal_umum_id' => $modelJurnalUmum->id,
                            'debit'          => $data['money_function_transaction'],
                            'kredit'         => 0,
                            'created_at'     => $tanggal_sekarang,
                            'updated_at'     => $tanggal_sekarang,
                            'created_by'     => auth()->user()->id,
                        ];

                        $detail[] = [
                            'akun_id'        => $this->akunOpening()->id,
                            'jurnal_umum_id' => $modelJurnalUmum->id,
                            'debit'          => 0,
                            'kredit'         => $data['money_function_transaction'],
                            'created_at'     => $tanggal_sekarang,
                            'updated_at'     => $tanggal_sekarang,
                            'created_by'     => auth()->user()->id,
                        ];

                        DetailJurnalUmum::insert($detail);

                        # T R A N S A K S I 

                        $transaksi = [
                            'nominal'           => $data['money_function_transaction'],
                            'tanggal'           => $data['tanggal_masuk'],
                            'created_by'        => auth()->user()->id,
                        ];

                        $debit  = $transaksi;
                        $kredit = $transaksi;
                        $debit['akun']  = $model->id;
                        $kredit['akun'] = $this->akunOpening()->id;
                        $debit['dari']  = 'Saldo Awal Akun per item '.$modelJurnalUmum->id;
                        $kredit['dari'] = 'Saldo Awal Akun per item balance '.$modelJurnalUmum->id;

                        $this->transaksi->ExecuteTransactionSingle('insert', $modelJurnalUmum, $debit, 1, $this->generateNoFaktur($data['tanggal_masuk']));
                        $this->transaksi->ExecuteTransactionSingle('insert', $modelJurnalUmum, $kredit, 0,$this->generateNoFaktur($data['tanggal_masuk']));

                    }
                }
            }
        }

        return true;
    }

    public function generateNoFaktur($tanggal)
    {
        $tanggal = Carbon::parse($tanggal)->format('d-m-Y');
        $last_jurnal = JurnalUmum::orderBy('id','DESC')->first();
        if ($last_jurnal === null) {
            $last_jurnal = $last_jurnal + 1;
        }else {
            $last_jurnal = $last_jurnal->id + 1;
        }
        $format_no_faktur = sprintf('saldo-awal/%s/%s', $tanggal, $last_jurnal);

        return $format_no_faktur;
    }

    public function insertJurnalUmum($data)
    {
        $inputJurnalUmum = [
            'no_faktur'   => $this->generateNoFaktur($data['tanggal_masuk']),
            'tanggal'     => $data['tanggal_masuk'],
            'description' => 'Saldo Awal',
            'status'      => 0,
            'created_by'  => auth()->user()->id
        ];

        return JurnalUmum::create($inputJurnalUmum);
    }

    public function updateJurnalUmum($data, $model)
    {
        $jurnal_umum = $this->getJurnalUmum($model);

        if ($data['money_function_transaction'] > 0) {
            if ($jurnal_umum->isNotEmpty()) {
                $jurnal_umum = $this->deleteDetailJurnal($jurnal_umum);
                $jurnal_umum->update([
                    'no_faktur'     => $this->generateNoFaktur($data['tanggal_masuk']),
                    'tanggal'       => $data['tanggal_masuk'],
                    'description'   => 'Saldo Awal',
                    'status'        => 0,
                ]);
            }else {
                $jurnal_umum = $this->insertJurnalUmum($data);    
            }
        }else  {
            if ($jurnal_umum->isNotEmpty()) {
                $jurnal_umum = $this->delJurnalUmum($model);
            }
        }

        return $jurnal_umum;
    }

    public function delJurnalUmum($akun)
    {
        $jurnal_umum = $this->getJurnalUmum($akun);

        if ($jurnal_umum->isNotEmpty()) {
            $jurnal_umum = $this->deleteDetailJurnal($jurnal_umum);
            $jurnal_umum->delete();
        }
        return true;
    }

    public function deleteDetailJurnal($jurnal_umum)
    {
        $jurnal_umum    = $jurnal_umum->first();
        $detail_jurnal  = DetailJurnalUmum::where('jurnal_umum_id', $jurnal_umum->id);

        $this->delTransactionDetailJurMum($jurnal_umum);

        $detail_jurnal->delete();

        return $jurnal_umum;
    }

    public function delTransactionDetailJurMum($jurnal_umum)
    {
        $this->transaksi->ExecuteTransactionSingle("delete", $jurnal_umum);

        return true;
    }

    public function getJurnalUmum($model)
    {
        $jurnal_umum    = JurnalUmum::with('detailJurnalUmum')->where("status", 0)
                                                              ->whereHas('detailJurnalUmum', function ($query) use ($model)
                                                              {
                                                                  $query->where('akun_id', $model->id);
                                                              })->get();
        return $jurnal_umum;
    }

    public function getInsertedIds($data)
    {
        $ids = [];
        foreach ($data as $data_detail) {
            $ids[] = DetailJurnalUmum::insertGetId($data_detail);
        }
        if (!empty($ids)) {
            $arrDetail = [
                'first'   => DetailJurnalUmum::findOrFail($ids[0]),
                'second'  => DetailJurnalUmum::findOrFail($ids[1])
            ];
        }else {
            $arrDetail = [
                null
            ];
        }
        return $arrDetail;
    }

    public function akunOpening()
    {
        return Akun::where('nama_akun','Opening Balance Equity')->first();
    }

    public function bukuBank($request)
    {
        $akun = $this->model->where('id', $request['akun'])
                ->whereDoesntHave('childAkun')->whereHas('tipeAkun', function ($query)
                {
                    $query->where('title','Kas/Bank');
                })->first();

        if($akun === null){
            return abort(404);
        }

        $tanggal_dari = Carbon::parse($request->get('tanggal_dari'))->format('Y-m-d H:i:s');
        $tanggal_sampai = Carbon::parse($request->get('tanggal_sampai'))->format('Y-m-d H:i:s');

        $semua_transaksi = $this->isTransaksi->whereHas('akun', function ($query) use ($request, $tanggal_dari, $tanggal_sampai)
        {
            $query->where('id', $request['akun']);
        })->whereBetween('tanggal', [$tanggal_dari, $tanggal_sampai])->get()->sortBy('tanggal');
        
        if ($request->ajax()) {
            $view = view('akuntansi::buku_bank.content', compact('semua_transaksi','akun'))->render();
            return response()->json($view);
        }else{
            $view = [
                'from_akun' => true,
                'listAkun' => $akun->nama_akun,
                'listAkunTarget' => $akun->id,
                'semua_transaksi' => $semua_transaksi
            ];
            return view('akuntansi::buku_bank.index')->with($view);
        }
    }

    public function formattingAllData($data)
    {
        $data['money_function'] = floatval(preg_replace('/[^\d.]/', '', $data['money_function']));
        
        return $data;
    }

    public function createdByUser($data)
    {
        $data['created_by'] = auth()->user()->id;

        return $data;
    }
}
