<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang as ProdukDetail;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\PemindahanBarang;
use App\Modules\Akuntansi\Models\DetailPemindahanBarang;
use Generator\Interfaces\RepositoryInterface;
use Helpers\IndonesiaDate as DateHelper;
use Carbon\carbon;

class PemindahanBarangRepository implements RepositoryInterface
{
    public function __construct(PemindahanBarang $model, Produk $produk, Gudang $gudang, DateHelper $date)
    {
        $this->model = $model;
        $this->produk = $produk;
        $this->gudang = $gudang;
        $this->date = $date;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->with(['dataDariGudang','dataKeGudang'])->orderBy('id','DESC')->paginate(20);;
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_pindah_barang')){
            return abort(403);
        }

        $check = $this->model->findOrFail($id);
        $this->deleteSubData($check);
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        if (empty($data['tanggal'])) {
            $data['tanggal'] = date('Y-m-d');
        } else {
            $data['tanggal'] = $this->date->IndonesiaToSql($data['tanggal']);
        }
        $model = $this->findItem($id)->fill($data);
        if ($model->save()) {
            $this->deleteSubData($model);
            $this->updateProdukGudang($data, $model);
            $this->updateDetailProdukGudang($data, $model);
            $this->insertDetailPemindahanGudang($data, $model);
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        if (empty($data['tanggal'])) {
            $data['tanggal'] = date('Y-m-d');
        } else {
            $data['tanggal'] = $this->date->IndonesiaToSql($data['tanggal']);
        }
        $model = $this->model->fill($data);
        if ($model->save()) {
            $this->updateProdukGudang($data, $model);
            $this->updateDetailProdukGudang($data, $model);
            $this->insertDetailPemindahanGudang($data, $model);
        }

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->with(['dataDariGudang','dataKeGudang'])->orderBy('id','DESC')->filter($data)->paginate(20);
    }

    public function listProduk()
    {
        return $this->produk->where('tipe_barang', '!==', 3)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->no_barang." | ".$item->keterangan;

            return $output;
        }, []);
    }

    public function listGudang()
    {
        return $this->gudang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function insertDetailPemindahanBarang($data, $model)
    {
        $count = count($data['produk_id']);
        $items = [];
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');

        for ($i = 0; $i < $count; ++$i) {
            $items[] = [
                'pemindahan_barang_id' => $model->id,
                'produk_id' => $data['produk_id'][$i],
                'keterangan' => $data['keterangan'][$i],
                'jumlah' => $data['jumlah'][$i],
                'satuan' => $data['satuan'][$i],
                'serial_number' => $data['serial_number'][$i],
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];
        }
        DetailPemindahanBarang::insert($items);
    }

    public function updateProdukGudang($data, $model)
    {
        // $produk_id = $data['produk_id'];
        // foreach ($produk_id as $key => $value) {
        //     $dataa = Produk::where('id', $value)->update([
        //         'gudang_id' => $model->ke_gudang_id,
        //     ]);
        // }
    }

    public function updateDetailProdukGudang($data, $model)
    {
        $produk_id     = $data['produk_id'];
        $produkDetail  = [];
        $dataBerkurang = [];
        $dataBertambah = [];

        foreach ($produk_id as $key => $value) {
            $latestProduk = ProdukDetail::where([
                'produk_id' => $value,
                'gudang_id' => $model->dari_gudang_id
            ])->latest()->first();
                if($latestProduk){
                    $produkDetail[$key] = [
                        'no_faktur'      => $data['no_transfer'],
                        'tanggal'        => date('Y-m-d H:i:s'),
                        'harga_modal'    => $latestProduk->harga_modal,
                        'harga_terakhir' => $latestProduk->harga_terakhir,
                        'transaksi_id'   => $latestProduk->transaksi_id,
                        'sn'             => $model->serial_number,
                        'status'         => $latestProduk->status,
                        'item_type'      => get_class($this->model),
                        'item_id'        => $model->id,
                        'produk_id'      => $value,
                        'created_at'     => date('Y-m-d H:i:s'),
                    ];
                    $dataBerkurang[$key]              = $produkDetail[$key];
                    $dataBerkurang[$key]['gudang_id'] = $model->dari_gudang_id;
                    $dataBerkurang[$key]['kuantitas'] = "-".$data['jumlah'][$key];

                    $dataBertambah[$key]              = $produkDetail[$key];
                    $dataBertambah[$key]['gudang_id'] = $model->ke_gudang_id;
                    $dataBertambah[$key]['kuantitas'] = $data['jumlah'][$key];
                
                }
            }
        ProdukDetail::insert($dataBerkurang);
        ProdukDetail::insert($dataBertambah);

    }

    public function insertDetailPemindahanGudang($data, $model)
    {
        $count = count($data['produk_id']);
        $items = [];
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        for ($i = 0; $i < $count; ++$i) {
            $items[] = [
                'pemindahan_barang_id' => $model->id,
                'produk_id' => $data['produk_id'][$i],
                'keterangan' => $data['keterangan_produk'][$i],
                'jumlah' => $data['jumlah'][$i],
                'satuan' => $data['satuan'][$i],
                'serial_number' => $data['serial_number'][$i],
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];
        }
        DetailPemindahanBarang::insert($items);
    }

    public function deleteSubData($model)
    {
        DetailPemindahanBarang::where('pemindahan_barang_id', $model->id)->delete();
        ProdukDetail::where([
            'item_type' => get_class($this->model),
            'item_id'   => $model->id,
        ])->delete();

        return true;
    }
}
