<?php
namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\PesananPenjualan;
use App\Modules\Akuntansi\Models\PenawaranPenjualan;
use App\Modules\Akuntansi\Models\BarangPenawaranPenjualan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\KategoriProduk;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use App\Modules\Akuntansi\Models\JasaPengiriman;
use App\Modules\Akuntansi\Models\BarangPesananPenjualan;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use Carbon\carbon;
use Helpers\IndonesiaDate as DateHelper;
use Helpers\TransaksiHelper;
use Helpers\CodeHelper;
use Generator\Interfaces\RepositoryInterface;

class PesananPenjualanRepository implements RepositoryInterface
{
	public function __construct(PesananPenjualan $model, KategoriProduk $kategori, Gudang $gudang, Akun $akun, InformasiPelanggan $pelanggan, SyaratPembayaran $termin, Produk $produk, JasaPengiriman $pengiriman, DateHelper $date, CodeHelper $code, KodePajak $kode_pajak, TransaksiHelper $transaksi, PenawaranPenjualan $penawaran, PreferensiMataUang $preferensi_mata_uang)
	{
		$this->produk     = $produk;
		$this->model      = $model;
		$this->kategori   = $kategori;
		$this->gudang     = $gudang;
		$this->akun       = $akun;
		$this->pelanggan  = $pelanggan;
		$this->termin     = $termin;
		$this->pengiriman = $pengiriman;
		$this->date       = $date;
		$this->code       = $code;
		$this->kode_pajak = $kode_pajak;
		$this->transaksi  = $transaksi;
		$this->penawaran  = $penawaran;
		$this->preferensi_mata_uang  = $preferensi_mata_uang;
	}

	/**
	 * ini untuk mengambil data keseluruhan
	 * user di data repositori.
	 *
	 * @return Collection data list user
	 */
	public function getItems()
	{
		# Naikkan Memory Limit Karena data terlalu Berat
		ini_set('memory_limit', '4095M'); 
		$model = $this->model->orderBy('so_date')->paginate(20);
		# Turunkan Kembali Memory Limit Ketika Proses Query sudah di jalankan
        # Untuk mengembalikan Performa Server seperti sediakala
        ini_set('memory_limit', '512M');
		return $model;
	}

	/**
	 * ini untuk mencari user berdasarkan id yang dicari.
	 *
	 * @param int $id
	 *
	 * @return object
	 */
	public function findItem($id)
	{
		$pesanan_penjualan            = $this->model->findOrFail($id);
		$pesanan_penjualan->so_date   = Carbon::parse($pesanan_penjualan->so_date)->format('d F Y');
		$pesanan_penjualan->ship_date = Carbon::parse($pesanan_penjualan->ship_date)->format('d F Y');

		$pesanan_penjualan->ongkir = number_format($pesanan_penjualan->ongkir); 
        $pesanan_penjualan->total_potongan_rupiah = number_format($pesanan_penjualan->total_potongan_rupiah);

		return $pesanan_penjualan;
	}

	/**
	 * ini untuk menghapus data berdasarkan id.
	 *
	 * @param [type] $id [description]
	 *
	 * @return [type] [description]
	 */
	public function delete($id)
	{
		if(!auth()->user()->hasPermissionTo('hapus_pesanan_penjualan')){
            return abort(403);
        }
		$check          = $this->model->findOrFail($id);
		$subItem        = BarangPesananPenjualan::where('pesanan_penjualan_id', $check->id);
		$data_penawaran = BarangPenawaranPenjualan::whereIn('id', $subItem->pluck('barang_penawaran_penjualan_id'))->pluck('penawaran_penjualan_id');
		$this->transaksi->UpdateParentStatus($this->penawaran, $subItem, 'available', 'penawaran_penjualan_id', $data_penawaran);

		$subItem->delete();

		return $this->findItem($id)->delete();
	}

	/**
	 * update data berdasarkan id dan data
	 * didapat dari variable request.
	 *
	 * @param [type] $id   [description]
	 * @param [type] $data [description]
	 *
	 * @return [type] [description]
	 */
	public function update($id, $data)
	{

		$checkData = $this->model->where('po_number', $data['po_number'])
                    ->where('id', '!=', $id)->first();
        
        if($checkData){
			throw new \Exception('Nomor Purchase Order terduplikat!');            
        }

		$data = $this->formattingSingleData($data);

		if (empty($data['so_date'])) {
			$data['so_date'] = date('Y-m-d');
		} else {
			$data['so_date'] = $this->date->IndonesiaToSql($data['so_date']);
		}
		if (empty($data['ship_date'])) {
			$data['ship_date'] = date('Y-m-d');
		} else {
			$data['ship_date'] = $this->date->IndonesiaToSql($data['ship_date']);
		}
		$data['taxable'] = (empty($data['taxable'])) ? 0 : 1 ;
        $data['in_tax'] = (empty($data['in_tax'])) ? 0 : 1 ;
		
		$model = $this->findItem($id)->fill($data);

		$subItem        = BarangPesananPenjualan::where('pesanan_penjualan_id', $model->id);
		$data_penawaran = BarangPenawaranPenjualan::whereIn('id', $subItem->pluck('barang_penawaran_penjualan_id'))->pluck('penawaran_penjualan_id');
		$this->transaksi->UpdateParentStatus($this->penawaran, $subItem, 'delete', 'penawaran_penjualan_id', $data_penawaran);

		$subItem->delete();

		$this->InsertPesananItem($model, $data);

		if(!empty($data['lanjutkan'])){
            if($data['lanjutkan'] == 'Simpan & Baru'){
                $model['continue_stat'] = 2;                
            }else{
                $model['continue_stat'] = 1;
            }
        }   

		return $model;
	}

	/**
	 * menambahkan data berdasarkan request.
	 *
	 * @param [type] $request [description]
	 *
	 * @return [type] [description]
	 */
	public function insert($data)
	{
		$data = $this->formattingSingleData($data);

		$data['so_number'] = $this->code->autoGenerate($this->model, 'so_number', $data['so_number']);
		$data['taxable'] = (empty($data['taxable'])) ? 0 : 1 ;
        $data['in_tax'] = (empty($data['in_tax'])) ? 0 : 1 ;
		if (empty($data['so_date'])) {
			$data['so_date'] = date('Y-m-d');
		} else {
			$data['so_date'] = $this->date->IndonesiaToSql($data['so_date']);
		}

		if (empty($data['ship_date'])) {
			$data['ship_date'] = date('Y-m-d');
		} else {
			$data['ship_date'] = $this->date->IndonesiaToSql($data['ship_date']);
		}
		$model = $this->model->fill($data);
		$this->InsertPesananItem($model, $data);

		if(!empty($data['lanjutkan'])){
            if($data['lanjutkan'] == 'Simpan & Baru'){
                $model['continue_stat'] = 2;                
            }else{
                $model['continue_stat'] = 1;
            }
		}        
		
		return $model;
	}

	/**
	 * ini berfungisi untuk melakukan filter terhadap
	 * data yang akan diambil dan ditampilkan kepada
	 * user nantinya.
	 *
	 * @param array $data
	 */
	public function filter($data)
	{
		return $this->model->filter($data)->paginate(10);
	}

	public function listKategoriProduk()
	{
		return $this->kategori->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama;

			return $output;
		}, []);
	}

	public function listGudang()
	{
		return $this->gudang->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama;

			return $output;
		}, []);
	}

	public function listAkun()
	{
		return $this->akun->whereDoesntHave('childAkun')->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama_akun;

			return $output;
		}, []);
	}

	public function listPelanggan()
	{
		return $this->pelanggan->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama;

			return $output;
		}, []);
	}

	public function listProduk($request)
	{
		return $this->produk->filter($request)->where('tipe_barang','!==',3)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->no_barang." | ".$item->keterangan;

            return $output;
        }, []);
	}

    public function listProdukWithDiscount($request)
    {
        return $this->produk->filter($request)
            ->where('tipe_barang','!==',3)
            ->where('gudang_id','=',6)
            ->get()
            ->reduce(function ($output, $item) {
                $output[] = [
                    'id' => $item->id,
                    'no_barang' =>  $item->no_barang,
                    'nama' => $item->keterangan,
                    'diskon' => $item->diskon] ;

                return $output;
            }, []);
    }

	public function listTermin()
	{
		return $this->termin->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->akan_dapat_diskon . '/' . $item->jika_membayar_antara . '/n' . $item->jatuh_tempo;

			return $output;
		}, []);
	}

	public function listJasaPengiriman()
	{
		return $this->pengiriman->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->nama;

			return $output;
		}, []);
	}

	public function listKodePajak()
	{
		return $this->kode_pajak->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->kode_pajak_formatted;

			return $output;
		}, []);
	}

	public function getAkunDp()
	{
		return $this->preferensi_mata_uang->pluck('uang_muka_penjualan_id')->first();
	}

	public function idPrediction()
	{
		$data = $this->model->orderBy('id', 'desc')->first();
		if ($data) {
			return $data->id + 1;
		}

		return 1;
	}

	public function InsertPesananItem($model, $data)
	{
		$items        = [];
		$jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
		$count        = count($data['produk_id']);
		if ($model->save()) {
			for ($i = 0; $i < $count; ++$i) {
				$ditutup   = (empty($data['ditutup'][$i])) ? 0 : $data['ditutup'][$i];
				$terproses = (empty($data['terproses'][$i])) ? 0 : $data['terproses'][$i];
				$pajak1    = null;
				$pajak2    = null;

				if (!empty($data['kode_pajak_id'][$i])) {
					// data ada.. walau hanya satu
					$pajak1 = $data['kode_pajak_id'][$i][0];
					if (count($data['kode_pajak_id'][$i]) > 1) {
						$pajak2 = $data['kode_pajak_id'][$i][1];
					}
				}

				$data = $this->formattingArrayData($data, $i);

				$items[] = [
					'produk_id'                     => $data['produk_id'][$i],
					'pesanan_penjualan_id'          => $model->id,
					'item_deskripsi'                => $data['keterangan_produk'][$i],
					'jumlah'                        => $data['qty_produk'][$i],
					'harga'                         => $data['unit_harga_produk'][$i],
					'satuan'                        => $data['satuan_produk'][$i],
					'barang_penawaran_penjualan_id' => !empty($data['barang_id']) ? $data['barang_id'][$i] : null,
					'diskon'                        => $data['diskon_produk'][$i],
					'harga_modal'                   => $data['harga_modal'][$i],
					'kode_pajak_id'                 => $pajak1,
					'kode_pajak_2_id'               => $pajak2,
					'terproses'                     => $terproses,
					'ditutup'                       => $ditutup,
					'created_at'                    => $jam_sekarang,
					'updated_at'                    => $jam_sekarang,
				];
			}

			if (!empty($data['penawaran_id'])) {
				$this->transaksi->UpdateParentStatus($this->penawaran, $model, 'update', 'penawaran_id', $data);
			}

			BarangPesananPenjualan::insert($items);
		}
	}

	public function formattingSingleData($data)
    {
        $data['ongkir'] 				= floatval(preg_replace('/[^\d.]/', '', $data['ongkir']));
        $data['total_potongan_rupiah']	= floatval(preg_replace('/[^\d.]/', '', $data['total_potongan_rupiah']));

        return $data;
    }

    public function formattingArrayData($data, $i)
    {
       $data['unit_harga_produk'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['unit_harga_produk'][$i]));
       return $data;
    }
}
