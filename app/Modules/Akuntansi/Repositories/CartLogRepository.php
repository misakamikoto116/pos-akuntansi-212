<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Pos\Models\CartLog;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\User;
use Generator\Interfaces\RepositoryInterface;

class CartLogRepository implements RepositoryInterface
{
    public function __construct(CartLog $model, User $user, Produk $produk)
    {
        $this->model = $model;
        $this->user = $user;
        $this->produk = $produk;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->orderBy('id','desc')->paginate(20);       
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->find($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->findItem($id)->fill($data);
        $model->save();

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $model = $this->model->fill($data);
        $model->save();

        return $model;
    }

    /**e
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->orderBy('id','desc')->paginate(20);
    }

}
