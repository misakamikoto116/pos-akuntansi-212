<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\DaftarAktivaTetap;
use App\Modules\Akuntansi\Models\DaftarAktivaTetapDetail;
use App\Modules\Akuntansi\Models\DetailJurnalUmum;
use App\Modules\Akuntansi\Models\JurnalUmum;
use App\Modules\Akuntansi\Models\MetodePenyusutan;
use App\Modules\Akuntansi\Models\PengeluaranAktiva;
use App\Modules\Akuntansi\Models\TipeAktivaTetap;
use App\Modules\Akuntansi\Models\Transaksi;
use Generator\Interfaces\RepositoryInterface;
use Helpers\IndonesiaDate as DateHelper;
use Helpers\TransaksiHelper;
use Carbon\Carbon;

class DaftarAktivaTetapRepository implements RepositoryInterface
{
    public function __construct(DaftarAktivaTetap $model, TipeAktivaTetap $tipeaktivatetap, Akun $akun, MetodePenyusutan $metode_penyusutan, DateHelper $date,
    TransaksiHelper $transaksiHelper, Transaksi $transaksi, PengeluaranAktiva $pengeluaranAktiva, DaftarAktivaTetapDetail $daftarAktivaTetapDetail,
    DetailJurnalUmum $detailJurnalUmum, JurnalUmum $jurnalUmum)
    {
        $this->model = $model;
        $this->daftarAktivaTetapDetail = $daftarAktivaTetapDetail;
        $this->detail_jurnal_umum = $detailJurnalUmum;
        $this->jurnal_umum = $jurnalUmum;
        $this->tipeaktivatetap = $tipeaktivatetap;
        $this->akun = $akun;
        $this->metode_penyusutan = $metode_penyusutan;
        $this->pengeluaranAktiva = $pengeluaranAktiva;
        $this->date = $date;
        $this->transaksi = $transaksi;
        $this->transaksiHelper = $transaksiHelper;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $edit_tanggal = $this->model->find($id);
        $this->convertTanggal($edit_tanggal);

        return $edit_tanggal;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if( !auth()->user()->hasPermissionTo('hapus_aktiva_tetap') ){
            return abort(403);
        }        
        $check = $this->model->findOrFail($id);
        $jurnal = $this->jurnal_umum->where('no_faktur', $check->kode_aktiva);
        $itemJurnal = $jurnal->pluck('id');

        if ($check) {
            PengeluaranAktiva::where('daftar_aktiva_tetap_id', $check->id)->delete();
            $this->daftarAktivaTetapDetail->where('daftar_aktiva_tetap_id', $check->id)->delete();
            $this->transaksi->whereIn('item_id', $itemJurnal)->where('item_type', get_class($this->jurnal_umum))->delete();
            $this->transaksi->where('item_id', $check->id)->where('item_type', get_class($this->model))->delete();
            $jurnal->delete();
            $this->detail_jurnal_umum->whereIn('jurnal_umum_id', $itemJurnal)->delete();
            return $this->findItem($id)->delete();
        }
        
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data = $this->formatingAllData($data);

        $data['aktiva_tidak_berwujud'] = (empty($data['aktiva_tidak_berwujud'])) ? 0 : $data['aktiva_tidak_berwujud'];
        $data['aktiva_tetap_fisikal'] = (empty($data['aktiva_tetap_fisikal'])) ? 0 : $data['aktiva_tetap_fisikal'];

        $model = $this->findItem($id)->fill($this->dateChecker($data));

        if ($model->save()) {
            PengeluaranAktiva::where('daftar_aktiva_tetap_id', $model->id)->delete();
            $this->daftarAktivaTetapDetail->where('daftar_aktiva_tetap_id', $model->id)->delete();
            $this->transaksi->where('item_id', $model->id)->where('item_type')->delete();
            $this->InsertPengeluaranAktiva($data, $model);
            $this->InsertDaftarAktivaTetapDetail($data, $model);
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->formatingAllData($data);

        $checkData = $this->model->where('kode_aktiva', $data['kode_aktiva'])->first();
        if($checkData){
            return redirect()->route('akuntansi.daftar-aktiva-tetap.create')->withErrors('Kode Aktiva '.$data['kode_aktiva'].' Sudah Ada');
        }
        $data['aktiva_tidak_berwujud'] = (empty($data['aktiva_tidak_berwujud'])) ? 0 : $data['aktiva_tidak_berwujud'];
        $data['aktiva_tetap_fisikal'] = (empty($data['aktiva_tetap_fisikal'])) ? 0 : $data['aktiva_tetap_fisikal'];

        $model = $this->model->fill($this->dateChecker($data));
        if ($model->save()) {
            $this->InsertPengeluaranAktiva($data, $model);
            $this->InsertDaftarAktivaTetapDetail($data, $model);
        }

        return $model;
    }

    /**e
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(10);
    }

    public function listTipeAktivaTetap()
    {
        return $this->tipeaktivatetap->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->tipe_aktiva_tetap;

            return $output;
        });
    }

    public function listAkun()
    {
        return $this->akun->whereDoesntHave('childAkun')->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        });
    }

    public function listMetodePenyusutan()
    {
        return $this->metode_penyusutan->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        });
    }

    public function listAkunAktivaTetap()
    {
        return $this->akun->where('parent_id', '!=', null)->whereHas('tipeAkun', function ($query) {
            $query->where('title', 'Aktiva Tetap');
        })->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        });
    }

    public function listAkunPenyusutan()
    {
        return $this->akun->where('parent_id', '!=', null)->whereHas('tipeAkun', function ($query) {
            $query->where('title', 'Akumulasi Penyusutan');
        })->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        });
    }

    public function dateChecker($data)
    {
        $data['tgl_beli'] = (empty($data['tgl_beli'])) ? $data['tgl_beli'] = date('Y-m-d H:i:s') : $data['tgl_beli'] = $this->date->IndonesiaToSql($data['tgl_beli']);
        $data['tgl_pakai'] = (empty($data['tgl_pakai'])) ? $data['tgl_pakai'] = date('Y-m-d H:i:s') : $data['tgl_pakai'] = $this->date->IndonesiaToSql($data['tgl_pakai']);
        $data['tgl_jurnal'] = (empty($data['tgl_jurnal'])) ? $data['tgl_jurnal'] = date('Y-m-d H:i:s') : $data['tgl_jurnal'] = $this->date->IndonesiaToSql($data['tgl_jurnal']);

        return $data;
    }

    public function InsertPengeluaranAktiva($data, $model)
    {
        if (!empty($data['akun_pengeluaran_aktiva_id'])) {
            $count = count($data['akun_pengeluaran_aktiva_id']);
            $items = [];
            $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
            $arrayPengeluaran = [];
            for ($i = 0; $i < $count; ++$i) {
                $items[] = [
                    'akun_pengeluaran_aktiva_id' => $data['akun_pengeluaran_aktiva_id'][$i],
                    'daftar_aktiva_tetap_id' => $model->id,
                    'tanggal' => Carbon::parse($data['tanggal_pengeluaran'][$i])->format('Y-m-d H:i:s'),
                    'keterangan' => $data['keterangan_pengeluaran'][$i],
                    'jumlah' => $data['jumlah_pengeluaran'][$i],
                    'created_at' => $jam_sekarang,
                    'updated_at' => $jam_sekarang,
                ];
            }
            return PengeluaranAktiva::insert($items);
        }
    }

    public function InsertDaftarAktivaTetapDetail($data, $model)
    {
        $create_month = Carbon::parse($data['tgl_pakai']);
        $create_month_now = Carbon::now();
        $loop_bulan = $create_month_now->diffInMonths($create_month, false);
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        if ($loop_bulan < 0) {
            $item = [];
            for ($i=0; $i <= abs($loop_bulan) ; $i++) {
                $dia =  $this->model->with(['pengeluaranAktiva' => function($query)use($model){
                            $query->where('daftar_aktiva_tetap_id', '=', $model->id);
                        }, 'daftarAktivaTetapDetail'])
                        ->where('id', $model->id)->get();
                // kondisi disini ya jangan lupa nanti diketik
                if ($data['metode_penyusutan_id'] == 1) { // Metode Garis Lurus
                    $nilai_penyusutan = (((($data['nilai_buku']) * ($data['rasio'] / 100)) / 12));
                } elseif ($data['metode_penyusutan_id'] == 2) { // Metode Double Declining
                    $nilai_penyusutan = (((($data['nilai_buku']) * ($data['rasio'] / 100)) / 12));
                } elseif ($data['metode_penyusutan_id'] == 3) { // Metode Tidak terdepresiasi
                    $nilai_penyusutan = 0;
                } elseif ($data['metode_penyusutan_id'] == 4) { // Metode Sum Of Year Digit
                    $tahun_to_bulan = $model['tahun'] * 12;
                    $sumI = 0;
                    for ($i = 1; $i <= $tahun_to_bulan; ++$i) {
                        $sumI += $i;
                    }
                    $harga_perolehan = $data['harga_perolehan'] - $data['sisa'];
                    $bagi_tahun = $model['tahun'] / $sumI;
                    $bulan = date_create($data['tgl_pakai']);
                    $akumulasi_bulan = $bulan->format('m') / 12;
                    $penyusutan = $harga_perolehan * $bagi_tahun;
                    $nilai_penyusutan = (($akumulasi_bulan * $penyusutan));
                }
                if($dia->last()['daftarAktivaTetapDetail']->isEmpty()){
                    // proses pembuatan aktiva
                    $arrayPengeluaran = [];
                    for ($j=0; $j < count($dia->last()['pengeluaranAktiva']) ; $j++) { 
                        $item[] = [
                            'daftar_aktiva_tetap_id' => $model->id,
                            'nama_beban' => null,
                            'nama_akumulasi' => $dia->last()['pengeluaranAktiva'][$j]['keterangan'],
                            'periode' => 0,
                            'tgl_jurnal' => $this->formatTanggal($data['tgl_pakai']),
                            'keterangan' => '',
                            'harga_perolehan' => $dia->last()['pengeluaranAktiva'][$j]['jumlah'],
                            'aktiva_dihentikan' => 0,
                            'akumulasi_penyusutan' => 0,
                            'nilai_buku_asuransi' => $data['harga_perolehan'],
                            'nilai_sisa' => $data['harga_perolehan'],
                            'nilai_penyusutan' => 0,
                            'created_at' => $jam_sekarang,
                            'updated_at' => $jam_sekarang,
                        ];

                        $arrayPengeluaran[] = [
                            'nominal' => $item[$j]['harga_perolehan'],
                            'tanggal' => $item[$j]['tgl_jurnal'],
                            'produk' => null,
                            'akun' => $dia->last()['pengeluaranAktiva'][$j]['akun_pengeluaran_aktiva_id'],
                            'dari' => $dia->last()['pengeluaranAktiva'][$j]['keterangan'],
                        ];
                        if($i == 0){
                            $this->transaksi->create([
                                'nominal' => $arrayPengeluaran[$i]['nominal'],
                                'tanggal' => $arrayPengeluaran[$i]['tanggal'],
                                'akun_id' => $arrayPengeluaran[$i]['akun'],
                                'produk_id' => $arrayPengeluaran[$i]['produk'],
                                'item_id' => $model->id,
                                'item_type' => get_class($this->model),
                                'status' => 0,
                                'no_transaksi' => $model->kode_aktiva,
                                'sumber' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->model)))->getShortName(), ' '),
                                'keterangan' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->model)))->getShortName(), ' ').' Activa '.$model->kode_aktiva.' Periode 0',
                                'dari' => $arrayPengeluaran[$i]['dari'],
                                'status_rekonsiliasi' => null,
                                'desc_dev' => '',
                                'created_at' => $jam_sekarang,
                                'deleted_at' => $jam_sekarang,
                            ]);
                        }
                    }
                    $itemAktiva = [
                        'daftar_aktiva_tetap_id' => $model->id,
                        'nama_beban' => $dia->last()['akunAktiva']['nama_akun'],
                        'nama_akumulasi' => null,
                        'periode' => 0,
                        'tgl_jurnal' => $this->formatTanggal($data['tgl_pakai']),
                        'harga_perolehan' => $data['harga_perolehan'],
                        'keterangan' => '',
                        'aktiva_dihentikan' => 0,
                        'akumulasi_penyusutan' => 0,
                        'nilai_buku_asuransi' => $data['harga_perolehan'],
                        'nilai_sisa' => $data['harga_perolehan'],
                        'nilai_penyusutan' => 0,
                        'created_at' => $jam_sekarang,
                        'updated_at' => $jam_sekarang,
                    ];
                    $arrayAkunAktiva = [
                        'nominal' => $itemAktiva['harga_perolehan'],
                        'tanggal' => $itemAktiva['tgl_jurnal'],
                        'produk' => null,
                        'akun' => $model['akun_aktiva_id'],
                        'dari' => $model['akunAktiva']['nama_akun'],
                    ];
                    if ($i == 0) {
                        $this->transaksi->create([
                            'nominal' => $arrayAkunAktiva['nominal'],
                            'tanggal' => $arrayAkunAktiva['tanggal'],
                            'akun_id' => $arrayAkunAktiva['akun'],
                            'produk_id' => $arrayAkunAktiva['produk'],
                            'item_id' => $model->id,
                            'item_type' => get_class($this->model),
                            'status' => 1,
                            'no_transaksi' => $model->kode_aktiva,
                            'sumber' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->model)))->getShortName(), ' '),
                            'keterangan' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->model)))->getShortName(), ' ').' '.$model->kode_aktiva.' Periode 0',
                            'dari' => $arrayAkunAktiva['dari'],
                            'status_rekonsiliasi' => null,
                            'desc_dev' => '',
                            'created_at' => $jam_sekarang,
                            'deleted_at' => $jam_sekarang,
                        ]);
                    }
                    $this->daftarAktivaTetapDetail->insert($itemAktiva);
                    $this->daftarAktivaTetapDetail->insert($item);
                    $this->saveTransaksiAkun($data, $arrayPengeluaran, $arrayAkunAktiva);
                }else{
                    // proses penyusutan aktiva
                    $jurnal              =  strtotime($dia->last()->tgl_pakai);
                    $periode_baru        =  $dia->last()['daftarAktivaTetapDetail']->last()['periode'] + 1;
                    // if($dia->last()['daftarAktivaTetapDetail']->last()['periode'] == 0){
                        // $date   =   $this->formatTanggal($dia->last()->tgl_pakai);
                    // }else{
                        $date   =   date('Y-m-d', strtotime("+".$i." month + 23 hours", $jurnal));
                    // }
                    $tgl_jurnal_sekarang =   $this->formatTanggal($date);

                    $arrayAkumulasi = [
                        'nominal' => $nilai_penyusutan,
                        'tanggal' => $tgl_jurnal_sekarang,
                        'produk' => null,
                        'akun' => $model['akun_akum_penyusutan_id'],
                        'dari' => $model['akunAkumPenyusutan']['nama_akun'],
                    ];
                    $arrayBeban = [
                        'nominal' => $nilai_penyusutan,
                        'tanggal' => $tgl_jurnal_sekarang,
                        'produk' => null,
                        'akun' => $model['akun_beban_penyusutan_id'],
                        'dari' => $model['akunBebanPenyusutan']['nama_akun'],
                    ];
                    $dataX = [
                        'periode' => $dia->last()['daftarAktivaTetapDetail']->last()['periode'] + 1,
                        'nilai_penyusutan' => $nilai_penyusutan,
                    ];
                    $this->daftarAktivaTetapDetail->create([
                        'daftar_aktiva_tetap_id' => $model->id,
                        'nama_beban' => $model['akunBebanPenyusutan']['nama_akun'],
                        'nama_akumulasi' => null,
                        'periode' => $dia->last()['daftarAktivaTetapDetail']->last()['periode'] + 1,
                        'tgl_jurnal' => $tgl_jurnal_sekarang,
                        'harga_perolehan' => $dia->last()['daftarAktivaTetapDetail']->last()['harga_perolehan'],
                        'keterangan' => 'Penyusutan Aktiva '.$dia->last()['kode_aktiva'].' Periode '.$periode_baru,
                        'aktiva_dihentikan' => 0,
                        'akumulasi_penyusutan' => $dia->last()['daftarAktivaTetapDetail']->last()['akumulasi_penyusutan'] + $nilai_penyusutan,
                        'nilai_buku_asuransi' => $dia->last()['daftarAktivaTetapDetail']->last()['nilai_buku_asuransi'] - $nilai_penyusutan,
                        'nilai_sisa' => $dia->last()['daftarAktivaTetapDetail']->last()['nilai_sisa'],
                        'nilai_penyusutan' => $nilai_penyusutan,
                        'created_at' => $jam_sekarang,
                        'updated_at' => $jam_sekarang,
                    ]);

                    $this->daftarAktivaTetapDetail->create([
                        'daftar_aktiva_tetap_id' => $model->id,
                        'nama_beban' => null,
                        'nama_akumulasi' => $model['akunAkumPenyusutan']['nama_akun'],
                        'periode' => $dia->last()['daftarAktivaTetapDetail']->last()['periode'] + 1,
                        'tgl_jurnal' => $tgl_jurnal_sekarang,
                        'harga_perolehan' => $dia->last()['daftarAktivaTetapDetail']->last()['harga_perolehan'],
                        'keterangan' => 'Penyusutan Aktiva '.$dia->last()['kode_aktiva'].' Periode '.$periode_baru,
                        'aktiva_dihentikan' => 0,
                        'akumulasi_penyusutan' => $dia->last()['daftarAktivaTetapDetail']->last()['akumulasi_penyusutan'] + $nilai_penyusutan,
                        'nilai_buku_asuransi' => $dia->last()['daftarAktivaTetapDetail']->last()['nilai_buku_asuransi'] - $nilai_penyusutan,
                        'nilai_sisa' => $dia->last()['daftarAktivaTetapDetail']->last()['nilai_sisa'],
                        'nilai_penyusutan' => $nilai_penyusutan,
                        'created_at' => $jam_sekarang,
                        'updated_at' => $jam_sekarang,
                    ]);
                    $this->saveTransaksiAkun($data, $arrayAkumulasi, $arrayBeban, $dataX);
                }
            }
        } else {
            // pembuatan aktiva ketika penyusutan terjadi bulan depan dari sekarang
            for ($k=0; $k < count($data['akun_pengeluaran_aktiva_id']) ; $k++) { 
                $item[] = [
                    'daftar_aktiva_tetap_id' => $model->id,
                    'nama_beban' => null,
                    'nama_akumulasi' => $model['pengeluaranAktiva'][$k]['keterangan'],
                    'periode' => 0,
                    'tgl_jurnal' => $jam_sekarang,
                    'keterangan' => '',
                    'harga_perolehan' => $model['pengeluaranAktiva'][$k]['jumlah'],
                    'aktiva_dihentikan' => 0,
                    'akumulasi_penyusutan' => 0,
                    'nilai_buku_asuransi' => 0,
                    'nilai_sisa' => 0,
                    'nilai_penyusutan' => 0,
                    'created_at' => $jam_sekarang,
                    'updated_at' => $jam_sekarang,
                ];
                $arrayPengeluaran[] = [
                    'nominal' => $item[$k]['harga_perolehan'],
                    'tanggal' => $jam_sekarang,
                    'produk' => null,
                    'akun' => $model['pengeluaranAktiva'][$k]['akun_pengeluaran_aktiva_id'],
                    'dari' => $model['pengeluaranAktiva'][$k]['keterangan'],
                ];
                $this->transaksi->create([
                    'nominal' => $arrayPengeluaran[$k]['nominal'],
                    'tanggal' => $arrayPengeluaran[$k]['tanggal'],
                    'akun_id' => $arrayPengeluaran[$k]['akun'],
                    'produk_id' => $arrayPengeluaran[$k]['produk'],
                    'item_id' => $model->id,
                    'item_type' => get_class($this->model),
                    'status' => 0,
                    'no_transaksi' => $model->kode_aktiva,
                    'sumber' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->model)))->getShortName(), ' '),
                    'keterangan' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->model)))->getShortName(), ' ').' Activa '.$model->kode_aktiva.' Periode 0',
                    'dari' => $arrayPengeluaran[$k]['dari'],
                    'status_rekonsiliasi' => null,
                    'desc_dev' => '',
                    'created_at' => $jam_sekarang,
                ]);
            }
            $item[] = [
                'daftar_aktiva_tetap_id' => $model->id,
                'nama_beban' => $model['akunAktiva']['nama_akun'],
                'nama_akumulasi' => null,
                'periode' => 0,
                'tgl_jurnal' => $jam_sekarang,
                'harga_perolehan' => $data['harga_perolehan'],
                'keterangan' => '',
                'aktiva_dihentikan' => 0,
                'akumulasi_penyusutan' => 0,
                'nilai_buku_asuransi' => $data['harga_perolehan'],
                'nilai_sisa' => $data['harga_perolehan'],
                'nilai_penyusutan' => 0,
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];
            $arrayAkunAktiva = [
                'nominal' => $data['harga_perolehan'],
                'tanggal' => $jam_sekarang,
                'produk' => null,
                'akun' => $model['akun_aktiva_id'],
                'dari' => $model['akunAktiva']['nama_akun'],
            ];
            $this->transaksi->create([
                'nominal' => $arrayAkunAktiva['nominal'],
                'tanggal' => $arrayAkunAktiva['tanggal'],
                'akun_id' => $arrayAkunAktiva['akun'],
                'produk_id' => $arrayAkunAktiva['produk'],
                'item_id' => $model->id,
                'item_type' => get_class($this->model),
                'status' => 1,
                'no_transaksi' => $model->kode_aktiva,
                'sumber' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->model)))->getShortName(), ' '),
                'keterangan' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->model)))->getShortName(), ' ').' '.$model->kode_aktiva.' Periode 0',
                'dari' => $arrayAkunAktiva['dari'],
                'status_rekonsiliasi' => null,
                'desc_dev' => '',
                'created_at' => $jam_sekarang,
            ]);
            $this->daftarAktivaTetapDetail->insert($item);
            $this->saveTransaksiAkun($data, $arrayPengeluaran, $arrayAkunAktiva);
        }
        return true;
    }

    public function convertTanggal($edit_tanggal)
    {
        if (!empty($edit_tanggal->tgl_pakai)) {
            $edit_tanggal->tgl_pakai = Carbon::parse($edit_tanggal->tgl_pakai)->format('d F Y');
        }
        if (!empty($edit_tanggal->tgl_beli)) {
            $edit_tanggal->tgl_beli = Carbon::parse($edit_tanggal->tgl_beli)->format('d F Y');
        }
        if (!empty($edit_tanggal->tgl_jurnal)) {
            $edit_tanggal->tgl_jurnal = Carbon::parse($edit_tanggal->tgl_jurnal)->format('d F Y');
        }

        return $edit_tanggal;
    }

    public function iPengeluaranAktiva($getData)
    {
        foreach ($getData as $key => $data) {
            $aktivaDetail = $data->daftarAktivaTetapDetail->last();
            $dateDiff = date_diff(date_create($data['tgl_pakai']), $aktivaDetail['created_at']);
            if ($aktivaDetail->periode < $data->getSumMonthAttribute()
                && ($aktivaDetail->harga_perolehan - $aktivaDetail->nilai_sisa) != $aktivaDetail->akumulasi_penyusutan + $aktivaDetail->nilai_penyusutan
                && ($aktivaDetail->harga_perolehan - $aktivaDetail->nilai_sisa) >= $aktivaDetail->akumulasi_penyusutan + $aktivaDetail->nilai_penyusutan) {
                if ($dateDiff->m > 0) {
                    $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
                    if ($data['metode_penyusutan_id'] == 1) { // Metode Garis Lurus
                        $nilai_penyusutan = (($aktivaDetail->harga_perolehan * ($data['rasio'] / 100)) / 12);
                    } elseif ($data['metode_penyusutan_id'] == 2) { // Metode Double Declining
                        $nilai_penyusutan = (($aktivaDetail->nilai_buku_asuransi * ($data['rasio'] / 100) / 12));
                    } elseif ($data['metode_penyusutan_id'] == 3) { // Metode Tidak terdepresiasi
                        $nilai_penyusutan = 0;
                    } elseif ($data['metode_penyusutan_id'] == 4) { // Metode Sum Of Year Digit
                            $tahun_to_bulan = $data['tahun'] * 12;
                        $sumI = 0;
                        for ($i = 1; $i <= $tahun_to_bulan; ++$i) {
                            $sumI += $i;
                        }
                        $harga_perolehan = $aktivaDetail['harga_perolehan'] - $aktivaDetail['nilai_sisa'];
                        $tahun = $data['tahun'] * 12;
                        $periode_tahun_ini = $tahun - $aktivaDetail['periode'];
                        $bagi_tahun_ini = $periode_tahun_ini / $sumI;
                        $nilai_penyusutan = $harga_perolehan * $bagi_tahun_ini;
                    }
                    $item = [
                            'daftar_aktiva_tetap_id' => $data->id,
                            'periode' => $aktivaDetail->periode + 1,
                            'harga_perolehan' => $aktivaDetail->harga_perolehan,
                            'aktiva_dihentikan' => $aktivaDetail->aktiva_dihentikan,
                            'akumulasi_penyusutan' => $aktivaDetail->akumulasi_penyusutan + $nilai_penyusutan,
                            'nilai_buku_asuransi' => $aktivaDetail->nilai_buku_asuransi - $nilai_penyusutan,
                            'nilai_sisa' => $aktivaDetail->nilai_sisa,
                            'nilai_penyusutan' => $nilai_penyusutan,
                            'created_at' => $jam_sekarang,
                        ];
                    $dt = strtotime($data['tgl_pakai']);
                    $arrayAkumulasi = [
                            'nominal' => $item['nilai_penyusutan'],
                            'tanggal' => date('Y-m-d', strtotime('+'.$item['periode'].' month', $dt)),
                            'produk' => null,
                            'akun' => $data['akun_akum_penyusutan_id'],
                            'dari' => $data->akunAkumPenyusutan->nama_akun,
                        ];
                    $arrayBeban = [
                            'nominal' => $item['nilai_penyusutan'],
                            'tanggal' => date('Y-m-d', strtotime('+'.$item['periode'].' month', $dt)),
                            'produk' => null,
                            'akun' => $data['akun_beban_penyusutan_id'],
                            'dari' => $data->akunBebanPenyusutan->nama_akun,
                        ];
                    $dataX = $this->daftarAktivaTetapDetail->create([
                            'daftar_aktiva_tetap_id' => $data->id,
                            'periode' => $aktivaDetail->periode + 1,
                            'harga_perolehan' => $aktivaDetail->harga_perolehan,
                            'aktiva_dihentikan' => $aktivaDetail->aktiva_dihentikan,
                            'akumulasi_penyusutan' => $aktivaDetail->akumulasi_penyusutan + $nilai_penyusutan,
                            'nilai_buku_asuransi' => $aktivaDetail->nilai_buku_asuransi - $nilai_penyusutan,
                            'nilai_sisa' => $aktivaDetail->nilai_sisa,
                            'nilai_penyusutan' => $nilai_penyusutan,
                            'created_at' => $jam_sekarang,
                        ]);
                    $this->saveTransaksiAkun($data, $arrayAkumulasi, $arrayBeban, $dataX);
                    dump('Transaksi Aktiva '.$data->kode_aktiva.' Periode '.$dataX->periode.' ['.
                            carbon::parse($arrayAkumulasi['tanggal'])->format('F Y').'] Berhasil diperbarui '
                        );
                    dump('oke');
                } else {
                    dump('Semua Aktiva Sudah Tersusut');
                    dump('Sip ?');
                }
            }
        }
    }

    public function saveTransaksiAkun($data, $akunPengeluaran, $akunAktiva, $dataX = null)
    {
        $tanggal_sekarang = date('Y-m-d H:i:s');
        $dataX !== null ? $periode = $dataX['periode'] : $periode = 0;
        $data_akun = [
            'keterangan' => $data['kode_aktiva'],
            'akun_aktiva' => $dataX !== null ? $data['akun_beban_penyusutan_id'] : $data['akun_aktiva_id'],
            'tanggal_aktiva' => $dataX !== null ? $akunPengeluaran['tanggal'] : $data['tgl_beli'],
            'nominal_aktiva' => $dataX !== null ? $dataX['nilai_penyusutan'] : $data['harga_perolehan'],
            'akun_pengeluaran' => $data['akun_pengeluaran_aktiva_id'] ?? $data['akun_akum_penyusutan_id'],
            'tanggal_pengeluaran' => $dataX !== null ? $akunPengeluaran['tanggal'] : $data['tgl_pakai'],
            'nominal_pengeluaran' => $dataX !== null ? $dataX['nilai_penyusutan'] : $data['jumlah_pengeluaran'],
        ];
        if ($periode != 0) {
            $modelJurnalUmum = $this->insertJurnalUmum($data_akun, $periode);
            $detailJurnalUmum = $this->insertDetailJurnalUmum($modelJurnalUmum, $data_akun, $tanggal_sekarang);
            $transaksiAkun = $this->insertTransaction($modelJurnalUmum, $data_akun, $akunPengeluaran, $akunAktiva, $periode);
        }
        // if ($data_akun) {
        //     $sum_modal = 0;
        //     if (is_array($data_akun['akun_pengeluaran'])) {
        //         for ($i = 0; $i < count($data_akun['akun_pengeluaran']); ++$i) {
        //             $transaksi = new Transaksi();
        //             $convert_modal = $data_akun['nominal_pengeluaran'][$i];
        //             $sum_modal += $convert_modal;
        //             $akun = new Akun();
        //             $transaksi->akun_id = $data_akun['akun_pengeluaran'][$i];
        //             $akun->where('id', $data_akun['akun_pengeluaran'][$i])->update(['money_function' => $convert_modal]);
        //             $transaksi->tanggal = carbon::parse($data_akun['tanggal_pengeluaran'])->format('Y-m-d H:i:s');
        //             $transaksi->nominal = $convert_modal;
        //             $transaksi->save();
        //             ++$i;
        //         }
        //     } else {
        //         $transaksi = new Transaksi();
        //         $convert_modal = $data_akun['nominal_pengeluaran'];
        //         $sum_modal += $convert_modal;
        //         $akun = new Akun();
        //         $transaksi->akun_id = $data_akun['akun_pengeluaran'];
        //         $akun->where('id', $data_akun['akun_pengeluaran'])->update(['money_function' => $convert_modal]);
        //         $transaksi->tanggal = carbon::parse($data_akun['tanggal_pengeluaran'])->format('Y-m-d H:i:s');
        //         $transaksi->nominal = $convert_modal;
        //         $transaksi->save();
        //     }
        //     $transaksi = new Transaksi;
        //     $convert_modal = $data_akun['nominal_aktiva'];
        //     $sum_modal += $convert_modal;
        //     $akun = new Akun;
        //     $transaksi->akun_id = $data_akun['akun_aktiva'];
        //     $akun->where('id',$data_akun['akun_aktiva'])->update(['money_function' => $convert_modal]);
        //     $transaksi->tanggal = carbon::parse($data_akun['tanggal_aktiva'])->format('Y-m-d H:i:s');
        //     $transaksi->nominal = $convert_modal;
        //     $transaksi->save();
        // }
    }

    public function insertJurnalUmum($data, $periode)
    {
        return $this->jurnal_umum->create([
            'no_faktur' => $data['keterangan'],
            'tanggal' => $this->formatTanggal($data['tanggal_aktiva']),
            'description' => 'Aktiva '.$data['keterangan'].' Periode '.$periode,
            'status' => 2,
        ]);
    }

    public function insertDetailJurnalUmum($modelJurnalUmum, $data, $tanggal_sekarang)
    {
        $detail = [];
        $detail[] = [
            'akun_id' => $data['akun_aktiva'],
            'jurnal_umum_id' => $modelJurnalUmum->id,
            'debit' => $data['nominal_aktiva'],
            'kredit' => 0,
            'created_at' => $tanggal_sekarang,
            'updated_at' => $tanggal_sekarang,
        ];
        if (is_array($data['akun_pengeluaran'])) {
            for ($i = 0; $i < count($data['akun_pengeluaran']); ++$i) {
                $detail[] = [
                    'akun_id' => $data['akun_pengeluaran'][$i] ?? $data['akun_pengeluaran'],
                    'jurnal_umum_id' => $modelJurnalUmum->id,
                    'debit' => 0,
                    'kredit' => $data['nominal_pengeluaran'][$i] ?? $data['nominal_pengeluaran'],
                    'created_at' => $tanggal_sekarang,
                    'updated_at' => $tanggal_sekarang,
                ];
            }
        } else {
            $detail = [
                'akun_id' => $data['akun_pengeluaran'],
                'jurnal_umum_id' => $modelJurnalUmum->id,
                'debit' => 0,
                'kredit' => $data['nominal_pengeluaran'],
                'created_at' => $tanggal_sekarang,
                'updated_at' => $tanggal_sekarang,
            ];
        }
        $this->detail_jurnal_umum->insert($detail);
    }

    public function insertTransaction($modelJurnalUmum, $data, $akunPengeluaran, $akunAktiva, $periode)
    {
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        $debit = $akunAktiva;
        $kredit = $akunPengeluaran;
        if (array_key_exists(0, $kredit) == true) {
            for ($i = 0; $i < count($kredit); ++$i) {
                $this->transaksi->create([
                    'nominal' => $kredit[$i]['nominal'],
                    'tanggal' => $kredit['tanggal'],
                    'akun_id' => $kredit[$i]['akun'],
                    'produk_id' => $kredit[$i]['produk'],
                    'item_id' => $modelJurnalUmum->id,
                    'item_type' => get_class($this->jurnal_umum),
                    'status' => 0,
                    'no_transaksi' => $modelJurnalUmum->no_faktur,
                    'sumber' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->jurnal_umum)))->getShortName(), ' '),
                    'keterangan' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->jurnal_umum)))->getShortName(), ' ').' Activa '.$modelJurnalUmum->no_faktur.' Periode '.$periode,
                    'dari' => $kredit[$i]['dari'],
                    'status_rekonsiliasi' => null,
                    'desc_dev' => '',
                    'created_at' => $jam_sekarang,
                ]);
            }
        } else {
            $this->transaksi->create([
                'nominal' => $kredit['nominal'],
                'tanggal' => $kredit['tanggal'],
                'akun_id' => $kredit['akun'],
                'produk_id' => $kredit['produk'],
                'item_id' => $modelJurnalUmum->id,
                'item_type' => get_class($this->jurnal_umum),
                'status' => 0,
                'no_transaksi' => $modelJurnalUmum->no_faktur,
                'sumber' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->jurnal_umum)))->getShortName(), ' '),
                'keterangan' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->jurnal_umum)))->getShortName(), ' ').' Activa '.$modelJurnalUmum->no_faktur.' Periode '.$periode,
                'dari' => $kredit['dari'],
                'status_rekonsiliasi' => null,
                'desc_dev' => '',
                'created_at' => $jam_sekarang,
            ]);
        }
        $this->transaksi->create([
            'nominal' => $debit['nominal'],
            'tanggal' => $debit['tanggal'],
            'akun_id' => $debit['akun'],
            'produk_id' => $debit['produk'],
            'item_id' => $modelJurnalUmum->id,
            'item_type' => get_class($this->jurnal_umum),
            'status' => 1,
            'no_transaksi' => $modelJurnalUmum->no_faktur,
            'sumber' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->jurnal_umum)))->getShortName(), ' '),
            'keterangan' => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->jurnal_umum)))->getShortName(), ' ').' '.$modelJurnalUmum->no_faktur.' Periode '.$periode,
            'dari' => $debit['dari'],
            'status_rekonsiliasi' => null,
            'desc_dev' => '',
            'created_at' => $jam_sekarang,
        ]);

        return true;
    }

    public function formatTanggal($tanggal)
    {
        return  Carbon::parse($tanggal)->format('Y-m-d H:i:s');
    }

    public function getKodeAktiva($data)
    {
        $checkData = $this->model->where('kode_aktiva', $data)->get();
        
        return $checkData;
    }

    public function formatingAllData($data)
    {
        $data['sisa'] = floatval(preg_replace('/[^\d.]/', '', $data['sisa']));

        if (!empty($data['akun_pengeluaran_aktiva_id'])) {
            for ($i = 0; $i < count($data['akun_pengeluaran_aktiva_id']); ++$i) {
                $data['jumlah_pengeluaran'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['jumlah_pengeluaran'][$i]));
            }
        }

        return $data;
    }
}
