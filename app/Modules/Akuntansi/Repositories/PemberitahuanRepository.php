<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Pemberitahuan;
use Generator\Interfaces\RepositoryInterface;


class PemberitahuanRepository implements RepositoryInterface
{
    protected $model;
    public function __construct(Pemberitahuan $model)
    {
        $this->model = $model;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list iklan
     */
    public function getItems()
    {
        return $this->model->orderBy('id', 'DESC')->paginate(5);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);
        $model->delete();
        return $model;
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $arrayParentData = $data;
        $model = $this->findItem($id)->fill($arrayParentData);
        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $model = $this->model->fill($data);
        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(20);
    }


}