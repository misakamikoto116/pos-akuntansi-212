<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\DetailInformasiPelanggan;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\TransaksiUangMuka;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;
use Carbon\Carbon;
use Helpers\IndonesiaDate as DateHelper;
use Generator\Interfaces\RepositoryInterface;


class UangMukaPelangganRepository implements RepositoryInterface
{
    public function __construct(FakturPenjualan $model, BarangFakturPenjualan $barang_faktur_penjualan, SyaratPembayaran $termin, DateHelper $date, KodePajak $kode_pajak, InformasiPelanggan $pelanggan, Produk $produk, CodeHelper $code, Akun $akun, TransaksiHelper $transaksi, PreferensiMataUang $preferensi, TransaksiUangMuka $uang_muka)
    {
        $this->model = $model;
        $this->termin = $termin;
        $this->date = $date;
        $this->kode_pajak = $kode_pajak;
        $this->pelanggan = $pelanggan;
        $this->produk = $produk;
        $this->code = $code;
        $this->barang_faktur_penjualan = $barang_faktur_penjualan;
        $this->akun = $akun;
        $this->transaksi = $transaksi;
        $this->preferensi = $preferensi;
        $this->uang_muka = $uang_muka;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->where('uang_muka',1)->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $uang_muka_pelanggan                                 = $this->model->findOrFail($id);
        $uang_muka_pelanggan->invoice_date                   = Carbon::parse($uang_muka_pelanggan->invoice_date)->format('d F Y');
        $uang_muka_pelanggan->barang->first()->unit_price    = number_format($uang_muka_pelanggan->barang->first()->unit_price);
        return $uang_muka_pelanggan;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $check = $this->model->findOrFail($id);
        $this->transaksi->ExecuteTransactionSingle('delete', $check); 
        // BarangFakturPenjualan::where('faktur_penjualan_id', $check->id)->delete();  
        
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data = $this->formattingAllData($data);
        $data['taxable'] = (empty($data['taxable'])) ? 0 : $data['taxable'];
        $data['in_tax'] = (empty($data['in_tax'])) ? 0 : $data['in_tax'];
        $uang_muka_pelanggan = $this->findItem($id);
        $detail_uang_muka = $this->barang_faktur_penjualan->where('faktur_penjualan_id',$uang_muka_pelanggan->id)->first();
        $model = $uang_muka_pelanggan->fill($this->dateChecker($data));
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        if($model->save()){
            
            $pajak1 = null;
            $pajak2 = null;
            if(!empty($data['kode_pajak_id'][0])){
                $pajak1 = $data['kode_pajak_id'][0];
                if(!empty($data['kode_pajak_id'][1])){
                    $pajak2 = $data['kode_pajak_id'][1];
                }
            }

            $detail_uang_muka->produk_id = $data['produk_id'];
            $detail_uang_muka->faktur_penjualan_id = $uang_muka_pelanggan->id;
            $detail_uang_muka->item_deskripsi = $data['item_deskripsi'];
            $detail_uang_muka->jumlah = $data['jumlah'];
            $detail_uang_muka->unit_price = $data['unit_price'];
            $detail_uang_muka->diskon = $data['diskon_produk'];
            $detail_uang_muka->kode_pajak_id = $pajak1;
            $detail_uang_muka->kode_pajak_2_id = $pajak2;
            $detail_uang_muka->update();
    
            $this->transaksi->ExecuteTransactionSingle('delete', $model); 

            $this->insertTransaction($data, $model);
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->formattingAllData($data);
        $data['taxable'] = (empty($data['taxable'])) ? 0 : $data['taxable'];
        $data['in_tax'] = (empty($data['in_tax'])) ? 0 : $data['in_tax'];
        $data['no_faktur'] = $this->code->autoGenerate($this->model, "no_faktur", $data['no_faktur']);
        $model = $this->model->fill($this->dateChecker($data));
        if($model->save()){
            $this->insertSubItem($data, $model);
            $this->insertTransaction($data, $model);
        }

        if(!empty($data['lanjutkan'])){
            $model['continue_stat'] = 2;                
        }    
        
        return $model;
    }

    public function insertSubItem($data, $model)
    {
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
            $pajak1 = null;
            $pajak2 = null;
            if(!empty($data['kode_pajak_id'][0])){
                $pajak1 = $data['kode_pajak_id'][0];
                if(!empty($data['kode_pajak_id'][1])){
                    $pajak2 = $data['kode_pajak_id'][1];
                }
            }
            $items = [
                'produk_id' => $data['produk_id'],
                'faktur_penjualan_id' => $model->id,
                'item_deskripsi' => $data['item_deskripsi'],
                'jumlah' => $data['jumlah'],
                'unit_price' => $data['unit_price'],
                'diskon' => $data['diskon_produk'],
                'kode_pajak_id' => $pajak1,
                'kode_pajak_2_id' => $pajak2,
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];
        BarangFakturPenjualan::insert($items);
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */

    public function dateChecker($data)
    {
        $data['invoice_date'] = (empty($data['invoice_date'])) ? $data['invoice_date'] = date('Y-m-d H:i:s') : $data['invoice_date'] = $this->date->IndonesiaToSql($data['invoice_date']); ;
        return $data;
    }

    public function filter($data)
    {
        return $this->model->where('uang_muka',1)->filter($data)->paginate(20);
    }

    public function listPelanggan()
    {
        return $this->pelanggan->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listPajak()
    {
        return $this->kode_pajak->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->kode_pajak_formatted;

            return $output;
        }, []);
    }

    public function listAkun()
    {
        return $this->akun->whereDoesntHave('childAkun')->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function getAkunDp()
    {
        if ($this->preferensi !== null) {
            return $this->preferensi->first();
        }
    }

    public function getDPFromProduk()
    {
        return $this->produk->where('tipe_barang', 3)->first();
    }

    public function insertTransaction($data,$model)
    {
        $data['invoice_date'] = (empty($data['invoice_date'])) ? $data['invoice_date'] = date('Y-m-d H:i:s') : $data['invoice_date'] = $this->date->IndonesiaToSql($data['invoice_date']);
        // dd($data);
        $allArrayKredit = [];
        $total_pajak1 = 0;
        $total_pajak2 = 0;
        // $diskon_unit_price = $data['unit_price'] * ($data['diskon_produk'] / 100);
        // $jumlah_after_diskon_barang = $data['unit_price'] - $diskon_unit_price;
        // $jumlah_after_diskon_faktur = $jumlah_after_diskon_barang * ($data['diskon'] / 100);
        // $total_setelah_diskon = $jumlah_after_diskon_barang - $jumlah_after_diskon_faktur; 

        if(!empty($data['kode_pajak_id'][0])){
            $data_pajak1  = KodePajak::find($data['kode_pajak_id'][0]); // res : collection
            $total_pajak1 = $data['unit_price'] * $data_pajak1->nilai / 100; // res : float
                $arrayPajak1 =[
                    'nominal' => $total_pajak1,
                    'tanggal' => $data['invoice_date'],
                    'dari'  => 'Pajak DP',
                    'akun'  => $data_pajak1->akun_pajak_penjualan_id,
                    'id'    => $data['no_faktur']
                ];
                $allArrayKredit[] = $arrayPajak1;
            
                if(!empty($data['kode_pajak_id'][1])){
                $data_pajak2  = KodePajak::find($data['kode_pajak_id'][1]); // res : collection
                $total_pajak2 = $data['unit_price'] * $data_pajak2->nilai / 100; // res : float
                    $arrayPajak2 =[
                        'nominal' => $total_pajak2,
                        'tanggal' => $data['invoice_date'],
                        'dari'  => 'Pajak DP',
                        'akun'  => $data_pajak2->akun_pajak_penjualan_id,
                        'id'    => $data['no_faktur']
                    ];
                    $allArrayKredit[] = $arrayPajak2;        
            }
        }
        $arrayTotalWithPajak[] = [
            'nominal' => $data['unit_price'] + $total_pajak1 + $total_pajak2,
            'tanggal' => $data['invoice_date'],
            'dari'  => 'Total DP dengan Pajak',
            'akun'  => $data['akun_piutang_id'],
            'id'    => $data['no_faktur']
        ];
        $arrayTotalTok = [
            'nominal' => $data['unit_price'],
            'tanggal' => $data['invoice_date'],
            'dari'  => 'Total DP',
            'akun'  => $data['akun_dp_id'],
            'id'    => $data['no_faktur']
        ];
        $allArrayKredit[] = $arrayTotalTok;
        $class = $this->uang_muka;
        $this->ExecuteTransactionMultiple('insert', $model, $class, $allArrayKredit); 
        $this->ExecuteTransactionMultiple('insert', $model, $class, $arrayTotalWithPajak, 1); 
    }

    public function ExecuteTransactionMultiple($type, $model, $class, $data = null, $dk = 0)
    {
        $new_data = [];
        $nama_kelas = (new \ReflectionClass(get_class($class)))->getShortName();   

        foreach($data as $item){
            if($item['nominal'] != 0){
                $new_data[] = [
                    'nominal'             => $item['nominal'],
                    'tanggal'             => $item['tanggal'] !== null ? $item['tanggal'] : date('Y-m-d H:i:s'),
                    'akun_id'             => $item['akun'],
                    'produk_id'           => (!empty($item['produk'])) ? $item['produk'] : null ,
                    'item_id'             => $model->id,
                    'item_type'           => get_class($class),
                    'status'              => $dk,
                    'no_transaksi'        => $item['id'],                
                    'sumber'              => $this->transaksi->fromCamelCase($nama_kelas,' '),
                    'keterangan'          => $this->transaksi->fromCamelCase($nama_kelas,' ')." ".$item['id'],
                    'dari'                => (!empty($item['dari'])) ? $item['dari'] : null,
                    'status_rekonsiliasi' => null,
                    'created_at'          => date('Y-m-d H:i:s')  
                ];
            }
        }

        if($type == 'insert'){
            $transaksi = Transaksi::insert($new_data);   
        }else{
            $transaksi = Transaksi::where(['item_id' => $model->id, 'item_type' => get_class($model)])->delete();
        }
        return $transaksi;
    }

    public function formattingAllData($data)
    {   
        // data produk
        if (!empty($data['produk_id'])) {
            $data['unit_price'] = floatval(preg_replace('/[^\d.]/', '', $data['unit_price']));
        }
        return $data;
    }
}