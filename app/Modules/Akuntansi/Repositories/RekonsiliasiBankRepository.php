<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\DetailHistoryRekonsiliasi;
use App\Modules\Akuntansi\Models\HistoryRekonsiliasi;
use App\Modules\Akuntansi\Models\Transaksi;
use Carbon\Carbon;
use Generator\Interfaces\RepositoryInterface;
use Helpers\IndonesiaDate as DateHelper;

class RekonsiliasiBankRepository implements RepositoryInterface
{
    public function __construct(HistoryRekonsiliasi $model, Akun $akun, DateHelper $date)
    {
        $this->model    = $model;
        $this->akun     = $akun;
        $this->date     = $date;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $model = $this->model->findOrFail($id);
        $this->formattingField($model);

        return $model;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        if (empty($data['tgl_lahir'])) {
            $data['tgl_lahir'] = date('Y-m-d');
        } else {
            $data['tgl_lahir'] = $this->date->IndonesiaToSql($data['tgl_lahir']);
        }
        $model = $this->findItem($id)->fill($data);

        return $model->save();
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->formattingAllData($data);
        $model = $this->model->fill($data);
        if ($model->save()) {
            $this->saveDetailHistory($data, $model);
        }
        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }

    public function formattingField($model)
    {
        if (!empty($model->tgl_lahir)) {
            $model->tgl_lahir = Carbon::parse($model->tgl_lahir)->format('d F Y');
        }

        return true;
    }

    public function listAkun()
    {
        return $this->akun->whereDoesntHave('childAkun')->whereHas('tipeAkun', function ($query)
        {
            $query->where('title','Kas/Bank');
        })->where('nama_akun','!=','Kas & Bank')->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function formattingAllData($data)
    {
        $data['tanggal_terakhir_rekonsil'] = (empty($data['tanggal_terakhir_rekonsil'])) ? $data['tanggal_terakhir_rekonsil'] = date('Y-m-d H:i:s') : $data['tanggal_terakhir_rekonsil'] = $this->date->IndonesiaToSql($data['tanggal_terakhir_rekonsil']);

        $data['saldo_rekening_koran']                   = floatval(preg_replace('/[^\d.]/', '', $data['saldo_rekening_koran']));
        $data['kalkulasi_saldo']                        = floatval(preg_replace('/[^\d.]/', '', $data['kalkulasi_saldo']));
        $data['selisih_saldo']                          = floatval(preg_replace('/[^\d.]/', '', $data['selisih_saldo']));
        return $data;
    }

    public function saveDetailHistory($data, $model)
    {
        if (!empty($data['transaksi_id'])) {
            $arrayDetailHistory = [];
            foreach ($data['transaksi_id'] as $key => $transaksi_id) {
                $arrayDetailHistory[] = [
                    'history_rekonsiliasi_id'           => $model->id,
                    'transaksi_id'                      => $key,
                    'detail_tanggal_terakhir_rekonsil'  => $model->tanggal_terakhir_rekonsil
                ];
            }
            DetailHistoryRekonsiliasi::insert($arrayDetailHistory);
        }
    }
}
