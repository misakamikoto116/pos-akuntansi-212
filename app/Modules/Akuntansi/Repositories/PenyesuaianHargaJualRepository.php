<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\PenyesuaianHargaJual;
use App\Modules\Akuntansi\Models\Produk;
use Generator\Interfaces\RepositoryInterface;
use Carbon\carbon;
use Helpers\IndonesiaDate as DateHelper;
use Helpers\CodeHelper;
use DB;

class PenyesuaianHargaJualRepository implements RepositoryInterface
{
    public function __construct(PenyesuaianHargaJual $model, Produk $produk, DateHelper $date)
    {
        $this->model = $model;
        $this->produk = $produk;
        $this->date = $date;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->find($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_selling_price_adjusment')){
            return abort(403);
        }

        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->findItem($id)->fill($data);
        $model->save();

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $model = $this->model->fill($data);
        $model->save();
            
        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }
    public function listProduk()
    {
        return $this->produk->where('tipe_barang','!==',3)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->no_barang." | ".$item->keterangan;

            return $output;
        }, []);
    }
}