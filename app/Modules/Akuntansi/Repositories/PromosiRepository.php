<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Pos\Models\Promosi;
use App\Modules\Pos\Models\SebabPromosi;
use App\Modules\Pos\Models\AkibatPromosi;
use Auth;
use Generator\Interfaces\RepositoryInterface;

class PromosiRepository implements RepositoryInterface
{
    public function __construct(Promosi $model, SebabPromosi $sebab, AkibatPromosi $akibat)
    {
        $this->model    = $model;
        $this->sebab    = $sebab;
        $this->akibat   = $akibat;
        $this->today    = date('Y-m-d H:i:s');

    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        if(Auth::user()->role == 'akuntansi'){
            return $this->model->paginate(10);
        }else{
            return $this->model->where([
                ['expired_date','>=',$this->today],
                ['active_date','<=',$this->today],
            ])->paginate(10);
        }
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->find($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);

        if ($model->delete()) {
            $this->deleteSubItem($model);
        }

        return $model;
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data = $this->convertData($data);
        $model = $this->findItem($id)->fill($data);

        if ($model->save()) {
            $this->deleteSubItem($model);
            $this->insertSubItem($model, $data);
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {

        $data = $this->convertData($data);
        $model = $this->model->fill($data);
        if ($model->save()) {
            $this->insertSubItem($model,$data);
        }

        return $model;
    }

    /**e
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }

    public function insertSubItem($model, $data)
    {
        // dd($data);
        $dataSebab      = [];
        $sebabTersimpan = "";
        if(array_key_exists('total',$data) && array_key_exists('total_akibat',$data)){
            foreach ($data['total'] as $idx_sebab => $total_sebab) {
                $dataAkibat = [];
                $dataSebab  = [
                    'promosi_id'  => $model->id,
                    'produk_id'   => $data['produk_id'][$idx_sebab] ?? null,
                    'qty'         => $data['qty'][$idx_sebab] ?? null,
                    'total'       => $total_sebab,
                    'type_sebab'  => $data['type_sebab'][$idx_sebab],
                    'tipe_barang' => $data['tipe_barang'][$idx_sebab],
                    'kelipatan'   => $data['kelipatan'][$idx_sebab]
                ];
                $sebabTersimpan = $this->sebab->create($dataSebab);
                foreach ($data['total_akibat'][$idx_sebab] as $idx_akibat => $akibat) {
                    $dataAkibat[] = [
                        'sebab_promosi_id' => $sebabTersimpan->id,
                        'produk_id'        => $data['produk_id_akibat'][$idx_sebab][$idx_akibat] ?? null,
                        'qty'              => $data['qty_akibat'][$idx_sebab][$idx_akibat] ?? 0,
                        'total'            => $data['total_akibat'][$idx_sebab][$idx_akibat],
                        'presentase'       => $data['presentase_akibat'][$idx_sebab][$idx_akibat] ?? 0,
                    ];
                }
                $this->akibat->insert($dataAkibat);
            }
        }
        
        if (array_key_exists('total_akibat_promosi',$data) && array_key_exists('total_sebab_promosi',$data)) {
            foreach ($data['total_akibat_promosi'] as $idx_akibat => $akibat) {
                $dataSebab = [];
                $dataAkibat  = [
                    'promosi_id' => $model->id,
                    'produk_id'  => $data['produk_id_akibat_promosi'][$idx_akibat] ?? null,
                    'qty'        => $data['qty_akibat_promosi'][$idx_akibat] ?? null,
                    'total'      => $data['total_akibat_promosi'][$idx_akibat] ?? null,
                    'presentase'  => $data['presentase_akibat_promosi'][$idx_akibat]
                ];
                $akibatTersimpan = $this->akibat->create($dataAkibat);
                foreach ($data['produk_id_sebab_promosi'][$idx_akibat] as $idx_sebab => $sebab) {
                    $dataSebab[] = [
                        'akibat_promosi_id' => $akibatTersimpan->id,
                        'produk_id'         => $data['produk_id_sebab_promosi'][$idx_akibat][$idx_sebab],
                        'qty'               => $data['qty_sebab_promosi'][$idx_akibat][$idx_sebab],
                        'total'             => $data['total_sebab_promosi'][$idx_akibat][$idx_sebab],
                        'type_sebab'        => $data['type_sebab_promosi'][$idx_akibat],
                        'kelipatan'         => $data['kelipatan_sebab_promosi'][$idx_akibat],
                        'tipe_barang'       => null,
                    ];
                }
                $this->sebab->insert($dataSebab);
            }
        }

        return true;
    }

    public function deleteSubItem($model)
    {
        return $this->sebab->where('promosi_id',$model->id)->delete();
    }

    public function convertData(Array $data) : Array
    {
        $data['active_date'] = date('Y-m-d H:i:s',strtotime($data['active_date']));
        $data['expired_date'] = date('Y-m-d H:i:s',strtotime($data['expired_date']));
        return $data;
    }
}
