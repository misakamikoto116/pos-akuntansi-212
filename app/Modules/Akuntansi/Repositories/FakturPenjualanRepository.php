<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\KategoriProduk;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\DetailInformasiPelanggan;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use App\Modules\Akuntansi\Models\JasaPengiriman;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\TransaksiUangMuka;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\BarangPengirimanPenjualan;
use App\Modules\Akuntansi\Models\PesananPenjualan;
use App\Modules\Akuntansi\Models\BarangPesananPenjualan;
use App\Modules\Akuntansi\Models\BantuanPengirimanPenjualan;
use App\Modules\Akuntansi\Models\BantuanPenerimaanPembelian;

use Carbon\carbon;
use Generator\Interfaces\RepositoryInterface;
use Helpers\IndonesiaDate as DateHelper;
use Helpers\LayoutHelper;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;

class FakturPenjualanRepository implements RepositoryInterface
{
    public function __construct(FakturPenjualan $model, CodeHelper $code, KategoriProduk $kategori, Gudang $gudang, Akun $akun, InformasiPelanggan $pelanggan, SyaratPembayaran $termin, Produk $produk, JasaPengiriman $pengiriman, DetailInformasiPelanggan $detailpelanggan, DateHelper $date, LayoutHelper $layout, KodePajak $kode_pajak, TransaksiHelper $transaksi, PreferensiMataUang $preferensi, PesananPenjualan $pesanan, PengirimanPenjualan $pengirimanPenjualan)
    {
        $this->produk = $produk;
        $this->model = $model;
        $this->pengirimanPenjualan = $pengirimanPenjualan;
        $this->kategori = $kategori;
        $this->gudang = $gudang;
        $this->akun = $akun;
        $this->pelanggan = $pelanggan;
        $this->termin = $termin;
        $this->date = $date;
        $this->detailpelanggan = $detailpelanggan;
        $this->pengiriman = $pengiriman;
        $this->layout = $layout;
        $this->pesanan = $pesanan;
        $this->code = $code;
        $this->kode_pajak = $kode_pajak;
        $this->transaksi = $transaksi;
        // $this->preferensi = $preferensi;
        $this->preferensi = ($preferensi->find(1)) ? $preferensi->find(1) : null;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->where('uang_muka', 0)->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $edit_tanggal = $this->model->findOrFail($id);
        if (!empty($edit_tanggal->invoice_date)) {
            $edit_tanggal->invoice_date = Carbon::parse($edit_tanggal->invoice_date)->format('d F Y');
        }
        if (!empty($edit_tanggal->ship_date)) {
            $edit_tanggal->ship_date = Carbon::parse($edit_tanggal->ship_date)->format('d F Y');
        }
        $edit_tanggal = $this->formattingToNumeric($edit_tanggal);

        return $edit_tanggal;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_faktur_penjualan')){
            return abort(403);
        }
        $model = $this->model->findOrFail($id);
        SaldoAwalBarang::where('no_faktur', $model->no_faktur)->delete(); 
        BarangFakturPenjualan::where('faktur_penjualan_id', $id)->delete();
        TransaksiUangMuka::where('faktur_penjualan_id', $id)->delete();

        $this->transaksi->ExecuteTransactionSingle('delete', $model); // T E R H A P U S
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data = $this->formattingAllData($data);
        $data['taxable'] = (empty($data['taxable'])) ? 0 : 1;
        $data['in_tax'] = (empty($data['in_tax'])) ? 0 : 1;
        $model = $this->findItem($id)->fill($this->dateChecker($data));
        $akun = $this->produk->whereIn('id', $data['produk_id'])->pluck('akun_disk_penjualan_id');
        $this->transaksi->checkGudang($data);

        if ($model->save()) {
            $this->deleteSubItem($data, $model);
            // $this->updateSubItem($data, $model);
            $this->insertSubItem($data, $model);
            $this->insertTransaction('insert', $data, $model);
        }

        if (!empty($data['lanjutkan'])) {
            if ('Simpan & Baru' == $data['lanjutkan']) {
                $model['continue_stat'] = 2;
            } else {
                $model['continue_stat'] = 1;
            }
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->formattingAllData($data);
        $data['taxable'] = (empty($data['taxable'])) ? 0 : 1;
        $data['in_tax'] = (empty($data['in_tax'])) ? 0 : 1;
        $data['no_faktur'] = $this->code->autoGenerate($this->model, 'no_faktur', $data['no_faktur']);
        $data['delivery_no'] = $data['no_faktur'];
        $modelPengiriman = $this->pengirimanPenjualan->fill($this->dateChecker($data));
        $this->transaksi->checkGudang($data);
        $akun = $this->produk->whereIn('id', $data['produk_id'])->pluck('akun_persedian_id');
		if ($modelPengiriman->save()) {
            $dataPengiriman = $this->InsertBarangPengirimanPenjualan($data, $modelPengiriman);
            $data['barang_pengiriman_penjualan_id'] = $dataPengiriman;
            $model = $this->model->fill($this->dateChecker($data));
            if ($model->save()) {
                $this->insertSubItem($data, $model);
                $this->insertTransaction('insert', $data, $model);
            }
		}
        if (!empty($data['lanjutkan'])) {
            if ('Simpan & Baru' == $data['lanjutkan']) {
                $model['continue_stat'] = 2;
            } else {
                $model['continue_stat'] = 1;
            }
        }

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->where('uang_muka', 0)->filter($data)->paginate(20);
    }

    public function listKategoriProduk()
    {
        return $this->kategori->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listGudang()
    {
        return $this->gudang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listAkun()
    {
        return $this->akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function listPelanggan()
    {
        return $this->pelanggan->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listProduk()
    {
        return $this->produk->where('tipe_barang', '!==', 3)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->no_barang.' | '.$item->keterangan;

            return $output;
        }, []);
    }

    public function listTermin()
    {
        return $this->termin->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->naration;

            return $output;
        }, []);
    }

    public function listPajak()
    {
        return $this->kode_pajak->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->kode_pajak_formatted;

            return $output;
        }, []);
    }

    public function listJasaPengiriman()
    {
        return $this->pengiriman->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function getAkunPiutang()
    {
        if (!empty($this->preferensi)) {
            return $this->preferensi->pluck('akun_piutang_id')->first();
        }
    }

    public function idPrediction()
    {
        $data = $this->model->orderBy('id', 'desc')->first();
        if ($data) {
            return $data->id + 1;
        }

        return 1;
    }

    public function dateChecker($data)
    {
        $data['invoice_date'] = (empty($data['invoice_date'])) ? $data['invoice_date'] = date('Y-m-d H:i:s') : $data['invoice_date'] = $this->date->IndonesiaToSql($data['invoice_date']);
        $data['delivery_date'] = $data['invoice_date'];
        $data['ship_date'] = (empty($data['ship_date'])) ? $data['ship_date'] = date('Y-m-d H:i:s') : $data['ship_date'] = $this->date->IndonesiaToSql($data['ship_date']);

        return $data;
    }

    public function deleteSubItem($data, $model)
    {
        SaldoAwalBarang::where('no_faktur', $model->no_faktur)->delete(); 
        BarangFakturPenjualan::where('faktur_penjualan_id', $model->id)->delete();
        TransaksiUangMuka::where('faktur_penjualan_id', $model->id)->delete();
        $this->transaksi->ExecuteTransactionSingle('delete', $model); // T E R H A P U S

        return true;
    }

    public function insertSubItem($data, $model)
    {
        $nilai_pajak_akhir = null;
        if (!empty($data['kode_pajak_id'])) {
            $nilai_pajak_akhir = $this->findPajak($data);
        }
        $data['invoice_date'] = (empty($data['invoice_date'])) ? $data['invoice_date'] = date('Y-m-d H:i:s') : $data['invoice_date'] = $this->date->IndonesiaToSql($data['invoice_date']);
        $this->cekPengiriman($data, $model);
        $items = [];
        $dp = [];
        $produkDetail = [];
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        $count = count($data['produk_id']);
        $count_dp = (empty($data['faktur_uang_muka_pelanggan_id'])) ? 0 : count($data['faktur_uang_muka_pelanggan_id']);
        for ($i = 0; $i < $count; ++$i) {
            $pajak1 = null;
            $pajak2 = null;

            // Untuk Harga Final & Bagian Diskon Master
            $barang = $data['unit_harga_produk'][$i] * $data['qty_produk'][$i];
            $total_diskon = (($barang * $data['diskon_produk'][$i]) / 100);
            $persen_barang = $data['amount_produk'][$i] / array_sum($data['amount_produk']);
            $total_diskon_faktur = (empty($data['jumlah_diskon_faktur'])) ? 0 : $data['jumlah_diskon_faktur'];
            $total_diskon_master = ($total_diskon_faktur * $persen_barang);
            $total_barang = ($barang - $total_diskon - $total_diskon_master)  - (!empty($data['in_tax']) ? $nilai_pajak_akhir[$i]['sum_pajak'] : 0);

            if (!empty($data['kode_pajak_id'][$i])) {
                // data ada.. walau hanya satu
                $pajak1 = $data['kode_pajak_id'][$i][0];
                if (count($data['kode_pajak_id'][$i]) > 1) {
                    $pajak2 = $data['kode_pajak_id'][$i][1];
                }
            }
            $items[] = [
                'produk_id' => $data['produk_id'][$i],
                'faktur_penjualan_id' => $model->id,
                'item_deskripsi' => $data['keterangan_produk'][$i],
                'item_unit' => $data['satuan_produk'][$i],
                'jumlah' => $data['qty_produk'][$i],
                'barang_pengiriman_penjualan_id' => !empty($data['barang_pengiriman_penjualan_id']) ? $data['barang_pengiriman_penjualan_id'][$i] : null,
                'unit_price' => $data['unit_harga_produk'][$i],
                'diskon' => $data['diskon_produk'][$i],
                'harga_modal' => $data['harga_modal'][$i],
                'gudang_id' => !empty($data['gudang_id']) ? $data['gudang_id'][$i] : null,
                'kode_pajak_id' => $pajak1,
                'kode_pajak_2_id' => $pajak2,
                'harga_final' => $total_barang / $data['qty_produk'][$i],
                'bagian_diskon_master' => $total_diskon_master,
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];
        }
        BarangFakturPenjualan::insert($items);

        if (!empty($data['faktur_uang_muka_pelanggan_id'])) {
            //cek apakah DPnya cukup? ato kelebihan?
            $this->cekDPpelanggan($count_dp, $data);

            for ($i = 0; $i < $count_dp; ++$i) {
                $in_tax = (empty($data['in_tax_dp'][$i])) ? 0 : $data['in_tax_dp'][$i];
                $pajak1 = null;
                $pajak2 = null;
                $total_pajak1 = null;
                $total_pajak2 = null;
                if (!empty($data['pajak_dp'][$i])) {
                    // data ada.. walau hanya satu
                    if (!empty($data['pajak_dp'][$i][0])) {
                        $pajak1 = $data['pajak_dp'][$i][0];
                        $data_pajak1 = KodePajak::find($pajak1);
                        $total_pajak1 = ($data['total_uang_muka_dp'][$i] * $data_pajak1->nilai) / 100;
                    }
                    if (count($data['pajak_dp'][$i]) > 1) {
                        if (!empty($data['pajak_dp'][$i][1])) {
                            $pajak2 = $data['pajak_dp'][$i][1];
                            $data_pajak2 = KodePajak::find($pajak2);
                            $total_pajak2 = ($data['total_uang_muka_dp'][$i] * $data_pajak2->nilai) / 100;
                        }
                    }
                }
                $dp[] = [
                    'faktur_uang_muka_pelanggan_id' => $data['faktur_uang_muka_pelanggan_id'][$i],
                    'faktur_penjualan_id' => $model->id,
                    'jumlah' => $data['total_uang_muka_dp'][$i],
                    'pajak_satu_id' => $pajak1,
                    'pajak_dua_id' => $pajak2,
                    'total_pajak1' => $total_pajak1,
                    'total_pajak2' => $total_pajak2,
                    'created_at' => $jam_sekarang,
                    'updated_at' => $jam_sekarang,
                ];
            }
            TransaksiUangMuka::insert($dp);
        }
        $this->checkIfAllItemWasUsed($data, $model);
        return true;
    }

    /**
     * For Transasction Purpose.
     */
    public function insertTransaction($param, $data, $model)
    {
        $data['invoice_date'] = (empty($data['invoice_date'])) ? $data['invoice_date'] = date('Y-m-d H:i:s') : $data['invoice_date'] = $this->date->IndonesiaToSql($data['invoice_date']);
        $akumulasi_diskon_item = 0;
        $arrayDebit = [];
        $arrayKredit = [];
        $akumulasi_dp = 0;
        
        $nilai_pajak_akhir = null;
        if (!empty($data['kode_pajak_id'])) {
            $nilai_pajak_akhir = $this->findPajak($data);
        }
        // array akun piutang => debit
        if (!empty($data['faktur_uang_muka_pelanggan_id'])) {
            $i = 0;
            foreach ($data['total_uang_muka_dp'] as $dp) {
                $total_pajak1 = 0;
                $total_pajak2 = 0;
                if (!empty($data['pajak_dp'][$i])) {
                    // data ada.. walau hanya satu
                    $pajak1 = $data['pajak_dp'][$i][0];
                    $data_pajak1 = KodePajak::find($pajak1); // res : collection
                    $total_pajak1 = ($dp * $data_pajak1->nilai) / 100; // res : float

                    if (count($data['pajak_dp'][$i]) > 1) {
                        $pajak2 = $data['pajak_dp'][$i][1];
                        $data_pajak2 = KodePajak::find($pajak2); // res : collection
                        $total_pajak2 = ($dp * $data_pajak2->nilai) / 100; // res : float
                    }
                }
                // $arrayKredit[] = [ // uang muka => kredit
                //     'nominal' => $dp + $total_pajak1 + $total_pajak2,
                //     'dari' => 'DP',
                //     'akun' => $data['akun_piutang_id'],
                // ];
                $akumulasi_dp += $dp + $total_pajak1 + $total_pajak2;
                ++$i;
            }

            $i = 0;

            foreach ($data['total_uang_muka_dp'] as $pajak) {
                $arrayDebit[] = [ //insert dp per item dengan debit
                    'nominal' => $pajak,
                    'tanggal' => $data['invoice_date'],
                    'dari' => 'DP per item',
                    'akun' => $data['akun_id_dp'][$i],
                ];
                ++$i;
            }
        }

        $pengurangan_total_dari_dp = $data['total'] - $akumulasi_dp;
        // dd($akumulasi_dp);
        if ($pengurangan_total_dari_dp > 0) {
            $arrayDebit[] = ['nominal' => $pengurangan_total_dari_dp, 'tanggal' => $data['invoice_date'], 'dari' => 'Total Penjualan', 'akun' => $data['akun_piutang_id']];
        } else {
            $arrayKredit[] = ['nominal' => $pengurangan_total_dari_dp, 'tanggal' => $data['invoice_date'], 'dari' => 'Total Penjualan', 'akun' => $data['akun_piutang_id']];
        }
        $arrayKredit[] = ['nominal' => $data['ongkir'], 'tanggal' => $data['invoice_date'], 'dari' => 'Ongkir Penjualan', 'akun' => $data['akun_ongkir_id']];

        if (!empty($data['kode_pajak_id'])) {
            $arrayPajak = $this->hitungPajak($data);
            $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayPajak, 0, $data['no_faktur']); //insert pajak per item (kredit)
        }

        if (!empty($data['pajak_dp'])) {
            $arrayPajakDp = $this->hitungPajakDp($data);
            $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayPajakDp, 1, $data['no_faktur']); //insert pajak per item (kredit)
        }

        $count = count($data['produk_id']);

        $detailProduk = Produk::whereIn('id', $data['produk_id'])->get()->toArray();
        $detailProdukArr = []; // y I do dis.. kalau 10 barang brti 10x query.. itu berat mz

        foreach ($data['produk_id'] as $produk) { //cari itemnya.. samakan indexnya..
            $i = 0;
            $x = 0;
            while ($x < 1) {
                if ($detailProduk[$i]['id'] == $produk) {
                    $detailProdukArr[] = [
                        'akun_penjualan_id' => $detailProduk[$i]['akun_penjualan_id'],
                        'akun_hpp_id' => $detailProduk[$i]['akun_hpp_id'],
                        'akun_disk_penjualan_id' => $detailProduk[$i]['akun_disk_penjualan_id'],
                        'akun_barang_terkirim_id' => $detailProduk[$i]['akun_barang_terkirim_id'],
                    ];
                    $x = 1;
                }
                ++$i;
            }
        }

        $sum_diskon_master = 0;

        for ($i = 0; $i < $count; ++$i) {

            $total_barang               = $this->perhitunganBarangPajak($data, $i, $nilai_pajak_akhir)['total_barang'];            
            $total_harga_modal          = $data['harga_modal'][$i] == 0 ? $data['amount_produk'][$i] * $data['qty_produk'][$i] : $data['harga_modal'][$i] * $data['qty_produk'][$i];
            $akumulasi_diskon_item      += $this->perhitunganBarangPajak($data, $i, $nilai_pajak_akhir)['total_diskon'];
            $sum_diskon_master          += $this->perhitunganBarangPajak($data, $i, $nilai_pajak_akhir)['total_diskon_master'];

            $total_pajak1 = 0;
            $total_pajak2 = 0;

            if (!empty($data['kode_pajak_id'][$i])) {
                // data ada.. walau hanya satu
                $pajak1 = $data['kode_pajak_id'][$i][0];
                $data_pajak1 = KodePajak::find($pajak1); // res : collection
                $total_pajak1 = ($total_barang * $data_pajak1->nilai) / 100; // res : float
                if (count($data['kode_pajak_id'][$i]) > 1) {
                    $pajak2 = $data['kode_pajak_id'][$i][1];
                    $data_pajak2 = KodePajak::find($pajak2); // res : collection
                    $total_pajak2 = ($total_barang * $data_pajak2->nilai) / 100; // res : float
                }
            }

            $arrayKredit[] = [ //penjualan dengan tax & diskon => kredit | ini yg kemaren  = + $total_pajak1 + $total_pajak2
                'nominal'   => $total_barang,
                'tanggal'   => $data['invoice_date'],
                'dari'      => 'Penjualan dengan Tax & Disc',
                'produk'    => $data['produk_id'][$i],
                'akun'      => $detailProdukArr[$i]['akun_penjualan_id'],
            ];
            $arrayDebit[] = [ //penjualan hpp => debit
                'nominal'   => $total_harga_modal,
                'tanggal'   => $data['invoice_date'],
                'dari'      => 'Penjualan HPP',
                'produk'    => $data['produk_id'][$i],
                'akun'      => $detailProdukArr[$i]['akun_hpp_id'],
            ];
            
            $arrayKredit[] = [ //barang terkirim => kredit
                'nominal'   => $total_harga_modal,
                'tanggal'   => $data['invoice_date'],
                'dari'      => 'Barang Terkirim',
                'produk'    => $data['produk_id'][$i],
                'akun'      => $detailProdukArr[$i]['akun_barang_terkirim_id'],
            ];
            $arrayDebit[] = [ //selisih diskon => Debit
                'nominal'   => $this->perhitunganBarangPajak($data, $i, $nilai_pajak_akhir)['total_diskon'],
                'tanggal'   => $data['invoice_date'],
                'produk'    => $data['produk_id'][$i],
                'dari'      => 'Selisih Diskon',
                'akun'      => $detailProdukArr[$i]['akun_disk_penjualan_id'],
            ];
        }

        $arrayDebit[] = [ //penjualan diskon => debit jar | $akumulasi_diskon_item
            'nominal' => $sum_diskon_master,
            'tanggal' => $data['invoice_date'],
            'dari' => 'Diskon Penjualan',
            'akun' => (null == $this->preferensi) ? null : $this->preferensi->diskon_penjualan_id,
        ];

        $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayDebit, 1, $data['no_faktur']);
        $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayKredit, 0, $data['no_faktur']);

        // sementara statis yg dibawah ini
        // $arrayFaktur[] = [
        //     'nominal' => $data['jumlah_diskon_faktur'],
        //     'produk'  => null,
        //     'akun'    => 56,
        //     'id'    => $data['no_faktur']
        // ];
        // $this->transaksi->ExecuteTransactionMultiple($param, $model, $arrayFaktur, 1); #diskon faktur
        return true;
    }

    public function perhitunganBarangPajak($data, $i, $nilai_pajak_akhir)
    {
        $barang                     = ($data['unit_harga_produk'][$i] * $data['qty_produk'][$i]);
        $total_diskon               = ($barang * $data['diskon_produk'][$i]) / 100;
        $persen_barang              = $data['amount_produk'][$i] / array_sum($data['amount_produk']);
        $total_diskon_faktur        = (empty($data['jumlah_diskon_faktur'])) ? 0 : $data['jumlah_diskon_faktur'];
        $total_diskon_master        = ($total_diskon_faktur * $persen_barang);
        
        if (!empty($data['in_tax'])) {
            $total_diskon_in_tax                = ( 100 / (100 + $nilai_pajak_akhir[$i]['total_pajak']) * $total_diskon);
            $total_diskon_master_in_tax         = ( 100 / (100 + $nilai_pajak_akhir[$i]['total_pajak']) * $total_diskon_master);
            $total_barang                       = ($barang - ($total_diskon - $total_diskon_in_tax) - ($total_diskon_master - $total_diskon_master_in_tax)) - $nilai_pajak_akhir[$i]['sum_pajak'];
            $total_diskon                       = $total_diskon_in_tax;
            $total_diskon_master                = $total_diskon_master_in_tax;
        }else {
            $total_barang                       = $barang;
        }

        return [
            'total_barang'          => $total_barang,
            'total_diskon'          => $total_diskon,
            'total_diskon_master'   => $total_diskon_master,
        ];
    }

    /**
     * Fungsi2 pendukung ada dibawah sini
     * cekidot.
     */
    public function cekDPpelanggan($count_dp, $data)
    {
        $total_dp = 0;
        $angka_yang_sudah_di_dp = 0;

        for ($y = 0; $y < $count_dp; ++$y) {
            $cek_transaksi = TransaksiUangMuka::where('faktur_uang_muka_pelanggan_id', $data['faktur_uang_muka_pelanggan_id'][$y])->get();
            if (!empty($cek_transaksi)) {
                $angka_yang_sudah_di_dp += $cek_transaksi->sum('jumlah');
                $total_dp += $data['total_uang_muka_dp'][$y];
            }
        }
        $total_dp -= $angka_yang_sudah_di_dp;
        // Yang di DP melebihi total belanja
        if ($total_dp > $data['total']) {
            throw new \Exception('DP yang dipakai berlebihan');
        }

        return true;
    }

    public function hitungPajak($data)
    {
        $nilai_pajak_akhir = $this->findPajak($data);

        $arrayPajak = [];
        $count = count($data['produk_id']);
        for ($i = 0; $i < $count; ++$i) {
            $pajak1 = null;
            $pajak2 = null;
            $barang = $data['unit_harga_produk'][$i] * $data['qty_produk'][$i];
            $total_diskon = (($barang * $data['diskon_produk'][$i]) / 100);
            $persen_barang = $data['amount_produk'][$i] / array_sum($data['amount_produk']);
            $total_diskon_faktur = (empty($data['jumlah_diskon_faktur'])) ? 0 : $data['jumlah_diskon_faktur'];
            $total_diskon_master = ($total_diskon_faktur * $persen_barang);
            $total_barang = $barang - $total_diskon - $total_diskon_master;
            // dd($total_diskon, $total_diskon_master);

            if (!empty($data['kode_pajak_id'][$i])) {
                // data ada.. walau hanya satu
                $pajak1 = $data['kode_pajak_id'][$i][0];
                $data_pajak1 = KodePajak::find($pajak1); // res : collection
                if (!empty($data['in_tax'])) {
                    $total_pajak1 = $nilai_pajak_akhir[$i]['pajak_1'];
                } else {
                    $total_pajak1 = ($total_barang * $data_pajak1->nilai) / 100;
                }
                $arrayPajak[] = [
                    'nominal'   => $total_pajak1,
                    'tanggal'   => $data['invoice_date'],
                    'produk'    => $data['produk_id'][$i],
                    'akun'      => $data_pajak1->akun_pajak_penjualan_id,
                    'dari'      => 'Pajak '.$data_pajak1->akun_pajak_penjualan_id.' per Item',
                ];

                if (count($data['kode_pajak_id'][$i]) > 1) {
                    $pajak2 = $data['kode_pajak_id'][$i][1];
                    $data_pajak2 = KodePajak::find($pajak2); // res : collection
                    if (!empty($data['in_tax'])) {
                        $total_pajak2 = $nilai_pajak_akhir[$i]['pajak_2'];
                    } else {
                        $total_pajak2 = ($total_barang * $data_pajak2->nilai) / 100;
                    }
                    $arrayPajak[] = [
                        'nominal'   => $total_pajak2,
                        'tanggal'   => $data['invoice_date'],
                        'produk'    => $data['produk_id'][$i],
                        'akun'      => $data_pajak2->akun_pajak_penjualan_id,
                        'dari'      => 'Pajak '.$data_pajak2->akun_pajak_penjualan_id.' per Item',
                    ];
                }
            }
        }

        return $arrayPajak;
    }

    public function findPajak($data)
    {
        $nilai_pajak_akhir             = [];
        // rumus mencari pajak in tax
        // mencari array price per unit
        $array_unit_price              = $this->sumUnitPrice($data);

        foreach ($array_unit_price as $key => $unit_price) {
            // cek kode_pajak_id
            $kode_pajak_id = array_key_exists('kode_pajak_id', $data) ? $data['kode_pajak_id'] : null;
            // mencari array nilai pajak
            $array_nilai_pajak                  = $this->findNilaiPajak($kode_pajak_id);
            // mencari total pajak
            $total_tax                          = $this->totalTax($array_nilai_pajak);

            $after_dikurang_diskon              = $this->afterDikurangDiskon($unit_price, $array_unit_price, $data['jumlah_diskon_faktur']);

            // mencari nilai pajak akhir
            $nilai_pajak_akhir[]                = $this->calculateSortedPajak($unit_price, $total_tax, $array_nilai_pajak, $after_dikurang_diskon);
        }

        return $nilai_pajak_akhir;
    }

    public function sumUnitPrice($data)
    {
        $array_unit_price   = [];

        foreach($data['unit_harga_produk'] as $key => $unit_price) {
            $sum_qty_price                      = $unit_price * $data['qty_produk'][$key];
            $sum_qty_price_with_diskon          = $sum_qty_price - (($data['diskon_produk'][$key] / 100 ) * $sum_qty_price);
            $array_unit_price[]                 = $sum_qty_price_with_diskon;
        }
   
        return $array_unit_price;
    }

    public function findNilaiPajak($data_pajak)
    {
        $pajak_1 = null;
        $pajak_2 = null;

        if (!empty($data_pajak)) {
            $pajak_1        = KodePajak::find($data_pajak[0]);
            if (collect($data_pajak)->count() > 1) {
                $pajak_2        = KodePajak::find($data_pajak[1]);
            }    
        }

        return [
            'pajak_1'       => $pajak_1,
            'pajak_2'       => $pajak_2,
        ];
    }

    public function totalTax($data_pajak)
    { 
        $total_pajak    = (!empty($this->checkNilaiPajak($data_pajak)['nilai_pajak_1']) ? $this->checkNilaiPajak($data_pajak)['nilai_pajak_1']->nilai : null ) + 
                          (!empty($this->checkNilaiPajak($data_pajak)['nilai_pajak_2']) ? $this->checkNilaiPajak($data_pajak)['nilai_pajak_2']->nilai : null ); 

        return $total_pajak;
    }

    public function checkNilaiPajak($data_pajak)
    {
        $nilai_pajak_1 = null;
        $nilai_pajak_2 = null;

        if (array_key_exists('pajak_1', $data_pajak)) {
            if (!empty($data_pajak['pajak_1'])) {
                $nilai_pajak_1 = $data_pajak['pajak_1'];
            }
        }

        if (array_key_exists('pajak_2', $data_pajak)) {
            if (!empty($data_pajak['pajak_2'])) {
                $nilai_pajak_2 = $data_pajak['pajak_2'];
            }
        }

        return [
            'nilai_pajak_1'     => $nilai_pajak_1,
            'nilai_pajak_2'     => $nilai_pajak_2,
        ];

    }

    public function afterDikurangDiskon($unit_price, $array_unit_price, $diskon)
    {
        return ($unit_price / array_sum($array_unit_price)) * $diskon;
    }

    public function calculateSortedPajak($unit_price, $total_tax, $array_nilai_pajak, $after_dikurang_diskon)
    {
        $nilai_pajak_1      = !empty($array_nilai_pajak['pajak_1']) ? $array_nilai_pajak['pajak_1'] : null;
        $nilai_pajak_2      = !empty($array_nilai_pajak['pajak_2']) ? $array_nilai_pajak['pajak_2'] : null;

        // (100 / (100 + 30) * (99000 - 9900))

        $temp_tax           = (100 / (100 + $total_tax) * ($unit_price - $after_dikurang_diskon));
        $total_in_tax1      = $temp_tax * ($nilai_pajak_1->nilai / 100);
        $total_in_tax2      = $temp_tax * ($nilai_pajak_2->nilai / 100);
        $sum_in_tax         = round($total_in_tax1, 2) + round($total_in_tax2, 2);

        $all_pajak = [
            'pajak_1'       => round($total_in_tax1, 2),
            'pajak_2'       => round($total_in_tax2, 2),
            'nilai_pajak_1' => $nilai_pajak_1,
            'nilai_pajak_2' => $nilai_pajak_2,
            'sum_pajak'     => $sum_in_tax,
            'total_pajak'   => $total_tax,
        ];

        return $all_pajak;
    }

    public function hitungPajakDp($data)
    {
        $arrayPajak = [];
        $count = count($data['akun_id_dp']);
        for ($i = 0; $i < $count; ++$i) {
            $pajak1 = null;
            $pajak2 = null;
            $total_barang = $data['total_uang_muka_dp'][$i];

            if (!empty($data['pajak_dp'][$i])) {
                // data ada.. walau hanya satu
                $pajak1 = $data['pajak_dp'][$i][0];
                $data_pajak1 = KodePajak::find($pajak1); // res : collection
                $total_pajak1 = ($total_barang * $data_pajak1->nilai) / 100; // res : float

                $arrayPajak[] = [
                    'nominal' => $total_pajak1,
                    'tanggal' => $data['invoice_date'],
                    'produk' => null,
                    'akun' => $data_pajak1->akun_pajak_penjualan_id,
                    'dari' => 'Pajak per Item (DP)',
                ];

                if (count($data['pajak_dp'][$i]) > 1) {
                    $pajak2 = $data['pajak_dp'][$i][1];
                    $data_pajak2 = KodePajak::find($pajak2); // res : collection
                    $total_pajak2 = ($total_barang * $data_pajak2->nilai) / 100; // res : float
                    $arrayPajak[] = [
                        'nominal' => $total_pajak2,
                        'tanggal' => $data['invoice_date'],
                        'produk' => null,
                        'akun' => $data_pajak2->akun_pajak_penjualan_id,
                        'dari' => 'Pajak per Item (DP)',
                    ];
                }
            }
        }

        return $arrayPajak;
    }

    public function cekPengiriman($data, $model)
    {
        // Delete semua transaksi jika punya pengiriman
        $produkDetail = [];
        if (!empty($data['pengiriman'])) {
            $getProduk = $this->produk->whereIn('id', $data['produk_id']);

            $akunPersediaan = $getProduk->pluck('akun_persedian_id', 'id')->toArray();
            $akunBarangTerkrim = $getProduk->pluck('akun_barang_terkirim_id', 'id')->toArray();

            foreach ($data['pengiriman'] as $pengiriman_id) {
                $arrayBarangTerkirim = [];
                $arrayPersediaan = [];

                $getTransaksi = Transaksi::where(['item_id' => $pengiriman_id, 'item_type' => 'App\Modules\Akuntansi\Models\PengirimanPenjualan']);
                $pluckTransaksi = $getTransaksi->pluck('id');
                // foreach ($pluckTransaksi as $gt) {
                // 	// mencari hasil similiar
                // 	SaldoAwalBarang::where([
                // 		'status' => 1,
                // 		'sn'     => null,
                // 		'item_type' => get_class(new PengirimanPenjualan),
                // 		'transaksi_id'     => $gt, ])->delete();
                // }
                $getTransaksi->delete();

                $pengiriman = PengirimanPenjualan::find($pengiriman_id);
                // loop per pengiriman
                foreach ($pengiriman->barang as $barang) {
                    // cek per index
                    foreach ($data['produk_id'] as $i => $produkID) {
                        // jika mempunya pengiriman penjualan id
                        if (!empty($data['barang_pengiriman_penjualan_id'][$i])) {
                            // jika barang dari pengiriman (id) sama indexnya dengan yg ada.. maka
                            if ($data['barang_pengiriman_penjualan_id'][$i] == $barang->id) {
                                $arrayBarangTerkirim = [
                                    'nominal' => $data['harga_modal'][$i] * $data['qty_produk'][$i],
                                    'tanggal' => $data['invoice_date'],
                                    'produk' => $data['produk_id'][$i],
                                    'akun' => $akunBarangTerkrim[$produkID],
                                    'item_id' => $pengiriman_id,
                                    'item_type' => 'App\Modules\Akuntansi\Models\PengirimanPenjualan',
                                    'dari' => 'Barang Terkirim',
                                ];

                                $arrayPersediaan[] = [
                                    'nominal' => $data['harga_modal'][$i] * $data['qty_produk'][$i],
                                    'tanggal' => $data['invoice_date'],
                                    'produk' => $data['produk_id'][$i],
                                    'akun' => $akunPersediaan[$produkID],
                                    'item_id' => $pengiriman_id,
                                    'item_type' => 'App\Modules\Akuntansi\Models\PengirimanPenjualan',
                                    'dari' => 'Persediaan',
                                ];
                            }
                        }
                    }
                    $getRandomLatestTransaction = $this->transaksi->ExecuteTransactionSingle('insert', $pengiriman, $arrayBarangTerkirim, 1, $data['po_no']); // piutang usaha => debit
                    SaldoAwalBarang::where([
                        'item_id' => $pengiriman_id,
                        'item_type' => 'App\Modules\Akuntansi\Models\PengirimanPenjualan',
                        'produk_id' => $arrayBarangTerkirim['produk'],
                    ])->update(['transaksi_id' => $getRandomLatestTransaction->id]);
                }

                foreach ($data['produk_id'] as $i => $produkID) {
                    $produkDetail[] = [
                        'no_faktur' => $data['po_no'],
                        'tanggal' => $data['delivery_date'],
                        'kuantitas' => 0,
                        'harga_modal' => $data['harga_modal'][$i],
                        'harga_terakhir' => $data['harga_terakhir'][$i],
                        'gudang_id' => !empty($data['gudang_id']) ? $data['gudang_id'][$i] : null,
                        'transaksi_id' => $getRandomLatestTransaction->id,
                        'sn' => null,
                        'status' => 1,
                        'item_type' => get_class($this->model),
                        'produk_id' => $produkID,
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                }
                $this->transaksi->ExecuteTransactionMultiple('insert', $pengiriman, $arrayPersediaan, 0, $data['po_no']); // piutang usaha => debit
                SaldoAwalBarang::insert($produkDetail);
            }
        }

        return true;
    }

    public function checkIfAllItemWasUsed($data, $model)
    {
        $valid_pesanan = 1;

        $barang_pengiriman = [];
        $barang_pesanan = [];
        $pesanan_id = [];

        foreach ($data['produk_id'] as $key => $val) { // key means index
            if (!empty($data['barang_pengiriman_penjualan_id'])) {
                array_push($barang_pengiriman, $data['barang_pengiriman_penjualan_id'][$key]);
            }
        }

        if (empty($barang_pengiriman)) {
            return true; // abort mission
        }

        $getBarangPengiriman = BarangPengirimanPenjualan::whereIn('id', $barang_pengiriman);
        foreach ($getBarangPengiriman->get() as $index => $gbp) {
            // cek apakah semua barang di kirim sudah di pakai
            // first what index ?
            $i = array_search($gbp->produk_id, $data['produk_id']);

            if (false === $i) {
                // which is not valid
                $valid_pesanan = 0;
            } else {
                // second, we got the product who is exaclty same.. then insert them to new array
                if (!empty($gbp->barang_pesanan_penjualan_id)) {
                    array_push($barang_pesanan, $gbp->barang_pesanan_penjualan_id);
                }
            }
        }

        // ==================================================================

        if (empty($barang_pesanan) or false === $i) {
            return true; // abort mission
        }

        $getBarangPesanan = BarangPesananPenjualan::whereIn('id', $barang_pesanan);
        foreach ($getBarangPesanan->get() as $index => $gbp) {
            // cek apakah semua barang di pesanan sudah di pakai
            // first what index ?
            $i = array_search($gbp->produk_id, $data['produk_id']);
            // second, if we dont get the index, it's mean not all of the item are used
            if (false === $i) {
                // which is not valid
                $valid_pesanan = 0;
            } else {
                // third, does the value is same or more?
                if ($data['qty_produk'][$i] < $gbp->jumlah) {
                    // if it's not equals/more than it's not valid
                    $valid_pesanan = 0;
                }
                // fourth. well all is valid tho
                array_push($pesanan_id, $gbp->pesanan_penjualan_id);
            }
        }
        if (1 == $valid_pesanan && null != $pesanan_id) {
            $this->transaksi->UpdateParentStatusDua($this->pesanan, '3', $pesanan_id); //update status pesanan
        } else {
            return true; //abort mission
        }
    }

    public function formattingAllData($data)
    {
        // data faktur
        $data['ongkir'] = floatval(preg_replace('/[^\d.]/', '', $data['ongkir']));
        $data['jumlah_diskon_faktur'] = floatval(preg_replace('/[^\d.]/', '', $data['jumlah_diskon_faktur']));

        // data produk
        if (!empty($data['produk_id'])) {
            for ($i = 0; $i < count($data['produk_id']); ++$i) {
                $data['unit_harga_produk'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['unit_harga_produk'][$i]));
                $data['amount_produk'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['amount_produk'][$i]));
            }
        }

        // data uang muka
        if (!empty($data['total_uang_muka_dp'])) {
            for ($y = 0; $y < count($data['total_uang_muka_dp']); ++$y) {
                $data['total_uang_muka_dp'][$y] = floatval(preg_replace('/[^\d.]/', '', $data['total_uang_muka_dp'][$y]));
            }
        }

        return $data;
    }

    public function formattingToNumeric($data)
    {
        if (!empty($data->ongkir)) {
            $data->ongkir = number_format($data->ongkir);
        }
        if (!empty($data->jumlah_diskon_faktur)) {
            $data->jumlah_diskon_faktur = number_format($data->jumlah_diskon_faktur);
        }

        return $data;
    }

    public function InsertBarangPengirimanPenjualan($data, $model)
	{
		$items             = [];
		$produkDetail      = [];
		$jam_sekarang      = Carbon::now()->format('Y-m-d H:i:s');
		$count             = count($data['produk_id']);
		$getProduk         = $this->produk->whereIn('id', $data['produk_id']);
		$akunPersediaan    = $getProduk->pluck('akun_persedian_id', 'id')->toArray();
		$akunBarangTerkrim = $getProduk->pluck('akun_barang_terkirim_id', 'id')->toArray();
		foreach ($data['produk_id'] as $i => $produk_id) {
			$items = [
				'item_deskripsi'              => $data['keterangan_produk'][$i],
				'produk_id'                   => $data['produk_id'][$i],
				'pengiriman_penjualan_id'     => $model->id,
				'jumlah'                      => $data['qty_produk'][$i],
				'item_unit'                   => $data['satuan_produk'][$i],
				'barang_pesanan_penjualan_id' => !empty($data['barang_id']) ? $data['barang_id'][$i] : null,
				'gudang_id'                   => $data['gudang_id'][$i],
				'harga_modal'                 => $data['harga_modal'][$i],
				'no_so'                       => $data['po_no'],
				'created_at'                  => $jam_sekarang,
				'updated_at'                  => $jam_sekarang,
            ];
			$arrayBarangTerkirim = [
                'nominal' => $data['harga_modal'][$i] * $data['qty_produk'][$i],
				'tanggal' => $data['delivery_date'],
				'produk'  => $data['produk_id'][$i],
				'akun'    => $akunBarangTerkrim[$produk_id],
				'dari'    => 'Barang Terkirim',
			];
			$transaksi_barang_terkirim = $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrayBarangTerkirim, 1, $data['po_no']); // piutang usaha => debit
			$produkDetail[] = [
				'no_faktur'        => $data['po_no'],
				'tanggal'          => $data['delivery_date'],
				'kuantitas'        => -$data['qty_produk'][$i],
				'harga_modal'      => $data['harga_modal'][$i],
				'harga_terakhir'   => $data['harga_terakhir'][$i] == null ? $data['unit_harga_produk'][$i] : $data['harga_terakhir'][$i],
				'gudang_id'        => !empty($data['gudang_id']) ? $data['gudang_id'][$i] : null,
				'transaksi_id'     => $transaksi_barang_terkirim->id, //harusnya pake $model->id
				'sn'               => null,
				'status'           => 1,
				'item_type' 	   => get_class($this->model), // dari pengiriman
				'item_id' 		   => $model->id, // dari pengiriman
				'produk_id'        => $data['produk_id'][$i],
				'created_at'       => date('Y-m-d H:i:s'),
			];
			$arrayPersediaan[] = [
				'nominal' => $data['harga_modal'][$i] * $data['qty_produk'][$i],
				'tanggal' => $data['delivery_date'],
				'produk'  => $data['produk_id'][$i],
				'akun'    => $akunPersediaan[$produk_id],
				'dari'    => 'Persediaan',
			];
			$bantuan_pembelian = BantuanPenerimaanPembelian::where('produk_id', $data['produk_id'][$i])->orderBy('id')->first();
            $brg_pengiriman = BarangPengirimanPenjualan::create($items);
			if(!empty($bantuan_pembelian)){
                BantuanPengirimanPenjualan::insert([
                    'brg_pengiriman_penjualan_id' => $brg_pengiriman->id,
					'bnt_penerimaan_pembelian_id' => $bantuan_pembelian->id
                ]);	
            }
            $dataPengirimanId[] = $brg_pengiriman->id;
		}
		$this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayPersediaan, 0, $data['po_no']); // piutang usaha => debit
		SaldoAwalBarang::insert($produkDetail);
        $this->checkIfAllItemWasUsed($data, $model);
        
		return $dataPengirimanId;
	}
}
