<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\PembiayaanPesanan;
use App\Modules\Akuntansi\Models\DetailPembiayaanPesanan;
use App\Modules\Akuntansi\Models\BebanPembiayaanPesanan;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\Gudang;
use Generator\Interfaces\RepositoryInterface;
use Helpers\TransaksiHelper;

class PembiayaanPesananRepository implements RepositoryInterface
{
    public function __construct(
        PembiayaanPesanan $model,
        Gudang $gudang,
        Akun $akun,
        DetailPembiayaanPesanan $detail,
        BebanPembiayaanPesanan $beban,
        TransaksiHelper $transaksi,
        Produk $produk,
        SaldoAwalBarang $produkDetail
    ) {
        $this->model = $model;
        $this->akun = $akun;
        $this->gudang = $gudang;
        $this->detail = $detail;
        $this->beban = $beban;
        $this->transaksi = $transaksi;
        $this->produk = $produk;
        $this->produkDetail = $produkDetail;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->orderBy('id','DESC')->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->find($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_pembiayaan_pesanan')){
            return abort(403);
        }

        $model = $this->findItem($id);
        if ($model->delete()) {
            $this->deleteSubItem($model, $id);
        }

        return $model;
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data = $this->formattingArrayData($data);
        $model = $this->findItem($id)->fill($data);

        if ($model->save()) {
            $this->deleteSubItem($model, $id);
            $this->insertSubItem($model, $data);
        }

        if (!empty($data['lanjutkan'])) {
            if ($data['lanjutkan'] == 'Simpan & Baru') {
                $model['continue_stat'] = 2;
            } else {
                $model['continue_stat'] = 1;
            }
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->formattingArrayData($data);
        $model = $this->model->fill($data);
        if ($model->save()) {
            $this->insertSubItem($model, $data);
        }

        if (!empty($data['lanjutkan'])) {
            if ($data['lanjutkan'] == 'Simpan & Baru') {
                $model['continue_stat'] = 2;
            } else {
                $model['continue_stat'] = 1;
            }
        }

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(20);
    }

    public function listGudang()
    {
        return $this->gudang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listAkun()
    {
        return $this->akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function insertSubItem(PembiayaanPesanan $model, array $data)
    {
        try {
            $data = $this->formattingArrayData($data);
            $arrDebit = [];
            $arrKredit = [];
            $arrKreditSingle = [];
            $items = [];
            $produkDetail = [];
            $grandtotal = 0;
            $akun = $this->produk->whereIn('id', $data['produk_id'])->get();
            // dd($data);
            foreach ($data['produk_id'] as $idx => $produk_id) {
                $itemRecord = $akun->where('id', $produk_id)->first();

                $grandtotal += $data['amount_produk'][$idx];

                $items[] = [
                    'pembiayaan_pesanan_id' => $model->id,
                    'produk_id' => $produk_id,
                    'tanggal' => $data['tanggal_produk'][$idx],
                    'qty' => $data['qty_produk'][$idx],
                    'unit' => $data['unit_produk'][$idx],
                    'cost' => $data['amount_produk'][$idx],
                    'sn' => $data['sn'][$idx],
                    'harga_modal' => $itemRecord->produk_detail->harga_modal,
                ];
                $arrKreditSingle = [
                    'nominal' => $data['amount_produk'][$idx],
                    'dari' => 'Item Pembiayaan Single',
                    'produk_id' => $produk_id,
                    'akun' => $itemRecord->akun_persedian_id,
                    'no_transaksi' => $data['batch_no'],
                ];

                $transaksiItem = $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrKreditSingle, 0, $data['batch_no']);

                $produkDetail[] = [
                    'no_faktur' => $data['batch_no'],
                    'tanggal' => $data['date'],
                    'kuantitas' => '-'.$data['qty_produk'][$idx],
                    'harga_modal' => $itemRecord->produk_detail->harga_modal,
                    'harga_terakhir' => $itemRecord->produk_detail->harga_terakhir,
                    'gudang_id' => $itemRecord->produk_detail->gudang_id,
                    'transaksi_id' => $transaksiItem->id,
                    'sn' => $data['sn'][$idx],
                    'status' => 1,
                    'item_type' => get_class($model),
                    'item_id' => $model->id,
                    'produk_id' => $produk_id,
                    'created_at' => $data['date'],
                ];
            }

            if (array_key_exists('akun_beban_id', $data)) {
                $beban = [];
                foreach ($data['akun_beban_id'] as $idx => $akun_beban_id) {
                    $beban[] = [
                        'pembiayaan_pesanan_id' => $model->id,
                        'akun_bbn_pembiayaan_pesanan_id' => $akun_beban_id,
                        'tanggal' => $data['tanggal_beban'][$idx],
                        'catatan' => $data['notes_beban'][$idx],
                        'amount' => $data['amount_beban'][$idx],
                    ];
                    $arrKredit[] = [
                        'nominal' => $data['amount_beban'][$idx],
                        'dari' => 'Beban Pembiayaan',
                        'akun' => $akun_beban_id,
                        'no_transaksi' => $data['batch_no'],
                    ];
                }
                $this->beban->insert($beban);
            }

            $arrDebit = [
                'nominal' => $grandtotal + $data['grandtotal'],
                'dari' => 'Total Pembiayaan',
                'akun' => $data['akun_pembiayaan_pesanan_id'],
                'no_transaksi' => $data['batch_no'],
            ];

            $this->detail->insert($items);
            $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrKredit, 0, $data['batch_no']);
            $this->transaksi->ExecuteTransactionSingle('insert', $model, $arrDebit, 1, $data['batch_no']);
            $this->produkDetail->insert($produkDetail);

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function deleteSubItem($model, $id)
    {
        try {
            $this->detail->where('pembiayaan_pesanan_id', $id)->delete();
            $this->beban->where('pembiayaan_pesanan_id', $id)->delete();

            Transaksi::where(['item_id' => $model->id, 'item_type' => get_class($model)])->delete();

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function formattingArrayData($data): array
    {
        $data['date'] = date('Y-m-d H:i:s', strtotime($data['date']));
        foreach ($data['produk_id'] as $idx => $produk_id) {
            $data['amount_produk'][$idx] = floatval(preg_replace('/[^\d.]/', '', $data['amount_produk'][$idx]));
            $data['tanggal_produk'][$idx] = date('Y-m-d H:i:s', strtotime($data['tanggal_produk'][$idx]));
        }

        if (array_key_exists('akun_beban_id', $data)) {
            foreach ($data['akun_beban_id'] as $idx => $akun_beban_id) {
                $data['amount_beban'][$idx] = floatval(preg_replace('/[^\d.]/', '', $data['amount_beban'][$idx]));
                $data['tanggal_beban'][$idx] = date('Y-m-d H:i:s', strtotime($data['tanggal_beban'][$idx]));
            }
        }

        return $data;
    }
}
