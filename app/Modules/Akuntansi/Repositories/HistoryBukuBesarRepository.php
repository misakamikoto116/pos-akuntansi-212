<?php

namespace App\Modules\Akuntansi\Repositories;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\JurnalUmum;
use App\Modules\Akuntansi\Models\Pembayaran;
use App\Modules\Akuntansi\Models\PembayaranPembelian;
use App\Modules\Akuntansi\Models\PembiayaanPesanan;
use App\Modules\Akuntansi\Models\Penerimaan;
use App\Modules\Akuntansi\Models\PenerimaanPembelian;
use App\Modules\Akuntansi\Models\PenerimaanPenjualan;
use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use App\Modules\Akuntansi\Models\PenyesuaianPersediaan;
use App\Modules\Akuntansi\Models\ReturPembelian;
use App\Modules\Akuntansi\Models\ReturPenjualan;
use App\Modules\Akuntansi\Models\TipeAkun;
use App\Modules\Akuntansi\Models\Transaksi;
use Generator\Interfaces\RepositoryInterface;
use Helpers\CodeHelper;

class HistoryBukuBesarRepository implements RepositoryInterface
{
    public function __construct(Transaksi $model, Akun $akun, TipeAkun $tipe_akun, InformasiPelanggan $pelanggan, InformasiPemasok $pemasok,
                                PengirimanPenjualan $pengiriman_penjualan, FakturPenjualan $faktur_penjualan, PenerimaanPenjualan $penerimaan_penjualan, ReturPenjualan $retur_penjualan,
                                PenerimaanPembelian $penerimaan_pembelian, FakturPembelian $faktur_pembelian, PembayaranPembelian $pembayaran_pembelian, ReturPembelian $retur_pembelian,
                                JurnalUmum $jurnal_umum, Pembayaran $pembayaran, Penerimaan $penerimaan, PenyesuaianPersediaan $penyesuaian_persediaan, PembiayaanPesanan $pembiayaan_pesanan)
    {
        $this->model                        = $model;
        $this->akun                         = $akun;
        $this->tipe_akun                    = $tipe_akun;
        $this->pelanggan                    = $pelanggan;
        $this->pemasok                      = $pemasok;
        $this->pengiriman_penjualan         = $pengiriman_penjualan;
        $this->faktur_penjualan             = $faktur_penjualan;
        $this->penerimaan_penjualan         = $penerimaan_penjualan;
        $this->retur_penjualan              = $retur_penjualan;
        $this->penerimaan_pembelian         = $penerimaan_pembelian;
        $this->faktur_pembelian             = $faktur_pembelian;
        $this->pembayaran_pembelian         = $pembayaran_pembelian;
        $this->retur_pembelian              = $retur_pembelian;
        $this->jurnal_umum                  = $jurnal_umum;
        $this->pembayaran                   = $pembayaran;
        $this->penerimaan                   = $penerimaan;
        $this->penyesuaian_persediaan       = $penyesuaian_persediaan;
        $this->pembiayaan_pesanan           = $pembiayaan_pesanan;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->with('akun')->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->find($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->findItem($id)->fill($data);
        $model->save();

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $model = $this->model->fill($data);
        $model->save();
            
        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->with('akun')->filter($data)->paginate(20);
    }

    public function listAkun()
    {
        return $this->akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }

    public function listTipeAkun()
    {
        return $this->tipe_akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->title;

            return $output;
        }, []);
    }

    public function listPelanggan()
    {
        return $this->pelanggan->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listPemasok()
    {
        return $this->pemasok->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listNameSpace()
    {
        $arrayNameSpace = [
            'Pengiriman Barang'         => get_class($this->pengiriman_penjualan),
            'Faktur Penjualan'          => get_class($this->faktur_penjualan),
            'Penerimaan Penjualan'      => get_class($this->penerimaan_penjualan),
            'Retur Penjualan'           => get_class($this->retur_penjualan),
            'Penerimaan Barang'         => get_class($this->penerimaan_pembelian),
            'Faktur Pembelian'          => get_class($this->faktur_pembelian),
            'Pembayaran Pembelian'      => get_class($this->pembayaran_pembelian),
            'Retur Pembelian'           => get_class($this->retur_pembelian),
            'Jurnal Umum'               => get_class($this->jurnal_umum),
            'Pembayaran'                => get_class($this->pembayaran),
            'Penerimaan'                => get_class($this->penerimaan),
            'Penyesuaian Persediaan'    => get_class($this->penyesuaian_persediaan),
            'Pembiayaan Pesanan'        => get_class($this->pembiayaan_pesanan),
        ];

        return $arrayNameSpace;
    }
}