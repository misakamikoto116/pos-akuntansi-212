<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\PermintaanPembelian;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\KategoriProduk;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\BarangPermintaanPembelian;
use Carbon\carbon;
use Helpers\CodeHelper;
use Generator\Interfaces\RepositoryInterface;


class PermintaanPembelianRepository implements RepositoryInterface
{
    public function __construct(PermintaanPembelian $model, KategoriProduk $kategori, Gudang $gudang, Akun $akun,InformasiPelanggan $pelanggan, KodePajak $kode_pajak, Produk $produk, CodeHelper $code)
    {
        $this->model = $model;
        $this->produk = $produk;
        $this->kategori = $kategori;
        $this->gudang = $gudang;
        $this->akun = $akun;
        $this->pelanggan = $pelanggan;
        $this->kode_pajak = $kode_pajak;
        $this->code = $code;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $permintaan_pembelian = $this->model->findOrFail($id);
        if (!empty($permintaan_pembelian->request_date)) {
            $permintaan_pembelian->request_date = Carbon::parse($permintaan_pembelian->request_date)->format('d F Y');
        }

        return $permintaan_pembelian;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_permintaan_pembelian')){
            return abort(403);
        }

        $check = $this->model->findOrFail($id);
        BarangPermintaanPembelian::where('permintaan_pembelian_id', $id)->delete();  
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        if (!empty($data['request_date'])) {
            $data['request_date'] = Carbon::parse($data['request_date'])->format('Y-m-d H:i:s');
        }
        if (!empty($data['status'])) {
            $data['status'] = (empty($data['status'])) ? 0 : 1 ;
        }
        $model = $this->findItem($id)->fill($data);   
        if ($model->save()) {
            BarangPermintaanPembelian::where('permintaan_pembelian_id',$model->id)->delete();
            $this->insertSubItem($data, $model);
        }

        if(!empty($data['lanjutkan'])){
            $model['continue_stat'] = 2;                
        }    

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data['request_no'] = $this->code->autoGenerate($this->model, "request_no", $data['request_no']);
        if (!empty($data['request_date'])) {
            $data['request_date'] = Carbon::parse($data['request_date'])->format('Y-m-d H:i:s');
        }
        if (!empty($data['status'])) {
            $data['status'] = (empty($data['status'])) ? 0 : 1 ;
        }
        $model = $this->model->fill($data);
        
        if($model->save()){
            $this->insertSubItem($data, $model);
        }

        if(!empty($data['lanjutkan'])){
            $model['continue_stat'] = 2;                
        }    

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(10);
    }

    public function insertSubItem($data, $model)
    {
        $items = [];
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        $count = count($data['produk_id']);        
        for ($i=0; $i < $count; $i++) {
            $required_date = Carbon::parse($data['required_date'][$i])->format('Y-m-d H:i:s');
            $items[] = [
               'produk_id' => $data['produk_id'][$i],
                'permintaan_pembelian_id' => $model->id,
                'item_deskripsi' => $data['keterangan_produk'][$i],
                'jumlah' => $data['qty_produk'][$i],
                'item_unit' => $data['satuan_produk'][$i],
                'notes' => $data['notes'][$i],
                'required_date' => $required_date,
                'created_at' => $jam_sekarang,
                'updated_at' => $jam_sekarang,
            ];
        }
        BarangPermintaanPembelian::insert($items);
    }

    public function listKategoriProduk()
    {
        return $this->kategori->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }
    public function listGudang()
    {
        return $this->gudang->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }
    public function listAkun()
    {
        return $this->akun->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama_akun;

            return $output;
        }, []);
    }
    public function listPelanggan()
    {
        return $this->pelanggan->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }
    
    public function listProduk()
    {
        return $this->produk->where('tipe_barang','!==',3)->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->no_barang." | ".$item->keterangan;

            return $output;
        }, []);
    }

    public function idPrediction()
    {
        $data = $this->model->orderBy('id','desc')->first();
        if($data){
            return $data->id + 1;
        }
        return 1;
    }

}