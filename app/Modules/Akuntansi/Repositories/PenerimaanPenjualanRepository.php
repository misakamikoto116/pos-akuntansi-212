<?php

namespace App\Modules\Akuntansi\Repositories;

use App\Modules\Akuntansi\Models\PenerimaanPenjualan;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\InvoicePenerimaanPenjualan;
use App\Modules\Akuntansi\Models\Akun;
use Generator\Interfaces\RepositoryInterface;
use Helpers\IndonesiaDate as DateHelper;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;
use Carbon\Carbon;
use App\Modules\Akuntansi\Models\FakturPenjualan;

class PenerimaanPenjualanRepository implements RepositoryInterface
{
    public function __construct(PenerimaanPenjualan $model,CodeHelper $code, InformasiPelanggan $pelanggan, Akun $akun, DateHelper $date, TransaksiHelper $transaksi)
    {
        $this->akun      = $akun;
        $this->model     = $model;
        $this->pelanggan = $pelanggan;
        $this->date      = $date;
        $this->code      = $code;
        $this->transaksi = $transaksi;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $edit_penerimaan_penjualan = $this->model->findOrFail($id);
        $this->formattingField($edit_penerimaan_penjualan);
        return $edit_penerimaan_penjualan;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        if(!auth()->user()->hasPermissionTo('hapus_penerimaan_penjualan')){
            return abort(403);
        }
        $model = $this->model->findOrFail($id);
        $this->deleteSubItem($model);
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data   = $this->formattingAllData($data);
        $model  = $this->findItem($id)->fill($this->dateChecker($data));

        if($model->save()){
            $this->deleteSubItem($model);
            $this->insertSubItem($data, $model);
        }   

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->formattingAllData($data);
        $data['form_no'] = $this->code->autoGenerate($this->model, "form_no", $data['form_no']);
        $model = $this->model->fill($this->dateChecker($data));

        if($model->save()){
            $this->insertSubItem($data, $model);
        }

        if(!empty($data['lanjutkan'])){
            $model['continue_stat'] = 2;                
		}    
        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(20);
    }

    public function listPelanggan()
    {
        return $this->pelanggan->get()->reduce(function ($output, $item) {
            $output[$item->id] = $item->nama;

            return $output;
        }, []);
    }

    public function listAkun()
    {
        return $this->akun->whereHas('tipeAkun', function($item){
            $item->where('title','Kas/Bank');
        })->get()->reduce(function ($output, $item) {
                $output[$item->id] = $item->nama_akun;

                return $output;
            }, []);
    }

    public function listAkunDiskon()
    {
        return $this->akun->whereHas('tipeAkun', function($item){
            $item->whereNotIn('title',['Kas/Bank']);
        })->get()->reduce(function ($output, $item) {
                $output[$item->id] = $item->nama_akun;

                return $output;
            }, []);
    }

    public function idPrediction()
    {
        $data = $this->model->orderBy('id','desc')->first();
        if($data){
            return $data->id + 1;
        }
        return 1;
    }

    public function dateChecker($data)
    {
        $data['payment_date'] = (empty($data['payment_date'])) ? $data['payment_date'] = date('Y-m-d H:i:s') : $data['payment_date'] = $this->date->IndonesiaToSql($data['payment_date']); ;
        $data['cheque_date'] = (empty($data['cheque_date'])) ? $data['cheque_date'] = date('Y-m-d H:i:s') : $data['cheque_date'] = $this->date->IndonesiaToSql($data['cheque_date']); ;

        return $data;
    }

    public function insertSubItem($data, $model)
    {
        $cheque_date = $data['cheque_no'] === null ? null : Carbon::parse($data['cheque_date'])->format('Y-m-d H:i:s');
        $items = [];
        $jam_sekarang = Carbon::now()->format('Y-m-d H:i:s');
        $count = count($data['faktur_id']);
        $arrayKredit = [];
        $arrayDebit = [];
        for ($i=0; $i < $count; $i++) {
            if ($data['payment_amount'][$i] > 0 && $data['status_pay'][$i] == 1) {
                $items[] = [
                    'faktur_id'                 => $data['faktur_id'][$i],
                    'penerimaan_penjualan_id'   => $model->id,
                    'akun_disc_id'              => $data['akun_diskon_mdl'][$i],
                    'tanggal'                   => $cheque_date ?? Carbon::parse($data['payment_date'])->format('Y-m-d H:i:s'),
                    'diskon_date'               => Carbon::parse($data['diskon_date'][$i])->format('Y-m-d H:i:s'),
                    'payment_amount'            => $data['payment_amount'][$i],
                    'diskon'                    => $data['diskon'][$i],
                    'last_owing'                => $data['owing'][$i],
                    'created_at'                => $jam_sekarang,
                    'updated_at'                => $jam_sekarang,
                ];
            }
            $akunFromFaktur = FakturPenjualan::findOrFail($data['faktur_id'][$i]);
            $akunFromPenerimaan = PenerimaanPenjualan::findOrFail($model->id);
            $arrayKredit[] = [
                'nominal'       => ($data['payment_amount'][$i]) + ($data['diskon'][$i] ?? 0),
                'tanggal'       => $cheque_date,
                'dari'          => 'Payment Amount Kredit',
                'akun'          => $akunFromFaktur->akun_piutang_id
            ];           

            $arrayDebit[] = [
                'nominal'       => ($data['payment_amount'][$i]),
                'tanggal'       => $cheque_date,
                'dari'          => 'Payment Amount Debit',
                'akun'          => $akunFromPenerimaan->akun_bank_id
            ];

            if($data['akun_diskon_mdl'][$i] !== null){
                $arrayDebit[] = [
                    'nominal'       => ($data['diskon'][$i] ?? 0),
                    'tanggal'       => $cheque_date,
                    'dari'          => 'Payment Amount Debit',
                    'akun'          => $data['akun_diskon_mdl'][$i],
                ];
            }
        }
        InvoicePenerimaanPenjualan::insert($items);
        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayKredit, 0 ,$data['form_no']); #payment amount => kredit
        $this->transaksi->ExecuteTransactionMultiple('insert', $model, $arrayDebit, 1, $data['form_no']); #payment amount => kredit
        $this->UpdateTakenRecord($model,'update',$data);            
        
    }

    public function deleteSubItem($model)
    {
        $subItem =  InvoicePenerimaanPenjualan::where('penerimaan_penjualan_id', $model->id);
        $this->transaksi->ExecuteTransactionSingle('delete', $model); 
        $this->UpdateTakenRecord($subItem,'delete');
        return $subItem->delete();
        
    }

    /**
     * @param collection 
     * @param string tipe masukkan
     * @param array data tambahan bila perlu
     */

    public function UpdateTakenRecord($model, $type, $data = null)
    {
        if($type == 'update' && !empty($data)){
            FakturPenjualan::whereIn('id',$data['faktur_id'])->update(['status'=>1]);
        }else{
            $array = [];
            foreach($model->get() as $item){
                array_push($array, $item->faktur_id);
            }
            if(!empty($array)){
                FakturPenjualan::whereIn('id',$array)->update(['status'=>0]);
            }
        }
    }

    public function formattingField($model)
    {
        if (!empty($model->cheque_date)) {
            $model->cheque_date = Carbon::parse($model->cheque_date)->format('d F Y');
        }
        if (!empty($model->payment_date)) {
            $model->payment_date = Carbon::parse($model->payment_date)->format('d F Y');
        }
        // if (!empty($model->cheque_amount)) {
        //     $model->cheque_amount = number_format($model->cheque_amount, 2, ".", ",");
        // }
    }

    public function formattingAllData($data)
    {
        // data faktur
        $data['cheque_amount']               = floatval(preg_replace('/[^\d.]/', '', $data['cheque_amount']));
        
        // data produk
        if (!empty($data['faktur_id'])) {
            for ($i=0; $i < count($data['faktur_id']); $i++) { 
                $data['amount'][$i]         = floatval(preg_replace('/[^\d.]/', '', $data['amount'][$i]));
                $data['owing'][$i]          = floatval(preg_replace('/[^\d.]/', '', $data['owing'][$i]));
                $data['payment_amount'][$i] = floatval(preg_replace('/[^\d.]/', '', $data['payment_amount'][$i]));
                $data['diskon'][$i]         = floatval(preg_replace('/[^\d.]/', '', $data['diskon'][$i]));
            }
        }
        return $data;
    }
}