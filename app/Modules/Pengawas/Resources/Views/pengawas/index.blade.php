@extends('chichi_theme.layout.app')

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/table_scroll.css') }}">
@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                @include('chichi_theme.layout.filter')
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Barang</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Daftar Barang</a></li>
                    </ol>

                </div>
            </div>
<!-- END Page-Title -->

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Daftar Barang</h5>
            </div>
            <div class="card-body">
                @if($items->isEmpty())
                    <div class="alert alert-warning"> Tidak ada data. </div>
                @else
                   <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>No Barang</th>
                            <th>Barcode</th>
                            <th>Keterangan</th>
                            <th>Nama Singkat</th>
                            <th>Kuantitas</th>
                            <th>Satuan</th>
                            <th>Harga Satuan</th>
                            <th>Tipe</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $key => $item)
                            <tr>
                                <td>{{ $key + $items->firstItem() }}</td>
                                <td>{{ $item->no_barang}}</td>
                                <td>{{ $item->barcode}}</td>
                                <td class="td-nama-barang">{{ $item->keterangan}}</td>
                                <td>{{ $item->nama_singkat}}</td>
                                <td>{!! $item->updated_qty !!}</td>
                                <td>{{ $item->unit}}</td>
                                <td class="mask td-harga-jual">{{ number_format($item->harga_jual) }}</td>
                                <td>{!! $item->tipe_formatted !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                </div>
                @endif
            </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>

@endsection