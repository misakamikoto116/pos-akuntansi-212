<?php

namespace App\Modules\Pengawas\Http\Controllers;

use Generator\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    protected $viewNamespace = 'pengawas';
}
