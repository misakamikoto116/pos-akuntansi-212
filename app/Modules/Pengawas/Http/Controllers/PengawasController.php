<?php

namespace App\Modules\Pengawas\Http\Controllers;
use App\Modules\Akuntansi\Repositories\BarangRepository;
use Generator\Interfaces\RepositoryInterface;

class PengawasController extends Controller
{
    public function __construct(RepositoryInterface $model, BarangRepository $barang)
    {
        $this->role         =   'pengawas';
        $this->model        =   $model;
        $this->barang       =   $barang;
        $this->title        =   'Dashboard Pengawas Toko';
        $this->request      =   null;
        $this->requestField =   [
        ];
        $this->module_url  = '';
    }

    public function formData()
    {
        return [
            'kategori' => $this->barang->listKategoriProduk(),
            'gudang' => $this->barang->listGudang(),
            'akun' => $this->barang->listAkun(),
            'pemasok' => $this->barang->listPemasok(),
            'kodePajak' => $this->barang->listKodePajak(),
            'prefBarang' => $this->barang->listPrefBarang(),
            'gudang' => $this->barang->listGudang(),

            'tipe_barang' => collect(['0' => 'Persediaan','1' => 'Non Persediaan','2' => 'Servis','3' => 'Uang Muka','4' => 'Grup']),
            'tipe_persediaan' => collect(['0' => 'Bahan Baku','1' => 'Bahan Baku Pembantu','2' => 'Barang Setengah Jadi','3' => 'Barang Jadi','4' => 'Barang lain-lain']),
            'filter' => ['kategori','tipe_barang','tipe_persediaan'],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }
}
