<?php

namespace App\Modules\Admin\Repositories;

use App\Modules\Akuntansi\Models\User;
use App\Modules\Akuntansi\Models\Role;
use App\Modules\Akuntansi\Models\Permission;
use Carbon\Carbon;
use Generator\Interfaces\RepositoryInterface;
use Auth;

class PermissionRepository implements RepositoryInterface
{

    public function __construct(Permission $model, Role $role, User $user)
    {
        $this->model = $model;
        $this->role = $role;
        $this->user = $user;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $check = $this->model->findOrFail($id);
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data['guard_name'] = 'web';
        $model = $this->findItem($id)->fill($data);
        $model->save();

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data['guard_name'] = 'web';
        $model = $this->model->fill($data);
        if (!empty($data['roles'])) { //If one or more role is selected
            foreach ($data['roles'] as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record
                $permission = Permission::where('name', '=', $data['name'])->first(); //Match input //permission to db record
                $r->givePermissionTo($permission->name);
            }
        }
        $model->save();
        return $model;
    }

    /**e
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(10);
    }

    public function listRole()
    {
        return $this->role->get();
    }

}
