<?php

namespace App\Modules\Admin\Repositories;

use App\Modules\Akuntansi\Models\User;
use App\Modules\Akuntansi\Models\Role;
use Carbon\Carbon;
use Generator\Interfaces\RepositoryInterface;
use Auth;
class UserRepository implements RepositoryInterface
{
    protected $user;
    public function __construct(User $model, Role $role)
    {
        $this->model = $model;
        $this->role = $role;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->where('id','!=',Auth::user()->id)->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->where('id','!=',Auth::user()->id)->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $check = $this->model->findOrFail($id);
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        if($data['role'] != 'akuntansi'){
            unset( $data['buat'], $data['ubah'], $data['hapus'], $data['laporan'], $data['lihat'], $data['daftar']);
        }
        $model = $this->findItem($id);

        $this->updateUser($data, $id);

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        if($data['role'] != 'akuntansi'){
            unset( $data['buat'], $data['ubah'], $data['hapus'], $data['laporan'], $data['lihat'], $data['daftar']);
        }
        $model = $this->updateUser($data);
        return $model;
    }

    /**e
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->where('id','!=',Auth::user()->id)->filter($data);
    }

    public function listRole()
    {
        return $this->role->get()->reduce(function ($output, $item) {
            $output[$item->name] = $item->name;

            return $output;
        }, []);
    }

    public function updateUser($data, $id = null)
    {
        if($id){
            $this->permissionItem($id, $data);
            $user = $this->model->findOrFail($id);
            $user = $user->syncRoles($data['role']);
            $user = $user->update([
                'name' => $data['name'],
                'email' => $data['email'],
            ]);
            if($data['password'] == $data['passwords'] && $data['password'] != '' && $data['password'] != '' ){
                $user = $this->model->findOrFail($id)->update([
                    'password' => bcrypt($data['password'])
                ]); 
                $user = $this->model->findOrFail($id)->assignRole($data['role']);
            }
        }else{
            $user = $this->model->create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password'])
            ]);   
            $user = $user->assignRole($data['role']);
            $this->permissionItem($user->id, $data);
        }
        return $user;
    }

    public function permissionItem($user, $request){
        unset($request['name'], $request['email'], $request['password'], $request['passwords'], $request['role']);

        $userX = $this->model->findOrFail($user);
        $userX->syncPermissions();
        
        if ( isset($request['buat']) || isset($request['ubah']) || isset($request['hapus'])
        || isset($request['laporan']) || isset($request['lihat']) || isset($request['daftar']) ) {
            $userX->givePermissionTo(array_flatten($request));
        }
        return true;
    }
}