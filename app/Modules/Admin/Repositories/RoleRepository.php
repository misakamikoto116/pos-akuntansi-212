<?php

namespace App\Modules\Admin\Repositories;

use App\Modules\Akuntansi\Models\User;
use App\Modules\Akuntansi\Models\Role;
use App\Modules\Akuntansi\Models\Permission;
use Carbon\Carbon;
use Generator\Interfaces\RepositoryInterface;
use Auth;

class RoleRepository implements RepositoryInterface
{
    public function __construct(Role $model, Permission $permission, User $user)
    {
        $this->model = $model;
        $this->permission = $permission;
        $this->user = $user;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $check = $this->model->findOrFail($id);
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data['guard_name'] = 'web';
        $permissions = $data['permissions'];
        unset($data['permissions']);

        $model = $this->findItem($id)->fill($data);
        $model->save();

        $p_all = Permission::all();//Get all permissions

        foreach ($p_all as $p) {
            $model->revokePermissionTo($p); //Remove all permissions associated with role
        }

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form //permission in db
            $model->givePermissionTo($p->name);  //Assign permission to role
        }


        return $model;
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data['guard_name'] = 'web';
        $model = $this->model->fill($data);
        $model->save();
        foreach ($data['permissions'] as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail();
            //Fetch the newly created role and assign permission
            $role = Role::where('name', '=', $data['name'])->first();
            $role->givePermissionTo($p->name);
        }

        return $model;
    }

    /**e
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->paginate(10);
    }

    public function listPermission()
    {
        return $this->permission->get();
    }
}
