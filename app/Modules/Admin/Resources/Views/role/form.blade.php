<div class="form-group row">
	{!! Form::label('name','Nama',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
		</div>
</div>
<div class="form-group row">
	{!! Form::label('permission','Masukkan Permission',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
		@foreach ($permissions as $permission) 
			<label> 
				{{ Form::checkbox('permissions[]',  $permission->id, $item->permissions ?? null ) }}
				{{ ucfirst($permission->name) }}
			</label>
			<br>
		@endforeach
	</div>
</div>