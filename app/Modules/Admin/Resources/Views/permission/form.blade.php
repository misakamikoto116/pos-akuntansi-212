<div class="form-group row">
	{!! Form::label('name','Nama',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
		</div>
</div>
@if(!$roles->isEmpty() && empty($item))
<div class="form-group row">
	{!! Form::label('role','Masukkan Permission ke Role',['class' => 'col-2 col-form-label']) !!}
	<div class="col-10">
		@foreach ($roles as $role) 
			<label> 
				{{ Form::checkbox('roles[]',  $role->id ) }}
				{{ ucfirst($role->name) }}
			</label>
			<br>
		@endforeach
	</div>
</div>
@endif