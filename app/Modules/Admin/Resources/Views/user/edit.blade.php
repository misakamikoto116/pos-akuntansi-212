@extends('chichi_theme.layout.app')

@section('custom_css')
    <style>
        .tab-border{
            border-top: 1px solid rgba(97,135,136,0.3);
            border-left: 1px solid rgba(97,135,136,0.3);
            border-right: 1px solid rgba(97,135,136,0.3);
            background: crimson;
        }

        .tab-padding{
            padding: 30px 5px;
        }

        .tab-padding-cek{
            padding: 20px 15px;
        }
    </style>
@endsection

@section('content')

<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <div class="warn_pass">
                        <div class="container">
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <i class="fa fa-exclamation-circle"></i> Password tidak sesuai
                            </div>
                        </div>
                    </div>
                    <h4 class="page-title">{{$title}}</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="{{ route('admin.user.index') }}">Daftar user</a></li>
                      <li><a href="#">Edit user</a></li>
                    </ol>

                </div>
            </div>


<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::model($item,['route' => [$module_url->update,$item->id],'method' => 'Put','class' => 'form-horizontal form-label-left']) !!}
                @include($form)
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Perbarui',['type' => 'submit','class' => 'btn btn-default btn-submit']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("custom_js")
  <script src="{{ asset('js/user.js') }}"></script>
  {{-- <script>
      $("#checkAll").click(function(){
        $('input[type="checkbox"]').not(this).prop('checked', this.checked);
    });
  </script> --}}
@endsection