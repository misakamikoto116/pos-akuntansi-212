<div class="form-group row">
	{!! Form::label('name','Nama',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
		</div>
</div>
<div class="form-group row">
	{!! Form::label('email','Email',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
			{!! Form::email('email', null, ['class' => 'form-control']) !!}
		</div>
</div>
<div class="form-group row">
	{!! Form::label('password','Password',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
			{!! Form::password('password', ['class' => 'form-control password']) !!}
		</div>
</div>
<div class="form-group row">
	{!! Form::label('passwords','Konfirmasi Password',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
			{!! Form::password('passwords', ['class' => 'form-control password2']) !!}
		<small>* Kosongkan password apabila tidak diubah</small>
		</div>
</div>

<div class="form-group row">
	{!! Form::label('role','Role/Peran',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
			{!! Form::select('role', $role, isset($item) ? $item->roles()->pluck('name')->first() : null, ['class' => 'form-control select2']) !!}
		</div>
</div>

<div id="list-role" >
	<!-- Form Tab -->
	<ul class="nav nav-pills" role="tablist" style="background: #f3f3f3 !important; border-bottom: 1px solid rgba(97,135,136,0.3);">
		<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab-penjualan">Penjualan</a></li>
		<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab-pembelian">Pembelian</a></li>
		<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab-baja">Barang, Jasa dan Persediaan</a></li>
		<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab-buku-besar-lain-lain">Buku Besar & Lain-lain</a></li>
		<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab-bank-dan-aktiva-tetap">Bank dan Aktiva Tetap</a></li>
		{{-- <li class="nav-item">
			{!! Form::checkbox('cek', 'cekAll', null, ['id' => 'checkAll']) !!}
		</li> --}}
	</ul>
	<div class="tab-content">
		<!-- Tab Penjualan  -->
		<div class="tab-pane show active" id="tab-penjualan">
			@include('admin::user.form_permission.tab_penjualan')
		</div>
		<!-- Tab Pembelian -->
		<div class="tab-pane fade" id="tab-pembelian">
			@include('admin::user.form_permission.tab_pembelian')
		</div>
		<!-- Tab Baja (Barang dan Jasa) -->
		<div class="tab-pane fade" id="tab-baja">
			@include('admin::user.form_permission.tab_barang_jasa')
		</div>
		<!-- Tab Buku Besar dan Lain-lain -->
		<div class="tab-pane fade" id="tab-buku-besar-lain-lain">
			@include('admin::user.form_permission.tab_buku_besar')
		</div>
		<!-- Tab Bank dan Aktiva Tetap -->
		<div class="tab-pane fade" id="tab-bank-dan-aktiva-tetap">
			@include('admin::user.form_permission.tab_bank_aktiva')
		</div>
	</div>
</div>