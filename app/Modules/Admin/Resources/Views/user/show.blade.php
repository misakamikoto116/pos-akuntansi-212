@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Transaksi Akun</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Daftar Transaksi {{ $item->nama_akun }}</a></li>
                    </ol>

                </div>
            </div>
<!-- END Page-Title -->

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Daftar Transaksi {{ $item->nama_akun }}</h5>
            <div class="menu-header">
            </div>
            </div>
            <div class="card-body">
                 @if($transaksi->isEmpty())
                    <div class="alert alert-warning"> Tidak ada data. </div>
                 @else
                   <table style="width: 100%" class="table table-bordered">
                    <tr>
                        <th width="1%">No</th>
                        <th>Tanggal</th>
                        <th>Sumber</th>
                        <th>No. Sumber</th>
                        <th>Nama Akun</th>
                        <th>Keterangan</th>
                        <th width="20%">Debit</th>
                        <th width="20%">Kredit</th>
                        <th width="10%">Opsi</th>
                    </tr>
                @foreach ($transaksi as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->tanggal_formatted }}</td>
                        <td>{{ $item->sumber }}</td>
                        <td>{{ $item->no_transaksi }}</td>
                        <td>{{ $item->akun->nama_akun }}</td>
                        <td>{{ $item->keterangan }}</td>
                        <td>@if($item->status == 1) Rp. {{ number_format($item->nominal)}} @else 0 @endif</td>
                        <td>@if($item->status == 0) Rp. {{ number_format($item->nominal)}} @else 0 @endif</td>
                        <td>
                            <a href="{{ $item->generate_link }}" class="btn btn-info">Sumber</a>
                        </td>
                    </tr>
                @endforeach
                </table>
                <div class="pull-right">
                    {!! $transaksi->links('vendor.pagination.bootstrap-4'); !!}
                </div>
                @endif
            </div>

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection
@section('custom_js')
    <script type="text/javascript">
        $('.mask').autoNumeric('init');
    </script>
@endsection