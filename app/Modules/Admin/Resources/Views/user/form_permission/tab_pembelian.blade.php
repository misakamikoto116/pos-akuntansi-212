<div class="tab-padding">
    <div class="form-group row">
        <div class="col col-1" style="margin: 0px 10px;">Buat</div>
        <div class="col col-1" style="margin: 0px 10px;">Ubah</div>
        <div class="col col-1" style="margin: 0px 10px;">Hapus</div>
        <div class="col col-1" style="margin: 0px 10px;">Lapoan</div>
        <div class="col col-1" style="margin: 0px 10px;">Lihat</div>
        <div class="col col-1" style="margin: 0px 10px;">Daftar</div>
    </div>
    <div class="form-group row">	<!-- checkbox Aktivitas Permintaan -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_permintaan_pembelian', isset($item) ? $item->hasPermissionTo('buat_permintaan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_permintaan_pembelian', isset($item) ? $item->hasPermissionTo('ubah_permintaan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_permintaan_pembelian', isset($item) ? $item->hasPermissionTo('hapus_permintaan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_permintaan_pembelian', isset($item) ? $item->hasPermissionTo('laporan_permintaan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_permintaan_pembelian', isset($item) ? $item->hasPermissionTo('lihat_permintaan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_permintaan_pembelian', isset($item) ? $item->hasPermissionTo('daftar_permintaan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktivitas_permintaan_pembelian', 'Aktivitas Permintaan Pembelian', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Aktivitas Pesanan -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_pesanan_pembelian', isset($item) ? $item->hasPermissionTo('buat_pesanan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_pesanan_pembelian', isset($item) ? $item->hasPermissionTo('ubah_pesanan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_pesanan_pembelian', isset($item) ? $item->hasPermissionTo('hapus_pesanan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_pesanan_pembelian', isset($item) ? $item->hasPermissionTo('laporan_pesanan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_pesanan_pembelian', isset($item) ? $item->hasPermissionTo('lihat_pesanan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_pesanan_pembelian', isset($item) ? $item->hasPermissionTo('daftar_pesanan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktivitas_pesanan_pembelian', 'Aktivitas Pesanan Pembelian', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Aktivitas Penerimaan -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_penerimaan_pembelian', isset($item) ? $item->hasPermissionTo('buat_penerimaan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_penerimaan_pembelian', isset($item) ? $item->hasPermissionTo('ubah_penerimaan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_penerimaan_pembelian', isset($item) ? $item->hasPermissionTo('hapus_penerimaan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_penerimaan_pembelian', isset($item) ? $item->hasPermissionTo('laporan_penerimaan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_penerimaan_pembelian', isset($item) ? $item->hasPermissionTo('lihat_penerimaan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_penerimaan_pembelian', isset($item) ? $item->hasPermissionTo('daftar_penerimaan_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktivitas_penerimaan_pembelian', 'Aktivitas Penerimaan Pembelian', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Aktivitas Faktur Pembelian -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_faktur_pembelian', isset($item) ? $item->hasPermissionTo('buat_faktur_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_faktur_pembelian', isset($item) ? $item->hasPermissionTo('ubah_faktur_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_faktur_pembelian', isset($item) ? $item->hasPermissionTo('hapus_faktur_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_faktur_pembelian', isset($item) ? $item->hasPermissionTo('laporan_faktur_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_faktur_pembelian', isset($item) ? $item->hasPermissionTo('lihat_faktur_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_faktur_pembelian', isset($item) ? $item->hasPermissionTo('daftar_faktur_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktivitas_faktur_pembelian', 'Aktivitas Faktur Pembelian', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Aktivitas Pembayaran Pembelian -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_pembayaran_pembelian', isset($item) ? $item->hasPermissionTo('buat_pembayaran_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_pembayaran_pembelian', isset($item) ? $item->hasPermissionTo('ubah_pembayaran_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_pembayaran_pembelian', isset($item) ? $item->hasPermissionTo('hapus_pembayaran_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_pembayaran_pembelian', isset($item) ? $item->hasPermissionTo('laporan_pembayaran_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_pembayaran_pembelian', isset($item) ? $item->hasPermissionTo('lihat_pembayaran_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_pembayaran_pembelian', isset($item) ? $item->hasPermissionTo('daftar_pembayaran_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktivitas_pembayaran_pembelian', 'Aktivitas Pembayaran Pembelian', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Aktivitas Retur Pembelian -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_retur_pembelian', isset($item) ? $item->hasPermissionTo('buat_retur_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_retur_pembelian', isset($item) ? $item->hasPermissionTo('ubah_retur_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_retur_pembelian', isset($item) ? $item->hasPermissionTo('hapus_retur_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_retur_pembelian', isset($item) ? $item->hasPermissionTo('laporan_retur_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_retur_pembelian', isset($item) ? $item->hasPermissionTo('lihat_retur_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_retur_pembelian', isset($item) ? $item->hasPermissionTo('daftar_retur_pembelian') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktivitas_retur_pembelian', 'Aktivitas Retur Pembelian', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
</div>