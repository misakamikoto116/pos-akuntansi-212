<div class="tab-padding">
    <div class="form-group row">
        <div class="col col-1" style="margin: 0px 10px;">Buat</div>
        <div class="col col-1" style="margin: 0px 10px;">Ubah</div>
        <div class="col col-1" style="margin: 0px 10px;">Hapus</div>
        <div class="col col-1" style="margin: 0px 10px;">Lapoan</div>
        <div class="col col-1" style="margin: 0px 10px;">Lihat</div>
        <div class="col col-1" style="margin: 0px 10px;">Daftar</div>
    </div>
    {{-- {{dd($item->hasPermissionTo('daftar_penawaran_penjualan') === true ? 'checked' : '')}} --}}
    <div class="form-group row">	<!-- checkbox Aktivitas Penawaran -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_penawaran_penjualan', isset($item) ? $item->hasPermissionTo('buat_penawaran_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_penawaran_penjualan', isset($item) ? $item->hasPermissionTo('ubah_penawaran_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_penawaran_penjualan', isset($item) ? $item->hasPermissionTo('hapus_penawaran_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_penawaran_penjualan', isset($item) ? $item->hasPermissionTo('laporan_penawaran_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_penawaran_penjualan', isset($item) ? $item->hasPermissionTo('lihat_penawaran_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_penawaran_penjualan', isset($item) ? $item->hasPermissionTo('daftar_penawaran_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktivitas_penawaran_penjualan', 'Aktivitas Penawaran Penjualan', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Aktivitas Pesanan -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_pesanan_penjualan', isset($item) ? $item->hasPermissionTo('buat_pesanan_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_pesanan_penjualan', isset($item) ? $item->hasPermissionTo('ubah_pesanan_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_pesanan_penjualan', isset($item) ? $item->hasPermissionTo('hapus_pesanan_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_pesanan_penjualan', isset($item) ? $item->hasPermissionTo('laporan_pesanan_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_pesanan_penjualan', isset($item) ? $item->hasPermissionTo('lihat_pesanan_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_pesanan_penjualan', isset($item) ? $item->hasPermissionTo('daftar_pesanan_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktivitas_pesanan_penjualan', 'Aktivitas Pesanan Penjualan', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Aktivitas Pengiriman -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_pengiriman_penjualan', isset($item) ? $item->hasPermissionTo('buat_pengiriman_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_pengiriman_penjualan', isset($item) ? $item->hasPermissionTo('ubah_pengiriman_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_pengiriman_penjualan', isset($item) ? $item->hasPermissionTo('hapus_pengiriman_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_pengiriman_penjualan', isset($item) ? $item->hasPermissionTo('laporan_pengiriman_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_pengiriman_penjualan', isset($item) ? $item->hasPermissionTo('lihat_pengiriman_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_pengiriman_penjualan', isset($item) ? $item->hasPermissionTo('daftar_pengiriman_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktivitas_pengiriman_penjualan', 'Aktivitas Pengiriman Pesanan', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Aktivitas Faktur -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_faktur_penjualan', isset($item) ? $item->hasPermissionTo('buat_faktur_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_faktur_penjualan', isset($item) ? $item->hasPermissionTo('ubah_faktur_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_faktur_penjualan', isset($item) ? $item->hasPermissionTo('hapus_faktur_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_faktur_penjualan', isset($item) ? $item->hasPermissionTo('laporan_faktur_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_faktur_penjualan', isset($item) ? $item->hasPermissionTo('lihat_faktur_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_faktur_penjualan', isset($item) ? $item->hasPermissionTo('daftar_faktur_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktivitas_faktur_penjualan', 'Aktivitas Faktur Penjualan', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Aktivitas Penerimaan -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_penerimaan_penjualan', isset($item) ? $item->hasPermissionTo('buat_penerimaan_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_penerimaan_penjualan', isset($item) ? $item->hasPermissionTo('ubah_penerimaan_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_penerimaan_penjualan', isset($item) ? $item->hasPermissionTo('hapus_penerimaan_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_penerimaan_penjualan', isset($item) ? $item->hasPermissionTo('laporan_penerimaan_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_penerimaan_penjualan', isset($item) ? $item->hasPermissionTo('lihat_penerimaan_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_penerimaan_penjualan', isset($item) ? $item->hasPermissionTo('daftar_penerimaan_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktivitas_penerimaan_penjualan', 'Aktivitas Penerimaan Penjualan', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Aktivitas Retur -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_retur_penjualan', isset($item) ? $item->hasPermissionTo('buat_retur_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_retur_penjualan', isset($item) ? $item->hasPermissionTo('ubah_retur_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_retur_penjualan', isset($item) ? $item->hasPermissionTo('hapus_retur_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_retur_penjualan', isset($item) ? $item->hasPermissionTo('laporan_retur_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_retur_penjualan', isset($item) ? $item->hasPermissionTo('lihat_retur_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_retur_penjualan', isset($item) ? $item->hasPermissionTo('daftar_retur_penjualan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktivitas_retur_penjualan', 'Aktivitas Retur Penjualan', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
</div>