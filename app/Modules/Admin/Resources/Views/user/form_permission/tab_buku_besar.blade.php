<div class="tab-padding">
    <div class="form-group row">
        <div class="col col-1" style="margin: 0px 10px;">Buat</div>
        <div class="col col-1" style="margin: 0px 10px;">Ubah</div>
        <div class="col col-1" style="margin: 0px 10px;">Hapus</div>
        <div class="col col-1" style="margin: 0px 10px;">Lapoan</div>
        <div class="col col-1" style="margin: 0px 10px;">Lihat</div>
        <div class="col col-1" style="margin: 0px 10px;">Daftar</div>
    </div>
    <div class="form-group row">	<!-- checkbox Daftar Akun -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]','buat_daftar_akun', isset($item) ? $item->hasPermissionTo('buat_daftar_akun') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]','ubah_daftar_akun', isset($item) ? $item->hasPermissionTo('ubah_daftar_akun') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]','hapus_daftar_akun', isset($item) ? $item->hasPermissionTo('hapus_daftar_akun') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]','laporan_daftar_akun', isset($item) ? $item->hasPermissionTo('laporan_daftar_akun') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;"></div>
        <div class="col col-1" style="margin: 0px 10px;"></div>
        <div class="col">
            {!! Form::label('label_daftar_akun', 'Daftar Akun', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Bukti Jurnal Umum -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]','buat_bukti_jurnal_umum', isset($item) ? $item->hasPermissionTo('buat_bukti_jurnal_umum') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]','ubah_bukti_jurnal_umum', isset($item) ? $item->hasPermissionTo('ubah_bukti_jurnal_umum') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]','hapus_bukti_jurnal_umum', isset($item) ? $item->hasPermissionTo('hapus_bukti_jurnal_umum') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]','laporan_bukti_jurnal_umum', isset($item) ? $item->hasPermissionTo('laporan_bukti_jurnal_umum') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]','lihat_bukti_jurnal_umum', isset($item) ? $item->hasPermissionTo('lihat_bukti_jurnal_umum') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]','daftar_bukti_jurnal_umum', isset($item) ? $item->hasPermissionTo('daftar_bukti_jurnal_umum') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_bukti_jurnal_umum', 'Bukti Jurnal Umum', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Data Pelanggan -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]','buat_data_pelanggan', isset($item) ? $item->hasPermissionTo('buat_data_pelanggan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]','ubah_data_pelanggan', isset($item) ? $item->hasPermissionTo('ubah_data_pelanggan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]','hapus_data_pelanggan', isset($item) ? $item->hasPermissionTo('hapus_data_pelanggan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]','laporan_data_pelanggan', isset($item) ? $item->hasPermissionTo('laporan_data_pelanggan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;"></div>
        <div class="col col-1" style="margin: 0px 10px;"></div>
        <div class="col">
            {!! Form::label('label_data_pelanggan', 'Data Pelanggan', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Data Pemasok -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]','buat_data_pemasok', isset($item) ? $item->hasPermissionTo('buat_data_pemasok') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]','ubah_data_pemasok', isset($item) ? $item->hasPermissionTo('ubah_data_pemasok') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]','hapus_data_pemasok', isset($item) ? $item->hasPermissionTo('hapus_data_pemasok') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]','laporan_data_pemasok', isset($item) ? $item->hasPermissionTo('laporan_data_pemasok') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]','lihat_data_pemasok', isset($item) ? $item->hasPermissionTo('lihat_data_pemasok') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]','daftar_data_pemasok', isset($item) ? $item->hasPermissionTo('daftar_data_pemasok') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_data_pemasok', 'Data Pemasok', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Laporan Keuangan -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]','buat_laporan_keuangan', isset($item) ? $item->hasPermissionTo('buat_laporan_keuangan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]','ubah_laporan_keuangan', isset($item) ? $item->hasPermissionTo('ubah_laporan_keuangan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]','hapus_laporan_keuangan', isset($item) ? $item->hasPermissionTo('hapus_laporan_keuangan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]','laporan_laporan_keuangan', isset($item) ? $item->hasPermissionTo('laporan_laporan_keuangan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]','lihat_laporan_keuangan', isset($item) ? $item->hasPermissionTo('lihat_laporan_keuangan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]','daftar_laporan_keuangan', isset($item) ? $item->hasPermissionTo('daftar_laporan_keuangan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_laporan_keuanganlaporan', 'Laporan Keuangan', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
</div>