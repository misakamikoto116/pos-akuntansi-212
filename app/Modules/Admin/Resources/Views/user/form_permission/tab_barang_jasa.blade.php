<div class="tab-padding">
    <div class="form-group row">
        <div class="col col-1" style="margin: 0px 10px;">Buat</div>
        <div class="col col-1" style="margin: 0px 10px;">Ubah</div>
        <div class="col col-1" style="margin: 0px 10px;">Hapus</div>
        <div class="col col-1" style="margin: 0px 10px;">Lapoan</div>
        <div class="col col-1" style="margin: 0px 10px;">Lihat</div>
        <div class="col col-1" style="margin: 0px 10px;">Daftar</div>
    </div>
    <div class="form-group row">	<!-- checkbox Barang -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]','buat_barang', isset($item) ? $item->hasPermissionTo('buat_barang') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]','ubah_barang', isset($item) ? $item->hasPermissionTo('ubah_barang') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]','hapus_barang', isset($item) ? $item->hasPermissionTo('hapus_barang') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]','laporan_barang', isset($item) ? $item->hasPermissionTo('laporan_barang') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;"></div>
        <div class="col col-1" style="margin: 0px 10px;"></div>
        <div class="col">
            {!! Form::label('label_barang', 'Barang', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Gudang -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]','buat_gudang', isset($item) ? $item->hasPermissionTo('buat_gudang') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]','ubah_gudang', isset($item) ? $item->hasPermissionTo('ubah_gudang') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]','hapus_gudang', isset($item) ? $item->hasPermissionTo('hapus_gudang') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]','laporan_gudang', isset($item) ? $item->hasPermissionTo('laporan_gudang') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;"></div>
        <div class="col col-1" style="margin: 0px 10px;"></div>
        <div class="col">
            {!! Form::label('label_gudang', 'Gudang', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Pindah Barang -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]','buat_pindah_barang', isset($item) ? $item->hasPermissionTo('buat_pindah_barang') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]','ubah_pindah_barang', isset($item) ? $item->hasPermissionTo('ubah_pindah_barang') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]','hapus_pindah_barang', isset($item) ? $item->hasPermissionTo('hapus_pindah_barang') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]','laporan_pindah_barang', isset($item) ? $item->hasPermissionTo('laporan_pindah_barang') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;"></div>
        <div class="col col-1" style="margin: 0px 10px;"></div>
        <div class="col">
            {!! Form::label('label_pindah_barang', 'Pindah Barang', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Pembiayaan Pesanan -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]','buat_pembiayaan_pesanan', isset($item) ? $item->hasPermissionTo('buat_pembiayaan_pesanan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]','ubah_pembiayaan_pesanan', isset($item) ? $item->hasPermissionTo('ubah_pembiayaan_pesanan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]','hapus_pembiayaan_pesanan', isset($item) ? $item->hasPermissionTo('hapus_pembiayaan_pesanan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]','laporan_pembiayaan_pesanan', isset($item) ? $item->hasPermissionTo('laporan_pembiayaan_pesanan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]','lihat_pembiayaan_pesanan', isset($item) ? $item->hasPermissionTo('lihat_pembiayaan_pesanan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]','daftar_pembiayaan_pesanan', isset($item) ? $item->hasPermissionTo('daftar_pembiayaan_pesanan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_pembiayaan_pesanan', 'Pembiayaan Pesanan', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Penyesuaian Persediaan -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]','buat_penyesuaian_persediaan', isset($item) ? $item->hasPermissionTo('buat_penyesuaian_persediaan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]','ubah_penyesuaian_persediaan', isset($item) ? $item->hasPermissionTo('ubah_penyesuaian_persediaan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]','hapus_penyesuaian_persediaan', isset($item) ? $item->hasPermissionTo('hapus_penyesuaian_persediaan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]','laporan_penyesuaian_persediaan', isset($item) ? $item->hasPermissionTo('laporan_penyesuaian_persediaan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]','lihat_penyesuaian_persediaan', isset($item) ? $item->hasPermissionTo('lihat_penyesuaian_persediaan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]','daftar_penyesuaian_persediaan', isset($item) ? $item->hasPermissionTo('daftar_penyesuaian_persediaan') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_penyesuaian_persediaan', 'Penyesuaian Persediaan', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Selling Price Adjustment -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]','buat_selling_price_adjusment', isset($item) ? $item->hasPermissionTo('buat_selling_price_adjusment') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]','ubah_selling_price_adjusment', isset($item) ? $item->hasPermissionTo('ubah_selling_price_adjusment') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]','hapus_selling_price_adjusment', isset($item) ? $item->hasPermissionTo('hapus_selling_price_adjusment') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]','laporan_selling_price_adjusment', isset($item) ? $item->hasPermissionTo('laporan_selling_price_adjusment') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]','lihat_selling_price_adjusment', isset($item) ? $item->hasPermissionTo('lihat_selling_price_adjusment') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]','daftar_selling_price_adjusment', isset($item) ? $item->hasPermissionTo('daftar_selling_price_adjusment') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_selling_price_adjusment', 'Selling Price Adjustment', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
</div>