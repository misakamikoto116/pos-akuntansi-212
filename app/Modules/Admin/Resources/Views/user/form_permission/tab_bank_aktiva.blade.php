<div class="tab-padding">
    <div class="form-group row">
        <div class="col col-1" style="margin: 0px 10px;">Buat</div>
        <div class="col col-1" style="margin: 0px 10px;">Ubah</div>
        <div class="col col-1" style="margin: 0px 10px;">Hapus</div>
        <div class="col col-1" style="margin: 0px 10px;">Lapoan</div>
        <div class="col col-1" style="margin: 0px 10px;">Lihat</div>
        <div class="col col-1" style="margin: 0px 10px;">Daftar</div>
    </div>
    <div class="form-group row">	<!-- checkbox Pembayaran Lain -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_pembayaran_lain', isset($item) ? $item->hasPermissionTo('buat_pembayaran_lain') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_pembayaran_lain', isset($item) ? $item->hasPermissionTo('ubah_pembayaran_lain') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_pembayaran_lain', isset($item) ? $item->hasPermissionTo('hapus_pembayaran_lain') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_pembayaran_lain', isset($item) ? $item->hasPermissionTo('laporan_pembayaran_lain') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_pembayaran_lain', isset($item) ? $item->hasPermissionTo('lihat_pembayaran_lain') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_pembayaran_lain', isset($item) ? $item->hasPermissionTo('daftar_pembayaran_lain') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_pembayaran_lain', 'Pembayaran Lain', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Penerimaan Lain -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_penerimaan_lain', isset($item) ? $item->hasPermissionTo('buat_penerimaan_lain') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_penerimaan_lain', isset($item) ? $item->hasPermissionTo('ubah_penerimaan_lain') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_penerimaan_lain', isset($item) ? $item->hasPermissionTo('hapus_penerimaan_lain') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_penerimaan_lain', isset($item) ? $item->hasPermissionTo('laporan_penerimaan_lain') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_penerimaan_lain', isset($item) ? $item->hasPermissionTo('lihat_penerimaan_lain') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_penerimaan_lain', isset($item) ? $item->hasPermissionTo('daftar_penerimaan_lain') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_penerimaan_lain', 'Penerimaan Lain', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Aktiva Tetap -->
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_aktiva_tetap', isset($item) ? $item->hasPermissionTo('buat_aktiva_tetap') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('ubah[]', 'ubah_aktiva_tetap', isset($item) ? $item->hasPermissionTo('ubah_aktiva_tetap') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('hapus[]', 'hapus_aktiva_tetap', isset($item) ? $item->hasPermissionTo('hapus_aktiva_tetap') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('laporan[]', 'laporan_aktiva_tetap', isset($item) ? $item->hasPermissionTo('laporan_aktiva_tetap') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('lihat[]', 'lihat_aktiva_tetap', isset($item) ? $item->hasPermissionTo('lihat_aktiva_tetap') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col col-1" style="margin: 0px 10px;">
            {!! Form::checkbox('daftar[]', 'daftar_aktiva_tetap', isset($item) ? $item->hasPermissionTo('daftar_aktiva_tetap') === true ? 'checked' : '' : '') !!}
        </div>
        <div class="col">
            {!! Form::label('label_aktiva_tetap', 'Aktiva Tetap', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Rekonsiliasi Bank -->
        <div class="col col-12" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_rekonsiliasi_bank', isset($item) ? $item->hasPermissionTo('buat_rekonsiliasi_bank') === true ? 'checked' : '' : '') !!}
            {!! Form::label('label_rekonsiliasi_bank', 'Rekonsiliasi Bank', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
    <div class="form-group row">	<!-- checkbox Menampilkan dan Cetak Buku Bank -->
        <div class="col col-12" style="margin: 0px 10px;">
            {!! Form::checkbox('buat[]', 'buat_menampilkan_cetak_buku_bank', isset($item) ? $item->hasPermissionTo('buat_menampilkan_cetak_buku_bank') === true ? 'checked' : '' : '') !!}
            {!! Form::label('label_menampilkan_cetak_buku_bank', 'Menampilkan dan Cetak Buku Bank', ['class' => 'col-6 col-form-label', 'style' => 'margin: 0px 10px;']) !!}
        </div>
    </div>
</div>