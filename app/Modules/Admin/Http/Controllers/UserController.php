<?php

namespace App\Modules\Admin\Http\Controllers;
use App\Modules\Akuntansi\Models\User;
use App\Modules\Admin\Http\Requests\UserRequest;
use App\Modules\Admin\Repositories\UserRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'admin';
        $this->model = $model;
        $this->title = 'Data User';
        $this->request = UserRequest::class;
        $this->requestField = [
            'name',
            'email',
            'password',
            'passwords',
            'role',
            // Permission ?
            'buat',
            'ubah',
            'hapus',
            'laporan',
            'lihat',
            'daftar'
        ];
    }

    public function formData()
    {
        return [
            'role' => $this->model->listRole(),
            'filter' => ['role']
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }

}
