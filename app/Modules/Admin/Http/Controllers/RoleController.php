<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Modules\Akuntansi\Models\Role;
use App\Modules\Admin\Http\Requests\RoleRequest;
use App\Modules\Admin\Repositories\RoleRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'admin';
        $this->model = $model;
        $this->title = 'Role';
        $this->request = RoleRequest::class;
        $this->requestField = [
            'name',
            'permissions'
        ];
    }

    public function formData()
    {
        return [
            'permissions' => $this->model->listPermission(),
            'filter' => []
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }
}
