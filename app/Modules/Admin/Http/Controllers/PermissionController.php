<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Modules\Akuntansi\Models\Permission;
use App\Modules\Admin\Http\Requests\PermissionRequest;
use App\Modules\Admin\Repositories\PermissionRepository;
use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'admin';
        $this->model = $model;
        $this->title = 'Permission';
        $this->request = PermissionRequest::class;
        $this->requestField = [
            'name',
            'roles'
        ];
    }

    public function formData()
    {
        return [
            'roles' => $this->model->listRole(),
            'filter' => []
        ];
    }

    public function index()
    {
        return parent::index()->with($this->formData());
    }
}
