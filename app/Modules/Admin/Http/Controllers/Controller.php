<?php

namespace App\Modules\Admin\Http\Controllers;

use  Generator\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
   protected $viewNamespace = 'admin';
}