<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecordApprovedBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('pesanan_penjualan', 'approved')) {
            Schema::table('pesanan_penjualan', function (Blueprint $table) {
                $table->tinyInteger('approved_by')->comment('0 => Kosong, 1 => approved_id')->after('akun_dp_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesanan_penjualan', function (Blueprint $table) {
            //
        });
    }
}
