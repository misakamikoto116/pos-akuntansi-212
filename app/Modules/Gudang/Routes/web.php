<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function(){
    return redirect(url('/gudang/dashboard'));
});
Route::get('/gudang', function(){
    return redirect(url('/gudang/dashboard'));
});
Route::group(['prefix' => 'gudang', 'as' => 'gudang.', 'middleware' => 'auth'], function () {
    Route::resource('dashboard', 'DashboardController');
    Route::resource('daftar-gudang', 'DaftarGudangController');
    Route::resource('barang-pergudang', 'BarangPergudangController');
    Route::resource('kategori-produk', 'KategoriProdukController');
    Route::resource('pemindahan-barang-gudang', 'PemindahanBarangGudangController');
});
