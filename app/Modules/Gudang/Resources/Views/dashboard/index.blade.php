@extends('chichi_theme.layout.app')

@section('custom_css')
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Dashboard Pesanan dari Toko</h4>
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Dashboard</a></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">Dashboard</h5>
                </div>
                <div class="card-body">
                    @if($items->isEmpty())
                        <div class="alert alert-warning"> Tidak ada data Pesanan dari Toko. </div>
                    @else
                        <div id="notifMe"></div>
                        <table class="table">
                            <tr>
                                <td>No</td>
                                <td>Nama Pelanggan</td>
                                <td>Alamat Pelanggan</td>
                                <td>No Penawaran Penjualan</td>
                                <td>No Pesanan Penjualan</td>
                                <td></td>
                            </tr>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{  $loop->iteration            }}</td>
                                    <td>{{  $item->pelanggan->nama      }}</td>
                                    <td>{{  $item->pelanggan->alamat    }}</td>
                                    <td>{{  $item->po_number            }}</td>
                                    <td>{{  $item->so_number            }}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#ModalPesanan-{{$item->id}}"> Detail</button>
                                    </td>
                                </tr>
                                <tbody class="tbodyPesanan" style="border: none !important;">
                                    <tr>
                                        <td style="border: none !important;">
                                            <div class="modal fade" id="ModalPesanan-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="ModalPesananLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                            <h4 class="modal-title" id="ModalPesananLabel">Detail Pesanan POS : {{ $item->id }}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table class="table">
                                                                <tr>
                                                                    <td>Nama Barang</td>
                                                                    <td>Kuantitas</td>
                                                                    <td>Satuan</td>
                                                                </tr>
                                                                    @foreach ($item->barang as $barang)
                                                                        <tr class="dupBarang">
                                                                            <td>{{ $barang->item_deskripsi }}</td>
                                                                            <td>{{ $barang->jumlah }}</td>
                                                                            <td>{{ $barang->satuan }}</td>
                                                                        </tr>
                                                                    @endforeach
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <span class="pull-right">
                                                                    {!! Form::model($item,['route' => [$module_url->update,$item->id], 'method' => 'Put']) !!}
                                                                        <div class="submit">
                                                                            {!! Form::button('<i class="fa fa-check"></i> Simpan',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit']) !!}
                                                                        </div>
                                                                    {!! Form::close() !!}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            @endforeach
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>    
@endsection

@section('custom_js')
    <script>
        // var GUDANG = "{!! 'gudang_'.auth()->user()->id !!}";
        // console.log(GUDANG);
        // socket.on(GUDANG, function (data) {
        //     $.Notification.autoHideNotify('custom', 'top right', "Pesan", data);
        // });
    </script>
@endsection