@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title"></h4>

                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Pemindahan Barang</a></li>
                    </ol>

                </div>
            </div>   

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Pemindahan Barang</h5>
            <div class="menu-header">
            <a href="{{ url('gudang/pemindahan-barang-gudang/create') }}" class="btn btn-default btn-rounded waves-effect waves-light">
                <span class="btn-label"><i class="fa fa-plus"></i></span>
                Tambah
            </a>    
            </div>
            </div>
            <div class="card-body">
                <table class="table">
                @if($items->isEmpty())
                    <div class="alert alert-warning"> Tidak ada data. </div>
                @else
                    <tr>
                        <th>No</th>
                        <th>No Transfer</th>
                        <th>Tanggal</th>
                        <th>Dari</th>
                        <th>s/d</th>
                        <th></th>
                    </tr>
                    @foreach ($items as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->no_transfer }}</td>
                        <td>{{ \Helpers\IndonesiaDate::indonesiaDate($item->tanggal) }}</td>
                        <td>{{ $item->alamat_dari_gudang }}</td>
                        <td>{{ $item->alamat_ke_gudang }}</td>
                        <td>
                        {!! Form::open(['route' => [$module_url->destroy, $item->id], 'method' => 'DELETE', 'class' => 'delete']) !!}
                            <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item edit" href="{{ route($module_url->edit, $item->id) }}"><i class="fa fa-pencil"></i> Edit</a>
                                <a class="dropdown-item deleteBtn" href="#">
                                    <i class="fa fa-trash"></i>&emsp;Delete
                                </a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        </td> 
                    </tr>
                    @endforeach
                @endif
                </table>
                <div class="pull-right">
                    <!-- Pagination disini -->
                </div>
            </div>   

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection

@section('custom_js')

@endsection
