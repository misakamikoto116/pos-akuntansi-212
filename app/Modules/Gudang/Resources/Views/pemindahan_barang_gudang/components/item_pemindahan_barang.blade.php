<tr>
    <td>{!! Form::select('produk_id['.($row ?? 0).']',$produk, $produk ?? null,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id', 'required'])!!}</td>
    <td>{!! Form::text('keterangan[]',null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk', 'required'])!!}</td>
    <td>{!! Form::text('jumlah[]',null,['class' => 'form-control qty_produk','id' => 'qty_produk','onchange' => 'sumQtyAndUnit(0)', 'required'])!!}</td>
    <td>{!! Form::text('satuan[]',null,['class' => 'form-control satuan_produk','id' => 'satuan_produk','onchange' => 'sumQtyAndUnit(0)', 'required'])!!}</td>
    <td>{!! Form::text('serial_number[]',null,['class' => 'form-control serial_number','id' => 'serial_number','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
    <td><button href="" class="remove btn btn-danger remove-rincian-pemindahan-barang" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
</tr>