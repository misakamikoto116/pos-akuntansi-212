<div class="form-group row">
  <div class="col-3 col-form-label">No. Transfer</div>
  <div class="col-3 col-form-label">Tanggal</div>
</div>
<div class="form-group row">
  <div class="col-3">
    {!! Form::text('no_transfer',null,['class'=>'form-control', 'id' => 'no_transfer']) !!}
  </div>
  <div class="col-3">
    @if (isset($item))
			{!! Form::text('tanggal',null,['class' => 'form-control tanggal'])!!}
		@else
			{!! Form::text('tanggal',null,['class' => 'form-control tanggal'])!!}
		@endif
  </div>
</div>
<hr>
<div class="form-group row">
  <div class="col-3 col-form-label">Transfer dari</div>
  <div class="col-3 col-form-label">Transfer ke</div>
</div>
<div class="form-group row">
  <div class="col-3">
    {!! Form::select('dari_gudang_id',$gudang,null,['placeholder'=>'-Pilih Dari Gudang-','class'=>'form-control select2', 'id' => 'dari_gudang_id']) !!}
		{!! Form::hidden('val_dari',isset($item) ? $item->dari_gudang_id : null,['class' => 'form-control col-form-label','id' => 'val_dari'])!!}
  </div>
  <div class="col-3">
    {!! Form::select('ke_gudang_id',$gudang,null,['placeholder'=>'-Pilih Ke Gudang-','class'=>'form-control select2', 'id' => 'ke_gudang_id']) !!}
    {!! Form::hidden('val_ke',isset($item) ? $item->ke_gudang_id : null,['class' => 'form-control col-form-label','id' => 'val_ke'])!!}
  </div>
</div>
<div class="form-group row">
  <div class="col-3 col-form-label">Dari Alamant</div>
  <div class="col-3 col-form-label">Ke Alamat</div>
</div>
<div class="form-group row">
  <div class="col-3">
    {!! Form::textarea('alamat_dari_gudang',null,['class'=>'form-control','id' => 'alamat_dari_gudang', 'readonly'])!!}
  </div>
  <div class="col-3">
    {!! Form::textarea('alamat_ke_gudang',null,['class'=>'form-control','id' => 'alamat_ke_gudang', 'readonly'])!!}
  </div>
</div>
<hr>


<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3);">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>
<div class="pemindahan-barang-sections" data-id="1">
<div class="duplicate-pemindahan-barang-sections">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab">
  	<table width="100%" class="table">
  		<thead class="thead-light" style="border-collapse: collapse;">
        <tr>
          <th width="20%">Item</th>
          <th>Keterangan Item</th>
          <th>Quantity</th>
          <th>Unit</th>
          <th>SN</th>
        </tr>
      </thead>
      <tbody class="pemindahanbarang">
        @if(isset($item))
          @if(old('produk_id') !== null)  
              @foreach(old('produk_id') as $row => $produk_id )
                @include('akuntansi::pemindahan_barang/components/item_pemindahan_barang',[
                  'row'                                 => $row, 
                  'produk_id'                           => $produk_id,
                  'keterangan'                          => old('keterangan')[$row],
                  'jumlah'                              => old('jumlah')[$row],
                  'satuan'                              => old('satuan')[$row],
                  'serial_number'                       => old('serial_number')[$row]
                ])
              @endforeach      
          @else
            @foreach($item->detailPemindahanBarang as $data)
              <tr>
                <td>{!! Form::select('produk_id[]',$produk,$data->produk_id,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id', 'required'])!!}</td>
                <td>{!! Form::text('keterangan[]',$data->keterangan,['class' => 'form-control keterangan_produk','id' => '',''])!!}</td>
                <td>{!! Form::text('jumlah[]',$data->jumlah,['class' => 'form-control qty_produk','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
                <td>{!! Form::text('satuan[]',$data->satuan,['class' => 'form-control satuan_produk','id' => '',''])!!}</td>
                <td>{!! Form::text('serial_number[]',$data->serial_number,['class' => 'form-control serial_number','id' => '','onchange' => 'sumQtyAndUnit(0)'])!!}</td>
                <td><button href="" class="remove btn btn-danger remove-rincian-serial-barang" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
              </tr>
            @endforeach
          @endif
        @elseif(old('produk_id') !== null)  
          @foreach(old('produk_id') as $row => $produk_id )
            @include('akuntansi::pemindahan_barang/components/item_pemindahan_barang',[
              'row'                                 => $row, 
              'produk_id'                           => $produk_id,
              'keterangan'                          => old('keterangan')[$row],
              'jumlah'                              => old('jumlah')[$row],
              'satuan'                              => old('satuan')[$row],
              'serial_number'                       => old('serial_number')[$row]
            ])
          @endforeach      
        @endif
      </tbody>
    </table>
  	<a class="btn btn-info add-pemindahan-barang"><i class="fa fa-plus"> Tambah</i></a>
  </div>
  </div>
</div><!-- END PILLS -->
<script type="text/template" id="table_pemindahan_barang_section" data-id="">
  <tr>
        <td>{!! Form::select('produk_id[]',$produk,null,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id', 'required'])!!}</td>
        <td>{!! Form::text('keterangan[]',null,['class' => 'form-control keterangan_produk','id' => 'keterangan_produk', 'required'])!!}</td>
        <td>{!! Form::text('jumlah[]',null,['class' => 'form-control qty_produk','id' => 'qty_produk', 'required'])!!}</td>
        <td>{!! Form::text('satuan[]',null,['class' => 'form-control satuan_produk','id' => 'satuan_produk', 'required'])!!}</td>
        <td>{!! Form::text('serial_number[]',null,['class' => 'form-control serial_number','id' => 'serial_number'])!!}</td>
        <td><button href="" class="remove btn btn-danger remove-rincian-pemindahan-barang" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
	</tr>
</script>
</div>