<div class="form-group row">
	{!! Form::label('nama','Nama',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
		{!! Form::text('nama',null,['class' => 'form-control','id' => 'nama'])!!}
		</div>
</div>
<div class="form-group row">
	{!! Form::label('keterangan','Keterangan',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
		{!! Form::text('keterangan',null,['class' => 'form-control','id' => 'keterangan'])!!}
		</div>
</div>
<div class="form-group row">
	{!! Form::label('alamat','Alamat',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
		{!! Form::text('alamat',null,['class' => 'form-control','id' => 'alamat'])!!}
		</div>
</div>
<div class="form-group row">
	{!! Form::label('penanggung_jawab','Penanggung Jawab',['class' => 'col-2 col-form-label']) !!}
		<div class="col-10">
		{!! Form::text('penanggung_jawab',null,['class' => 'form-control','id' => 'penanggung_jawab'])!!}
		</div>
</div>