@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection
@section('title')
    {{$title}}
@endsection
@section('content')
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title"></h4>

                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Barang Pergudang</a></li>
                    </ol>

                </div>
            </div>   

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header"><h5 class="title">Barang Pergudang</h5>
            <div class="menu-header">
            </div>
            </div>
            <div class="card-body">
                <table class="table">
                @if ($items->isEmpty())
                    <div class="alert alert-warning"> Tidak ada data. </div>
                @else
                    <tr>
                        @foreach($items->first() as $dataGudang)
                            <th>
                                {{  $dataGudang  }}
                            </th>
                        @endforeach
                    </tr>
                @endif
                @if($items->isEmpty())
                    <tr>
                            <td colspan={{$items->first()->count()}} align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia.
                            </div>
                        </td>
                    </tr>
                @else
                    @foreach($items->splice(1) as $dataGudang)
                        <tr>
                            @foreach($dataGudang as $data)
                                <td>
                                    {{$data}}
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                @endif
                </table>
            </div>   

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
@endsection

@section('custom_js')

@endsection
