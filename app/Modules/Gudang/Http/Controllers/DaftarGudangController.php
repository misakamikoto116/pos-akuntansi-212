<?php

namespace App\Modules\Gudang\Http\Controllers;
use App\Modules\Gudang\Http\Requests\DaftarGudangRequest;
use App\Modules\Gudang\Repositories\DaftarGudangRepositories;
use Generator\Interfaces\RepositoryInterface;
class DaftarGudangController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'gudang';
        $this->model = $model;
        $this->title = 'Daftar Gudang';
        $this->request = DaftarGudangRequest::class;
        $this->requestField = ['nama','keterangan','alamat','penanggung_jawab'];
    }
}
