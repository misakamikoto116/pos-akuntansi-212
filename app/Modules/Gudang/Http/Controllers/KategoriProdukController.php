<?php

namespace App\Modules\Gudang\Http\Controllers;
use App\Modules\Gudang\Http\Requests\KategoriProdukRequest;
use App\Modules\Gudang\Repositories\KategoriProdukRepositories;
use Generator\Interfaces\RepositoryInterface;
class KategoriProdukController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'gudang';
        $this->model = $model;
        $this->title = 'Kategori Produk';
        $this->request = KategoriProdukRequest::class;
        $this->requestField = ['nama'];
    }
}
