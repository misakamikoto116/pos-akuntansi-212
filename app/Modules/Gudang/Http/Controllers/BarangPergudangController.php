<?php

namespace App\Modules\Gudang\Http\Controllers;

use Generator\Interfaces\RepositoryInterface;

class BarangPergudangController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'gudang';
        $this->model = $model;
        $this->title = 'Barang Pergudang';
    }
}
