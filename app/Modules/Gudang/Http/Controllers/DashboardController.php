<?php

namespace App\Modules\Gudang\Http\Controllers;
use App\Modules\Gudang\Http\Requests\DashboardRequest;
use Generator\Interfaces\RepositoryInterface;

class DashboardController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role         =   'gudang';
        $this->model        =   $model;
        $this->title        =   'Dashboard Gudang Toko';
        $this->request      =   DashboardRequest::class;
        $this->requestField =   [
            'approved',
            'approved_by',
        ];
    }
}
