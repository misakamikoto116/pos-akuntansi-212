<?php

namespace App\Modules\Gudang\Http\Controllers;
use App\Modules\Akuntansi\Http\Requests\PemindahanBarangGudangRequest;
use App\Modules\Akuntansi\Repositories\PemindahanBarangGudangRepositories;
use Generator\Interfaces\RepositoryInterface;
class PemindahanBarangGudangController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
    	$this->role = 'gudang';
        $this->model = $model;
        $this->title = 'Pemindahan Barang';
        $this->request = PemindahanBarangRequest::class;
        $this->requestField = [
            'no_transfer',
            'tanggal',
            'dari_gudang_id',
            'alamat_dari_gudang',
            'ke_gudang_id',
            'alamat_ke_gudang',
            'produk_id',
            'keterangan',
            'jumlah',
            'satuan',
            'serial_number',
        ];
    }
    public function formData()
    {
        return [
            'gudang' => $this->model->listGudang(),
            'produk' => $this->model->listProduk(),
        ];
    }
}
