<?php

namespace App\Modules\Gudang\Repositories;

use Generator\Interfaces\RepositoryInterface;
use Carbon\Carbon;
use App\Modules\Akuntansi\Models\PesananPenjualan;
use App\Modules\Akuntansi\Models\Pemberitahuan;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
class DashboardRepositories implements RepositoryInterface 
{
    public function __construct(PesananPenjualan $model, Pemberitahuan $pemberitahuan)
    {
        $this->model            =   $model;
        $this->pemberitahuan    =   $pemberitahuan;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {   
        return $this->model->where('approved', 0)->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data   =   [
            'approved'      =>  0,
            'approved_by'   =>  auth()->user()->id,
        ];
        $model = $this->findItem($id)->fill($data);

        $dataPemberitahuan = [
            'tujuan' => $model->tokoKasir->id,
            'isi_pemberitahuan' =>  'Pesanan Nomor : '.$model->id.' Telah di-Validasi oleh Gudang : '.$model->gudangApproved->name,
            'created_by' =>  auth()->user()->id,
        ];
        $channel = 'kasir_'.$dataPemberitahuan['tujuan'];
        $pemberitahuan  =   $this->pemberitahuan->fill($dataPemberitahuan);
        if(send_notif_socket($channel, $dataPemberitahuan['isi_pemberitahuan'])){
            if($pemberitahuan->save()){
                $model->save();
                return response()->redirectToRoute('gudang.dashboard.index');
            }
        }else{
            return response()->redirectToRoute('gudang.dashboard.index',[], 302)
                ->withMessage('Gagal Mengirimkan notifikasi/pemberitahuan');
        }
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {        
        return $data;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     * @return mix
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }
}