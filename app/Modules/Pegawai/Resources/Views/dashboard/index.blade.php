@extends('chichi_theme.layout.app')

@section('custom_css')
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Dashboard Kepegawaian</h4>
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Dashboard</a></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">Dashboard</h5>
                </div>
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>    
@endsection

@section('custom_js')
    <script>

    </script>
@endsection