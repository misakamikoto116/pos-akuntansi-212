<?php

namespace App\Modules\Pegawai\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class DashboardPegawai extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        return view('pegawai::dashboard.index');
    }
}
