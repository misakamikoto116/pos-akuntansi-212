<?php

namespace App\Modules\Pos\Http\Middleware;

use Closure;

class KasirOnline
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $kasir)
    {
        if ($kasir === "true") {
            $cekKasir = session()->get('kasir_cek', false);

            if ($cekKasir === false) {
                return redirect()->route('pos.penjualan.kasir.check.in');
            }
        } else {
            $cekKasir = session()->get('kasir_cek', false);

            if ($cekKasir === true) {
                return redirect()->route('pos.penjualan.index');
            }
        }

        return $next($request);
    }
}
