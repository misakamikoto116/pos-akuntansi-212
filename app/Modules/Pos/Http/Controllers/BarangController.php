<?php

namespace App\Modules\Pos\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\StikerHarga;

class BarangController extends Controller
{
    /**
     * List Barang
     */
    public function index()
    {
        $view = [
            'goods' => Produk::whereNotNull('gudang_id')->Filter(request())->paginate(200)
        ];

        return view('pos::barang.index')->with($view);
    }

    /**
     * Print Stiker Harga
     */
    public function printStiker($id)
    {
        $view = [
            'goood' => Produk::findOrFail($id)
        ];

        return view('pos::barang.print')->with($view);
    }

    /**
     * Print Sticker Harga Barang
     *
     * @method GET
     * @param Request
     * @return view
     */
    public function priceCardBySelect(Request $request)
    {
        $selectBarang = explode(',', trim($request->get('selected')));
        $barang = Produk::whereIn('id', $selectBarang)
            ->limit(25)
            ->get();

        $stiker = StikerHarga::where('aktif', true)->first();

        $data = [
            'data' => $barang,
            'header' => $stiker
        ];

        return view('pos::barang.PricePrint')->with($data);
    }

    /**
     * Print Sticker Harga Barang
     *
     * @method GET
     * @param Request
     * @return view
     */
    public function promoCardBySelect(Request $request)
    {
        $selectBarang = explode(',', trim($request->get('selected')));
        $barang = Produk::whereIn('id', $selectBarang)
            ->limit(25)
            ->get();

        $stiker = StikerHarga::where('aktif', true)->first();

        $data = [
            'data' => $barang,
            'header' => $stiker
        ];
        
        return view('pos::barang.PromoPrint')->with($data);
    }
}
