<?php

namespace App\Modules\Pos\Http\Controllers;

use Illuminate\Http\Request;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\BarangFakturPembelian;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\BarangPenawaranPenjualan;
use App\Modules\Akuntansi\Models\BarangPenerimaanPembelian;
use App\Modules\Akuntansi\Models\BarangPengirimanPenjualan;
use App\Modules\Akuntansi\Models\BarangPermintaanPembelian;
use App\Modules\Akuntansi\Models\BarangPesananPembelian;
use App\Modules\Akuntansi\Models\BarangPesananPenjualan;
use App\Modules\Akuntansi\Models\BarangReturPembelian;
use App\Modules\Akuntansi\Models\BarangReturPenjualan;
use App\Modules\Akuntansi\Models\BebanFakturPembelian;
use App\Modules\Akuntansi\Models\FakturPembelian;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\HistoryRekonsiliasi;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Akuntansi\Models\PembiayaanPesanan;
use App\Modules\Akuntansi\Models\PenawaranPenjualan;
use App\Modules\Akuntansi\Models\PenerimaanPembelian;
use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use App\Modules\Akuntansi\Models\PermintaanPembelian;
use App\Modules\Akuntansi\Models\PesananPembelian;
use App\Modules\Akuntansi\Models\PesananPenjualan;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\ReturPembelian;
use App\Modules\Akuntansi\Models\ReturPenjualan;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use App\Modules\Akuntansi\Models\TipeAktivaTetapPajak;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\TransaksiUangMuka;
use App\Modules\Akuntansi\Models\TransaksiUangMukaPemasok;
use Carbon\Carbon;
use Cart;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    public function getDetailBarang(Request $request)
    {
        $id = $request->get('id');
        $barang = $request->get('type');
        $pelanggan_id = $request->get('pelanggan_id');
        $pemasok_id = $request->get('pemasok_id');
        if (!empty($id)) {
            if (!empty($pelanggan_id) && is_numeric($pelanggan_id)) {
                $informasi_pelanggan = InformasiPelanggan::where('id', $pelanggan_id)->first();
                $kode_pajak = KodePajak::whereIn('id', [$informasi_pelanggan->pajak_satu_id, $informasi_pelanggan->pajak_dua_id])->orderBy('nilai', 'ASC')->get();
            } elseif (!empty($pemasok_id) && is_numeric($pemasok_id)) {
                $informasi_pemasok = InformasiPemasok::where('id', $pemasok_id)->first();
                $kode_pajak = KodePajak::whereIn('id', [$informasi_pemasok->pajak_satu_id, $informasi_pemasok->pajak_dua_id])->orderBy('nilai', 'ASC')->get();
            } else {
                $kode_pajak = KodePajak::get();
            }
            switch ($barang) {
                // Bagian Penjualan
                case 'penawaran':
                    $data = BarangPenawaranPenjualan::with(['produk' => function ($item) {
                        $item->with('kodePajak');
                    }])->where(['penawaran_penjualan_id' => $id])->get();
                break;

                case 'pesanan':
                    $data = BarangPesananPenjualan::with('produk')->where(['pesanan_penjualan_id' => $id, 'pesanan_penjualan_id' => $id]);
                    $getBarangPesananId = $data->pluck('id');
                    $dataPengiriman = BarangPengirimanPenjualan::whereIn('barang_pesanan_penjualan_id', $getBarangPesananId)
                                    ->groupBy('produk_id')
                                    ->selectRaw('*, sum(jumlah) as sum')->get();
                    if ($dataPengiriman->count() <= 0) {
                        $data = $data->get();
                    } else {
                        $data = $data->get()->each(function ($item, $key) use ($dataPengiriman) {
                            $item->jumlah -= $dataPengiriman[$key]->sum;

                            return $item;
                        });
                    }
                break;

                case 'pengiriman':
                    $data = BarangPengirimanPenjualan::with('produk')->where('pengiriman_penjualan_id', $id)->get();
                break;

                case 'faktur':
                    $data = BarangFakturPenjualan::with('produk')->where('faktur_penjualan_id', $id)->get();
                break;

                case 'retur':
                    $data = BarangReturPenjualan::with('produk')->where('retur_penjualan_id', $id)->get();
                break;

                case 'uang-muka':
                $data = TransaksiUangMuka::with('fakturUangMukaPelanggan')->where('faktur_penjualan_id', $id)->get();
                break;

                // Bagian Pembelian
                case 'permintaan-pembelian':
                    $data = BarangPermintaanPembelian::where('permintaan_pembelian_id', $id)->get();
                break;

                case 'pesanan-pembelian':
                    $data = BarangPesananPembelian::with(['produk', 'pesananPembelian'])->where('pesanan_pembelian_id', $id)->get();
                break;

                case 'penerimaan-pembelian':
                    $data = BarangPenerimaanPembelian::with('produk')->where('penerimaan_pembelian_id', $id)->get();
                break;

                case 'faktur-pembelian':
                    $data = BarangFakturPembelian::with('produk')->where('faktur_pembelian_id', $id)->get();
                break;

                case 'barang':
                    $data = SaldoAwalBarang::where('produk_id', $id)->get();
                break;
            }
            if ($barang == 'barang') {
                $data = $this->formattingTanggalNominalBarang($data);
            }
            if ($barang == 'pesanan-pembelian') {
                $data = $data->map(function ($item) {
                    $item->updated_qty = $item->updated_qty_penerimaan_pembelian;

                    return $item;
                });
            }
            $response = [
                'code' => 200,
                'barang' => $data,
                'kode_pajak' => $kode_pajak,
            ];
            if ($barang == 'pesanan') {
                $response['penawaran_id'] = $this->getPenawaranId($data);
            }

            if ($barang == 'faktur-pembelian') {
                $response['beban'] = BebanFakturPembelian::where('faktur_pembelian_id', $id)->get();
                $response['dp'] = TransaksiUangMukaPemasok::with('fakturUangMukaPemasok.barang')->where('faktur_pembelian_id', $id)->get();
            }
        } else {
            $response = [
                'code' => 404,
                'barang' => null,
            ];
        }

        return response()->json($response);
    }

    /** 
     * 
     */

     public function getProdukByVal(Request $request)
    {
        $id = $request->get('id');
        $pelanggan_id = $request->get('pelanggan_id');
        $pemasok_id = $request->get('pemasok_id');
        $type = $request->get('type');
        if (null !== $id) {
            switch ($type) {
                case 'penawaran':
                    $data = BarangPenawaranPenjualan::findOrFail($id);
                break;

                case 'pesanan':
                    $data = BarangPesananPenjualan::findOrFail($id);
                break;

                case 'pengiriman':
                    $data = BarangPengirimanPenjualan::findOrFail($id);
                break;

                case 'faktur':
                    $data = BarangFakturPenjualan::findOrFail($id);
                break;

                case 'retur':
                    $data = BarangReturPenjualan::where('barang_faktur_penjualan_id', $id)->first();
                break;

                // Bagian Pembelian
                case 'permintaan-pembelian':
                    $data = BarangPermintaanPembelian::findOrFail($id);
                break;

                case 'pesanan-pembelian':
                    $data = BarangPesananPembelian::findOrFail($id);
                break;

                case 'penerimaan-pembelian':
                    $data = BarangPenerimaanPembelian::findOrFail($id);
                break;

                case 'faktur-pembelian':
                    $data = BarangFakturPembelian::findOrFail($id);
                break;

                case 'retur-pembelian':
                    $data = BarangReturPembelian::findOrFail($id);
                break;
            }
            // Produk hanya digunakan guna mencari pajak / tax
            $produk = Produk::where('id', $data->produk_id)->first();
            $saldoAwalBarang = new SaldoAwalBarang();
            $array_gudang = [];
            $produk_gudang = Produk::where('id', $data->produk_id)->get()
                ->map(function ($item) use ($array_gudang, $saldoAwalBarang) {
                    if (!empty($item->gudang->id)) {
                        $array_gudang['gudang'] = [0 => ['id' => $item->gudang->id, 'nama' => $item->gudang->nama]];
                        $i = 1;
                        $cek_gudang = $saldoAwalBarang->where('produk_id', $item->id)->groupBy('gudang_id')->get();
                        foreach ($cek_gudang as $saldo_awal) {
                            if ($item->gudang->id != $saldo_awal->gudang_id) {
                                $array_gudang['gudang'] += [$i => ['id' => $saldo_awal->gudang_id,
                                    'nama' => $saldo_awal->gudang->nama, ]];
                                ++$i;
                            }
                        }
                    }else {
                        $array_gudang['gudang'] = [0 => ['id' => $item->gudang_lain->id, 'nama' => $item->gudang_lain->nama]];
                        $i = 1;
                        $cek_gudang = $saldoAwalBarang->where('produk_id', $item->id)->groupBy('gudang_id')->get();
                        foreach ($cek_gudang as $saldo_awal) {
                            if ($item->gudang_lain->id != $saldo_awal->gudang_id) {
                                $array_gudang['gudang'] += [$i => ['id' => $saldo_awal->gudang_id,
                                    'nama' => $saldo_awal->gudang->nama, ]];
                                ++$i;
                            }
                        }
                    }

                    return $array_gudang;
                });

            // Ambil Semua Pajak Pelanggan & Pemasok
            if (!empty($pelanggan_id) && is_numeric($pelanggan_id)) {
                $informasi_pelanggan = InformasiPelanggan::where('id', $pelanggan_id)->first();
                $kode_pajak = KodePajak::whereIn('id', [$informasi_pelanggan->pajak_satu_id, $informasi_pelanggan->pajak_dua_id])->orderBy('nilai', 'ASC')->get();
            } elseif (!empty($pemasok_id) && is_numeric($pemasok_id)) {
                $informasi_pemasok = InformasiPemasok::where('id', $pemasok_id)->first();
                $kode_pajak = KodePajak::whereIn('id', [$informasi_pemasok->pajak_satu_id, $informasi_pemasok->pajak_dua_id])->orderBy('nilai', 'ASC')->get();
            } else {
                $kode_pajak = KodePajak::get();
            }
            // Apabila type yang dipanggil pesanan
            if ((!empty($data))) {
                $response = [
                    // 'produk' =>$produk->kodePajak,
                    'id' => $data->id,
                    'keterangan' => $data->item_deskripsi,
                    'jumlah' => $data->jumlah,
                    'diskon' => $data->diskon,
                    'tax' => $kode_pajak,
                    'satuan' => $data->satuan,
                    'unitPrice' => $data->harga,
                    'barang_id' => $data->id,
                    'terproses' => $data->terproses,
                    'ditutup' => $data->ditutup,
                    'multiGudang' => $produk_gudang,
                    'harga_modal' => $produk->produk_detail,
                    'all' => $data,
                ];
                if (!empty($data->kode_pajak_id)) {
                    $response['kode_pajak_id'] = $data->kode_pajak_id;
                    $response['kode_pajak_2_id'] = $data->kode_pajak_2_id;
                }

                if ('retur-pembelian' == $type) {
                    $response['barang_faktur_pembelian_id'] = $data->barang_faktur_pembelian_id;
                    $response['taxable'] = ReturPembelian::findOrFail($data->retur_pembelian_id);
                }
                if ('faktur-pembelian' == $type) {
                    $response['satuan'] = $data->item_unit;
                    $response['unitPrice'] = $data->unit_price;
                    $response['taxable'] = FakturPembelian::where('id', $data->faktur_pembelian_id)->first();
                }
                if (
                    // $type == 'faktur' or
                    'permintaan-pembelian' == $type
                ) {
                    $response['satuan'] = $data->item_unit;
                    $response['unitPrice'] = $data->unit_price;
                }

                if ('pesanan-pembelian' == $type) {
                    $pesanan_pembelian = PesananPembelian::where('id', $data->pesanan_pembelian_id)->first();
                    $response['fob'] = $pesanan_pembelian->fob;
                    $response['ship_id'] = $pesanan_pembelian->ship_id;
                    $response['term_id'] = $pesanan_pembelian->term_id;
                    $response['satuan'] = $data->satuan;
                    $response['taxable'] = $pesanan_pembelian;
                    $response['updated_qty'] = $data->updated_qty_penerimaan_pembelian;
                }

                if ('faktur' == $type) {
                    $response['satuan'] = $data->item_unit;
                    $response['unitPrice'] = $data->unit_price;
                    $response['taxable'] = FakturPenjualan::where('id', $data->faktur_penjualan_id)->first();
                }

                if ('retur' == $type) {
                    $response['barang_faktur_penjualan_id'] = $data->barang_faktur_penjualan_id;
                }

                if ('penawaran' == $type) {
                    $response['penawaran_id'] = $data->penawaran_penjualan_id;
                    $response['taxable'] = PenawaranPenjualan::where('id', $data->penawaran_penjualan_id)->first();
                }

                if ('pengiriman' == $type) {
                    $pengiriman_penjualan = PengirimanPenjualan::where('id', $data->pengiriman_penjualan_id)->first();
                    $response['satuan'] = $data->item_unit;
                    $response['delivery_date'] = $pengiriman_penjualan->delivery_date;
                    // Cari Harga sampai dapat
                    if (null !== $data->barang_pesanan_penjualan_id) {
                        $cari_harga_ke_pesanan = BarangPesananPenjualan::where('id', $data->barang_pesanan_penjualan_id)->first();
                        if (null !== $cari_harga_ke_pesanan->harga) {
                            $response['unitPrice'] = $cari_harga_ke_pesanan->harga;
                        } else {
                            $response['unitPrice'] = 0;
                        }
                    } else {
                        $response['unitPrice'] = 0;
                    }
                    // Cari Diskon sampai dapat
                    if (null !== $data->barang_pesanan_penjualan_id) {
                        $cari_diskon_ke_pesanan = BarangPesananPenjualan::where('id', $data->barang_pesanan_penjualan_id)->first();
                        if (null !== $cari_diskon_ke_pesanan->diskon) {
                            $response['diskon'] = $cari_diskon_ke_pesanan->diskon;
                        } else {
                            $response['diskon'] = 0;
                        }
                    } else {
                        $response['diskon'] = 0;
                    }
                    // Cari Pajak Ke Pesanan
                    if (null === $data->kode_pajak_id || null === $data->kode_pajak_2_id) {
                        $data_pajak = BarangPesananPenjualan::where('id', $data->barang_pesanan_penjualan_id)->first();
                        $response['kode_pajak_id'] = $data_pajak->kode_pajak_id;
                        $response['kode_pajak_2_id'] = $data_pajak->kode_pajak_2_id;
                    }
                    // Cari So Number & Term ID & FOB
                    $cari_no_so = PesananPenjualan::whereHas('barang.barangPengirimanPenjualan', function ($query) use ($data) {
                        $query->where('id', $data->id);
                    })->first();
                    $response['diskon_faktur'] = $cari_no_so->diskon;
                    $response['jumlah_diskon_faktur'] = $cari_no_so->jumlah_diskon_faktur;
                    $response['keterangan_faktur'] = $cari_no_so->keterangan;
                    $response['ongkir_faktur'] = $cari_no_so->ongkir;
                    $response['no_so'] = $cari_no_so->so_number;
                    $response['term_id'] = $cari_no_so->term_id;
                    $response['fob'] = $cari_no_so->fob;
                    $response['no_do'] = $pengiriman_penjualan->delivery_no;
                    $response['po_no'] = $pengiriman_penjualan->po_no;
                    $response['ship_id'] = $pengiriman_penjualan->ship_id;
                    $response['barang_pengiriman_penjualan_id'] = $data->id;
                    $response['taxable'] = $cari_no_so;
                }
                if ('penerimaan-pembelian' == $type) {
                    $response['keterangan'] = $data->produk->keterangan;
                    // Cari value ke penerimaan-pembelian
                    $data_penerimaan = PenerimaanPembelian::where('id', $data->penerimaan_pembelian_id)->first();
                    $response['form_no'] = $data_penerimaan->form_no;
                    $response['ship_date'] = Carbon::parse($data_penerimaan->ship_date)->format('d F Y');
                    $response['receive_date'] = $data_penerimaan->receive_date;
                    $response['fob'] = $data_penerimaan->fob;
                    $response['ship_id'] = $data_penerimaan->ship_id;

                    // Cari Value dari penerimaan pembelian ke pesanan pembelian
                    $data_pesanan = BarangPesananPembelian::where('id', $data->barang_pesanan_pembelian_id)->first();

                    $response['unitPrice'] = 0;
                    $response['diskon'] = 0;
                    $response['kode_pajak_id'] = null;
                    $response['kode_pajak_2_id'] = null;
                    $response['updated_qty'] = $data->updated_qty;

                    if (!empty($data_pesanan)) {
                        $response['unitPrice'] = $data_pesanan->harga;
                        $response['diskon'] = $data_pesanan->diskon;
                        $response['kode_pajak_id'] = $data_pesanan->kode_pajak_id;
                        $response['kode_pajak_2_id'] = $data_pesanan->kode_pajak_2_id;

                        // Cari value ke pesanan pembelian
                        $data_pesanan_pembelian = PesananPembelian::where('id', $data_pesanan->pesanan_pembelian_id)->first();
                        $response['term_id'] = $data_pesanan_pembelian->term_id;
                        $response['taxable'] = $data_pesanan_pembelian;
                    }
                }
                if ('pesanan' == $type) {
                    $pesanan_penjualan = PesananPenjualan::where('id', $data->pesanan_penjualan_id)->first();
                    $response['diskon_faktur'] = $pesanan_penjualan->diskon;
                    $response['keterangan_faktur'] = $pesanan_penjualan->keterangan;
                    $response['ongkir_faktur'] = $pesanan_penjualan->ongkir;
                    $response['no_so'] = $pesanan_penjualan->so_number;
                    $response['po_no'] = $pesanan_penjualan->po_number;
                    $response['ship_id'] = $pesanan_penjualan->ship_id;
                    $response['term_id'] = $pesanan_penjualan->term_id;
                    $response['fob'] = $pesanan_penjualan->fob;
                    $response['taxable'] = $pesanan_penjualan;
                    $response['nominal'] = $data->harga_total;
                    // $response['harga_modal'] 					= $data->harga_modal;
                }
            }
        } else {
            $response = [
                'code' => '404',
            ];
        }

        return response()->json($response);
    }

    /** 
     * 
     */

    public function getReturnedItem(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->get('id'); //faktur penjualannya
            $pelanggan_id = $request->get('pelanggan_id');
            $data = ReturPenjualan::where(['pelanggan_id' => $pelanggan_id, 'faktur_penjualan_id' => $id])->pluck('id');

            if (!empty($data)) {
                $barang = BarangReturPenjualan::whereIn('retur_penjualan_id', $data)->get();
                $arrayBarang = [];
                foreach ($barang as $key => $data_barang) {
                    $arrayBarang[] = [
                        'produk_id' => $data_barang->produk_id,
                        'qty' => $data_barang->where('produk_id', $data_barang->produk_id)->sum('jumlah'),
                    ];
                }
                $response = [
                    'code' => '200',
                    'data' => $arrayBarang,
                ];
            } else {
                $response = [
                    'code' => '404',
                    'data' => '',
                ];
            }

            return $response;
        }
    }
}
