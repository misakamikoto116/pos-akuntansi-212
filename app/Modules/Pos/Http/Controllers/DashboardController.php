<?php

namespace App\Modules\Pos\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __invoke()
    {
        return view('pos::dashboard.index');
    }
}
