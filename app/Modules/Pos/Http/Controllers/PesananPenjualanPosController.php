<?php
namespace App\Modules\Pos\Http\Controllers;

use App\Modules\Pos\Http\Requests\PesananPenjualanPosRequest;
use Generator\Interfaces\RepositoryInterface;
use Cart;
use Illuminate\Http\Request;

class PesananPenjualanPosController extends Controller
{
	public function __construct(RepositoryInterface $model)
	{
		$this->role         = 'pos';
		$this->model        = $model;
		$this->title        = 'Pesanan Penjualan';
		$this->request      = PesananPenjualanPosRequest::class;
		$this->requestField = [];
	}

	public function formData()
	{
		return [
			'getCartTotal' => $this->getCartTotal(),
		];
	}

	public function index()
	{
		return parent::index()->with($this->formData());
	}

	public function savePesananPenjualan()
	{
		$request = request();

		$cart = Cart::instance('pesanan-penjualan-' . auth()->user()->id);

		return $this->model->storePesanan($request);
	}

	public function getPenawaranPenjualan(Request $request)
	{
		return $this->model->getAllPenawaranPenjualan($request);
	}

	public function getProduk(Request $request)
	{
		return $this->model->getAllProduk($request);
	}

	public function getPelanggan(Request $request)
	{
		return $this->model->getAllPelanggan($request);
	}

	public function getCartTotal()
	{
		$total = 0;

		$cart = Cart::instance('pesanan-penjualan-' . auth()->user()->id);

		foreach ($cart->content() as $item) {
			$total += $item->options->price_final;
		}

		return $total;
	}

	public function addItem(Request $request)
	{
		try {
			$produk = $this->model->getProduk($request);

			$cart = Cart::instance('pesanan-penjualan-' . auth()->user()->id);

			$subTotalDiskon = ($produk->harga_jual * ($request->diskon_barang ?? 0)) / 100;
			$priceReal      = $produk->harga_jual - $subTotalDiskon;
			$totalDiskon    = $priceReal * $request->qty;

			Cart::instance('pesanan-penjualan-' . auth()->user()->id)->add([
				'id'      => $produk->id,
				'name'    => $produk->keterangan,
				'qty'     => (int) $request->qty,
				'price'   => $produk->harga_jual ?? 0,
				'options' => [
					'type'           => $request->type,
					'produk'         => $produk,
					'harga_modal'    => $produk->saldoAwalBarang->last()->harga_modal ?? 0,
					'harga_terakhir' => $produk->saldoAwalBarang->last()->harga_terakhir ?? 0,
					'price_real'     => $priceReal,
					'price_final'    => $totalDiskon,
					'diskon'         => $request->diskon_barang,
					'order_id'       => $cart->content()->isEmpty() ?
										1 :
										$cart->content()->max('options.order_id') + 1,
				],
			]);

			send_display_kasir([
				'barang'   => $produk->keterangan,
				'qty'      => $request->qty,
				'price'    => $produk->harga_jual ?? 0,
				'subtotal' => $request->qty * $produk->harga_jual ?? 0,
				'total'    => $this->getCartTotal(),
				'kasir'    => 'kasir_' . auth()->user()->id,
			]);

			$response = [
				'status'   => true,
				'template' => view('pos::pesanan_penjualan_pos.cart_penjualan')->render(),
				'total'    => $this->getCartTotal(),
			];
		} catch (Exception $e) {
			$response = [
				'status'  => false,
				'message' => $e->getMessage(),
			];
		}

		return response()->json($response);
	}

	public function diskonItem(Request $request)
	{
		return $this->model->diskonItem($request);
	}

	public function removeItem(Request $request)
	{
		try {
			Cart::instance('pesanan-penjualan-' . auth()->user()->id)->remove($request->id);

			$response = [
				'status'   => true,
				'total'    => $this->getCartTotal(),
				'template' => view('pos::pesanan_penjualan_pos.cart_penjualan')->render(),
			];
		} catch (Exception $e) {
			$response = [
				'status'  => false,
				'message' => $e->getMessage(),
			];
		}

		return response()->json($response);
	}

	public function displayKasir()
	{
		$kasir_id = 'kasir_' . auth()->user()->id;
		$iklan    = $this->iklan->where('publish', '=', true)
			->orderBy('urutan', 'ASC')
			->orderBy('created_at', 'desc')
			->get();

		return view('pos::penjualan.display_kasir')->with(['kasir_id' => $kasir_id, 'iklan' => $iklan]);
	}

	public function addItemPenawaranPenjualan(Request $request)
	{
		try {
			$penawaran = $this->model->getPenawaranPenjualan($request);

			$cart = Cart::instance('pesanan-penjualan-' . auth()->user()->id);

			// cek apabila ada barang yang sama maka update row list barang yang sama
			foreach ($penawaran as $data_penawaran) {
				// perhitungan pesanan
				$subTotalDiskon = ($data_penawaran->harga * ($data_penawaran->diskon ?? 0)) / 100;
				$priceReal      = $data_penawaran->harga - $subTotalDiskon;
				$totalDiskon    = $priceReal * $data_penawaran->jumlah;

				// tambah row list barang
				Cart::instance('pesanan-penjualan-' . auth()->user()->id)->add([
					'id'      => $data_penawaran->id,
					'name'    => $data_penawaran->item_deskripsi,
					'qty'     => $data_penawaran->jumlah,
					'price'   => $data_penawaran->harga ?? 0,
					'options' => [
						'type'           => $request->type,
						'produk'         => $data_penawaran,
						'harga_modal'    => $data_penawaran->produk->saldoAwalBarang->last()->harga_modal ?? 0,
						'harga_terakhir' => $data_penawaran->produk->saldoAwalBarang->last()->harga_terakhir ?? 0,
						'price_real'     => $priceReal,
						'price_final'    => $totalDiskon,
						'diskon'         => $data_penawaran->diskon,
						'order_id'       => $cart->content()->isEmpty() ?
											  1 :
											  $cart->content()->max('options.order_id') + 1,
					],
				]);

				send_display_kasir([
					'barang'   => $data_penawaran->item_deskripsi,
					'qty'      => $data_penawaran->jumlah,
					'price'    => $data_penawaran->harga ?? 0,
					'subtotal' => $data_penawaran->jumlah * $data_penawaran->harga ?? 0,
					'total'    => $this->getCartTotal(),
					'kasir'    => 'kasir_' . auth()->user()->id,
				]);
			}

			$response = [
				'status'    => true,
				'template'  => view('pos::pesanan_penjualan_pos.cart_penjualan')->render(),
				'total'     => $this->getCartTotal(),
				'pelanggan' => $penawaran->first()->penawaranPenjualan->pelanggan,
			];
		} catch (Exception $e) {
			$response = [
				'status'  => false,
				'message' => $e->getMessage(),
			];
		}

		return response()->json($response);
	}

	public function updateItemCart(Request $request)
	{
		try {
			$data = null;
			$cart = Cart::instance('pesanan-penjualan-' . auth()->user()->id);

			// Kondisi type dari penawaran atau barang
			if ('penawaran' == $request->type) {
				$data = $this->model->getPenawaranPenjualan($request);
			} elseif ('barang' == $request->type) {
				$data = $this->model->getProduk($request);
			}

			if (null !== $data) {
				// samakan cart dan data
				$checkItemCart = $cart->search(function ($cartItem) use ($data) {
					return $cartItem->id === $data->id;
				});

				// ambil yang pertama
				$existCart = $checkItemCart->first();

				// cek apabila attribute ada yang kosong
				$harga       = null;
				$nama_barang = null;
				// cek harga
				if (null !== $data->harga_jual) {
					$harga = $data->harga_jual;
				} elseif (null !== $data->harga) {
					$harga = $data->harga;
				} else {
					$harga = 0;
				}
				// cek nama barang
				if (null !== $data->keterangan) {
					$nama_barang = $data->keterangan;
				} elseif (null !== $data->item_deskripsi) {
					$nama_barang = $data->item_deskripsi;
				}

				// perhitungan
				$subTotalDiskon = ($harga * ($request->diskon ?? 0)) / 100;
				$priceReal      = $harga - $subTotalDiskon;
				$totalDiskon    = $priceReal * ($request->qty);

				// update cart
				Cart::instance('pesanan-penjualan-' . auth()->user()->id)->update($existCart->rowId, [
					'id'      => $data->id,
					'name'    => $nama_barang,
					'qty'     => $request->qty,
					'price'   => $harga ?? 0,
					'options' => [
						'type'           => $request->type,
						'produk'         => $data,
						'harga_modal'    => $existCart->options->harga_modal,
						'harga_terakhir' => $existCart->options->harga_terakhir,
						'price_real'     => $priceReal,
						'price_final'    => $totalDiskon,
						'order_id'       => $existCart->options->order_id,
						'diskon'         => $request->diskon,
					],
				]);

				$response = [
					'status'   => true,
					'template' => view('pos::pesanan_penjualan_pos.cart_penjualan')->render(),
					'total'    => $this->getCartTotal(),
				];
			} else {
				$response = [
					'status' => false,
				];
			}
		} catch (Exception $e) {
			$response = [
				'status'  => false,
				'message' => $e->getMessage(),
			];
		}

		return response()->json($response);
	}
}
