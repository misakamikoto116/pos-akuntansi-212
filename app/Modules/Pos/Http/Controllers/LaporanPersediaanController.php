<?php

namespace App\Modules\Pos\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;

class LaporanPersediaanController extends Controller
{
    protected function identitasPerushaan()
    {
        $nama_perusahaan        =       Identitas::first()->nama_perusahaan;
        return $nama_perusahaan;
    }

    public function dateCarbon($ubah)
    {
        $format     =       Carbon::parse($ubah)->format('d F Y');
        return $format;
    }

    /**
     * Laporan Kartu Stok Persediaan
     *
     * @method GET
     * @param Request
     * @return view
     */
    public function kartu_stok_persediaan(Request $request)
    {
        $produkDetail = SaldoAwalBarang::pluck('produk_id');
        $transaksi    = Produk::with(['saldoAwalBarang.transaksi' => function($query){
                                            $query->where('item_type', '!=', Akun::class)
                                                  ->where('item_type', '!=', Produk::class)
                                                  ->where('item_type', '!=', PenerimaanPenjualan::class);
                                        },'saldoAwalBarang.produk',
                                        'saldoAwalBarang.transaksi' => function ($queryy){
                                            $queryy->where('item_type', '!=', Akun::class)
                                                  ->where('item_type', '!=', Produk::class)
                                                  ->where('item_type', '!=', PenerimaanPenjualan::class);
                                        },'gudang'])
                            ->where('tipe_barang', '=', '0')
                            ->whereIn('id', $produkDetail)
                            ->DateFilter($request);
        $array = [];
        foreach ($transaksi->get() as $dataTransaksi) {
            foreach ($dataTransaksi->saldoAwalBarang as $data) {
                if ($data->kuantitas > 0) {
                    $qtyMasuk  = $data->kuantitas;
                    $qtyKeluar = 0;
                    $qty       = ($qtyMasuk ?? 0);
                } elseif ($data->kuantitas < 0) {
                    $qtyMasuk  = 0;
                    $qtyKeluar = $data->kuantitas;
                    $qty       = ($qtyKeluar ?? 0);
                }
                if ($data->transaksi) {
                    foreach ($data->transaksi->item->barang as $dataBarang) {
                        $array[] = [
                            'no_barang'  => $dataTransaksi->no_barang,
                            'nm_barang'  => $dataTransaksi->keterangan,
                            'kts_saldo'  => $dataTransaksi->kuantitas,
                            'tanggal'    => $this->dateCarbon($data->tanggal) ?? null,
                            'tipe'       => $data->transaksi->sumber ?? null,
                            'no_faktur'  => $data->transaksi->no_transaksi ?? 0,
                            'keterangan' => $data->transaksi->keterangan,
                            'kts_masuk'  => $qtyMasuk ?? 0,
                            'kts_keluar' => $qtyKeluar ?? 0,
                            'kuantitas'  => $qty ?? 0,
                            'gudang'     => $dataBarang->gudang->nama ?? 0,
                        ];
                    }
                }
            }
        }
        $newCollection = collect($array)->groupBy('no_barang');

        $view = [
            'title'        => 'Laporan Kartu Stok Persediaan',
            'perusahaan'   => $this->identitasPerushaan(),
            'dari_tanggal' => $this->dateCarbon($request['start-date']),
            'ke_tanggal'   => $this->dateCarbon($request['end-date']),
            'item'         => $newCollection,
        ];

        return view('akuntansi::laporan.include_laporan.detail_laporan.laporan_persediaan.kartu_stok_persediaan')->with($view);
    }
}
