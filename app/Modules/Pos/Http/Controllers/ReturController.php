<?php

namespace App\Modules\Pos\Http\Controllers;


use Generator\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class ReturController extends Controller
{
    public function __construct(RepositoryInterface $model)
    {
        $this->role = 'pos';
        $this->model = $model;
        $this->title = 'Retur Penjualan';
        // $this->request = PesananPenjualanPosRequest::class;
        // $this->requestField = [];
    }

    public function checkTransaksi(Request $request)
    {
        $check = $this->model->checkTransaction($request);
        $data = [
            'item'      => $check,
            'gudang'    => $this->model->listGudang(),
            'returnUrl' => $request->get('currentUrl')
        ];
        if($check){
            return view('pos::retur.create')->with($data);
        }

        $request->session()->flash('flash_notification', [
            'level'     => 'danger',
            'message'   => 'Nomor transaksi tidak ditemukan!'
        ]);

        return redirect()->back();
    }

    public function submitRetur(Request $request)
    {
        $this->model->processTransaction($request);

        $request->session()->flash('flash_notification', [
            'level'     => 'success',
            'message'   => 'Retur penjualan berhasil!'
        ]);

        $url = (empty($request->get('currentUrl'))) ? '/' : $request->get('currentUrl') ;
        
        return redirect($url);
    }
}
