<?php

namespace App\Modules\Pos\Http\Controllers;

use App\Events\DisplayKasirEvent;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\BarangReturPenjualan;
use App\Modules\Akuntansi\Models\DisplayIklan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\ReturPenjualan;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\TypeEdc;
use App\Modules\Pos\Http\Requests\PenjualanRequest;
use App\Modules\Pos\Models\AkibatPromosi;
use App\Modules\Pos\Models\CartPenjualan;
use App\Modules\Pos\Models\Penjualan;
use App\Modules\Pos\Models\Promosi;
use App\Modules\Pos\Models\SebabPromosi;
use Auth;
use Carbon\Carbon;
use Cart;
use Exception;
use Generator\Interfaces\RepositoryInterface;
use Helpers\PenjualanApi;
use Helpers\TransaksiHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use PDF;
use Session;

class PenjualanController extends Controller
{
    protected $iklan;
    private $totalDiskon;

    public function __construct(RepositoryInterface $model, DisplayIklan $iklan, TransaksiHelper $helper, Produk $produk, Gudang $gudang, TypeEdc $typeEdc)
    {
        $this->role = 'pos';
        $this->model = $model;
        $this->iklan = $iklan;
        $this->title = 'Penjualan';
        $this->request = PenjualanRequest::class;
        $this->requestField = [];
        $this->today = date('Y-m-d H:i:s');
        $this->helper = $helper;
        $this->produk = $produk;
        $this->gudang = $gudang;
        $this->typeEdc = $typeEdc;
    }

    public function index()
    {
        $carts = new CartPenjualan;
        $cart = $carts->last();

        return view('pos::penjualan.index')->with([
            'getPelangganUmum'      => $this->getPelangganUmum(),
            'getCartTotal'          => ($cart) ? $cart->getFinalPrice() : 0,
            'carts'                 => $carts,
            'cart'                  => $cart,
            // 'getCartTotal'          => $this->getCartTotal(),
            'no_penjualan'          => $this->model->generateNoPenjualan(),
            'pelangganPOS'          => $this->model->getInformasiPelangganPos(),
            'typeEdc'               => $this->model->getTypeEdc(),
            'getCartEdcDebitTotal'  => $this->getCartEdcDebitTotal(),
            'getEdcDebitPelunasan'  => $this->getEdcDebitPelunasan(),
            'getTanggalCustom'      => $this->getTanggalCustom(),
        ]);
    }

    public function kasirCheckIn()
    {
        $view = [
            'kasir_mesin'           => $this->model->getKasirMesin(),
            'preferensi_pos'        => $this->model->listPreferensiPos(),
        ];

        return view('pos::penjualan.check_in')->with($view);
    }

    public function kasirCheckInStore(Request $request)
    {
        return $this->model->kasirCheckInStore($request);
    }

    public function kasirCheckOutStore(Request $request)
    {
        $data = $this->model->kasirCheckOutStore($request);
        $pdf = PDF::setOptions(['dpi' => 203, 'defaultFont' => 'sans-serif'])->loadView('pos::penjualan.report.check_out', $data)->setPaper([0, 0, 227, (235 + (3 * 30))]);
        $pdf->save(storage_path('app/report_cek_out/'. $data['nomor_report'] .'.pdf'));

        // if ($request->get('isNative', 0) === 0) {
        return [
                'url' => route('pos.penjualan.view-pdf', $data['nomor_report']),
                'data' => $data
            ];
        // }
    }

    public function viewPdf($nomor_report)
    {
        $filePath = storage_path("app/report_cek_out/{$nomor_report}.pdf");
        if (Storage::exists($filePath)) {
            abort('404');
        }
        return response()->file($filePath);
    }

    public function kasirCheckOutStoreJson(Request $request)
    {
        return $this->model->kasirCheckOutStoreJson($request);
    }

    public function getTransaksi(Request $request)
    {
        $view = [
            'title'         => 'Rekap Transaksi Penjualan',
            'items'         => $this->model->getTransaksi()->where('created_by', auth()->user()->id)->whereDate('invoice_date', Carbon::now()->format('Y-m-d'))->paginate(10),
            'nama_kasir'    => auth()->user()->name,
            'tanggal'       => Carbon::now()->format('d F Y'),
            'total'         => $this->getTotalRekap(),
            'filter'       => [
                'kode_transaksi_pos',
            ],
            'module_url'    => (Object)[
                'index' => 'pos.penjualan.kasir.transaksi',
            ],
        ];

        return view('pos::transaksi.transaksi')->with($view);
    }

    public function getTotalRekap()
    {
        $totals     = $this->model->getTransaksi()->where('created_by', auth()->user()->id)->whereDate('invoice_date', Carbon::now()->format('Y-m-d'))->get();
        $total_all  = 0;

        foreach ($totals as $key => $total) {
            $sum_retur  = $total->returPenjualan->sum('total');
            $total_all += $total->sum_harga_total;
            $total_all  = $total_all - $sum_retur;
        }

        return $total_all;
    }

    public function getDetailTransaksi($id, $status)
    {
        
        if ($status === 'retur') {
            $faktur     = ReturPenjualan::findOrFail($id);
            $barang     = BarangReturPenjualan::where('retur_penjualan_id', $id)->get();
        }else {
            $faktur     = FakturPenjualan::findOrFail($id);
            $barang     = BarangFakturPenjualan::where('faktur_penjualan_id', $id)->get();
        }
        
        $view = [
            'title'             => 'Detail Transaksi',
            'items'             => $barang,
            'tanggal'           => !empty($faktur->invoice_date) ? Carbon::parse($faktur->invoice_date)->format('d F Y H:i:s') : Carbon::parse($faktur->tanggal)->format('d F Y H:i:s'), 
            'kode_transaksi'    => !empty($faktur->no_faktur) ? $faktur->no_faktur : $faktur->sr_no,
            'id'                => $faktur->id,
        ];

        return view('pos::transaksi.detail_transaksi')->with($view);
    }

    public function getPesananPenjualan(Request $request)
    {
        return $this->model->getAllPesananPenjualan($request);
    }

    public function getPelanggan(Request $request)
    {
        return $this->model->getAllPelanggan($request);
    }

    public function store()
    {
        $request = request();
        $cart = CartPenjualan::findOrCreate($request->cart_id);
        // $cart = Cart::instance('penjualan-'.auth()->user()->id);

        if ($cart->items->count() <= 0) {
            return response()->json([
                'status' => false,
                'message' => 'Item masih kosong',
            ]);
        }

        if ($this->model->getPreferensiPos() !== true) {
            return response()->json([
                'status' => false,
                'message' => 'Tolong isi preferensi akun default',
            ]);
        }

        if ($this->model->getInformasiPelangganPos() === null) {
            return response()->json([
                'status' => false,
                'message' => 'Pelanggan Pos Tidak tersedia',
            ]);
        }
        $request['cart'] = $cart;
        return $this->model->storePos($request);
    }

    public function getProduk(Request $request)
    {
        return $this->model->getAllProduk($request);
    }

    public function getProdukTest(Request $request)
    {
        return $this->model->getAllProdukTest($request);
    }

    public function getKasirMesin(Request $request)
    {
        return $this->model->getAllKasirMesin($request);
    }

    public function getProdukBarcode(Request $request)
    {

        $barcode = $request->get('barcode') ?? $request->get('q'); 

        if ($barcode !== null) {

            $produk_id = Produk::where('no_barang', $barcode)->pluck('id');
            
            if ($produk_id->isNotEmpty()) {
                
                $request->barang_id = $produk_id->first();

                return response()->json($this->addItem($request));

            }else {

                $view = [
                    'status'        => false,
                    'message'       => 'Data tidak ditemukan',
                ];

                return response()->json($view);

            }

        
        }else {

            $view = [
                'status'        => false,
                'message'       => 'Barcode tidak boleh kosong',
            ];

            return response()->json($view);
        }
    }

    public function getProdukBarcodeCariBarang(Request $request)
    {
        $barcode  = $request->get('barcode');

        $gudang_toko = $this->gudang->where('nama', 'Gudang Toko')->pluck('id');

        $produk = $this->produk->whereNotIn('tipe_barang', ['3','5','4'])
                               ->whereHas('saldoAwalBarang', function ($q) use ($gudang_toko) {
                                   $q->whereIn('gudang_id', $gudang_toko);
                               })
                               ->where(function ($query) use ($barcode)
                               {
                                   $query->advancedSearch($barcode);
                                   // if (strpos($barcode, ' ') === false ) {
                                   //     $query->where('no_barang', 'LIKE', '%' .$barcode. '%')
                                   //           ->orWhere('keterangan', 'LIKE', '%' .$barcode. '%')
                                   //           ->orWhere('nama_singkat', 'LIKE', '%' .$barcode. '%')
                                   //           ->orWhere('barcode', 'LIKE', '%' .$barcode. '%'); 
                                   // }else {
                                   //      $arr_barcode = explode(' ', $barcode);
                                   //      foreach ($arr_barcode as $key => $barcode_arr) {
                                   //          $query->where('keterangan', 'LIKE', '%' .$barcode_arr. '%')
                                   //                ->orWhere('nama_singkat', 'LIKE', '%' .$barcode_arr. '%');
                                   //      }
                                   // }
                               })->get();




        if (!empty($barcode)) {
            
            if ($produk->isNotEmpty()) {
                $produks = $produk->map(function ($item)
                              {
                                  return [
                                    'no_barang'     => $item->no_barang,
                                    'barcode'       => $item->barcode,
                                    'nama_barang'   => $item->keterangan,
                                    'harga_jual'    => $item->harga_jual,
                                    'qty'           => $item->updated_qty,
                                  ];
                              });

                $response = [
                    'status'    => true,
                    'template'  => view('pos::penjualan.pencarian_barang', compact('produks'))->render(),
                    'message'   => 'Data ditemukan'
                ];

            }else {

                $response = [
                    'status'    => false,
                    'template'  => null,
                    'message'   => 'Data tidak ditemukan'
                ];

            }

        }else {

            $response = [
                'status'    => false,
                'template'  => null,
                'message'   => 'Barcode / No Barang tidak boleh kosong'
            ];

        }

        return response()->json($response);
    }

    public function getCartTotal()
    {
        $total = 0;
        if(Cart::instance('penjualan-diskon-'.auth()->user()->id)->count() > 0){
            $cart = Cart::instance('penjualan-diskon-'.auth()->user()->id);
        }else{
            $cart = Cart::instance('penjualan-'.auth()->user()->id);
        }

        foreach ($cart->content() as $item) {
            $total += $item->options->price_final;
        }

        return $total;
    }

    public function addItem(Request $request)
    {
        try {
            $produk = $this->model->getProduk($request);

            // $cart = Cart::instance('penjualan-'.auth()->user()->id);

            // $checkItemCart = $cart->search(function ($cartItem) use ($produk) {
            //     return $cartItem->id === $produk->id;
            // });
            $cart = CartPenjualan::findOrCreate($request->cart_id);
            $checkItemCart = $cart->items->where('id', $produk->id);

            $promotion = Promosi::where([
                ['expired_date','>=',$this->today],
                ['active_date','<=',$this->today],
            ])->get();
            $promoWarning = "";
            foreach ($promotion as $promo) {
                foreach ($promo->sebab as $itemSebab) {
                    if ($produk->id == $itemSebab->produk_id) {
                        $promoWarning = "<h5><a target='__blank' style='color:#fff;' href='".url('pos/promosi/'.$promo->id)."'> Barang ".$itemSebab->produk->keterangan." sedang promo!
                        <b> Cek Disini</b></a></h5>";
                    }
                }

                foreach ($promo->akibat as $itemAkibat) {
                    foreach ($itemAkibat->sebab as $itemSebab) {
                        if ($produk->id == $itemSebab->produk_id) {
                            $promoWarning = "<h5><a target='__blank' style='color:#fff;' href='".url('pos/promosi/'.$promo->id)."'> Barang ".$itemSebab->produk->keterangan." sedang promo!
                                                <b> Cek Disini</b></a></h5>";
                        }
                    }
                }
            }

            $request->qty = $this->checkQtyForCart($request);

            // $harga_jual = $this->getTingkatHargaJual($produk, $request);

            if ($checkItemCart->isEmpty()) {
                $subTotalDiskon = ($produk->harga_jual * ($produk->diskon ?? 0)) / 100;
                $priceReal = $produk->harga_jual - $subTotalDiskon;
                $totalDiskon = $priceReal * $request->qty;

                $cart->addItem([
                    'id' => $produk->id,
                    'name' => $produk->keterangan,
                    'qty' => $request->qty,
                    'price' => $produk->harga_jual ?? 0,
                    'tipe_barang' => $produk->tipe_barang ?? null,
                    'produk' => $produk,
                    'harga_modal' => $produk->saldoAwalBarang->last()->harga_modal ?? 0,
                    'harga_terakhir' => $produk->saldoAwalBarang->last()->harga_terakhir ?? 0,
                    'price_real' => $priceReal,
                    'price_final' => $totalDiskon,
                    'order_id' => $cart->items->isEmpty() ?
                                    1 :
                                    $cart->items->count() + 1,
                    'diskon' => 0
                ]);
                // send_display_kasir([
                //     'barang' => $produk->keterangan,
                //     'qty' => $request->qty,
                //     'price' => $produk->harga_jual ?? 0,
                //     'subtotal' => $request->qty * $produk->harga_jual ?? 0,
                //     'total' => $this->getCartTotal(),
                //     'kasir' => 'kasir_'.auth()->user()->id,
                // ]);

                $kasirId        = 'kasir_'.auth()->user()->id; 
                $dataToDisplay  = [
                    'barang' => $produk->keterangan,
                    'qty' => $request->qty,
                    'price' => $produk->harga_jual ?? 0,
                    'subtotal' => $request->qty * $produk->harga_jual ?? 0,
                    'total' => $this->getCartTotal(),
                    'kasir' => 'kasir_'.auth()->user()->id,
                ];
                
            } else {
                $existCart = $checkItemCart->first();
                
                $subTotalDiskon = ($produk->harga_jual * ($produk->diskon ?? 0)) / 100;
                $priceReal = $produk->harga_jual - $subTotalDiskon;
                $totalDiskon = $priceReal * ($existCart['qty'] + $request->qty);

                $cart->updateItem([
                    'id' => $produk->id,
                    'name' => $produk->keterangan,
                    'qty' => $existCart['qty'] + $request->qty,
                    'price' => $produk->harga_jual ?? 0,
                    'tipe_barang' => $produk->tipe_barang ?? null,
                    'produk' => $produk,
                    'harga_modal' => $existCart['harga_modal'],
                    'harga_terakhir' => $existCart['harga_terakhir'],
                    'price_real' => $priceReal,
                    'price_final' => $totalDiskon,
                    'order_id' => $existCart['order_id'],
                    'diskon' => 0
                ]);

                // send_display_kasir([
                //     'barang' => $produk->keterangan,
                //     'qty' => $existCart->qty,
                //     'price' => $produk->harga_jual ?? 0,
                //     'subtotal' => ($existCart->qty) * $produk->harga_jual ?? 0,
                //     'total' => $this->getCartTotal(),
                //     'kasir' => 'kasir_'.auth()->user()->id,
                // ]);
                $kasirId        = 'kasir_'.auth()->user()->id;
                $dataToDisplay  = [
                    'barang' => $produk->keterangan,
                    'qty' => $existCart['qty'],
                    'price' => $produk->harga_jual ?? 0,
                    'subtotal' => ($existCart['qty']) * $produk->harga_jual ?? 0,
                    'total' => $cart->getFinalPrice(),
                    'kasir' => 'kasir_'.auth()->user()->id,
                ];
            }

            event(new DisplayKasirEvent($kasirId, $dataToDisplay));

            $this->helper->makeCartLog($produk->id, $request->qty, $produk->harga_jual, "insert", $request->no_penjualan);
            Cart::instance('penjualan-diskon-'.auth()->user()->id)->destroy();
            // Session::forget('terdiskon-'.Auth::user()->id);
            
            $response = [
                'status' => true,
                'template' => view('pos::penjualan.cart_penjualan', ['cart' => $cart])->render(),
                'total' => $cart->getFinalPrice(),
                'total_items_cart' => count($cart->items),
                'promo' => $promoWarning
            ];
        } catch (Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage(),
            ];
        }

        return response()->json($response);
    }

    public function editHargaJual(Request $request)
    {
        try {

            $data = null;
            // $cart = Cart::instance('penjualan-' . auth()->user()->id);
            $cart = CartPenjualan::findOrCreate($request->cart_id);
            $data = $this->model->getProduk($request);
            if ($data !== null) {
                // samakan cart dan data
                // $checkItemCart = $cart->search(function ($cartItem) use ($data) {
                //     return $cartItem->id === $data->id;
                // });
                $checkItemCart = $cart->items->where('id', $data['id']);

                // ambil yang pertama
                $existCart = $checkItemCart->first();

                // qty update or not
                $request->qty !== null ? $qty = $request->qty : $qty = $existCart->qty;
                
                // diskon persen
                $request->diskon_persen !== null ? $diskon_persen = $request->diskon_persen : $diskon_persen = $data->diskon;

                // diskon rupiah
                $request->diskon_rupiah !== null ? $diskon_rupiah = $request->diskon_rupiah : $diskon_rupiah = 0;

                // perhitungan
                $subTotalDiskon     = ($request->harga * ($diskon_persen ?? 0)) / 100;
                $priceReal          = $request->harga - $subTotalDiskon - $diskon_rupiah;
                $totalDiskon        = $priceReal * ($qty);

                $existCart = [
                    "id"                => $data->id, 
                    "name"              => $data->keterangan,
                    "qty"               => $qty, 
                    "price"             => $request->harga, 
                    "produk"            => $data, 
                    "harga_modal"       => $data->saldoAwalBarang->last()->harga_modal,
                    "harga_terakhir"    => $data->saldoAwalBarang->last()->harga_terakhir, 
                    "price_real"        => $priceReal, 
                    "price_final"       => $totalDiskon, 
                    "order_id"          => $existCart['order_id'], 
                    "diskon"            => $diskon_persen, 
                    'diskon_rupiah'     => $diskon_rupiah
                ];

                $cart->updateItem($existCart);
                // $itemIndex = $cart->findItem($data['id']);
                // if ($itemIndex !== false) {
                //     dd($cart->items->put($itemIndex, $existCart));
                //     dd($cart->syncToCart());
                //     $cart->syncToCart();
                // }
                // Cart::instance('penjualan-' . auth()->user()->id)->update($existCart->rowId, [
                //     'id'        => $data->id,
                //     'name'      => $existCart->name,
                //     'qty'       => $qty,
                //     'price'     => $request->harga,
                //     'options'   => [
                //         'produk'            => $data,
                //         'harga_modal'       => $existCart->options->harga_modal,
                //         'harga_terakhir'    => $existCart->options->harga_terakhir,
                //         'price_real'        => $priceReal,
                //         'price_final'       => $totalDiskon,
                //         'order_id'          => $existCart->options->order_id,
                //         'diskon'            => $diskon_persen,
                //         'diskon_rupiah'     => $diskon_rupiah,
                //     ]
                // ]);

                $response = [
                        'status' => true,
                        // 'template' => view('pos::penjualan.cart_penjualan')->render(),
                        'template' => view('pos::penjualan.cart_penjualan', ['cart' => $cart])->render(),
                        'total' => $cart->getFinalPrice()
                    ];

            // send_display_kasir([
            //     'barang' => $existCart->name,
            //     'qty'    => $existCart->qty,
            //     'price'  => $request->harga,
            //     'subtotal' => ($existCart->qty) * $request->harga ?? 0,
            //     'total' => $this->getCartTotal(),
            //     'kasir' => 'kasir_'.auth()->user()->id,
            // ]);
            $kasirId       = 'kasir_'.auth()->user()->id;
            $dataToDisplay = [
                'barang' => $existCart['name'],
                'qty'    => $existCart['qty'],
                'price'  => $request->harga,
                'subtotal' => ($existCart['qty']) * $request->harga ?? 0,
                'total' => $cart->getFinalPrice(),
                'kasir' => 'kasir_'.auth()->user()->id,
            ];

            event(new DisplayKasirEvent($kasirId, $dataToDisplay));

            }else {
                $response = [
                    'status' => false,
                ];    
            }


        } catch (Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        return response()->json($response);

    }

    public function addItemPesananPenjualan(Request $request)
    {
        try {
            $pesanan = $this->model->getPesananPenjualan($request);

            $cart = Cart::instance('penjualan-' . auth()->user()->id);


            // cek apabila ada barang yang sama maka update row list barang yang sama
            foreach ($pesanan as $data_pesanan) {

                // perhitungan pesanan
                $subTotalDiskon     = ($data_pesanan->harga * ($data_pesanan->diskon ?? 0)) / 100;
                $priceReal          = $data_pesanan->harga - $subTotalDiskon;
                $totalDiskon        = $priceReal * $data_pesanan->jumlah;
                
                // tambah row list barang
                Cart::instance('penjualan-' . auth()->user()->id)->add([
                    'id'    => $data_pesanan->id,
                    'name'  => $data_pesanan->item_deskripsi,
                    'qty'   => $data_pesanan->jumlah,
                    'price' => $data_pesanan->harga ?? 0,
                    'options' => [
                        'produk'            => $produk,
                        'harga_modal'       => $data_pesanan->produk->saldoAwalBarang->last()->harga_modal ?? 0,
                        'harga_terakhir'    => $data_pesanan->produk->saldoAwalBarang->last()->harga_terakhir ?? 0,
                        'price_real'        => $priceReal,
                        'price_final'       => $totalDiskon,
                        'diskon'            => $data_pesanan->diskon,
                        'order_id'          => $cart->content()->isEmpty() ?
                                            1 :
                                            $cart->content()->max('options.order_id') + 1
                    ],
                ]);
            }
    
            Cart::instance('penjualan-diskon-'.auth()->user()->id)->destroy();
            Session::forget('terdiskon-'.Auth::user()->id);

                // $response = [
                //     'status' => true,
                //     'data' => $cart,
                //     'template' => view('pos::penjualan.cart_penjualan')->render(),
                //     'total' => $this->getCartTotal(),
                // ];
        
            $response = [
                'status'        => true,
                'template'      => view('pos::penjualan.cart_penjualan')->render(),
                'total'         => $this->getCartTotal(),
                'pelanggan'     => $pesanan->first()->pesananPenjualan->pelanggan,
            ];

        } catch (Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage(),
            ];
        }

        return response()->json($response);
    }

    public function diskonItem(Request $request)
    {
        return $this->model->diskonItem($request);
    }

    public function removeItem(Request $request)
    {
        try {
            if ($request->id !== null) {
                // $cart = Cart::instance('penjualan-'.auth()->user()->id)->get($request->id);

                // $qty = $cart->qty - 1;

                // $produk = $cart->options->produk;

                // $subTotalDiskon = ($produk->harga_jual * ($produk->diskon ?? 0)) / 100;
                // $priceReal = $produk->harga_jual - $subTotalDiskon;
                // $totalDiskon = $priceReal * ($cart->qty - 1);

                // if ($qty == 0) {
                $cart = CartPenjualan::find($request->cart_id);
                $cart->removeItem($request->id);
                // } else {
                //     Cart::instance('penjualan-'.auth()->user()->id)->update($request->id, [
                //         'qty' => $qty,
                //         'options' => [
                //             'produk' => $produk,
                //             'harga_modal' => $cart->options->harga_modal,
                //             'harga_terakhir' => $cart->options->harga_terakhir,
                //             'price_real' => $priceReal,
                //             'price_final' => $totalDiskon,
                //             'order_id' => $cart->options->order_id,
                //             'diskon' => 0
                //         ],
                //     ]);
                // }

                // send_display_kasir([
                //     'barang' => $qty === 0 ? '' : $cart->options->produk->keterangan,
                //     'qty'    => $qty, //$total_qty ?? 0,
                //     'price'  => $qty === 0 ? '' : $cart->options->produk->harga_jual,
                //     'subtotal' => $cart->options->produk->harga_jual * $qty,
                //     'total' => $this->getCartTotal(),
                //     'kasir' => 'kasir_'.auth()->user()->id,
                // ]);

                // send_display_kasir([
                //     'barang' => '',
                //     'qty'    => '', //$total_qty ?? 0,
                //     'price'  => '',
                //     'subtotal' => '',
                //     'total' => $this->getCartTotal(),
                //     'kasir' => 'kasir_'.auth()->user()->id,
                // ]);

                $kasirId       = 'kasir_'.auth()->user()->id; 
                $dataToDisplay = [
                    'barang' => '',
                    'qty'    => '', //$total_qty ?? 0,
                    'price'  => '',
                    'subtotal' => '',
                    'total' => $cart->getFinalPrice(),
                    'kasir' => 'kasir_'.auth()->user()->id,
                ];

                event(new DisplayKasirEvent($kasirId, $dataToDisplay));

                Cart::instance('penjualan-diskon-'.auth()->user()->id)->destroy();
                // Session::forget('terdiskon-'.Auth::user()->id);

                $response = [
                    'status' => true,
                    'remove' => '',
                    'data' => '',
                    'total' => $cart->getFinalPrice(),
                    'template' => view('pos::penjualan.cart_penjualan', ['cart' => $cart])->render(),
                    'total_items_cart' => count($cart->items)
                ];

            } else {
                $response = [
                    'status' => false,
                    'message' => 'Cart Penjualan Tidak Ditemukan!',
                ];
            }
        } catch (Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage(),
            ];
        }

        return response()->json($response);
    }

    public function getResetPenjualan(Request $request)
    {
        $response = false;
        $cart = CartPenjualan::find($request->cart_id);
        if ($cart != null) {
            $response = $cart->resetItem();
        }

        return response()->json($response);
    }

    public function updateItemCart(Request $request)
    {
        try {

            $data = null;
            $cart = Cart::instance('penjualan-' . auth()->user()->id);
            
            // Kondisi type dari penawaran atau barang 
            if ($request->type == "pesanan") {
                $data = $this->model->getPesananPenjualan($request);
            }else if ($request->type == "barang"){
                $data = $this->model->getProduk($request);
            }
       
            if ($data !== null) {

                // samakan cart dan data
                $checkItemCart = $cart->search(function ($cartItem) use ($data) {
                    return $cartItem->id === $data->id;
                });

                // ambil yang pertama
                $existCart = $checkItemCart->first();

                // cek apabila attribute ada yang kosong
                $harga          = null;
                $nama_barang    = null;
                // cek harga
                if ($data->harga_jual !== null) {
                    $harga = $data->harga_jual;
                }else if ($data->harga !== null) {
                    $harga = $data->harga;
                }else {
                    $harga = 0;
                }
                // cek nama barang
                if ($data->keterangan !== null) {
                    $nama_barang = $data->keterangan;
                }else if ($data->item_deskripsi !== null) {
                    $nama_barang = $data->item_deskripsi;
                }
                
                // perhitungan
                $subTotalDiskon     = ($harga * ($request->diskon ?? 0)) / 100;
                $priceReal          = $harga - $subTotalDiskon;
                $totalDiskon        = $priceReal * ($request->qty);

                // update cart
                Cart::instance('penjualan-' . auth()->user()->id)->update($existCart->rowId, [
                    'id'        => $data->id,
                    'name'      => $nama_barang,
                    'qty'       => $request->qty,
                    'price'     => $harga ?? 0,
                    'options'   => [
                        'type'              => $request->type,
                        'produk'            => $data,
                        'harga_modal'       => $existCart->options->harga_modal,
                        'harga_terakhir'    => $existCart->options->harga_terakhir,
                        'price_real'        => $priceReal,
                        'price_final'       => $totalDiskon,
                        'order_id'          => $existCart->options->order_id,
                        'diskon'            => $request->diskon,
                    ]
                ]);

                if ($request->diskon === 0) {
                    $cart = Cart::instance('penjualan-'.auth()->user()->id)->get($request->id);
                    $this->helper->makeCartLog($cart->id, $cart->qty, $cart->price, "deleted", $request->no_penjualan);
                                
                    Cart::instance('penjualan-'.auth()->user()->id)->remove($request->id);
                    Cart::instance('penjualan-diskon-'.auth()->user()->id)->destroy();
                    Session::forget('terdiskon-'.Auth::user()->id);
                } else {
                    $cart = Cart::instance('penjualan-diskon-'.auth()->user()->id)->get($request->id);
                    $this->helper->makeCartLog($cart->id, $cart->qty, $cart->price, "deleted", $request->no_penjualan);
                    Cart::instance('penjualan-diskon-'.auth()->user()->id)->remove($request->id);
                }


                $response = [
                    'status' => true,
                    'template' => view('pos::penjualan.cart_penjualan')->render(),
                    'total' => $this->getCartTotal()
                ];
            }else {
                $response = [
                    'status' => false,
                ];    
            }


        } catch (Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage(),
            ];
        }

        return response()->json($response);
    }

    /**
     * Download Last Sales Report then Logout.
     *
     * REVIEW: [
     *  - Chunk            => FALSE
     *  - Transaction      => FALSE
     *  - Permission       => FALSE
     *  - Unit Testing     => FALSE
     * ]
     *
     * @method GET
     *
     * @param Request
     */
    public function printAndLogout(Request $request)
    {
        // $transactions = FakturPenjualan::get();

        // $view = [
        //     'transactions' => $data
        // ];

        // return view('pos::penjualan.report.transaction')->with($view);

        $pdf = PDF::loadView('pos::penjualan.report.transaction', $transactions);
        $pdf->download('report.pdf');

        // return redirect()->route('logout');
    }

    public function displayKasir()
    {
        $kasir_id = 'kasir_'.auth()->user()->id;
        $iklan = $this->iklan->where('publish', '=', true)
            ->orderBy('urutan', 'ASC')
            ->orderBy('created_at', 'desc')
            ->get();

        return view('pos::penjualan.display_kasir')->with(['kasir_id' => $kasir_id, 'iklan' => $iklan]);
    }

    public function cariDiskon(Request $request)
    {
        if ($request->ajax()) {
            try {
                $totalDiskon = 0;

                $promotion = Promosi::where([
                    ['expired_date','>=',$this->today],
                    ['active_date','<=',$this->today],
                ])->get();

                $this->pindahCart();
                $cart = Cart::instance('penjualan-diskon-'.auth()->user()->id);
                # Cari 1 sebab banyak akibat
                
                $countSebabAkibat = 0;
                // $countAkibatSebab = 0;
                foreach ($cart->content() as $itemBelanja) {
                    if($countSebabAkibat <= 0){
                        foreach ($promotion as $promo) {
                            foreach ($promo->sebab as $sebabItem) {
                                if ($sebabItem->type_sebab == 0) { #Karena Produk
                                    if ($itemBelanja->id == $sebabItem->produk_id && $itemBelanja->qty >= $sebabItem->qty) {
                                        if ($sebabItem->kelipatan == 0) { #Tidak berlaku Kelipatan
                                            $i = 1;
                                        } else { #Berlaku kelipatan
                                            $i = floor($itemBelanja->qty / $sebabItem->qty);
                                        }

                                        for ($x = 0; $x < $i; $x++) {
                                            # Diskon suntik ke per item, ntah tambah item atau itemnya dikasih diskon aja
                                            $this->promosiSebabAkibat($cart, $itemBelanja, $sebabItem, $request->no_penjualan);
                                        }
                                    }
                                } else {  #Totalan bukan karena produk
                                    #Check total belanjaan
                                    if ($sebabItem->kelipatan == 0) { #Tidak berlaku Kelipatan
                                        $i = 1;
                                    } else { #Berlaku kelipatan
                                        $i = floor($this->getCartTotal() / $sebabItem->total);
                                    }

                                    if ($sebabItem->tipe_barang === null) {
                                        $sebabTotal = $sebabItem->total;
                                    } else {
                                        $sebabTotal = $this->sumByCategory($cart, $sebabItem->tipe_barang);
                                    }
                                    for ($x = 0; $x < $i; $x++) {
                                        if ($this->getCartTotal() >= $sebabTotal && $sebabItem->akibat()->first()->produk_id == null) {
                                            $totalDiskon += $sebabItem->akibat()->sum('total');
                                        } else {
                                            if ($this->getCartTotal() >= $sebabTotal) {
                                                $this->promosiSebabAkibat($cart, $itemBelanja, $sebabItem, $request->no_penjualan);
                                            }
                                        }
                                    }
                                    $countSebabAkibat++;
                                }
                            }
                        }
                    }
                }
                

                # Cari 1 Akibat banyak sebab. for this case, sebab haruslah solid (kalau krn produk ya hrs krn produk semua)
                $sebabTotal = 0;
                foreach ($promotion as $promo) {
                    foreach ($promo->akibat as $akibatItem) {
                        $dapatPromo = 0;
                        $dapatDiskonTotal = 0;
                        $kelipatan = 0;
                        $collectionDiskon = [];
                        foreach ($akibatItem->sebab as $sebabItem) {
                            foreach ($cart->content() as $itemBelanja) {
                                if ($itemBelanja->id == $sebabItem->produk_id && $itemBelanja->qty >= $sebabItem->qty && $sebabItem->type_sebab == 0) {
                                    $dapatPromo++;
                                    if ($sebabItem->kelipatan == 1) {
                                        $kelipatan++;
                                        $collectionDiskon[] = $itemBelanja;
                                    }
                                }

                                if ($itemBelanja->id == $sebabItem->produk_id && $itemBelanja->qty >= $sebabItem->qty && $sebabItem->type_sebab == 1) {
                                    $dapatDiskonTotal++;
                                    if ($sebabItem->kelipatan == 1) {
                                        $kelipatan++;
                                        $collectionDiskon[] = $itemBelanja;
                                    }
                                }
                            }
                        }

                        if ($kelipatan > 0) {
                            $i = collect($collectionDiskon)->min('qty');
                        } else {
                            $i = 1;
                        }

                        for ($x = 0; $x < $i; $x++) {
                            if ($dapatPromo == $akibatItem->sebab->count()) {
                                $produk = Produk::find($akibatItem->produk_id);

                                $checkItemCart = $cart->search(function ($cartItem) use ($produk) {
                                    return $cartItem->id === $produk->id;
                                })->first();
                                $this->promoAkibatSebab($cart, $akibatItem, $checkItemCart, $request->no_penjualan);
                            } else {
                                if ($dapatDiskonTotal == $akibatItem->sebab->count()) {
                                    $totalDiskon += $akibatItem->total;
                                }
                            }
                        }
                    }
                }
                Session::put('terdiskon-'.Auth::user()->id, '1');
                $response = [
                    'status' => true,
                    'template' => view('pos::penjualan.cart_penjualan')->render(),
                    'total' => $this->getCartTotal(),
                    'total_diskon' => $totalDiskon
                ];
            } catch (Exception $e) {
                $response = [
                    'status' => false,
                    'message' => $e->getMessage(),
                ];
            }
            return response()->json($response);
        }
    }

    public function sumByCategory($cart, $num) : float
    {
        return $cart->content()->whereIn('tipe_barang', $num)->sum(function ($item) {
            return $item->qty * $item->price;
        });
    }

    public function promosiSebabAkibat($cart, $itemBelanja, $sebabItem, $no_penjualan)
    {
        foreach ($sebabItem->akibat as $akibatItem) {
            $pengali = ($akibatItem->qty  <= 0) ? 1 : $akibatItem->qty;
            if ($akibatItem->produk_id == $itemBelanja->id) { #Syaratnya produk_id harus sama
                Cart::instance('penjualan-diskon-'.auth()->user()->id)->update($itemBelanja->rowId, [
                    'id' => $itemBelanja->id,
                    'name' => $itemBelanja->name,
                    'qty' => $itemBelanja->qty + $akibatItem->qty,
                    'price' => $itemBelanja->price ?? 0,
                    'options' => [
                        'produk' => $itemBelanja->options->produk,
                        'harga_modal' => $itemBelanja->options->harga_modal,
                        'harga_terakhir' => $itemBelanja->options->harga_terakhir,
                        'price_real' => $itemBelanja->options->price_real,
                        'price_final' => $itemBelanja->options->price_final - ($itemBelanja->options->price_real * $pengali - ($pengali * $akibatItem->total)),
                        'order_id' => $itemBelanja->options->order_id,
                        'diskon' => $itemBelanja->options->diskon + ($akibatItem->total * $pengali)
                    ],
                ]);
                
                $this->helper->makeCartLog($itemBelanja->id, $akibatItem->qty, $akibatItem->total, "insert-diskon", $no_penjualan);
                
                // send_display_kasir([
                //     'barang' => $itemBelanja->name,
                //     'qty' => $itemBelanja->qty + $akibatItem->qty,
                //     'price' => $itemBelanja->price ?? 0,
                //     'subtotal' => $itemBelanja->options->price_final - ($itemBelanja->options->price_real * $pengali - ($pengali * $akibatItem->total)),
                //     'total' => $this->getCartTotal(),
                //     'kasir' => 'kasir_'.auth()->user()->id,
                // ]);

                $kasirId       = 'kasir_'.auth()->user()->id; 
                $dataToDisplay = [
                    'barang' => $itemBelanja->name,
                    'qty' => $itemBelanja->qty + $akibatItem->qty,
                    'price' => $itemBelanja->price ?? 0,
                    'subtotal' => $itemBelanja->options->price_final - ($itemBelanja->options->price_real * $pengali - ($pengali * $akibatItem->total)),
                    'total' => $this->getCartTotal(),
                    'kasir' => 'kasir_'.auth()->user()->id,
                ];

            } else {
                $produk = Produk::find($akibatItem->produk_id);
                $subTotalDiskon = ($produk->harga_jual * ($produk->diskon ?? 0)) / 100;
                $priceReal = $produk->harga_jual - $subTotalDiskon;
                $totalDiskon = $priceReal * $akibatItem->qty;
                                                        
                Cart::instance('penjualan-diskon-'.auth()->user()->id)->add([
                    'id' => $produk->id,
                    'name' => $produk->keterangan,
                    'qty' => $akibatItem->qty,
                    'price' => $produk->harga_jual ?? 0,
                    'options' => [
                        'produk' => $produk,
                        'harga_modal' => $produk->saldoAwalBarang->last()->harga_modal ?? 0,
                        'harga_terakhir' => $produk->saldoAwalBarang->last()->harga_terakhir ?? 0,
                        'price_real' => $priceReal,
                        'price_final' => $priceReal * $pengali - ($pengali * $akibatItem->total),
                        'order_id' => $cart->content()->isEmpty() ?
                                    1 :
                                    $cart->content()->max('options.order_id') + 1,
                        'diskon' => $totalDiskon * $pengali
                    ],
                ]);

                $this->helper->makeCartLog($produk->id, $akibatItem->qty, $akibatItem->total, "insert-diskon", $no_penjualan);
         
                // send_display_kasir([
                //     'barang' => $produk->keterangan,
                //     'qty' => $akibatItem->qty,
                //     'price' => $produk->harga_jual ?? 0,
                //     'subtotal' => $priceReal * $pengali - ($pengali * $akibatItem->total),
                //     'total' => $this->getCartTotal(),
                //     'kasir' => 'kasir_'.auth()->user()->id,
                // ]);

                $kasirId       = 'kasir_'.auth()->user()->id;
                $dataToDisplay = [
                    'barang' => $produk->keterangan,
                    'qty' => $akibatItem->qty,
                    'price' => $produk->harga_jual ?? 0,
                    'subtotal' => $priceReal * $pengali - ($pengali * $akibatItem->total),
                    'total' => $this->getCartTotal(),
                    'kasir' => 'kasir_'.auth()->user()->id,
                ];

                $totalDiskon = 0;
            }

            event(new DisplayKasirEvent($kasirId, $dataToDisplay));

        }
    }

    public function promoAkibatSebab($cart, $akibatItem, $checkItemCart, $no_penjualan)
    {
        $pengali = ($akibatItem->qty  <= 0) ? 1 : $akibatItem->qty;
        if (!empty($checkItemCart->id)) { #Syaratnya produk_id harus sama
            Cart::instance('penjualan-diskon-'.auth()->user()->id)->update($checkItemCart->rowId, [
                'id' => $checkItemCart->id,
                'name' => $checkItemCart->name,
                'qty' => $checkItemCart->qty + $akibatItem->qty,
                'price' => $checkItemCart->price ?? 0,
                'options' => [
                    'produk' => $checkItemCart->options->produk,
                    'harga_modal' => $checkItemCart->options->harga_modal,
                    'harga_terakhir' => $checkItemCart->options->harga_terakhir,
                    'price_real' => $checkItemCart->options->price_real,
                    'price_final' => $itemBelanja->options->price_final - ($itemBelanja->options->price_real * $pengali - ($pengali * $akibatItem->total)),
                    'order_id' => $checkItemCart->options->order_id,
                    'diskon' => $itemBelanja->options->diskon + ($akibatItem->total * $pengali)
                ],
            ]);
            $this->helper->makeCartLog($checkItemCart->id, $akibatItem->qty, $akibatItem->total, "insert-diskon", $no_penjualan);
                                                                    
            // send_display_kasir([
            //     'barang' => $checkItemCart->name,
            //     'qty' => $checkItemCart->qty + $akibatItem->qty,
            //     'price' => $checkItemCart->price ?? 0,
            //     'subtotal' => $itemBelanja->options->price_final - ($itemBelanja->options->price_real * $pengali - ($pengali * $akibatItem->total)),
            //     'total' => $this->getCartTotal(),
            //     'kasir' => 'kasir_'.auth()->user()->id,
            // ]);

            $kasirId       = 'kasir_'.auth()->user()->id;
            $dataToDisplay = [
                'barang' => $checkItemCart->name,
                'qty' => $checkItemCart->qty + $akibatItem->qty,
                'price' => $checkItemCart->price ?? 0,
                'subtotal' => $itemBelanja->options->price_final - ($itemBelanja->options->price_real * $pengali - ($pengali * $akibatItem->total)),
                'total' => $this->getCartTotal(),
                'kasir' => 'kasir_'.auth()->user()->id,
            ];



        } else {
            $produk = Produk::find($akibatItem->produk_id);
            $subTotalDiskon = ($produk->harga_jual * ($produk->diskon ?? 0)) / 100;
            $priceReal = $produk->harga_jual - $subTotalDiskon;
            $totalDiskon = $priceReal * $akibatItem->qty;

            Cart::instance('penjualan-diskon-'.auth()->user()->id)->add([
                'id' => $produk->id,
                'name' => $produk->keterangan,
                'qty' => $akibatItem->qty,
                'price' => $produk->harga_jual ?? 0,
                'options' => [
                    'produk' => $produk,
                    'harga_modal' => $produk->saldoAwalBarang->last()->harga_modal ?? 0,
                    'harga_terakhir' => $produk->saldoAwalBarang->last()->harga_terakhir ?? 0,
                    'price_real' => $priceReal,
                    'price_final' => $priceReal * $pengali - ($pengali * $akibatItem->total),
                    'order_id' => $cart->content()->isEmpty() ?
                                1 :
                                $cart->content()->max('options.order_id') + 1,
                    'diskon' => $totalDiskon * $pengali
                ],
            ]);
            $this->helper->makeCartLog($produk->id, $akibatItem->qty, $akibatItem->total, "insert-diskon", $no_penjualan);
            // send_display_kasir([
            //     'barang' => $produk->keterangan,
            //     'qty' => $akibatItem->qty,
            //     'price' => $produk->harga_jual ?? 0,
            //     'subtotal' => $priceReal * $pengali - ($pengali * $akibatItem->total),
            //     'total' => $this->getCartTotal(),
            //     'kasir' => 'kasir_'.auth()->user()->id,
            // ]);

            $kasirId       = 'kasir_'.auth()->user()->id;
            $dataToDisplay = [
                'barang' => $produk->keterangan,
                'qty' => $akibatItem->qty,
                'price' => $produk->harga_jual ?? 0,
                'subtotal' => $priceReal * $pengali - ($pengali * $akibatItem->total),
                'total' => $this->getCartTotal(),
                'kasir' => 'kasir_'.auth()->user()->id,
            ];

            $totalDiskon = 0;
        }

        event(new DisplayKasirEvent($kasirId, $dataToDisplay));
    }

    public function pindahCart()
    {
        $cart = Cart::instance('penjualan-'.auth()->user()->id);
        foreach ($cart->content() as $cartAsli) {
            Cart::instance('penjualan-diskon-'.auth()->user()->id)->add([
                'id' => $cartAsli->id,
                'name' => $cartAsli->name,
                'qty' => $cartAsli->qty,
                'price' => $cartAsli->price,
                'options' => [
                    'produk' => $cartAsli->options->produk,
                    'harga_modal' => $cartAsli->options->harga_modal,
                    'harga_terakhir' => $cartAsli->options->harga_terakhir,
                    'price_real' => $cartAsli->options->price_real,
                    'price_final' => $cartAsli->options->price_final,
                    'order_id' => $cartAsli->options->order_id,
                    'diskon' => $cartAsli->options->diskon
                ],
            ]);
        }
        return true;
    }

    public function batalDiskon(Request $request)
    {
        $cart = Cart::instance('penjualan-diskon-'.auth()->user()->id);

        foreach ($cart->content() as $cartDiskon) {
            $this->helper->makeCartLog($cartDiskon->id, $cartDiskon->qty, $cartDiskon->price, "deleted-diskon", $request->no_penjualan);
        }

        Cart::instance('penjualan-diskon-'.auth()->user()->id)->destroy();
        Session::forget('terdiskon-'.Auth::user()->id);

        $response = [
            'status' => true,
            'template' => view('pos::penjualan.cart_penjualan')->render(),
            'total' => $this->getCartTotal(),
            'total_diskon' => 0
        ];
        return response()->json($response);

    }

    public function getTingkatHargaJual($produk, $request)
    {
        $tingkat_harga_pelanggan = InformasiPelanggan::where('id', $request['pelanggan_id'])->firstOrFail()->tingkatan_harga_jual;

        if (!empty($tingkat_harga_pelanggan)) {

            if (!empty($produk->tingkatHargaBarang)) {

                if ($tingkat_harga_pelanggan === 1) {
                    $harga_jual = $produk->tingkatHargaBarang->tingkat_1 ?? 0;
                }else if ($tingkat_harga_pelanggan === 2) {
                    $harga_jual = $produk->tingkatHargaBarang->tingkat_2 ?? 0;
                }else if ($tingkat_harga_pelanggan === 3) {
                    $harga_jual = $produk->tingkatHargaBarang->tingkat_3 ?? 0;
                }else if ($tingkat_harga_pelanggan === 4) {
                    $harga_jual = $produk->tingkatHargaBarang->tingkat_4 ?? 0;
                }else if ($tingkat_harga_pelanggan === 5) {
                    $harga_jual = $produk->tingkatHargaBarang->tingkat_5 ?? 0;
                }else {
                    $harga_jual = $produk->harga_jual ?? 0;
                }
            
            }else {
                $harga_jual = $produk->harga_jual ?? 0;
            }
        
        }else {
            $harga_jual = $produk->harga_jual ?? 0;
        }

        return $harga_jual;
    }

    public function editHarga(Request $request)
    {
        $request->harga_jual_modal = floatval(preg_replace('/[^\d.]/', '', $request->get('harga_jual_modal')));

        $cart = Cart::instance('penjualan-' . auth()->user()->id);
        $row_id = $request->get('row_id');


        

        // samakan cart dan data
        $checkItemCart = $cart->search(function ($cartItem) use ($row_id) {
            return $cartItem->rowId === $row_id;
        });

        // ambil yang pertama
        $existCart = $checkItemCart->first();

        // perhitungan
        $subTotalDiskon     = ($request->harga_jual_modal * ($existCart->options->diskon ?? 0)) / 100;
        $priceReal          = $request->harga_jual_modal - $subTotalDiskon;
        $totalDiskon        = $priceReal * ($existCart->qty);

        // update cart
        Cart::instance('penjualan-' . auth()->user()->id)->update($existCart->rowId, [
            'id'        => $existCart->id,
            'name'      => $existCart->name,
            'qty'       => $existCart->qty,
            'price'     => $request->harga_jual_modal,
            'options'   => [
                'type'              => $existCart->options->type,
                'produk'            => $existCart->options->produk,
                'harga_modal'       => $existCart->options->harga_modal,
                'harga_terakhir'    => $existCart->options->harga_terakhir,
                'price_real'        => $priceReal,
                'price_final'       => $totalDiskon,
                'order_id'          => $existCart->options->order_id,
                'diskon'            => $existCart->options->diskon,
            ]
        ]);

        return redirect()->back();
    }

    public function beliPulsa(Request $request)
    {
        $transaksi = Transaksi::orderBy('id','desc')->first()->id;
        $id = $request->get('id') ?? $transaksi + 1;
        $helper = new PenjualanApi;
        $helper = $helper->topUpPulsaRegularTest($request->get('code'), $request->get('nomor'), $id);      
        return $helper;
    }

    public function daftarPulsa(Request $request)
    {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 300);
        $produkDetail = SaldoAwalBarang::filter($request)
            ->with('produk')
            ->groupBy('produk_id')
            ->orderBy('id', 'desc')
            ->whereHas('produk',function($produk){
                // $produk->where('tipe_barang', '!==', 3);
                $produk->where('keterangan', 'LIKE', '%PULSA%');
            })
            // ->where('gudang_id','=',6)
            ->get();
        // dd($produkDetail->count());
        $transform = $produkDetail->transform(function ($item) {

            /** @var array $res di format ulang response */
            $res = [
                'id' => $item->produk->produk_detail->id,
                'nama' => $item->produk->keterangan,
                'harga' => $item->produk->harga_jual,
                'unit' => $item->produk->unit,
                'kategori' => $item->produk->kategori->nama,
                'no_barang' => $item->produk->no_barang,
                'diskon' => $item->produk->diskon,
                'harga_terakhir' => $item->harga_terakhir,
                'harga_modal' => $item->harga_modal,
                'foto_produk' => $item->produk->foto_produk,
                'tersedia' => $item->produk->UpdatedQty,
                //'tipe_persediaan' => $item->produk->TipePersediaanApi,
                //'harga' => $item->harga_terakhir
            ];
            return $res;
        });
        return response()->json([
            'status' => true,
            'message' => 'Ditemukan data',
            'data' => $transform,
        ]);
    }

    public function addEdcDebit(Request $request)
    {
        $data = $request->all();

        try {
            
            $uniqueId = $this->generateUniqueIdCart('edc-debit');

            $cart = Cart::instance('edc-debit-'.auth()->user()->id);

            $session_edc = [
                'id'            => $uniqueId,
                'name'          => 'edc debit name',
                'qty'           => 1,
                'price'         => 1000,
                'options' => [
                    'type_edc_id'       => $data['type_edc_debit'],
                    'no_kartu'          => $data['no_kartu_debit'].' '.'['.$this->typeEdc->findOrFail($data['type_edc_debit'])->nama.']',
                    'batch'             => $data['no_batch_debit'],
                    'pembayaran_debit'  => $data['pembayaran_debit'],
                    'order_id'          => $cart->content()->isEmpty() ?
                                           1 :
                                           $cart->content()->max('options.order_id') + 1,
                ],
            ];

            Cart::instance('edc-debit-'.auth()->user()->id)->add($session_edc);

            $response = [
                'status'    => true,
                'total'     => $this->getCartEdcDebitTotal(),
                'template'  => view('pos::penjualan.cart_debit')->render(),
            ];

        } catch (Exception $e) {
            $response = [
                'status'    => false,
                'message'   => $e->getMessage(),
            ];
        }

        return response()->json($response);           

    }

    public function removeEdcDebit(Request $request)
    {
        try {
            if ($request->id !== null) {

                Cart::instance('edc-debit-'.auth()->user()->id)->remove($request->id);

                $response = [
                    'status'                    => true,
                    'total'                     => $this->getCartEdcDebitTotal(),
                    'template'                  => view('pos::penjualan.cart_debit')->render(),
                ];

            } else {
                $response = [
                    'status' => false,
                    'message' => 'Cart Edc Tidak Ditemukan!',
                ];
            }
        } catch (Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage(),
            ];
        }

        return response()->json($response);
    }

    public function getCartEdcDebitTotal()
    {
        $total = 0;

        $cart = Cart::instance('edc-debit-'.auth()->user()->id);

        foreach ($cart->content() as $item) {
            $total += $item->options->pembayaran_debit;
        }

        return $total;
    }

    public function getEdcDebitPelunasan()
    {
        return $this->getCartTotal() - $this->getCartEdcDebitTotal();
    }

    public function generateUniqueIdCart($type)
    {
        $newUniqueId = null;

        if ($type === 'edc-debit') {
            $uniqueId    = Cart::instance('edc-debit-'.auth()->user()->id)->content()->pluck('id')->toArray();
            $newUniqueId = uniqid();
            while (in_array($newUniqueId, $uniqueId)) {
                $newUniqueId = uniqid();
            }
        }

        return $newUniqueId;
    }

    public function checkQtyForCart($request)
    {
        if (empty($request->qty)) {
            $request->qty = 1;
        }

        return $request->qty;
    }

        public function getPelangganUmum()
    {
        return InformasiPelanggan::where('nama','Pelanggan Umum')->first()->id;
    }

    public function getTanggalCustom()
    {
        return $this->model->getTanggalCustom();
    }

    public function getCetakUlang()
    {
        return $this->model->getCetakUlang();
    }

    public function cetakStrukDetail(Request $request)
    {
        $data = $request->all();

        return $this->model->cetakStrukDetail($data);
    }

    public function saveToCart(Request $request)
    {
        $response = [
            'success' => false,
            'message' => 'Terjadi Kesalahan!'
        ];
        $cart = CartPenjualan::find($request->cart_id);
        if($cart !== null){
            $cart->setDesc($request->desc);
            $response['success'] = true;
            $response['message'] = 'data berhasil disimpan!';
            $response['cart_list'] = view('pos::penjualan.components.cart_list', ['carts' => new CartPenjualan])->render();
            $response['next_cart_id'] = CartPenjualan::generateCartId();
        }
        return response()->json($response);
    }

    public function removeFromCart(Request $request)
    {
        $response = [
            'success' => false,
            'message' => 'Terjadi Kesalahan!'
        ];

        if($request->has('cart_id') && CartPenjualan::destroyPenjualan($request->cart_id)){
            $response['success'] = true;
            $response['message'] = 'data berhasil dihapus!';
            $response['cart_list'] = view('pos::penjualan.components.cart_list', ['carts' => new CartPenjualan])->render();
        }

        return response()->json($response);
    }

    public function getFromCart(Request $request)
    {
        $response = [
            'success' => false,
            'message' => 'Terjadi Kesalahan!'
        ];

        $cart = CartPenjualan::find($request->cart_id);
        
        if ($cart !== null) {
            $response['success'] = true;
            $response['message'] = 'berhasil';
            $response['cart_penjualan'] = view('pos::penjualan.cart_penjualan', ['cart' => $cart])->render();
            $response['cart_final_price'] = $cart->getFinalPrice();
            $response['cart_list'] = view('pos::penjualan.components.cart_list', ['carts' => new CartPenjualan])->render();
        }

        return response()->json($response);
    }

    public function checkChartList(Request $request)
    {
        $response = [
            'success' => false,
            'message' => 'Terjadi Kesalahan!',
            'description' => null
        ];

        $cart = CartPenjualan::find($request->cart_id);

        if ($cart !== null && $cart->description != null) {
            $response['success'] = true;
            $response['message'] = 'Data Antrian Ditemukan';
            $response['description'] = $cart->description;
        }

        return response()->json($response);
    }

    public function resetKasir()
    {
        Artisan::call('checkout:kasir');

        return redirect()->back();
    }
}
