<?php

namespace App\Modules\Pos\Http\Controllers;

use Generator\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
   protected $viewNamespace = 'pos';
}