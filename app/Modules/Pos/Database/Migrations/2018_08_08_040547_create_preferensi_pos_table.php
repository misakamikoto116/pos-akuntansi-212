<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreferensiPosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasTable('preferensi_pos')) {
            Schema::create('preferensi_pos', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('akun_cash_id')->unsigned()->nullable();
                $table->integer('akun_hutang_id')->unsigned()->nullable();

                $table->foreign('akun_cash_id')->references('id')->on('akun');
                $table->foreign('akun_hutang_id')->references('id')->on('akun');

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferensi_pos');
    }
}
