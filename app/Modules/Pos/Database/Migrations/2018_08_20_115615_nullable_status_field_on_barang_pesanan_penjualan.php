<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullableStatusFieldOnBarangPesananPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('barang_pesanan_penjualan', 'status')) {
            Schema::table('barang_pesanan_penjualan', function (Blueprint $table) {
                $table->dropColumn('status');
            });
        }

        if (!Schema::hasColumn('barang_pesanan_penjualan', 'status')) {
            Schema::table('barang_pesanan_penjualan', function (Blueprint $table) {
                $table->tinyInteger('status')->nullable()->after('ditutup')->comment('0 = tetap, 1 = berubah, 2 = new');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_pesanan_penjualan', function (Blueprint $table) {
            //
        });
    }
}
