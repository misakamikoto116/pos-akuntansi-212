<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUangModalOnTablePreferensiPos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('preferensi_pos', 'uang_modal')) {
            Schema::table('preferensi_pos', function (Blueprint $table) {
                $table->float('uang_modal',19,2)->after('akun_hutang_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preferensi_pos', function (Blueprint $table) {
            //
        });
    }
}
