<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAkibatPromosisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akibat_promosi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sebab_promosi_id')->unsigned();
            $table->foreign('sebab_promosi_id')
                            ->references('id')
                            ->on('sebab_promosi');

            $table->integer('produk_id')->unsigned()->nullable();
            $table->foreign('produk_id')
                            ->references('id')
                            ->on('produk');
            $table->integer('qty')->default(1)->nullable();
            
            $table->float('total',19,2)->nullable();
            $table->float('presentase',19,2)->nullable();

            $table->boolean('type_akibat')->comment('0 = karena produk, 1 = bukan produk (potongan harga lsng)')->default(0);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akibat_promosi');
    }
}
