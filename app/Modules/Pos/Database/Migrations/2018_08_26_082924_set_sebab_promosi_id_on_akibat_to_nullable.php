<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetSebabPromosiIdOnAkibatToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('akibat_promosi', function (Blueprint $table) {
            $table->dropForeign('akibat_promosi_sebab_promosi_id_foreign');
            $table->dropColumn('sebab_promosi_id');
        });

        Schema::table('akibat_promosi', function (Blueprint $table) {
            $table->integer('sebab_promosi_id')->after('id')->nullable()->unsigned();
            $table->foreign('sebab_promosi_id')
                ->references('id')
                ->on('sebab_promosi');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
