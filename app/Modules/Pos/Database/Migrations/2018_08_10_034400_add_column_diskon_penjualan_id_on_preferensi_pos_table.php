<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDiskonPenjualanIdOnPreferensiPosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('preferensi_pos', 'diskon_penjualan_id')) {
            Schema::table('preferensi_pos', function (Blueprint $table) {
                $table->integer('diskon_penjualan_id')->unsigned()->nullable()->after('kode_pajak_id');

                $table->foreign('diskon_penjualan_id')->references('id')->on('akun');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
