<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAkibatPromosiIdOnSebab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('sebab_promosi', 'akibat_promosi_id')) {
            Schema::table('sebab_promosi', function (Blueprint $table) {
                $table->integer('akibat_promosi_id')->unsigned()->after('promosi_id')->nullable();
                $table->foreign('akibat_promosi_id')
                            ->references('id')
                            ->on('akibat_promosi');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
