<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiskonRupiahPersenOnTableRiwayatPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('riwayat_pembayaran', 'diskon_persen')) {
            Schema::table('riwayat_pembayaran', function (Blueprint $table) {
                $table->integer('diskon_persen')->after('kembalian');
            });
        }

        if (!Schema::hasColumn('riwayat_pembayaran', 'diskon_rupiah')) {
            Schema::table('riwayat_pembayaran', function (Blueprint $table) {
                $table->float('diskon_rupiah', 19,2)->after('diskon_persen');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riwayat_pembayaran', function (Blueprint $table) {
            //
        });
    }
}
