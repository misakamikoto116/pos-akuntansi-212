<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusDanMesinOnPengirimanDanFakturPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('pengiriman_penjualan', 'status_modul')) {
            Schema::table('pengiriman_penjualan', function (Blueprint $table) {
                $table->tinyInteger('status_modul')->default(0)->comment('0 : Akuntansi, 1 : Penjualan (POS)');
            });
        }

        if ( ! Schema::hasColumn('pengiriman_penjualan', 'kasir_mesin_id')) {
            Schema::table('pengiriman_penjualan', function (Blueprint $table) {
                $table->integer('kasir_mesin_id')->nullable()->unsigned();

                $table->foreign('kasir_mesin_id')->references('id')->on('kasir_mesin');
            });
        }

        if ( ! Schema::hasColumn('faktur_penjualan', 'status_modul')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->tinyInteger('status_modul')->default(0)->comment('0 : Akuntansi, 1 : Penjualan (POS)');
            });
        }

        if ( ! Schema::hasColumn('faktur_penjualan', 'kasir_mesin_id')) {
            Schema::table('faktur_penjualan', function (Blueprint $table) {
                $table->integer('kasir_mesin_id')->nullable()->unsigned();

                $table->foreign('kasir_mesin_id')->references('id')->on('kasir_mesin');
            });   
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
