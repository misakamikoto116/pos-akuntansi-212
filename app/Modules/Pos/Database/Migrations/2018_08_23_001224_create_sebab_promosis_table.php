<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSebabPromosisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sebab_promosi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('promosi_id')->unsigned();
            $table->foreign('promosi_id')
                            ->references('id')
                            ->on('promosi');
            $table->integer('produk_id')->unsigned()->nullable();
            $table->foreign('produk_id')
                            ->references('id')
                            ->on('produk');
            $table->integer('qty')->default(1)->nullable();

            $table->float('total', 19,2)->nullable();
            $table->float('presentase', 19,2)->nullable();

            $table->boolean('type_sebab')->comment('0 = karena produk, 1 = bukan produk (potongan harga lsng)')->default(0);
            $table->boolean('kelipatan')->comment('0 = tidak berlaku, 1 = berlaku kelipatan')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sebab_promosi');
    }
}
