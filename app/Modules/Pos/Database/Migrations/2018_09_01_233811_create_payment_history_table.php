<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('payment_history')) {
            Schema::create('payment_history', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('penerimaan_penjualan_id')->unsigned()->nullable();
                $table->tinyInteger('status')->comment('0 = Cash, 1 = Non Cash');
                $table->float('total', 19,2);
                $table->foreign('penerimaan_penjualan_id')->references('id')->on('penerimaan_penjualan');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_history');
    }
}
