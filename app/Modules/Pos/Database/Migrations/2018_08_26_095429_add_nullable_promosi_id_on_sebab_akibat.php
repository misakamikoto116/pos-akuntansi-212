<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullablePromosiIdOnSebabAkibat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('akibat_promosi', function (Blueprint $table) {
            $table->integer('promosi_id')->unsigned()->nullable()->after('id');
            $table->foreign('promosi_id')
                                    ->references('id')
                                    ->on('promosi');
        });

        Schema::table('sebab_promosi', function (Blueprint $table) {
            $table->dropForeign('sebab_promosi_promosi_id_foreign');
            $table->dropColumn('promosi_id');
        });

        Schema::table('sebab_promosi', function (Blueprint $table) {
            $table->integer('promosi_id')->after('id')->nullable()->unsigned();
            $table->foreign('promosi_id')
                ->references('id')
                ->on('promosi');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
