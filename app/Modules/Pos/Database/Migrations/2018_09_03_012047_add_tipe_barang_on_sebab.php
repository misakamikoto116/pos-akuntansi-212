<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipeBarangOnSebab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('sebab_promosi', 'tipe_barang')) {
            Schema::table('sebab_promosi', function (Blueprint $table) {
                $table->tinyInteger('tipe_barang')->nullable()->after('kelipatan');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
