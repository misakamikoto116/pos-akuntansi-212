<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DelInvoicePenerimaanPenjualanIdAddPenerimaanPenjualanId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('transaction_non_cash', 'invoice_penerimaan_penjualan_id')) {
            Schema::table('transaction_non_cash', function (Blueprint $table) {
                $table->dropForeign('transaction_non_cash_invoice_penerimaan_penjualan_id_foreign');
                $table->dropColumn('invoice_penerimaan_penjualan_id');
            });
        }

        if (!Schema::hasColumn('transaction_non_cash', 'penerimaan_penjualan_id')) {
            Schema::table('transaction_non_cash', function (Blueprint $table) {
                $table->integer('penerimaan_penjualan_id')->unsigned()->after('id');
                $table->foreign('penerimaan_penjualan_id')->references('id')->on('penerimaan_penjualan');
            });
        }

        if (!Schema::hasColumn('transaction_non_cash', 'bulan_tahun')) {
            Schema::table('transaction_non_cash', function (Blueprint $table) {
                $table->integer('bulan_tahun')->after('total');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_non_cash', function (Blueprint $table) {
            //
        });
    }
}
