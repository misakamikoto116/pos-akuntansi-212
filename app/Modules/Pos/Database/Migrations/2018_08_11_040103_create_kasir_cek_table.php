<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKasirCekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasTable('kasir_cek')) {
            Schema::create('kasir_cek', function (Blueprint $table) {
                $table->increments('id');
                $table->char('check_state', 3)->comment('IN atau OUT');
                $table->datetime('check_time');

                $table->double('modal', 16, 2);

                $table->integer('kasir_mesin_id')->unsigned()->nullable();

                $table->foreign('kasir_mesin_id')
                                ->references('id')
                                ->on('kasir_mesin');
                
                // $table->integer('created_by')->unsigned()->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kasir_cek');
    }
}
