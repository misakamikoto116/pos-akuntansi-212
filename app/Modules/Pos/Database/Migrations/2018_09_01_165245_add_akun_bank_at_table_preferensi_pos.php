<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAkunBankAtTablePreferensiPos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('preferensi_pos', 'akun_bank_id')) {
            Schema::table('preferensi_pos', function (Blueprint $table) {
                $table->integer('akun_bank_id')->unsigned()->nullable()->after('akun_cash_id');
                
                $table->foreign('akun_bank_id')->references('id')->on('akun');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
