<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnKodePajakIdOnPreferensiPosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('preferensi_pos', 'kode_pajak_id')) {
            Schema::table('preferensi_pos', function (Blueprint $table) {
                $table->integer('kode_pajak_id')->unsigned()->nullable()->after('akun_hutang_id');

                $table->foreign('kode_pajak_id')->references('id')->on('kode_pajak');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
