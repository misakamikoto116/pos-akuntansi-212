<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DelColPenerimaanPenjualanIdAddPaymentHistoryId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('transaction_non_cash', 'penerimaan_penjualan_id')) {
            Schema::table('transaction_non_cash', function (Blueprint $table) {
                $table->dropForeign('transaction_non_cash_penerimaan_penjualan_id_foreign');
                $table->dropColumn('penerimaan_penjualan_id');
            });
        }

        if (!Schema::hasColumn('transaction_non_cash', 'payment_history_id')) {
            Schema::table('transaction_non_cash', function (Blueprint $table) {
                $table->integer('payment_history_id')->unsigned()->after('id');
                $table->foreign('payment_history_id')->references('id')->on('payment_history');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_non_cash', function (Blueprint $table) {
            //
        });
    }
}
