<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiwayatPembayaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('riwayat_pembayaran')) {
            Schema::create('riwayat_pembayaran', function (Blueprint $table) {
                $table->increments('id');
                $table->float('kembalian', 19,2);
                $table->float('dibayar', 19,2);
                $table->integer('faktur_penjualan_id')->unsigned();
                $table->foreign('faktur_penjualan_id')
                      ->references('id')
                      ->on('faktur_penjualan');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_pembayaran');
    }
}
