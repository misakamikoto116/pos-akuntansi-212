<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedByUpdatedByDeletedByOnTablePrefPosKasirMesinKasirCek extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data =  [
            'kasir_cek',
            'kasir_mesin',
            'preferensi_pos',
        ];

        foreach ($data as $table) {
            if (!Schema::hasColumn($table, 'created_by')) {
                Schema::table($table, function (Blueprint $table) {
                    $table->integer('created_by')->unsigned()->nullable();
                });
            }

            if (!Schema::hasColumn($table, 'updated_by')) {
                Schema::table($table, function (Blueprint $table) {
                    $table->integer('updated_by')->unsigned()->nullable();
                });
            }

            if (!Schema::hasColumn($table, 'deleted_by')) {
                Schema::table($table, function (Blueprint $table) {
                    $table->integer('deleted_by')->unsigned()->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
