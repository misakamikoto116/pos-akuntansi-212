<?php

namespace App\Modules\Pos\Repositories;

use App\Modules\Akuntansi\Models\BarangPenawaranPenjualan;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\PenawaranPenjualan;
use App\Modules\Akuntansi\Models\PesananPenjualan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\barangPesananPenjualan;
use Carbon\Carbon;
use Cart;
use DB;
use Generator\Interfaces\RepositoryInterface;

class PesananPenjualanPosRepositories implements RepositoryInterface
{
    public function __construct(PesananPenjualan $model, 
                                PenawaranPenjualan $penawaran_penjualan, 
                                Produk $produk, 
                                Gudang $gudang, 
                                BarangPenawaranPenjualan $barang_penawaran_penjualan,
                                InformasiPelanggan $pelanggan,
                                PesananPenjualan $pesanan_penjualan)
    {
        $this->model                                    = $model;
        $this->penawaran_penjualan                      = $penawaran_penjualan;
        $this->produk                                   = $produk;
        $this->gudang                                   = $gudang;
        $this->barang_penawaran_penjualan               = $barang_penawaran_penjualan;
        $this->pelanggan                                = $pelanggan;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems()
    {
        return $this->model->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        $model = $this->model->findOrFail($id);
        return $model;
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        return $this->findItem($id)->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->findItem($id)->fill($data);

        return $model->save();
    }

    /**
     * menambahkan data berdasarkan request.
     *
     * @param [type] $request [description]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $model = $this->model->fill($data);

        return $model->save();
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($data)
    {
        return $this->model->filter($data)->get();
    }

    public function storePesanan($request)
    {
        DB::beginTransaction();

        $data = $request->all();
        
        $data['total_potongan_rupiah'] = floatval(preg_replace('/[^\d.]/', '', $data['total_potongan_rupiah']));


        try {

            $carbonNow = Carbon::now()->format('Y-m-d H:i:s');

            // $pelangganPos = $this->getInformasiPelangganPos();

            // Pesanan Penjualan
            $pesananPenjualan = $this->model->create([
                'so_number'                                 => $this->makeNoPesananPos($this->model->whereDate('created_at', Carbon::now()->format('Y-m-d'))->orderBy('id','desc')->first()->id ?? 0),
                'pelanggan_id'                              => $data['pelanggan_id'],
                'alamat_pengiriman'                         => '',
                'so_date'                                   => $carbonNow,
                'diskon'                                    => $data['diskon'],
                'total_potongan_rupiah'                     => $data['total_potongan_rupiah'],
                'created_at'                                => $carbonNow,
                'updated_at'                                => $carbonNow,
                'created_by'                                => auth()->user()->id
            ]);

            $cart = Cart::instance('pesanan-penjualan-' . auth()->user()->id);

            foreach ($cart->content() as $item) {
                // Barang Pesanan Penjualan
                $barangPesananPenjualan = $pesananPenjualan->barang()->save(
                    new BarangPesananPenjualan([
                        'produk_id'                         => $item->options->produk->produk_id !== null ? $item->options->produk->produk_id : $item->options->produk->id,
                        'pesanan_penjualan_id'              => $pesananPenjualan->id,
                        'barang_penawaran_penjualan_id'     => $item->options->type == "penawaran" ? $item->id : null,
                        'item_deskripsi'                    => $item->name,
                        'jumlah'                            => $item->qty,
                        'satuan'                            => $item->options->produk->produk !== null ? $item->options->produk->produk->unit : $item->options->produk->unit,
                        'diskon'                            => $item->options->diskon,
                        'harga'                             => $item->price,
                        'harga_modal'                       => $item->options->harga_modal,
                        'created_at'                        => $carbonNow,
                        'updated_at'                        => $carbonNow,
                        'created_by'                        => auth()->user()->id,
                    ])
                );

                if ($barangPesananPenjualan->barang_penawaran_penjualan_id !== null) {
                    $barangPesananPenjualan->barangPenawaranPenjualan->penawaranPenjualan->update([
                      'status'    => 1,
                    ]);
                }
            }



            $cart->destroy();

            DB::commit();
            
            $response = [
                'status'    => true,
                'message'   => 'Pesanan Berhasil Dilakukan'
            ];

        } catch (Exception $e) {

            DB::rollBack();

            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }

        return response()->json($response);
    }

    public function getProduk($request)
    {
        $produk = null;
        if ($request->type_submit == "edit") {
            $produk = $this->produk->findOrFail($request->id);
        }else {
            $produk = $this->produk->findOrFail($request->barang_id);
        }
        return $produk;
    }

    public function getPenawaranPenjualan($request)
    {
        if ($request->type_submit == "edit") {
            return $this->barang_penawaran_penjualan->findOrFail($request->id);
        }else {
            return $this->barang_penawaran_penjualan->where('penawaran_penjualan_id',$request->penawaran_penjualan_id)->get();
        }
    }

    public function diskonItem($request)
    {
        try {
            $produk = $this->getProduk($request);

            if ($produk === null) {
                $response = [
                    'status' => false
                ];
            } else {
                
                $response = [
                    'status' => true,
                    'data' => $produk
                ];

            }

        } catch (Exception $e) {
            $response = [
                'status' => false
            ];
        }

        return response()->json($response);
    }

    public function getAllPenawaranPenjualan($request)
    {
        $page        = $request->get('page');
        $resultCount = 25;

        $offset = ($page - 1) * $resultCount;

        $penawaran = $this->penawaran_penjualan
                          ->where('status', 0)
                          ->where(function ($query) use ($request)
                          {
                              $query->where('no_penawaran','LIKE','%'. $request->search . '%');
                          })
                          ->skip($offset)
                          ->take($resultCount)
                          ->get()
                          ->map(function ($item)
                          {
                              return [
                                'id'            => $item->id,
                                'text'          => $item->no_penawaran,
                                'tanggal'       => Carbon::parse($item->tanggal)->format('d F Y')
                              ];
                          });

            $count     = count($penawaran);
            $endCount  = $offset + $resultCount;
            $morePages = $count > $endCount;

            $results = [
                'results'    => $penawaran,
                'pagination' => [
                    'more' => $morePages,
                ],
            ];

        return response()->json($results);
    }

    public function getAllPelanggan($request)
    {
        $page        = $request->get('page');
        $resultCount = 25;

        $offset = ($page - 1) * $resultCount;

        $pelanggan      = $this->pelanggan
                          ->where(function ($query) use ($request)
                          {
                              $query->where('no_pelanggan','LIKE','%'. $request->search . '%')
                                    ->orWhere('nama','LIKE','%'. $request->search .'%');
                          })
                          ->where('tipe_pelanggan_id', 6)
                          ->skip($offset)
                          ->take($resultCount)
                          ->get()
                          ->map(function ($item)
                          {
                              return [
                                'id'            => $item->id,
                                'text'          => $item->nama,
                                'no_pelanggan'  => $item->no_pelanggan
                              ];
                          });

            $count     = count($pelanggan);
            $endCount  = $offset + $resultCount;
            $morePages = $count > $endCount;

            $results = [
                'results'    => $pelanggan,
                'pagination' => [
                    'more' => $morePages,
                ],
            ];

        return response()->json($results);
    }

    public function getAllProduk($request)
    {
        $page        = $request->get('page');
        $resultCount = 25;

        $offset = ($page - 1) * $resultCount;

        $gudang_toko = $this->gudang->where('nama', 'Gudang Toko')->get();

        if ($gudang_toko->isNotEmpty()) {
            $produk = $this->produk->whereNotIn('tipe_barang', ['3'])->where(function ($query) use ($request) {
                $query->where('keterangan', 'LIKE', '%' . $request->search . '%')
                       ->orWhere('no_barang', 'LIKE', '%' . $request->search . '%');
             })->whereIn('gudang_id', $gudang_toko->pluck('id'))
             ->whereNotNull('akun_beban_id')
             ->whereNotNull('akun_persedian_id')
             ->whereNotNull('akun_penjualan_id')
             ->whereNotNull('akun_ret_penjualan_id')
             ->whereNotNull('akun_disk_penjualan_id')
             ->whereNotNull('akun_barang_terkirim_id')
             ->whereNotNull('akun_hpp_id')
             ->skip($offset)
               ->take($resultCount)
               ->get()
               ->map(function ($item) {
                   return [
                       'id' => $item->id,
                       'text' => $item->keterangan,
                       'no_barang' => $item->no_barang
                   ];
               });

            $count     = count($produk);
            $endCount  = $offset + $resultCount;
            $morePages = $count > $endCount;

            $results = [
                'results'    => $produk,
                'pagination' => [
                    'more' => $morePages,
                ],
            ];
        } else {
            $results = [
                'results'    => [],
                'pagination' => [
                    'more' => false,
                ],
            ];
        }

        return response()->json($results);
    }

    public function makeNoPesananPos($id)
    {
        $id                   = $id + 1;
        $so_number            = 'POS/'.Carbon::now()->format('Y/m/d').'/'.$id;
        return $so_number;
    }
}
