<?php 
namespace App\Modules\Pos\Repositories;

use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\BarangPengirimanPenjualan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\InvoicePenerimaanPenjualan;
use App\Modules\Akuntansi\Models\PenerimaanPenjualan;
use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Pos\Models\KasirCek;
use App\Modules\Pos\Models\KasirMesin;
use App\Modules\Pos\Models\PaymentHistory;
use App\Modules\Pos\Models\PreferensiPos;
use App\Modules\Pos\Models\TransactionNonCash;
/// Printer
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\CupsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\EscposImage;
/// Additional
use Auth;
use Carbon\Carbon;
use Cart;
use DB;
use Generator\Interfaces\RepositoryInterface;
use Helpers\TransaksiHelper;
use PDF;
use Session;

trait TraitPenjualan
{
        public function printRaw($cart, $data, $port) {
        try{
            $connector = new WindowsPrintConnector($port);
            $printer = new Printer($connector);
            $printer->selectPrintMode();
            $logo = EscposImage::load(public_path('assets/images/logo_dark.png'), false);
            $printer -> setFont(Printer::FONT_C);
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer -> bitImage($logo);
            $printer -> text("212 Mart \n");
            #$printer -> feed();
            
            $printer -> text("-----------------------------------------------". "\n");
            
        
            foreach ($cart as $val) {
                $printer -> setJustification(Printer::JUSTIFY_LEFT);
                $printer -> text(' ' . $val['qty'] . ' ' . $val['name']."\n");
                $printer -> setJustification(Printer::JUSTIFY_RIGHT);
                $printer -> text($val['price'] . " x ". $val['qty'] . " : ". $val['price'] * $val['qty'] ."\n");
            }
        
            $printer -> text("-----------------------------------------------". "\n");
            $printer -> setJustification(Printer::JUSTIFY_RIGHT);
            $printer -> setTextSize(1, 1);
            $printer -> text('Total: '. $data['total']. "\n");
            $printer -> text('Diterima: '. $data['tunai']. "\n");
            $printer -> text('Kembali: '. $data['kembali']. "\n");
            // $member = $data['anggt_kop'];
            // if($member !== null){
            //     $printer -> text("Agt Kopr.: ".$data['anggota_name']. "\n");
            // }
            
            // $voucher = $data['voucher'];
            // if($voucher !== null){
            //     $printer -> text("Voucher. : ".$voucher. "\n");
            // }


            $printer -> text("Kasir: ".Auth::user()->name. "\n");
            $printer -> text(date('Y-m-d H:i:s'). "\n");
            $printer -> text("-----------------------------------------------". "\n");
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer->selectPrintMode();
            $printer->setBarcodeHeight(30);
            #$printer -> text("\n");
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_BELOW);
            // $printer->barcode($data['kode_transaksi'],Printer::BARCODE_CODE39);//50mm
            //$printer -> barcode("{B" . '212.300318.000001',Printer::BARCODE_CODE128);//80mm
            $printer -> text("TERIMA KASIH TELAH BERBELANJA di \n212 Mart\n");
            $printer -> text($data['no_penjualan']."\n");
            $printer -> feed(1);
            #$printer -> text("\n");
            $printer -> cut();
            $printer -> pulse();
            $printer -> close();
            
        }catch (Exception $e){
            $printer -> text($e -> getMessage() . "\n");
        }
    }
}