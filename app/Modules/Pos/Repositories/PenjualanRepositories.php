<?php
namespace App\Modules\Pos\Repositories;

use App\Events\DisplayKasirEvent;
use App\Jobs\PenjualanQueue;
use App\Modules\Akuntansi\Models\BarangFakturPenjualan;
use App\Modules\Akuntansi\Models\BarangPengirimanPenjualan;
use App\Modules\Akuntansi\Models\BarangPesananPenjualan;
use App\Modules\Akuntansi\Models\FakturPenjualan;
use App\Modules\Akuntansi\Models\ReturPenjualan;
use App\Modules\Akuntansi\Models\Gudang;
use App\Modules\Akuntansi\Models\Identitas;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\InvoicePenerimaanPenjualan;
use App\Modules\Akuntansi\Models\PenerimaanPenjualan;
use App\Modules\Akuntansi\Models\PengirimanPenjualan;
use App\Modules\Akuntansi\Models\PesananPenjualan;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\TypeEdc;
use App\Modules\Pos\Models\CartPenjualan;
use App\Modules\Pos\Models\KasirCek;
use App\Modules\Pos\Models\KasirMesin;
use App\Modules\Pos\Models\PaymentHistory;
use App\Modules\Pos\Models\PreferensiPos;
use App\Modules\Pos\Models\RiwayatPembayaran;
use App\Modules\Pos\Models\TransactionNonCash;
use Auth;
use Carbon\Carbon;
use Cart;
use DB;
use Generator\Interfaces\RepositoryInterface;
use Helpers\TransaksiHelper;
use Mike42\Escpos\Printer;
use PDF;
use Session;

class PenjualanRepositories implements RepositoryInterface
{
	public function __construct(
		Produk $produk,
		PreferensiPos $preferensiPos,
		PengirimanPenjualan $pengirimanPenjualan,
		BarangPengirimanPenjualan $barangPengirimanPenjualan,
		FakturPenjualan $fakturPenjualan,
		Transaksi $transaksi,
		SaldoAwalBarang $saldoAwalBarang,
		TransaksiHelper $transaksiHelper,
		Gudang $gudang,
		KasirMesin $kasirMesin,
		KasirCek $kasirCek,
		InformasiPelanggan $informasiPelanggan,
		PenerimaanPenjualan $penerimaanPenjualan,
		Identitas $identitas,
		InvoicePenerimaanPenjualan $invoicePenerimanaanPenjualan,
		TransactionNonCash $transactionNonCash,
		PesananPenjualan $pesanan_penjualan,
		BarangPesananPenjualan $barang_pesanan_penjualan,
		RiwayatPembayaran $riwayat_pembayaran,
		PaymentHistory $payment_history,
		TypeEdc $type_edc,
		returPenjualan $returPenjualan
	) {
		$this->produk                       = $produk;
		$this->preferensiPos                = $preferensiPos;
		$this->pengirimanPenjualan          = $pengirimanPenjualan;
		$this->barangPengirimanPenjualan    = $barangPengirimanPenjualan;
		$this->fakturPenjualan              = $fakturPenjualan;
		$this->transaksi                    = $transaksi;
		$this->saldoAwalBarang              = $saldoAwalBarang;
		$this->transaksiHelper              = $transaksiHelper;
		$this->gudang                       = $gudang;
		$this->kasirMesin                   = $kasirMesin;
		$this->kasirCek                     = $kasirCek;
		$this->informasiPelanggan           = $informasiPelanggan;
		$this->penerimaanPenjualan          = $penerimaanPenjualan;
		$this->identitas                    = $identitas;
		$this->invoicePenerimanaanPenjualan = $invoicePenerimanaanPenjualan;
		$this->transactionNonCash           = $transactionNonCash;
		$this->pesanan_penjualan            = $pesanan_penjualan;
		$this->barang_pesanan_penjualan     = $barang_pesanan_penjualan;
		$this->riwayat_pembayaran           = $riwayat_pembayaran;
		$this->payment_history              = $payment_history;
		$this->type_edc                     = $type_edc;
		$this->returPenjualan               = $returPenjualan;
	}

	use TraitPenjualan;

	/**
	 * ini untuk mengambil data keseluruhan
	 * user di data repositori.
	 *
	 * @return Collection data list user
	 */
	public function getItems()
	{
		return $this->model->paginate(20);
	}

	/**
	 * ini untuk mencari user berdasarkan id yang dicari.
	 *
	 * @param int $id
	 *
	 * @return object
	 */
	public function findItem($id)
	{
		return $this->model->findOrFail($id);
	}

	/**
	 * ini untuk menghapus data berdasarkan id.
	 *
	 * @param [type] $id [description]
	 *
	 * @return [type] [description]
	 */
	public function delete($id)
	{
		return $this->findItem($id)->delete();
	}

	/**
	 * update data berdasarkan id dan data
	 * didapat dari variable request.
	 *
	 * @param [type] $id   [description]
	 * @param [type] $data [description]
	 *
	 * @return [type] [description]
	 */
	public function update($id, $data)
	{
		$model = $this->findItem($id)->fill($data);
		$model->save();

		return $model;
	}

	public function getAllPesananPenjualan($request)
	{
		$page        = $request->get('page');
		$resultCount = 25;

		$offset = ($page - 1) * $resultCount;

		$pesanan = $this->pesanan_penjualan
						  ->where(['approved' => 1, 'status' => 0])
						  ->where(function ($query) use ($request) {
						  	$query->where('so_number', 'LIKE', '%' . $request->search . '%');
						  })
						  ->skip($offset)
						  ->take($resultCount)
						  ->get()
						  ->map(function ($item) {
						  	return [
						  		'id'      => $item->id,
						  		'text'    => $item->so_number,
						  		'tanggal' => Carbon::parse($item->so_date)->format('d F Y'),
						  	];
						  });

		$count     = count($pesanan);
		$endCount  = $offset + $resultCount;
		$morePages = $count > $endCount;

		$results = [
			'results'    => $pesanan,
			'pagination' => [
				'more' => $morePages,
			],
		];

		return response()->json($results);
	}

	public function getAllPelanggan($request)
	{
		$page        = $request->get('page');
		$resultCount = 25;

		$offset = ($page - 1) * $resultCount;

		$pelanggan = $this->informasiPelanggan
						  ->where(function ($query) use ($request) {
						  	$query->where('no_pelanggan', 'LIKE', '%' . $request->search . '%')
									->orWhere('nama', 'LIKE', '%' . $request->search . '%');
						  })
						  ->whereHas('tipePelanggan', function ($query) {
						  	$query->where('nama', 'Anggota')
								  ->orWhere('nama', 'POS')
								  ->orWhere('nama', 'Pelanggan Baru Umum')
								  ->orWhere('nama', 'Trust');
						  })
						  ->skip($offset)
						  ->take($resultCount)
						  ->get()
						  ->map(function ($item) {
						  	return [
						  		'id'           => $item->id,
						  		'text'         => $item->nama,
						  		'no_pelanggan' => $item->no_pelanggan,
						  	];
						  });

		$count     = count($pelanggan);
		$endCount  = $offset + $resultCount;
		$morePages = $count > $endCount;

		$results = [
			'results'    => $pelanggan,
			'pagination' => [
				'more' => $morePages,
			],
		];

		return response()->json($results);
	}

	public function getPesananPenjualan($request)
	{
		if ('edit' == $request->type_submit) {
			return $this->barang_pesanan_penjualan->findOrFail($request->id);
		} else {
			return $this->barang_pesanan_penjualan->where('pesanan_penjualan_id', $request->pesanan_penjualan_id)->get();
		}
	}

	/**
	 * menambahkan data berdasarkan request.
	 *
	 * @param [type] $request [description]
	 *
	 * @return [type] [description]
	 */
	public function insert($data)
	{
	}

	public function storePos($request)
	{
		DB::beginTransaction();

		$data                          = $request->except(['nilai_bayar_fisik', 'nilai_bayar_elektronik']);
		$nilai_bayar_fisik             = floatval(preg_replace('/[^\d.]/', '', $request->get('nilai_bayar_fisik', 0)));
		$nilai_bayar_elektronik_debit  = floatval(preg_replace('/[^\d.]/', '', $request->get('nilai_debit', 0)));
		$nilai_bayar_elektronik_kredit = floatval(preg_replace('/[^\d.]/', '', $request->get('nilai_kredit', 0)));
		$data['nilai_bayar']           = $nilai_bayar_elektronik_kredit + $nilai_bayar_elektronik_debit + $nilai_bayar_fisik;
		$data['nilai_kembalian']       = floatval(preg_replace('/[^\d.]/', '', $data['nilai_kembalian']));
		$data['nilai_diskon_rupiah']   = floatval(preg_replace('/[^\d.]/', '', $data['nilai_diskon_rupiah']));
		// $data['nilai_diskon_persen']            = floatval(preg_replace('/[^\d.]/', '', $data['nilai_diskon_persen']));
		// $data['nilai_diskon']                   = floatval(preg_replace('/[^\d.]/', '', $data['nilai_diskon']));

		$total_all = $data['nilai_bayar'] - $data['nilai_kembalian'];

		// dd($nilai_bayar_fisik, $nilai_bayar_elektronik_debit, $data['nilai_bayar'], $data['nilai_kembalian'], $data['nilai_diskon_rupiah'], $total_all);

		try {
			if (array_key_exists('tanggal_custom', $data)) {
				$carbonNow            = $data['tanggal_custom'] ?? \Carbon\Carbon::now();
				$data['no_penjualan'] = $this->generateNoPenjualanCustom($carbonNow);
			} else {
				$carbonNow = \Carbon\Carbon::now();
			}

			/**
			 * Display a listing of the resource.
			 *
			 * @return \Illuminate\Http\Response
			 */
			$pelangganPos = $this->getInformasiPelangganPos();

			$selected_pelanggan = $this->informasiPelanggan->findOrFail($data['id']);

			// Pesanan Penjualan
			$pesanan_penjualan = $this->pesanan_penjualan->create([
				'pelanggan_id'          => $selected_pelanggan->id ?? $pelangganPos->id,
				'pelanggan_kirim_ke_id' => null,
				'alamat_tagihan'        => null,
				'alamat_pengiriman'     => $selected_pelanggan->alamat ?? '',
				'po_number'             => null,
				'so_number'             => $data['no_penjualan'],
				'so_date'               => $carbonNow,
				'ship_date'             => null,
				'ship_id'               => null,
				'total'                 => '0' === $data['tipe_pembayaran'] ? $total_all : $data['nilai_pelunasan'],
				'created_by'            => auth()->user()->id,
			]);

			// Pengiriman Penjualan
			$pengirimanPenjualan = $this->pengirimanPenjualan->create([
				'pelanggan_id'          => $selected_pelanggan->id ?? $pelangganPos->id,
				'pelanggan_kirim_ke_id' => null,
				'alamat_tagihan'        => null,
				'alamat_pengiriman'     => $selected_pelanggan->alamat ?? '',
				'delivery_no'           => $data['no_penjualan'],
				'delivery_date'         => $carbonNow,
				'po_no'                 => null,
				'ship_id'               => null,
				'keterangan'            => null,
				'cetak'                 => 0,
				'status_modul'          => 1,
				'kasir_mesin_id'        => session()->get('kasir_mesin'),
				'created_by'            => auth()->user()->id,
			]);
			// Faktur Penjualan
			$fakturPenjualan = $this->fakturPenjualan->create([
				'pelanggan_id'         => $selected_pelanggan->id ?? $pelangganPos->id,
				'no_faktur'            => $data['no_penjualan'],
				'alamat_pengiriman'    => $selected_pelanggan->alamat ?? '',
				'diskon'               => $data['nilai_diskon_persen'],
				'jumlah_diskon_faktur' => $data['nilai_diskon_rupiah'],
				'po_no'                => '',
				'invoice_date'         => $carbonNow,
				'no_fp_std'            => '',
				'no_fp_std2'           => '',
				'no_fp_std_date'       => $carbonNow,
				'ongkir'               => null,
				'total'                => '0' === $data['tipe_pembayaran'] ? $total_all : $data['nilai_pelunasan'],
				'status_modul'         => 1,
				'kasir_mesin_id'       => session()->get('kasir_mesin'),
				'created_by'           => auth()->user()->id,
			]);

			// Riwayat Pembayaran
			$riwayat_pembayaran = $this->riwayat_pembayaran->create([
				'kembalian'           => $data['nilai_kembalian'],
				'dibayar'             => $data['nilai_bayar'],
				'diskon_persen'       => $data['nilai_diskon_persen'],
				'diskon_rupiah'       => $data['nilai_diskon_rupiah'],
				'faktur_penjualan_id' => $fakturPenjualan->id,
			]);

			$identitas = $this->identitas->first();

			// Kalau tidak hutang
			if ('0' === $data['tipe_pembayaran']) {
				$lastPenerimaanPenjualan = $this->penerimaanPenjualan->get();

				// if ($lastPenerimaanPenjualan->isNotEmpty()) {
				//     $noPenerimaanPenjualan = $lastPenerimaanPenjualan->last()->id;
				// } else {
				//     $noPenerimaanPenjualan = 1;
				// }

				if (null !== $identitas) {
					$rate = $identitas->mataUang->nilai_tukar ?? 1;
				} else {
					$rate = 1;
				}

				if ($nilai_bayar_elektronik_debit > 0) {
					$data_penerimaan_edc_debit = [
						'rate'                => $rate,
						'carbonNow'           => $carbonNow,
						'pelangganPos'        => $pelangganPos,
						'selected_pelanggan'  => $selected_pelanggan,
						'faktur_penjualan_id' => $fakturPenjualan,
						'nilai_debit'         => $nilai_bayar_elektronik_debit,
					];

					$this->penerimaanPenjualanEdcDebitSave($data_penerimaan_edc_debit, $data);
				}

				if ($nilai_bayar_fisik > 0) {
					$penerimaanPenjualan = $this->penerimaanPenjualan->create([
						'pelanggan_id'      => $selected_pelanggan->id ?? $pelangganPos->id,
						'akun_bank_id'      => $this->preferensiPos->first()->akun_cash_id,
						'rate'              => $rate,
						'cheque_no'         => null,
						'cheque_date'       => null,
						'memo'              => 'Pembayaran POS',
						'form_no'           => $data['no_penjualan'],
						'payment_date'      => $carbonNow,
						'kosong'            => 0,
						'fiscal_payment'    => null,
						'cheque_amount'     => 0,
						'existing_credit'   => null,
						'distribute_amount' => 0,
						'created_by'        => auth()->user()->id,
					]);
				}

				$nomerAnggota = (null !== $selected_pelanggan) ? null : $selected_pelanggan->no_pelanggaan;
				if (0.0 !== $nilai_bayar_fisik) {
					// bayar fisik
					$penerimaanPenjualan->paymentHistory()->save(
							$paymenHistory = new PaymentHistory([
								'status'     => 0,
								'total'      => $nilai_bayar_fisik,
								'created_by' => auth()->user()->id,
							])
						);
				}
			}

			// Transaski Faktur Penjualan

			$akumulasi_diskon_item = 0;
			$harga_total           = 0;
			$arrayDebit            = [];
			$arrayKredit           = [];

			// $cart = Cart::instance('penjualan-'.auth()->user()->id);
			$cart     = $request['cart'];
			$cartItem = [];
			foreach ($cart->items as $item) {
				$cartItem[] = [
					'id'      => $item['id'],
					'name'    => $item['name'],
					'qty'     => $item['qty'],
					'price'   => $item['price'],
					'options' => [
						'produk'         => $item['produk'],
						'harga_modal'    => $item['harga_modal'],
						'harga_terakhir' => $item['harga_terakhir'],
						'price_real'     => $item['price_real'],
						'price_final'    => $item['price_final'],
						'order_id'       => $item['order_id'],
					],
				];

				$produk = null !== $item['produk']->produk ? $item['produk']->produk : $item['produk'];

				// TODO membuat decimal qty
				// 1. ubah migrasi yang berhubungan dengan qty, dari integer ke float
				//    - list table dan field
				//      - barang_pesanan_penjualan = jumlah
				//      - barang_pengiriman_penjualan = jumlah
				//      - produk_detail = kuantitas
				//      - barang_faktur_penjualan = jumlah
				// 2. Cek proses penjualan nya apakah baik baik saja
				// 3. Selesai

				// Barang Pesanan Penjualan
				$barangPesananPenjualan = $pesanan_penjualan->barang()->save(
					new BarangPesananPenjualan([
						'produk_id'                     => $produk->id,
						'pesanan_penjualan_id'          => $pesanan_penjualan->id,
						'barang_penawaran_penjualan_id' => null,
						'item_deskripsi'                => $item['name'],
						'jumlah'                        => $item['qty'],
						'satuan'                        => 'PCS',
						'harga'                         => $item['price'],
						'harga_modal'                   => $item['harga_modal'],
						'created_by'                    => auth()->user()->id,
					])
				);

				// Barang Pengimriman Penjualan
				$barangPengirimanPenjualan = $pengirimanPenjualan->barangPengirimanPenjualan()->save(
					new BarangPengirimanPenjualan([
						'produk_id'                   => $produk->id,
						'pengiriman_penjualan_id'     => $pengirimanPenjualan->id,
						'barang_pesanan_penjualan_id' => $barangPesananPenjualan->id ?? null,
						'item_deskripsi'              => $item['name'],
						'jumlah'                      => $item['qty'],
						'item_unit'                   => 'PCS',
						'gudang_id'                   => 6,
						'harga_modal'                 => $item['harga_modal'],
					])
				);

				// update status pesanan penjualan
				if (null !== $barangPengirimanPenjualan->barang_pesanan_penjualan_id) {
					$barangPengirimanPenjualan->barangPesananPenjualan->pesananPenjualan->update([
						'status' => 1,
					]);
				}

				// Transaksi Pengiriman Penjualan

				$arrayBarangTerkirim = [
					'nominal' => $item['harga_modal'] * $item['qty'],
					'tanggal' => $carbonNow,
					'produk'  => $produk->id,
					'akun'    => $produk->akun_barang_terkirim_id,
					'dari'    => 'Barang Terkirim',
				];

				$transaksi = $this->transaksi->create([
					'nominal'             => $arrayBarangTerkirim['nominal'],
					'tanggal'             => $carbonNow,
					'akun_id'             => $arrayBarangTerkirim['akun'],
					'produk_id'           => $arrayBarangTerkirim['produk'],
					'item_id'             => $pengirimanPenjualan->id,
					'item_type'           => get_class($this->pengirimanPenjualan),
					'status'              => 1,
					'no_transaksi'        => $pengirimanPenjualan->delivery_no,
					'sumber'              => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->pengirimanPenjualan)))->getShortName(), ' '),
					'keterangan'          => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->pengirimanPenjualan)))->getShortName(), ' ') . ' ' . $data['no_penjualan'],
					'dari'                => $arrayBarangTerkirim['dari'],
					'status_rekonsiliasi' => null,
					'desc_dev'            => '',
					'created_at'          => $carbonNow,
				]);

				$produkDetail[] = [
					'no_faktur'      => 1,
					'tanggal'        => $carbonNow,
					'kuantitas'      => -$item['qty'],
					'harga_modal'    => $item['harga_modal'],
					'harga_terakhir' => $item['harga_terakhir'],
					'gudang_id'      => 6,
					'transaksi_id'   => $transaksi->id,
					'sn'             => null,
					'status'         => 1,
					'item_type'      => get_class($this->pengirimanPenjualan), // dari pengiriman
					'item_id'        => $pengirimanPenjualan->id, // dari pengiriman
					'produk_id'      => $arrayBarangTerkirim['produk'],
					'created_at'     => $carbonNow,
				];

				$arrayPersediaan = [
					'nominal' => $item['harga_modal'] * $item['qty'],
					'tanggal' => $carbonNow,
					'produk'  => $produk->id,
					'akun'    => $produk->akun_persedian_id,
					'dari'    => 'Persediaan',
				];

				$this->transaksi->create([
					'nominal'             => $arrayPersediaan['nominal'],
					'tanggal'             => $carbonNow,
					'akun_id'             => $arrayPersediaan['akun'],
					'produk_id'           => $arrayPersediaan['produk'],
					'item_id'             => $pengirimanPenjualan->id,
					'item_type'           => get_class($this->pengirimanPenjualan),
					'status'              => 0,
					'no_transaksi'        => $pengirimanPenjualan->delivery_no,
					'sumber'              => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->pengirimanPenjualan)))->getShortName(), ' '),
					'keterangan'          => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->pengirimanPenjualan)))->getShortName(), ' ') . ' ' . $data['no_penjualan'],
					'dari'                => $arrayPersediaan['dari'],
					'status_rekonsiliasi' => null,
					'desc_dev'            => '',
					'created_at'          => $carbonNow,
				]);

				// Barang Faktur Penjualan

				$bagian_diskon_master = ($item['price_real'] / $this->getCartRealTotal($cart)) * ($data['nilai_diskon_rupiah'] ?? 0);

				$harga_final = number_format((float) ($item['price_real'] - $bagian_diskon_master), 2, '.', '');

				$fakturPenjualan->barang()->save(
					new BarangFakturPenjualan([
						'produk_id'                      => $produk->id,
						'barang_pengiriman_penjualan_id' => $barangPengirimanPenjualan->id,
						'item_deskripsi'                 => $produk->keterangan,
						'item_unit'                      => 'PCS',
						'unit_price'                     => $produk->harga_jual,
						'jumlah'                         => $item['qty'],
						'harga_final'                    => $harga_final,
						'bagian_diskon_master'           => $bagian_diskon_master,
						'diskon'                         => $item['diskon'] ?? 0,
						'gudang_id'                      => 6,
						'harga_modal'                    => $item['harga_modal'],
						'created_at'                     => $carbonNow,
					])
				);
				// Transaski Faktur Penjualan

				$preferensiPosPajak = 1 == $this->preferensiPos->first()->status ? $this->preferensiPos->first()->kodePajak->nilai : 0;

				$total_pajak = ($harga_final * $preferensiPosPajak) / 100;

				$harga_total += ($harga_final * $item['qty']) + $total_pajak;

				if (1 == $this->preferensiPos->first()->status) {
					$arrayPajak = [
						'nominal' => $total_pajak,
						'tanggal' => $carbonNow,
						'produk'  => $produk->id,
						'akun'    => $this->preferensiPos->first()->kodePajak->akun_pajak_penjualan_id,
						'dari'    => 'Pajak ' . $this->preferensiPos->first()->kode_pajak_id . ' per Item',
					];

					$this->transaksi->create([
						'nominal'             => $arrayPajak['nominal'],
						'tanggal'             => $carbonNow,
						'akun_id'             => $arrayPajak['akun'],
						'produk_id'           => $arrayPajak['produk'] ?? null,
						'item_id'             => $fakturPenjualan->id,
						'item_type'           => get_class($this->fakturPenjualan),
						'status'              => 0,
						'no_transaksi'        => $fakturPenjualan->no_faktur,
						'sumber'              => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->fakturPenjualan)))->getShortName(), ' '),
						'keterangan'          => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->fakturPenjualan)))->getShortName(), ' ') . ' ' . $data['no_penjualan'],
						'dari'                => $arrayPajak['dari'],
						'status_rekonsiliasi' => null,
						'desc_dev'            => '',
						'created_at'          => $carbonNow,
					]);
				}

				$barang            = $item['price'] * $item['qty'];
				$total_diskon      = ($barang * $item['diskon']) / 100;
				$total_barang      = $barang - $total_diskon;
				$total_harga_modal = $item['harga_modal'] * $item['qty'];
				$akumulasi_diskon_item += $total_diskon;

				$arrayKredit[] = [
					'nominal' => $barang,
					'tanggal' => $carbonNow,
					'dari'    => 'Penjualan dengan Tax & Disc',
					'produk'  => $produk->id,
					'akun'    => $produk->akun_penjualan_id,
				];

				$arrayDebit[] = [
					'nominal' => $total_harga_modal,
					'tanggal' => $carbonNow,
					'dari'    => 'Penjualan HPP',
					'produk'  => $produk->id,
					'akun'    => $produk->akun_hpp_id,
				];

				$arrayKredit[] = [
					'nominal' => $total_harga_modal,
					'tanggal' => $carbonNow,
					'dari'    => 'Barang Terkirim',
					'produk'  => $produk->id,
					'akun'    => $produk->akun_barang_terkirim_id,
				];

				$arrayDebit[] = [
					'nominal' => $total_diskon,
					'tanggal' => $carbonNow,
					'produk'  => $produk->id,
					'dari'    => 'Selisih Diskon',
					'akun'    => $produk->akun_disk_penjualan_id,
				];
			}

			$arrayDebit[] = [
				'nominal' => $data['nilai_diskon'] ?? 0,
				'tanggal' => $carbonNow,
				'dari'    => 'Diskon Penjualan',
				'akun'    => $this->preferensiPos->first()->diskon_penjualan_id,
			];

			$arrayDebit[] = [
				'nominal' => $harga_total,
				'tanggal' => $carbonNow,
				'dari'    => 'Total Penjualan',
				'akun'    => $this->preferensiPos->first()->akun_hutang_id,
			];

			// Tambah field edc di invoice penerimaan penjualan
			// bisa null
			// 0 => false
			// 1 => true

			// Tambah Uang Modal di Preferensi POS

			// Cek Qty di Barang

			if ('0' === $data['tipe_pembayaran']) {
				if ($nilai_bayar_fisik > 0) {
					$invoicePenerimanaanPenjualan = $this->invoicePenerimanaanPenjualan->create([
						'faktur_id'               => $fakturPenjualan->id,
						'payment_amount'          => $harga_total,
						'diskon'                  => 0,
						'last_owing'              => 0,
						'diskon_date'             => null,
						'penerimaan_penjualan_id' => $penerimaanPenjualan->id,
						'tanggal'                 => $carbonNow,
					]);

					$arrayInvoicePenerimaanPenjualanKredit = [
						'nominal' => $harga_total,
						'dari'    => 'Payment Amount Kredit',
						'akun'    => $this->preferensiPos->first()->akun_hutang_id,
					];

					$arrayInvoicePenerimaanPenjualanDebit = [
						'nominal' => $harga_total,
						'dari'    => 'Payment Amount Debit',
						'akun'    => $penerimaanPenjualan->akun_bank_id,
					];

					$this->transaksi->create([
						'nominal'             => $arrayInvoicePenerimaanPenjualanKredit['nominal'],
						'tanggal'             => $carbonNow,
						'akun_id'             => $arrayInvoicePenerimaanPenjualanKredit['akun'],
						'produk_id'           => $arrayInvoicePenerimaanPenjualanKredit['produk'] ?? null,
						'item_id'             => $penerimaanPenjualan->id,
						'item_type'           => get_class($this->penerimaanPenjualan),
						'status'              => 0,
						'no_transaksi'        => $penerimaanPenjualan->form_no,
						'sumber'              => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->penerimaanPenjualan)))->getShortName(), ' '),
						'keterangan'          => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->penerimaanPenjualan)))->getShortName(), ' ') . ' ' . $data['no_penjualan'],
						'dari'                => $arrayInvoicePenerimaanPenjualanKredit['dari'],
						'status_rekonsiliasi' => null,
						'desc_dev'            => '',
						'created_at'          => $carbonNow,
					]);

					$this->transaksi->create([
						'nominal'             => $arrayInvoicePenerimaanPenjualanDebit['nominal'],
						'tanggal'             => $carbonNow,
						'akun_id'             => $arrayInvoicePenerimaanPenjualanDebit['akun'],
						'produk_id'           => $arrayInvoicePenerimaanPenjualanDebit['produk'] ?? null,
						'item_id'             => $penerimaanPenjualan->id,
						'item_type'           => get_class($this->penerimaanPenjualan),
						'status'              => 1,
						'no_transaksi'        => $penerimaanPenjualan->form_no,
						'sumber'              => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->penerimaanPenjualan)))->getShortName(), ' '),
						'keterangan'          => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->penerimaanPenjualan)))->getShortName(), ' ') . ' ' . $data['no_penjualan'],
						'dari'                => $arrayInvoicePenerimaanPenjualanDebit['dari'],
						'status_rekonsiliasi' => null,
						'desc_dev'            => '',
						'created_at'          => $carbonNow,
					]);
				}
			}

			foreach ($arrayDebit ?? [] as $debit) {
				$this->transaksi->create([
					'nominal'             => $debit['nominal'],
					'tanggal'             => $carbonNow,
					'akun_id'             => $debit['akun'],
					'produk_id'           => $debit['produk'] ?? null,
					'item_id'             => $fakturPenjualan->id,
					'item_type'           => get_class($this->fakturPenjualan),
					'status'              => 1,
					'no_transaksi'        => $fakturPenjualan->no_faktur,
					'sumber'              => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->fakturPenjualan)))->getShortName(), ' '),
					'keterangan'          => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->fakturPenjualan)))->getShortName(), ' ') . ' ' . $data['no_penjualan'],
					'dari'                => $debit['dari'],
					'status_rekonsiliasi' => null,
					'desc_dev'            => '',
					'created_at'          => $carbonNow,
				]);
			}

			foreach ($arrayKredit ?? [] as $kredit) {
				$this->transaksi->create([
					'nominal'             => $kredit['nominal'],
					'tanggal'             => $carbonNow,
					'akun_id'             => $kredit['akun'],
					'produk_id'           => $kredit['produk'] ?? null,
					'item_id'             => $fakturPenjualan->id,
					'item_type'           => get_class($this->fakturPenjualan),
					'status'              => 0,
					'no_transaksi'        => $fakturPenjualan->no_faktur,
					'sumber'              => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->fakturPenjualan)))->getShortName(), ' '),
					'keterangan'          => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->fakturPenjualan)))->getShortName(), ' ') . ' ' . $data['no_penjualan'],
					'dari'                => $kredit['dari'],
					'status_rekonsiliasi' => null,
					'desc_dev'            => '',
					'created_at'          => $carbonNow,
				]);
			}

			$this->saldoAwalBarang->insert($produkDetail);

			$harga_jual = $cart->items->reduce(function ($output, $carry) {
				return $output + ($carry['price'] * $carry['qty']);
			}, 0);

			CartPenjualan::destroyPenjualan($request['cart_id']);
			// $cart->destroy();

			DB::commit();

			$no_faktur = str_replace('/', '-', $fakturPenjualan->no_faktur);

			if ('0' === $data['tipe_pembayaran']) {
				$dataToDisplay = [
					'status'    => 'kembalian',
					'kembalian' => 'Rp.' . number_format($data['nilai_kembalian']),
				];
				$this->sendDisplayKasir($dataToDisplay);
			}

			PDF::setOptions(['dpi' => 203])->loadView('pos::penjualan.struk', [
				'data'       => $data,
				'cart'       => $cartItem,
				'harga_jual' => $harga_jual,
				'total'      => $harga_jual - ($data['nilai_diskon_rupiah'] ?? 0),
				'tunai'      => $data['nilai_bayar'] ?? 0,
				'diskon'     => $data['nilai_diskon_rupiah'] ?? 0,
				'kembali'    => '0' === $data['tipe_pembayaran'] ? $data['nilai_kembalian'] : 0,
				'tanggal'    => $carbonNow,
				'kasir'      => auth()->user()->name ?? '-',
			])->setPaper([
				0, 0, 227, (200 + (count($cartItem) * 30)),
			])
			->save(
				storage_path('app/struk/' . $no_faktur . '.pdf')
			);
			// Jika pakai printer USB Epson.. jika tidak pakai hapus aja fungsi dibawah ini
			// $dataPrinter = [];
			// $dataPrinter['no_penjualan'] = $request->get('no_penjualan');
			// $dataPrinter['total'] = $harga_jual - (str_replace(',', '', $data['nilai_diskon'] ?? 0));
			// $dataPrinter['tunai'] = $data['nilai_bayar'] ?? 0;
			// $dataPrinter['diskon'] = $data['nilai_diskon'] ?? 0;
			// $dataPrinter['kembali']= $data['nilai_kembalian'] ?? 0;
			// $dataPrinter['tanggal'] = $carbonNow;

			// $this->printRaw($cartItem, $dataPrinter, 'EPSON TM-T82 Receipt');
			// Sampai sini ...

			//PART: Pelanggan
			$pelanggan = $this->informasiPelanggan->where('id', $data['id'])->first();

			$response = [
				'status'          => true,
				'message'         => 'Transaksi Berhasil Dilakukan ',
				'exhange_message' => 'Kembalian Rp.' . number_format($data['nilai_kembalian'] ?? 0),
				'data'            => [
					'cart'            => $cartItem,
					'harga_jual'      => number_format($harga_jual),
					'total'           => number_format($harga_jual - ($data['nilai_diskon_rupiah'] ?? 0)),
					'tunai'           => number_format($data['nilai_bayar'] ?? 0),
					'diskon'          => number_format($data['nilai_diskon_rupiah'] ?? 0),
					'kembali'         => '0' === $data['tipe_pembayaran'] ? number_format($data['nilai_kembalian'] ?? 0) : 0,
					'tanggal'         => Carbon::now('Asia/Makassar')->format('Y-m-d H:i:s'),
					'kasir'           => auth()->user()->name ?? '-',
					'nama_perusahaan' => $identitas->nama_perusahaan ?? '-',
					'alamat'          => $identitas->alamat ?? '-',
					// 'footer_struk'      => $identitas->footer_struk ?? '-',
					'pesan'            => json_decode($identitas->footer_struk)->pesan,
					'slogan'           => json_decode($identitas->footer_struk)->slogan,
					'pelanggan'        => $pelanggan->nama ?? null,
					'no_penjualan'     => $this->generateNoPenjualanCustom($carbonNow),
					'no_penjualan_now' => $data['no_penjualan'],
					'tipe_pembayaran'  => $data['tipe_pembayaran'],
				],
			];

			Cart::instance('struk-' . auth()->user()->id)->destroy();

			Cart::instance('struk-' . auth()->user()->id)->add([
				'id'      => '1',
				'name'    => 'cetak_ulang',
				'qty'     => '1',
				'price'   => '0',
				'options' => [
					'cart'            => $cartItem,
					'harga_jual'      => number_format($harga_jual),
					'total'           => number_format($harga_jual - ($data['nilai_diskon_rupiah'] ?? 0)),
					'tunai'           => number_format($data['nilai_bayar'] ?? 0),
					'diskon'          => number_format($data['nilai_diskon_rupiah'] ?? 0),
					'kembali'         => '0' === $data['tipe_pembayaran'] ? number_format($data['nilai_kembalian'] ?? 0) : 0,
					'tanggal'         => Carbon::now('Asia/Makassar')->format('Y-m-d H:i:s'),
					'kasir'           => auth()->user()->name ?? '-',
					'nama_perusahaan' => $identitas->nama_perusahaan ?? '-',
					'alamat'          => $identitas->alamat ?? '-',
					// 'footer_struk'      => $identitas->footer_struk ?? '-',
					'pesan'            => json_decode($identitas->footer_struk)->pesan,
					'slogan'           => json_decode($identitas->footer_struk)->slogan,
					'pelanggan'        => $pelanggan->nama ?? null,
					'no_penjualan'     => $this->generateNoPenjualanCustom($carbonNow),
					'no_penjualan_now' => $data['no_penjualan'],
					'tipe_pembayaran'  => $data['tipe_pembayaran'],
				],
			]);

			if (env('USE_KOP')) {
				$penjualanToApi = [
					'tanggal'       => date('Y-m-d'),
					'total'         => $harga_jual - ($data['nilai_diskon_rupiah'] ?? 0),
					'jml_umkm'      => '0',
					'jml_non'       => $harga_jual - ($data['nilai_diskon_rupiah'] ?? 0),
					'nomor_anggota' => $pelanggan->no_pelanggan ?? '0',
					'anggota_id'    => $pelanggan->id ?? '0',
				];
				dispatch(new PenjualanQueue($penjualanToApi))->delay(now('Asia/Makassar')->second(5));
			}
			Cart::instance('penjualan-' . auth()->user()->id)->destroy();
			Cart::instance('edc-debit-' . auth()->user()->id)->destroy();
		} catch (Exception $e) {
			DB::rollBack();

			$response = [
				'status'  => false,
				'message' => $e->getMessage(),
			];
		}

		return response()->json($response);
	}

	/**
	 * ini berfungisi untuk melakukan filter terhadap
	 * data yang akan diambil dan ditampilkan kepada
	 * user nantinya.
	 *
	 * @param array $data
	 *
	 * @return mix
	 */
	public function filter($data)
	{
		return $this->model->filter($data)->get();
	}

	public function getProduk($request)
	{
		if (!empty($request->barcode_barang)) {
			$produk = $this->produk->where('barcode', $request->barcode_barang)
									->orWhere('no_barang', $request->barcode_barang)->first();

			return $produk;
		} elseif (!empty($request->id)) {
			$produk = $this->produk->findOrFail($request->id);

			return $produk;
		} else {
			$produk = null;

			return $produk;
		}
	}

	public function diskonItem($request)
	{
		try {
			$produk = $this->getProduk($request);

			if (null === $produk) {
				$response = [
					'status' => false,
				];
			} else {
				$response = [
					'status' => true,
					'data'   => $produk,
				];
			}
		} catch (Exception $e) {
			$response = [
				'status' => false,
			];
		}

		return response()->json($response);
	}

	public function getAllProdukTest($request)
	{
		$gudang_toko = $this->gudang->where('nama', 'Gudang Toko')->get();

		if ($gudang_toko->isNotEmpty()) {
			$produk = $this->produk->whereNotIn('tipe_barang', ['3', '5', '4'])->whereHas('saldoAwalBarang', function ($q) use ($gudang_toko) {
				$q->whereIn('gudang_id', $gudang_toko->pluck('id'));
			})
			 ->whereNotNull('akun_persedian_id')
			 ->whereNotNull('akun_penjualan_id')
			 ->whereNotNull('akun_ret_penjualan_id')
			 ->whereNotNull('akun_disk_penjualan_id')
			 ->whereNotNull('akun_barang_terkirim_id');

			if ($request->get('q')) {
				$constrains = $produk;
				$produk     = $this->produk->search($request->get('q'))->constrain($constrains);
			}

			$get_produk = $produk->get()
								   ->map(function ($item) {
								   	return [
								   		'id'       => $item->id,
								   		'text'     => $item->keterangan,
								   		'barcode'  => empty($item->barcode) ? $item->no_barang : $item->barcode,
								   		'fullName' => $item->no_barang . ' - ' . $item->keterangan,
								   	];
								   });

			$results = [
				'results' => $get_produk,
			];
		} else {
			$results = [
				'results' => [],
			];
		}

		return response()->json($results);
	}

	public function getAllProduk($request)
	{
		$gudang_toko = $this->gudang->where('nama', 'Gudang Toko')->get();

		if ($gudang_toko->isNotEmpty()) {
			$produk = $this->produk->whereNotIn('tipe_barang', ['3', '5', '4'])->whereHas('saldoAwalBarang', function ($q) use ($gudang_toko) {
				$q->whereIn('gudang_id', $gudang_toko->pluck('id'));
			})->where(function ($query) use ($request) {
				$query->where('keterangan', 'LIKE', '%' . $request->get('q') . '%')
					   ->orWhere('barcode', 'LIKE', '%' . $request->get('q') . '%')
					   ->orWhere('no_barang', 'LIKE', '%' . $request->get('q') . '%');
			})
			 // ->whereNotNull('akun_beban_id')
			 ->whereNotNull('akun_persedian_id')
			 ->whereNotNull('akun_penjualan_id')
			 ->whereNotNull('akun_ret_penjualan_id')
			 ->whereNotNull('akun_disk_penjualan_id')
			 ->whereNotNull('akun_barang_terkirim_id')
			 // ->whereNotNull('akun_hpp_id')
			   ->get()
			   ->map(function ($item) {
			   	return [
			   		'id'        => $item->id,
			   		'text'      => $item->keterangan,
			   		'barcode'   => $item->barcode,
			   		'no_barang' => $item->no_barang,
			   		'fullName'  => $item->no_barang . ' - ' . $item->keterangan,
			   	];
			   });
			// dd($produk);
			$results = [
				'results' => $produk,
			];
		} else {
			$results = [
				'results' => [],
			];
		}

		return response()->json($results);
	}

	public function getAllKasirMesin($request)
	{
		$kasir_mesin = $this->kasirMesin->where('status', 0)->where('nama', 'LIKE', '%' . $request->get('search') . '%')->get();

		if ($kasir_mesin->isNotEmpty()) {
			$kasir = $kasir_mesin->map(function ($item) {
				return [
					'id'   => $item->id,
					'text' => $item->nama,
				];
			});

			$results = [
				'results' => $kasir,
			];
		} else {
			$results = [
				'results' => [],
			];
		}

		return response()->json($results);
	}

	public function getCartTotal()
	{
		$total = 0;

		$cart = Cart::instance('penjualan-diskon-' . auth()->user()->id);

		foreach ($cart->content() as $item) {
			$total += $item->options->price_final;
		}

		return $total;
	}

	public function getCartRealTotal($cart)
	{
		$total = 0;
		// $cart = Cart::instance('penjualan-'.auth()->user()->id);

		foreach ($cart->items as $item) {
			$total += $item['price_real'] * $item['qty'];
		}

		return $total;
	}

	public function getPreferensiPos()
	{
		if (null === $this->preferensiPos->first()) {
			return false;
		}

		if (null === $this->preferensiPos->first()->akunCash || null === $this->preferensiPos->first()->akunHutang || // $this->preferensiPos->first()->kodePajak === null ||
		null === $this->preferensiPos->first()->diskonPenjualan) {
			return false;
		}

		return true;
	}

	public function getInformasiPelangganPos()
	{
		return $this->informasiPelanggan->whereHas('tipePelanggan', function ($query) {
			$query->where('nama', 'POS')
				  ->orWhere('nama', 'Anggota');
		})->get()->reduce(function ($output, $item) {
			$output[$item->id] = $item->no_pelanggan . ' | ' . $item->nama;

			return $output;
		}, []);
	}

	public function getKasirMesin()
	{
		return $this->kasirMesin->where('status', false)->pluck('nama', 'id');
	}

	public function getTypeEdc()
	{
		return $this->type_edc->pluck('nama', 'id');
	}

	public function listPreferensiPos()
	{
		return $this->preferensiPos->with(['akunCash', 'akunHutang', 'kodePajak'])->first();
	}

	public function kasirCheckInStore($request)
	{
		DB::beginTransaction();

		try {
			if (null !== $request->kasir_mesin_id && null !== $request->check_state) {
				session([
					'kasir_cek'   => true,
					'kasir_mesin' => $request->kasir_mesin_id,
				]);

				$kasir_mesin = $this->kasirMesin->findOrFail($request->kasir_mesin_id);

				$kasir_mesin->update([
					'status' => 1,
				]);

				$this->kasirCek->create([
					'check_state'    => $request->check_state,
					'check_time'     => \Carbon\Carbon::now(),
					'modal'          => $this->preferensiPos->first()->uang_modal ?? 0,
					'kasir_mesin_id' => $request->kasir_mesin_id,
					'created_by'     => auth()->user()->id,
				]);

				DB::commit();

				return redirect()->route('pos.penjualan.index');
			} else {
				Session::flash('flash_notification', [
					'level'   => 'warning',
					'message' => 'Tolong cek kambali inputan anda',
				]);

				DB::rollBack();
			}
		} catch (Exception $e) {
			DB::rollBack();
		}

		return redirect()->back();
	}

	// Cek out kasir
	public function kasirCheckOutStore($request)
	{
		$data = $request->all();

		$data['uang_fisik'] = floatval(preg_replace('/[^\d.]/', '', $data['uang_fisik']));

		DB::beginTransaction();

		try {
			$kasir_mesin = $this->kasirMesin->findOrFail(session()->get('kasir_mesin'));

			$kasir_mesin->update([
				'status' => 0,
			]);

			$get_faktur_penjualan = $this->getTransaksi()->where('created_by', auth()->user()->id)->whereDate('invoice_date', Carbon::now()->format('Y-m-d'))->get();

			$sum_total_with_retur = 0;

			foreach ($get_faktur_penjualan  as $key => $total_faktur) {
				$sum_total_with_retur += $total_faktur->sum_harga_total;
			}

			$last_modal = $this->kasirCek->whereDate('check_time', \Carbon\Carbon::now()->format('Y-m-d'))
										 ->where(['check_state' => 'IN', 'kasir_mesin_id' => session()->get('kasir_mesin'), 'created_by' => auth()->user()->id])
										 ->orderBy('id', 'desc')
										 ->firstOrFail();

			$edc = $this->transactionNonCash->whereDate('created_at', \Carbon\Carbon::now()->format('Y-m-d'))
												   ->where('created_by', auth()->user()->id)
												   ->sum('total');

			$hutang = $this->getTransaksi()->doesntHave('invoice')->where('created_by', auth()->user()->id)->whereDate('invoice_date', Carbon::now()->format('Y-m-d'))->sum('total');

			$retur = $this->returPenjualan->where('created_by', auth()->user()->id)->whereDate('tanggal', Carbon::now()->format('Y-m-d'))->sum('total');

			$sum_total = $sum_total_with_retur - $retur;

			$this->kasirCek->create([
				'check_state'    => 'OUT',
				'check_time'     => Carbon::now(),
				'modal'          => $last_modal->modal,
				'kasir_mesin_id' => session()->get('kasir_mesin'),
				'created_by'     => auth()->user()->id,
			]);

			DB::commit();

			$nomor_report = sprintf('CheckOut-%s-%s-%s', auth()->user()->name, $last_modal->kasirMesin->nama, Carbon::now()->format('d-m-Y_H-i-s'));

			$user_name = auth()->user()->name ?? '-';

			// auth()->logout();

			$total_sistem = $sum_total + $last_modal->modal;

			return [
				'alamat'          => $this->identitas->first()->alamat,
				'kasir'           => $user_name,
				'kasir_mesin'     => $last_modal->kasirMesin->nama,
				'tanggal_cek_out' => Carbon::now('Asia/Makassar')->format('d/m/Y - H:i:s'),
				'uang_fisik'      => number_format($data['uang_fisik']),
				'uang_sistem'     => number_format($sum_total),
				'modal_awal'      => number_format($last_modal->modal),
				'total_sistem'    => number_format($total_sistem),
				'edc'             => number_format($edc),
				'selisih'         => number_format(($data['uang_fisik'] + $edc + $hutang) - $total_sistem),
				'uang_setoran'    => number_format($data['uang_fisik'] - $last_modal->modal),
				'nomor_report'    => $nomor_report,
				'hutang'          => number_format($hutang),
				'kepalaToko'      => $this->identitas->first()->kepala_toko,
			];
		} catch (Exception $e) {
			DB::rollBack();
		}
	}

	public function kasirCheckOutStoreJson($request)
	{
		$sum_total = $this->getTransaksi()->where('created_by', auth()->user()->id)->whereDate('invoice_date', Carbon::now()->format('Y-m-d'))->sum('total');

		$last_modal = $this->kasirCek->whereDate('check_time', \Carbon\Carbon::now()->format('Y-m-d'))
										 ->where(['check_state' => 'IN', 'kasir_mesin_id' => session()->get('kasir_mesin'), 'created_by' => auth()->user()->id])
										 ->orderBy('id', 'desc')
										 ->firstOrFail();

		$edc = $this->transactionNonCash->whereDate('created_at', \Carbon\Carbon::now()->format('Y-m-d'))
												   ->where('created_by', auth()->user()->id)
												   ->sum('total');

		$total_sistem = $sum_total + $last_modal->modal;

		$data = [
			'kasir'             => auth()->user()->name,
			'tgl_check_out'     => Carbon::now()->format('d/m/Y - H:i:s'),
			'kasir_mesin'       => $last_modal->kasirMesin->nama,
			'modal'             => $last_modal->modal,
			'uang_sistem'       => $sum_total,
			'total_uang_sistem' => $total_sistem,
			'uang_fisik'        => $request->money,
			'edc'               => $edc,
			// 'selisih' => $total_sistem - $request->money - $edc,
			// 'setoran' => $request->money - $last_modal->modal,
			// 'kepala_toko' => ''
		];

		return response()->json($data);
	}

	public function getTransaksi()
	{
		$request = request();

		return  $this->fakturPenjualan->filter($request)->with(['barang', 'transaksi' => function ($query) {
			$query->where('dari', 'Penjualan dengan Tax & Disc');
		}, 'kasirMesin', 'barang.produk', 'invoice', 'riwayatPembayaran', 'returPenjualan.barang'])->has('transaksi')->where('status_modul', 1)->whereNotNull('kasir_mesin_id');
		// dd($d);
	}

	// TODO new Format Nomor Penjualan
	// 1. Format = POS/20181101/user_id/id penjualan terakhir, yang disortir dari created_by dan tanggal hari ini ditambah 1
	// 2. Ambil data faktur hari ini dan user id yang sedang login
	// 3. Membuat kondisi apakah ada faktur hari ini yang sesuai dengan user id yang sedang login

	public function generateNoPenjualan()
	{
		$current_time = Carbon::now();
		// tanggal custom
		// $year = 2019; $month = 4; $day = 30;
		// $hour = 00; $minute = 00; $second = 00;
		// $current_time = Carbon::create($year, $month, $day, $hour, $minute, $second);

		$no_penjualan = $this->getNoPenjualan($current_time);

		return $no_penjualan;
	}

	public function generateNoPenjualanCustom($current_time)
	{
		return $this->getNoPenjualan($current_time);
	}

	public function getNoPenjualan($current_time)
	{
		if (is_string($current_time)) {
			$current_time = Carbon::parse($current_time);
		}

		if (empty($current_time)) {
			$current_time = Carbon::parse($current_time);
		}

		$faktur_penjualan = $this->fakturPenjualan->whereDate('created_at', $current_time->format('Y-m-d'))
												  // ->where('created_by', auth()->user()->id)
												  ->where('status_modul', 1)
												  ->get();

		if ($faktur_penjualan->isEmpty()) {
			// $no_penjualan = "POS/".$current_time->format('Ymd')."/".$auth."/1";
			$no_penjualan = $current_time->format('Y.m.d') . '.00001';
		} else {
			$no_faktur_sebelum = $faktur_penjualan->last()->no_faktur;
			$explode_string    = explode('.', $no_faktur_sebelum);
			// kondisi jika jual dengan api ketika auth tidak ter-input
			if (3 == count($explode_string)) {
				$new_urutan   = $explode_string[2] + 1;
				$no_penjualan = 'POS/' . $current_time->format('Ymd') . '/3/' . $new_urutan;
			} elseif (4 == count($explode_string)) {
				$new_urutan = $explode_string[3] + 1;
				// $no_penjualan = "POS/".$current_time->format('Ymd')."/".$auth."/".$new_urutan;
				$format_last_number = sprintf('%05d', $new_urutan);
				$no_penjualan       = $current_time->format('Y.m.d') . '.' . $format_last_number;
			}
		}

		return $no_penjualan;
	}

	public function nominalDibayar($nilai_bayar_fisik, $nilai_bayar_elektronik)
	{
		$dibayar = 0;
		if (0.0 !== $nilai_bayar_fisik) {
			$dibayar = $nilai_bayar_fisik;
		} elseif (0.0 !== $nilai_bayar_elektronik) {
			$dibayar = $nilai_bayar_elektronik;
		}

		return $dibayar;
	}

	public function sendDisplayKasir($dataToDisplay)
	{
		$kasirId = 'kasir_' . auth()->user()->id;

		event(new DisplayKasirEvent($kasirId, $dataToDisplay));

		return true;
	}

	public function penerimaanPenjualanEdcDebitSave($data_penerimaan_edc_debit, $data)
	{
		$cart_debit = Cart::instance('edc-debit-' . auth()->user()->id);

		foreach ($cart_debit->content() as $key => $debit) {
			$type_edc_akun_id = $this->type_edc->findOrFail($debit->options->type_edc_id);

			$penerimaanPenjualanEdcDebit = $this->penerimaanPenjualan->create([
				'pelanggan_id'      => $data_penerimaan_edc_debit['selected_pelanggan']->id,
				'akun_bank_id'      => $type_edc_akun_id->akun_id,
				'rate'              => $data_penerimaan_edc_debit['rate'],
				'cheque_no'         => null,
				'cheque_date'       => null,
				'memo'              => 'Pembayaran POS ' . $type_edc_akun_id->nama,
				'form_no'           => $data['no_penjualan'],
				'payment_date'      => $data_penerimaan_edc_debit['carbonNow'],
				'kosong'            => 0,
				'fiscal_payment'    => null,
				'cheque_amount'     => 0,
				'existing_credit'   => null,
				'distribute_amount' => 0,
				'created_by'        => auth()->user()->id,
			]);

			$invoicePenerimanaanPenjualanEdcDebit = $this->invoicePenerimanaanPenjualan->create([
				'faktur_id'               => $data_penerimaan_edc_debit['faktur_penjualan_id']->id,
				'payment_amount'          => $data_penerimaan_edc_debit['nilai_debit'] ?? 0,
				'diskon'                  => 0,
				'last_owing'              => 0,
				'diskon_date'             => null,
				'penerimaan_penjualan_id' => $penerimaanPenjualanEdcDebit->id,
				'tanggal'                 => $data_penerimaan_edc_debit['carbonNow'],
			]);

			// save payment history
			$paymenHistoryEdcDebit = $this->payment_history->create([
				'status'                  => 1,
				'total'                   => $data_penerimaan_edc_debit['nilai_debit'],
				'created_by'              => auth()->user()->id,
				'penerimaan_penjualan_id' => $penerimaanPenjualanEdcDebit->id,
			]);

			// save transaction non cash
			$paymenHistoryEdcDebit->transactionNonCash()->save(
				$transactionNonCashDebit = new TransactionNonCash([
					'no_transaksi_bank' => $data['no_penjualan'],
					'batch'             => $debit->options->batch,
					'no_kartu'          => $debit->options->no_kartu,
					'total'             => $data_penerimaan_edc_debit['nilai_debit'],
					'created_by'        => auth()->user()->id,
				])
			);

			$arrayInvoicePenerimaanPenjualanKredit = [
				'nominal' => $data_penerimaan_edc_debit['nilai_debit'],
				'dari'    => 'Payment Amount Kredit ' . $type_edc_akun_id->nama,
				'akun'    => $this->preferensiPos->first()->akun_hutang_id,
			];

			$arrayInvoicePenerimaanPenjualanDebit = [
				'nominal' => $data_penerimaan_edc_debit['nilai_debit'],
				'dari'    => 'Payment Amount Debit ' . $type_edc_akun_id->nama,
				'akun'    => $penerimaanPenjualanEdcDebit->akun_bank_id,
			];

			$this->transaksi->create([
				'nominal'             => $arrayInvoicePenerimaanPenjualanKredit['nominal'],
				'tanggal'             => $data_penerimaan_edc_debit['carbonNow'],
				'akun_id'             => $arrayInvoicePenerimaanPenjualanKredit['akun'],
				'produk_id'           => $arrayInvoicePenerimaanPenjualanKredit['produk'] ?? null,
				'item_id'             => $penerimaanPenjualanEdcDebit->id,
				'item_type'           => get_class($this->penerimaanPenjualan),
				'status'              => 0,
				'no_transaksi'        => $penerimaanPenjualanEdcDebit->form_no,
				'sumber'              => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->penerimaanPenjualan)))->getShortName(), ' '),
				'keterangan'          => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->penerimaanPenjualan)))->getShortName(), ' ') . ' ' . $data['no_penjualan'],
				'dari'                => $arrayInvoicePenerimaanPenjualanKredit['dari'],
				'status_rekonsiliasi' => null,
				'desc_dev'            => '',
				'created_at'          => $data_penerimaan_edc_debit['carbonNow'],
			]);

			$this->transaksi->create([
				'nominal'             => $arrayInvoicePenerimaanPenjualanDebit['nominal'],
				'tanggal'             => $data_penerimaan_edc_debit['carbonNow'],
				'akun_id'             => $arrayInvoicePenerimaanPenjualanDebit['akun'],
				'produk_id'           => $arrayInvoicePenerimaanPenjualanDebit['produk'] ?? null,
				'item_id'             => $penerimaanPenjualanEdcDebit->id,
				'item_type'           => get_class($this->penerimaanPenjualan),
				'status'              => 1,
				'no_transaksi'        => $penerimaanPenjualanEdcDebit->form_no,
				'sumber'              => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->penerimaanPenjualan)))->getShortName(), ' '),
				'keterangan'          => $this->transaksiHelper->fromCamelCase((new \ReflectionClass(get_class($this->penerimaanPenjualan)))->getShortName(), ' ') . ' ' . $data['no_penjualan'],
				'dari'                => $arrayInvoicePenerimaanPenjualanDebit['dari'],
				'status_rekonsiliasi' => null,
				'desc_dev'            => '',
				'created_at'          => $data_penerimaan_edc_debit['carbonNow'],
			]);

			return true;
		}
	}

	public function getTanggalCustom()
	{
		$preferensi_pos = $this->preferensiPos->first();

		if (!empty($preferensi_pos)) {
			return $preferensi_pos->tanggal_custom;
		} else {
			return false;
		}
	}

	public function getCetakUlang()
	{
		$cart = Cart::instance('struk-' . auth()->user()->id);

		$cart_item = $cart->content()->first();

		$response = [
			'data' => [
				'cart'             => $cart_item->options->cart,
				'harga_jual'       => $cart_item->options->harga_jual,
				'total'            => $cart_item->options->total,
				'tunai'            => $cart_item->options->tunai,
				'diskon'           => $cart_item->options->diskon,
				'kembali'          => $cart_item->options->kembali,
				'tanggal'          => $cart_item->options->tanggal,
				'kasir'            => $cart_item->options->kasir,
				'nama_perusahaan'  => $cart_item->options->nama_perusahaan,
				'alamat'           => $cart_item->options->alamat,
				'pesan'            => $cart_item->options->pesan,
				'slogan'           => $cart_item->options->slogan,
				'pelanggan'        => $cart_item->options->pelanggan,
				'no_penjualan'     => $cart_item->options->no_penjualan,
				'no_penjualan_now' => $cart_item->options->no_penjualan_now,
				'tipe_pembayaran'  => $cart_item->options->tipe_pembayaran,
			],
		];

		return response()->json($response);
	}

	public function cetakStrukDetail($data)
	{
		$identitas = $this->identitas->first();

		$faktur_penjualan = $this->fakturPenjualan->with(['barang.produk', 'riwayatPembayaran', 'pelanggan'])->findOrFail($data['faktur_id']);

		$data_cart = $faktur_penjualan->barang->map(function ($barang) {
			return [
				'name'  => $barang->item_deskripsi,
				'qty'   => $barang->jumlah,
				'price' => $barang->harga_final,
			];
		});

		$response = [
			'data' => [
				'alamat'           => $identitas->alamat,
				'tipe_pembayaran'  => $faktur_penjualan->status_penjualan_pos,
				'diskon'           => $faktur_penjualan->riwayatPembayaran->diskon_rupiah,
				'total'            => $faktur_penjualan->sum_harga_total,
				'tunai'            => $faktur_penjualan->riwayatPembayaran->dibayar,
				'kembali'          => $faktur_penjualan->riwayatPembayaran->kembalian,
				'no_penjualan_now' => $faktur_penjualan->no_faktur,
				'kasir'            => auth()->user()->name,
				'tanggal'          => Carbon::parse($faktur_penjualan->invoice_date)->format('Y-m-d H:i:s'),
				'pelanggan'        => $faktur_penjualan->pelanggan->nama,
				'pesan'            => json_decode($identitas->footer_struk)->pesan,
				'nama_perusahaan'  => $identitas->nama_perusahaan,
				'slogan'           => json_decode($identitas->footer_struk)->slogan,
				'cart'             => $data_cart,
			],
		];

		return response()->json($response);
	}

	public function checkedCart($cart, $request)
	{
		$new_array = array_filter($request->data_id_array);

		$array_cart = [];

		foreach ($cart->content() as $key => $item_cart) {
			$array_cart[] = $item_cart->id;
		}

		foreach ($new_array as $id => $qty) {
			if (!in_array($id, $array_cart)) {
				$produk         = $this->produk->findOrFail($id);
				$harga_jual     = $this->getTingkatHargaJual($produk, $request->id);
				$subTotalDiskon = ($harga_jual * ($produk->diskon ?? 0)) / 100;
				$priceReal      = $harga_jual - $subTotalDiskon;
				$totalDiskon    = $priceReal * $qty;
				Cart::instance('penjualan-' . auth()->user()->id)->add([
					'id'          => $produk->id,
					'name'        => $produk->keterangan,
					'qty'         => $qty,
					'price'       => $harga_jual ?? 0,
					'tipe_barang' => $produk->tipe_barang ?? null,
					'options'     => [
						'produk'         => $produk,
						'harga_modal'    => $produk->saldoAwalBarang->last()->harga_modal ?? 0,
						'harga_terakhir' => $produk->saldoAwalBarang->last()->harga_terakhir ?? 0,
						'price_real'     => $priceReal,
						'price_final'    => $totalDiskon,
						'order_id'       => 1,
						'diskon'         => 0,
					],
				]);
			}
		}

		return true;
	}

	public function getTingkatHargaJual($produk, $id)
	{
		$tingkat_harga_pelanggan = InformasiPelanggan::findOrFail($id)->tingkatan_harga_jual;

		if (!empty($tingkat_harga_pelanggan)) {
			if (!empty($produk->tingkatHargaBarang)) {
				if (1 === $tingkat_harga_pelanggan) {
					$harga_jual = $produk->tingkatHargaBarang->tingkat_1 ?? 0;
				} elseif (2 === $tingkat_harga_pelanggan) {
					$harga_jual = $produk->tingkatHargaBarang->tingkat_2 ?? 0;
				} elseif (3 === $tingkat_harga_pelanggan) {
					$harga_jual = $produk->tingkatHargaBarang->tingkat_3 ?? 0;
				} elseif (4 === $tingkat_harga_pelanggan) {
					$harga_jual = $produk->tingkatHargaBarang->tingkat_4 ?? 0;
				} elseif (5 === $tingkat_harga_pelanggan) {
					$harga_jual = $produk->tingkatHargaBarang->tingkat_5 ?? 0;
				} else {
					$harga_jual = $produk->harga_jual ?? 0;
				}
			} else {
				$harga_jual = $produk->harga_jual ?? 0;
			}
		} else {
			$harga_jual = $produk->harga_jual ?? 0;
		}

		return $harga_jual;
	}
}
