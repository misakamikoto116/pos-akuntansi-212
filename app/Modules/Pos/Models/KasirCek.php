<?php

namespace App\Modules\Pos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KasirCek extends Model
{
    protected $table = 'kasir_cek';
    use softDeletes;

    protected $fillable = [
        'check_state',
        'check_time',
        'modal',
        'kasir_mesin_id',
        'created_by',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function kasirMesin()
    {
        return $this->belongsTo(KasirMesin::class,'kasir_mesin_id');
    }
}
