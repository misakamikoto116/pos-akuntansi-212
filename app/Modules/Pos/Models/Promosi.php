<?php

namespace App\Modules\Pos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promosi extends Model
{
    use SoftDeletes;
    protected $table = 'promosi';

    protected $fillable = [
        'title',
        'active_date',
        'expired_date',
    ];

    protected $dates = [
        'active_date',
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function sebab()
    {
        return $this->hasMany(SebabPromosi::class);        
    }

    public function akibat()
    {
        return $this->hasMany(AkibatPromosi::class);        
    }

    public function getActiveDateFormattedAttribute()
    {
        return date('d F Y H:i:s',strtotime($this->getAttribute('active_date')));
    }

    public function getExpiredDateFormattedAttribute()
    {
        return date('d F Y H:i:s',strtotime($this->getAttribute('expired_date')));
    }

}
