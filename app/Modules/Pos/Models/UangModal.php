<?php

namespace App\Modules\Pos\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Pos\Models\KasirMesin;

class UangModal extends Model
{
    protected $table = 'uang_modal';

    protected $fillable = [
        'id',
        'mesin_id',
        'created_by',
        'created_at',
        'updated_at',
        'jumlah_uang'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function mesin()
    {
        return $this->belongsTo(KasirMesin::class);
    }
}
