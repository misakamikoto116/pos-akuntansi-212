<?php

namespace App\Modules\Pos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\Akuntansi\Models\Produk;

class AkibatPromosi extends Model
{
    use SoftDeletes;
    protected $table = 'akibat_promosi';

    /**
     * @NB
     * Kenapa ada total & presentase ? = agar mudah hitungnya/ngambilnya
     * Kenapa ada type sebab ? = agar mudah di query
     * 
     * Cara kerja : 
     * Jika di sebab sudah terpenuhi kondisinya
     * Maka di akibat men diskon harga sesuai akibatnya
     * 
     * misal : 
     * beli 2 gratis 1
     * maka jika sudah beli 2 maka = tambahkan item dengan yg ada di akibat yaitu 1
     * dan dipotong harganya brp % ? yaitu 100%
     * result : beli 2 gratis 1
     * 
     * gimana dengan kelipatan ? 
     * untuk mencari kondisi sebab maka dibutuhkan pembagian qty_beli / qty_sebab
     * 
     * ==========================================================================
     * 
     * update struktur table 
     * jadi sekarang sebab dan akibat adalah many to many, why? karena 
     * multi sebab bisa hanya punya 1 akibat, begitu pula sebaliknya
     * 
     */

    protected $fillable = [
        'promosi_id',
        'sebab_promosi_id',
        'produk_id',
        'qty',
        'total',
        'presentase'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;
    
    public function getTotalFormattedAttribute()
    {
        return "Rp. ".number_format($this->getAttribute('total'));
    }
    
    public function produk()
	{
		return $this->belongsTo(Produk::class);
    }   

    public function sebab()
    {
        return $this->hasMany(SebabPromosi::class);        
    }
}
