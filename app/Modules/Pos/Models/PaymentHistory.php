<?php

namespace App\Modules\Pos\Models;

use App\Modules\Akuntansi\Models\PenerimaanPenjualan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentHistory extends Model
{
     use SoftDeletes;

    protected $table = 'payment_history';

    protected $fillable = [
    	'penerimaan_penjualan_id',
    	'status',
    	'total',
        'created_by',
    ];

    public function penerimaanPenjualan()
    {
    	return $this->belongsTo(PenerimaanPenjualan::class, 'penerimaan_penjualan_id');
    }

    public function transactionNonCash()
    {
    	return $this->hasOne(TransactionNonCash::class);
    }

    public function getStatusPaymentAttribute()
    {
        $html = '';

        if ($this->getAttribute('status') === 0) {
            $html = 'Cash';
        }else if ($this->getAttribute('status') === 1) {
            $html = 'EDC';
        }

        return $html;
    }

}
