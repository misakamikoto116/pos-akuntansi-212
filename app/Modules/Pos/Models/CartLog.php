<?php

namespace App\Modules\Pos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\Akuntansi\Models\User;
use App\Modules\Akuntansi\Models\Produk;
use Helpers\LayoutHelper;

class CartLog extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'produk_id',
        'qty',
        'harga',
        'status',
        'no_transaksi',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;    

    public function produk()
	{
		return $this->belongsTo(Produk::class);
    }

    public function user()
	{
		return $this->belongsTo(User::class);
    }

    public function getHargaFormattedAttribute()
    {
        return "Rp. ".number_format($this->getAttribute('harga'));
    }

    public function getStatusFormattedAttribute()
    {
        $layout = new LayoutHelper;
        if($this->getAttribute('status') == 'insert'){
            return $layout->formLabel('success','INSERT ITEM');
        }elseif($this->getAttribute('status') == 'insert-diskon'){
            return $layout->formLabel('info','INSERT DISKON');
        }elseif($this->getAttribute('status') == 'deleted'){
            return $layout->formLabel('danger','DELETE ITEM');
        }else{
            return $layout->formLabel('warning','DELETE DISKON');            
        }
    }

    public function scopeFilter($query, $data)
    {
        return $query->where('no_transaksi','like','%'.$data['search'].'%');
    }
}
