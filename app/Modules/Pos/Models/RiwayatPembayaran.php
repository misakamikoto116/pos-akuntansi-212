<?php

namespace App\Modules\Pos\Models;

use App\Modules\Akuntansi\Models\FakturPenjualan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiwayatPembayaran extends Model
{

    protected $table = 'riwayat_pembayaran';
    public $timestamps = true;
    use SoftDeletes;
    protected $fillable = [
    	'kembalian',
    	'dibayar',
        'diskon_persen',
        'diskon_rupiah',
    	'faktur_penjualan_id',
    ];

    // Relation
    public function fakturPenjualan()
    {
    	return $this->belongsTo(FakturPenjualan::class,'faktur_penjualan_id');
    }
    // 
}
