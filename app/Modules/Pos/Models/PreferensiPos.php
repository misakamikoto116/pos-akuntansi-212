<?php

namespace App\Modules\Pos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\KodePajak;

class PreferensiPos extends Model
{
    protected $table = 'preferensi_pos';
    use softDeletes;
    
    protected $fillable = [
        'akun_cash_id',
        'akun_hutang_id',
        'kode_pajak_id',
        'diskon_penjualan_id',
        'akun_kas_id',
        'akun_bank_id',
        'status',
        'uang_modal',
        'tanggal_custom',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function akunCash()
    {
        return $this->belongsTo(Akun::class, 'akun_cash_id');
    }

    public function akunHutang()
    {
        return $this->belongsTo(Akun::class, 'akun_hutang_id');
    }

    public function akunBank()
    {
        return $this->belongsTo(Akun::class, 'akun_bank_id');
    }

    public function kodePajak()
    {
        return $this->belongsTo(KodePajak::class, 'kode_pajak_id');
    }

    public function diskonPenjualan()
    {
        return $this->belongsTo(Akun::class, 'diskon_penjualan_id');
    }
}
