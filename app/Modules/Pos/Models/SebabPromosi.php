<?php

namespace App\Modules\Pos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\Akuntansi\Models\Produk;
use Helpers\LayoutHelper;
class SebabPromosi extends Model
{
    use SoftDeletes;
    protected $table = 'sebab_promosi';

    /**
     * @NB
     * Kenapa ada type sebab ? = agar mudah di query
     *
     * Cara kerja :
     * Jika di sebab sudah terpenuhi kondisinya
     * Maka di akibat men diskon harga sesuai akibatnya
     *
     * misal :
     * beli 2 gratis 1
     * maka jika sudah beli 2 maka = tambahkan item dengan yg ada di akibat yaitu 1
     * dan dipotong harganya brp % ? yaitu 100%
     * result : beli 2 gratis 1
     *
     * gimana dengan kelipatan ?
     * untuk mencari kondisi sebab maka dibutuhkan pembagian qty_beli / qty_sebab
     *
     * ==========================================================================
     *
     * update struktur table
     * jadi sekarang sebab dan akibat adalah many to many, why? karena
     * multi sebab bisa hanya punya 1 akibat, begitu pula sebaliknya
     *
     */


    protected $fillable = [
        'promosi_id',
        'akibat_promosi_id',
        'produk_id',
        'qty',
        'total',
        'type_sebab',
        'kelipatan',
        'tipe_barang'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    protected static function boot() {
        parent::boot();

        static::deleting(function($sebab) {
            $sebab->akibat()->delete();
        });
    }

    public function akibat()
    {
        return $this->hasMany(AkibatPromosi::class);        
    }

    public function reverseAkibat()
    {
        return $this->belongsTo(AkibatPromosi::class,'');        
    }
    
    public function produk()
	{
		return $this->belongsTo(Produk::class);
    }
    
    public function getTotalFormattedAttribute()
    {
        return "Rp. ".number_format($this->getAttribute('total'));
    }

    public function getTipeFormattedAttribute()
    {
        $layout = new LayoutHelper;
        if($this->getAttribute('type_sebab') == 0){
            return $layout->formLabel('primary','Diskon Produk');
        }else{
            return $layout->formLabel('info', 'Diskon Total Belanja');
        }
    }

    public function getTipeBarangFormattedAttribute()
    {
        $layout = new LayoutHelper;
        if($this->getAttribute('tipe_barang') === 0){
            return $layout->formLabel('warning','Non Food');
        }elseif($this->getAttribute('tipe_barang') === 1){
            return $layout->formLabel('success', 'Food');
        }else{
            return $layout->formLabel('default', 'No Categories');
        }
    }

    public function getKelipatanFormattedAttribute()
    {
        $layout = new LayoutHelper;
        if($this->getAttribute('kelipatan') == 0){
            return $layout->formLabel('danger','<i class="fa fa-close"></i>');
        }else{
            return $layout->formLabel('success', '<i class="fa fa-check"></i>');
        }
    }

    
}
