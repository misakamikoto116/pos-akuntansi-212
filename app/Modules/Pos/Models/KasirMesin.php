<?php

namespace App\Modules\Pos\Models;

use App\Modules\Akuntansi\Models\FakturPenjualan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KasirMesin extends Model
{
    protected $table = 'kasir_mesin';
    use softDeletes;
    
    protected $fillable = [
        'id',
        'nama',
        'print_tunnel_address',
        'status',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function fakturPenjualan()
    {
        return $this->hasMany(FakturPenjualan::class);
    }

    public function kasirCek()
    {
        return $this->hasMany(KasirCek::class);
    }
}
