<?php

namespace App\Modules\Pos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\Akuntansi\Models\InvoicePenerimaanPenjualan;

class TransactionNonCash extends Model
{
    use SoftDeletes;

    protected $table = 'transaction_non_cash';

    protected $fillable = [
        'penerimaan_penjualan_id',
        'payment_history_id',
        'no_transaksi_bank',
        'no_kartu',
        'total',
        'batch',
        'created_by',
    ];


    public function penerimaanPenjualan()
    {
        return $this->belongsTo(InvoicePenerimaanPenjualan::class, 'invoice_penerimaan_penjualan_id');
    }
}
