@extends('chichi_theme.layout.app')

@section('custom_css')

<style type="text/css">
    big{
        font-weight: bold;
    }
    .nav .nav-pills{
        border: 1px solid rgba(97,135,136,0.3);
    }
    .tab-content{
        box-shadow: none;
    }
    .tab-content>.active{
        padding: 10px;
        padding-top: 30px;
        margin-bottom: 20px;
    }
    .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
        position: relative;
        top: 1px;
        border-top: 1px solid rgba(97,135,136,0.3);
        border-left: 1px solid rgba(97,135,136,0.3);
        border-right: 1px solid rgba(97,135,136,0.3);
        background: white;
        border-radius: 0px;
        color: #27393d;
    }
    .nav-pills .nav-link{
        position: relative;
        top: 1px;
        padding: 1px 30px;
    }
    .nav-pills li a{
        font-weight: bold;
        color: #618788;
        letter-spacing: 0.5px;
    }

    .form-check-input{
        position: relative;
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 1rem;
    }
    .table{
        font-size: 12px;
    }
    .table th{
        padding: 6px 3px;
        text-align: center;
        font-size: 12px;
    }
    .table td{
        padding: 8px 3px;
    }
    .tag{
        background-color: #618788; 
        color: white; 
        text-align: center; 
        position: relative; 
        top: 4px; 
        padding: 3px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
</style>

@endsection

@section('content')
<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Penjualan</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb" style="border: none;">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="">Retur Penjualan</a></li>
                    </ol>

                </div>
            </div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Sales Return / Retur Penjualan</h5>
            </div>
                <div class="card-body">
                <div class="p-20">
                {!! Form::open(['route' => ['pos.submit.retur'], 'method' => 'POST', 'class' => 'form form-horizontal form-label-left', 'id' => 'form-penjualan-penawaran']) !!}
                 @include('pos::retur.form')
                </div>
                <div class="submit">
                <div class="btn-group">
                    {!! Form::submit('Simpan', ['class' => 'btn btn-info btn-block']) !!}                
                </div>
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger']) !!}
                
                </div>
                </div>
                {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

@section('custom_js')

        <!--Form Wizard-->
        <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/starting.js') }}"></script>
        <script src="{{ asset('assets/js/single-duplicate.js') }}"></script>

        <script type="text/javascript">
            var pelanggan_id_old = "{{ old('pelanggan_id') }}";
            var q = 0;

            $(function() {
              enable_cb();
              filterSelectedId({{ $item->id }});
              let form_old = "{{ old('pelanggan_id') }}";
              if(form_old !== ""){
                setDataPelanggan();
                calculate();
              }
              $("#group1").click(enable_cb);
            });
            function enable_cb() {
              if ($("input#group1").is(':checked')) {
                $("input.group2").removeAttr("disabled");
              } else {
                $("input.group2").attr("disabled", true).removeAttr("checked");
              }
            }

            $("#pelanggan_id").change(function() {
                setDataPelanggan();
            });

            function filterSelectedId(id){
                var target = 'faktur';
                    $.ajax({
                        type: "GET",
                        url: "{{ route('pos.get-detail-barang') }}",
                        data: {id : id, type : target,
                        _token: '{{ csrf_token() }}'},
                        dataType: "json",
                        success: function(response){
                            q = 0;
                            $('.purchaseFaktur').find('tr').remove().end();
                            $.each(response.barang, function(index, item){
                                duplicateForm();
                                let countTemp = q - 1;
                                $('#produk_id'+countTemp).append( // Append an object to the inside of the select box
                                    $("<option></option>") // Yes you can do this.
                                        .text(item.produk.no_barang + " | " + item.produk.keterangan)
                                        .val(item.produk_id)
                                );
                                $('#produk_id'+countTemp).val(item.produk_id); 
                                insertItemToForm(countTemp, item.id, target); //-1 karena diatas (diplicateForm) sdh di increment
                            });
                            kurangkanDenganYangSudahDiretur($("#list-sales-invoice option:selected").val());

                        }, failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
            }

            function insertItemToForm(vale, item, target) {
                var url = "{{ route('pos.get-produk-by-val') }}";
                var pelanggan_id = $("#pelanggan_id").val();                                    
                   $.ajax({
                       type: "GET",
                       url: url,
                       data: {id : item, type : target, pelanggan_id : pelanggan_id,
                           _token: '{{ csrf_token() }}'},
                       dataType: "json",
                       success: function(response){
                       $('#tax_produk'+vale).find('option').remove().end();                        
                       $('#gudang_id'+vale).find('option').remove().end();                        
                       $('#barang_faktur_penjualan_id'+vale).val(response.id);
                       $('#keterangan_produk'+vale).val(response.keterangan);
                       $('#qty_produk'+vale).val(response.jumlah);
                       $('#satuan_produk'+vale).val(response.satuan);
                       changeToMasking(response,vale);
                       $('#unit_harga_produk'+vale).autoNumeric('init').autoNumeric('set',response.unitPrice);
                       $('#amount_produk'+vale).autoNumeric('init').autoNumeric('set',0); 
                       $('#diskon_produk'+vale).val(response.diskon);
                       $('#terproses'+vale).val(response.terproses);
                       if (response.harga_modal) {
                            $('#harga_modal'+vale).val(response.harga_modal.harga_modal);
                            $('#harga_terakhir'+vale).val(response.harga_modal.harga_terakhir);
                       }
                       if(response.ditutup == 1){
                           $('#ditutup'+vale).prop('checked', true);
                       }
                       
                       $.each(response.tax, function (index, item){
                            if(target == 'faktur'){
                                if(response.kode_pajak_id == item.id || response.kode_pajak_2_id == item.id){
                                    $('#tax_produk'+vale).append( 
                                    $("<option></option>")
                                    .text(item.nilai+ " / " +item.nama)
                                    .val(item.id)
                                    .attr('selected','selected')
                                    );
                                }else{
                                    $('#tax_produk'+vale).append( 
                                    $("<option></option>") 
                                    .text(item.nilai+ " / " +item.nama)
                                    .val(item.id)
                                    );
                                }
                            }else{
                                $('#tax_produk'+vale).append( 
                                    $("<option></option>") 
                                   .text(item.nilai+ " / " +item.nama)
                                   .val(item.id)
                                );
                            }   
                            
                        });
                       $(response.multiGudang).each(function (val, text) {
                           $(text.gudang).each(function (index, item) {
                               $('#gudang_id'+vale).append(`<option value="${item.id}">${item.nama}</option>`);
                           });
                       });
                       if (response.taxable.taxable == 1) {
                            $("#group1").prop('checked', true);
                            $("#group2").removeAttr('disabled');
                        }else if(response.taxable.taxable == 0 || response.taxable.taxable === null) {
                            $("#group1").prop('checked', false);
                            $("#group2").attr('disabled','disabled');
                        }
                        if (response.taxable.in_tax == 1) {
                            $("#group2").prop('checked', true);
                        }else if(response.taxable.in_tax == 0 || response.taxable.in_tax  === null){
                            $("#group2").prop('checked', false);
                        }
                        sumQtyAndUnit(vale);

                       $('.select2').select2();
                       }, failure: function(errMsg) {
                           alert(errMsg);
                       }
                   });
           }

        function kurangkanDenganYangSudahDiretur(id){
            var pelanggan_id = $("#pelanggan_id").val();         
            $.ajax({
                type: "GET",
                url: "{{ route('pos.get-returned-item') }}",
                data: {id : id, pelanggan_id : pelanggan_id,
                    _token: '{{ csrf_token() }}'},
                dataType: "json",
                success: function(response){
                    if (response.data) {
                        $.each(response.data, function(key, val){
                            if(val.produk_id == $('#produk_id'+key).val()){
                                let current_qty = $('#qty_produk'+key).val();
                                let result_qty = current_qty - val.qty;
                                $('#qty_produk'+key).val(result_qty);
                                sumQtyAndUnit(key);
                            }
                        });
                    }  
                }
            });
        }

        function changeToMasking(response, vale) {
            if(pelanggan_id_old !== ""){
                changeMaskingToNumber($('#unit_harga_produk'+vale).val(response.unitPrice));
                changeMaskingToNumber($('#amount_produk'+vale).val(0));
            }
        }
        </script>
        <script src="{{ asset('js/retur_penjualan.js') }}"></script>
@endsection
