  
  {!! Form::hidden('currentUrl', $returnUrl) !!}
  {!! Form::hidden('total', null, ['class' => 'kolom_grandtotal']) !!}  
  {!! Form::hidden('faktur_penjualan_id', $item->id, ['class' => 'faktur_penjualan_id']) !!}
  
  {{-- {!! Form::hidden('sub_total',null, ['class' => 'kolom_subtotal']) !!}   --}}

  <div class="row">
    <div class="col-sm-8">
      <div class="form-group row">
      <div class="col-2" style="padding: 0px">
        @if (isset($item))
        {!! Form::checkbox('taxable', 1, $item->taxable == 1 ? true : false,['class' => 'form-control taxable','id' => 'group1','onchange' => 'calculate()']) !!}
        @else
        {!! Form::checkbox('taxable', 1, true,['class' => 'form-control taxable','id' => 'group1','onchange' => 'calculate()']) !!}
        @endif
      </div>
      <div class="col-10 col-form-label" style="padding: 4px 0px;">Cust. is Taxable</div>
    </div>
    <div class="form-group row">
      <div class="col-2" style="padding: 0px">
        @if (isset($item))
        {!! Form::checkbox('intax', 1, $item->intax == 1 ? true : false,['class' => 'form-control group2 in_tax', 'id' => 'group2', 'onchange' => 'calculate()']) !!}
        @else
        {!! Form::checkbox('intax', 1, null,['class' => 'form-control group2 in_tax', 'id' => 'group2', 'onchange' => 'calculate()']) !!}
        @endif
      </div>
      <div class="col-10 col-form-label" style="padding: 4px 0px;">Inclusive Tax</div>
    </div>
    </div>
    <div class="col-sm-3">
      <div class="row">
        <div class="col-6">
          <div class="form-group">
          <div class="tag">SR No.</div>
          {!! Form::text('sr_no', $item->no_faktur,['class' => 'form-control','id' => ''])!!}
          </div>
        </div>
        <div class="col-6">
          <div class="form-group">
          <div class="tag">Date</div>
          @if (isset($item) || old('tanggal') !== null)
          {!! Form::text('tanggal',null,['class' => 'form-control tanggal1','id' => '','required'])!!}
          @else
          {!! Form::text('tanggal',null,['class' => 'form-control tanggal1','id' => '','required'])!!}
          @endif
          </div>
        </div>
        <div class="col-6 tax">
          <div class="form-group">
          <div class="tag">No. Fiscal</div>
          {!! Form::text('no_fiscal', $item->no_faktur,['class' => 'form-control','id' => '','required'])!!}
          </div>
        </div>
        <div class="col-6 tax" >
          <div class="form-group">
          <div class="tag">Tgl Fiscal</div>
          @if (isset($item)|| old('tgl_fiscal') !== null)
          {!! Form::text('tgl_fiscal',null,['class' => 'form-control tanggal1','id' => '','required'])!!}
          @else
          {!! Form::text('tgl_fiscal',null,['class' => 'form-control tanggal1','id' => '','required'])!!}
          @endif
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="form-group">
    <label>Description :</label>
    {!! Form::textarea('keterangan',null,['class' => 'form-control','id' => '',''])!!}
  </div>


<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3); margin-top: 20px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barang-tab" data-toggle="pill" href="#pills-barang" role="tab" aria-controls="pills-barang" aria-selected="true">Barang</a>
  </li>
</ul>

<div class="faktur-sections" data-id="1">
  <div class="duplicate-faktur-sections">
  <div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="pills-barang" role="tabpanel" aria-labelledby="pills-barang-tab" style="width: 100%; overflow: auto;">
      <div style="width: 2000px;">
        <table width="100%" class="table duplicate-sections">
          <thead class="thead-light" style="border-collapse: collapse;">
            <tr>
              <th width="12%">Item</th>
              <th width="12%">Item Description</th>
              <th>Qty</th>
              <th>Item Unit</th>
              <th>Unit Price</th>
              <th>Disc %</th>
              <th>Tax</th>
              <th>Amount</th>
              <th width="10%">Gudang</th>
              <th>SN</th>
              <th></th>
            </tr>
          </thead>
          <tbody class="purchaseFaktur">

            @if(old('produk_id') !== null)  
                  @foreach(old('produk_id') as $row => $produk_id )
                    @include('akuntansi::retur_penjualan/components/item_retur_penjualan',[
                      'row'                                 => $row, 
                      'produk_id'                           => $produk_id,
                      'barang_faktur_penjualan_id'          => old('barang_faktur_penjualan_id')[$row],
                      'keterangan_produk'                   => old('keterangan_produk')[$row],
                      'qty_produk'                          => old('qty_produk')[$row],
                      'satuan_produk'                       => old('satuan_produk')[$row],
                      'unit_harga_produk'                   => old('unit_harga_produk')[$row],
                      'diskon_produk'                       => old('diskon_produk')[$row],
                      'tax_produk'                          => old('tax_produk')[$row],
                      'amount_produk'                       => old('amount_produk')[$row],
                      'gudang_id'                           => old('gudang_id')[$row],
                      'sn'                                  => old('sn')[$row],
                      'harga_modal'                         => old('harga_modal')[$row],
                      'harga_terakhir'                      => old('harga_terakhir')[$row],
                      'harga_dgn_pajak'                     => old('harga_dgn_pajak')[$row],
                    ])
                  @endforeach      
              @endif

          </tbody>
        </table>
      </div>
    </div>
  
  </div><!-- END PILLS -->
  </div>
  </div>

<hr>
<div class="row">
  <div class="offset-sm-8 col-sm-4">
		<div class="form-group row">
		<div class="offset-2 col-3 col-form-label text-right"><strong>Sub Total</strong></div>
		<div class="col-7 text-right col-form-label" id="subtotal"><strong>0</strong></div>
    </div>
    
		<div class="form-group row">
			<div class="col-2 col-form-label">Discount:</div>
			<div class="col-3 col-form-label">{!! Form::text('diskon',isset($item) ? $item->diskon : 0,['class' => 'form-control','id' => 'diskonTotal', 'onchange' => 'calculate()','required'])!!}</div>
			<div class="col-2 col-form-label text-center">% =</div>
      <div class="col-5 col-form-label">{!! Form::text('jumlah_diskon_retur', old('jumlah_diskon_retur') ? old('jumlah_diskon_retur') : 0,['class' => 'form-control mask','id' => 'totalPotonganRupiah','onchange' => 'calculate()','required'])!!}</div>
    </div>
    <div class="form-group row">
      <div class="offset-1 col-4 col-form-label text-right"></div>
      <div class="col-7 text-right col-form-label" id="taxNaration"></div>
      {!! Form::hidden('tax_cetak0', null,['id' => 'tax_cetak0'])!!}
      {!! Form::hidden('tax_cetak1', null,['id' => 'tax_cetak1'])!!}
      {!! Form::hidden('tax_cetak_label0', null,['id' => 'tax_cetak_label0'])!!}
      {!! Form::hidden('tax_cetak_label1', null,['id' => 'tax_cetak_label1'])!!}
    </div>
		<div class="form-group row">
		<div class="offset-1 col-4 col-form-label text-right"><strong>Total Invoice</strong></div>
    <div class="col-7 text-right col-form-label grandtotal"><strong>0</strong></div>
		</div>
	</div>
</div>
<script type="text/template" id="table_faktur_section" data-id=""/>
  <tr>
    <td>
        {!! Form::hidden('barang_faktur_penjualan_id[]', null, ['class' => 'barang_faktur_penjualan_id']) !!}
        {!! Form::select('produk_id[]', [], null,['placeholder' => '-Pilih-','class' => 'form-control select2 produk_id','required'])!!}
    </td>
    <td>{!! Form::text('keterangan_produk[]',null,['class' => 'form-control keterangan_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('qty_produk[]',null,['class' => 'form-control qty_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('satuan_produk[]',null,['class' => 'form-control satuan_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('unit_harga_produk[]',null,['class' => 'form-control unit_harga_produk','id' => '','required'])!!}</td>
    <td>{!! Form::text('diskon_produk[]',null,['class' => 'form-control diskon_produk','id' => '','required'])!!}</td>
    <td width="10%">
			{!! Form::select('tax_produk[][]',[],null,['class' => 'form-control select2 select2-multiple tax_produk','multiple' => 'multiple','multiple','id' => '',''])!!}
    </td>
    <td>{!! Form::text('amount_produk[]',null,['class' => 'form-control amount_produk','id' => '',''])!!}</td>
    <td>{!! Form::select('gudang_id[]',isset($item) ? $gudang : [],null,['class' => 'form-control select2 gudang_id','id' => 'gudang_id',''])!!}</td>
    <td>
        {!! Form::text('sn[]',null,['class' => 'form-control sn','id' => '',''])!!}
        {!! Form::hidden('harga_modal[]',null,['class' => 'form-control harga_modal','id' => '',''])!!}
        {!! Form::hidden('harga_terakhir[]',null,['class' => 'form-control harga_terakhir','id' => '',''])!!}
        {!! Form::hidden('harga_dgn_pajak[]',null,['class' => 'form-control harga_dgn_pajak','id' => '',''])!!}
    </td>
    <td><button href="" class="btn btn-danger remove-rincian-penawaran" type="button" style="line-height: 0.6;"><i class="fa fa-times"></i></button></td>
  </tr>
</script>