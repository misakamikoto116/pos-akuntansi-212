@extends('chichi_theme.layout.app')

@section('custom_css')
    <style>
        .btn-xs {
            padding: 1px 5px;
            font-size: 10px;
            line-height: 1.5;
            border-radius: 3px;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">Retur</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Retur Penjualan</a></li>
            </ol>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">Retur Penjualan</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3" style="margin-left: auto; margin-right: auto">
                            {!! Form::open(['route' => 'pos.retur.check.transasction', 'method' => 'post']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="kasir_mesin">Scan nomor transaksi <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        {!! Form::text('no_transaksi', null, ['class' => 'form-control','autofocus' => 'autofocus']) !!}
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-warning waves-effect waves-light w-lg btn-check-in" type="submit">
                                        <span>Submit</span>
                                    </button> 
                                </div>   
                            {!! Form::close() !!}     
                        </div>
                    </div>
                       

                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom_js')
    <script>
        jQuery(document).ready(function (e) {

             $('.modal_input').autoNumeric('init', {
                aPad: false
            });

            $('body').on('click', '.btn-check-in', function (e) {
                e.preventDefault();
                
                var el = $(this);

                var form = $(el).closest('form');

                var checkVal = $(form)[0].checkValidity();

                if(checkVal) {
                   $(form).submit();
                } else {
                    $(form)[0].reportValidity();
                }

            });

        });
    </script>
@endsection