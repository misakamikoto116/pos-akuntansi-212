@extends('pos::laporan.laporan')
@section('title')
    {{ $title }}
@endsection
@section('laporan')
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan=8>
                            <p><h4> {{ $perusahaan }} </h4></p>
                            <p><h6> {{ $title }} </h6></p>
                            <p> Dari <b> {{ $dari_tanggal }} </b> Ke <b> {{ $ke_tanggal }} </b></p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>Tipe</th>
                        <th>No. Faktur</th>
                        <th>Keterangan</th>
                        <th>Kts. Masuk</th>
                        <th>Kts. Keluar</th>
                        <th>Kuantitas</th>
                        <th>Gudang</th>
                    </tr>
                </thead>
                @if($item->isEmpty())
                    <tr>
                        <td colspan=8 align="center">
                            <div class="alert alert-warning">
                                Tidak ada data {{ $title }} yang tersedia <br>
                                pada tanggal {{ $dari_tanggal }} hingga {{ $ke_tanggal }}.
                            </div>
                        </td>
                    </tr>
                @else
                    @foreach($item as $item_key => $dataKartuStokPersediaan)
                        <tr>
                            <td>{{ $item_key                                  ??   null }}</td>
                            <td>{{ $dataKartuStokPersediaan[0]['nm_barang']   ??   null }}</td>
                            <td colspan=4>&nbsp;</td>
                            <td>{{ $dataKartuStokPersediaan[0]['kts_saldo']   ??   null }}</td>
                        </tr>
                        @php $qtyMasuk = 0 ; $qtyKeluar = 0; @endphp
                        @foreach($dataKartuStokPersediaan as $dataKartuStok)                            
                            @php
                                $qtyMasuk  += $dataKartuStok['kts_masuk'];
                                $qtyKeluar += $dataKartuStok['kts_keluar'];
                                $qty        = $dataKartuStok['kts_saldo']   + ($qtyMasuk + $qtyKeluar);
                            @endphp
                            <tr>
                                <td>{{ $dataKartuStok['tanggal']              ?? null }}</td>
                                <td>{{ $dataKartuStok['tipe']                 ?? null }}</td>
                                <td>{{ $dataKartuStok['no_faktur']            ?? null }}</td>
                                <td>{{ $dataKartuStok['keterangan']           ?? null }}</td>
                                <td>{{ $dataKartuStok['kts_masuk']            ?? null }}</td>
                                <td>{{ abs($dataKartuStok['kts_keluar'])      ?? null }}</td>
                                <td>{{ $qty                                   ?? null }}</td>
                                <td>{{ $dataKartuStok['gudang']               ?? null }}</td>
                            </tr>                            
                        @endforeach
                        <tr>
                            <td colspan=4>&nbsp;</td>
                            <td class="border-top">{{ number_format($qtyMasuk)  }}</td>
                            <td class="border-top">{{ number_format(abs($qtyKeluar)) }}</td>
                            <td colspan=2>&nbsp;</td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
    </div>
@endsection
