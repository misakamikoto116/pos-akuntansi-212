<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h1 class="title" align="center">Detail Laporan Persediaan</h1>
        <ul class="list-group">
            <li class="list-group-item"><a data-toggle="collapse" data-parent="#accordion" href="#rinpersediaan"><i class="fa fa-tags"></i> Rincian Persediaan</a></li>
            <div id="rinpersediaan" class="collapse">
                <ul class="list-group">           
                    <li class="list-group-item"><a href="{{ url('pos/laporan/persediaan/kartu-stok-persediaan') }}" data-toggle="modal" data-target="#kartu_stok_persediaan">Kartu Stok Persediaan</a></li>
                </ul>
            </div>
        </ul>
    </div>
</section>

{{-- Modal Kartu Stok Persediaan --}}
<div id="kartu_stok_persediaan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Kartu Stok Persediaan</h4>
      </div>
        {!! Form::open(['url' => url('pos/laporan/persediaan/kartu-stok-persediaan'), 'method' => 'get', 'target' => '_blank']) !!}
        <div class="modal-body">
            <div class="form-group row">
                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Lanjutkan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
