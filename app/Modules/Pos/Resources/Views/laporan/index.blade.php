@extends('chichi_theme.layout.app')

@section('custom_css')
    <style>
        #ul-group > li > a > i{
            font-size: 15px;
            color: black;
        }
        .list-group-item > a{
            color: black;
        }
    </style>
@endsection

@section('content')
<div class="form-group row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="title">Kategori Laporan</h4>
            </div>
            <div class="card-body">
                <br>
                <div class="form-group row">
                    <div class="col-md-6">
                        <h1 class="title" align="center">Kategori Laporan</h1>
                        <ul id="ul-group" class="nav nav-pills list-group" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3);">
                            <li class="nav-item list-group-item">
                                <a onclick="scrolltop()" id="tab-laporan-persediaan" data-toggle="pill" href="#laporan_persediaan" role="tab" aria-controls="laporan_persediaan" aria-selected="true"><i class="fa fa-folder"> Laporan Persediaan</i></a>
                            </li>                            
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade" id="laporan_persediaan" role="tabpanel" aria-labelledby="tab-laporan-persediaan">
                                @include('pos::laporan.include_laporan.laporan_persediaan')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
    <script>
        function scrolltop() {
            $("html, body").animate({ scrollTop: 125 }, 1000);
        }
    </script>
@endsection