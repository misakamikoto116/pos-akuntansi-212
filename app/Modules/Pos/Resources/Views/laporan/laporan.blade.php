<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title', chichi_title ())</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <meta name="csrf-token" content="{{ csrf_token() }}">        
        <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/papper.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/laporan.css')}}" rel="stylesheet" type="text/css" />
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    </head>
    <body class="container">
        <header></header>
        <img class="watermark" src="{{asset('assets/images/chichi_render.png')}}">
        <section class="sheet padding-0mm">
            <article>
                @yield('laporan')
            </article>
        </section>
        <footer>
            <label style="margin: 5px 20px;">
                @yield('title')
                {{date('Y-m-d \ H:i:s')}}
            </label>
        </footer>
    </body>
</html>
<script>
    $(document).ready(function(){
        // window.print();
    });
</script>