@extends('chichi_theme.layout.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">List barang</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">        
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Barang</h5>
                </div>

                <div class="card-body">
                    {{ Form::open(['url' => route('pos.barang.index'), 'method' => 'get']) }}
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Nama Barang</label>
                                    <input type="text" class="form-control" name="search" value="{{ request()->search }}">
                                </div>
                            </div>                                
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Gudang</label><br>
                                    <input type="radio" name="warehouse" value="current" {{ request()->warehouse == 'current' ? 'checked' : '' }}> Current
                                    <input type="radio" name="warehouse" value="other" {{ request()->warehouse == 'other' ? 'checked' : '' }}> Other
                                </div>
                            </div>
                            <div class="col-md-2">                        
                                <div class="form-group">
                                    <label>Options</label><br>        
                                    <a href="{{ url()->current() }}" class="btn btn-fill btn-danger">
                                        <i class="ti-close"></i>
                                    </a>
                                    <button type="submit" class="btn btn-success btn-fill">
                                        <i class="ti-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    {{ Form::close() }}
                    <div class="row">
                        <div class="col-md-2">
                            <button class="btn-select-card btn btn-success btn-rounded waves-effect waves-light m-b-30">
                                <span><i class="fa fa-print"></i></span>
                                Print Selected Price Card
                            </button>
                        </div>
                        <div class="col-md-2">
                            <button class="btn-promo-card btn btn-success btn-rounded waves-effect waves-light m-b-30">
                                <span><i class="fa fa-print"></i></span>
                                Print Selected Promo Card
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">        
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th class="bs-checkbox " style="width: 36px; " data-field="state">
                                    <input id="select-all" name="btSelectAll" type="checkbox">
                                </th>
                                <th>No</th>
                                <th width="20%">Barang</th>
                                <th>Qty</th>
                                <th>Harga Jual</th>
                                <th>Diskon</th>
                            </tr>
                        </thead>

                        <tbody>                        
                        @foreach ($goods as $good)
                            <tr>
                                <td class="bs-checkbox "><input data-item-id="{{$good->id}}" name="btSelectItem" type="checkbox"></td>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $good->keterangan }}</td>
                                <td>{{ $good->getSumQtyProdukAttribute() }}</td>
                                <td>{{ $good->harga_jual }}</td>
                                <td>{{ $good->diskon }} </td>
                            </tr>
                        @endforeach                        
                        </tbody>
                    </table>

                    {{ $goods->appends(request()->all())->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#select-all').click(function(e){
                var table= $(e.target).closest('table');
                $('td input:checkbox',table).prop('checked',this.checked);
            });
            $(".btn-select-card").click(function(){
                var diPilih = [];

                $.each($("input[name='btSelectItem']:checked"), function(){
                    diPilih.push($(this).attr("data-item-id"));
                });
                if (diPilih.length < 1) {
                    alert("Pilih dulu data yg akan di cetak maksimum 25 item")
                }else{
                    // alert("Terpilih: " + diPilih.join(", "));
                    // console.log(diPilih.join(","));
                    window.open("barang/price-card-select?selected="+diPilih.join(", "),'_blank');
                }
            });

            $(".btn-promo-card").click(function(){
                var diPilih = [];

                $.each($("input[name='btSelectItem']:checked"), function(){
                    diPilih.push($(this).attr("data-item-id"));
                });
                if (diPilih.length < 1) {
                    alert("Pilih dulu data yg akan di cetak maksimum 25 item")
                }else{
                    //alert("Terpilih: " + diPilih.join(", "));
                    // console.log(diPilih.join(","));
                    // window.location = "barang/promo-card-select?selected="+diPilih.join(", ")  ;
                    window.open("barang/promo-card-select?selected="+diPilih.join(", "),'_blank');
                }
            });
        });
    </script>
@endsection