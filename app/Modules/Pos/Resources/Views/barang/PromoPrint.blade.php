<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Struk Belanja</title>
    <!-- Normalize or reset CSS with your favorite library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

    <!-- Load paper.css for happy printing -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <link href="https://fonts.googleapis.com/css?family=Share+Tech+Mono" rel="stylesheet">
    <style>
        body {
            font-family: "Verdana";
        }
        /*34cm * 21.5*/
        /*legal {*/
            /*height: 34cm;*/
            /*width: 21.5cm;*/
        /*}*/
        @page { size: legal }
        .kotak_label {
            width: 5.5cm;
            border: #0b0b0b solid 1px;
            float: left;
            margin: 5px;
            height: 4.5cm;
        }
        .text-barang {
            text-align: center;
            height: 1.5cm;
            border-bottom: #0b0b0b solid 1px;
            margin-top: 5px;
        }
        .text-harga {
            text-align: center;
            font-weight: bold;
            font-size: 20pt;
            font-family: "Arial Black";
            color: #8a0816;
            margin-top: 5px;
            /*border-bottom: #0b0b0b solid 1px;*/
        }
        .text-back {
            position: absolute;
            padding: 0 0 0px 12px;
            font-size: 50px;
            filter: blur(3px);
            color: grey
        }

        .text-title {
            text-align: center;
            font-weight: bold;
            font-size: 14pt;
            font-family: "Arial Black";
            color: #8a0816;
            border-bottom: #0b0b0b solid 1px;
        }

        @media print
        {
            .no-print, .no-print *
            {
                display: none !important;
            }
        }
    </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="legal">

<!-- Each sheet element should have the class "sheet" -->
<!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
<section class="sheet padding-10mm">

    <!-- Write HTML just like a web page -->
    @foreach($data as $row)

        <div class="kotak_label">
            @if ($header !== null)
                <img style="border: #0b0b0b solid 1px;" width="208" height="50" src="{{ asset('storage/barang/stiker/'. $header->nama_stiker) }}"><br>
            @else
                <div class="text-title">
                    {{ chichi_title() }}
                </div>    
            @endif
            <div class="text-barang">
                <div class="text-back">
                    PROMO
                </div>
                {{ $row->keterangan }} <br/> {{ $row->no_barang }}
            </div>
            <div class="text-harga"><sup style="font-size: 1px; line-height: 2pt">Rp. </sup>
                {{ 'Rp. '.number_format($row->harga_jual, 2)  }}
            </div>
        </div>
    @endforeach
</section>
</body>
</html>