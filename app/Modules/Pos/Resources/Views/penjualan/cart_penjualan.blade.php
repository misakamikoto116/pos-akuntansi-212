@if ( $cart != null )
    @foreach ( $cart->items as $item )
        <tr id="{{ $item['id'] }}" data-id="{{ $item['id'] }}">
            <td style="width: 500px;">{{ $item['produk']->keterangan ?? '-' }}</td>
            <td style="width: 150px;" class="qty_edit">
                <a class="cart-qty" data-qty="{{ $item['qty'] }}">{{ $item['qty'] }}</a>
            </td>
            <td style="width: 149px;" class="harga_edit">
                {{ number_format($item['price'], 2, '.', ',') }}
            </td>
            {{-- hidden harga jual --}}
            <td style="width: 150px; display: none;" class="qty_edit_hidden">
                {!! Form::text('qty_edited',null,['class' => 'form-control qty_detail']) !!}
            </td>
            <td style="width: 149px; display: none;" class="harga_edit_hidden">
                {!! Form::text('harga_jual_edited',null,['class' => 'form-control harga_detail']) !!}
            </td>
            <td style="width: 154px; display: none;" class="diskon_persen_hidden">
                {!! Form::text('diskon_persen_edited',null,['class' => 'form-control diskon_persen_detail']) !!}
            </td>
            <td style="width: 176px; display: none;" class="diskon_rupiah_hidden">
                {!! Form::text('diskon_rupiah_edited',null,['class' => 'form-control diskon_rupiah_detail']) !!}
            </td>
            <td style="width: 154px;" class="diskon_persen_edit">
                @if( $item['produk']->diskon > $item['diskon'] )
                    {{  number_format($item['produk']->diskon)  }}
                @else
                    {{  number_format($item['diskon'])  }}
                @endif
            </td>
            <td style="width: 176px;" class="diskon_rupiah_edit">
                {{ number_format( isset($item->options) ? $item->options->diskon_rupiah : 0, 2, '.', ',') }}
            </td>
            <td style="width: 140px;">
                <a class="cart-subtotal">{{ number_format( isset($item->options) ? $item->options->price_final : $item['price_final'], 2, '.', ',') }}</a>
            </td>
            <td style="width: 173px;">
                <a href="#" data-id="{{ $item['id'] }}" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs btn-remove-item"> <i class="fa fa-trash"></i> </a>
                <a href="#" data-id="{{ $item['id'] }}" data-toggle="tooltip" title="Edit" class="btn btn-warning btn-xs btn-edit-item"> <i class="fa fa-pencil"></i> </a>
            </td>
        </tr>
    @endforeach
@endif