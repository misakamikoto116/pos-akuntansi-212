@extends('chichi_theme.layout.app')

@section('custom_css')
    <style>
        .tbodyCartList {
            word-wrap: break-word;
            display:block;
            min-height: 275px;
            max-height: 275px;
            overflow:auto;
        }
        .theadCartList, .tbodyCartList tr {
            display:table;
            width:100%;
            table-layout:fixed;
        }
        .theadCartList {
            width: calc( 100% - 1px )
        }
        a.disabled {
          pointer-events: none;
          cursor: default;
        }

        .btn-xs {
             padding: 1px 5px; 
            font-size: 10px;
            line-height: 1.5;
            border-radius: 3px;
        }
        p.alert-exchange {
            font-size: 20px;
            font-weight: bold;
            text-align: center;
        }

        span.twitter-typeahead {
            position: relative;
            display: inline-block;
            width: 100%;
        }
        
       .tt-query {
         -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
                 box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
       }

       .tt-hint {
         color: #999;
       }

       .tt-menu {    /* used to be tt-dropdown-menu in older versions */
         width: 422px;
         margin-top: 4px;
         padding: 4px 0;
         background-color: #fff;
         border: 1px solid #ccc;
         border: 1px solid rgba(0, 0, 0, 0.2);
         -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
                 border-radius: 4px;
         -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
                 box-shadow: 0 5px 10px rgba(0,0,0,.2);
       }

       .tt-suggestion {
         padding: 3px 20px;
         line-height: 24px;
       }

       .tt-suggestion.tt-cursor,.tt-suggestion:hover {
         color: #fff;
         background-color: #0097cf;

       }

       .tt-suggestion p {
         margin: 0;
       }

       /*table#penjualan-table {
          display: block;
          height: 410px;
          overflow-y: scroll;
      }*/

      .fixed_header{
          width: 100%;
          table-layout: fixed;
          border-collapse: collapse;
      }

      .fixed_header tbody{
        display:block;
        width: 100%;
        overflow: auto;
        height: 370px;
      }

      .fixed_header thead tr {
         display: block;
      }

      .fixed_header th, .fixed_header td {
        width: 159px;
      }
      .wrapper {
        padding-top: 50px;
        color: #000;
      }

      .container {
        width: 100%;
      }

    .ui-autocomplete {
        max-height: 200px;
        overflow-y: auto;
        /* prevent horizontal scrollbar */
        overflow-x: hidden;
    }
    /* IE 6 doesn't support max-height
    * we use height instead, but this forces the menu to always be this tall
    */
    * html .ui-autocomplete {
        height: 200px;
    }

    .label-modal {
      text-align: right;
      background: #5fbeaa;
      color: white;
      border: 1px;
      border-radius: 10px;
    }

    .jarak-modal {
      margin-bottom: 10px;
    }

    .btn-md-modal {
      margin-top: 11px;
      margin-left: -29px;
      height: 49px;
      width: 85px;
    }

    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <!-- Judul Halaman -->

            <!-- Judul Halaman -->

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
                <div class="card-body">

                    @include ('chichi_theme.layout.flash')

                    <div class="alert-pos">
                        
                    </div>

                    <div class="row">
                        <div class="col-sm-7" style="border: 2px solid #ebeff2; border-radius: 25px; padding: 20px;">
                            {{-- Modal Antrian --}}
                            <div class="modal fade fade-in" id="save-cart" role="dialog" style="padding: 100px;">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header" style="border:none; background: lightgray">
                                            <h5 class="col-12 modal-title text-center" style="font-size: 20px;">
                                                - Tunda Pesanan -
                                                <button type="button" class="close text-right" data-dismiss="modal" id="close-save-cart"><i class="fa fa-close"></i></button>
                                            </h5>
                                        </div>
                                        <div class="modal-body" style="padding-top: 20px;">
                                            <div class="row form-group">
                                                <div class="col-3 col-form-label">Keterangan</div>
                                                <div class="col-9">
                                                    {!! Form::text('', null, ['class' => 'form-control', 'placeholder' => 'Keterangan', 'id' => 'keteranganTunda', 'id' => 'cartdesc']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-3 col-form-label">Total</div>
                                                <div class="col-9">
                                                    <label id="final_price" class="form-control total-number" data-total="{{ $getCartTotal }}" readonly>{{ $getCartTotal }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-default" type="button" id="savetochart">Simpan</button>
                                            <button type="button" class="btn btn-danger batal-antrian" data-dismiss="modal">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="daftar-penjualan table-responsive">
                                @if ($getTanggalCustom === 1)
                                  <div class="form-group row" style="margin-bottom: 8px">
                                      <label for="" class="col-2 col-form-label">Tanggal Transaksi</label>
                                      <div class="col-3">
                                          {!! Form::date('tanggal_custom', null,['class' => 'form-control tanggal-custom']) !!}
                                      </div>
                                  </div>
                                @endif
                                <table class="table table-bordered fixed_header" id="penjualan-table">
                                    <thead>
                                        <tr>
                                            <th style="width: 500px;">Barang</th>
                                            <th style="width: 150px;">Qty</th>
                                            <th style="width: 170px;">Hrg</th>
                                            <th style="width: 140px;">Disk %</th>
                                            <th style="width: 180px;">Disk Rp</th>
                                            <th style="width: 161px;">S. Total</th>
                                            <th style="width: 183px;">Opt</th>
                                        </tr>
                                    </thead>

                                    <tbody class="box-penjualan" id="list-item">
                                        @include('pos::penjualan.cart_penjualan')
                                    </tbody>

                                </table>

                                <div class="form-group text-left" style="position: absolute; bottom: 39px; left: 22px;">
                                    <button class="btn btn-warning btn-reset-penjualan">Reset Penjualan</button>
                                </div>

                                <div class="form-group text-right" style="position: absolute; bottom: 24px; right: 150px;">
                                    <label for="" style="font-size: 25px;">Total : </label>
                                    <label id="final_price" class="total-number" data-total="{{ $getCartTotal }}" style="font-size: 25px;">{{ $getCartTotal }}</label>
                                </div>

                            </div>

                        </div>

                        <div class="col-sm-5" style="border: 2px solid #ebeff2; border-radius: 25px; padding: 20px;">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group pull-right">
                                        @if ( isset($cart->items) )
                                            @if( $cart->items->isEmpty() )
                                                <a href="#" data-toggle="modal" class="btn btn-github disabled tundaBtn">Tunda</a>
                                            @else
                                                <a href="#" data-toggle="modal" class="btn btn-github tundaBtn">Tunda</a>
                                            @endif
                                        @else
                                            <a href="#" data-toggle="modal" class="btn btn-github disabled tundaBtn">Tunda</a>
                                        @endif
                                        <a href="#" class="btn btn-github" data-target="#modal-cart" data-toggle="modal">Panggil</a>
                                        @if ($getTanggalCustom === 1)
                                          <a href="#" data-toggle="modal" data-target="#ReturModal" class="btn btn-danger" onclick="returFocus()">Retur</a>
                                        @endif
                                        {{-- <a href="#" class="btn btb-md btn-primary btn-edc" data-target="#modal-edc" data-toggle="modal">EDC</a> --}}
                                        <a href="#" class="btn btn-md btn-success btn-cek-barang" data-target="#modal-cek-barang" data-toggle="modal">F7 | Cari Barang</a>
                                        <div class="modal fade fade-in" id="modal-cart" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="border:none; background: lightgray">
                                                        <h5 class="col-12 modal-title text-center" style="font-size: 20px;">
                                                            - Panggil Pesanan -
                                                            <button type="button" class="close text-right" data-dismiss="modal" id="close-save-cart"><i class="fa fa-close"></i></button>
                                                        </h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row" id="cart-list">
                                                            @include('pos::penjualan.components.cart_list', ['carts' => $carts])
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{ Form::open(['']) }}

                                <hr>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>No. Penjualan
                                                <i class="fa fa-tags"></i>
                                            </label>
                                            {{ Form::text('no_penjualans',$no_penjualan,['class' => 'form-control no_penjualan','readonly']) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Cari Pelanggan
                                                <i class="fa fa-users"></i>
                                            </label>
                                            {{ Form::select('pelanggan_id', $pelangganPOS, $getPelangganUmum, ['class' => 'form-control pelanggan_select select2', 'placeholer' => 'Pelanggan', 'required']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="row div_label_not_found" style="display: none; position: absolute;" >
                                  <div class="col-sm-12">
                                    <div class="form-group">
                                      <span class="label label-danger" style="padding-right: 153px;">Data tidak ditemukan </span>
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="row" style="margin-top: 30px">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Cari Barang
                                                <i class="fa fa-barcode"></i>
                                            </label>
                                            <em style="font-size : 10px; float: right; color: red;">* Tekan ESC</em>
                                            {!! Form::text('barcode_barang', null,['class' => 'form-control barcode_barang','placeholder' => 'Scan Barcode / Cari Barcode']) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group div-qty">
                                            <label>Qty
                                            </label>
                                            <em style="font-size : 10px; float: right; color: red;">* Tekan F2</em>
                                            {{ Form::number('qty', null, ['class' => 'form-control qty_input', 'placeholer' => 'Qty']) }}
                                        </div>
                                    </div>
                                </div>
                            {{ Form::close() }}

                            <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group pull-right">
                                            <button disabled="" class="btn btn-md btn-primary btn-store" data-toggle="modal" data-target="#modalPenjualan" onclick="focusToPersen()"> F8 | Proses Penjualan</button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>

                </div>
        </div>
    </div>

    {{-- Modal edc lama --}}
      <div class="modal fade" id="modal-edc" role="dialog" aria-labelledby="modal-edc-label" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="modal-edc-label">EDC</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                          <div class="form-group row">
                              <label for="" class="control-label col-md-4 col-sm-4 col-xs-12">No. Kartu</label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                  {{ Form::text('no_kartu_edc', null, ['class' => 'form-control no_kartu_edc']) }}
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="" class="control-label col-md-4 col-sm-4 col-xs-12">Type EDC </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                  {{ Form::select('typ_edc_id', $typeEdc, null, ['class' => 'form-control type_edc select2','placeholder' => '- Pilih -']) }}
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="" class="control-label col-md-4 col-sm-4 col-xs-12">Bulan / Tahun </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                  {{ Form::text('bulan_tahun', null, ['class' => 'form-control bulan_tahun', 'disabled']) }}
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="" class="control-label col-md-4 col-sm-4 col-xs-12">No. Transaksi </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                  {{ Form::text('no_transaksi_edc', null, ['class' => 'form-control no_transaksi_edc', 'disabled']) }}
                              </div>
                          </div>
      
                          <div class="form-group row ">
                              <label for="" class="control-label col-md-4 col-sm-4 col-xs-12">Total</label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                  {{ Form::text('total_edc', $getCartTotal, ['class' => 'form-control total_edc', 'data-total' => $getCartTotal, 'disabled']) }}
                              </div>
                          </div>
                      </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-success button-edc">Ok</button>
                  </div>
              </div>
          </div>
      </div>
    {{--  --}}

    <!-- Modal Pembayaran -->
    <div id="modalPenjualan" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
        {{ Form::open(['route' => 'pos.penjualan.store', 'methods' => 'POST']) }}
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="col-12 modal-title" style="text-align: center;">PEMBAYARAN</h4>
          </div>

          <div class="modal-body">
            <div class="form-group row jarak-modal">
              <div class="col-6"></div>
              {!! Form::label('tipe-pembayaran','Pembayaran',['class' => 'col-2 col-form-label']) !!}
              <div class="col-4">
                {!! Form::select('tipe_pembayaran',['0' => 'Cash', '1' => 'Hutang'], null, ["class" => 'form-control select2 tipe-pembayaran']) !!}
              </div>
            </div>

            <div class="form-group row jarak-modal">
              <div class="col-6"></div>
              {!! Form::label('sub-total','Sub Total',['class' => 'col-2 col-form-label']) !!}
              <div class="col-4 label-modal">
                <label for="" class="col-form-label sub-total-modal">{{ $getCartTotal }}</label>
              </div>
            </div>

            <div class="form-group row jarak-modal">
              <div class="col-6">
                <em style="font-size : 10px; float: right; color: red;">* Tekan Alt + 1</em>
              </div>
              {!! Form::label('potongan','% | Potongan',['class' => 'col-2 col-form-label']) !!}
              <div class="col-2">
                {!! Form::text('nilai_diskon_persen', 0,['class' => 'form-control potongan-persen-modal','onchange' => 'diskonPersenRupiah("persen")']) !!}
              </div>
              <div class="col-2">
                {!! Form::text('nilai_diskon_rupiah', 0,['class' => 'form-control potongan-rupiah-modal','onchange' => 'diskonPersenRupiah("rupiah")']) !!}
              </div>
            </div>

            <div class="form-group row jarak-modal">
              <div class="col-6"></div>
              {!! Form::label('pembulatan','Pembulatan',['class' => 'col-2 col-form-label']) !!}
              <div class="col-4 label-modal">
                <label for="" class="col-form-label">0</label>
              </div>
            </div>

            <div style="background: #5fbeaa;border: 1px;border-radius: 10px; margin-right: -11px;">
              <div class="form-group row" style="margin-left: 10px;">
                <label for="total" class="col-12 col-form-label" style="color: white;">Total</label>
              </div>

              <div class="form-group row">
                  <label for="nilai_total" class="col-12 col-form-label nilai-pelunasan-modal" style="text-align: right;font-size: 40px;color: white;margin-left: -10px;margin-bottom: -30px;margin-top: -30px;">{{ $getCartTotal }}</label>
              </div>

              <div class="form-group row" style="color: white;margin-left: 10px;">
                  <label for="nilai_pelunasan" class="col-12 col-form-label">Nilai Pelunasan <span style="margin-left: 10px;" class="nilai-pelunasan-akhir-modal"> 0</span></label>
              </div>
            </div>

            <div class="form-group row jarak-modal">
              <div class="col-6">
                <em style="font-size : 10px; float: right; color: red;">* Tekan Alt + 3</em>
              </div>
              {!! Form::label('tunai','Tunai',['class' => 'col-2 col-form-label']) !!}
              <div class="col-4">
                {!! Form::text('nilai_bayar_fisik', $getCartTotal,['class' => 'form-control tunai-modal', 'onchange' => 'diskonPersenRupiah("tunai")']) !!}
              </div>
            </div>

            <div class="form-group row jarak-modal">
              <div class="col-6">
                <em style="font-size : 10px; float: right; color: red;">* Tekan Alt + 4</em>
              </div>
              {!! Form::label('kartu_kredit','Kartu Kredit',['class' => 'col-2 col-form-label']) !!}
              <div class="col-1">
                <a href="#" data-toggle="modal" class="btn btn-default btn-kredit" data-animation="false" data-target="#modal-kredit"><i class="fa fa-credit-card"></i></a>
              </div>
              <div class="col-3 label-modal">
                <label for="" class="col-form-label nilai-kredit">0</label>
                {!! Form::hidden('nilai_kredit', 0,['class' => 'nilai-kredit-val']) !!}
              </div>
            </div>

            <div class="form-group row jarak-modal">
              <div class="col-6">
                <em style="font-size : 10px; float: right; color: red;">* Tekan Alt + 5</em>
              </div>
              {!! Form::label('kartur_debit','Kartu Debit',['class' => 'col-2 col-form-label']) !!}
              <div class="col-1">
                <a href="#" data-toggle="modal" class="btn btn-default btn-debit" data-animation="false" data-target="#modal-debit"><i class="fa fa-credit-card"></i></a>
              </div>
              <div class="col-3 label-modal">
                <label for="" class="col-form-label nilai-debit">0</label>
                {!! Form::hidden('nilai_debit', 0,['class' => 'nilai-debit-val']) !!}
              </div>
            </div>

            <div class="form-group row jarak-modal">
              <div class="col-6"></div>
              {!! Form::label('kembali','Kembali',['class' => 'col-2 col-form-label']) !!}
              <div class="col-4 label-modal">
                <label for="" class="col-form-label kembali-modal">0</label>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="form-group row col-12">
              <div class="col-9">
                <button type="button" class="btn btn-danger btn-md-modal" data-dismiss="modal">Batal</button>
              </div>
              <div class="col-3">
                <button type="button" class="btn btn-success btn-lunas-modal" style="width: 178px; height: 60px;">F9 | Lunas</button>
              </div>
            </div>
          </div>
        {{ Form::hidden('no_penjualan',$no_penjualan, ['class' => 'no_penjualan']) }}
        {!! Form::close() !!}
        </div>

      </div>
    </div>

    <!-- Modal Kartu Kredit -->
    <div id="modal-kredit" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="col-12 modal-title" style="text-align: center;">- Kartu Kredit -</h4>
          </div>
          <div class="modal-body">
            
            <div class="form-group row jarak-modal">
                {!! Form::label('jenis_kartu','Jenis Kartu',['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                  {!! Form::select('type_edc_id',['0' => 'EDC BNI', '1' => 'EDC BCA'],null,['class' => 'form-control select2','placeholder' => '- Pilih -']) !!}
                </div>
            </div>
            <div class="form-group row jarak-modal">
                {!! Form::label('pembayaran_kredit','Pembayaran',['class' => 'col-3 col-form-label']) !!}
                <div class="col-4">
                  {!! Form::text('pembayaran_kredit', 0,['class' => 'form-control']) !!}
                </div>
                {!! Form::label('nilai_pelunasan','Nilai Pelunasan',['class' => 'col-3 col-form-label']) !!}
                {!! Form::label('nilai_rupiah_pelunasan','15.500',['class' => 'col-2 col-form-label']) !!}
            </div>
            <div class="form-group row jarak-modal">
                {!! Form::label('batch','Batch',['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                  {!! Form::text('no_batch', null,['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group row jarak-modal">
                {!! Form::label('no_kartu','No Kartu',['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                  {!! Form::text('no_kartu', null,['class' => 'form-control']) !!}
                </div>
            </div>

            <div  style="border: 2px solid #ebeff2; border-radius: 10px; padding: 20px;">
              <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <td width="60%">No. Kartu</td>
                      <td width="10%">#</td>
                      <td width="30%" style="text-align: right;">Total</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1000</td>
                      <td>wew</td>
                      <td style="text-align: right;">15.000</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <div class="form-group row jarak-modal" style="margin-top: 10px;">
              <div class="col-9"></div>
              {!! Form::label('total_kredit','Total',['class' => 'col-1 col-form-label']) !!}
              {!! Form::label('total_nilai_kredit','15.000',['class' => 'col-2 col-form-label', 'style' => 'border: 1px solid #ebeff2; border-radius: 10px; text-align: right; margin-left: -8px;']) !!}
            </div>

          </div>
          <div class="modal-footer">
            <div class="form-group row col-12">
              <div class="col-10">
                <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-left: -31px; height: 55px; width: 127px;">Hapus</button>
              </div>
              <div class="col-2">
                <button type="button" class="btn btn-success" style="margin-left: -14px; height: 55px; width: 127px;">OK</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Kartu Debit -->
    <div id="modal-debit" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="col-12 modal-title" style="text-align: center;">- Kartu Debit -</h4>
          </div>
          <div class="modal-body">

            <div class="form-group row jarak-modal">
                {!! Form::label('jenis_kartu','Jenis Kartu',['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                  {!! Form::select('type_edc_id', $typeEdc, null,['class' => 'form-control select2 type-edc-debit','placeholder' => '- Pilih -']) !!}
                </div>
            </div>
            <div class="form-group row jarak-modal">
                {!! Form::label('pembayaran_debit','Pembayaran',['class' => 'col-3 col-form-label']) !!}
                <div class="col-4">
                  {!! Form::text('pembayaran_debit', 0,['class' => 'form-control pembayaran-debit']) !!}
                </div>
                {!! Form::label('nilai_pelunasan','Nilai Pelunasan',['class' => 'col-3 col-form-label']) !!}
                <label for="" class="col-2 col-form-label nilai-pelunasan-debit">{{ $getEdcDebitPelunasan }}</label>
            </div>
            <div class="form-group row jarak-modal">
                {!! Form::label('batch','Batch',['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                  {!! Form::text('no_batch', null,['class' => 'form-control no-batch-debit']) !!}
                </div>
            </div>
            <div class="form-group row jarak-modal">
                {!! Form::label('no_kartu','No Kartu',['class' => 'col-3 col-form-label']) !!}
                <div class="col-9">
                  {!! Form::text('no_kartu', null,['class' => 'form-control no-kartu-debit']) !!}
                </div>
            </div>

            <div  style="border: 2px solid #ebeff2; border-radius: 10px; padding: 20px;">
              <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <td width="50%">No. Kartu</td>
                      <td width="10%">#</td>
                      <td width="30%" style="text-align: right;">Total</td>
                      <td width="10%">Opt</td>
                    </tr>
                  </thead>
                  <tbody class="table-edc-debit">
                    @foreach (Cart::instance('edc-debit-' . auth()->user()->id)->content()->sortByDesc('options.id') as $cart)
                      <tr>
                        <td>{{ $cart->options->no_kartu }}</td>
                          {!! Form::hidden('type_edc_id', $cart->options->type_edc_id) !!}
                        <td>{{ $cart->options->batch }}</td>
                        <td style="text-align: right;">{{ number_format($cart->options->pembayaran_debit) }}</td>
                        <td>
                          <a href="#" data-id="{{ $cart->rowId }}" class="btn btn-danger btn-xs btn-remove-debit"> 
                            <i class="fa fa-trash"></i> 
                          </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>

            <div class="form-group row jarak-modal" style="margin-top: 10px;">
              <div class="col-9"></div>
              {!! Form::label('total_debit','Total',['class' => 'col-1 col-form-label']) !!}
              <label for="" class="col-2 col-form-label total-debit" style="border: 1px solid #ebeff2; border-radius: 10px; text-align: right; margin-left: -8px;">{{ $getCartEdcDebitTotal }}</label>
            </div>
          </div>
          <div class="modal-footer">
            <div class="form-group row col-12">
                <button type="button" class="btn btn-success btn-edc-debit" data-dismiss="modal" data-animation="false" style="margin-left: -14px; height: 55px; width: 127px;" >OK</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- Modal Kembalian --}}
    <div class="modal fade" id="modal-kembalian" role="dialog" aria-labelledby="modal-kembalian-label" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content" style="width: 500px;">
                <div class="modal-header">
                    <h5 class="modal-title col-12" id="modal-edc-label" style="text-align: center; font-size: 28px;">- Kembalian -</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="font-size: 28px;">
                    <div class="form-group row">
                        <label for="" class="control-label col-md-6 col-sm-6 col-xs-12">Total</label>
                        <label for="" class="control-label col-md-6 col-sm-6 col-xs-12 label-modal kembalian-total"></label>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-6 col-sm-6 col-xs-12">Bayar</label>
                        <label for="" class="control-label col-md-6 col-sm-6 col-xs-12 label-modal kembalian-bayar"></label>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-6 col-sm-6 col-xs-12">Kembalian</label>
                        <label for="" class="control-label col-md-6 col-sm-6 col-xs-12 label-modal kembalian-kembalian"></label>
                    </div>
                </div>
                <div class="modal-footer">
                      <button type="button" style="margin-top: 24px;" class="btn btn-warning col-md-5 col-sm-5 col-xs-12 btn-cetak-ulang">Cetak Ulang</button>
                      <div class="col-md-1 col-sm-1 col-xs-12"></div>
                      <button type="button" style="height: 63px;" class="btn btn-success col-md-6 col-sm-6 col-xs-12 btn-lanjut" data-dismiss="modal">ENTER | LANJUT &nbsp;&nbsp;>></button>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal cek barang --}}
    <div class="modal fade" id="modal-cek-barang" role="dialog" aria-labelledby="modal-cek-barang-label" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-harga-label">Cek Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 45px;">
                            {{ Form::text('cari_barang', null, ['class' => 'form-control cari-barang','placeholder' => 'Scan Barcode / Cari Barang']) }}
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No. Barang</th>
                                            <th>No. Barcode</th>
                                            <th>Nama Barang</th>
                                            <th>Harga</th>
                                            <th>Kuantitas</th>
                                        </tr>
                                    </thead>

                                    <tbody class="box-cari-barang">
                                       
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom_js')
    <script type="text/javascript">   
        const token = $('meta[name="csrf-token"]').attr('content');
        @if ( ($cart != null) )
            @if ( $cart->cart_id != null )
                let cart_id = {{ $cart->cart_id }};
            @else
                let cart_id = {{ $carts->generateCartId() }};
            @endif
        @else
            let cart_id = {{ $carts->generateCartId() }};
        @endif

        function focusToPersen() {
          setTimeout(function() { $('.potongan-persen-modal').focus() }, 300);
        }     

        jQuery(document).ready(function () {

            checkTablePenjualan();

            var edc = 0;
            setTimeout(function() { $('.barcode_barang').focus() }, 300);
            function clearEDC(){
                $('.bulan_tahun').val('');
                $('.no_transaksi_edc').val('');
                $('.nilai_bayar_elektronik').autoNumeric('set', 0);
                $('.total_edc').autoNumeric('set', 0);

                $('.nilai_bayar').removeAttr('disabled');
                $('.bulan_tahun').attr('disabled', true).removeAttr('required');
                $('.no_transaksi_edc').attr('disabled', true).removeAttr('required');
                $('.total_edc').attr('disabled', true).removeAttr('required'); 
            }

            $('body').on('click', '.btn-edc', function (e) {
                e.preventDefault();
            });

            $('.no_kartu_edc').attr('maxlength', '19');
            $('.bulan_tahun').attr('maxlength', '4');
            $('.no_kartu_edc').keydown(function(e) {
                let keyAngka = [8, 37, 38, 39, 40, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
                if (keyAngka.indexOf(e.which) !== -1){
                    var keyCode = e.keyCode || e.which;
                    if (keyCode == 40) {
                        $('.bulan_tahun').focus();
                    }
                }else{
                    return false;
                }
            });

            $('.bulan_tahun').keydown(function(e) {
                let keyAngka = [8, 37, 38, 39, 40, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
                if (keyAngka.indexOf(e.which) !== -1){
                    var keyCode = e.keyCode || e.which;
                    if (keyCode == 40) {
                        $('.no_transaksi_edc').focus();
                    }else if (keyCode == 38) {
                        $('.no_kartu_edc').focus();
                    }
                }else{  
                    return false;
                }
            });

            $('body').keydown(function(e) {
                var keyCode = e.keyCode || e.which;
                // F2
                if (keyCode == 113) {
                    $('.qty_input').focus();
                }
                // TAB
                if (keyCode == 27) {
                    setTimeout(function() { $('.barcode_barang').focus() }, 300);
                }
                // F4
                if (keyCode == 115) {
                    $('.nilai_bayar').focus();
                }
                // F7
                if (keyCode == 118) {
                    $('.btn-cek-barang').trigger('click');
                }
                // F8
                // if (keyCode == 119) {
                //     $('.btn-promo').trigger('click');
                // }

                // F8
                if (keyCode == 119) {
                    $('.btn-store').trigger('click');
                    setTimeout(function() { $('.potongan-persen-modal').focus() }, 300);
                }

                // F9
                if (($("#modalPenjualan").data('bs.modal') || {})._isShown) {
                  if (keyCode == 120) {
                      $('.btn-lunas-modal').trigger('click');
                  }
                }                

                // modal kembalian dan klik enter
                if (($("#modal-kembalian").data('bs.modal') || {})._isShown) {
                    if (keyCode == 13) {
                        $(".btn-lanjut").trigger('click');
                    }
                }

                // alt + 1
                if (keyCode === 49 && e.altKey) {
                    $('.potongan-persen-modal').focus();
                }

                // alt + 3
                if (keyCode === 51 && e.altKey) {
                    $('.tunai-modal').focus();
                }

                // alt + 4
                if (keyCode === 52 && e.altKey) {
                    $('.btn-kredit').trigger('click');
                }

                // alt + 5
                if (keyCode === 53 && e.altKey) {
                    $('.btn-debit').trigger('click');
                }

            });

            $('body').on('click', '.btn-cek-barang', function () {
                $("#modal-cek-barang").show();
                $("tbody.box-cari-barang").html('');
                $("input[name='cari_barang']").val('');
                $("input[name='cari_barang']").focus(); 
            });

            $('.no_transaksi_edc').keydown(function(e) {
                let keyAngka = [8, 37, 38, 39, 40, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
                if (keyAngka.indexOf(e.which) !== -1){
                    var keyCode = e.keyCode || e.which;
                    if (keyCode == 40) {
                        $('.total_edc').focus();
                    }if (keyCode == 38) {
                        $('.bulan_tahun').focus();
                    }
                }
            });

            $('.total_edc').keydown(function(e) {
                let keyAngka = [8, 37, 38, 39, 40, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
                if (keyAngka.indexOf(e.which) !== -1){
                    var keyCode = e.keyCode || e.which;
                    if (keyCode == 38) {
                        $('.no_transaksi_edc').focus();
                    }
                }else{  
                    return false;
                }
            });
            $('body').on('keyup', '.no_kartu_edc', function (e) {
                
                var el = $(this);
                
                if ( $(el).val().length == 19) {
                    
                    if (edc == 0) {
                        edc = 1;

                        $(el).attr('required', true);

                        $('.bulan_tahun').attr('required', true).removeAttr('disabled');
                        $('.no_transaksi_edc').attr('required', true).removeAttr('disabled');
                        $('.total_edc').attr('required', true).removeAttr('disabled');
                    }

                } else {
                    if (edc == 1) {
                        edc = 0;

                        $(el).removeAttr('required');
                        clearEDC();
                        $('.nilai_bayar').removeAttr('disabled');
                        $('.bulan_tahun').attr('disabled', true).removeAttr('required');
                        $('.no_transaksi_edc').attr('disabled', true).removeAttr('required');
                        $('.total_edc').attr('disabled', true).removeAttr('required'); 
                    }

                }

            });

            // TODO Ajax edit harga jual
            // 1. membuat show & hide label harga dan input harga jual
            // 2. fungis ajax update harga di cart
            $('body').on('click','.btn-edit-item', function (e) {
                e.preventDefault();
                
                // menghilangkan spasi dari label barga jual & menjadikan numeric
                var temp_harga                    = $(this).closest('tr').find('.harga_edit').text();
                var temp_diskon_rupiah            = $(this).closest('tr').find('.diskon_rupiah_edit').text();
                var temp_qty                      = $(this).closest('tr').find('.qty_edit').text();
                var temp_diskon_persen            = $(this).closest('tr').find('.diskon_persen_edit').text();
                var temp_harga_no_space           = temp_harga.replace(/\s/g, '');
                var temp_diskon_rupiah_no_space   = temp_diskon_rupiah.replace(/\s/g, '');
                var harga_to_numeric              = changeMaskingToNumber(temp_harga_no_space);
                var diskon_rupiah_to_numeric      = changeMaskingToNumber(temp_diskon_rupiah_no_space);

                // show & hidden
                $(this).closest('tr').find('.harga_edit').hide();
                $(this).closest('tr').find('.qty_edit').hide();
                $(this).closest('tr').find('.diskon_persen_edit').hide();
                $(this).closest('tr').find('.diskon_rupiah_edit').hide();

                $(this).closest('tr').find('.harga_edit_hidden')
                                     .show();
                $(this).closest('tr').find('.diskon_persen_hidden')
                                     .show();
                $(this).closest('tr').find('.diskon_rupiah_hidden')
                                     .show();
                $(this).closest('tr').find('.qty_edit_hidden')
                                     .attr('data-toggle','tooltip')
                                     .attr('data-placement','left')
                                     .attr('title','Tekan Enter')
                                     .show();

                $(this).closest('tr').find('.qty_edit_hidden').tooltip('show');
                $(this).closest('tr').find('.qty_detail').autoNumeric('init').autoNumeric('set', temp_qty);
                $(this).closest('tr').find('.harga_detail').autoNumeric('init').autoNumeric('set', harga_to_numeric);
                $(this).closest('tr').find('.diskon_persen_detail').autoNumeric('init').autoNumeric('set', temp_diskon_persen);
                $(this).closest('tr').find('.diskon_rupiah_detail').autoNumeric('init').autoNumeric('set', diskon_rupiah_to_numeric);
                $(this).closest('tr').find('.qty_detail').select().focus();


                $(this).hide();
            });

            $('body').on('keydown','.harga_detail', function (event) {

                var el      = $(this);
                var keyCode = event.keyCode || event.which;

                if (keyCode == 13) {

                    updateItem(el,'harga');

                }
            });

            $('body').on('keydown','.qty_detail', function (event) {

                var el      = $(this);
                var keyCode = event.keyCode || event.which;

                if (keyCode == 13) {

                    updateItem(el,'qty');

                }
            });

            $('body').on('keydown','.diskon_persen_detail', function (event) {

                var el      = $(this);
                var keyCode = event.keyCode || event.which;

                if (keyCode == 13) {

                    updateItem(el,'diskon_persen');

                }
            });

            $('body').on('keydown','.diskon_rupiah_detail', function (event) {

                var el      = $(this);
                var keyCode = event.keyCode || event.which;

                if (keyCode == 13) {

                    updateItem(el,'diskon_rupiah');

                }
            });

            $('body').on('click', '.button-edc', function (e) {
                var total_edc = $('.total_edc').autoNumeric('get');
                $('.nilai_bayar_elektronik').autoNumeric('set', Number(total_edc) );
                $('.nilai_bayar').autoNumeric('set', 0);


                // perhitungan kembalian
                var nilai_diskon    = $('.nilai_diskon').autoNumeric('get');
                var total           = $('.total-number').data('total');
                var kembalian       = Number(0);

                kembalian = ( Number(total_edc) ) - ( Number(total) - Number(nilai_diskon) );

                if (kembalian < 0) {
                    $('.nilai_bayar').removeAttr('disabled');
                }else {
                    $('.nilai_bayar').attr('disabled','disabled');
                }

                $('.nilai_kembalian').autoNumeric('set', Number(kembalian) );

                if($('.no_kartu_edc').val().length == 19 && $('.total_edc').val() < 1){
                    alert('Nominal Total EDC tidak boleh 0');
                    $('.total_edc').focus();
                }else{
                    $('#modal-edc').modal('hide');
                }
            });

            // $('body').on('click','.btn-hutang', function (e) {
                
            //     e.preventDefault();

            //     $("#status-pos-hidden").val(1);

            //     var event = $.Event( "click" );
            //     $( ".btn-store" ).trigger( event );

            // });

            $('body').on('click', '.btn-lunas-modal', function (e) {

                e.preventDefault();

                $('.alert-pos').html(
                    ''
                );

                var el = $(this);

                var form = $(this).closest('form');

                var data_id_array = [];

                $(".box-penjualan").find('tr').each(function () {
                    data_id_array[$(this).data('id')] = $(this).find('.cart-qty').data('qty');                    
                });

                $.ajax({
                    url: $(form).attr('action') + '?' + $(form).serialize(),
                    type: $(form).attr('method'),
                    dataType: 'JSON',
                    data: {
                        id: $(".pelanggan_select").val(),
                        cart_id: cart_id,
                        nilai_kembalian : $(".kembali-modal").autoNumeric('get'),
                        nilai_pelunasan : $('.nilai-pelunasan-modal').autoNumeric('get'),
                        tanggal_custom : $(".tanggal-custom").val(),
                        data_id_array : data_id_array,
                    },
                    success: function (result) {
                        if ($.isFunction(window.printStruck)) {
                            window.printStruck(result);
                        }else{
                          // console.log("http://localhost:8901/print-local"+encodeURIComponent(JSON.stringify(result)));
                          //   $.ajax({
                          //       url: 'http://localhost:8901/print-local',
                          //       type: 'GET',
                          //       dataType: 'JSON',
                          //       data: {
                          //           struk: result, 
                          //       },
                          //   })
                          //   .done(function() {
                          //       console.log("success");
                          //   })
                          //   .fail(function() {
                          //       console.log("error");
                          //   })
                          //   .always(function() {
                          //       console.log("complete");
                          //   });
                            
                          // window.open("http://localhost:8901/print-local?data="+encodeURIComponent(JSON.stringify(result)));


                        }

                        if (result.status) {                            

                            clearModalPenjualan();

                            $('.box-penjualan').html('');

                            $('.table-edc-debit').html('');

                            $('.total-number').val(''); $('.total-number').html('');

                            $('.pelanggan_select').val(1).trigger('change');

                            setTimeout(function() {
                                $('.barcode_barang').focus();
                                $('.barcode_barang').val(); $('.barcode_barang').html();
                             }, 300);
                        } else {
                            $(template).find('.alert').addClass('alert-warning');
                        }

                        modalKembalian(result);

                        $('.barcode_barang').val('');
                        $('.no_penjualan').val(result.data.no_penjualan);
                        $("#status-pos-hidden").val(0);
                    },
                    error: function () {
                        var template = $(
                            $('#template-alert').html()
                        ).clone();

                        $(template).find('.alert-message').html('Terjadi Kesalahan Server');

                        $('.alert-pos').html(
                            template
                        );
                        $("#status-pos-hidden").val(0);
                    }
                }); 
                $(this).attr('disabled',true);
                setTimeout(function(){ $('.btn-lunas-modal').attr('disabled',false)},4000);
                $(".btn-store").attr('disabled','disabled');
            });

            function clearModalPenjualan() {
              $('.potongan-persen-modal').val(0);
              $('.potongan-rupiah-modal').autoNumeric('set', 0);
              $('.sub-total-modal').autoNumeric('set', 0);
              $('.nilai-pelunasan-akhir-modal').autoNumeric('set', 0);
              $('.nilai-pelunasan-modal').autoNumeric('set', 0);
              $('.tunai-modal').autoNumeric('set', 0);
              $('.kembali-modal').autoNumeric('set', 0);
              $('.tipe-pembayaran').val('0').trigger('change');
            }

            function modalKembalian(result) {
                $(".kembalian-total").text(result.data.total);
                $('.kembalian-bayar').text(result.data.tunai);
                $('.kembalian-kembalian').text(result.data.kembali);
                $('#modalPenjualan').modal('hide');
                $('#modal-kembalian').modal('show');
            }

            $('.nilai_bayar_elektronik').autoNumeric('init', {
                aPad: false
            });

            $('.nilai_bayar').autoNumeric('init', {
                aPad: false
            });

            $('.nilai_diskon').autoNumeric('init', {
                aPad: false
            });

            $('.nilai_kembalian').autoNumeric('init', {
                aPad: false
            });

            $('.total_edc').autoNumeric('init', {
                aPad: false
            });

            $('.total-number').autoNumeric('init', {
                aPad: false
            });

            $('.harga_detail').autoNumeric('init', {
                aPad: false
            });

            $('.nilai-pelunasan-modal').autoNumeric('init', {
              aPad: false
            });

            $('.sub-total-modal').autoNumeric('init', {
              aPad: false
            });

            $('.harga_jual_modal').autoNumeric('init', {
                aPad: false
            });

            $('.potongan-rupiah-modal').autoNumeric('init', {
                aPad: false
            });

            $('.tunai-modal').autoNumeric('init', {
                aPad: false
            });

            $('.nilai-pelunasan-akhir-modal').autoNumeric('init', {
                aPad: false
            });

            $('.kembali-modal').autoNumeric('init', {
                aPad: false
            });

            $('.pembayaran-debit').autoNumeric('init', {
                aPad: false
            });

            $('.nilai-pelunasan-debit').autoNumeric('init', {
                aPad: false
            });

            $('.total-debit').autoNumeric('init', {
                aPad: false
            });

            $('.nilai-kredit').autoNumeric('init', {
                aPad: false
            });

            $('.nilai-debit').autoNumeric('init', {
                aPad: false
            });

            $('body').on('keyup', '.nilai_bayar', function (e) {

                var el = $(this);

                var bayar = $(el).autoNumeric('get');

                if (edc == 1) {

                    var total_edc = $('.nilai_bayar_elektronik').autoNumeric('get');

                    var nilai_diskon = $('.nilai_diskon').autoNumeric('get');
                    var total = $('.total-number').data('total');
                    var kembalian = Number(0);
                    
                    if ( Number(nilai_diskon) > Number(total) ) {
                        bayar = Number(total_edc) + Number(bayar);

                        $('.nilai_kembalian').autoNumeric('set', Number(bayar) );
                    } else {
                        kembalian = ( Number(total_edc) + Number(bayar) ) - ( Number(total) - Number(nilai_diskon) );

                        $('.nilai_kembalian').autoNumeric('set', Number(kembalian) );
                    }  

                } else {
                    var nilai_diskon = $('.nilai_diskon').autoNumeric('get');
                    var total = $('.total-number').data('total');
                    var kembalian = Number(0);
                    
                    if ( Number(nilai_diskon) > Number(total) ) {
                        bayar = Number(bayar);
                        $('.nilai_kembalian').autoNumeric('set', Number(bayar) );

                    } else {
                        kembalian = Number(bayar) - ( Number(total) - Number(nilai_diskon) );

                        $('.nilai_kembalian').autoNumeric('set', Number(kembalian) );
                    }
                }
            });


            $('body').on('keyup', '.nilai_diskon', function (e) {

                var el = $(this);

                var bayar = $('.nilai_bayar').autoNumeric('get');

                if (edc == 1) {

                    var total_edc = $('.total_edc').autoNumeric('get');

                    var nilai_diskon = $(el).autoNumeric('get');
                    var total = $('.total-number').data('total');
                    var kembalian = Number(0);
                    
                    if ( Number(nilai_diskon) > Number(total) ) {
                        bayar = Number(total_edc) + Number(bayar);

                        $('.nilai_kembalian').autoNumeric('set', Number(bayar) );
                    } else {
                        kembalian = ( Number(total_edc) + Number(bayar) ) - ( Number(total) - Number(nilai_diskon) );

                        $('.nilai_kembalian').autoNumeric('set', Number(kembalian) );
                    }  

                } else {
                    var nilai_diskon = $(el).autoNumeric('get');
                    var total = $('.total-number').data('total');
                    var kembalian = Number(0);
                    
                    if ( Number(nilai_diskon) > Number(total) ) {
                        bayar = Number(bayar);

                        $('.nilai_kembalian').autoNumeric('set', Number(bayar) );
                    } else {
                        kembalian = Number(bayar) - ( Number(total) - Number(nilai_diskon) );

                        $('.nilai_kembalian').autoNumeric('set', Number(kembalian) );
                    }  
                }
                $(".nilai_diskon_persen").val(0);
            });

            $('body').on('keyup', '.total_edc', function (e) {

                var el = $(this);

                var bayar = $('.nilai_bayar').autoNumeric('get');

                if (edc == 1) {

                    var total_edc = $(el).autoNumeric('get');

                    var nilai_diskon = $('.nilai_diskon').autoNumeric('get');
                    var total = $('.total-number').data('total');
                    var kembalian = Number(0);
                    
                    $('.nilai_bayar').attr('disabled', true); 

                    if ( Number(nilai_diskon) > Number(total) ) {
                        bayar = Number(total_edc) + Number(bayar);

                        $('.nilai_kembalian').autoNumeric('set', Number(bayar) );
                        $('.nilai_bayar_elektronik').autoNumeric('set', Number(total_edc) );
                        $('.nilai_bayar').autoNumeric('set', 0 );
                    } else {
                        kembalian = ( Number(total_edc) + Number(bayar) ) - ( Number(total) - Number(nilai_diskon) );

                        $('.nilai_kembalian').autoNumeric('set', Number(kembalian) );
                        $('.nilai_bayar_elektronik').autoNumeric('set', Number(total_edc) );
                        $('.nilai_bayar').autoNumeric('set', 0 );
                    }  

                } else {
                    var nilai_diskon = $('.nilai_diskon').autoNumeric('get');
                    var total = $('.total-number').data('total');
                    var kembalian = Number(0);
                    
                    if ( Number(nilai_diskon) > Number(total) ) {
                        bayar = Number(bayar);

                        $('.nilai_kembalian').autoNumeric('set', Number(bayar) );
                        $('.nilai_bayar_elektronik').autoNumeric('set', 0 );
                    } else {
                        kembalian = Number(bayar) - ( Number(total) - Number(nilai_diskon) );

                        $('.nilai_kembalian').autoNumeric('set', Number(kembalian) );
                        $('.nilai_bayar_elektronik').autoNumeric('set', 0 );
                    }  
                }
            });

            // select pelanggan
            $('.pelanggan_select').select2({
                placeholder: 'Cari Pelanggan',
                width: '100%',
                allowClear: false,
                ajax: {
                    url: '{{ route("pos.penjualan.pelanggan") }}',
                    dataType: 'JSON',
                    type: 'GET',
                    data: function (params) {
                        return {
                            search: params.term, // search term
                            page: params.page
                        };
                    },
                    cache: true
                },
                templateResult: function (state) {
                    if ( ! state.id) {
                        return state.text;
                    }
                    
                    var $state = $(
                        '<div style="font-size: 14px">  <span style="display:block"> <small> ' + state.no_pelanggan + '</small> </span>' + state.text + '</div>'
                    );

                    return $state;
                },
                minimumInputLength: 3
            });

            
            $('body').on('keypress','.barcode_barang', function (e) {            
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13 ) {
                    searchBarangInText();
                }
                // checkSessionTerdiskon();  
            });
            
            

            $(".barcode_barang").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "{{ route('pos.penjualan.produk.test') }}",
                        dataType: "JSON",
                        data: {
                            q: request.term
                        },
                        success: function(data) {
                            if(data.results.length > 0) {
                                $(".div_label_not_found").hide();
                                response( $.map( data.results, function( item ) {
                                    var object = new Object();
                                    object.fullName = item.fullName;
                                    object.barcode = item.barcode;
                                    object.no_barang = item.no_barang;
                                    return object
                                }));
                            }else {
                                $(".div_label_not_found").hide();
                                $(".div_label_not_found").fadeIn(500);
                                $(".barcode_barang").focus();
                            }
                        }
                    });
                },
                minLength: 3,
                focus: function( event, ui ) {
                    $( ".barcode_barang" ).val( ui.item.barcode );
                    
                    return false;
                },
                select: function( event, ui ) {
                    $( ".barcode_barang" ).val( ui.item.barcode );
                    searchBarangInText();
                    return false;
                }
            })
            .autocomplete( "instance" )._renderItem = function( ul, item ) {
                  return $( "<li>" )
                  .append('<div style="font-size: 14px">  <span style="display:block"> <small> ' + item.barcode  + '</small> </span>' + item.fullName + '</div>')
                  .appendTo( ul );
            };

            $('body').on('keypress','.cari-barang', function (e) {

                var keyCode = e.keyCode || e.which;

                if (keyCode === 13 ) {

                    searchBarangInModal();

                }                 
            });            
            // 

            $('body').on('click', '.btn-reset-penjualan', function (e) {
                e.preventDefault();

                $.ajax({
                    url: '{{ route('pos.get.reset.penjualan') }}',
                    data: {cart_id: cart_id},
                    type: 'POST',
                    dataType: 'JSON',
                })
                .done(function(response) {
                    
                    if (response === true) {
                        swal('Sukses!', 'berhasil melakukan reset data!', 'success');
                        $('.qty_input').val('');

                        $('.barcode_barang').val('').trigger('change');

                        $('.total-number').autoNumeric('set', 0); 

                        $('.total-number').data('total', 0);

                        clearModalPenjualan();

                        $('.total_edc').autoNumeric('set', 0);

                        $('.total_edc').data('total', 0);

                        $('.box-penjualan').html(
                            ''
                        );

                        checkTablePenjualan();

                    }

                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

            $('body').on('click', '.btn-remove-item', function (e) {
                e.preventDefault();

                $('.alert-pos').html(
                    ''
                );

                var el = $(this);

                $('.nilai_bayar').autoNumeric('set', 0);
                $('.nilai_diskon').autoNumeric('set', 0);
                $('.nilai_kembalian').autoNumeric('set', 0);

                $.ajax({
                    url: '{{ route("pos.penjualan.remove.item") }}',
                    type: 'GET',
                    data: {
                        id: $(el).data('id'),
                        no_penjualan : $('.no_penjualan').val(),
                        diskon : 0,
                        cart_id: cart_id,
                    },
                    dataType: 'JSON',
                    success: function (result) {
                        if (result.status) {

                            if( result.total_items_cart == 0 ) {
                                $('.tundaBtn').addClass('disabled');
                            }

                            if (edc == 1) {
                                $('.total_edc').autoNumeric('set', result.total);

                                $('.total_edc').data('total', result.total);

                                $('.nilai_bayar_elektronik').autoNumeric('set', result.total);
                            }

                            $('.total-number').autoNumeric('set', result.total);

                            $('.total-number').data('total', result.total);

                            $('.sub-total-modal').autoNumeric('set', result.total);

                            $('.tunai-modal').autoNumeric('set', result.total);
                            
                            calculateModal(result.total, $('.tunai-modal').autoNumeric('get'), checkDiskonPersen(result.total, $('.potongan-persen-modal').val()));

                            $('.box-penjualan').html(
                                result.template
                            );

                            $(el).closest('tr').remove();
                            
                            checkTablePenjualan();

                            // checkSessionTerdiskon();
                        }
                    }
                });
            });

            $('body').on('keypress','.no-kartu-debit', function (e) {

                var keyCode = e.keyCode || e.which;

                    
                if (keyCode === 13 ) {

                    addListEdcDebit();

                }   

            });

            $('body').on('click', '.btn-remove-debit', function (e) {

                var el = $(this);

                e.preventDefault();

                removeListEdcDebit(el);

            });

            $('body').on('click', '.btn-edc-debit', function (e) {

                e.preventDefault();

                submitEdcDebit();

            });

            $('body').on('change','.tipe-pembayaran', function (e) {
                e.preventDefault();

                tipePembayaran($(this).val());

            });

            $('body').on('click','.btn-cetak-ulang', function (e) {
                e.preventDefault();

                cetakUlang();
            });
            
        });

        function cetakUlang() {
            $.ajax({
              url: '{{ route('pos.cetak.ulang') }}',
              type: 'POST',
              dataType: 'JSON',
              data: {
                param1: 'value1'
              },
            })
            .done(function(result) {
              if ($.isFunction(window.printStruck)) {
                  window.printStruck(result);
              }
            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });
            
        }

        function tipePembayaran(tipe) {

            if (tipe === '0') {

                clearPartOfPenjualan();
                $('.tunai-modal').removeAttr('disabled');
                $('.btn-debit').removeClass('disabled');
                $('.btn-kredit').removeClass('disabled');

            }else {

                clearPartOfPenjualan();
                $('.tunai-modal').attr('disabled','disabled');
                $('.btn-debit').addClass('disabled');
                $('.btn-kredit').addClass('disabled');
                $('.btn-lunas-modal').removeAttr('disabled');

            }

        }

        function clearPartOfPenjualan() {

            $('.potongan-persen-modal').val(0);
            $('.potongan-rupiah-modal').val(0);
            $('.nilai-pelunasan-akhir-modal').text(0);
            $('.tunai-modal').val(0);
            $('.nilai-kredit').text(0);
            $('.nilai-kredit-val').val(0);
            $('.nilai-debit').text(0);
            $('.nilai-debit-val').val(0);
            $('.kembali-modal').text(0);

        }

        function submitEdcDebit() {

            var total_debit = $(".total-debit").autoNumeric('get');
            
            $(".nilai-debit").autoNumeric('set', total_debit);

            $('.nilai-debit-val').val(total_debit);

            diskonPersenRupiah(type = null);     
        
        }

        function removeListEdcDebit(el) {

            $.ajax({
                url: '{{ route("pos.penjualan.remove.edc.debit") }}',
                type: 'GET',
                data: {
                    id: $(el).data('id'),
                },
                dataType: 'JSON',
                success: function (result) {
                    if (result.status) {

                        var temp_pelunasan = $('.nilai-pelunasan-akhir-modal').autoNumeric('get') > 0 ? 
                                             $('.nilai-pelunasan-akhir-modal').autoNumeric('get') : 
                                             $('.nilai-pelunasan-modal').autoNumeric('get');

                        $('.total-debit').autoNumeric('set', result.total);

                        $('.nilai-pelunasan-debit').autoNumeric('set', temp_pelunasan - result.total);

                        $('.table-edc-debit').html(
                            result.template
                        );

                        $(el).closest('tr').remove();
                    }
                }
            });

        }

        function addListEdcDebit() {
            if ($(".no-kartu-debit").val() !== "" && $(".pembayaran-debit").autoNumeric('get') > 0 && $(".pembayaran-debit").autoNumeric('get') !== null && $(".type-edc-debit").val() !== "") { 
                
                $.ajax({
                    url: '{{ route("pos.penjualan.add.edc.debit") }}',
                    type: 'GET',
                    dataType: 'JSON',
                    data: {
                        no_kartu_debit : $('.no-kartu-debit').val(), 
                        pembayaran_debit : $('.pembayaran-debit').autoNumeric('get'),
                        no_batch_debit : $('.no-batch-debit').val(),
                        type_edc_debit : $('.type-edc-debit').val(),
                    },
                    success: function (result) {

                        if (result.status === true) {

                            $('.type-edc-debit').val('').trigger('change');

                            $('.pembayaran-debit').autoNumeric('set', 0);

                            $('.no-batch-debit').val('');

                            $('.no-kartu-debit').val('');

                            $('.total-debit').autoNumeric('set', result.total);

                            calcNilaiPelunasanDebit(result.total);

                            $('.table-edc-debit').html(
                                result.template
                            );
                        }

                        $('.type-edc-debit').focus();
                    }
                });

            }else {

                swal({
                    icon: "error",
                    text: "Data tidak lengkap"
                });

            }
        }

        function calcNilaiPelunasanDebit(total) {
            var temp_pelunasan        = $('.nilai-pelunasan-akhir-modal').autoNumeric('get') > 0 ? $('.nilai-pelunasan-akhir-modal').autoNumeric('get') : $('.nilai-pelunasan-modal').autoNumeric('get');
            var nilai_pelunasan_debit = temp_pelunasan - total;

            $('.nilai-pelunasan-debit').autoNumeric('set', nilai_pelunasan_debit);
        }

        function diskonPersenRupiah(type) {

            var potongan_persen     = $('.potongan-persen-modal').val();
            var potongan_rupiah     = $('.potongan-rupiah-modal').autoNumeric('get');
            var sub_total           = $('.sub-total-modal').autoNumeric('get');
            var nilai_tunai         = $('.tunai-modal').autoNumeric('get');
            var kembali             = $('.kembali-modal').autoNumeric('get');
            var new_diskon_rupiah   = 0;
            var sub_total_diskon    = 0;
            var nilai_pelunasan     = 0;
            var nilai_kembali       = 0;

            if ($(".tipe-pembayaran").val() === '0') {

                if (type === "persen") {
                    new_diskon_rupiah       = checkDiskonPersen(sub_total, potongan_persen);
                }else if(type === "rupiah") {
                    new_diskon_rupiah       = potongan_rupiah;
                    $('.potongan-persen-modal').val(0);
                }else if(type === 'tunai') {
                    new_diskon_rupiah       = potongan_rupiah;
                }else {
                    new_diskon_rupiah       = potongan_rupiah;
                }          

                calculateModal(sub_total, nilai_tunai, new_diskon_rupiah);

            }else {

                if (type === "persen") {
                    new_diskon_rupiah       = checkDiskonPersen(sub_total, potongan_persen);
                }else if(type === "rupiah") {
                    new_diskon_rupiah       = potongan_rupiah;
                    $('.potongan-persen-modal').val(0);
                }else if(type === 'tunai') {
                    new_diskon_rupiah       = potongan_rupiah;
                }else {
                    new_diskon_rupiah       = potongan_rupiah;
                }                

                sub_total_diskon            = sub_total - new_diskon_rupiah;
                $('.potongan-rupiah-modal').autoNumeric('set', new_diskon_rupiah);
                $('.nilai-pelunasan-modal').autoNumeric('set', sub_total_diskon);

            }

        }

        function checkDiskonPersen(sub_total, potongan_persen) {
            return (sub_total * (potongan_persen / 100));
        }

        function calculateModal(sub_total, nilai_tunai, new_diskon_rupiah) {

            $('.potongan-rupiah-modal').autoNumeric('set', new_diskon_rupiah);

            var nilai_tunai_akhir   = (parseInt(nilai_tunai) + parseInt($(".nilai-debit").autoNumeric('get')) + parseInt($(".nilai-kredit").autoNumeric('get')));

            sub_total_diskon        = sub_total - new_diskon_rupiah;
            nilai_pelunasan         = sub_total_diskon - nilai_tunai;
            nilai_kembali           = nilai_tunai_akhir - sub_total_diskon;

            $('.nilai-pelunasan-modal').autoNumeric('set', sub_total_diskon);

            if (nilai_tunai_akhir < sub_total_diskon) {
                calcNilaiPelunasanModal(nilai_pelunasan);            
                $('.kembali-modal').autoNumeric('set', 0);
                $('.btn-lunas-modal').attr('disabled','disabled');
            }else {
                $('.nilai-pelunasan-akhir-modal').autoNumeric('set', 0);            
                $('.kembali-modal').autoNumeric('set', nilai_kembali);
                $('.btn-lunas-modal').removeAttr('disabled');
            }

        }

        function calcNilaiPelunasanModal(nilai_pelunasan) {
            
            var nilai_kredit          = $(".nilai-kredit").autoNumeric('get');
            var nilai_debit           = $(".nilai-debit").autoNumeric('get');

            var nilai_pelunasan_akhir = nilai_pelunasan - (nilai_kredit + nilai_debit); 

            $('.nilai-pelunasan-akhir-modal').autoNumeric('set', nilai_pelunasan_akhir);
            $('.nilai-pelunasan-debit').autoNumeric('set', nilai_pelunasan_akhir);
        }

        function checkTablePenjualan() {
            if ($(".box-penjualan").find('tr').length > 0) {
                $(".btn-store").removeAttr('disabled');
            }else {
                $(".btn-store").attr('disabled','disabled');
            }
        }

        // Search barang di modal
        function searchBarangInModal() {
            
            $.ajax({
                url: '{{ route('pos.cari.barang.barcode') }}',
                type: 'GET',
                dataType: 'JSON',
                data: {
                    barcode: $("input[name='cari_barang']").val(),
                },
            })
            .done(function(result) {
                if (result.status === false) {

                    $('.box-cari-barang').html(
                        ''
                    );
                    
                    swal({
                        icon: "error",
                        text: result.message
                    });

                }else if (result.status === true) {
                    $('.cari-barang').val('').trigger('change');

                    $('.box-cari-barang').html(
                        result.template
                    );
                }
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });

        }

        function searchBarangInText() {
            
            var pelanggan_id_val = $(".pelanggan_select").val();

            var el = $("input[name='barcode_barang']");

            $('.diskon_barang').val('0');
            var form = $(el).closest('form');

            if ( $(el).val() != '' && $(el).val() != null) {

                $.ajax({
                    url: '{{ route("pos.penjualan.diskon.item") }}',
                    type: 'GET',
                    data: {
                        pelanggan_id : pelanggan_id_val,
                        barcode_barang: $(el).val()
                    },
                    dataType: 'JSON',
                    success: function (result) {
                        if (result.status) {

                            // show qty dan diskon barang
                            $(".div-diskon").show();
                            $(".diskon_barang").removeAttr('disabled');
                        
                            if (result.data.diskon == null || result.data.diskon == 0)  {

                                $('.diskon_barang').val('0');
                                
                                // $('.qty_input').select();
                                // $('.qty_input').focus();

                                if ($("input[name='barcode_barang']").val() != '' && $(el).val() != '' && $("input[name='barcode_barang']").val() != null && $(el).val() != null) {
                                    $.ajax({
                                        url: '{{ route("pos.penjualan.add.item") }}?' + form.serialize(),
                                        type: 'GET',
                                        dataType: 'JSON',
                                        data: {
                                            pelanggan_id : pelanggan_id_val, 
                                            no_penjualan: $('.no_penjualan').val(),
                                            cart_id: cart_id
                                        },
                                        success: function (result) {

                                            if (result.status === true) {
                                                if ( result.total_items_cart > 0 ) {
                                                    $('.tundaBtn').removeClass('disabled');
                                                }
                                                $('.qty_input').val('');

                                                $('.barcode_barang').val('').trigger('change');

                                                $('.total-number').autoNumeric('set', result.total);

                                                $('.total-number').data('total', result.total);

                                                $('.sub-total-modal').autoNumeric('set', result.total);

                                                $('.tunai-modal').autoNumeric('set', result.total);

                                                calculateModal(result.total, $('.tunai-modal').autoNumeric('get'), checkDiskonPersen(result.total, $('.potongan-persen-modal').val()));

                                                $('.total_edc').autoNumeric('set', result.total);

                                                $('.total_edc').data('total', result.total);
                                                if(result.promo != ""){
                                                    $.Notification.notify('error','top left', 'Peringatan Promosi', result.promo);
                                                }

                                                $('.box-penjualan').html(
                                                    result.template
                                                );
                                                
                                                checkTablePenjualan();

                                                setTimeout(function() {
                                                    $('.barcode_barang').focus();
                                                    $('.barcode_barang').typeahead('val', '');
                                                }, 300);
                                            }
                                        }
                                    });
                                }

                            } else {

                                $('.diskon_barang').val(result.data.diskon);
                            
                            }

                        }else if(result.status === false) {

                            swal({
                                icon: "error",
                                text: "Data tidak ditemukan"
                            });

                        }

                    }
                });

            }

        }

        function changeMaskingToNumber(value) {
              if (value) {
                  if (typeof value == "string") {
                      return parseFloat(value.replace(/\,/gi, '').replace('.00', ''));                        
                  }else{
                      return value;
                  }
              }
        }

        function updateItem(el, type) {

            if (type == 'harga') {
              var harga_val                 = $(el).autoNumeric('init').autoNumeric('get');
              var qty_val                   = $(el).closest('tr').find('.qty_detail').autoNumeric('init').autoNumeric('get');
              var diskon_persen_val         = $(el).closest('tr').find('.diskon_persen_detail').autoNumeric('init').autoNumeric('get');
              var diskon_rupiah_val         = $(el).closest('tr').find('.diskon_rupiah_detail').autoNumeric('init').autoNumeric('get');
            }else if(type == 'qty') {
              var harga_val                 = $(el).closest('tr').find('.harga_detail').autoNumeric('init').autoNumeric('get');
              var qty_val                   = $(el).autoNumeric('init').autoNumeric('get');
              var diskon_persen_val         = $(el).closest('tr').find('.diskon_persen_detail').autoNumeric('init').autoNumeric('get');
              var diskon_rupiah_val         = $(el).closest('tr').find('.diskon_rupiah_detail').autoNumeric('init').autoNumeric('get');
            }else if (type == 'diskon_persen') {
              var qty_val                   = $(el).closest('tr').find('.qty_detail').autoNumeric('init').autoNumeric('get');
              var harga_val                 = $(el).closest('tr').find('.harga_detail').autoNumeric('init').autoNumeric('get');
              var diskon_persen_val         = $(el).autoNumeric('init').autoNumeric('get');
              var diskon_rupiah_val         = $(el).closest('tr').find('.diskon_rupiah_detail').autoNumeric('init').autoNumeric('get');
            }else if (type == 'diskon_rupiah') {
              var qty_val                   = $(el).closest('tr').find('.qty_detail').autoNumeric('init').autoNumeric('get');
              var harga_val                 = $(el).closest('tr').find('.harga_detail').autoNumeric('init').autoNumeric('get');
              var diskon_persen_val         = $(el).closest('tr').find('.diskon_persen_detail').autoNumeric('init').autoNumeric('get');
              var diskon_rupiah_val         = $(el).autoNumeric('init').autoNumeric('get');
            }

            // var
            var id_row          = $(el).closest('tr').data('id');

            // Kondisi
            if (harga_val !== '' || qty_val !== '') {
                $.ajax({
                    url: '{{ route("pos.update.harga_jual_pos") }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id              : id_row,
                        harga           : harga_val,
                        qty             : qty_val,
                        diskon_persen   : diskon_persen_val,
                        diskon_rupiah   : diskon_rupiah_val,
                        cart_id: cart_id
                    },
                    success: function (result) {
                        if (result.status) {

                            // tooltip
                            $(el).closest('tr').find(".qty_edit_hidden")
                                               .removeAttr('data-toggle')
                                               .removeAttr('data-placement')
                                               .removeAttr('title')
                            $(el).closest('tr').find('.qty_edit_hidden').tooltip('hide');
                            $('.box-penjualan').html(
                                result.template
                            );

                            // $(el).val(null).trigger('change');

                            $('.total-number').autoNumeric('set', Number(result.total));

                            $('.total-number').data('total', result.total);

                            $('.sub-total-modal').autoNumeric('set', result.total);

                            $('.tunai-modal').autoNumeric('set', result.total);

                            calculateModal(result.total, $('.tunai-modal').autoNumeric('get'), checkDiskonPersen(result.total, $('.potongan-persen-modal').val()));

                            $('.total_edc').autoNumeric('set', result.total);

                            $('.total_edc').data('total', result.total);
                        }

                        setTimeout(function() { $('.barcode_barang').focus() }, 300);
                        
                    }
                });
            }else {
                swal({
                    icon: "error",
                    text: "Kolom Harga Tidak Boleh Kosong",
                });
            }
        }

        $('#save-cart').on('show.bs.modal', function (e) {
            setTimeout(() => {$('#cartdesc').focus();}, 500);
        });

        // menyimpan keterangan cart
        $('#savetochart').on('click', function() {
            $('#close-save-cart').trigger('click');
            const desc = $('#cartdesc').val();

            if(desc == ''){
                $('.tundaBtn').removeClass('disabled');
                swal('Error!', 'Tolong isi deskripsi cart', 'error');
            }else{
                $.ajax({
                    url: '{{ route('pos.savetocart') }}',
                    type: 'POST',
                    data: {
                        desc: desc, 
                        cart_id: cart_id,
                        _token: token
                    },
                    success: function (results) {
                        if(results.success){
                            $('#cartdesc').val('').trigger('change');
                            $('.tundaBtn').addClass('disabled');
                            $('.barcode_barang').val('').trigger('change');
                            $('.total-number').autoNumeric('set', 0); 
                            $('.total-number').data('total', 0);
                            $('.sub-total-modal').autoNumeric('set', 0);
                            $('#list-item').empty();
                            $('#cart-list').html(results.cart_list);
                            cart_id = results.next_cart_id;
                            HapusPembayaran();
                        }
                        setTimeout(() => {
                        }, 1000);
                    }
                });
                $('.tundaBtn').addClass('disabled');
            }
        });

        const removeFromCart = (id) => {
            swal({
                title: "Anda Yakin?",
                text: "setelah dihapus data tidak dapat dikembalikan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '{{ route('pos.deletefromcart') }}',
                        type: 'POST',
                        data: {cart_id: id, _token: token, _method: 'DELETE'},
                        success: (results) => {
                            if(results.success == true){
                                $('.tundaBtn').addClass('disabled');
                                $('#final_price').html('0');
                                swal(results.message, {
                                    icon: "success",
                                });
                                $('.barcode_barang').val('').trigger('change');
                                $('.total-number').autoNumeric('set', 0); 
                                $('.total-number').data('total', 0);
                                $('.sub-total-modal').autoNumeric('set', 0);
                                $('#list-item').empty();
                                $('#cart-list').html(results.cart_list);
                                HapusPembayaran();
                            }
                        }
                    });
                }
            });
        }

        const getFromCart = (id) => {
            $('#list-item').empty();
            $.ajax({
                url: '{{ route('pos.getfromcart') }}',
                type: 'GET',
                data: {
                    cart_id: id
                },
                success: (results) => {
                    if(results.success == true){
                        $('.tundaBtn').removeClass('disabled');
                        cart_id = id;
                        $('.barcode_barang').val('').trigger('change');
                        $('.total-number').autoNumeric('set', results.cart_final_price);
                        $('.total-number').data('total', results.cart_final_price);
                        $('.sub-total-modal').autoNumeric('set', results.cart_final_price);
                        $('#list-item').html(results.cart_penjualan);
                        $('#cart-list').html(results.cart_list);
                    }
                }
            });
        }

        setInterval(() => {
            $('#cart-option-'+cart_id).fadeOut();
        }, 500);
        

        $('.tundaBtn').on('click', function(e) {
            $.ajax({
                url: '{{ route('pos.checkChartList') }}',
                type: 'GET',
                data: {
                    cart_id: cart_id
                },
                success: (results) => {
                    if(results.success == true && results.description != null){
                        let desc = results.description;
                        $.ajax({
                            url: '{{ route('pos.savetocart') }}',
                            type: 'POST',
                            data: {
                                desc: desc, 
                                cart_id: cart_id,
                                _token: token
                            },
                            success: function (results) {
                                if(results.success){
                                    $('#list-item').empty();
                                    $('#cart-list').html(results.cart_list);
                                    cart_id = results.next_cart_id;
                                    $('.barcode_barang').val('').trigger('change');
                                    $('.total-number').autoNumeric('set', 0); 
                                    $('.total-number').data('total', 0);
                                    $('.sub-total-modal').autoNumeric('set', 0);
                                    HapusPembayaran();
                                }
                                setTimeout(() => {
                                }, 1000);
                            }
                        });
                    } else if ( results.success == false ) {
                        $('#save-cart').modal({
                            backdrop: 'static',
                            keyboard: false, 
                            show: true
                        });
                    }
                }
            });
            $('.tundaBtn').addClass('disabled');
        });

        function HapusPembayaran() {
            $('.potongan-persen-modal').val(0);
            $('.potongan-rupiah-modal').autoNumeric('set', 0);
            $('.sub-total-modal').autoNumeric('set', 0);
            $('.nilai-pelunasan-akhir-modal').autoNumeric('set', 0);
            $('.nilai-pelunasan-modal').autoNumeric('set', 0);
            $('.tunai-modal').autoNumeric('set', 0);
            $('.kembali-modal').autoNumeric('set', 0);
        }

        $('.batal-antrian').click( function() {
            $('.tundaBtn').removeClass('disabled');
        });

        // Pencarian dalam modal antrian
        $('#CariPanggil').keyup(function(){
            search_table($(this).val());  
        }); 

        function search_table(value){  
            $('#tableCartList tr').each(function(){  
                var ketemu = 'false';  
                $(this).each(function(){  
                    if($(this).text().toLowerCase().indexOf(value.toLowerCase()) >= 0) {  
                        ketemu = 'true';  
                    }  
                });  
                if( ketemu == 'true') {  
                    $(this).show();  
                }  
                else {  
                    $(this).hide();  
                }  
            });  
        }
    </script>

    <script type="text/template" id="template-alert">
        <div>
            <div class="container">
                <div class="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p class="alert-message" style="text-align: center; font-size: 20px;"></p>
                    <p class="alert-exchange"></p>
                </div>
            </div>
        </div>
    </script>

@endsection