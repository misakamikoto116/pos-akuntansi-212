<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Struk Belanja</title>
 <!-- Normalize or reset CSS with your favorite library -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
<link href="https://fonts.googleapis.com/css?family=Share+Tech+Mono" rel="stylesheet">
  <style>
    /* @page { size: 80mm } output size */
    /* bodsy.receipt .sheet { font-family: 'Share Tech Mono', monospace; width: 58mm; } */
    /* body.receipt .sheet { font-family: 'Sans', regular; width: 80mm; } */
    /* @media print { body.receipt { width: 80mm } } fix for Chrome */
    .head_nota {
        text-align: center;
        margin-top: 1em;
    }
    h1     { font-size: 8pt;padding-top: 0; margin-top: 0}
    .list_barang {
        margin:1mm;
        font-size: 8pt;
        width:100%;
        text-align: left;
    }
    table td, table td * {
        vertical-align: top;
    }
    .pull-right{
        text-align:right;
        padding-right:1mm;
    }
  </style>
</head>

<body class="receipt">
    <section class="sheet padding-10mm" style="padding: 5mm">

        <img style="margin-left:5mm" width="80%" src="" />
        
        <div class="head_nota">
            <h1>212 Mart</h1>    
        </div>

        <hr>

        <table class="list_barang">   
            @foreach ( $cart as $item )
                <tr>
                    <td width="50%">{{ $item['name'] }}</td>
                    <td width="10%">{{ $item['qty'] }}</td>
                    <td width="20%">{{ $item['price'] }}</td>
                    <td width="20%" style="text-align: right">{{ $item['price'] * $item['qty'] }}</td>
                </tr>  
            @endforeach
        </table>

        <hr>

        <table class="list_barang">
            <tr>
                <td width="70%" style="text-align: right">Harga Jual</td>
                <td> : </td>
                <td style="text-align: right">{{ $harga_jual ?? 0 }}</td>
            </tr>

            @foreach ( $cart as $item )

                @if ($item['options']['produk']->diskon !== null && $item['options']['produk']->diskon > 0)
                    <tr>
                        <td width="70%" style="text-align: right">{{ $item['name'] }}</td>
                        <td> : </td>
                        <td style="text-align: right">{{ $item['price'] - $item['options']['price_real'] }}</td>
                    </tr> 
                @endif
            @endforeach

            @if ( $diskon) !== null && $diskon > 0 )
                <tr>
                    <td width="70%" style="text-align: right">Diskon</td>
                    <td> : </td>
                    <td style="text-align: right">{{ $diskon ?? 0 }}</td>
                </tr>
            @endif

        </table>

        <hr>

        <table class="list_barang">
            <tr>
                <td width="70%" style="text-align: right">Total</td>
                <td> : </td>
                <td style="text-align: right">{{ $total ?? 0 }}</td>
            </tr>

            <tr>
                <td width="70%" style="text-align: right">Tunai</td>
                <td> : </td>
                <td style="text-align: right">{{ $tunai ?? 0 }}</td>
            </tr>

            <tr>
                <td width="70%" style="text-align: right">Kembali</td>
                <td> : </td>
                <td style="text-align: right">{{ $kembali ?? 0 }}</td>
            </tr>
        </table>
        
        <em style="font-size:8pt">Kasir : {{ $kasir ?? '-' }}</em> 
        
        <br/>
        
        <em style="font-size:8pt">Tgl/jam : {{ $tanggal ?? '-' }}</em>
        
        <div class="head_nota"><h1>Terima Kasih</h1></div>
        
        <hr/>

    </section>
</body>
</html>