<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-us36{border-color:inherit;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
.tgs {
  padding:0 !important;
  border:0 #fff !important;
  height: 0 !important;
  line-height: 0;
}
.tgs-border {
  border-top-width: 1px !important;
}
</style>
<table class="tg">

  <thead>
    <tr>
      <th class="tg-us36">No</th>
      <th class="tg-us36">Nama Barang</th>
      <th class="tg-us36">Qty</th>
      <th class="tg-us36">Harga</th>
      <th class="tg-us36">Total Harga</th>
      <th class="tg-us36">Tanggal</th>
      <th class="tg-us36">Jam</th>
      <th class="tg-us36">Petugas</th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <td colspan="6" class="tgs"></td>
      <td rowspan="4" class="tgs-border">2</td>
      <td rowspan="4" class="tgs-border">5</td>
    </tr>
    <tr>
      <td rowspan="3">5</td>
    </tr>

    <tr>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
    

  </tbody>

  

  @foreach ($transactions as $transaction)
    <tr>
      <td rowspan="{{ count($transaction->barang) + 1}}" class="tg-yw4l">{{ $loop->iteration }}</td>      
    </tr>       
    @foreach ($transaction->barang as $barang)
      <tr> 
        <td>{{ $barang->produk->keterangan }}</td>
        <td>{{ $barang->jumlah }}</td>
        <td>{{ $barang->unit_price }}</td>
        <td>{{ $barang->harga_final }}</td>      
        <td>{{ $barang->created_at->format('d-m-Y') }}</td>
        <td>{{ $barang->created_at->format('h:i:s') }}</td>
        <td>{{ auth()->user()->name }}</td>
      </tr>  
    @endforeach    
  @endforeach
</table>