<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Check Out Kasir</title>
 <!-- Normalize or reset CSS with your favorite library -->
 {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"> --}}

<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
<link href="https://fonts.googleapis.com/css?family=Share+Tech+Mono" rel="stylesheet">
  <style>
    /* @page { size: 80mm } output size */
    /* bodsy.receipt .sheet { font-family: 'Share Tech Mono', monospace; width: 58mm; } */
    /* body.receipt .sheet { font-family: 'Sans', regular; width: 80mm; } */
    /* @media print { body.receipt { width: 80mm } } fix for Chrome */
    .head_nota {
        text-align: center;
        margin-top: 0em;
    }
    h1     { font-size: 8pt;padding-top: 0; margin-top: 0}
    .list_barang {
        margin:1mm;
        font-size: 8pt;
        width:100%;
        text-align: left;
    }
    table td, table td * {
        vertical-align: top;
    }
    .pull-right{
        text-align:right;
        padding-right:1mm;
    }
  </style>
</head>

<body class="receipt">
    <section class="sheet padding-10mm" style="padding: 5mm">

        <img style="margin-left:15mm" width="50%" src="{{ public_path('/assets/images/212-mart-logo-black.png') }}" />
        
        <div class="head_nota">    
            <h1>Tanda Terima Rekap Kasir</h1>
        </div>

        <hr>

        <em style="font-size:8pt">Kasir : {{ $kasir ?? null }}</em> 
        
        <br/>
        
        <em style="font-size:8pt">Tgl/jam  : {{ $tanggal_cek_out ?? null }}</em>

        <br/>
        
        <em style="font-size:8pt">Kasir Mesin  : {{ $kasir_mesin ?? null }}</em>

        <hr>

        <table class="list_barang">
            <tr>
                <td width="70%" style="text-align: right">Modal Awal</td>
                <td> : </td>
                <td style="text-align: right">{{ $modal_awal }}</td>
            </tr>

            <tr>
                <td width="70%" style="text-align: right">Uang Sistem</td>
                <td> : </td>
                <td style="text-align: right">{{ $uang_sistem }}</td>
            </tr> 

            <tr>
                <td width="70%" style="text-align: right">Total Uang Sistem</td>
                <td> : </td>
                <td style="text-align: right">{{ $total_sistem }}</td>
            </tr>

            <tr>
                <td width="70%" style="text-align: right">Uang Fisik</td>
                <td> : </td>
                <td style="text-align: right">{{ $uang_fisik }}</td>
            </tr>

            <tr>
                <td width="70%" style="text-align: right">EDC</td>
                <td> : </td>
                <td style="text-align: right">{{ $edc }}</td>
            </tr>

            <tr>
                <td width="70%" style="text-align: right">Hutang</td>
                <td> : </td>
                <td style="text-align: right">{{ $hutang }}</td>
            </tr>

            <tr>
                <td width="70%" style="text-align: right">Selisih</td>
                <td> : </td>
                <td style="text-align: right">{{ $selisih }}</td>
            </tr>
        </table>
        
        <hr>

        <em style="font-size:8pt">Setoran  : {{ $uang_setoran ?? null }}</em>

        <table width="100%">
            <tr style="font-size:8pt">
                <td width="40%">Kasir</td>
                <td width="20%">&nbsp;</td>
                <td width="40%">Kepala Toko</td>
            </tr>
            <tr>
                <td colspan=3>&nbsp;</td>
            </tr>
            <tr style="font-size:8pt">
                <td width="40%">{{ $kasir ?? null }}</td>
                <td width="20%">&nbsp;</td>
                <td width="40%">{{ $kepalaToko ?? '..........' }}</td>
            </tr>
        </table>
        <br>
    </section>
</body>
</html>