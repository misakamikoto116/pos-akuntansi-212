@extends('chichi_theme.layout.app')

@section('custom_css')
    <style>
        .btn-xs {
            padding: 1px 5px;
            font-size: 10px;
            line-height: 1.5;
            border-radius: 3px;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">Kasir</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Check In</a></li>
            </ol>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">Check In</h5>
                    <div style="position: absolute;right: 10px;top: 50%;transform: translateY(-50%);">
                        <a href="{{ route('pos.reset.kasir') }}" class="btn btn-info btn-rounded waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-refresh"></i></span>
                            Reset Kasir
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3" style="margin-left: auto; margin-right: auto">
                            {!! Form::open(['route' => 'pos.penjualan.kasir.check.in.store', 'method' => 'post',]) !!}
                                {!! Form::hidden('check_state','IN',['id'=>'cek_state']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="kasir_mesin">PC Kasir <span class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        {{ Form::select('kasir_mesin_id', [], null, ['id' => 'kasir_mesin','class'=>'form-control', 'placeholder' => 'PC Kasir' ,'required'])  }}
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="kasir_mesin">Modal</label>
                                    <div class="col-sm-9">
                                        <label class="col-form-label">{{ number_format($preferensi_pos->uang_modal ?? 0) }}</label>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button class="btn btn-warning waves-effect waves-light w-lg btn-check-in" type="submit">
                                        <span>Cek In</span>
                                        <i class="fa fa-money m-l-5"></i>
                                    </button> 
                                </div>   
                            {!! Form::close() !!}     
                        </div>
                    </div>
                       

                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom_js')
    <script>
        jQuery(document).ready(function (e) {

            $('.modal_input').autoNumeric('init', {
                aPad: false
            });

            $('body').on('click', '.btn-check-in', function (e) {
                e.preventDefault();
                
                var el = $(this);

                var form = $(el).closest('form');

                var checkVal = $(form)[0].checkValidity();

                if(checkVal) {
                   $(form).submit();
                } else {
                    $(form)[0].reportValidity();
                }

            });

            $('#kasir_mesin').select2({
                placeholder: 'Cari Mesin Kasir',
                width: '100%',
                allowClear: false,
                ajax: {
                    url: '{{ route("pos.get.kasir.mesin") }}',
                    dataType: 'JSON',
                    type: 'GET',
                    data: function (params) {
                        return {
                            search: params.term, // search term
                        };
                    },
                    cache: true
                },
                templateResult: function (state) {

                    if ( ! state.id) {
                        return state.text;
                    }
                    
                    var $state = $(
                        '<div style="font-size: 14px"> ' + state.text + '</div>'
                    );


                    return $state;
                },
                minimumInputLength: 3
            });

        });

    </script>
@endsection