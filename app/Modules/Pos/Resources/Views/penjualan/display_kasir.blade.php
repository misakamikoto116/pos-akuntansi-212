<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Display Kasir</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Dashaboard 212 Pos">
    <meta name="author" content="Thortech Asia">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon_1.ico') }}">
    <meta name="robots" content="noindex" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tambahan css -->
    <!-- App css -->
    <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/sevent_seg_fonts/specimen_files/specimen_stylesheet.css') }}" type="text/css" charset="utf-8" />
    <link rel="stylesheet" href="{{ asset('assets/css/sevent_seg_fonts/stylesheet.css') }}" type="text/css" charset="utf-8" />
    <script>
        var BASE_URL = '{{ url("/") }}/';
    </script>
    <style>
        /* custom web font */
        .text-display {
            font-family: 'dseg7_modernregular';
            font-size: 28pt;
            font-weight: bolder;
            line-height: 10px;
        }
        .wrapper-page {
            margin: 5px auto;
            position: relative;
            /*width: 420px;*/
        }
        .card-box {
            padding: 20px;
            border: 1px solid rgba(54, 64, 74, 0.05);
            border-radius: 5px;
            margin-bottom: 5px;
            background-clip: padding-box;
            background-color: #000000;
        }
    </style>
</head>
<body style="height: 100%">



<div class="wrapper-page" style="width: 95%">
    <div class=" card-box">

        <div id="display-beli" class="p-20">

        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 m-t-sm-40">
            <!-- START carousel-->
            <div id="carouselExample" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExample" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExample" data-slide-to="1"></li>
                    <li data-target="#carouselExample" data-slide-to="2"></li>
                </ol>
                <div style="height: 100%" class="carousel-inner" role="listbox">
                    @foreach($iklan as $row)

                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                        <img style="height: 850px; width: 100%;" class="d-block img-fluid" src="{!! asset('storage/iklan/'.$row->image_path) !!}" alt="{{ $row->nama_iklan }}" />
                    </div>

                    @endforeach


                </div>
            </div>
            <!-- END carousel-->
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-center">

        </div>
    </div>

</div>
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/tether.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/tether.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{ asset('assets/plugins/notifyjs/js/notify.js') }}"></script>
<script src="{{ asset('assets/plugins/notifications/notify-metro.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>


<script>
    
    // var client = io('{{ env('SOCKET_URL', 'http://localhost:3500/') }}',{
    //   path: '/node/socket.io',
    //   // namespace: '/node/',
    //   // secure:location.protocol === 'https:',
    //   // rejectUnauthorized : false
    // });
    // client.on('connect',() =>{
    //   console.log(`connected client : ${client.id}`);
    //   client.on('ping',()=>{
    //     console.log(client.id,'pong');
    //   })
    //   client.on("{!! 'kasir_'.auth()->user()->id !!}", function (data) {
    //       console.log(data);
    //       if(data.status === 'kembalian') {
    //           insertKembalian(data.kembalian);
    //       }else { 
    //           insertBelanja(data.barang, data.price, data.qty, data.subtotal, data.total);
    //       }
    //   });
    // });

    // function formatDuit(nilai){
    //     return nilai.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    // }
    // function insertBelanja(nama_barang, harga_barang,qty, sub_total, total_all) {
    //     $('#display-beli').html('');
    //     $('#display-beli')
    //         .html('<h4 style="color:#76ff03;letter-spacing: 3px; font-size: 14pt;" class="m-0 counter font-600"><small style="color:#76ff03;letter-spacing: 3px; font-size: 14pt;">'+nama_barang+'</small><br/><br/> \n' +
    //             '<br><span class="text-display">'+formatDuit(harga_barang)+'</span> x '+qty+' <span style="color:#76ff03;letter-spacing: 3px;" class="pull-right text-display">'+formatDuit(sub_total)+'</span></h4>\n' +
    //             '<br><br><h4 style="color:#76ff03;letter-spacing: 3px;" class="m-0 pull-right counter font-600 text-display">'+ formatDuit(total_all) +'</h4>');
    // }

    // function insertKembalian(kembalian) {
    //     $('#display-beli').html('');
    //     $('#display-beli').html('<h4 style="color: #76ff03; letter-spacing: 3px; font-size: 40pt; text-align: center; font-weight: 100; " class="m-0 counter">KEMBALIAN</h4>'+
    //                             '<h4 style="color: #76ff03; letter-spacing: 3px; font-size: 40pt; text-align: center; padding-top: 40px; " class="m-0 counter font-600">'+ kembalian +'</h4>');
    // }

    @if (env('APP_ENV') !== 'production')
        var sock = io("{{ env('PUBLISHER_URL') }}:{{ env('PORT_SERVER') }}", {
            path: '/node/socket.io',
        });
    @else
        var sock = io("{{ env('PUBLISHER_URL') }}", {
            path: '/node/socket.io',
        });
    @endif


    sock.on('display-kasir:App\\Events\\DisplayKasirEvent', function (data) {

        var kasir_id = "{!! 'kasir_'.auth()->user()->id !!}";

        if (kasir_id == data.kasirId) {
            if(data.dataToDisplay.status === 'kembalian') {
                insertKembalian(data.dataToDisplay.kembalian);
            }else { 
                insertBelanja(data.dataToDisplay.barang, 
                              data.dataToDisplay.price, 
                              data.dataToDisplay.qty, 
                              data.dataToDisplay.subtotal, 
                              data.dataToDisplay.total);
            }
        }
    });

    function formatDuit(nilai){
        return nilai.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    function insertBelanja(nama_barang, harga_barang,qty, sub_total, total_all) {
        $('#display-beli').html('');
        $('#display-beli')
            .html('<h4 style="color:#76ff03;letter-spacing: 3px; font-size: 14pt;" class="m-0 counter font-600"><small style="color:#76ff03;letter-spacing: 3px; font-size: 14pt;">'+nama_barang+'</small><br/><br/> \n' +
                '<br><span class="text-display">'+formatDuit(harga_barang)+'</span> x '+qty+' <span style="color:#76ff03;letter-spacing: 3px;" class="pull-right text-display">'+formatDuit(sub_total)+'</span></h4>\n' +
                '<br><br><h4 style="color:#76ff03;letter-spacing: 3px;" class="m-0 pull-right counter font-600 text-display">'+ formatDuit(total_all) +'</h4>');
    }

    function insertKembalian(kembalian) {
        $('#display-beli').html('');
        $('#display-beli').html('<h4 style="color: #76ff03; letter-spacing: 3px; font-size: 40pt; text-align: center; font-weight: 100; " class="m-0 counter">KEMBALIAN</h4>'+
                                '<h4 style="color: #76ff03; letter-spacing: 3px; font-size: 40pt; text-align: center; padding-top: 40px; " class="m-0 counter font-600">'+ kembalian +'</h4>');
    }

    

</script>
</body>
</html>