@foreach ($produks as $produk)
	<tr>
	    <td>{{ $produk['no_barang'] ?? 'Kosong' }}</td>
	    <td>{{ $produk['barcode'] ?? 'Kosong' }}</td>
	    <td>{{ $produk['nama_barang'] ?? 'Kosong' }}</td>
	    <td>{{ number_format($produk['harga_jual']) ?? 'Kosong' }}</td>
	    <td>{{ $produk['qty'] ?? 'Kosong' }}</td>
	</tr>
@endforeach