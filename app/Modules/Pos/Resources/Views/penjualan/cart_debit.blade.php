@foreach (Cart::instance('edc-debit-' . auth()->user()->id)->content()->sortByDesc('options.id') as $cart)
	<tr>
	  <td>{{ $cart->options->no_kartu }}</td>
	  {!! Form::hidden('type_edc_id', $cart->options->type_edc_id) !!}
	  <td>{{ $cart->options->batch }}</td>
	  <td style="text-align: right;">{{ number_format($cart->options->pembayaran_debit) }}</td>
	  <td>
	  	<a href="#" data-id="{{ $cart->rowId }}" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-xs btn-remove-debit"> 
	  	  <i class="fa fa-trash"></i> 
	  	</a>
	  </td>
	</tr>
@endforeach