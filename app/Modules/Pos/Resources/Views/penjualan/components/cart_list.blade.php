<div class="col-3 col-form-label">Cari</div>
<div class="col-9">
    {!! Form::text('', null, ['class' => 'form-control', 'id' => 'CariPanggil', 'placeholder' => 'Cari Deskripsi']) !!}
</div>
<div class="table-responsive" style="padding-top: 10px;">
    <table class="table" id="tableCartList">
        <thead class="theadCartList">
            <tr>
                <th>No.</th>
                <th>Deskripsi</th>
                <th>Total</th>
                <th class="text-right">Option</th>
            </tr>
        </thead>
        <tbody class="tbodyPanggil tbodyCartList">
            @foreach ($carts as $cartItem)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{!! $cartItem->description ?? 'Antrian Terakhir' !!}</td>
                    <td>{!! number_format($cartItem->items->sum('price_final'), 2) !!}</td>
                    <td>
                        <div id="cart-option-{{ $cartItem->cart_id }}">
                            <button class="btn btn-success" onclick="getFromCart('{!! $cartItem->cart_id !!}')"><i class="fa fa-cart-arrow-down"></i></button>
                            -
                        </div>
                        <button class="btn btn-danger text-right" onclick="removeFromCart('{!! $cartItem->cart_id !!}')"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
