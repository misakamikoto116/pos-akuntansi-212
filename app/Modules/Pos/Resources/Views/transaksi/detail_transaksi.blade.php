@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">{{ $title }}</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">{{ $title }}</a></li>
            </ol>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">{{ $title }}</h5>
                    
                </div>
                <div class="card-body">
                    <div class="col-sm-13">
                        <a href="#" class="btn btn-default btn-print waves-effect waves-light" style="float: right;">Print</a>
                        <div class="row form-group col-sm-6">
                            <label class="col-sm-3 col-form-label">Tanggal</label>
                            <div class="col-sm-8">
                                <label class="col-form-label">: {{ $tanggal }}</label>
                            </div>
                        </div>
                        <div class="row form-group col-sm-6">
                            <label class="col-sm-3 col-form-label">Kode Transaksi</label>
                            <div class="col-sm-9">
                                <label class="col-form-label">: {{ $kode_transaksi }}</label>
                            </div>
                        </div>
                    </div>
                    @if($items->isEmpty())
                        <div class="alert alert-warning"> Tidak ada data. </div>
                    @else
                    <table class="table">
                        <thead>
                            <th>No</th>
                            <th>Nomor Barang</th>
                            <th>Nama Barang</th>
                            <th>Satuan</th>
                            <th style="text-align: center;">Kuantitas</th>
                            <th style="text-align: center;">Harga Satuan</th>
                            <th style="text-align: center;">Perubahan Harga Jual</th>
                            <th style="text-align: center;">Total Harga Jual</th>
                            <th style="text-align: center;">Diskon</th>
                        </thead>

                        <tbody>
                            @php
                                $sumKuantitas   =   0; $sumUnitPrice   =   0; $sumHargaFinal  =   0; $sumTotalHarga  =   0;
                            @endphp
                            @foreach ($items as $item)
                            @php
                                $unitPrice  = $item->unit_price !== null ? $item->unit_price : $item->harga;
                                $hargaFinal = $item->unit_price != $item->harga_final ? $item->harga_final : $item->unit_price;
                                $TotalHarga = $item->unit_price != $item->harga_final ? $item->harga_final * $item->jumlah : $item->unit_price * $item->jumlah;
                            @endphp
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->produk->no_barang }}</td>
                                    <td>{{ $item->item_deskripsi }}</td>
                                    <td>{{ $item->item_unit ?? $item->satuan }}</td>
                                    <td style="text-align: center;">{{ $item->jumlah }}</td>
                                    <td style="text-align: right">{{ number_format($unitPrice) }}</td>
                                    <td style="text-align: right">{{ number_format($hargaFinal) }}</td>
                                    <td style="text-align: right">{{ number_format($TotalHarga) }}</td>
                                    <td style="text-align: center;">{{ $item->diskon }}</td>
                                </tr>
                                @php
                                    $sumKuantitas   +=   $item->jumlah; $sumUnitPrice   +=   $unitPrice;
                                    $sumHargaFinal  +=   $hargaFinal;   $sumTotalHarga  +=   $TotalHarga;
                                @endphp
                            @endforeach
                            <tr style="border-top:2px solid black;">
                                <td colspan="3"></td>
                                <td style="text-align: right;">Total :</td>
                                <td style="text-align: center;">{{ $sumKuantitas }}</td>
                                <td style="text-align: right">{{ number_format($sumUnitPrice) }}</td>
                                <td style="text-align: right">{{ number_format($sumHargaFinal) }}</td>
                                <td style="text-align: right">{{ number_format($sumTotalHarga) }}</td>
                                <td style="text-align: center;"></td>
                            </tr>
                        </tbody>
                    </table>
                    @endif    
                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom_js')
    <script>
        jQuery(document).ready(function (e) {
            $('.btn-print').on('click', function () {
                $.ajax({
                     url: '{{ route('pos.cetak.struk.detail') }}',
                     type: 'POST',
                     dataType: 'JSON',
                     data: {
                        faktur_id: '{{ $id }}',
                    },
                 })
                 .done(function(result) {
                    if ($.isFunction(window.printStruck)) {
                        window.printStruck(result);
                    }
                 })
                 .fail(function() {
                     console.log("error");
                 })
                 .always(function() {
                     console.log("complete");
                 });
                  
            });
        });
    </script>
@endsection