@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')
    <div class="row">
        @include('chichi_theme.layout.filter')
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">{{ $title }}</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">{{ $title }}</a></li>
            </ol>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">{{ $title }}</h5>
                </div>
                <div class="card-body">
                    <div class="row form-group col-sm-6">
                        <label class="col-sm-3 col-form-label">Nama Kasir</label>
                        <div class="col-sm-9">
                            <label class="col-form-label">: {{ ucwords($nama_kasir) }}</label>
                        </div>
                    </div>
                    <div class="row form-group col-sm-6">
                        <label class="col-sm-3 col-form-label">Tanggal</label>
                        <div class="col-sm-9">
                            <label class="col-form-label">: {{ $tanggal }}</label>
                        </div>
                    </div>
                    <div class="row form-group col-sm-6">
                        <label class="col-sm-3 col-form-label">Total</label>
                        <div class="col-sm-9">
                            <label class="col-form-label">: Rp. {{ number_format($total) }}</label>
                        </div>
                    </div>
                    @if($items->isEmpty())
                        <div class="alert alert-warning"> Tidak ada data. </div>
                    @else
                    <table class="table">
                        <thead>
                            <th>No</th>
                            <th>Tanggal & Jam</th>
                            <th>Kode Transaksi</th>
                            <th>Kasir Mesin</th>
                            <th style="text-align: center;">Jenis Pembayaran</th>
                            <th style="text-align: right">Total Transaksi</th>
                            <th style="text-align: right">Dibayar</th>
                            <th style="text-align: right">Kembalian</th>
                            <th style="text-align: center;">Option</th>
                        </thead>

                        <tbody>
                            @php
                                $sumTransaksiTotal      = 0; $sumTransaksiDibayar    = 0; $sumTransaksiKembalian  = 0;
                            @endphp
                            @foreach ($items as $item)
                                @php
                                    $dibayar    = $item->riwayatPembayaran !== null ? $item->riwayatPembayaran->dibayar : 0;
                                    $kembalian  = $item->riwayatPembayaran !== null ? $item->riwayatPembayaran->kembalian : 0;
                                @endphp
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ Carbon\Carbon::parse($item->invoice_date)->format('d F Y - H:i:s') }}</td>
                                    <td>{{ $item->no_faktur }}</td>
                                    <td>{{ $item->kasirMesin->nama }}</td>
                                    <td style="text-align: center;">{!! $item->status_penjualan_pos !!}</td>
                                    <td style="text-align: right">{{ number_format($item->sum_harga_total) ?? 0 }}</td>
                                    <td style="text-align: right">{{ number_format($dibayar) }}</td>
                                    <td style="text-align: right">{{ number_format($kembalian) }}</td>
                                    <td style="text-align: center;">
                                        <div class="btn-group">
                                            <button id="option{{$item->id}}" type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                            <div class="dropdown-menu">
                                                <a href="{{ route('pos.penjualan.kasir.detail-transaksi',[$item->id, 'faktur']) }}" class="dropdown-item jurnal">Detail</a>
                                                <a href="#" class="dropdown-item edit" onclick="cetakStruk({{ $item->id }})">Print</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @if ($item->returPenjualan->isNotEmpty())
                                    @foreach ($item->returPenjualan as $item_retur)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ Carbon\Carbon::parse($item_retur->tanggal)->format('d F Y - H:i:s') }}</td>
                                            <td>{{ $item_retur->sr_no }}</td>
                                            <td>{{ $item->kasirMesin->nama }}</td>
                                            <td style="text-align: center;"><label class='label label-danger'>Retur</label></td>
                                            <td style="text-align: right">{{ number_format($item_retur->total) ?? 0 }}</td>
                                            <td style="text-align: right">0</td>
                                            <td style="text-align: right">0</td>
                                            <td style="text-align: center;">
                                                <div class="btn-group">
                                                    <button id="option{{$item->id}}" type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                                                    <div class="dropdown-menu">
                                                        <a href="{{ route('pos.penjualan.kasir.detail-transaksi',[$item_retur->id, 'retur']) }}" class="dropdown-item jurnal">Detail</a>
                                                        <a href="#" class="dropdown-item edit" onclick="cetakStruk({{ $item->id }})">Print</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                @php
                                    if($item->returPenjualan->isNotEmpty()){
                                        $sumRetur               =  $item->returPenjualan->sum('total');
                                        $sumTransaksiTotal     += $item->sum_harga_total;
                                        $sumTransaksiTotal      =  $sumTransaksiTotal - $sumRetur;
                                        $sumTransaksiDibayar    += $dibayar;
                                        $sumTransaksiKembalian  += $kembalian;
                                    }else{
                                        $sumTransaksiTotal      += $item->sum_harga_total;
                                        $sumTransaksiDibayar    += $dibayar;
                                        $sumTransaksiKembalian  += $kembalian;
                                    }
                                @endphp
                            @endforeach
                            <tr style="border-top:2px solid black;">
                                <td colspan="4"></td>
                                <td style="text-align: right;">Total :</td>
                                <td style="text-align: right">{{ number_format($sumTransaksiTotal) }}</td>
                                <td style="text-align: right">{{ number_format($sumTransaksiDibayar) }}</td>
                                <td style="text-align: right">{{ number_format($sumTransaksiKembalian) }}</td>
                                <td style="text-align: center;"></td>
                            </tr>
                        </tbody>
                    </table>
                    @endif    
                    <div class="pull-right">
                        {!! $items->links('vendor.pagination.bootstrap-4'); !!}
                    </div>  
                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom_js')
<script>
    function cetakStruk(id) {
        $.ajax({
             url: '{{ route('pos.cetak.struk.detail') }}',
             type: 'POST',
             dataType: 'JSON',
             data: {
                faktur_id: id,
            },
         })
         .done(function(result) {
            if ($.isFunction(window.printStruck)) {
                window.printStruck(result);
            }
         })
         .fail(function() {
             console.log("error");
         })
         .always(function() {
             console.log("complete");
         });
    }
    jQuery(document).ready(function (e) {
        
    });
</script>
@endsection