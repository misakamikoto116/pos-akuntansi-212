@extends('chichi_theme.layout.app')

@section('custom_css')
    <style>
        .btn-xs {
            padding: 1px 5px;
            font-size: 10px;
            line-height: 1.5;
            border-radius: 3px;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <!-- Judul Halaman -->
            <h4 class="page-title">Pesanan Penjualan</h4>

            <!-- Judul Halaman -->
            <ol id="breadcrumb">
                <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                <li><a href="#">Pesanan Penjualan</a></li>
            </ol>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header"><h5 class="title">Pesanan Penjualan</h5>
                </div>
                <div class="card-body">

                    @include ('chichi_theme.layout.flash')

                    <div class="alert-pos">
                        
                    </div>

                    <div class="row">
                        <div class="col-sm-7">

                            <div class="daftar-penjualan table-responsive">
                                <div class="form-group" style="margin-bottom: 8px">
                                    <label for=""></label>
                                </div>
                                <table class="table table-bordered ">
                                    <thead>
                                        <tr>
                                            <th width="30%">Barang</th>
                                            <th width="15%">Qty</th>
                                            <th>Hrg</th>
                                            <th width="15%">Disk</th>
                                            <th>S. Total</th>
                                            <th width="10%">Opt</th>
                                        </tr>
                                    </thead>

                                    <tbody class="box-penjualan">
                                        @foreach (Cart::instance('pesanan-penjualan-' . auth()->user()->id)->content()->sortBy('options.order_id') as $cart)

                                            <tr id="{{ $cart->id }}" data-id="{{ $cart->id }}" data-text="{{ $cart->options->type }}">
                                                <td>{{ $cart->name }}</td>

                                                <td class="qty_edit">
                                                    <a class="cart-qty" data-qty="{{ $cart->qty }}">{{ $cart->qty }}</a>
                                                </td>
                                                {{-- hidden  input qty --}}
                                                <td class="qty_edit_hidden" style="display: none;">
                                                    {!! Form::number('qty_detail', $cart->qty,['class' => 'form-control qty_detail']) !!}
                                                </td>
                                                
                                                <td>{{ number_format($cart->price) }}</td>
                                                
                                                <td class="diskon_edit">{{ $cart->options->diskon ?? '0' }}</td>
                                                {{-- hidden  input diskon --}}
                                                <td class="diskon_edit_hidden" style="display: none;">
                                                    {!! Form::number('diskon_detail', $cart->options->diskon ?? 0,['class' => 'form-control diskon_detail']) !!}
                                                </td>
                                                
                                                <td><a class="cart-subtotal">{{ number_format($cart->options->price_final) }}</a></td>
                                                <td>
                                                    <a href="#" data-id="{{ $cart->rowId }}" class="btn btn-warning btn-xs btn-edit-item">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a href="#" data-id="{{ $cart->rowId }}" class="btn btn-primary btn-xs btn-save-item" style="display: none;">
                                                        <i class="fa fa-save"></i>
                                                    </a>
                                                    <a href="#" data-id="{{ $cart->rowId }}" class="btn btn-danger btn-xs btn-remove-item">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                </table>

                                <div class="form-group text-left">
                                    <label for="">Total : </label>
                                    <label for="" class="total-number" data-total="{{ $getCartTotal }}">{{ $getCartTotal }}</label>
                                </div>
                            </div>


                            {{ Form::open(['route' => 'pos.pesanan-penjualan.save.pesanan-penjualan', 'methods' => 'POST']) }}
                                <br>
                                <div class="row">
                                    <div class="col-sm-12">         
                                        <div class="form-group pull-right" style="margin-left: 1em;">
                                            <label for="nilai_kembalian">Diskon Nilai</label>
                                            {{ Form::text('total_potongan_rupiah', 0,['class' => 'form-control nilai_diskon']) }}
                                            {{ Form::hidden('pelanggan_id', null,['class' => 'pelanggan_id_val']) }}
                                        </div>
                                        <div class="form-group pull-right">
                                            <label for="nilai_kembalian">Diskon Persen</label>
                                            {{ Form::number('diskon', 0,['class' => 'form-control nilai_diskon_persen']) }}
                                        </div>                               
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="form-group pull-right">
                                                <button class="btn btn-md btn-primary btn-store">Proses Pesanan Penjualan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>

                        <div class="col-sm-5">
                            {{ Form::open(['']) }}
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Pilih Pelanggan
                                                <i class="fa fa-user"></i>
                                            </label>
                                            {{ Form::select('pelanggan_id_select', [], null, ['class' => 'form-control pelanggan_id', 'placeholer' => 'Penawaran Penjualan']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Cari Penawaran Penjualan
                                                <i class="fa fa-handshake-o"></i>
                                            </label>
                                            {{ Form::select('penawaran_penjualan_id', [], null, ['class' => 'form-control penawaran_penjualan', 'placeholer' => 'Penawaran Penjualan', 'required']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Cari Barang
                                                <i class="fa fa-barcode"></i>
                                            </label>
                                            {{ Form::select('barang_id', [], null, ['class' => 'form-control barang_select', 'placeholer' => 'Barang', 'required']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group div-qty" style="display: none;">
                                            <label>Qty
                                            </label>
                                            {{ Form::number('qty', 1, ['class' => 'form-control qty_input', 'placeholer' => 'Qty', 'min' => 1, 'disabled']) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group div-diskon-barang" style="display: none;">
                                            <label for="">Disk</label>

                                            {{ Form::number('diskon_barang', 0, ['class' => 'form-control diskon_barang', 'placeholder' => 'Diskon', 'disabled']) }}
                                        </div>
                                    </div>
                                </div>
                            {{ Form::close() }}

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom_js')

    <script type="text/javascript">
        jQuery(document).ready(function () {
            
            
            $('body').on('click', '.btn-store', function (e) {
                e.preventDefault();

                $('.alert-pos').html(
                    ''
                );

                var el = $(this);

                var form = $(this).closest('form');
                var nilai_diskon = $('.nilai_diskon').autoNumeric('get');
                var total = $('.total-number').data('total');

                $.ajax({
                    url: $(form).attr('action') + '?' + $(form).serialize(),
                    type: $(form).attr('method'),
                    dataType: 'JSON',
                    success: function (result) {

                        var template = $(
                            $('#template-alert').html()
                        ).clone();

                        if (result.status == true) {
                            $(template).find('.alert').addClass('alert-success');

                            $('.nilai_diskon').autoNumeric('set', 0);
                            $('.nilai_diskon_persen').val(0);
                            $('.pelanggan_id').val(' ').trigger('change');
                            $('.pelanggan_id_val').val(' ');
                            $('.box-penjualan').html('');

                            $('.total-number').html('0');
                            $('.total-number').attr('data-total', '0');

                        } else {
                            $(template).find('.alert').addClass('alert-warning');
                        }

                        $(template).find('.alert-message').html(result.message);

                        $('.alert-pos').html(
                            template
                        );
                    },
                    error: function () {
                        var template = $(
                            $('#template-alert').html()
                        ).clone();

                        $(template).find('.alert-message').html('Terjadi Kesalahan Server');

                        $('.alert-pos').html(
                            template
                        );
                    }
                });
            });

            // Set AutoNumeric
            $('.nilai_diskon').autoNumeric('init', {
                aPad: false
            });

            $('.total-number').autoNumeric('init', {
                aPad: false
            });


            // perhitungan nilai diskon persen
            $('body').on('keydown', '.nilai_diskon_persen', function (e) {
                
                var keyEnter = e.keyCode || e.which;
                
                // var
                var el                  = $(this);
                var nilai_diskon_persen = $(el).val();
                var total               = $('.total-number').data('total');
                
                if (keyEnter == 13) {
                    e.preventDefault();
                    // perhitungan
                    var subTotalDiskon      = (Number(total) * Number(nilai_diskon_persen)) / 100;

                    $(".nilai_diskon").autoNumeric('set', Number(subTotalDiskon)); 
                }

            });

            // perhitungan nilai diskon nilai

            $('body').on('keydown', '.nilai_diskon', function (e) {
                var keyEnter = e.keyCode || e.which;
                
                if (keyEnter == 13) {
                    e.preventDefault();
                    $(".nilai_diskon_persen").val(0); 
                }
            });

            // select barang
            $('.barang_select').select2({
                placeholder: 'Cari Barang',
                width: '100%',
                allowClear: false,
                ajax: {
                    url: '{{ route("pos.pesanan-penjualan.produk") }}',
                    dataType: 'JSON',
                    type: 'GET',
                    data: function (params) {
                        return {
                            search: params.term, // search term
                            page: params.page
                        };
                    },
                    cache: true
                },
                templateResult: function (state) {
                    if ( ! state.id) {
                        return state.text;
                    }
                    
                    var $state = $(
                        '<div style="font-size: 14px">  <span style="display:block"> <small> ' + state.no_barang + '</small> </span>' + state.text + '</div>'
                    );

                    return $state;
                },
                minimumInputLength: 3
            });

            // select pelanggan
            $('.pelanggan_id').select2({
                placeholder: 'Cari Pelanggan',
                width: '100%',
                allowClear: false,
                ajax: {
                    url: '{{ route("pos.pesanan-penjualan.pelanggan") }}',
                    dataType: 'JSON',
                    type: 'GET',
                    data: function (params) {
                        return {
                            search: params.term, // search term
                            page: params.page
                        };
                    },
                    cache: true
                },
                templateResult: function (state) {
                    if ( ! state.id) {
                        return state.text;
                    }
                    
                    var $state = $(
                        '<div style="font-size: 14px">  <span style="display:block"> <small> ' + state.no_pelanggan + '</small> </span>' + state.text + '</div>'
                    );

                    return $state;
                },
                minimumInputLength: 3
            });


            // diskon barang
            $('body').on('change', '.barang_select', function (e) {
                var el = $(this);

                $('.diskon_barang').val('0');

                if ( $(el).val() != '' &&
                     $(el).val() != null) {
                    
                    $.ajax({
                        url: '{{ route("pos.pesanan-penjualan.diskon.item") }}',
                        type: 'GET',
                        data: {
                            barang_id: $(el).val()
                        },
                        dataType: 'JSON',
                        success: function (result) {
                            
                            // hide and show 
                            $('.div-qty').show();
                            $('.div-diskon-barang').show();
                            $('.qty_input').removeAttr('disabled');
                            $('.diskon_barang').removeAttr('disabled');

                            if (result.status) {

                                if (result.data.diskon == null) {
                                    $('.diskon_barang').val('0');
                                } else {
                                    $('.diskon_barang').val(result.data.diskon);
                                }
                            
                            }
                        }
                    });
                }
            });


            // tambah item barang
            $('body').on('keydown', '.qty_input', function (e) {
                $('.alert-pos').html(
                    ''
                );

                var keyCode = e.keyCode || e.which;

                var el = $(this);
                var form = $(el).closest('form');

                if (keyCode == 13) {
                    e.preventDefault();

                    $('.nilai_diskon').autoNumeric('set', 0);

                    if ($('.barang_select').val() != '' && $(el).val() != '' && $(el).val() > 0 && $('.barang_select').val() != null && $(el).val() != null) {
                        $.ajax({
                            url: '{{ route("pos.pesanan-penjualan.add.item") }}?' + form.serialize(),
                            type: 'GET',
                            dataType: 'JSON',
                            data: {
                                type: "barang",
                            },
                            success: function (result) {
                                if (result.status) {
                                    $('.box-penjualan').html(
                                        result.template
                                    );

                                    $(el).val(1);

                                    $('.barang_select').val(null).trigger('change');

                                    $('.total-number').autoNumeric('set', Number(result.total));

                                    $('.total-number').data('total', result.total);

                                    // hide qty dan diskon
                                    $('.div-qty').hide();
                                    $('.div-diskon-barang').hide();
                                    $('.qty_input').attr('disabled','disabled');
                                    $('.diskon_barang').attr('disabled','disabled');

                                }
                            }
                        });
                    }

                }
            });

            // tambah item melalui diskon
            $('body').on('keydown', '.diskon_barang', function (e) {
                $('.alert-pos').html(
                    ''
                );

                var keyCode = e.keyCode || e.which;

                var el = $(this);
                var form = $(el).closest('form');

                if (keyCode == 13) {
                    e.preventDefault();

                    $('.nilai_diskon').autoNumeric('set', 0);

                    if ($('.barang_select').val() != '' && $(el).val() != '' && $(el).val() > 0 && $('.barang_select').val() != null && $(el).val() != null) {
                        $.ajax({
                            url: '{{ route("pos.pesanan-penjualan.add.item") }}?' + form.serialize(),
                            type: 'GET',
                            dataType: 'JSON',
                            data: {
                                type: "barang",
                            },
                            success: function (result) {
                                if (result.status) {
                                    $('.box-penjualan').html(
                                        result.template
                                    );

                                    $(el).val(0);

                                    $('.qty_input').val(1);

                                    $('.barang_select').val(null).trigger('change');

                                    $('.total-number').autoNumeric('set', Number(result.total));

                                    $('.total-number').data('total', result.total);

                                    // hide qty dan diskon
                                    $('.div-qty').hide();
                                    $('.div-diskon-barang').hide();
                                    $('.qty_input').attr('disabled','disabled');
                                    $('.diskon_barang').attr('disabled','disabled');

                                }
                            }
                        });
                    }

                }
            });

            // Remove item
            $('body').on('click', '.btn-remove-item', function (e) {
                e.preventDefault();

                $('.alert-pos').html(
                    ''
                );

                var el = $(this);

                $('.nilai_diskon').autoNumeric('set', 0);

                $.ajax({
                    url: '{{ route("pos.pesanan-penjualan.remove.item") }}',
                    type: 'GET',
                    data: {
                        id: $(el).data('id')
                    },
                    dataType: 'JSON',
                    success: function (result) {
                        if (result.status) {
                            $('.total-number').autoNumeric('set', Number(result.total));

                            $('.total-number').data('total', result.total);

                            $('.box-penjualan').html(
                                result.template
                            );

                            $(el).closest('tr').remove();
                        }
                    }
                });
            });
            // -----------------------------------------------------------------------------------------------------

            // Penawaran Penjualan
            // penawaran penjualan select
            $('.penawaran_penjualan').select2({
                placeholder: 'Cari Penawaran Penjualan',
                width: '100%',
                allowClear: false,
                ajax: {
                    url: '{{ route("pos.pesanan-penjualan.penawaran-penjualan") }}',
                    dataType: 'JSON',
                    type: 'GET',
                    data: function (params) {
                        return {
                            search: params.term, // search term
                            page: params.page
                        };
                    },
                    cache: true
                },
                templateResult: function (state) {
                    if ( ! state.id) {
                        return state.text;
                    }
                    
                    var $state = $(
                        '<div style="font-size: 14px">  <span style="display:block"> <small> ' + state.text + '</small> </span>' + state.tanggal + '</div>'
                    );

                    return $state;
                },
                minimumInputLength: 3
            });  

            // tambah penawaran penjualan
            $('.penawaran_penjualan').on('change', function (e) {
                $('.alert-pos').html(
                    ''
                );

                var el = $(this);
                var form = $(el).closest('form');


                $('.nilai_diskon').autoNumeric('set', 0);

                if ($(el).val() != '' && $(el).val() > 0 && $(el).val() != null) {
                    $.ajax({
                        url: '{{ route("pos.pesanan-penjualan.add.penawaran-penjualan") }}?' + form.serialize(),
                        type: 'GET',
                        dataType: 'JSON',
                        data: { 
                                type: 'penawaran',
                              },
                        success: function (result) {

                            if (result.status) {
                                $('.box-penjualan').html(
                                    result.template
                                );

                                $(el).val(null).trigger('change');

                                $('.total-number').autoNumeric('set', Number(result.total));

                                $('.total-number').data('total', result.total);

                                var data_pelanggan = '<option value="' + result.pelanggan.id + '">' + result.pelanggan.nama + '</option>'; 
                                $('.pelanggan_id').append(data_pelanggan);
                                $('.pelanggan_id').val(result.pelanggan.id);
                                $('.pelanggan_id_val').val(result.pelanggan.id);

                            }
                        }
                    });
                }
            });

            // edit row list barang
            $('.box-penjualan').on('click', '.btn-edit-item', function (e) {
                e.preventDefault();

                // show & hidden
                $(this).closest('tr').find(".qty_edit").hide();
                $(this).closest('tr').find(".qty_edit_hidden").show();
                $(this).closest('tr').find(".diskon_edit").hide();
                $(this).closest('tr').find(".diskon_edit_hidden").show();
                $(this).hide();
                $(this).closest('tr').find(".btn-save-item").show();
            });

            //  update row list barang
            $(".box-penjualan").on("click", ".btn-save-item", function (e) {
                e.preventDefault();

                // var
                var el          = $(this);
                var qty_val     = $(el).closest('tr').find('.qty_detail').val();
                var diskon_val  = $(el).closest('tr').find('.diskon_detail').val();
                var id_row      = $(el).closest('tr').data('id');
                var type_row    = $(el).closest('tr').data('text');

                // Kondisi
                if (qty_val !== '' && diskon_val !== '' && qty_val > 0) {
                    $.ajax({
                        url: '{{ route("pos.pesanan-penjualan.update.item") }}',
                        type: 'GET',
                        dataType: 'JSON',
                        data: {
                            id          : id_row,
                            type        : type_row,
                            qty         : qty_val,
                            diskon      : diskon_val,
                            type_submit : "edit",
                        },
                        success: function (result) {
                            if (result.status) {
                                $('.box-penjualan').html(
                                    result.template
                                );

                                $(el).val(null).trigger('change');

                                $('.total-number').autoNumeric('set', Number(result.total));

                                $('.total-number').data('total', result.total);
                            }
                        }
                    });
                }else {
                    swal({
                        icon: "error",
                        text: "Kolom Quantity Tidak Boleh Kosong",
                    });
                }

            });   
            // -----------------------------------------------------------------------------------------------------

        });
    </script>

    <script type="text/template" id="template-alert">
        <div>
            <div class="container">
                <div class="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p class="alert-message"></p>
                </div>
            </div>
        </div>
    </script>

@endsection