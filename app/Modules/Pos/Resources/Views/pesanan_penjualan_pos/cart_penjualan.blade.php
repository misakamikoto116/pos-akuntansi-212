@foreach (Cart::instance('pesanan-penjualan-' . auth()->user()->id)->content()->sortBy('options.order_id') as $cart)
    <tr id="{{ $cart->id }}" data-id="{{ $cart->id }}" data-text="{{ $cart->options->type }}">
        <td>{{ $cart->name ?? '-' }}</td>
        
        <td class="qty_edit">
            <a class="cart-qty" data-qty="{{ $cart->qty }}">{{ $cart->qty }}</a>
        </td>
        {{-- hidden  input qty --}}
        <td class="qty_edit_hidden" style="display: none;">{!! Form::text('qty_detail', $cart->qty,['class' => 'form-control qty_detail']) !!}</td>

        <td>{{ number_format($cart->price) }}</td>
        
        <td class="diskon_edit">{{ $cart->options->diskon ?? '0' }}</td>
        {{-- hidden  input diskon --}}
        <td class="diskon_edit_hidden" style="display: none;">{!! Form::text('diskon_detail', $cart->options->diskon ?? '0',['class' => 'form-control diskon_detail']) !!}</td>
        
        <td>
            <a class="cart-subtotal">{{ number_format($cart->options->price_final) }}</a>
        </td>
        <td>
            <a href="#" data-id="{{ $cart->rowId }}" class="btn btn-warning btn-xs btn-edit-item"><i class="fa fa-pencil"></i></a>
            <a href="#" data-id="{{ $cart->rowId }}" class="btn btn-primary btn-xs btn-save-item" style="display: none;">
                <i class="fa fa-save"></i>
            </a>
            <a href="#" data-id="{{ $cart->rowId }}" class="btn btn-danger btn-xs btn-remove-item"> <i class="fa fa-trash"></i> </a>
        </td>
    </tr>
@endforeach