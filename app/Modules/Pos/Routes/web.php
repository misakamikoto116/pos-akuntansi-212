<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Jobs\PenjualanQueue;

Route::group(['prefix' => 'pos', 'as' => 'pos.', 'middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController');
    Route::resource('retur', 'ReturController');
    Route::post('retur/check-transaksi','ReturController@checkTransaksi')->name('retur.check.transasction');
    Route::post('retur/submit-retur','ReturController@submitRetur')->name('submit.retur');

    Route::get('get-detail-barang', 'AjaxController@getDetailBarang')->name('get-detail-barang');
    Route::get('get-produk-by-val', 'AjaxController@getProdukByVal')->name('get-produk-by-val');
    Route::get('get-returned-item', 'AjaxController@getReturnedItem')->name('get-returned-item');

    Route::get('reset-kasir','PenjualanController@resetKasir')->name('reset.kasir');

    //Route::get('get-nama-produk', 'AjaxController@getDaftarNamaProduk')->name('get-nama-produk');

    Route::group(['middleware' => 'KasirOnline:true'], function () {
        Route::resource('penjualan', 'PenjualanController');
        Route::resource('pesanan-penjualan','PesananPenjualanPosController');
                
        // Penjualan
        Route::get('penjualan/ajax/produk', 'PenjualanController@getProduk')->name('penjualan.produk');
        Route::get('penjualan/ajax/produk-test', 'PenjualanController@getProdukTest')->name('penjualan.produk.test');
        Route::get('penjualan/ajax/cari/diskon', 'PenjualanController@cariDiskon')->name('penjualan.cari.diskon');
        Route::get('penjualan/ajax/batal/diskon', 'PenjualanController@batalDiskon')->name('penjualan.batal.diskon');
        Route::get('penjualan/ajax/add/item', 'PenjualanController@addItem')->name('penjualan.add.item');
        Route::get('penjualan/ajax/remove/item', 'PenjualanController@removeItem')->name('penjualan.remove.item');
        Route::get('penjualan/ajax/diskon/item', 'PenjualanController@diskonItem')->name('penjualan.diskon.item');
        Route::get('penjualan/print/logout', 'PenjualanController@printAndLogout')->name('penjualan.print');
        Route::post('penjualan/edit/harga','PenjualanController@editHargaJual')->name('update.harga_jual_pos');

        // scan barcode
        Route::get('penjualan/scan/barcode','PenjualanController@getProdukBarcode')->name('scan.barcode');
        // scan barcode cari barang
        Route::get('penjualan/scan/barcode/cari/barang','PenjualanController@getProdukBarcodeCariBarang')->name('cari.barang.barcode');
        // Reset Penjualan
        Route::post('penjualan/reset','PenjualanController@getResetPenjualan')->name('get.reset.penjualan');


        // Cek out
        Route::post('penjualan/check-out-kasir', 'PenjualanController@kasirCheckOutStore')->name('penjualan.check-out-kasir');
        Route::get('penjualan/view-pdf/{nomor}', 'PenjualanController@viewPdf')->name('penjualan.view-pdf');
        // Route::post('penjualan/check-out-kasir/json', 'PenjualanController@kasirCheckOutStoreJson')->name('penjualan.check-out-kasir.json');
        // Route::post('penjualan/check-out-kasir','PenjualanController@kasirCheckOutStore')->name('penjualan.check-out-kasir');
        // Route stream
     
        // Pesanan Penjualan
        Route::get('penjualan/ajax/pesanan-penjualan', 'PenjualanController@getPesananPenjualan')->name('penjualan.pesanan-penjualan');
        Route::get('penjualan/ajax/add/pesanan-penjualan', 'PenjualanController@addItemPesananPenjualan')->name('penjualan.add.pesanan-penjualan');
        Route::get('penjualan/ajax/update/pesanan-penjualan','PenjualanController@updateItemCart')->name('penjualan.update.item');
        // Pelanggan
        Route::get('penjualan/ajax/pelanggan', 'PenjualanController@getPelanggan')->name('penjualan.pelanggan');



        // Pesanan Penjualan
        // Barang
        Route::get('pesanan-penjualan/ajax/produk', 'PesananPenjualanPosController@getProduk')->name('pesanan-penjualan.produk');
        Route::get('pesanan-penjualan/ajax/add/item', 'PesananPenjualanPosController@addItem')->name('pesanan-penjualan.add.item');
        Route::get('pesanan-penjualan/ajax/remove/item', 'PesananPenjualanPosController@removeItem')->name('pesanan-penjualan.remove.item');
        Route::get('pesanan-penjualan/ajax/diskon/item', 'PesananPenjualanPosController@diskonItem')->name('pesanan-penjualan.diskon.item');
        // Penawaran Penjualan
        Route::get('pesanan-penjualan/ajax/penawaran-penjualan', 'PesananPenjualanPosController@getPenawaranPenjualan')->name('pesanan-penjualan.penawaran-penjualan');
        Route::get('pesanan-penjualan/ajax/add/penawaran-penjualan', 'PesananPenjualanPosController@addItemPenawaranPenjualan')->name('pesanan-penjualan.add.penawaran-penjualan');
        Route::get('pesanan-penjualan/ajax/update/penawaran-penjualan','PesananPenjualanPosController@updateItemCart')->name('pesanan-penjualan.update.item');
        Route::post('pesanan-penjualan/ajax/save/pesanan-penjualan','PesananPenjualanPosController@savePesananPenjualan')->name('pesanan-penjualan.save.pesanan-penjualan');
        // Pelanggan
        Route::get('pesanan-penjualan/ajax/pelanggan', 'PesananPenjualanPosController@getPelanggan')->name('pesanan-penjualan.pelanggan');

        // EDC
        // Debit
        Route::get('penjualan/ajax/add/edc/debit', 'PenjualanController@addEdcDebit')->name('penjualan.add.edc.debit');
        Route::get('penjualan/ajax/remove/edc/debit', 'PenjualanController@removeEdcDebit')->name('penjualan.remove.edc.debit');

        // cetak ulang
        Route::post('penjualan/cetak-ulang','PenjualanController@getCetakUlang')->name('cetak.ulang');

        // cetak detail
        Route::post('penjualan/cetak-struk-detail','PenjualanController@cetakStrukDetail')->name('cetak.struk.detail');
    });

    Route::get('print', function () {
        return view('pos::penjualan.struk');
    });

    Route::get('barang', 'BarangController@index')->name('barang.index');

    Route::get('barang/price-card-select', 'BarangController@priceCardBySelect')->name('get.stok-price-card-select');
    Route::get('barang/promo-card-select', 'BarangController@promoCardBySelect')->name('get.stok-promo-card-select');


    
    Route::group(['middleware' => 'KasirOnline:false'], function () {
        Route::get('penjualan/kasir/check-in', 'PenjualanController@kasirCheckIn')->name('penjualan.kasir.check.in');
        Route::post('penjualan/kasir/check-in/store', 'PenjualanController@kasirCheckInStore')->name('penjualan.kasir.check.in.store');
    });

    // Rekap Transaksi
    Route::get('penjualan/kasir/transaksi', 'PenjualanController@getTransaksi')->name('penjualan.kasir.transaksi');
    Route::get('penjualan/kasir/detail-transaksi/{id}/{status}', 'PenjualanController@getDetailTransaksi')->name('penjualan.kasir.detail-transaksi');

    // Cari Kasir Mesin
    Route::get('penjualan/search/kasir-mesin','PenjualanController@getKasirMesin')->name('get.kasir.mesin');

    //Display Kasir
    Route::get('display-kasir', 'PenjualanController@displayKasir')->name('pos.display.kasir');
    Route::get('api-antrian', function (){
        //$pelanggan = $this->informasiPelanggan->where('id', $data['id'])->first();
        $penjualanToApi = [
            'tanggal' => date('Y-m-d'),
            'total' => '12242',
            'jml_umkm' => '2323',
            'jml_non' => '23232',
            'nomor_anggota' => '1213212',
            'anggota_id' => '121',
        ];
         dispatch(new PenjualanQueue($penjualanToApi));
    });
    
    Route::get('getFromCart', 'PenjualanController@getFromCart')->name('getfromcart');
    Route::get('checkChartList', 'PenjualanController@checkChartList')->name('checkChartList');
    Route::post('savetocart', 'PenjualanController@saveToCart')->name('savetocart');
    Route::delete('removefromcart', 'PenjualanController@removeFromCart')->name('deletefromcart');
});
