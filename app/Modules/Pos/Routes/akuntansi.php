<?php 
Route::group(['prefix' => 'pos', 'as' => 'pos.', 'middleware' => 'auth'], function () {
    Route::resource('promosi', 'PromosiController')->only(['index', 'show']);
});