<?php

Route::group(['prefix' => 'pos/laporan', 'as' => 'pos.', 'middleware' => 'auth'], function () {
    Route::get('', function () {
        return view('pos::laporan.index');
    })->name('laporan.list');

    Route::get('persediaan/kartu-stok-persediaan', 'LaporanPersediaanController@kartu_stok_persediaan');
});
