<?php
Route::post('login', 'Api\ApiAuthController@login');
//Dev Pos Jual submit
Route::post('pos/submit','Api\Pos\PosController@devPost')->name('api.post.dev_pos');
Route::post('omzet-weekly', 'Api\Pelanggan\PelangganController@omzetHarian');
Route::post('performa-anggota', 'Api\Pelanggan\PelangganController@topPelanggan');

Route::post('anggota', 'Api\Pelanggan\Anggota@anggotaPost')->name('api.pos.create_pelanggan');
Route::put('anggota/{id}', 'Api\Pelanggan\Anggota@anggotaUpdate')->name('api.pos.update_pelanggan');
Route::delete('anggota/{id}', 'Api\Pelanggan\Anggota@anggotaDelete')->name('api.pos.delete_pelanggan');

// Route::get('beli-pulsa','ProfileController@testPulsa');
// Route::post('beli-pulsa-proses','ProfileController@testPulsaProses');

Route::middleware('auth:api')->group(function () {
    Route::get('produk/all', 'Api\Produk\ProdukController@allProduk')->name('api.get.list-all-produk');

    Route::get('pos/penjualan-list', function(){
        return response()->json(['sukses' => true]);
    });

    //dev mode api list penjualan
    Route::get('pos/list-transaksi','Api\Pos\PosController@devPosListPenjualan')->name('api.get.dev_pos_list');
    
    //Produk list kategori
    Route::get('produk/kategori', 'Api\Produk\ProdukController@listKategori')->name('api.get.list-kategori');
    
    //produk add penawaran
    Route::post('produk/penawaran', 'Api\Produk\ProdukController@addPenawaranPost')->name('api.post.add-penawaran');
    Route::post('pos/barcode', 'Api\Pos\PosController@findByBarcode')->name('api.post.findBarcode');

    //produk resources
    Route::apiResource('produk', 'Api\Produk\ProdukController', ['only' => ['index', 'show'], ['except' => ['listKategori', 'allProduk'] ]]);
    
    //EndPoint API Penjualan
    Route::apiResource('pos', 'Api\Pos\PosController');
    Route::apiResource('pelanggan', 'Api\Pelanggan\PelangganController');
    Route::get('list-pos', 'Api\Pos\PosController@listPost')->name('api.list-pos');
    Route::get('penjualan/transaksi', 'Api\Penjualan\ApiPenjualanController@ListTransaksi')->name('api.list.transaksi');
    Route::get('penjualan/detail-transaksi', 'Api\Penjualan\ApiPenjualanController@detailTransaksi')->name('api.detail-transaksi');
    
    // Route::post('penjualan/create', 'Api\Penjualan\ApiPenjualanController@store')->name('api.penjualan.create');

    // List kasir
    Route::apiResource('list-kasir','Api\ListKasir\ListKasirController');
    // List Pelanggan
    Route::apiResource('list-pelanggan','Api\ListPelanggan\ListPelangganController');
    // Kasir Check In
    Route::apiResource('check-in','Api\KasirCheckIn\KasirCheckInController');
    // Kasir Check Out
    Route::apiResource('check-out','Api\KasirCheckOut\KasirCheckOutController');
    // Uang Modal Kasir
    Route::apiResource('uang-modal','Api\UangModal\UangModalController');
   
});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
//Route::middleware('auth:api')->post('/anggota', ""
// Route::post('/anggota', "Api\Pelanggan\Anggota@anggotaPost")->middleware('auth:api');
//     //EndPoint API Penjualan
//     Route::apiResource('pos', 'Api\Pos\PosController');
// });