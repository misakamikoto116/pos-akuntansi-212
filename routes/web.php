<?php
Route::prefix('ajax')->group(function () {
    // Validasi get-started
    Route::get('/perusahaan-validasi', 'GetStartedController@validPerusahaan')->name('get.perusahaan');
    // Route::get('/kas-bank-validasi', 'GetStartedController@validKasBank')->name('get.kas.bank');
});

Route::group(['middleware' => 'configured:true'], function () {

    // Route::get('/', function () {
    //     return redirect('home');
    // });
    Route::get('/dashboard', function () {
        return view('chichi_theme.dashboard.home.dashboard');
    })->name('home');
    Auth::routes();
    Route::get('profile', 'ProfileController@index')->name('profile');
    Route::patch('profile/update', 'ProfileController@save')->name('profile/update');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
});

Route::group(['middleware' => 'configured:false'], function () {
    Route::get('get-started', 'GetStartedController@index')->name('chichi.starting');
    Route::post('get-started/save', 'GetStartedController@save')->name('save.starting');
    Route::get('import-get-started','GetStartedController@indexImportGetStarted')->name('import-get-started');

});

Route::get('sticker/header', 'IntegratedController@stickerHeader')->name('integrated.sticker.header');

Route::get('', 'Auth\LoginController@redirectTo');

// use App\Akun;

// use Helpers\HelperRekursif;

// $recursive = [];

// Route::get('wawan', function() {
//     $ww = new HelperRekursif;

//     dd($ww->get());
// });
