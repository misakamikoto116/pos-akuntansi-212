<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Modules\Akuntansi\Models\User;
use App\Modules\Akuntansi\Models\SyaratPembayaran;

class SyaratPembayaranTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->assertPathIs('/akuntansi')
                    ->assertSee('Dashboard')
                    ->click('li.has-submenu:nth-child(2) > a:nth-child(1)', 'Master')->pause('500')
                    ->mouseover('.navigation-menu > li:nth-child(2) > ul:nth-child(2) > li:nth-child(5) > a:nth-child(1)')->pause('500')
                    ->click('.navigation-menu > li:nth-child(2) > ul:nth-child(2) > li:nth-child(5) > a:nth-child(1)')->pause('500')
                    ->assertPathIs('/akuntansi/syarat-pembayaran')
                    ->assertSee('Daftar Syarat Pembayaran')->pause('400');
            // $this->createSyaratPembayaran( $browser );
            $browser->assertPathIs('/akuntansi/syarat-pembayaran');
            // $this->updateSyaratPembayaran( $browser );
            $browser->assertPathIs('/akuntansi/syarat-pembayaran');
            $this->deleteSyaratPembayaran( $browser );
            $browser->assertPathIs('/akuntansi/syarat-pembayaran');
            $browser->pause('50000');
        });
    }

    public function createSyaratPembayaran ( $browser ) {
        $browser->clickLink('Tambah')->pause('500')
                ->assertPathIs('/akuntansi/syarat-pembayaran/create')->pause('500')
                ->assertSee('Tambah ')
                ->assertSee('Nama')
                ->type('nama', $this->randomInput(10))->pause('500')
                ->assertSee('Jika Membayar Antara')
                ->type('jika_membayar_antara', rand(0, 14) )->pause('500')
                ->assertSee('Akan Dapat Diskon')
                ->type('akan_dapat_diskon', rand(0, 100))->pause('500')
                ->assertSee('Jatuh Tempo')
                ->type('jatuh_tempo', '30' )->pause('500')
                ->assertSee('Keterangan')
                ->type('keterangan', $this->randomInput(80) )->pause('500')
                ->script('window.scrollTo( { top : document.body.scrollHeight, behavior : "smooth" })');
        $browser->assertSee('Tunai Saat Pengantaran')->pause('1500');
        $randomStatus = rand( 0, 1 );
        if ( $randomStatus == 1 ) {
            $browser->check('status_cod')->pause('500');
        } else if ( $randomStatus == 0 ) {
            $browser->uncheck('status_cod')->pause('500');
        }
        $browser->press('#btn-submit')->pause('1000')
                ->press('.swal-button--confirm')->pause('500');
    }

    public function updateSyaratPembayaran ( $browser ) {
        $lastSyaratPembayaran = SyaratPembayaran::get()->last();
        $browser->script('window.scrollTo( { top : 250, behavior : "smooth" } )');
        $browser->assertSee('Option')->pause('500')
                    ->press('#option'.$lastSyaratPembayaran['id'])->pause('500')
                    ->click('.show > div:nth-child(2) > a:nth-child(1)')->pause('6000')
                    ->assertPathIs('/akuntansi/syarat-pembayaran/'.$lastSyaratPembayaran['id'].'/edit')->pause('500')
                    ->assertSee('Edit ')
                    ->assertSee('Nama')
                    ->type('nama', $this->randomInput(10))->pause('500')
                    ->assertSee('Jika Membayar Antara')
                    ->type('jika_membayar_antara', rand(0, 14) )->pause('500')
                    ->assertSee('Akan Dapat Diskon')
                    ->type('akan_dapat_diskon', rand(0, 100))->pause('500')
                    ->assertSee('Jatuh Tempo')
                    ->type('jatuh_tempo', '30' )->pause('500')
                    ->assertSee('Keterangan')
                    ->type('keterangan', $this->randomInput(80) )->pause('500')
                    ->script('window.scrollTo( { top : document.body.scrollHeight, behavior : "smooth" })');
        $browser->assertSee('Tunai Saat Pengantaran')->pause('1500');
        $randomStatus = rand( 0, 1 );
        if ( $randomStatus == 1 ) {
            $browser->check('status_cod')->pause('500');
        } else if ( $randomStatus == 0 ) {
            $browser->uncheck('status_cod')->pause('500');
        }
        $browser->press('.btn-default')->pause('1000')
                ->press('.swal-button--confirm')->pause('500');
    }

    public function deleteSyaratPembayaran ( $browser ) {
        $lastSyaratPembayaran = SyaratPembayaran::get()->last();
        $browser->script('window.scrollTo( { top : 250, behavior : "smooth" } )');
        $browser->assertSee('Option')->pause('500')
                ->press('#option'.$lastSyaratPembayaran['id'])->pause('500')
                ->click('.show > div:nth-child(2) > a:nth-child(2)')->pause('5000')
                ->press('.swal-button--confirm')
                ->pause('500');
    }

    public function randomInput ( $length = 10 ) {
        $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charlt = strlen($char);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $char[rand(0, $charlt - 1)];
        }
        return $randomString;
    }
}
