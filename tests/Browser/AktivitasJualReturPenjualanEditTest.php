<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use DB;
use App\Modules\Akuntansi\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AktivitasJualReturPenjualanEditTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $kondisi        =   $this->KondisiRetur();

            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->click('li.has-submenu:nth-child(7) > a:nth-child(1)', 'Aktivitas')->pause('2000')
                    ->mouseover('.megamenu > li:nth-child(1) > ul:nth-child(1) > li:nth-child(7) > a:nth-child(1)')->pause('5000')
                    ->clickLink('Retur Penjualan')->pause('2000')
                    ->assertPathIs('/akuntansi/retur-penjualan')->pause('2000')
                    ->script('window.scrollTo(0, document.body.scrollHeight)');
            
            $browser->assertSee('Option')->pause('2000')
                    ->press('#option'.$kondisi['retur_terakhir'])->pause('2000')
                    ->click('.show > div:nth-child(2) > a:nth-child(2)')->pause('2000')
                    ->assertPathIs('/akuntansi/retur-penjualan/'.$kondisi['retur_terakhir'].'/edit')->pause('2000')
                    ->select2('#pelanggan_id', 'PT 123')->pause('2000')
                    ->script('window.scrollTo(0,180);');

            $browser->select2('#list-sales-invoice', $kondisi['faktur'])->pause('2000')
                    ->type('sr_no', $kondisi['sr_no'])->pause('2000')
                    ->type('no_fiscal', $kondisi['fiscal_no'])->pause('2000')
                    ->script('window.scrollTo(0, document.body.scrollHeight)');

            $browser->type('keterangan', 'Edit Retur Penjualan')->pause('2000')
                    ->type('diskon', 10)->pause('2000')
                    ->assertSee('Simpan')->pause('2000')
                    ->press('#btn-submit')->pause('2000')
                    ->assertPathIs('/akuntansi/retur-penjualan')->pause('2000')
                    ->screenshot('Berhasil Melakukan Penambahan Retur Penjualan')
                    ->press('.swal-button--confirm')->pause('3000');
        });
    }

    public function KondisiRetur(){
        $pelanggan  =   DB::table('informasi_pelanggan')->find(1);
        $faktur     =   DB::table('faktur_penjualan')->where('pelanggan_id', $pelanggan->id)->get();
        $retur      =   DB::table('retur_penjualan')->orderBy('id', 'desc')->first();
        if(is_null($retur)){
            return [
                'retur_terakhir'        =>  0,
                'sr_no'                 =>  1,
                'fiscal_no'             =>  1,
                'faktur'                =>  $faktur['0']->no_faktur,
            ];
        }else{
            return [
                'retur_terakhir'        =>  $retur->id,
                'sr_no'                 =>  $retur->sr_no + 1,
                'fiscal_no'             =>  $retur->no_fiscal + 1,
                'faktur'                =>  $faktur['0']->no_faktur,
            ];
        }
    }
}
