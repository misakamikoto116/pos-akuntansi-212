<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\User;

class ImportProdukByAkunTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->maximize()
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->assertPathIs('/akuntansi')
                    ->assertSee('Dashboard')
                    ->click('li.has-submenu:nth-child(3) > a:nth-child(1)', 'Import')->pause('500')
                    ->mouseover('.navigation-menu > li:nth-child(3) > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)')->pause('500')
                    ->click('.navigation-menu > li:nth-child(3) > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)')->pause('500')
                    ->assertPathIs('/akuntansi/upload-produk-by-akun')->pause('2000');
                    $this->selectAkun( $browser );
            $browser->assertSee('File XLSX')->pause('500')
                    ->attach('import_file', $this->fileImport())->pause('500')
                    ->press('#btn-submit')->pause('500')
                    ->press('.swal-button--confirm')->pause('500')
                    ->assertPathIs('/akuntansi/upload-produk-by-akun')
                    ->pause('3000');
        });
    }

    public function selectAkun ( $browser )
    {
        $browser
                ->assertSee('Akun Persediaan')
                ->select2('.akun_persediaan_id' , $this->randomAkunProduk() )
                ->assertSee('Akun Penjualan')
                ->select2('.akun_penjualan_id' , $this->randomAkunProduk() )
                ->assertSee('Akun Retur Penjualan')
                ->select2('.akun_retur_penjualan_id' , $this->randomAkunProduk() )
                ->assertSee('Akun Diskon Barang')
                ->select2('.akun_diskon_barang_id' , $this->randomAkunProduk() )
                ->script('window.scrollTo( { top : document.body.scrollHeight, behavior : "smooth" })');
        $browser->assertSee('Akun Barang Terkirim')
                ->select2('.akun_barang_terkirim_id' , $this->randomAkunProduk() )
                ->assertSee('Akun HPP')
                ->select2('.akun_hpp_id' , $this->randomAkunProduk() )
                ->assertSee('Akun Retur Pembelian')
                ->select2('.akun_retur_pembelian_id' , $this->randomAkunProduk() )
                ->assertSee('Akun Beban')
                ->select2('.akun_beban_id' , $this->randomAkunProduk() )
                ->assertSee('Akun Belum Tertagih')
                ->select2('.akun_belum_tertagih_id' , $this->randomAkunProduk() );
    }

    public function randomAkunProduk ( ) {
        $akunProduk     =   Akun::get()->random();
        return $akunProduk->nama_akun;
    }

    public function fileImport() {
        $randomFile = glob(__DIR__."/File_attachments/Files/export-produkmaster-without-akun.xlsx", GLOB_BRACE);
        return $randomFile[0];
    }
}
