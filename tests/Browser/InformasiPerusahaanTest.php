<?php

namespace Tests\Browser;

use App\Modules\Akuntansi\Models\User;
use App\Modules\Akuntansi\Models\MataUang;
use App\Modules\Akuntansi\Models\Identitas;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;

class InformasiPerusahaanTest extends DuskTestCase
{
    public function setUp(){
        parent::setUp();
    }

    use withFaker;
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $anotherModelFunct = $this->anotherModel();
            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->assertSee('Dashboard')
                    ->click('li.has-submenu:nth-child(2) > a:nth-child(1)', 'Master')->pause('500')
                    ->mouseover('.navigation-menu > li:nth-child(2) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)')->pause('500')
                    ->click('.navigation-menu > li:nth-child(2) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)', 'Info Perusahaan')->pause('500')
                    ->assertSee('Data Perusahaan')->pause('500');
                    $this->EditPerusahaanUmum( $browser, $anotherModelFunct );
                    $this->EditPerusahaanPajak( $browser, $anotherModelFunct );
                    $this->EditPerusahaanCabang( $browser );
                    $this->EditPerusahaanUploadLogo( $browser );
            $browser->script('window.scrollTo( { top : document.body.scrollHeight, behavior : "smooth" })');
            $browser->press('#btn-submit-identitas')->pause('5000');
        });
    }

    public function EditPerusahaanUmum( $browser, $data ) {
        $browser->script('window.scrollTo( { top : 250, behavior : "smooth" } )');
        $browser->pause('500')
                ->assertSee('Nama Perusahaan')
                ->type('nama_perusahaan', $data['identitas']['nama_perusahaan'])->pause('500')
                ->assertSee('Tanggal Tutup Buku')
                ->type('#tanggal_mulai', $data['tanggalIdentitas'])
                ->keys('#tanggal_mulai', '{enter}')->pause('500')
                ->assertSee('Alamat')
                ->type('alamat', $this->faker->streetAddress)->pause('500')
                ->assertSee('Kepala Toko')
                ->type('kepala_toko', $this->faker->name)->pause('500')
                ->assertSee('Kode Pos')
                ->type('kode_pos', $this->faker->postcode)->pause('500')
                ->assertSee('No Telepon')
                ->type('no_telepon', $this->faker->phoneNumber)->pause('500')
                ->assertSee('Negara')
                ->type('negara', $this->faker->state)->pause('500')
                ->assertSee('Mata Uang')
                ->select2('#mata_uang_id', $data['mataUang']->random())->pause('500')
                ->assertSee('Pesan Footer')
                ->type('footer_1', substr($this->faker->text, 0, 20))->pause('500')
                ->assertSee('Slogan Footer')
                ->type('footer_2', substr($this->faker->text, 0, 20))->pause('500')
                ->pause('500');
    }

    public function EditPerusahaanPajak ( $browser, $data ) {
        $browser->script('window.scrollTo( { top : 250, behavior : "smooth" } )');
        $browser->click('li.nav-item:nth-child(2) > a:nth-child(1)')->pause('500')
                ->assertSee('Nama Perusahaan')
                ->assertSee('Alamat')
                ->assertSee('No Seri Faktur Pajak')
                ->type('no_seri_faktur_pajak', $this->randomInput(20) )->pause('500')
                ->assertSee('No Pokok Wajib Pajak')
                ->type('npwp', $this->randomInput(20) )->pause('500')
                ->assertSee('No Pengukuhan PKP')
                ->type('no_pengukuhan_pkp', $this->randomInput(30))->pause('500')
                ->assertSee('Tanggal Pengukuhan PKP')
                ->type('tanggal_pengukuhan_pkp', $data['tanggalIdentitas'])->pause('500')
                ->keys('#tanggal_pengukuhan_pkp', '{enter}')->pause('500')
                ->assertSee('Kode Cabang')
                ->type('kode_cabang_pajak', $this->randomInput(30))->pause('500')
                ->script('window.scrollTo( { top : 400, behavior : "smooth" } )');
        $browser->assertSee('Jenis Usaha')
                ->type('jenis_usaha', $this->randomInput(80))->pause('500')
                ->assertSee('KLU SPT')
                ->type('klu_spt', $this->randomInput(10))->pause('500')
                ->pause('500');
    }

    public function EditPerusahaanCabang( $browser ){
        $browser->script('window.scrollTo( { top : 250, behavior : "smooth" })');
        $browser->click('li.nav-item:nth-child(3) > a:nth-child(1)')->pause('500')
                ->assertSee('ID Cabang')
                ->assertSee('Nama Cabang');
        $browser->script('window.scrollTo( { top : document.body.scrollHeight, behavior : "smooth" })');
        $countCodeCabangClass = $browser->script('return $(".kode_cabang").length;');
        $browser->click('.add-cabang')->pause('500');
        $browser->script('window.scrollTo( { top : document.body.scrollHeight, behavior : "smooth" })');
        if ( $countCodeCabangClass > 0 ) {
            $nwCounter = 0;
        } else {
            $nwCounter = $countCodeCabangClass;
        }
        $browser->type('#kode_cabang'. $nwCounter, $this->randomInput(10) )->pause('500')
                ->type('#nama_cabang'. $nwCounter, $this->randomInput(20) )->pause('500');
    }

    public function EditPerusahaanUploadLogo( $browser ){
        $browser->script('window.scrollTo( { top : 250, behavior : "smooth" } )');
        $browser->assertSee('Logo Perusahaan')
                ->click('li.nav-item:nth-child(4) > a:nth-child(1)')->pause('500')
                ->attach('logo_perusahaan', $this->randomImage())->pause('500');
    }

    public function anotherModel()
    {
        $identitas = Identitas::first();
        $mataUang  = MataUang::pluck('kode');
        $tanggalIdentitas = Carbon::parse($identitas['tanggal_mulai'])->format('d F Y');

        return [
            'identitas' => $identitas,
            'mataUang'  => $mataUang,
            'tanggalIdentitas' => $tanggalIdentitas,
        ];
    }

    public function randomImage() {
        $randomFile = glob(__DIR__."/File_attachments/Images/*.{jpg,gif,png}", GLOB_BRACE);
        $randomPick = rand(0, ( count($randomFile) -1 ) );
        return $randomFile[$randomPick];
    }

    public function randomInput ( $length = 10 ) {
        $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charlt = strlen($char);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $char[rand(0, $charlt - 1)];
        }
        return $randomString;
    }
}