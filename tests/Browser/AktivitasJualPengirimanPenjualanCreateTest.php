<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use DB;
use App\Modules\Akuntansi\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AktivitasJualPengirimanPenjualanCreateTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $kondisi    =   $this->KondisiPengiriman();

            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')->click('li.has-submenu:nth-child(7) > a:nth-child(1)', 'Aktivitas')->pause('2000')
                    ->mouseover('.megamenu > li:nth-child(1) > ul:nth-child(1) > li:nth-child(4) > a:nth-child(1)')->pause('5000')
                    ->clickLink('Pengiriman Pesanan')->pause('2000')
                    ->assertPathIs('/akuntansi/pengiriman-penjualan')->pause('2000')
                    ->clickLink('Tambah')->pause('2000')
                    ->assertPathIs('/akuntansi/pengiriman-penjualan/create')->pause('2000')
                    ->select2('#pelanggan_id', $kondisi['pelanggan'])->pause('2000')
                    ->press('.swal-button--confirm')->pause('2000')
                    ->check('pesanan[]')->pause('2000')
                    ->press('.modal-footer > button:nth-child(1)')->pause('2000')
                    ->type('delivery_no', $kondisi['do'])->pause('2000')
                    ->select2('#ship_id', 'TIKI')->pause('2000')
                    ->type('keterangan', 'Pengiriman Penjualan')->pause('2000')
                    ->assertSee('Simpan')->pause('2000')
                    ->script('window.scrollTo(0, document.body.scrollHeight)');
                    
            $browser->press('#btn-submit')->pause('2000')
                    ->assertSee('Simpan & Tutup')->pause('2000')
                    ->press('ul.dropdown-menu > li:nth-child(3)')->pause('5000')
                    ->screenshot('Berhasil Melakukan Perubahan Terakhir Pengiriman Penjualan')
                    ->assertPathIs('/akuntansi/pengiriman-penjualan')->pause('5000')
                    ->press('.swal-button')->pause('5000');
        });
    }
    public function KondisiPengiriman(){
        $pengiriman     =   DB::table('pengiriman_penjualan')->orderBy('id', 'desc')->first();
        $pelanggan      =   DB::table('informasi_pelanggan')->find(2);
        $do             =   0;
        if(is_null($pengiriman)){
            return [
                'pengiriman_terakhir'    =>      1,
                'pelanggan'              =>      $pelanggan->nama,
                'do'                     =>      $do    =   $do + 1,
            ];
        }else{
            return [
                'pengiriman_terakhir'    =>      $pengiriman->id,
                'pelanggan'              =>      $pelanggan->nama,
                'do'                     =>      $pengiriman->delivery_no + 1,
            ];
        }
    }
}
