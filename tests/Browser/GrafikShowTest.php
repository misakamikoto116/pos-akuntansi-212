<?php

namespace Tests\Browser;

use App\Modules\Akuntansi\Models\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class GrafikShowTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        try {
            $this->browse(function (Browser $browser) {
                $browser
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->assertSee('GRAFIK LABA RUGI')
                    ->assertPathIs('/akuntansi');
            });
        } catch (\Exception $e) {
        } catch (\Throwable $e) {
        }
    }
}
