<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use DB;
use App\Modules\Akuntansi\Models\User;

class ExampleTest extends DuskTestCase
{
    /**
     * @test
     * A basic browser test example.
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->pause('100000');
        });
    }
}
