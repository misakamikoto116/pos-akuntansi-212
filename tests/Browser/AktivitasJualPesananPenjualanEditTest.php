<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use DB;
use App\Modules\Akuntansi\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AktivitasJualPesananPenjualanEditTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $kondisi    =   $this->KondisiPesanan();
            
            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')->click('li.has-submenu:nth-child(7) > a:nth-child(1)', 'Aktivitas')->pause('2000')
                    ->mouseover('.megamenu > li:nth-child(1) > ul:nth-child(1) > li:nth-child(3) > a:nth-child(1)')->pause('5000')
                    ->clickLink('Pesanan Penjualan')->pause('2000')
                    ->assertPathIs('/akuntansi/pesanan-penjualan')->pause('2000')
                    ->script('window.scrollTo(0, document.body.scrollHeight)');

            $browser->assertSee('Option')->pause('2000')
                    ->press('#option'.$kondisi['pesanan_terakhir'])->pause('2000')
                    ->click('.show > div:nth-child(2) > a:nth-child(2)')->pause('2000')
                    ->assertPathIs('/akuntansi/pesanan-penjualan/'.$kondisi['pesanan_terakhir'].'/edit')->pause('2000')
                    ->select2('#pelanggan_id', 'PT 456')->pause('2000')
                    ->script('window.scrollTo(0,180);');

            $browser->type('po_number', $kondisi['po'])->pause('2000')
                    ->type('so_number', $kondisi['so'])->pause('2000')
                    ->select2('#fob', 'Destination')->pause('2000')
                    ->select2('#term_id', '10/10/n30')->pause('2000')
                    ->select2('#ship_id', 'TIKI')->pause('2000')
                    ->script('window.scrollTo(0,600);');

            $browser->pause('2000')
                    ->select2('#produk_id0', 'Barang ABC')->pause('2000')
                    ->select2('#produk_id1', 'Barang DEF')->pause('2000');
        if(is_null('#produk_id2')){
            $browser->clickLink('Tambah')->pause('2000')
                    ->select2('#produk_id2', 'Barang GHI')->pause('2000');
        }else{
            $browser->select2('#produk_id2', 'Barang XYZ')->pause('2000');
        }
            $browser->type('keterangan', '[Edit] Pesanan Penjualan')->pause('2000')
                    ->type('diskon', '15')->pause('2000')
                    ->type('ongkir', '52000')->pause('2000')
                    ->script('window.scrollTo(0,850);');

            $browser->assertSee('Simpan')->pause('2000')
                    ->press('#btn-submit')->pause('2000')
                    ->press('.btn-warning')->pause('2000')
                    ->assertPathIs('/akuntansi/pesanan-penjualan')->pause('5000')
                    ->screenshot('Berhasil Melakukan Perubahan Terakhir Pesanan Penjualan')
                    ->press('.swal-button')->pause('5000')
//Logout Role
                    ->click('li.list-inline-item:nth-child(4)')->pause('2000')
                    ->click('.profile-dropdown > a:nth-child(3)')->pause('2000')
                    ->assertPathIs('/login')
                    ->pause('5000');
        });
    }
    public function KondisiPesanan(){
        $pesanan  =   DB::table('pesanan_penjualan')->orderBy('id', 'desc')->first();
        if(!is_null($pesanan)){
            return [
                'pesanan_terakhir'     =>      $pesanan->id,
                'po'                   =>      $pesanan->po_number,
                'so'                   =>      $pesanan->so_number,
            ];
        }
    }
}
