<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use DB;
use App\Modules\Akuntansi\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AktivitasJualPesananPenjualanCreateTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $kondisi    =   $this->KondisiPesanan();
            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->click('li.has-submenu:nth-child(7) > a:nth-child(1)', 'Aktivitas')->pause('2000')
                    ->mouseover('.megamenu > li:nth-child(1) > ul:nth-child(1) > li:nth-child(3) > a:nth-child(1)')->pause('5000')
                    ->clickLink('Pesanan Penjualan')->pause('2000')
                    ->assertPathIs('/akuntansi/pesanan-penjualan')->pause('2000')
                    ->clickLink('Tambah')->pause('2000')
                    ->assertPathIs('/akuntansi/pesanan-penjualan/create')->pause('2000')
                    
                    ->select2('#pelanggan_id', 'PT 456')->pause('2000')
                    ->press('.swal-button--confirm')->pause('2000')
                    ->check('penawaran[]')->pause('2000')
                    ->press('.modal-footer > button:nth-child(1)')->pause('2000')
                    ->script('window.scrollTo(0,250);');

            $browser->type('po_number', $kondisi['po'])->pause('2000')
                    ->type('so_number', $kondisi['so'])->pause('2000')
                    ->select2('#fob', 'Shiping Point')->pause('2000')
                    ->select2('#term_id', '5/10/n60')->pause('2000')
                    ->select2('#ship_id', 'JNE')->pause('2000')
                    ->type('keterangan', 'Pesanan Penjualan')->pause('2000')
                    ->type('diskon', '10')->pause('2000')
                    ->type('ongkir', '50000')->pause('2000')
                    ->script('window.scrollTo(0,850);');

            $browser->assertSee('Simpan')->pause('2000')
                    ->press('#btn-submit')->pause('2000')
                    ->press('.btn-warning')->pause('2000')
                    ->assertPathIs('/akuntansi/pesanan-penjualan')->pause('2000')
                    ->screenshot('Berhasil Melakukan Penambahan Penawaran Penjualan')
                    ->press('.swal-button')->pause('3000');
        });
    }
    public function KondisiPesanan(){
        $pesanan  =   DB::table('pesanan_penjualan')->orderBy('id', 'desc')->first();
        $po       =   0;
        $so       =   0;
        if(is_null($pesanan)){
            return [
                'pesanan_terakhir'      =>      1,
                'po'                    =>      $po    =   $po + 1,
                'so'                    =>      $so    =   $so + 1,
            ];
        }else{
            return [
                'pesanan_terakhir'      =>      $pesanan->id,
                'po'                    =>      $pesanan->po_number + 1,
                'so'                    =>      $pesanan->so_number + 1,
            ];
        }
    }
}
