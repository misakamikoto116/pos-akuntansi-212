<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Modules\Akuntansi\Models\User;

class ImportPemasokTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->maximize()
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->assertPathIs('/akuntansi')
                    ->assertSee('Dashboard')
                    ->click('li.has-submenu:nth-child(3) > a:nth-child(1)', 'Import')->pause('500')
                    ->mouseover('.navigation-menu > li:nth-child(3) > ul:nth-child(2) > li:nth-child(4) > a:nth-child(1)')->pause('500')
                    ->clickLink('Import Pemasok')->pause('500')
                    ->assertPathIs('/akuntansi/upload-pemasok')->pause('2000')
                    ->assertSee('Upload Pemasok')->pause('1000')
                    ->assertSee('File xlsx')->pause('1000')
                    ->attach('import_file', $this->fileImport())->pause('500')
                    ->press('#btn-submit')->pause('500')
                    ->press('.swal-button--confirm')->pause('500')
                    ->assertPathIs('/akuntansi/upload-pemasok')->pause('3000');
        });
    }

    public function fileImport() {
        $randomFile = glob(__DIR__."/File_attachments/Files/export-pemasokmaster.xlsx", GLOB_BRACE);
        return $randomFile[0];
    }
}
