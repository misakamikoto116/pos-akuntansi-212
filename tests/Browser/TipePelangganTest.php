<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Modules\Akuntansi\Models\User;
use App\Modules\Akuntansi\Models\TipePelanggan;
use Illuminate\Foundation\Testing\WithFaker;

class TipePelangganTest extends DuskTestCase
{
    public function setUp(){
        parent::setUp();
    }

    use withFaker;
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->maximize()
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')->pause('500')
                    ->assertSee('Dashboard')->pause('500')
                    ->click('li.has-submenu:nth-child(2) > a:nth-child(1)', 'Master')->pause('500')
                    ->mouseover('.navigation-menu > li:nth-child(2) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)')->pause('500')
                    ->click('.navigation-menu > li:nth-child(2) > ul:nth-child(2) > li:nth-child(6) > a:nth-child(1)', 'Tipe Pelanggan')->pause('500')
                    ->assertPathIs('/akuntansi/tipe-pelanggan')->pause('500');
            $this->CreateTipePelanggan( $browser );
            $browser->assertPathIs('/akuntansi/tipe-pelanggan')->pause('500');
            $this->UpdateTipePelanggan( $browser );
            $browser->assertPathIs('/akuntansi/tipe-pelanggan')->pause('500');
            $this->DeleteTipePelanggan( $browser );
            $browser->assertPathIs('/akuntansi/tipe-pelanggan')->pause('500');
            $browser->pause('3000');
        });
    }

    public function CreateTipePelanggan ( $browser )
    {
        $browser->clickLink('Tambah')->pause('500')
                ->assertPathIs('/akuntansi/tipe-pelanggan/create')->pause('500')
                ->assertSee('Tambah Tipe Pelanggan')->pause('500')
                ->assertSee('Nama')->pause('500')
                ->type('nama', $this->faker()->name)->pause('500')
                ->press('#btn-submit')->pause('500')
                ->press('.swal-button--confirm')->pause('500');
    }

    public function UpdateTipePelanggan ( $browser )
    {
        $tipePelanggan = TipePelanggan::get()->last();
        $browser->script('window.scrollTo( { top : 200, behavior : "smooth" } )');
        $browser->assertSee('Option')->pause('500')
                    ->press('#option'.$tipePelanggan['id'])->pause('500')
                    ->clickLink('Edit')->pause('500')
                    ->assertPathIs('/akuntansi/tipe-pelanggan/'.$tipePelanggan['id'].'/edit')->pause('500')
                    ->assertSee('Edit Tipe Pelanggan')->pause('500')
                    ->assertSee('Nama')->pause('500')
                    ->type('nama', $this->faker()->name)->pause('500')
                    ->press('.btn-default')->pause('500')
                    ->press('.swal-button--confirm')->pause('500');
    }

    public function DeleteTipePelanggan ( $browser )
    {
        $tipePelanggan = TipePelanggan::get()->last();
        $browser->script('window.scrollTo( { top : 200, behavior : "smooth" } )');
        $browser->assertSee('Option')->pause('500')
                    ->press('#option'.$tipePelanggan['id'])->pause('500')
                    ->clickLink('Delete')->pause('500')
                    ->press('.swal-button--danger')->pause('500')
                    ->press('.swal-button--confirm')->pause('500');
        
    }
}
