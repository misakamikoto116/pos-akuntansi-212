<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use DB;
use App\Modules\Akuntansi\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AktivitasJualPenawaranPenjualanEditTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $kondisi    =   $this->KondisiPenawaran();
            
            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->click('li.has-submenu:nth-child(7) > a:nth-child(1)', 'Aktivitas')->pause('2000')
                    ->mouseover('.megamenu > li:nth-child(1) > ul:nth-child(1) > li:nth-child(2) > a:nth-child(1)')->pause('5000')
                    ->clickLink('Penawaran Penjualan')->pause('2000')
                    ->assertPathIs('/akuntansi/penawaran-penjualan')->pause('2000')
                    ->script('window.scrollTo(0, document.body.scrollHeight)');
                    
            $browser->assertSee('Option')->pause('2000')
                    ->press('#option'.$kondisi['penawaran_terakhir'])->pause('2000')
                    ->click('.show > div:nth-child(2) > a:nth-child(2)')->pause('2000')
                    ->assertPathIs('/akuntansi/penawaran-penjualan/'.$kondisi['penawaran_terakhir'].'/edit')->pause('2000')
                    ->select2('#pelanggan_id', 'PT 456')->pause('2000')
                    ->assertSee('Quote Number')->pause('2000')
                    ->type('no_penawaran', $kondisi['fob'])->pause('2000')
                    ->script('window.scrollTo(0,400);');
                    
            $browser->pause('5000')
                    ->select2('#produk_id0', 'Barang DEF')->pause('2000');
            if(!is_null('#produk_id1')){
                $browser->select2('#produk_id1', 'Barang GHI')->pause('2000');
            }else{
                $browser->clickLink('Tambah')->pause('2000')
                        ->select2('#produk_id1', 'Barang XYZ')->pause('2000');
            }
            $browser->script('window.scrollTo(0, document.body.scrollHeight)');

            $browser->pause('5000')
                    ->type('keterangan', '[Edit] Penawaran Penjualan')
                    ->type('diskon', '53')->pause('2000')
                    ->type('ongkir', '50000')->pause('2000')
                    ->assertSee('Simpan')->pause('2000')
                    ->press('#btn-submit')->pause('2000')
                    ->assertSee('Simpan & Tutup')->pause('2000')
                    ->press('ul.dropdown-menu > li:nth-child(2)')->pause('2000')
                    ->assertPathIs('/akuntansi/penawaran-penjualan')->pause('2000')
                    ->screenshot('Berhasil Melakukan Perubahan Terakhir Penawaran Penjualan')
                    ->press('.swal-button')->pause('5000');

        });
    }
    public function KondisiPenawaran(){
        $penawaran  =   DB::table('penawaran_penjualan')->orderBy('id', 'desc')->first();
        if(!is_null($penawaran)){
            return [
                'penawaran_terakhir'    =>      $penawaran->id,
                'fob'                   =>      $penawaran->no_penawaran,
            ];
        }
    }
}
