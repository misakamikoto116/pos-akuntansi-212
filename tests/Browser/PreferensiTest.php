<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use App\Modules\Akuntansi\Models\User;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\PreferensiMataUang;
use App\Modules\Akuntansi\Models\PreferensiBarang;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PreferensiTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $dataPreferensiMataUang  = $this->PreferensiMataUang();
            $dataPreferensiBarang    = $this->PreferensiBarang();
            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->assertPathIs('/akuntansi')
                    ->assertSee('Dashboard')
                    ->click('li.has-submenu:nth-child(2) > a:nth-child(1)', 'Master')->pause('500')
                    ->mouseover('.navigation-menu > li:nth-child(2) > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)')->pause('500')
                    ->click('.navigation-menu > li:nth-child(2) > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)')->pause('500')
                    ->assertPathIs('/akuntansi/preferensi')
                    ->assertSee('Preferensi')->pause('400');
            $this->tabAkunDefaultMataUang( $browser, $dataPreferensiMataUang);
            $browser->assertPathIs('/akuntansi/preferensi');
            $this->tabAkunDefaultBarang( $browser, $dataPreferensiBarang);
            $browser->assertPathIs('/akuntansi/preferensi');
            $this->tabAkunNotifikasi( $browser, $dataPreferensiBarang);
            $browser->assertPathIs('/akuntansi/preferensi');
            $browser->pause('1000');
        });
    }

    public function tabAkunDefaultMataUang ( $browser, $dataPreferensi )
    {
        $browser->script('window.scrollTo( { top : 0, behavior : "smooth" } )');
        $browser->pause('500')
                ->assertSee('Akun Default Mata Uang')
                ->clickLink('Akun Default Mata Uang')->pause('400')
                ->assertSee('Akun Hutang')
                ->select2('.akun_hutang_id', $dataPreferensi['akun_hutang_id'] )->pause('500')
                ->assertSee('Akun Piutang')
                ->select2('.akun_piutang_id ', $dataPreferensi['akun_piutang_id'] )->pause('500')
                ->assertSee('Uang Muka Pembelian')
                ->select2('.uang_muka_pembelian_id', $dataPreferensi['uang_muka_pembelian_id'] )->pause('500')
                ->assertSee('Uang Muka Penjualan')
                ->select2('.uang_muka_penjualan_id', $dataPreferensi['uang_muka_penjualan_id'] )->pause('500')
                ->script('window.scrollTo( { top : document.body.scrollHeight, behavior : "smooth" })');
        $browser->assertSee('Diskon Penjualan')
                ->select2('.diskon_penjualan_id', $dataPreferensi['diskon_penjualan_id'] )->pause('500')
                ->assertSee('Laba/Rugi Terealisir')
                ->select2('.laba_rugi_terealisir_id', $dataPreferensi['laba_rugi_terealisir_id'] )->pause('500')
                ->assertSee('Laba/Rugi Tak Terealisir')
                ->select2('.laba_rugi_tak_terealisir_id', $dataPreferensi['laba_rugi_tak_terealisir_id'] )->pause('500')
                ->assertSee('Akun Penyesuaian')
                ->select2('.akun_penyesuaian_id', $dataPreferensi['akun_penyesuaian_id'] )->pause('500')
                ->press('#btn-submit')->pause('1000')
                ->press('.swal-button--confirm')->pause('500');
    }

    public function tabAkunDefaultBarang ( $browser, $dataPreferensi )
    {
        $browser->script('window.scrollTo( { top : 250, behavior : "smooth" } )');
        $browser->pause('500')
                ->assertSee('Akun Default Barang')
                ->clickLink('Akun Default Barang')->pause('400')
                ->assertSee('Akun Persediaan')
                ->select2('.akun_persediaan_id ', $dataPreferensi['akun_persediaan_id'] )->pause('500')
                ->assertSee('Akun Penjualan')
                ->select2('.akun_penjualan_id ', $dataPreferensi['akun_penjualan_id'] )->pause('500')
                ->assertSee('Akun Retur Penjualan')
                ->select2('.akun_retur_penjualan_id', $dataPreferensi['akun_retur_penjualan_id'] )->pause('500')
                ->assertSee('Akun Diskon Barang')
                ->select2('.akun_diskon_barang_id', $dataPreferensi['akun_diskon_barang_id'] )->pause('500')
                ->assertSee('Akun Barang Terkirim')
                ->select2('.akun_barang_terkirim_id', $dataPreferensi['akun_barang_terkirim_id'] )->pause('500')
                ->assertSee('Akun HPP')
                ->select2('.akun_hpp_id ', $dataPreferensi['akun_hpp_id'] )->pause('500')
                ->assertSee('Akun Retur Pembelian')
                ->select2('.akun_retur_pembelian_id', $dataPreferensi['akun_retur_pembelian_id'] )->pause('500')
                ->script('window.scrollTo( { top : document.body.scrollHeight, behavior : "smooth" })');
        $browser->assertSee('Akun Beban')
                ->select2('.akun_beban_id', $dataPreferensi['akun_beban_id'] )->pause('500')
                ->assertSee('Akun Belum Tertagih')
                ->select2('.akun_belum_tertagih_id', $dataPreferensi['akun_belum_tertagih_id'] )->pause('500')
                ->press('#btn-submit-default-barang')->pause('1000')
                ->press('.swal-button--confirm')->pause('500');
    }

    public function tabAkunNotifikasi ( $browser, $dataPreferensi )
    {
        $browser->script('window.scrollTo( { top : 0, behavior : "smooth" } )');
        $browser->pause('400')
                ->assertSee('Notifikasi Barang')
                ->clickLink('Notifikasi Barang')->pause('400')
                ->assertSee('Minimal Stock')->pause('500')
                ->type('minimal_stock', $dataPreferensi['minimal_stock'])->pause('500')
                ->assertSee('Estimasi Kada Luarsa')->pause('500')
                ->type('estimasi_kada_luarsa', $dataPreferensi['estimasi_kada_luarsa'])->pause('500')
                ->press('#btn-submit-notifikasi')->pause('1000')
                ->press('.swal-button--confirm')->pause('500');
    }

    public function PreferensiMataUang () {
        $preferensiMataUang = PreferensiMataUang::first();
        return [
            'akun_hutang_id'                => $preferensiMataUang->akunHutang->nama_akun,
            'akun_piutang_id'               => $preferensiMataUang->akunPiutang->nama_akun,
            'uang_muka_pembelian_id'        => $preferensiMataUang->akunUangMukaPembelian->nama_akun,
            'uang_muka_penjualan_id'        => $preferensiMataUang->akunUangMukaPenjualan->nama_akun,
            'diskon_penjualan_id'           => $preferensiMataUang->akunDiskonPenjualan->nama_akun,
            'laba_rugi_terealisir_id'       => $preferensiMataUang->akunLabaRugiTerRealisir->nama_akun,
            'laba_rugi_tak_terealisir_id'   => $preferensiMataUang->akunLabaRugiTakTerRealisir->nama_akun,
            'akun_penyesuaian_id'           => $preferensiMataUang->akunPenyesuaian->nama_akun,
        ];
    }

    public function PreferensiBarang () {
        $preferensiBarang   = PreferensiBarang::first();
        return [
            'akun_persediaan_id'        =>  $preferensiBarang->akunPersediaan->nama_akun,
            'akun_penjualan_id'         =>  $preferensiBarang->akunPenjualan->nama_akun,
            'akun_retur_penjualan_id'   =>  $preferensiBarang->akunReturPenjualan->nama_akun,
            'akun_diskon_barang_id'     =>  $preferensiBarang->akunDiskonBarang->nama_akun,
            'akun_barang_terkirim_id'   =>  $preferensiBarang->akunBarangTerkirim->nama_akun,
            'akun_hpp_id'               =>  $preferensiBarang->akunHpp->nama_akun,
            'akun_retur_pembelian_id'   =>  $preferensiBarang->akunReturPembelian->nama_akun,
            'akun_beban_id'             =>  $preferensiBarang->akunBeban->nama_akun,
            'akun_belum_tertagih_id'    =>  $preferensiBarang->akunBelumTertagih->nama_akun,
            'minimal_stock'             =>  $preferensiBarang->minimal_stock,
            'estimasi_kada_luarsa'      =>  $preferensiBarang->estimasi_kada_luarsa,
        ];
    }
}
