<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class GetStarted extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->resize(1920, 1080)
                    ->visit('/get-started')->pause('2000')
                    ->assertSee('Getting Started')->pause('2000')
                    ->script('window.scrollTo(0,150);');

            $browser->type('nama_perusahaan', 'Chichi Jaya Bung Tomo')->pause('2000')
                    ->type('alamat', 'Jalan Bung Tomo No. 80')->pause('2000')
                    ->type('tanggal_mulai', '2018-01-01')->pause('2000')
                    ->select2('#mata_uang_id', 'Rupiah')->pause('2000')
                    ->clickLink('Next')->pause('2000') // Tahap 1
                    ->clickLink('Next')->pause('2000') // Tahap 2
                    ->clickLink('Next')->pause('2000') // Tahap 3
                    ->clickLink('Next')->pause('2000') // Tahap 4
                    ->clickLink('Next')->pause('2000') // Tahap 5
                    ->clickLink('Next')->pause('2000') // Tahap 6
                    ->clickLink('Finish')->pause('2000') // Tahap 7
                    ->press('.swal-button--confirm')->pause('2000')
                    ->screenshot('Berhasil Registrasi Wizzard')
                    ->pause('5000');
        });
    }
}
