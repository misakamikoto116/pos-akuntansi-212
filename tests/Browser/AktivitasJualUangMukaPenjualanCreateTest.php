<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use DB;
use App\Modules\Akuntansi\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AktivitasJualUangMukaPenjualanCreateTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $kondisi        =       $this->KondisiUangMuka();
            
            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->click('li.has-submenu:nth-child(7) > a:nth-child(1)', 'Aktivitas')->pause('2000')
                    ->mouseover('.megamenu > li:nth-child(1) > ul:nth-child(1) > li:nth-child(8) > a:nth-child(1)')->pause('5000')
                    ->clickLink('Uang Muka Pelanggan')->pause('2000')
                    ->assertPathIs('/akuntansi/uang-muka-pelanggan')->pause('2000')
                    ->clickLink('Tambah')->pause('2000')
                    ->assertPathIs('/akuntansi/uang-muka-pelanggan/create')->pause('2000')
                    ->select2('#pelanggan_id', 'PT 123')->pause('2000')
                    ->type('no_faktur', $kondisi['faktur'])->pause('2000')
                    ->type('unit_price', 1000000)->pause('2000')
                    ->type('catatan', 'Uang Muka Pelanggan')->pause('2000')
                    ->script('window.scrollTo(0, document.body.scrollHeight)');

            $browser->assertSee('Simpan')->pause('2000')
                    ->press('#btn-submit')->pause('2000')
                    ->press('button.btn-warning:nth-child(1)')->pause('2000')
                    ->assertPathIs('/akuntansi/uang-muka-pelanggan')->pause('2000')
                    ->screenshot('Berhasil Melakukan Penambahan Uang Muka pelanggan')
                    ->press('.swal-button--confirm')->pause('3000');
        });
    }

    public function KondisiUangMuka(){
        $pelanggan      =   DB::table('informasi_pelanggan')->find(1);
        $uang_muka      =   DB::table('faktur_penjualan')->where('pelanggan_id', $pelanggan->id)->orderBy('id', 'desc')->first();
        $nofak          =   DB::table('faktur_penjualan')->where('uang_muka', '1')->get();
        if(is_null($uang_muka)){
            return[
                'uang_muka_terakhir'        =>     0,
                'faktur'                    =>     1,
            ];
        }else{
            return[
                'uang_muka_terakhir'        =>     $uang_muka->id,
                'faktur'                    =>     $nofak['0']->no_faktur + 1,
            ];
        }
    }
}
