<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use DB;
use App\Modules\Akuntansi\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AktivitasJualPenerimaanPenjualanCreateTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $kondisi        =       $this->KondisiPenerimaan();

            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->click('li.has-submenu:nth-child(7) > a:nth-child(1)', 'Aktivitas')->pause('2000')
                    ->mouseover('.megamenu > li:nth-child(1) > ul:nth-child(1) > li:nth-child(6) > a:nth-child(1)')->pause('5000')
                    ->clickLink('Penerimaan Penjualan')->pause('2000')
                    ->assertPathIs('/akuntansi/penerimaan-penjualan')->pause('2000')
                    ->clickLink('Tambah')->pause('2000')
                    ->assertPathIs('/akuntansi/penerimaan-penjualan/create')->pause('2000')
                    ->select2('#pelanggan_id', 'PT 123')->pause('2000')
                    ->type('form_no', $kondisi['no_form'])->pause('2000')
                    ->type('payment_date', '08022018')->pause('2000')
                    ->select2('#akun_id', 'Kas')->pause('2000')
                    ->type('cheque_no', $kondisi['cheq_no'])->pause('2000')
                    ->script('window.scrollTo(0, document.body.scrollHeight)');

            $browser->value('#payment_amount1', number_format("1000",2))->pause('2000')
                    ->check('status_pay[]')->pause('2000');

            $browser->assertSee('Simpan')->pause('2000')
                    ->press('#btn-submit')->pause('2000')
                    ->press('button.btn-warning:nth-child(1)')->pause('2000')
                    ->assertPathIs('/akuntansi/penerimaan-penjualan')->pause('2000')
                    ->screenshot('Berhasil Melakukan Penambahan Penerimaan Penjualan')
                    ->press('.swal-button--confirm')->pause('3000');
        });
    }
    public function KondisiPenerimaan(){
        $penerimaan     =   DB::table('penerimaan_penjualan')->orderBy('id', 'desc')->first();
        if(is_null($penerimaan)){
            return [
                'penerimaan_terakhir'       =>      0,
                'no_form'                   =>      1,
                'cheq_no'                   =>      1,
            ];
        }else{
            return [
                'penerimaan_terakhir'       =>      $penerimaan->id,
                'no_form'                   =>      $penerimaan->form_no + 1,
                'cheq_no'                   =>      $penerimaan->cheque_no + 1,
            ];
        }
    }
}
