<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Modules\Akuntansi\Models\User;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
use App\Modules\Akuntansi\Models\JasaPengiriman;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\Akun;

class PenjualanPembelianTest extends DuskTestCase
{

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $dataUser = User::where('name', 'akuntansi')->first();

        $this->browse(function (Browser $browser) use ($dataUser) {
            if ($dataUser !== null) {
                $browser->loginAs($dataUser)
                        ->visit('/akuntansi')
                        ->assertSee('Dashboard');

                $data = [
                    'fob'       =>  collect(['Shiping Point', 'Destination']),
                    'ship_id'   =>  JasaPengiriman::pluck('nama'),
                    'term_id'   =>  SyaratPembayaran::pluck('akan_dapat_diskon'),
                    'barang_id' =>  Produk::where('tipe_barang',0)->whereHas('saldoAwalBarang', function($query){
                                        return $query->where('kuantitas','>',0);
                                    })->get(),
                    'akun'      =>  Akun::pluck('nama_akun'),
                    'akun_bank' =>  Akun::whereHas('tipeAkun', function($item){
                                        $item->where('title','Kas/Bank');
                                    })->pluck('nama_akun')
                ];

                # Berapa jumlah transaksi penjualan & pembelian yang diinginkan
                for ($i=0; $i < 3; $i++) { 
                    $random = rand(0,1);

                    if ($random == 1) {
                        $this->runPenjualan($browser, $data);
                    } else {
                        $this->runPembelian($browser, $data);
                    }
                }
            }
        });
    }

    private function runPenjualan($browser, $data)
    {
        $date = rand(1, 30).' November 2018';
        /** Pesanan */
        $pelanggan = InformasiPelanggan::where('alamat','!=',NULL)->get();
        $browser->visit('/akuntansi/pesanan-penjualan')
                ->assertSee('Pesanan Penjualan')
                ->clickLink('Tambah')
                ->assertSee('Sales Order / Pesanan Penjualan')
                ->select2('#pelanggan_id', $pelanggan->random()->nama, 5)->pause('1000')
                ->script('window.scrollTo(0,300);');

        $browser->select2('#fob', $data['fob']->random(), 5)->pause('1000')
                ->select2('#term_id', $data['term_id']->random(), 5)->pause('1000')
                ->select2('#ship_id', $data['ship_id']->random(), 5)->pause('1000');
        
        $browser->keys('#so_date', ['{control}', 'a'], $date);
        $browser->keys('#ship_date', ['{control}', 'a'], $date);

        $browser->script('window.scrollTo(0,500);');

        for ($i=0; $i < rand(1, 5); $i++) { 
            $barang = $data['barang_id']->random();
            $browser->click('.add-pesanan')
                    ->select2('#produk_id'.$i, $barang->keterangan, 15)->pause('1000')
                    ->keys('#qty_produk'.$i, ['{backspace}', rand(1, 5)])
                    ->pause('2000');
            
            if ($browser->element('kosong')){
                $browser->script('window.scrollTo(300,500)');
                $browser->press('remove-rincian-pesanan');
            }
        }

        $ongkir = array_random(['0','10000','25000']);
        $browser->script('window.scrollTo(0,900);');
        $browser->type('ongkir', $ongkir)->pause('2000');
        $browser->type('keterangan', 'Pesanan by unit testing')->pause('2000');

        $browser->press('Simpan')->pause('1000');
        $browser->press('Kirim Pesanan')
                ->pause('2500')
                ->assertSee('Pengiriman Penjualan');
        /** End of Pesanan */

        /** Start Pengiriman Penjualan */
        $browser->pause('2500');
        $browser->script('swal.close()');
        $browser->keys('#delivery_date', ['{control}', 'a'], $date);
        $browser->script('window.scrollTo(0,900);');
        $browser->type('keterangan', 'Pengiriman by unit testing')->pause('2000');
        $browser->press('Simpan')->pause('1000')->press('Faktur')
                ->pause('2500')
                ->assertSee('Faktur Penjualan');
        /** End of Pengiriman */

        /** Start Faktur Penjualan */
        $browser->pause('2500');
        $browser->script('swal.close()');

        $browser->keys('#invoice_date', ['{control}', 'a'], $date);
        $browser->keys('#ship_date', ['{control}', 'a'], $date);
        
        $browser->script('window.scrollTo(0,1300);');
        
        if($ongkir > 0){
            $browser->select2('.akun_id', $data['akun']->random(), 5)->pause('1000');
        }
        $browser->select2('akun_piutang_id', $data['akun_bank']->random(), 5)->pause('1000');

        $browser->press('Simpan')->pause('1000')->press('Terima Bayaran')
                ->pause('2500')
                ->assertSee('Penerimaan Penjualan');
        /** End of Faktur Penjualan */

        /** Start of Penerimaan Penjualan */
        $browser->pause('2500')->press('.swal-button--confirm');
        $browser->script('window.scrollTo(0,200);');

        $browser->select2('#akun_id', 'IDR', 5)->pause('1000');
        $browser->keys('#payment_date', ['{control}', 'a'], $date);

        $browser->script('window.scrollTo(0,900);');
        
        $browser->keys('.owing', ['{control}', 'a'])->pause('800');
        $browser->keys('.owing', ['{control}', 'c'])->pause('800');
        $browser->keys('.payment_amount', ['{control}', 'a'])->pause('2000');
        $browser->keys('.payment_amount', ['{control}', 'v'])->pause('2000');

        $browser->press('Simpan')->pause('1000')->press('Simpan & Tutup')
                ->pause('3000')
                ->assertSee('Penerimaan Penjualan');

        /** End of Penerimaan Penjualan */

    }

    private function runPembelian($browser, $data)
    {
        $date = rand(1, 30)." November 2018";
        /** Pesanan */
        $pemasok = InformasiPemasok::where('alamat','!=',NULL)->get();
        $browser->visit('/akuntansi/pesanan-pembelian')
                ->assertSee('Pesanan Pembelian')
                ->clickLink('Tambah')
                ->assertSee('Purcase Order / Pesanan Pembeliaan')
                ->select2('#pemasok_id', $pemasok->random()->nama, 5)->pause('1000')
                ->script('window.scrollTo(0,300);');

        $browser->select2('#fob', $data['fob']->random(), 5)->pause('1000')
                ->select2('#term_id', $data['term_id']->random(), 5)->pause('1000')
                ->select2('#ship_id', $data['ship_id']->random(), 5)->pause('1000');
        
        $browser->keys('#po_date', ['{control}', 'a'], $date);
        $browser->keys('#expected_date', ['{control}', 'a'], $date);

        $browser->script('window.scrollTo(0,500);');
        
        for ($i=0; $i < rand(1, 5); $i++) {
            $barang = $data['barang_id']->random();
            $browser->click('.add-penawaran')
                    ->select2('#produk_id'.$i, $barang->keterangan, 15)->pause('1000')
                    ->keys('#qty_produk'.$i, ['{backspace}', rand(1, 5)])
                    ->pause('2000');

            if ($browser->element('kosong')){
               $browser->script('window.scrollTo(300,500)');
               $browser->press('remove-rincian-penawaran'); 
            }
        }
        $ongkir = array_random(['0','10000','25000']);
        $browser->script('window.scrollTo(0,900);');
        $browser->type('ongkir', $ongkir)->pause('2000');
        $browser->script('$("#catatan").focus()');
        $browser->script('window.scrollTo(0,1100);');
        $browser->type('keterangan', 'Pesanan by unit testing')->pause('2000');
        if($ongkir > 0){
            $browser->select2('akun_ongkir_id', rand(1,10), 5)->pause('1000');            
        }
        $browser->press('Simpan')->pause('1000');
        $browser->press('Terima Barang')
                ->pause('2500')
                ->assertSee('Penerimaan Barang');
        /** End of Pesanan */

        /** Start Penerimaan Pembelian*/
        $browser->pause('2500')->press('.swal-button--confirm');

        $browser->keys('#receive_date', ['{control}', 'a'], $date);
        $browser->keys('#ship_date', ['{control}', 'a'], $date);

        $browser->script('window.scrollTo(0,900);');
        $browser->type('keterangan', 'Penerimaan by unit testing')->pause('2000');
        $browser->press('Simpan')->pause('1000')->press('Buat Faktur')
                ->pause('2500')
                ->assertSee('Faktur Pembelian');
        /** End of Penerimaan Pembelian */

        /** Start Faktur Pembelian */
        $browser->pause('2500');
        $browser->script('swal.close()');

        $browser->keys('#invoice_date', ['{control}', 'a'], $date);
        $browser->keys('#ship_date', ['{control}', 'a'], $date);

        $browser->script('window.scrollTo(0,1200);');

        $browser->press('Simpan')->pause('1000')->press('Bayar Pemasok')
                ->pause('2500')
                ->assertSee('Pembayaran Pembelian');

        /** End of Faktur Pembelian */

        /** Start Pembayaran Pembelian */

        $browser->pause('2500')->press('.swal-button');
        $browser->script('window.scrollTo(0,200);');
        $browser->select2('#akun_id', 'IDR', 5)->pause('1000');
        $browser->keys('#payment_date', ['{control}', 'a'], $date);
        
        $browser->script('window.scrollTo(0,700);');
        $browser->keys('.owing',['{control}', 'a'])->pause('800');
        $browser->keys('.owing',['{control}', 'c'])->pause('800');
        $browser->keys('.payment_amount', ['{control}', 'a'])->pause('2000');        
        $browser->keys('.payment_amount', ['{control}', 'v'])->pause('2000');        

        $browser->press('Simpan')->pause('1000')->press('Simpan & Tutup')
                ->pause('3000')
                ->assertSee('Pembayaran Pembelian');

        /** End of Pembayaran Pembelian */

    }
}
