<?php

namespace Tests\Browser;

use App\Modules\Akuntansi\Models\User;
use App\Modules\Pos\Models\PreferensiPos;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PreferensiPOSTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $dataPreferensi     =       $this->PreferensiPOS();
            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->assertPathIs('/akuntansi')
                    ->assertSee('Dashboard')
                    ->click('li.has-submenu:nth-child(2) > a:nth-child(1)', 'Master')->pause('500')
                    ->mouseover('.navigation-menu > li:nth-child(2) > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)')->pause('500')
                    ->click('.navigation-menu > li:nth-child(2) > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)')->pause('500')
                    ->assertPathIs('/akuntansi/preferensi-pos')
                    ->assertSee('Preferensi')->pause('400');
            $this->tabAkunDefaultPOS( $browser, $dataPreferensi);
            $browser->assertPathIs('/akuntansi/preferensi-pos');
            $this->tabAkunDefaultPajak( $browser, $dataPreferensi);
            $browser->assertPathIs('/akuntansi/preferensi-pos');
            $this->tabAkunModal( $browser, $dataPreferensi);
            $browser->assertPathIs('/akuntansi/preferensi-pos');
            $browser->pause('1000');
        });
    }

    public function tabAkunDefaultPOS ( $browser, $dataPreferensi )
    {
        $browser->script('window.scrollTo( { top : 0, behavior : "smooth" } )');
        $browser->pause('500')
                ->assertSee('Akun Default POS')
                ->clickLink('Akun Default POS')->pause('400')
                ->assertSee('Akun Cash')
                ->select2('#cash_id', $dataPreferensi['akun_cash_id'] )->pause('500')
                ->assertSee('Akun Piutang')
                ->select2('#hutang_id', $dataPreferensi['akun_hutang_id'] )->pause('500')
                ->assertSee('Akun Diskon Penjualan')
                ->select2('#diskon_penjualan_id', $dataPreferensi['diskon_penjualan_id'] )->pause('500')
                ->assertSee('Akun Bank')
                ->select2('#bank_id', $dataPreferensi['akun_bank_id'] )->pause('500')
                ->press('#btn-submit-default-pos')->pause('1000')
                ->press('.swal-button--confirm')->pause('500');
    }

    public function tabAkunDefaultPajak ( $browser, $dataPreferensi )
    {
        $browser->script('window.scrollTo( { top : 250, behavior : "smooth" } )');
        $browser->pause('500')
                ->assertSee('Akun Default Pajak')
                ->clickLink('Akun Default Pajak')->pause('400')
                ->assertSee('Kode Pajak')
                ->assertSee('Status');
        $randomStatus = rand( 0, 1 );
        if ( $randomStatus == 1 ) {
            $browser->check('status');
        } else if ( $randomStatus == 0 ) {
            $browser->uncheck('status');
        }
        $browser->select2('#pajak_id ', $dataPreferensi['kode_pajak_id'] )->pause('500')
                ->press('#btn-submit-default-pajak')->pause('1000')
                ->press('.swal-button--confirm')->pause('500');
    }

    public function tabAkunModal ( $browser, $dataPreferensi )
    {
        $browser->script('window.scrollTo( { top : 0, behavior : "smooth" } )');
        $browser->pause('400')
                ->assertSee('Default Modal')
                ->clickLink('Default Modal')->pause('400')
                ->assertSee('Modal Awal')->pause('500')
                ->type('uang_modal', rand(0, 1000000))->pause('500')
                ->press('#btn-submit-default-modal')->pause('1000')
                ->press('.swal-button--confirm')->pause('500');
    }

    public function PreferensiPOS () {
        $preferensiPOS   = PreferensiPos::first();

        return [
            'akun_cash_id'          =>  $preferensiPOS->akunCash->nama_akun,
            'akun_hutang_id'        =>  $preferensiPOS->akunHutang->nama_akun,
            'diskon_penjualan_id'   =>  $preferensiPOS->diskonPenjualan->nama_akun,
            'akun_bank_id'          =>  $preferensiPOS->akunBank->nama_akun,
            'kode_pajak_id'         =>  $preferensiPOS->kodePajak->nama . ' - ' .$preferensiPOS->kodePajak->nilai . '%',
            'status'                =>  $preferensiPOS->status,
        ];
    }
}
