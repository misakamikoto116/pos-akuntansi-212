<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use DB;
use App\Modules\Akuntansi\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AktivitasJualFaktruPenjualanEditTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $kondisi    =   $this->KondisiFaktur();
            
            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->click('li.has-submenu:nth-child(7) > a:nth-child(1)', 'Aktivitas')->pause('2000')
                    ->mouseover('.megamenu > li:nth-child(1) > ul:nth-child(1) > li:nth-child(5) > a:nth-child(1)')->pause('5000')
                    ->clickLink('Faktur Penjualan')->pause('2000')
                    ->assertPathIs('/akuntansi/faktur-penjualan')->pause('2000')
                    ->script('window.scrollTo(0, document.body.scrollHeight)');

            $browser->assertSee('Option')->pause('2000')
                    ->press('#option'.$kondisi['faktur_terakhir'])->pause('2000')
                    ->click('.show > div:nth-child(2) > a:nth-child(2)')->pause('2000')
                    ->assertPathIs('/akuntansi/faktur-penjualan/'.$kondisi['faktur_terakhir'].'/edit')->pause('2000')
                    ->select2('#pelanggan_id', 'PT 123')->pause('2000')
                    ->type('no_faktur', $kondisi['no_faktur'])->pause('2000')
                    ->type('no_fp_std', $kondisi['no_fp_std'])->pause('2000')
                    ->type('no_fp_std2', $kondisi['no_fp_std2'])->pause('2000');
            $hari_ini = now();
            $browser->keys('#no_fp_std_date', $hari_ini->day)
                    ->keys('#no_fp_std_date', $hari_ini->month)
                    ->keys('#no_fp_std_date', $hari_ini->year)
                    ->type('ongkir', 25000)->pause('2000')
                    ->type('diskon', 15)->pause('2000')
                    ->select2('#akun_ongkir_id', 'Barang Terkirim')->pause('2000')
                    ->script('window.scrollTo(0, document.body.scrollHeight)');

            $browser->assertSee('Simpan')->pause('2000')
                    ->press('#btn-submit')->pause('2000')->press('.btn-warning')->pause('2000')
                    ->assertPathIs('/akuntansi/faktur-penjualan')->pause('2000')
                    ->screenshot('Berhasil Melakukan Perubahan Faktur Penjualan')
                    ->press('.swal-button')->pause('3000');
        });
    }
    public function KondisiFaktur(){
        $faktur         =   DB::table('faktur_penjualan')->orderBy('id', 'desc')->first();
        if(!is_null($faktur)){
            return [
                'faktur_terakhir'   => $faktur->id,
                'no_faktur'         => $faktur->no_faktur + 1 ,
                'no_fp_std'         => $faktur->no_fp_std,
                'no_fp_std2'        => $faktur->no_fp_std2  ,
            ];
        }
    }
}
