<?php

namespace Tests\Browser;


use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Modules\Akuntansi\Models\User;
use App\Modules\Akuntansi\Models\SaldoAwalBarang as DetailProduk;

class PenjualanTest extends DuskTestCase
{
    /**
     * A basic test example.
     *
     * @test
     */

    public function penjualan()
    {
        $dataUser = User::where('name','pos')->first();
        
        $this->browse(function (Browser $browser) use ($dataUser) {
            if ($dataUser !== null) {
                $browser->loginAs($dataUser)
                        ->visit('/pos')
                        ->clickLink('Penjualan')
                        ->assertSee('Check In')
                        ->select2('#kasir_mesin', 'kasir 1')
                        ->click('.btn-check-in')
                        ->assertSee('Penjualan');

                $barangRandom = DetailProduk::where('status',0)
                                            ->whereNotNull('item_id')
                                            ->whereNotNull('item_type')
                                            ->whereHas('produk')->get()->shuffle();


                $y = 0;
                for($i = 0; $i < 1000; $i++){
                    $randInt = rand(1,15);
                    $bayar = 0;
                    for($x = 0; $x < $randInt; $x++){
                        $jumlah = rand(1,2);
                        $browser->type('.qty_input', $jumlah) 
                                ->pause(2000)
                                ->type('barcode_barang', $barangRandom[$y]->produk->no_barang)
                                ->pause(2000)
                                ->keys('.barcode_barang',['{enter}',$barangRandom[$y]->produk->no_barang])
                                ->pause(2000);
                        $bayar += $barangRandom[$y]->produk->harga_jual * $jumlah;
                        $y++;
                        if ($y == 20) {
                            $y = 0;
                        }
                    }
    
                    $browser->script("$('.btn-promo').click()");

                    //$total = $browser->script("$('.total-number').data('total')");
                    //$diskon = $browser->script("$('.nilai_diskon').autoNumeric('get')");
                    //$grandTotal = ($total - $diskon);

                    $browser->type('.nilai_bayar', $bayar);
                    $browser->pause(2000)
                            ->script("$('.btn-store').click()");

                    

                    $browser->pause(6000)
                            ->assertSee('Transaksi Berhasil Dilakukan');
                }
            }
        });

    }
}
