<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use DB;
use App\Modules\Akuntansi\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AktivitasJualPenawaranPenjualanCreateTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $kondisi    =   $this->KondisiPenawaran();

            $browser->resize(1920, 1080)
                    ->loginAs(User::find(2))
                    ->visit('/akuntansi')
                    ->click('li.has-submenu:nth-child(7) > a:nth-child(1)', 'Aktivitas')->pause('2000')
                    ->mouseover('.megamenu > li:nth-child(1) > ul:nth-child(1) > li:nth-child(2) > a:nth-child(1)')->pause('5000')
                    ->clickLink('Penawaran Penjualan')->pause('2000')
                    ->assertPathIs('/akuntansi/penawaran-penjualan')->pause('2000')
                    ->clickLink('Tambah')->pause('2000')
                    ->assertPathIs('/akuntansi/penawaran-penjualan/create')->pause('2000')
                    ->select2('#pelanggan_id', 'PT 123')->pause('2000')
                    ->assertSee('Quote Number')->pause('2000')
                    ->type('no_penawaran', $kondisi['fob'])->pause('2000')
                    ->script('window.scrollTo(0,400);');

            $browser->pause('5000')
                    ->clickLink('Tambah')->pause('2000')
                    ->select2('#produk_id0', 'Barang ABC')->pause('2000')
                    ->script('window.scrollTo(0, document.body.scrollHeight)');

            $browser->pause('5000')
                    ->type('keterangan', 'Penawaran Penjualan')->pause('2000')
                    ->type('diskon', '10')->pause('2000')
                    ->type('ongkir', '5000')->pause('2000')
                    ->assertSee('Simpan')->pause('2000')
                    ->press('#btn-submit')->pause('2000')
                    ->assertSee('Simpan & Tutup')->pause('2000')
                    ->press('ul.dropdown-menu > li:nth-child(3)')->pause('2000')
                    ->assertPathIs('/akuntansi/penawaran-penjualan')->pause('2000')
                    ->screenshot('Berhasil Melakukan Penambahan Penawaran Penjualan')
                    ->press('.swal-button')->pause('9000');
        });
    }
    public function KondisiPenawaran(){
        $penawaran  =   DB::table('penawaran_penjualan')->orderBy('id', 'desc')->first();
        $fob    =   0;
        if(is_null($penawaran)){
            return [
                'penawaran_terakhir'    =>      1,
                'fob'                   =>      $fob    =   $fob + 1,
            ];
        }else{
            return [
                'penawaran_terakhir'    =>      $penawaran->id,
                'fob'                   =>      $penawaran->no_penawaran + 1,
            ];
        }
    }
}
