<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableAkunR1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('akun', 'money_function')) {
          Schema::table('akun', function(Blueprint $table) {
              $table->dropColumn('money_function');
          });
        }
        if (!Schema::hasColumn('akun', 'money_function')) {
          Schema::table('akun', function (Blueprint $table) {
              $table->float('money_function', 19, 2)->nullable()->after('nama_akun')->unsigned();
          });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('akun', function (Blueprint $table) {
        //     //
        // });
    }
}
