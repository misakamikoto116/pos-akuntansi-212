<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSyaratPembayaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!schema::hasTable('syarat_pembayaran')){
            Schema::create('syarat_pembayaran', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('jika_membayar_antara')->default(0);
                $table->float('akan_dapat_diskon', 19,2)->default(0.00);
                $table->integer('jatuh_tempo')->default(0);
                $table->string('keterangan');
                $table->tinyInteger('status_cod')->default(0)->comment('0 => false, 1 => true');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('syarat_pembayaran');
    }
}
