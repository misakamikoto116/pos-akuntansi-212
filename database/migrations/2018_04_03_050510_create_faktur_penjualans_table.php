<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFakturPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if (!Schema::hasTable('faktur_penjualan')) {
            Schema::create('faktur_penjualan', function (Blueprint $table) {
                $table->increments('id');
                $table->string('no_faktur');
                $table->integer('pelanggan_id')->unsigned();
                $table->foreign('pelanggan_id')
                                    ->references('id')
                                    ->on('informasi_pelanggan');
                $table->text('alamat_pengiriman',255);
                $table->tinyInteger('taxable')->nullable();
                $table->tinyInteger('in_tax')->nullable();
                $table->integer('diskon')->nullable();
                
                $table->string('po_no');
                $table->string('invoice_no');
                $table->datetime('invoice_date');
                $table->tinyInteger('fob');
    
                /** 
                 * 0 = Shopping Point
                 * 1 = Destination
                 */
    
                $table->integer('term_id')->unsigned();
                $table->integer('ship_id')->unsigned();
                $table->string('no_fp_std');
                $table->string('no_fp_std2');
                $table->datetime('no_fp_std_date');
    
                $table->foreign('ship_id')
                ->references('id')
                ->on('jasa_pengiriman')
                ->onUpdate('cascade');
    
                $table->foreign('term_id')
                ->references('id')
                ->on('syarat_pembayaran')
                ->onUpdate('cascade');

                $table->float('ongkir',19,2)->nullable();
                $table->float('total',19,2)->nullable();
                $table->text('keterangan',255)->nullable();
                $table->text('catatan',255)->nullable();
                $table->integer('akun_piutang_id')->unsigned();
                $table->foreign('akun_piutang_id')
                                ->references('id')
                                ->on('akun');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faktur_penjualan');
    }
}
