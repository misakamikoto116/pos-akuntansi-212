<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailBukuKeluarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('detail_buku_keluar')) {
            Schema::create('detail_buku_keluar', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('akun_id')->unsigned();
                $table->foreign('akun_id')
                                ->references('id')
                                ->on('akun');
                $table->bigInteger('buku_keluar_id')->unsigned();
                $table->foreign('buku_keluar_id')
                                ->references('id')
                                ->on('buku_keluar');
                $table->string('catatan');
                $table->float('nominal', 19,2);
                $table->softDeletes();
                $table->timestamps();
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_buku_keluar');
    }
}
