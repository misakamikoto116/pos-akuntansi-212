<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangFakturPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('barang_faktur_penjualan')) {
            Schema::create('barang_faktur_penjualan', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('produk_id')->unsigned();
                $table->foreign('produk_id')
                                ->references('id')
                                ->on('produk');
                $table->integer('faktur_penjualan_id')->unsigned();
                $table->foreign('faktur_penjualan_id')
                                ->references('id')
                                ->on('faktur_penjualan');
                $table->integer('barang_pengiriman_penjualan_id')->unsigned()->nullable();
                $table->foreign('barang_pengiriman_penjualan_id')
                                ->references('id')
                                ->on('barang_pengiriman_penjualan');
                $table->string('item_deskripsi');
                $table->string('item_unit')->nullable();
                $table->integer('jumlah');
                $table->float('unit_price',19,2);
                $table->float('diskon');
                $table->integer('kode_pajak_id')->unsigned()->nullable();
                $table->foreign('kode_pajak_id')
                    ->references('id')
                    ->on('kode_pajak');
                $table->integer('gudang_id')->unsigned()->nullable();
                $table->foreign('gudang_id')
                                ->references('id')
                                ->on('gudang');
                $table->float('sn')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_faktur_penjualan');
    }
}
