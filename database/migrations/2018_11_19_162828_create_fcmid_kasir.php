<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFcmidKasir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('kasir_mesin', 'fcm_id')) {
            Schema::table('kasir_mesin', function (Blueprint $table) {
                $table->string('fcm_id', 255)
                    ->comment('fmc device android')
                    ->nullable()->after('print_tunnel_address');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kasir_mesin', function (Blueprint $table) {
            $table->dropColumn('fcm_id');
        });
    }
}
