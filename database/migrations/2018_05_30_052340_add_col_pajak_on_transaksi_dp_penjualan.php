<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColPajakOnTransaksiDpPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('transaksi_uang_muka', 'pajak_satu_id')) {
            Schema::table('transaksi_uang_muka', function (Blueprint $table) {
                $table->integer('pajak_satu_id')->unsigned()->after('faktur_uang_muka_pelanggan_id')->nullable();
                $table->integer('pajak_dua_id')->unsigned()->after('pajak_satu_id')->nullable();
                
                $table->foreign('pajak_satu_id')->references('id')
                    ->on('kode_pajak');

                $table->foreign('pajak_dua_id')->references('id')
                    ->on('kode_pajak');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
