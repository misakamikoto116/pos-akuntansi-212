<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipeAktivaTetapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!schema::hasTable('tipe_aktiva_tetap')){
            Schema::create('tipe_aktiva_tetap', function (Blueprint $table) {
                $table->increments('id');
                $table->string('tipe_aktiva_tetap');
                $table->integer('tipe_aktiva_tetap_pajak_id')->unsigned();
                $table->foreign('tipe_aktiva_tetap_pajak_id')
                                    ->references('id')
                                    ->on('tipe_aktiva_tetap_pajak');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipe_aktiva_tetap');
    }
}
