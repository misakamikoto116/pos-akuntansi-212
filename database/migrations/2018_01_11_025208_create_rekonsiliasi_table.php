<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekonsiliasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('rekonsiliasi')) {
            Schema::create('rekonsiliasi', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('akun_id')->unsigned();
                $table->foreign('akun_id')
                                ->references('id')
                                ->on('akun');
                $table->float('nominal', 19,2)->comment('Jumlah Nominal Rekonsiliasi');
                $table->string('keterangan');
                $table->softDeletes();
                $table->timestamps();
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekonsiliasi');
    }
}
