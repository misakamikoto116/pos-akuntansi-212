<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFakturPembelianId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('beban_faktur_pembelian', 'faktur_pembelian_id')) {
            Schema::table('beban_faktur_pembelian', function (Blueprint $table) {
                $table->integer('faktur_pembelian_id')->unsigned()->after('akun_beban_id')->nullable();
                $table->foreign('faktur_pembelian_id')->references('id')
                    ->on('faktur_pembelian');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('beban_faktur_pembelian', function (Blueprint $table) {
        });
    }
}
