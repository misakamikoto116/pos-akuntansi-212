<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableDetailJurnalUmum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('detail_jurnal_umum', 'jurnal_umum_id')) {
            Schema::table('detail_jurnal_umum', function (Blueprint $table) {
                $table->integer('jurnal_umum_id')->after('akun_id')->unsigned();

                $table->foreign('jurnal_umum_id')
                        ->references('id')
                        ->on('jurnal_umum');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_jurnal_umum', function (Blueprint $table) {
            //
        });
    }
}
