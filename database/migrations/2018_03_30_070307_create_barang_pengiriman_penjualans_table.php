
<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangPengirimanPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('barang_pengiriman_penjualan')) {
            Schema::create('barang_pengiriman_penjualan', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('produk_id')->unsigned();
                $table->foreign('produk_id')
                                ->references('id')
                                ->on('produk');
                $table->integer('pengiriman_penjualan_id')->unsigned();
                $table->foreign('pengiriman_penjualan_id')
                                ->references('id')
                                ->on('pengiriman_penjualan');
                $table->integer('barang_pesanan_penjualan_id')->unsigned();
                $table->foreign('barang_pesanan_penjualan_id')
                    ->references('id')
                    ->on('barang_pesanan_penjualan');
                $table->string('item_deskripsi');
                $table->string('item_unit')->nullable();
                $table->integer('jumlah');
                $table->string('no_so')->nullable();
                $table->integer('gudang_id')->unsigned()->nullable();
                $table->foreign('gudang_id')
                                ->references('id')
                                ->on('gudang');
                $table->float('sn')->nullable();
                $table->integer('qty_used');
                $table->tinyInteger('status')->comment('0 => tetap , 1 => berubah , 2 => new');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_pengiriman_penjualan');
    }
}
