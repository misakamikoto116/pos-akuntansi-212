<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableDetailBukuMasuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('detail_buku_masuk', 'nominal')) {
            Schema::table('detail_buku_masuk', function(Blueprint $table) {
                $table->dropColumn('nominal');
            });
        }
        if (!Schema::hasColumn('detail_buku_masuk', 'nominal')) {
            Schema::table('detail_buku_masuk', function(Blueprint $table) {
                $table->float('nominal', 19,2)->after('catatan')->unsigned();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_buku_masuk', function (Blueprint $table) {
            //
        });
    }
}
