<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangPermintaanPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('barang_permintaan_pembelian')) {
            Schema::create('barang_permintaan_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('produk_id')->unsigned();
                $table->foreign('produk_id')
                                ->references('id')
                                ->on('produk');
                $table->integer('permintaan_pembelian_id')->unsigned();
                $table->foreign('permintaan_pembelian_id')
                                ->references('id')
                                ->on('permintaan_pembelian');
                $table->string('item_deskripsi');
                $table->string('item_unit')->nullable();
                $table->integer('jumlah');
                $table->date('required_date');
                $table->text('notes')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_permintaan_pembelian');
    }
}
