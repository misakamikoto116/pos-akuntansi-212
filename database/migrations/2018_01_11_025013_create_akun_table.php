<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAkunTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('akun')) {
            Schema::create('akun', function (Blueprint $table) {
                $table->increments('id');
                $table->string('kode_akun');
                $table->string('nama_akun');
                $table->tinyInteger('money_function')->nullable();
                $table->integer('parent_id')->unsigned()->nullable();
                $table->foreign('parent_id')
                                ->references('id')
                                ->on('akun');
                $table->integer('tipe_akun_id')->unsigned();
                $table->foreign('tipe_akun_id')
                                ->references('id')
                                ->on('tipe_akun');
                $table->softDeletes();
                $table->timestamps();
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('akun');
    }
}
