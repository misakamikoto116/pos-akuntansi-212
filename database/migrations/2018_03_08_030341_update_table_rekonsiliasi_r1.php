<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableRekonsiliasiR1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('rekonsiliasi', 'nominal')) {
            Schema::table('rekonsiliasi', function(Blueprint $table) {
            $table->dropColumn('nominal');
        });
        }
        if (Schema::hasColumn('rekonsiliasi', 'keterangan')) {
            Schema::table('rekonsiliasi', function(Blueprint $table) {
            $table->dropColumn('keterangan');
        });
        }
        if (!Schema::hasColumn('rekonsiliasi', 'saldo_rekening_koran')) {
            Schema::table('rekonsiliasi', function (Blueprint $table) {
            $table->float('saldo_rekening_koran', 19, 2)->after('akun_id');
            });
        }
        if (!Schema::hasColumn('rekonsiliasi', 'kalkulasi_saldo')) {
            Schema::table('rekonsiliasi', function (Blueprint $table) {
            $table->float('kalkulasi_saldo', 19, 2)->after('saldo_rekening_koran');
            });
        }
        if (!Schema::hasColumn('rekonsiliasi', 'selisih_saldo')) {
            Schema::table('rekonsiliasi', function (Blueprint $table) {
            $table->float('selisih_saldo', 19, 2)->after('kalkulasi_saldo');
            });
        }
        if (!Schema::hasColumn('rekonsiliasi', 'tanggal_rekonsil')) {
            Schema::table('rekonsiliasi', function (Blueprint $table) {
            $table->dateTime('tanggal_rekonsil')->after('selisih_saldo');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
