<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('transaksi')) {
            Schema::create('transaksi', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->float('nominal', 19, 2)->nullable();
                // $table->float('nominal', 19, 2)->nullable()->unsigned();
                $table->dateTime('tanggal');
                $table->integer('akun_id')->unsigned();
                $table->foreign('akun_id')
                                ->references('id')
                                ->on('akun');
                $table->integer('item_id')->nullable()->unsigned();
                $table->string('item_type')->nullable();
                $table->tinyInteger('status')->comment("0 => Kredit, 1 => Debet")->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
