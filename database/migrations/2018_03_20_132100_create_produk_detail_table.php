<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdukDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('produk_detail')) {
            Schema::create('produk_detail', function (Blueprint $table) {
                $table->increments('id');
                $table->string('no_faktur')->nullable();
                $table->dateTime('tanggal')->nullable();
                $table->integer('kuantitas');
                $table->float('biaya',19,2);
                $table->float('harga_modal',19,2);

                $table->integer('gudang_id')->unsigned();
                $table->foreign('gudang_id')
                                ->references('id')
                                ->on('gudang');

                $table->integer('produk_id')->unsigned();
                $table->foreign('produk_id')
                                ->references('id')
                                ->on('produk');

                $table->string('sn')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk_detail');
    }
}
