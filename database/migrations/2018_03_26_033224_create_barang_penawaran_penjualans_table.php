<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangPenawaranPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('barang_penawaran_penjualan')) {
            Schema::create('barang_penawaran_penjualan', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('produk_id')->unsigned();
                $table->foreign('produk_id')
                                ->references('id')
                                ->on('produk');
                $table->integer('penawaran_penjualan_id')->unsigned();
                $table->foreign('penawaran_penjualan_id')
                                ->references('id')
                                ->on('penawaran_penjualan');
                $table->string('item_deskripsi');
                $table->integer('jumlah');
                $table->float('harga',19,2);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_penawaran_penjualan');
    }
}
