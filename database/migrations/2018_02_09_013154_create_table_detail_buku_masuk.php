<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetailBukuMasuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('detail_buku_masuk')) {
            Schema::create('detail_buku_masuk', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('akun_id')->unsigned();
                $table->foreign('akun_id')
                                ->references('id')
                                ->on('akun');
                $table->bigInteger('buku_masuk_id')->unsigned();
                $table->foreign('buku_masuk_id')
                                ->references('id')
                                ->on('buku_masuk');
                $table->string('catatan');
                $table->float('nominal', 19,2)->unsigned();
                $table->softDeletes();
                $table->timestamps();
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
