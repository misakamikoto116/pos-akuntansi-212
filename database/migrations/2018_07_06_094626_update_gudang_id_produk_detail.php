<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class UpdateGudangIdProdukDetail extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up()
	{
		if (Schema::hasColumn('produk_detail', 'gudang_id')) {
			Schema::table('produk_detail', function ($table) {
				$table->unsignedInteger('gudang_id')->nullable()->change();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 */
	public function down()
	{
	}
}
