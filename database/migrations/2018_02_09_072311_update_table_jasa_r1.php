<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableJasaR1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('jasa', 'harga_jual')) {
          Schema::table('jasa', function(Blueprint $table) {
              $table->dropColumn('harga_jual');
          });
        }
        if (!Schema::hasColumn('jasa', 'harga_jual')) {
          Schema::table('jasa', function (Blueprint $table) {
              $table->float('harga_jual', 19, 2)->after('tanggal_jasa')->unsigned();
          });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jasa', function (Blueprint $table) {
            //
        });
    }
}
