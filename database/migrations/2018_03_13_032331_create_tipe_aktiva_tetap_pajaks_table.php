<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipeAktivaTetapPajaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!schema::hasTable('tipe_aktiva_tetap_pajak')){
            Schema::create('tipe_aktiva_tetap_pajak', function (Blueprint $table) {
                $table->increments('id');
                $table->string('tipe_aktiva_tetap_pajak');
                $table->integer('metode_penyusutan_id')->unsigned();
                $table->foreign('metode_penyusutan_id')
                                ->references('id')
                                ->on('metode_penyusutan');
                $table->integer('estimasi_umur');
                $table->float('tarif_penyusutan', 19,2);
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipe_aktiva_tetap_pajak');
    }
}
