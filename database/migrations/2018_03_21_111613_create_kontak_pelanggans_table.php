<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKontakPelanggansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!schema::hasTable('kontak_pelanggan')){
            Schema::create('kontak_pelanggan', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('pelanggan_id')->unsigned()->nullable();
                $table->foreign('pelanggan_id')
                                ->references('id')
                                ->on('informasi_pelanggan');
                $table->integer('pemasok_id')->unsigned()->nullable();
                $table->foreign('pemasok_id')
                                ->references('id')
                                ->on('informasi_pemasok');
                $table->string('nama');
                $table->string('nama_depan')->nullable();
                $table->string('jabatan')->nullable();
                $table->string('telepon_kontak')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kontak_pelanggan');
    }
}
