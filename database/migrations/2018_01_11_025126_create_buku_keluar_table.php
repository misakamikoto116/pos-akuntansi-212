<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBukuKeluarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('buku_keluar')) {
            Schema::create('buku_keluar', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('akun_id')->unsigned();
                $table->foreign('akun_id')
                                ->references('id')
                                ->on('akun');
                $table->string('no_faktur');
                $table->string('no_cek');
                $table->dateTime('tanggal');
                $table->string('keterangan');
                $table->string('peruntukan');
                $table->float('nominal', 19,2);
                $table->softDeletes();
                $table->timestamps();
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku_keluar');
    }
}
