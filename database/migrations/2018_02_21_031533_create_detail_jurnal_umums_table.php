<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailJurnalUmumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('detail_jurnal_umum')) {
            Schema::create('detail_jurnal_umum', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('akun_id')->unsigned();
                $table->foreign('akun_id')
                                ->references('id')
                                ->on('akun');
                $table->float('debit', 19, 2)->nullable();
                $table->float('kredit', 19, 2)->nullable();
                $table->text('memo')->nullable();
                $table->text('subsidiary_ledger')->nullable();
                $table->text('departement')->nullable();
                $table->text('project')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
         } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_jurnal_umum');
    }
}
