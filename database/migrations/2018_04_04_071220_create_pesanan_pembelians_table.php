<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesananPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if (!Schema::hasTable('pesanan_pembelian')) {
            Schema::create('pesanan_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('pemasok_id')->unsigned();
                $table->foreign('pemasok_id')
                                    ->references('id')
                                    ->on('informasi_pemasok');
                $table->text('alamat_pengiriman',255);
                $table->string('po_number')->nullable();
                $table->date('po_date')->nullable();
                $table->date('expected_date')->nullable();
                $table->date('ship_date')->nullable();
                $table->tinyInteger('fob')->nullable();
                $table->integer('term_id')->unsigned();
                $table->foreign('term_id')
                                ->references('id')
                                ->on('syarat_pembayaran');
                $table->integer('ship_id')->unsigned();
                $table->foreign('ship_id')
                                ->references('id')
                                ->on('jasa_pengiriman');
                $table->integer('diskon')->nullable();
                $table->float('ongkir',19,2)->nullable();
                $table->float('total',19,2)->nullable();
                $table->text('keterangan',255)->nullable();
                $table->integer('akun_dp_id')->unsigned();
                $table->foreign('akun_dp_id')
                                ->references('id')
                                ->on('akun');
                $table->integer('status')->default(0)->comment('0 => sdg di proses , 1 => di tutup, 2 => diterima penuh');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanan_pembelian');
    }
}
