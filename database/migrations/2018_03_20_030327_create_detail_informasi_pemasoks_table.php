<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailInformasiPemasoksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('detail_informasi_pemasok')) {
            Schema::create('detail_informasi_pemasok', function (Blueprint $table) {
                $table->increments('id');
                $table->string('no_faktur');
                $table->dateTime('tanggal');
                $table->float('saldo_awal',19,2);
                $table->integer('informasi_pemasok_id')->unsigned();
                $table->integer('syarat_pembayaran_id')->unsigned()->nullable();
                $table->foreign('syarat_pembayaran_id')
                                ->references('id')
                                ->on('syarat_pembayaran');
                $table->foreign('informasi_pemasok_id')
                                ->references('id')
                                ->on('informasi_pemasok');
                $table->softDeletes();
                $table->timestamps();
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_informasi_pemasok');
    }
}
