<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengirimanPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pengiriman_penjualan')) {
            Schema::create('pengiriman_penjualan', function (Blueprint $table) {
                $table->increments('id');
                $table->string('no_pesanan')->nullable();
                $table->integer('pelanggan_id')->unsigned();
                $table->foreign('pelanggan_id')
                                    ->references('id')
                                    ->on('informasi_pelanggan');
                $table->text('alamat_pengiriman',255);
                $table->string('delivery_no')->nullable();
                $table->date('delivery_date')->nullable();
                $table->string('po_no')->nullable();
                $table->date('ship_date')->nullable();
                $table->integer('ship_id')->unsigned();
                $table->foreign('ship_id')
                                ->references('id')
                                ->on('jasa_pengiriman');
                $table->text('keterangan',255)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengiriman_penjualan');
    }
}
