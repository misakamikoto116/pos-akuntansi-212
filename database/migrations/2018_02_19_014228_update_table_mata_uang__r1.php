<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableMataUangR1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('mata_uang', 'nilai_tukar')) {
            Schema::table('mata_uang', function (Blueprint $table) {
            $table->float('nilai_tukar', 19,2)->after('nama')->unsigned()->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mata_uang', function (Blueprint $table) {
            //
        });
    }
}
