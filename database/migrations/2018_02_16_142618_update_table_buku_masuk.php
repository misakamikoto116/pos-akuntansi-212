<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableBukuMasuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('buku_masuk', 'nominal')) {
          Schema::table('buku_masuk', function(Blueprint $table) {
              $table->dropColumn('nominal');
          });
        }
        if (!Schema::hasColumn('buku_masuk', 'nominal')) {
          Schema::table('buku_masuk', function (Blueprint $table) {
              $table->float('nominal', 19, 2)->after('keterangan')->unsigned();
          });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buku_masuk', function (Blueprint $table) {
            //
        });
    }
}
