<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewApi extends Migration
{
    private $view_name = 'view_api_produk';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("
           CREATE OR REPLACE VIEW $this->view_name AS 
                select 
                p.id as id, p.keterangan as nama, k.nama as kategori, p.no_barang as no_barang, p.foto_produk,
                (select harga_terakhir from produk_detail where produk_id = p.id order by id DESC limit 1 ) as harga_terakhir,
                (select harga_modal from produk_detail where produk_id = p.id order by id DESC limit 1 ) as harga_modal,
                (select kuantitas from produk_detail where produk_id = p.id order by id DESC limit 1 ) as tersedia,
                p.unit as unit 
                from produk p 
                left join kategori_produk k on p.kategori_produk_id = k.id
                
                where p.tipe_barang not in(3,5,2, 4) and p.gudang_id = 6
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("DROP VIEW $this->view_name;");
    }
}
