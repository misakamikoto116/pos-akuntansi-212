<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeColumnOnBarangPenawaranPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barang_penawaran_penjualan', function (Blueprint $table) {
            $table->string('satuan')->after('jumlah');
            $table->double('diskon')->after('satuan'); // presentasi %
            $table->double('pajak')->after('diskon'); // presentasi %
            $table->integer('terproses')->after('harga')->default(0);
            $table->integer('ditutup')->after('terproses')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_penawaran_penjualan', function (Blueprint $table) {
            $table->dropColumn('satuan');
            $table->dropColumn('diskon'); // %
            $table->dropColumn('pajak'); // %
            $table->dropColumn('terproses');
            $table->dropColumn('ditutup');
        });
    }
}
