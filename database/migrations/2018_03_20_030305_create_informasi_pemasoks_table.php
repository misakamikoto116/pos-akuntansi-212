<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformasiPemasoksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('informasi_pemasok')) {
            Schema::create('informasi_pemasok', function (Blueprint $table) {
                $table->increments('id');
                $table->string('no_pemasok')->nullable();
                $table->string('nama');
                $table->string('alamat')->nullable();
                $table->string('alamat_pajak')->nullable();
                $table->string('kota')->nullable();
                $table->string('prop')->nullable();
                $table->string('kode_pos')->nullable();
                $table->string('negara')->nullable();
                $table->string('telepon')->nullable();
                $table->string('personal_kontak')->nullable();
                $table->string('email')->nullable();
                $table->string('halaman_web')->nullable();
                $table->integer('syarat_pembayaran_id')->unsigned()->nullable();
                $table->foreign('syarat_pembayaran_id')
                                ->references('id')
                                ->on('syarat_pembayaran');
                $table->integer('mata_uang_id')->unsigned()->nullable();
                $table->foreign('mata_uang_id')
                                ->references('id')
                                ->on('mata_uang');
                $table->string('keterangan')->nullable();
                $table->integer('pajak_satu_id')->unsigned()->nullable();
                $table->foreign('pajak_satu_id')
                                ->references('id')
                                ->on('kode_pajak');
                $table->integer('pajak_dua_id')->unsigned()->nullable();
                $table->foreign('pajak_dua_id')
                                ->references('id')
                                ->on('kode_pajak');
                $table->tinyInteger('pajak_faktur')->default(0)->comment('0 => false, 1 => true');
                $table->string('npwp')->nullable();
                $table->string('no_pkp')->nullable();
                $table->integer('tipe_pajak_id')->unsigned()->nullable();
                $table->foreign('tipe_pajak_id')
                                ->references('id')
                                ->on('tipe_pajak');
                $table->string('catatan')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informasi_pemasok');
    }
}
