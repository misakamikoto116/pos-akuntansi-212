<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatOnPenawaranpenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penawaran_penjualan', function (Blueprint $table) {
            $table->integer('status')->after('total')->default(0);
            /** 
             * @NB : Status field means
             * 0 = Mengantri
             * 1 = Menunggu
             * 2 = Terproses
             * 3 = Ditutup
             */
            $table->dateTime('tanggal')->after('catatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penawaran_penjualan', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('tanggal');
        });
    }
}
