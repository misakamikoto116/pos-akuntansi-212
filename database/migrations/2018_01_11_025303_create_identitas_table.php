<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdentitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('identitas')) {
            Schema::create('identitas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nama_perusahaan');
                $table->string('alamat');
                $table->string('kode_pos')->nullable();
                $table->string('no_telepon')->nullable();
                $table->string('negara')->nullable();
                $table->dateTime('tanggal_mulai');
                $table->integer('mata_uang_id')->unsigned();
                $table->foreign('mata_uang_id')
                                ->references('id')
                                ->on('mata_uang');
                $table->softDeletes();
                $table->timestamps();
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identitas');
    }
}
