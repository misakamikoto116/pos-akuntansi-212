<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKodePajaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!schema::hasTable('kode_pajak')){
            Schema::create('kode_pajak', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nama');
                $table->float('nilai', 19,2);
                $table->string('kode');
                $table->string('keterangan')->nullable();
                $table->integer('akun_pajak_penjualan_id')->unsigned();
                $table->foreign('akun_pajak_penjualan_id')
                                ->references('id')
                                ->on('akun');
                $table->integer('akun_pajak_pembelian_id')->unsigned();
                $table->foreign('akun_pajak_pembelian_id')
                                ->references('id')
                                ->on('akun');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kode_pajak');
    }
}
