<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewHargaModal extends Migration
{
    private $view_name = 'view_api_harga_modal';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("
         CREATE OR REPLACE VIEW $this->view_name AS 
            SELECT p.id as id, p.keterangan as nama, p.no_barang as no_barang,
            (SELECT harga_terakhir FROM produk_detail WHERE produk_id = p.id order by id DESC LIMIT 1 ) as harga_terakhir,
            (SELECT harga_modal FROM produk_detail WHERE produk_id = p.id ORDER BY id DESC LIMIT 1 ) as harga_modal,
            (SELECT kuantitas FROM produk_detail WHERE produk_id = p.id ORDER BY id DESC LIMIT 1 ) as tersedia
            FROM produk p
            WHERE p.tipe_barang NOT IN(3,5,4)
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("DROP VIEW $this->view_name;");
    }
}
