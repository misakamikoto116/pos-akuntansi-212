<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangPesananPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('barang_pesanan_penjualan')) {
            Schema::create('barang_pesanan_penjualan', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('produk_id')->unsigned();
                $table->foreign('produk_id')
                                ->references('id')
                                ->on('produk');
                $table->integer('pesanan_penjualan_id')->unsigned();
                $table->foreign('pesanan_penjualan_id')
                                ->references('id')
                                ->on('pesanan_penjualan');
                $table->integer('barang_penawaran_penjualan_id')->unsigned()->nullable();
                $table->foreign('barang_penawaran_penjualan_id')
                                ->references('id')
                                ->on('barang_penawaran_penjualan');
                $table->string('item_deskripsi');
                $table->integer('jumlah');
                $table->float('harga',19,2);
                $table->tinyInteger('status')->comment('0 => tetap , 1 => berubah , 2 => new');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_pesanan_penjualan');
    }
}
