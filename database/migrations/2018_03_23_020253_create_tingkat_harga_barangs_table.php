<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTingkatHargaBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!schema::hasTable('tingkat_harga_barang')) {
            Schema::create('tingkat_harga_barang', function (Blueprint $table) {
                $table->increments('id');
                $table->float('tingkat_1', 19,2)->nullable();
                $table->float('tingkat_2', 19,2)->nullable();
                $table->float('tingkat_3', 19,2)->nullable();
                $table->float('tingkat_4', 19,2)->nullable();
                $table->float('tingkat_5', 19,2)->nullable();
                $table->integer('produk_id')->unsigned()->nullable();
                $table->foreign('produk_id')
                                ->references('id')
                                ->on('produk');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tingkat_harga_barang');
    }
}
