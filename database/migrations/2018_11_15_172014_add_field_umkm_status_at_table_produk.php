<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldUmkmStatusAtTableProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('produk', 'status_umkm')) {
            Schema::table('produk', function (Blueprint $table) {
                $table->tinyInteger('status_umkm')
                        ->comment('0 => False, 1 => True')
                        ->nullable()->after('catatan');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produk_detail', function (Blueprint $table) {
            //
        });
    }
}
