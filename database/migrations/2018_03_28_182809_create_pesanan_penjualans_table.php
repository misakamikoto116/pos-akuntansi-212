<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesananPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pesanan_penjualan')) {
            Schema::create('pesanan_penjualan', function (Blueprint $table) {
                $table->increments('id');
                $table->string('no_penawaran');
                $table->integer('pelanggan_id')->unsigned();
                $table->foreign('pelanggan_id')
                                    ->references('id')
                                    ->on('informasi_pelanggan');
                $table->text('alamat_pengiriman',255)->nullable();
                $table->tinyInteger('taxable')->nullable();
                $table->tinyInteger('in_tax')->nullable();
                $table->string('po_number')->nullable();
                $table->string('so_number')->nullable();
                $table->date('so_date')->nullable();
                $table->date('ship_date')->nullable();
                $table->tinyInteger('fob')->nullable();
                $table->integer('term_id')->unsigned();
                $table->foreign('term_id')
                                ->references('id')
                                ->on('syarat_pembayaran');
                $table->integer('ship_id')->unsigned();
                $table->foreign('ship_id')
                                ->references('id')
                                ->on('jasa_pengiriman');
                $table->integer('diskon')->nullable();
                $table->float('ongkir',19,2)->nullable();
                $table->float('total',19,2)->nullable();
                $table->text('keterangan',255)->nullable();
                $table->text('catatan',255)->nullable();
                $table->integer('akun_dp_id')->unsigned()->nullable();
                $table->foreign('akun_dp_id')
                                ->references('id')
                                ->on('akun');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanan_penjualan');
    }
}
