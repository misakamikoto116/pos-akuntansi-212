<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermintaanPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('permintaan_pembelian')) {
            Schema::create('permintaan_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->string('request_no');
                $table->date('request_date');
                $table->tinyInteger('status')->default(0);
                $table->text('catatan')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_pembelian');
    }
}
