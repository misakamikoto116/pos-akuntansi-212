<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('produk')) {
            Schema::create('produk', function (Blueprint $table) {
                $table->increments('id');
                $table->tinyInteger('tipe_barang')->comment('0 => Persedian. 1 => Non Persedian, 2 => Servis, 3 => Uang Muka, 4 => Grup');
                $table->bigInteger('no_barang');
                $table->integer('parent_id')->unsigned()->nullable();
                $table->foreign('parent_id')
                                ->references('id')
                                ->on('produk');
                $table->tinyInteger('status')->default(1)->comment('1 => Aktiv. 0 => Non Aktiv');
                $table->string('keterangan');
                $table->tinyInteger('tipe_persedian')->nullable();
                $table->float('harga_standar_def',19,2)->nullable();
                $table->integer('kategori_produk_id')->unsigned()->nullable();
                $table->foreign('kategori_produk_id')
                                ->references('id')
                                ->on('kategori_produk');
                $table->integer('gudang_def_id')->unsigned()->nullable();
                $table->foreign('gudang_def_id')
                                ->references('id')
                                ->on('gudang');
                $table->integer('kuantitas')->nullable();
                $table->string('unit')->nullable();
                $table->float('harga_unit',19,2)->nullable();
                $table->integer('gudang_id')->unsigned()->nullable();
                $table->foreign('gudang_id')
                                ->references('id')
                                ->on('gudang');
                $table->float('harga_jual',19,2)->nullable();
                $table->float('diskon',19,2)->nullable();
                $table->integer('kode_pajak_penj_id')->unsigned()->nullable();
                $table->foreign('kode_pajak_penj_id')
                                ->references('id')
                                ->on('kode_pajak');
                $table->integer('pemasok_id')->unsigned()->nullable();
                $table->foreign('pemasok_id')
                                ->references('id')
                                ->on('informasi_pemasok');
                $table->integer('min_jumlah_reorder')->nullable();
                $table->integer('kode_pajak_perse_id')->unsigned()->nullable();
                $table->foreign('kode_pajak_perse_id')
                                ->references('id')
                                ->on('kode_pajak');
                $table->integer('kode_pajak_pemb_id')->unsigned()->nullable();
                $table->foreign('kode_pajak_pemb_id')
                                ->references('id')
                                ->on('kode_pajak');
                $table->integer('akun_beban_id')->unsigned()->nullable();
                $table->foreign('akun_beban_id')
                                ->references('id')
                                ->on('akun');
                $table->integer('akun_persedian_id')->unsigned()->nullable();
                $table->foreign('akun_persedian_id')
                                ->references('id')
                                ->on('akun');
                $table->integer('akun_penjualan_id')->unsigned()->nullable();
                $table->foreign('akun_penjualan_id')
                                ->references('id')
                                ->on('akun');
                $table->integer('akun_ret_penjualan_id')->unsigned()->nullable();
                $table->foreign('akun_ret_penjualan_id')
                                ->references('id')
                                ->on('akun');
                $table->integer('akun_disk_penjualan_id')->unsigned()->nullable();
                $table->foreign('akun_disk_penjualan_id')
                                ->references('id')
                                ->on('akun');
                $table->integer('akun_barang_terkirim_id')->unsigned()->nullable();
                $table->foreign('akun_barang_terkirim_id')
                                ->references('id')
                                ->on('akun');
                $table->integer('akun_hpp_id')->unsigned()->nullable();
                $table->foreign('akun_hpp_id')
                                ->references('id')
                                ->on('akun');
                $table->integer('akun_ret_pembelian_id')->unsigned()->nullable();
                $table->foreign('akun_ret_pembelian_id')
                                ->references('id')
                                ->on('akun');
                $table->integer('akun_belum_tertagih_id')->unsigned()->nullable();
                $table->foreign('akun_belum_tertagih_id')
                                ->references('id')
                                ->on('akun');
                $table->string('catatan')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk');
    }
}
