<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangPesananPembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('barang_pesanan_pembelian')) {
            Schema::create('barang_pesanan_pembelian', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('produk_id')->unsigned();
                $table->foreign('produk_id')
                                ->references('id')
                                ->on('produk');
                $table->integer('pesanan_pembelian_id')->unsigned();
                $table->foreign('pesanan_pembelian_id')
                                ->references('id')
                                ->on('pesanan_pembelian');
                $table->integer('barang_permintaan_pembelian_id')->unsigned();
                $table->foreign('barang_permintaan_pembelian_id')
                                ->references('id')
                                ->on('barang_permintaan_pembelian');
                $table->string('item_deskripsi');
                $table->string('item_unit');
                $table->integer('jumlah');
                $table->float('harga',19,2);
                $table->float('diskon', 19,2)->nullable();
                $table->tinyInteger('ditutup')->default(0)->comment('0 => false, 1 => true');
                $table->integer('kode_pajak_id')->unsigned();
                $table->foreign('kode_pajak_id')
                                ->references('id')
                                ->on('kode_pajak');
                $table->tinyInteger('status')->default(0)->comment('0 => tetap, 1 => berubah');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_pesanan_pembelian');
    }
}
