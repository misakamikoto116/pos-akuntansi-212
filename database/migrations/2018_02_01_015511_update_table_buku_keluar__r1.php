<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableBukuKeluarR1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('buku_keluar', 'nominal')) {
            Schema::table('buku_keluar', function(Blueprint $table) {
                $table->dropColumn('nominal');
            });
        }
        if (!Schema::hasColumn('buku_keluar', 'nominal')) {
            Schema::table('buku_keluar', function (Blueprint $table) {
                $table->float('nominal', 19, 2)->after('peruntukan')->unsigned();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buku_keluar', function (Blueprint $table) {
            //
        });
    }
}
