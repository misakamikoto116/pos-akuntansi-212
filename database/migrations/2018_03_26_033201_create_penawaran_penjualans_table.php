<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenawaranPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('penawaran_penjualan')) {
            Schema::create('penawaran_penjualan', function (Blueprint $table) {
                $table->increments('id');
                $table->string('no_penawaran');
                $table->integer('pelanggan_id')->unsigned();
                $table->foreign('pelanggan_id')
                                    ->references('id')
                                    ->on('informasi_pelanggan');
                $table->text('alamat_pengiriman',255);
                $table->tinyInteger('taxable')->nullable();
                $table->tinyInteger('in_tax')->nullable();
                $table->integer('diskon')->nullable();
                $table->float('ongkir',19,2)->nullable();
                $table->float('total',19,2)->nullable();
                $table->text('keterangan',255)->nullable();
                $table->text('catatan',255)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penawaran_penjualan');
    }
}
