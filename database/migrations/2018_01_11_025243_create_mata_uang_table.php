<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMataUangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('mata_uang')) {
            Schema::create('mata_uang', function (Blueprint $table) {
                $table->increments('id');
                $table->string('negara');
                $table->string('kode');
                $table->string('kode_simbol');
                $table->string('nama');
                $table->softDeletes();
                $table->timestamps();
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mata_uang');
    }
}
