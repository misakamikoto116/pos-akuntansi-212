<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNomorPajaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!schema::hasTable('nomor_pajak')){
            Schema::create('nomor_pajak', function (Blueprint $table) {
                $table->increments('id');
                $table->string('dari')->nullable();
                $table->string('sd')->nullable();
                $table->string('tetap')->nullable();
                $table->tinyInteger('status')->default(0)->comment('0 => false, 1 => true');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nomor_pajak');
    }
}
