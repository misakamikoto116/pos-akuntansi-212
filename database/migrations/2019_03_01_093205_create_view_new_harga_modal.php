<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewNewHargaModal extends Migration
{
    private $view_name = 'view_api_harga_modal';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // \DB::statement("
        //  CREATE OR REPLACE VIEW $this->view_name AS 
        //     SELECT detail.id as id, detail.harga_modal as harga_modal, detail.item_deskrips as nama, detail.gudang_id as gudang_id, 
        //     (SELECT harga_terakhir FROM produk_detail WHERE produk_id = p.id order by id DESC LIMIT 1 ) as harga_terakhir,
        //     (SELECT harga_modal FROM produk_detail WHERE produk_id = p.id ORDER BY id DESC LIMIT 1 ) as harga_modal,
        //     (SELECT kuantitas FROM produk_detail WHERE produk_id = p.id ORDER BY id DESC LIMIT 1 ) as tersedia
        //     FROM barang_faktur_pembelian detail
        //     WHERE p.tipe_barang NOT IN(3,5,4)
        // ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("DROP VIEW $this->view_name;");
    }
}
