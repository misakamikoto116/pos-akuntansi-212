<?php

use Illuminate\Database\Seeder;
use App\Modules\Pos\Models\Promosi;
use App\Modules\Pos\Models\Produk;
use App\Modules\Pos\Models\SebabPromosi;
use App\Modules\Pos\Models\AkibatPromosi;

class PromosiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = DB::table('promosi')->first();
        $promosi = [
            'title' => 'Gebyar diskon 45!',
            'active_date' => '2018-08-17 00:00:00',
            'expired_date' => '2018-10-17 00:00:00'
        ];

       if (null == isset($check)) {
            DB::beginTransaction();
            try{
                $model = Promosi::create($promosi);
                # Beli 2 gratis 1
                $sebab = [
                    'promosi_id' => $model->id,
                    'akibat_promosi_id' => null,
                    'produk_id' => 6,
                    'qty' => 2,
                    'total' => 0,
                    'type_sebab' => 0,
                    'kelipatan' => 1,
                ];

                $modelSebab1 = SebabPromosi::create($sebab);

                $akibat = [
                    'promosi_id' => null,
                    'sebab_promosi_id' => $modelSebab1->id,
                    'produk_id' => 6,
                    'qty' => 1,
                    'total' => 2100,
                    'presentase' => 100
                ];

                AkibatPromosi::create($akibat);
                // $akibat = [];
                // $sebab = [];

                // # beli 3 item dapat 1 item

                // $akibat = [
                //     'promosi_id' => $model->id,
                //     'sebab_promosi_id' => null,
                //     'produk_id' => 6,
                //     'qty' => 1,
                //     'total' => 0,
                //     'presentase' => 0
                // ];

                // $modelAkibat1 = AkibatPromosi::create($akibat);

                // $sebab = [
                //     ['promosi_id' => null,
                //     'akibat_promosi_id' => $modelAkibat1->id,
                //     'produk_id' => 3,
                //     'qty' => 1,
                //     'total' => 0,
                //     'type_sebab' => 0,
                //     'kelipatan' => 0],
                    
                //     ['promosi_id' => null,
                //     'akibat_promosi_id' => $modelAkibat1->id,
                //     'produk_id' => 4,
                //     'qty' => 1,
                //     'total' => 0,
                //     'type_sebab' => 0,
                //     'kelipatan' => 0],

                //     ['promosi_id' => null,
                //     'akibat_promosi_id' => $modelAkibat1->id,
                //     'produk_id' => 5,
                //     'qty' => 1,
                //     'total' => 0,
                //     'type_sebab' => 0,
                //     'kelipatan' => 0]
                // ];

                // SebabPromosi::insert($sebab);

                DB::commit();
            }catch(Exception $e){
                DB::rollback();
                throw $e;
            }

       } else {
           $this->command->info('sudah mempunyai data');
       }
   
    }
}
