<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\TipeAkun;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\MataUang;
use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\JurnalUmum;
use App\Modules\Akuntansi\Models\DetailJurnalUmum;
use Helpers\TransaksiHelper;

class NewAkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $tipe_akun = TipeAkun::all();
        $exists = DB::table('akun')->first();
        $tipe_kas_bank = TipeAkun::where('title', 'Kas/Bank')->pluck('id')->first();
        $tipe_piutang = TipeAkun::where('title', 'Akun Piutang')->pluck('id')->first();
        $tipe_aktiva_tetap = TipeAkun::where('title', 'Aktiva Tetap')->pluck('id')->first();
        $tipe_persediaan = TipeAkun::where('title', 'Persediaan')->pluck('id')->first();
        $tipe_aktiva_lainnya = TipeAkun::where('title', 'Aktiva Lainnya')->pluck('id')->first();
        $tipe_aktiva_lancar = TipeAkun::where('title', 'Aktiva Lancar')->pluck('id')->first();
        $tipe_akumulasi_penyusutan = TipeAkun::where('title', 'Akumulasi Penyusutan')->pluck('id')->first();
        $tipe_hutang = TipeAkun::where('title', 'Akun Hutang')->pluck('id')->first();
        $tipe_hutang_lancar = TipeAkun::where('title', 'Hutang Lancar Lainnya')->pluck('id')->first();
        $tipe_ekuitas = TipeAkun::where('title', 'Ekuitas')->pluck('id')->first();
        $tipe_pendapatan = TipeAkun::where('title', 'Pendapatan')->pluck('id')->first();
        $tipe_hpp = TipeAkun::where('title', 'Harga Pokok Penjualan')->pluck('id')->first();
        $tipe_beban = TipeAkun::where('title', 'Beban')->pluck('id')->first();
        $tipe_pendapatan_lain = TipeAkun::where('title', 'Pendapatan Lain')->pluck('id')->first();
        $tipe_beban_lain = TipeAkun::where('title', 'Beban lain-lain')->pluck('id')->first();
        $mata_uang = MataUang::orderBy('id')->first();

        $all_akun = [
            'Kas' => [
                        'kode_akun' => '1101',
                        'nama_akun' => 'Kas',
                        'parent_id' => null,
                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_kas_bank,
                        'Child' => [
                                                [
                                                    'kode_akun' => '1101-001',
                                                    'nama_akun' => 'Kas IDR',
                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_kas_bank,
                                                ],
                                                [
                                                    'kode_akun' => '1101-002',
                                                    'nama_akun' => 'Kas USD',
                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_kas_bank,
                                                ],
                                                [
                                                    'kode_akun' => '1101-003',
                                                    'nama_akun' => 'Kas SGD',
                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_kas_bank,
                                                ],
                                            ],
                      ],
            'Bank' => [
                        'kode_akun' => '1102',
                        'nama_akun' => 'Bank',
                        'parent_id' => null,
                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_kas_bank,
                        'Child' => [
                                                [
                                                    'kode_akun' => '1102-001',
                                                    'nama_akun' => 'Mandiri IDR',
                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_kas_bank,
                                                ],
                                                [
                                                    'kode_akun' => '1102-002',
                                                    'nama_akun' => 'BCA IDR',
                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_kas_bank,
                                                ],
                                                [
                                                    'kode_akun' => '1102-003',
                                                    'nama_akun' => 'Danamon USD',
                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_kas_bank,
                                                ],
                                                [
                                                    'kode_akun' => '1102-005',
                                                    'nama_akun' => 'Panin SGD',
                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_kas_bank,
                                                ],
                                           ],
                      ],
            'Piutang Usaha' => [
                                'kode_akun' => '1103',
                                'nama_akun' => 'Piutang Usaha',
                                'parent_id' => null,
                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_piutang,
                                'Child' => [
                                                        [
                                                            'kode_akun' => '1103-001',
                                                            'nama_akun' => 'Piutang Usaha IDR',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_piutang,
                                                        ],
                                                        [
                                                            'kode_akun' => '1103-002',
                                                            'nama_akun' => 'Piutang Usaha Proyek IDR',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_piutang,
                                                        ],
                                                        [
                                                            'kode_akun' => '1103-003',
                                                            'nama_akun' => 'Piutang Usaha USD',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_piutang,
                                                        ],
                                                        [
                                                            'kode_akun' => '1103-004',
                                                            'nama_akun' => 'Piutang Usaha SGD',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_piutang,
                                                        ],
                                                        [
                                                            'kode_akun' => '1103.1',
                                                            'nama_akun' => 'Piutang Usaha',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_piutang,
                                                        ],
                                                   ],
                               ],
            'Uang Muka Pembelian' => [
                                        'kode_akun' => '1104',
                                        'nama_akun' => 'Uang Muka Pembelian',
                                        'parent_id' => null,
                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_piutang,
                                        'Child' => [
                                                                [
                                                                    'kode_akun' => '1104-001',
                                                                    'nama_akun' => 'Uang Muka Pembelian IDR',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_piutang,
                                                                ],
                                                                [
                                                                    'kode_akun' => '1104-002',
                                                                    'nama_akun' => 'Uang Muka Pembelian USD',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_piutang,
                                                                ],
                                                                [
                                                                    'kode_akun' => '1104-003',
                                                                    'nama_akun' => 'Uang Muka Pembelian SGD',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_piutang,
                                                                ],
                                                           ],
                                     ],
            'Persediaan Barang Dagang' => [
                                                'kode_akun' => '1105',
                                                'nama_akun' => 'Persediaan Barang Dagang',
                                                'parent_id' => null,
                                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_persediaan,
                                                'Child' => [
                                                                       [
                                                                            'kode_akun' => '1105-001',
                                                                            'nama_akun' => 'Persediaan Barang Toko',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_persediaan,
                                                                       ],
                                                                       [
                                                                            'kode_akun' => '1105-002',
                                                                            'nama_akun' => 'Persediaan Perkakas',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_persediaan,
                                                                       ],
                                                                       [
                                                                            'kode_akun' => '1105-003',
                                                                            'nama_akun' => 'Persediaan Elektronik',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_persediaan,
                                                                       ],
                                                                       [
                                                                            'kode_akun' => '1105-004',
                                                                            'nama_akun' => 'Persediaan Furniture',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_persediaan,
                                                                       ],
                                                                       [
                                                                            'kode_akun' => '1105-005',
                                                                            'nama_akun' => 'Persediaan Pesanan',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_persediaan,
                                                                       ],
                                                                   ],
                                          ],
            'Persediaan Dalam Proses Manufaktur' => [
                                                        'kode_akun' => '1105-666',
                                                        'nama_akun' => 'Persediaan Dalam Proses Manufaktur',
                                                        'parent_id' => null,
                                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_persediaan,
                                                    ],

            'Persediaan Dalam Proses' => [
                                            'kode_akun' => '1105-998',
                                            'nama_akun' => 'Persediaan Dalam Proses',
                                            'parent_id' => null,
                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_persediaan,
                                         ],

            'Barang Terkirim' => [
                                    'kode_akun' => '1105-999',
                                    'nama_akun' => 'Barang Terkirim',
                                    'parent_id' => null,
                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_persediaan,
                                 ],

            'Biaya dibayar dimuka' => [
                                                'kode_akun' => '1106',
                                                'nama_akun' => 'Biaya dibayar dimuka',
                                                'parent_id' => null,
                                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_lancar,
                                                'Child' => [
                                                                        [
                                                                            'kode_akun' => '1106-001',
                                                                            'nama_akun' => 'Sewa dibayar dimuka',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_lancar,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '1106-002',
                                                                            'nama_akun' => 'Asuransi dibayar dimuka',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_lancar,
                                                                        ],
                                                                   ],
                                      ],

            'PPN Masukan' => [
                                'kode_akun' => '1107',
                                'nama_akun' => 'PPN Masukan',
                                'parent_id' => null,
                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_lancar,
                             ],

            'PPh 23 Final (Sales Tax)' => [
                                            'kode_akun' => '1108',
                                            'nama_akun' => 'PPh 23 Final (Sales Tax)',
                                            'parent_id' => null,
                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_lancar,
                                          ],

            'Bea Masuk' => [
                            'kode_akun' => '1109',
                            'nama_akun' => 'Bea Masuk',
                            'parent_id' => null,
                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_lancar,
                           ],

            'Perlengkapan (Supplies)' => [
                                            'kode_akun' => '1110',
                                            'nama_akun' => 'Perlengkapan (Supplies)',
                                            'parent_id' => null,
                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_lancar,
                                          ],

            'Akun Silang' => [
                                'kode_akun' => '1111',
                                'nama_akun' => 'Akun Silang',
                                'parent_id' => null,
                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_lancar,
                             ],

            'Transaksi Aktiva Tetap' => [
                                            'kode_akun' => '1112',
                                            'nama_akun' => 'Transaksi Aktiva Tetap',
                                            'parent_id' => null,
                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_lancar,
                                        ],

            'PPN Lebih Bayar' => [
                                    'kode_akun' => '1113',
                                    'nama_akun' => 'PPN Lebih Bayar',
                                    'parent_id' => null,
                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_lancar,
                                 ],

            'Proyek Dalam Proses' => [
                                        'kode_akun' => '1114',
                                        'nama_akun' => 'Proyek Dalam Proses',
                                        'parent_id' => null,
                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_lancar,
                                     ],

            'Aktiva Tetap' => [
                                                'kode_akun' => '1201',
                                                'nama_akun' => 'Aktiva Tetap',
                                                'parent_id' => null,
                                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_tetap,
                                                'Child' => [
                                                                        [
                                                                            'kode_akun' => '1201-001',
                                                                            'nama_akun' => 'Tanah',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_tetap,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '1201-002',
                                                                            'nama_akun' => 'Bangunan',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_tetap,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '1201-003',
                                                                            'nama_akun' => 'Peralatan Kantor',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_tetap,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '1201-004',
                                                                            'nama_akun' => 'Peralatan Toko',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_tetap,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '1201-005',
                                                                            'nama_akun' => 'Kendaraan',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_aktiva_tetap,
                                                                        ],
                                                                   ],
                               ],

            'Akumulasi Penyusutan' => [
                                        'kode_akun' => '1202',
                                        'nama_akun' => 'Akumulasi Penyusutan',
                                        'parent_id' => null,
                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_akumulasi_penyusutan,
                                        'Child' => [
                                                                [
                                                                    'kode_akun' => '1202-001',
                                                                    'nama_akun' => 'Akum. Penys. Bangunan',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_akumulasi_penyusutan,
                                                                ],
                                                                [
                                                                    'kode_akun' => '1202-002',
                                                                    'nama_akun' => 'Akum. Penys. Peralatan Kantor',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_akumulasi_penyusutan,
                                                                ],
                                                                [
                                                                    'kode_akun' => '1202-003',
                                                                    'nama_akun' => 'Akum. Penys. Peralatan Toko',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_akumulasi_penyusutan,
                                                                ],
                                                                [
                                                                    'kode_akun' => '1202-004',
                                                                    'nama_akun' => 'Akum. Penys. Kendaraan',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_akumulasi_penyusutan,
                                                                ],
                                                           ],
                                      ],

            'Hutang Usaha' => [
                                'kode_akun' => '2101',
                                'nama_akun' => 'Hutang Usaha',
                                'parent_id' => null,
                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang,
                                'Child' => [
                                                        [
                                                            'kode_akun' => '2101-001',
                                                            'nama_akun' => 'Hutang Usaha IDR',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang,
                                                        ],
                                                        [
                                                            'kode_akun' => '2101-002',
                                                            'nama_akun' => 'Hutang Usaha USD',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang,
                                                        ],
                                                        [
                                                            'kode_akun' => '2101-003',
                                                            'nama_akun' => 'Hutang Usaha SGD',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang,
                                                        ],
                                                        [
                                                            'kode_akun' => '2101.1',
                                                            'nama_akun' => 'Hutang Usaha',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang,
                                                        ],
                                                   ],
                              ],

            'Uang Muka Penjualan' => [
                                        'kode_akun' => '2102',
                                        'nama_akun' => 'Uang Muka Penjualan',
                                        'parent_id' => null,
                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang,
                                        'Child' => [
                                                                [
                                                                    'kode_akun' => '2102-001',
                                                                    'nama_akun' => 'Uang Muka Penjualan IDR',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang,
                                                                ],
                                                                [
                                                                    'kode_akun' => '2102-002',
                                                                    'nama_akun' => 'Uang Muka Penjualan USD',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang,
                                                                ],
                                                                [
                                                                    'kode_akun' => '2102-003',
                                                                    'nama_akun' => 'Uang Muka Penjualan SGD',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang,
                                                                ],
                                                           ],
                                     ],

            'PPN Keluaran' => [
                                'kode_akun' => '2103',
                                'nama_akun' => 'PPN Keluaran',
                                'parent_id' => null,
                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang_lancar,
                              ],

            'PPh 23 Final (Purchase Tax)' => [
                                                'kode_akun' => '2104',
                                                'nama_akun' => 'PPh 23 Final (Purchase Tax)',
                                                'parent_id' => null,
                                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang_lancar,
                                             ],
            'Hutang Biaya' => [
                                'kode_akun' => '2105',
                                'nama_akun' => 'Hutang Biaya',
                                'parent_id' => null,
                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang_lancar,
                                'Child' => [
                                                        [
                                                            'kode_akun' => '2105-001',
                                                            'nama_akun' => 'Hutang Bunga',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang_lancar,
                                                        ],
                                                        [
                                                            'kode_akun' => '2105-002',
                                                            'nama_akun' => 'Hutang Gaji',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang_lancar,
                                                        ],
                                                        [
                                                            'kode_akun' => '2105-003',
                                                            'nama_akun' => 'Hutang Sewa Alat Proyek',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang_lancar,
                                                        ],
                                                        [
                                                            'kode_akun' => '2105-004',
                                                            'nama_akun' => 'Hutang Biaya Proyek Lain-Lain',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang_lancar,
                                                        ],
                                                        [
                                                            'kode_akun' => '2105-005',
                                                            'nama_akun' => 'Hutang Gaji/Upah Karyawan Proyek',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang_lancar,
                                                        ],
                                                   ],
                              ],

            'Penerimaan Barang Belum Tertagih' => [
                                                    'kode_akun' => '2106',
                                                    'nama_akun' => 'Penerimaan Barang Belum Tertagih',
                                                    'parent_id' => null,
                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang_lancar,
                                                  ],

            'PPN Kurang Bayar' => [
                                    'kode_akun' => '2107',
                                    'nama_akun' => 'PPN Kurang Bayar',
                                    'parent_id' => null,
                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang_lancar,
                                  ],

            'Pendapatan Proyek Diterima Dimuka' => [
                                                        'kode_akun' => '2108',
                                                        'nama_akun' => 'Pendapatan Proyek Diterima Dimuka',
                                                        'parent_id' => null,
                                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang_lancar,
                                                   ],

            'Hutang Jangka Panjang' => [
                                            'kode_akun' => '2201',
                                            'nama_akun' => 'Hutang Jangka Panjang',
                                            'parent_id' => null,
                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hutang_lancar,
                                       ],

            'Modal' => [
                            'kode_akun' => '3000',
                            'nama_akun' => 'Modal',
                            'parent_id' => null,
                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_ekuitas,
                       ],

            'Deviden' => [
                            'kode_akun' => '3100',
                            'nama_akun' => 'Deviden',
                            'parent_id' => null,
                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_ekuitas,
                         ],

            'Laba Ditahan' => [
                                'kode_akun' => '3200',
                                'nama_akun' => 'Laba Ditahan',
                                'parent_id' => null,
                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_ekuitas,
                             ],

            'Opening Balance Equity' => [
                                            'kode_akun' => '3300',
                                            'nama_akun' => 'Opening Balance Equity',
                                            'parent_id' => null,
                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_ekuitas,
                                         ],

            'Pendapatan' => [
                                'kode_akun' => '4101',
                                'nama_akun' => 'Pendapatan',
                                'parent_id' => null,
                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                'Child' => [
                                                        [
                                                            'kode_akun' => '4101-001',
                                                            'nama_akun' => 'Penjualan Barang Toko',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                        [
                                                            'kode_akun' => '4101-002',
                                                            'nama_akun' => 'Penjualan Perkakas',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                        [
                                                            'kode_akun' => '4101-003',
                                                            'nama_akun' => 'Penjualan Electronik',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                        [
                                                            'kode_akun' => '4101-004',
                                                            'nama_akun' => 'Penjualan Furnitur',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                        [
                                                            'kode_akun' => '4101-005',
                                                            'nama_akun' => 'Penjualan Grup',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                        [
                                                            'kode_akun' => '4101-006',
                                                            'nama_akun' => 'Pendapatan Jasa',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                        [
                                                            'kode_akun' => '4101-007',
                                                            'nama_akun' => 'Penjualan Pesanan',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                        [
                                                            'kode_akun' => '4101-008',
                                                            'nama_akun' => 'Pendapatan Proyek',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                   ],
                            ],

        'Retur Penjualan' => [
                                'kode_akun' => '4201',
                                'nama_akun' => 'Retur Penjualan',
                                'parent_id' => null,
                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                'Child' => [
                                                        [
                                                            'kode_akun' => '4201-001',
                                                            'nama_akun' => 'Retur Penjualan Barang Toko',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                        [
                                                            'kode_akun' => '4201-002',
                                                            'nama_akun' => 'Retur Penjualan Perkakas',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                        [
                                                            'kode_akun' => '4201-003',
                                                            'nama_akun' => 'Retur Penjualan Electronik',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                        [
                                                            'kode_akun' => '4201-004',
                                                            'nama_akun' => 'Retur Penjualan Furnitur',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                        [
                                                            'kode_akun' => '4201-005',
                                                            'nama_akun' => 'Retur Penjualan Grup',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                        [
                                                            'kode_akun' => '4201-006',
                                                            'nama_akun' => 'Retur Penjualan Pesanan',
                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                                        ],
                                                   ],
                             ],

        'Potongan Penjualan' => [
                                    'kode_akun' => '4202',
                                    'nama_akun' => 'Potongan Penjualan',
                                    'parent_id' => null,
                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan,
                                ],

        'Harga Pokok Penjualan' => [
                                        'kode_akun' => '5100',
                                        'nama_akun' => 'Harga Pokok Penjualan',
                                        'parent_id' => null,
                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hpp,
                                        'Child' => [
                                                                [
                                                                    'kode_akun' => '5100-001',
                                                                    'nama_akun' => 'HPP Barang Toko',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hpp,
                                                                ],
                                                                [
                                                                    'kode_akun' => '5100-002',
                                                                    'nama_akun' => 'HPP Perkakas',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hpp,
                                                                ],
                                                                [
                                                                    'kode_akun' => '5100-003',
                                                                    'nama_akun' => 'HPP Electronik',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hpp,
                                                                ],
                                                                [
                                                                    'kode_akun' => '5100-004',
                                                                    'nama_akun' => 'HPP Furnitur',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hpp,
                                                                ],
                                                                [
                                                                    'kode_akun' => '5100-005',
                                                                    'nama_akun' => 'HPP Pesanan',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hpp,
                                                                ],
                                                           ],
                                   ],

            'HPP Proyek' => [
                                'kode_akun' => '5100-006',
                                'nama_akun' => 'HPP Proyek',
                                'parent_id' => null,
                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hpp,
                            ],

            'variance' => [
                            'kode_akun' => '5100-666',
                            'nama_akun' => 'variance',
                            'parent_id' => null,
                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hpp,
                          ],

            'Biaya Angkut Pembelian' => [
                                            'kode_akun' => '5200',
                                            'nama_akun' => 'Biaya Angkut Pembelian',
                                            'parent_id' => null,
                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hpp,
                                         ],

            'Potongan Pembelian' => [
                                        'kode_akun' => '5300',
                                        'nama_akun' => 'Potongan Pembelian',
                                        'parent_id' => null,
                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_hpp,
                                     ],

             'Beban Penyusutan' => [
                                        'kode_akun' => '6100',
                                        'nama_akun' => 'Beban Penyusutan',
                                        'parent_id' => null,
                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                        'Child' => [
                                                                [
                                                                    'kode_akun' => '6100-001',
                                                                    'nama_akun' => 'Beban Penyusutan Bangunan',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                ],
                                                                [
                                                                    'kode_akun' => '6100-002',
                                                                    'nama_akun' => 'Beban Penyusutan Peralatan Kantor',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                ],
                                                                [
                                                                    'kode_akun' => '6100-003',
                                                                    'nama_akun' => 'Beban Penyusutan Peralatan Toko',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                ],
                                                                [
                                                                    'kode_akun' => '6100-004',
                                                                    'nama_akun' => 'Beban Penyusutan Kendaraan',
                                                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                ],
                                                           ],
                                     ],

            'Biaya Umum & Administrasi' => [
                                                'kode_akun' => '6200',
                                                'nama_akun' => 'Biaya Umum & Administrasi',
                                                'parent_id' => null,
                                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                'Child' => [
                                                                        [
                                                                            'kode_akun' => '6200-001',
                                                                            'nama_akun' => 'Biaya Listrik, PAM & Telp',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '6200-002',
                                                                            'nama_akun' => 'Biaya Gaji & Upah',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '6200-003',
                                                                            'nama_akun' => 'Biaya Gaji/Upah Karyawan Proyek',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '6200-004',
                                                                            'nama_akun' => 'Biaya Pemasaran',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '6200-005',
                                                                            'nama_akun' => 'Biaya Umum & Adm Lainya',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '6200-006',
                                                                            'nama_akun' => 'Biaya Sewa',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '6200-007',
                                                                            'nama_akun' => 'Biaya Asuransi',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '6200-008',
                                                                            'nama_akun' => 'Beban Perlengkapan',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '6200-009',
                                                                            'nama_akun' => 'Beban Komisi Penjualan',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '6200-010',
                                                                            'nama_akun' => 'Biaya Sewa Alat Proyek',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '6200-011',
                                                                            'nama_akun' => 'Biaya Proyek Lain-lain',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                                        ],
                                                                   ],
                                           ],

            'Biaya Gaji/Upah Karyawan Manufaktur' => [
                                                        'kode_akun' => '6300',
                                                        'nama_akun' => 'Biaya Gaji/Upah Karyawan Manufaktur',
                                                        'parent_id' => null,
                                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban,
                                                     ],

            'Pendapatan Lain-lain' => [
                                            'kode_akun' => '7100',
                                            'nama_akun' => 'Pendapatan Lain-lain',
                                            'parent_id' => null,
                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan_lain,
                                            'Child' => [
                                                                    [
                                                                        'kode_akun' => '7100-001',
                                                                        'nama_akun' => 'Pendapatan Bunga Bank',
                                                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan_lain,
                                                                    ],
                                                                    [
                                                                        'kode_akun' => '7100-002',
                                                                        'nama_akun' => 'Penjualan Aktiva Tetap',
                                                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan_lain,
                                                                    ],
                                                                    [
                                                                        'kode_akun' => '7100-003',
                                                                        'nama_akun' => 'Pendapatan Lain-lain',
                                                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan_lain,
                                                                    ],
                                                               ],
                                      ],

            'Biaya Lain-lain' => [
                                    'kode_akun' => '7200',
                                    'nama_akun' => 'Biaya Lain-lain',
                                    'parent_id' => null,
                                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                    'Child' => [
                                                            [
                                                                'kode_akun' => '7200-001',
                                                                'nama_akun' => 'Biaya Administrasi Bank',
                                                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                            ],
                                                            [
                                                                'kode_akun' => '7200-002',
                                                                'nama_akun' => 'Pajak Bunga Bank',
                                                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                            ],
                                                            [
                                                                'kode_akun' => '7200-003',
                                                                'nama_akun' => 'Biaya Bunga Pinjaman Lainya',
                                                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                            ],
                                                            [
                                                                'kode_akun' => '7200-004',
                                                                'nama_akun' => 'Biaya Lain-lain',
                                                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                            ],
                                                            [
                                                                'kode_akun' => '7200-005',
                                                                'nama_akun' => 'Selisih Barang Terkirim',
                                                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                            ],
                                                       ],
                                 ],

            'Laba/Rugi Terealisasi' => [
                                            'kode_akun' => '7300',
                                            'nama_akun' => 'Laba/Rugi Terealisasi',
                                            'parent_id' => null,
                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                            'Child' => [
                                                                    [
                                                                        'kode_akun' => '7300-001',
                                                                        'nama_akun' => 'Laba/Rugi Terealisasi IDR',
                                                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                                    ],
                                                                    [
                                                                        'kode_akun' => '7300-002',
                                                                        'nama_akun' => 'Laba/Rugi Terealisasi USD',
                                                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                                    ],
                                                                    [
                                                                        'kode_akun' => '7300-003',
                                                                        'nama_akun' => 'Laba/Rugi Terealisasi SGD',
                                                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                                    ],
                                                               ],
                                       ],

            'Laba/Rugi Belum Terealisasi' => [
                                                'kode_akun' => '7400',
                                                'nama_akun' => 'Laba/Rugi Belum Terealisasi',
                                                'parent_id' => null,
                                                'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                'Child' => [
                                                                        [
                                                                            'kode_akun' => '7400-001',
                                                                            'nama_akun' => 'Laba/Rugi Belum Terealisasi IDR',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '7400-002',
                                                                            'nama_akun' => 'Laba/Rugi Belum Terealisasi USD',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                                        ],
                                                                        [
                                                                            'kode_akun' => '7400-003',
                                                                            'nama_akun' => 'Laba/Rugi Belum Terealisasi SGD',
                                                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                                        ],
                                                                   ],
                                             ],

            'Beban Pajak Penghasilan' => [
                                            'kode_akun' => '7500',
                                            'nama_akun' => 'Beban Pajak Penghasilan',
                                            'parent_id' => null,
                                            'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                         ],

            'Laba/Rugi Penghentian Aktiva Tetap' => [
                                                        'kode_akun' => '8100',
                                                        'nama_akun' => 'Laba/Rugi Penghentian Aktiva Tetap',
                                                        'parent_id' => null,
                                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_beban_lain,
                                                     ],

            'Laba/Rugi Revaluasi Aktiva Tetap' => [
                                                        'kode_akun' => '8200',
                                                        'nama_akun' => 'Laba/Rugi Revaluasi Aktiva Tetap',
                                                        'parent_id' => null,
                                                        'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_pendapatan_lain,
                                                     ],
];

		if (empty($exists)) {
               foreach ($all_akun as $key => $value) {
                   $parent = Akun::create([
                       'kode_akun' 	=> $value['kode_akun'],
                       'nama_akun'		=> $value['nama_akun'],
                       'parent_id'		=> $value['parent_id'],
                       'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' =>$value['tipe_akun_id']
                   ]);
                   if (!empty($value['Child'])) {
                       foreach ($value['Child'] as $index => $data) {
                           $data['parent_id'] = $parent->id;
                           $child = Akun::create($data);
                       }
                   }
			   }
			// $detail = [];
			// $idr = Akun::where('kode_akun','1101-001')->first();
   //          $inputJurnalUmum = [
   //              'no_faktur' => '1',
   //              'tanggal' => date('Y-m-d H:i:s'),
   //              'description' => 'Saldo Awal',
   //          ];
   //          $modelJurnalUmum = JurnalUmum::create($inputJurnalUmum);
   //          $detail[] = [
   //              'akun_id' => $idr->id,
   //              'jurnal_umum_id' => $modelJurnalUmum->id,
   //              'debit' => 100000000,
   //              'kredit' => 0,
   //          ];

   //          $detail[] = [
   //              'akun_id' => $idr->id,
   //              'jurnal_umum_id' => $modelJurnalUmum->id,
   //              'debit' => 0,
   //              'kredit' => 100000000,
   //          ];

   //          DetailJurnalUmum::insert($detail);

   //          // T R A N S A K S I

   //          $transaksi = [
   //              'nominal' => 100000000,
   //              'tanggal' => date('Y-m-d H:i:s'),
   //              'dari' => 'Saldo Awal Akun',
   //          ];

   //          $debit = $transaksi;
   //          $kredit = $transaksi;
   //          $debit['akun_id'] = $idr->id;
   //          $kredit['akun_id'] = $idr->id;

			// $transaki_helper = new TransaksiHelper;
   //          $transaki_helper->ExecuteTransactionSingle('insert', $idr, $debit, 1, rand(10, 100));
   //          $transaki_helper->ExecuteTransactionSingle('insert', $idr, $kredit, 0, rand(10, 100));

		}else {
			$this->command->info('data sudah ada');
		}
    }
}
