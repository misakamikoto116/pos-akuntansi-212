<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\JasaPengiriman;
class JasaPengirimanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = DB::table('jasa_pengiriman')->first();
        $namas   = ['JNE','TIKI','J&T','POS','KILAT'];

        if ($exists == null) {
        	foreach ($namas as $nama) {
        		JasaPengiriman::create([
        			'nama' => $nama
        		]);
        	}
        }else {
        	$this->command->info('data sudah ada');
        }

    }
}
