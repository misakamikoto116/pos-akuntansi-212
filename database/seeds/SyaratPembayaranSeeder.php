<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\SyaratPembayaran;
class SyaratPembayaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = DB::table('syarat_pembayaran')->first();
        $namas   = [
            [
                'jika_membayar_antara' => '10',
                'akan_dapat_diskon'    => '10',
                'jatuh_tempo'          => '30',
                'keterangan'           => 'baik',
                'status_cod'               => 0
            ],
            [
                'jika_membayar_antara' => '10',
                'akan_dapat_diskon'    => '5',
                'jatuh_tempo'          => '60',
                'keterangan'           => 'baik',
                'status_cod'               => 0
            ],
            [
                'jika_membayar_antara' => '5',
                'akan_dapat_diskon'    => '25',
                'jatuh_tempo'          => '20',
                'keterangan'           => 'baik',
                'status_cod'               => 0
            ],
            [
                'jika_membayar_antara' => '10',
                'akan_dapat_diskon'    => '30',
                'jatuh_tempo'          => '25',
                'keterangan'           => 'baik',
                'status_cod'               => 0
            ],
            [
                'jika_membayar_antara' => '14',
                'akan_dapat_diskon'    => '35',
                'jatuh_tempo'          => '30',
                'keterangan'           => 'baik',
                'status_cod'               => 0
            ],
        ];

        if ($exists == null) {
        	foreach ($namas as $nama) {
        		SyaratPembayaran::create($nama);
        	}
        }else {
        	$this->command->info('data sudah ada');
        }
    }
}
