<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = Permission::get()->isEmpty();

        $permissions = [
        // Penjualan
            'buat_penawaran_penjualan',
            'ubah_penawaran_penjualan',
            'hapus_penawaran_penjualan',
            'laporan_penawaran_penjualan',
            'lihat_penawaran_penjualan',
            'daftar_penawaran_penjualan',

            'buat_pesanan_penjualan',
            'ubah_pesanan_penjualan',
            'hapus_pesanan_penjualan',
            'laporan_pesanan_penjualan',
            'lihat_pesanan_penjualan',
            'daftar_pesanan_penjualan',

            'buat_pengiriman_penjualan',
            'ubah_pengiriman_penjualan',
            'hapus_pengiriman_penjualan',
            'laporan_pengiriman_penjualan',
            'lihat_pengiriman_penjualan',
            'daftar_pengiriman_penjualan',

            'buat_faktur_penjualan',
            'ubah_faktur_penjualan',
            'hapus_faktur_penjualan',
            'laporan_faktur_penjualan',
            'lihat_faktur_penjualan',
            'daftar_faktur_penjualan',
            
            'buat_penerimaan_penjualan',
            'ubah_penerimaan_penjualan',
            'hapus_penerimaan_penjualan',
            'laporan_penerimaan_penjualan',
            'lihat_penerimaan_penjualan',
            'daftar_penerimaan_penjualan',
            
            'buat_retur_penjualan',
            'ubah_retur_penjualan',
            'hapus_retur_penjualan',
            'laporan_retur_penjualan',
            'lihat_retur_penjualan',
            'daftar_retur_penjualan',
        // End -->
        // Pembelian
            'buat_permintaan_pembelian',
            'ubah_permintaan_pembelian',
            'hapus_permintaan_pembelian',
            'laporan_permintaan_pembelian',
            'lihat_permintaan_pembelian',
            'daftar_permintaan_pembelian',
            
            'buat_pesanan_pembelian',
            'ubah_pesanan_pembelian',
            'hapus_pesanan_pembelian',
            'laporan_pesanan_pembelian',
            'lihat_pesanan_pembelian',
            'daftar_pesanan_pembelian',
            
            'buat_penerimaan_pembelian',
            'ubah_penerimaan_pembelian',
            'hapus_penerimaan_pembelian',
            'laporan_penerimaan_pembelian',
            'lihat_penerimaan_pembelian',
            'daftar_penerimaan_pembelian',
            
            'buat_faktur_pembelian',
            'ubah_faktur_pembelian',
            'hapus_faktur_pembelian',
            'laporan_faktur_pembelian',
            'lihat_faktur_pembelian',
            'daftar_faktur_pembelian',
            
            'buat_pembayaran_pembelian',
            'ubah_pembayaran_pembelian',
            'hapus_pembayaran_pembelian',
            'laporan_pembayaran_pembelian',
            'lihat_pembayaran_pembelian',
            'daftar_pembayaran_pembelian',
            
            'buat_retur_pembelian',
            'ubah_retur_pembelian',
            'hapus_retur_pembelian',
            'laporan_retur_pembelian',
            'lihat_retur_pembelian',
            'daftar_retur_pembelian',
        // End -->
        // Barang Jasa
            'buat_barang',
            'ubah_barang',
            'hapus_barang',
            'laporan_barang',

            'buat_gudang',
            'ubah_gudang',
            'hapus_gudang',
            'laporan_gudang',

            'buat_pindah_barang',
            'ubah_pindah_barang',
            'hapus_pindah_barang',
            'laporan_pindah_barang',

            'buat_pembiayaan_pesanan',
            'ubah_pembiayaan_pesanan',
            'hapus_pembiayaan_pesanan',
            'laporan_pembiayaan_pesanan',
            'lihat_pembiayaan_pesanan',
            'daftar_pembiayaan_pesanan',

            'buat_penyesuaian_persediaan',
            'ubah_penyesuaian_persediaan',
            'hapus_penyesuaian_persediaan',
            'laporan_penyesuaian_persediaan',
            'lihat_penyesuaian_persediaan',
            'daftar_penyesuaian_persediaan',

            'buat_selling_price_adjusment',
            'ubah_selling_price_adjusment',
            'hapus_selling_price_adjusment',
            'laporan_selling_price_adjusment',
            'lihat_selling_price_adjusment',
            'daftar_selling_price_adjusment',
        // End -->
        // Buku Besar
            'buat_daftar_akun',
            'ubah_daftar_akun',
            'hapus_daftar_akun',
            'laporan_daftar_akun',

            'buat_bukti_jurnal_umum',
            'ubah_bukti_jurnal_umum',
            'hapus_bukti_jurnal_umum',
            'laporan_bukti_jurnal_umum',
            'lihat_bukti_jurnal_umum',
            'daftar_bukti_jurnal_umum',
            
            'buat_data_pelanggan',
            'ubah_data_pelanggan',
            'hapus_data_pelanggan',
            'laporan_data_pelanggan',

            'buat_data_pemasok',
            'ubah_data_pemasok',
            'hapus_data_pemasok',
            'laporan_data_pemasok',
            'lihat_data_pemasok',
            'daftar_data_pemasok',

            'buat_laporan_keuangan',
            'ubah_laporan_keuangan',
            'hapus_laporan_keuangan',
            'laporan_laporan_keuangan',
            'lihat_laporan_keuangan',
            'daftar_laporan_keuangan',
        // End -->
        // Bank & Aktiva
            'buat_pembayaran_lain',
            'ubah_pembayaran_lain',
            'hapus_pembayaran_lain',
            'laporan_pembayaran_lain',
            'lihat_pembayaran_lain',
            'daftar_pembayaran_lain',

            'buat_penerimaan_lain',
            'ubah_penerimaan_lain',
            'hapus_penerimaan_lain',
            'laporan_penerimaan_lain',
            'lihat_penerimaan_lain',
            'daftar_penerimaan_lain',

            'buat_aktiva_tetap',
            'ubah_aktiva_tetap',
            'hapus_aktiva_tetap',
            'laporan_aktiva_tetap',
            'lihat_aktiva_tetap',
            'daftar_aktiva_tetap',

            'buat_rekonsiliasi_bank',
            'buat_menampilkan_cetak_buku_bank',
        // End -->
        ];

        foreach ($permissions as $permission) {
            Permission::firstOrCreate([
                'name' => $permission,
                'guard_name' => 'web'
            ]);
        }
    }
}
