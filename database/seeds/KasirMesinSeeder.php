<?php

use Illuminate\Database\Seeder;
use App\Modules\Pos\Models\KasirMesin;
class KasirMesinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$exists = DB::table('kasir_mesin')->first();

        $datas = [
        	'Kasir 1',
        	'Kasir 2',
        	'Kasir 3',
        ];

        if ($exists == null) {
	        foreach ($datas as $data) {
	        	KasirMesin::create([
	        		'nama'		=> $data,
	        	]);
	        }
        }else {
			$this->command->info('data sudah ada');
		}

    }
}
