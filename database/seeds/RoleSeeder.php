<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = Role::get()->isEmpty();

        $roles = [
            'admin',
            'akuntansi',
            'pos',
            'supervisi',
            'gudang'
        ];

        foreach ($roles as $role) {
            Role::firstOrCreate([
                'name' => $role,
                'guard_name' => 'web'
            ]);
        }
    }
}
