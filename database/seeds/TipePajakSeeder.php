<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\TipePajak;
class TipePajakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = DB::table('tipe_pajak')->first();
        $namas   = [
        	[
        		'nama' => 'PPh',
        		'kode' => '100'
        	],
        	[
        		'nama' => 'PPN',
        		'kode' => '199'
        	],
        	[
        		'nama' => 'PPn BM',
        		'kode' => '200'
        	],
        	[
        		'nama' => 'PBB',
        		'kode' => '300'
        	],
        	[
        		'nama' => 'BPHTB',
        		'kode' => '310'
        	],
        ];

        if ($exists == null) {
        	foreach ($namas as $nama) {
        		TipePajak::create($nama);
        	}
        }else {
        	$this->command->info('data sudah ada');
        }
    }
}
