<?php

use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\MataUang;
use App\Modules\Akuntansi\Models\TipeAkun;
use Illuminate\Database\Seeder;

class OpeningBalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$exists = DB::table('akun')->first();
    	$mata_uang = MataUang::orderBy('id')->first();
    	$tipe_ekuitas = TipeAkun::where('title', 'Ekuitas')->pluck('id')->first();

    	$all_akun = [

                    'kode_akun' => '3300',
                    'nama_akun' => 'Opening Balance Equity',
                    'parent_id' => null,
                    'mata_uang_id' => $mata_uang->id, 'tipe_akun_id' => $tipe_ekuitas,

    	];

    	if (empty($exists)) {
               Akun::create($all_akun);
		}else {
			$this->command->info('data sudah ada');
		}
    }
}
