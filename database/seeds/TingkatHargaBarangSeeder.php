<?php

use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\TingkatHargaBarang;
use Faker\Factory;
use Illuminate\Database\Seeder;

class TingkatHargaBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $harga_barang = TingkatHargaBarang::get();

        $list_barang  = Produk::whereNotIn('tipe_barang',[3,4])->pluck('harga_jual','id');

        if ($harga_barang->isEmpty()) {
        	foreach ($list_barang as $id => $harga_jual) {
        		TingkatHargaBarang::create([
        			'tingkat_1'		=> $harga_jual,
        			'tingkat_2'		=> $faker->randomNumber(2).'00',
        			'tingkat_3'		=> $faker->randomNumber(2).'00',
        			'tingkat_4'		=> $faker->randomNumber(2).'00',
        			'tingkat_5'		=> $faker->randomNumber(2).'00',
        			'produk_id'		=> $id,
        		]);
        	}
        }else {
        	$this->command->info('data sudah ada');
        }
    }
}
