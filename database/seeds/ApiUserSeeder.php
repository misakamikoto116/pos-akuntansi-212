<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\Role;

class ApiUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::firstOrCreate([
            'name' => 'pramuniaga',
            'guard_name' => 'web'
        ]);
            $user = new \App\Modules\Akuntansi\Models\User();
            $user->name = 'pramuniaga';
            $user->email = 'pramuniaga@gmail.com';
            $user->password = bcrypt('123456');
            $user->save();


            $user->assignRole('pramuniaga');


    }
}
