<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\MataUang;

class MataUangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $check = DB::table('mata_uang')->first();
 

       $mata_uang = [
       		[
       			'negara' => 'indonesia',
       			'kode' => 'IDR',
       			'kode_simbol' => 'Rp',
       			'nama' => 'Rupiah',
       		],[
       			'negara' => 'Singapore',
       			'kode' => 'SGD',
       			'kode_simbol' => '$',
       			'nama' => 'Singapore Dollar',
       		],[
       			'negara' => 'United',
       			'kode' => 'USD',
       			'kode_simbol' => '$',
       			'nama' => 'US Dollar',
       		],[
       			'negara' => 'Malaysia',
       			'kode' => 'MYR',
       			'kode_simbol' => 'RM',
       			'nama' => 'Malaysian Ringgit',
       		],
       ];
       if (null == $check) {
            foreach ($mata_uang as $datas) {
                MataUang::create($datas);
           		}
          } else{
           		$this->command->info('sudah mempunyai data');
       }
    }
}