<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\TipeAkun;
use App\Modules\Akuntansi\Models\Akun;
class AkunSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$exists = DB::table('akun')->first();
    	$TipeAkun = TipeAkun::all();
        $akunParent = [
       		[
       			'kode_akun' => '1000',
       			'nama_akun' => 'Kas & Bank',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Kas/Bank')->first()->id,
       		],
       		[
       			'kode_akun' => '1100',
       			'nama_akun' => 'Piutang',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Akun Piutang')->first()->id,
       		],
       		[
       			'kode_akun' => '1700',
       			'nama_akun' => 'Aktiva Tetap',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Aktiva Tetap')->first()->id,
       		],
       		[
       			'kode_akun' => '1710',
       			'nama_akun' => 'Akumulasi Depresiasi Fixed Asset',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Akumulasi Penyusutan')->first()->id,
       		],
       		[
       			'kode_akun' => '2000',
       			'nama_akun' => 'Hutang',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Akun Hutang')->first()->id,
       		],
       		[
       			'kode_akun' => '4000',
       			'nama_akun' => 'Pendapatan',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Pendapatan')->first()->id,
       		],
       		[
       			'kode_akun' => '6000',
       			'nama_akun' => 'BIAYA PEMASARAN UMUM & ADM',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
       		],
       		[
       			'kode_akun' => '6200',
       			'nama_akun' => 'Biaya Umum & Administrasi',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
       		],
       		[
       			'kode_akun' => '7100',
       			'nama_akun' => 'PENDAPATAN DILUAR USAHA',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Pendapatan lain')->first()->id,
       		],
       		[
       			'kode_akun' => '7200',
       			'nama_akun' => 'BIAYA DILUAR USAHA',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Beban lain-lain')->first()->id,
       		],
       		[
       			'kode_akun' => '8100',
       			'nama_akun' => 'Gain/Loss Dispossal F.A',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Beban lain-lain')->first()->id,
       		],[
       			'kode_akun' => '8200',
       			'nama_akun' => 'Gain/Loss Revaluation FA',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Pendapatan lain')->first()->id,
       		],[
       			'kode_akun' => '910002',
       			'nama_akun' => 'Realize Gain or Loss IDR',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
       		],[
       			'kode_akun' => '910003',
       			'nama_akun' => 'Unrealize Gain or Loss IDR',
       			'money_function' => null,
       			'parent_id' => null,
       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
       		],
       ];
       foreach ($akunParent as $value) {
       		Akun::create($value);
       }
       $akun1 = Akun::all();
       $akunSecondParent =[
       			[
	       			'kode_akun' => '6100',
	       			'nama_akun' => 'Biaya Pemasaran',
	       			'money_function' => null,
	       			'parent_id' => $akun1->where('kode_akun',6000)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
	       		],[
	       			'kode_akun' => '6300',
	       			'nama_akun' => 'Biaya Penyusutan & Amortisasi',
	       			'money_function' => null,
	       			'parent_id' => $akun1->where('kode_akun',6000)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
	       		],[
	       			'kode_akun' => '6201',
	       			'nama_akun' => 'Gaji & Tunjangan Karyawan',
	       			'money_function' => null,
	       			'parent_id' => $akun1->where('kode_akun',6200)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
	       		],[
	       			'kode_akun' => '6203',
	       			'nama_akun' => 'Beban Utiliti, Adm, Sewa & Lainya',
	       			'money_function' => null,
	       			'parent_id' => $akun1->where('kode_akun',6200)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
	       		],[
	       			'kode_akun' => '6204',
	       			'nama_akun' => 'Repair & Maintenance Expense',
	       			'money_function' => null,
	       			'parent_id' => $akun1->where('kode_akun',6200)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
	       		]
	       	];
		foreach ($akunSecondParent as $value) {
       		Akun::create($value);
       }
       $akun = Akun::all();
       $akunChild = [
       			[
	       			'kode_akun' => '1000.01',
	       			'nama_akun' => 'Kas Kecil',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1000)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Kas/Bank')->first()->id,
       			],[
	       			'kode_akun' => '1000.02',
	       			'nama_akun' => 'Kas Besar',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1000)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Kas/Bank')->first()->id,
       			],[
	       			'kode_akun' => '1000.03',
	       			'nama_akun' => 'Bank',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1000)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Kas/Bank')->first()->id,
       			],[
	       			'kode_akun' => '1000.04',
	       			'nama_akun' => 'Kas Transfer',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1000)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Kas/Bank')->first()->id,
       			],[
	       			'kode_akun' => '1001',
	       			'nama_akun' => 'Deposito',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1000)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Kas/Bank')->first()->id,
       			],[
	       			'kode_akun' => '1100.02',
	       			'nama_akun' => 'Uang muka pembelian',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1100)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Akun Piutang')->first()->id,
       			],[
	       			'kode_akun' => '110302',
	       			'nama_akun' => 'Account Receivable IDR',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Akun Piutang')->first()->id,
       			],[
	       			'kode_akun' => '110402',
	       			'nama_akun' => 'Advance Purchase IDR',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Akun Piutang')->first()->id,
       			],[
	       			'kode_akun' => '1200',
	       			'nama_akun' => 'Persediaan Barang Dagang',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Persediaan')->first()->id,
       			],[
	       			'kode_akun' => '1201',
	       			'nama_akun' => 'Barang Terkirim',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Persediaan')->first()->id,
       			],[
	       			'kode_akun' => '1300',
	       			'nama_akun' => 'Perlengkapan',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Aktiva Lancar lainnya')->first()->id,
       			],[
	       			'kode_akun' => '1400',
	       			'nama_akun' => 'Sewa Gedung Dibayar Dimuka',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Aktiva Lancar lainnya')->first()->id,
       			],[
	       			'kode_akun' => '1500',
	       			'nama_akun' => 'Asuransi Dibayar Dimuka',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Aktiva Lancar lainnya')->first()->id,
       			],[
	       			'kode_akun' => '1600',
	       			'nama_akun' => 'PPn Masukan',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Aktiva Lancar lainnya')->first()->id,
       			],[
	       			'kode_akun' => '1700.01',
	       			'nama_akun' => 'Tanah',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1700)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Aktiva Tetap')->first()->id,
       			],[
	       			'kode_akun' => '1700.02',
	       			'nama_akun' => 'Gedung',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1700)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Aktiva Tetap')->first()->id,
       			],[
	       			'kode_akun' => '1700.03',
	       			'nama_akun' => 'Kendaraan',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1700)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Aktiva Tetap')->first()->id,
       			],[
	       			'kode_akun' => '1700.04',
	       			'nama_akun' => 'Peralatan',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1700)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Aktiva Tetap')->first()->id,
       			],[
	       			'kode_akun' => '1700.05',
	       			'nama_akun' => 'Inventaris Kantor',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1700)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Aktiva Tetap')->first()->id,
       			],[
	       			'kode_akun' => '1710.01',
	       			'nama_akun' => 'Akumulasi Penyusutan Gedung',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1710)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Akumulasi Penyusutan')->first()->id,
       			],[
	       			'kode_akun' => '1710.02',
	       			'nama_akun' => 'Akumulasi Penyusutan Kendaraan',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1710)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Akumulasi Penyusutan')->first()->id,
       			],[
	       			'kode_akun' => '1710.03',
	       			'nama_akun' => 'Akumulasi Penyusutan Peralatan',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1710)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Akumulasi Penyusutan')->first()->id,
       			],[
	       			'kode_akun' => '1710.04',
	       			'nama_akun' => 'Akumulasi Penyusutan Inventaris Kantor',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',1710)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Akumulasi Penyusutan')->first()->id,
       			],[
	       			'kode_akun' => '2000.02',
	       			'nama_akun' => 'Uang Muka Penjualan',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',2000)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Akun Hutang')->first()->id,
       			],[
	       			'kode_akun' => '2100',
	       			'nama_akun' => 'PPn Keluaran',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Hutang lancar lainnya')->first()->id,
       			],[
	       			'kode_akun' => '210102',
	       			'nama_akun' => 'Account Payable IDR',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Akun Hutang')->first()->id,
       			],[
	       			'kode_akun' => '210202',
	       			'nama_akun' => 'Advance Sales IDR',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Akun Hutang')->first()->id,
       			],[
	       			'kode_akun' => '2200',
	       			'nama_akun' => 'Hutang Pembelian Belum Ditagih',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Hutang lancar lainnya')->first()->id,
       			],[
	       			'kode_akun' => '2300',
	       			'nama_akun' => 'Hutang Jangka Panjang',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Hutang lancar lainnya')->first()->id,
       			],[
	       			'kode_akun' => '3000',
	       			'nama_akun' => 'Modal',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Ekuitas')->first()->id,
       			],[
	       			'kode_akun' => '310001',
	       			'nama_akun' => 'OPENING BALANCE EQUITY',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Ekuitas')->first()->id,
       			],[
	       			'kode_akun' => '3200.02',
	       			'nama_akun' => 'Deviden',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Ekuitas')->first()->id,
       			],[
	       			'kode_akun' => '320001',
	       			'nama_akun' => 'RETAINED EARNING',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Ekuitas')->first()->id,
       			],[
	       			'kode_akun' => '4000.01',
	       			'nama_akun' => 'Penjualan',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',4000)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Pendapatan')->first()->id,
       			],[
	       			'kode_akun' => '4000.02',
	       			'nama_akun' => 'Pendapatan Jasa',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',4000)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Pendapatan')->first()->id,
       			],[
	       			'kode_akun' => '4000.03',
	       			'nama_akun' => 'Retur Penjualan',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',4000)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Pendapatan')->first()->id,
       			],[
	       			'kode_akun' => '4000.04',
	       			'nama_akun' => 'Potongan Penjualan',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',4000)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Pendapatan')->first()->id,
       			],[
	       			'kode_akun' => '410104',
	       			'nama_akun' => 'Sales Term Discount IDR',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Pendapatan')->first()->id,
       			],[
	       			'kode_akun' => '5000',
	       			'nama_akun' => 'COGS',
	       			'money_function' => null,
	       			'parent_id' => null,
	       			'tipe_akun_id' => $TipeAkun->where('title','Harga Pokok Penjualan')->first()->id,
       			],[
		       			'kode_akun' => '6101',
		       			'nama_akun' => 'Biaya Iklan',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6100)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
       				],[
		       			'kode_akun' => '6102',
		       			'nama_akun' => 'Biaya Komisi',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6100)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
       				],[
		       			'kode_akun' => '6104',
		       			'nama_akun' => 'Biaya Entertainment',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6100)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6199',
		       			'nama_akun' => 'Biaya Pemasaran Lainya',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6100)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6300.01',
		       			'nama_akun' => 'Biaya Penyusutan Gedung',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6300)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6300.02',
		       			'nama_akun' => 'Biaya Penyusutan Kendaraan',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6300)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6300.03',
		       			'nama_akun' => 'Biaya Penyusutan Peralatan',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6300)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6300.04',
		       			'nama_akun' => 'Biaya Penyusutan Inventaris kantor',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6300)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6201.01',
		       			'nama_akun' => 'Biaya Gaji, Lembur & THR',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6201)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6201.02',
		       			'nama_akun' => 'Biaya Bonus Pesangon & Kompensasi',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6201)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6201.03',
		       			'nama_akun' => 'Biaya Transport Karyawan',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6201)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6201.04',
		       			'nama_akun' => 'Biaya Upah & Honorer',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6201)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6201.05',
		       			'nama_akun' => 'Biaya Catering & Makan Karyawan',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6201)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6201.06',
		       			'nama_akun' => 'Biaya Tunjangan Kesehatan',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6201)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6201.07',
		       			'nama_akun' => 'Biaya Asuransi Karyawan',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6201)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6201.08',
		       			'nama_akun' => 'Biaya THR',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6201)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6203.01',
		       			'nama_akun' => 'Biaya Listrik',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6203)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6203.02',
		       			'nama_akun' => 'Biaya PAM',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6203)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6203.03',
		       			'nama_akun' => 'Biaya Telekomunikasi',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6203)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6203.04',
		       			'nama_akun' => 'Biaya Koran & Majalah',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6203)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6203.05',
		       			'nama_akun' => 'Biaya Ekspedisi, Pos & Materai',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6203)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6203.06',
		       			'nama_akun' => 'Biaya Perjalanan Dinas',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6203)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6203.07',
		       			'nama_akun' => 'Biaya Perlengkapan Kantor',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6203)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6203.08',
		       			'nama_akun' => 'STNK, KIR & Pajak Kendaraan',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6203)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6203.09',
		       			'nama_akun' => 'Biaya Pajak',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6203)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6203.10',
		       			'nama_akun' => 'Biaya Retribusi & Sumbangan',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6203)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6203.11',
		       			'nama_akun' => 'Biaya Sewa Gedung',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6203)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       			'kode_akun' => '6203.12',
		       			'nama_akun' => 'Biaya Umum & Adm Lainnya',
		       			'money_function' => null,
		       			'parent_id' => $akun->where('kode_akun',6203)->first()->id,
		       			'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       		],[
		       		'kode_akun' => '6204.01',
		       		'nama_akun' => 'Biaya Pemeliharaan Gedung',
		       		'money_function' => null,
		       		'parent_id' => $akun->where('kode_akun',6204)->first()->id,
		       		'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       	],[
		       		'kode_akun' => '6204.02',
		       		'nama_akun' => 'Biaya Pemeliharaan Peralatan Kantor',
		       		'money_function' => null,
		       		'parent_id' => $akun->where('kode_akun',6204)->first()->id,
		       		'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       	],[
		       		'kode_akun' => '6204.03',
		       		'nama_akun' => 'Biaya Pemeliharaan Kendaraan',
		       		'money_function' => null,
		       		'parent_id' => $akun->where('kode_akun',6204)->first()->id,
		       		'tipe_akun_id' => $TipeAkun->where('title','Beban')->first()->id,
		       	],[
	       			'kode_akun' => '7100.01',
	       			'nama_akun' => 'Pendapatan Jasa Giro',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',7100)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Pendapatan lain')->first()->id,
	       		],[
	       			'kode_akun' => '7100.02',
	       			'nama_akun' => 'Pendapatan Bunga Deposito',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',7100)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Pendapatan lain')->first()->id,
	       		],[
	       			'kode_akun' => '7100.03',
	       			'nama_akun' => 'Penjualan Inventory / Perlengkapan',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',7100)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Pendapatan lain')->first()->id,
	       		],[
	       			'kode_akun' => '7100.99',
	       			'nama_akun' => 'Pendapatan Lain-Lain',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',7100)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Pendapatan lain')->first()->id,
	       		],[
	       			'kode_akun' => '7200.01',
	       			'nama_akun' => 'Biaya Bungan Pinjaman Lainya',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',7200)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban lain-lain')->first()->id,
	       		],[
	       			'kode_akun' => '7200.02',
	       			'nama_akun' => 'Biaya Adm Bank & Buku Cek/Giro',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',7200)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban lain-lain')->first()->id,
	       		],[
	       			'kode_akun' => '7200.03',
	       			'nama_akun' => 'Pajak Jasa Giro',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',7200)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban lain-lain')->first()->id,
	       		],[
	       			'kode_akun' => '7200.99',
	       			'nama_akun' => 'Beban Lain-Lain',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',7200)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban lain-lain')->first()->id,
	       		],[
	       			'kode_akun' => '7200.01',
	       			'nama_akun' => 'Biaya Bungan Pinjaman Lainya',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',7200)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban lain-lain')->first()->id,
	       		],[
	       			'kode_akun' => '7200.02',
	       			'nama_akun' => 'Biaya Adm Bank & Buku Cek/Giro',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',7200)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban lain-lain')->first()->id,
	       		],[
	       			'kode_akun' => '7200.03',
	       			'nama_akun' => 'Pajak Jasa Giro',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',7200)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban lain-lain')->first()->id,
	       		],[
	       			'kode_akun' => '7200.99',
	       			'nama_akun' => 'Beban Lain-Lain',
	       			'money_function' => null,
	       			'parent_id' => $akun->where('kode_akun',7200)->first()->id,
	       			'tipe_akun_id' => $TipeAkun->where('title','Beban lain-lain')->first()->id,
	       		],

       ];
       if ($exists == null) {
        	foreach ($akunChild as $value) {
		       	Akun::create($value);
		    }
        }else {
        	$this->command->info('data sudah ada');
        }
    }
}
