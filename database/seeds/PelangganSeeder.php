<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\InformasiPelanggan;
use Faker\Factory;
class PelangganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = DB::table('informasi_pelanggan')->first();

        // $syarat_pembayarans = DB::table('syarat_pembayaran')
        //     ->select('id')
        //     ->get();

        // $rand_syarat_pembayaran_id = [];
        // foreach ($syarat_pembayarans as $row_syarat_pembayaran) {
        //     $rand_syarat_pembayaran_id[] = $row_syarat_pembayaran->id;
        // }


        // $mata_uangs = DB::table('mata_uang')
        //     ->select('id')
        //     ->get();

        // $rand_mata_uang_id = [];
        // foreach ($mata_uangs as $row_mata_uang) {
        //     $rand_mata_uang_id[] = $row_mata_uang->id;
        // }

        // $kode_pajaks = DB::table('kode_pajak')
        //     ->select('id')
        //     ->get();

        // $rand_kode_pajak_id = [];
        // foreach ($kode_pajaks as $row_kode_pajak) {
        //     $rand_kode_pajak_id[] = $row_kode_pajak->id;
        // }

        // $tipe_pajaks = DB::table('tipe_pajak')
        //     ->select('id')
        //     ->get();

        // $rand_tipe_pajak_id = [];
        // foreach ($tipe_pajaks as $row_tipe_pajak) {
        //     $rand_tipe_pajak_id[] = $row_tipe_pajak->id;
        // }

        // $tipe_pelanggans = DB::table('tipe_pelanggan')
        //     ->select('id')
        //     ->get();

        // $rand_tipe_pelanggan_id = [];
        // foreach ($tipe_pelanggans as $row_tipe_pelanggan) {
        //     $rand_tipe_pelanggan_id[] = $row_tipe_pelanggan->id;
        // }

        $faker = Factory::create('id_ID');
        $datas = [
   //      	[
   //      		'nama' => 'PT 123',
   //      		'no_pelanggan' => '11010',
   //      		'alamat_pajak' => $faker->address,
   //      		'alamat' => $faker->address,
   //              'kota' => 'Samarinda',
   //      		'prop' => 'Kalimantan Timur',
   //      		'kode_pos' => '75121',
   //      		'negara' => 'Indonesia',
   //      		'telepon' => $faker->phoneNumber,
   //      		'personal_kontak' => $faker->phoneNumber,
   //      		'email' => $faker->email,
   //      		'halaman_web' => 'www.pt_abc.com',
   //      		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
   //      		'mata_uang_id' => $faker->randomElement($rand_mata_uang_id),
   //      		'pajak_satu_id' => $faker->randomElement($rand_kode_pajak_id),
   //      		'pajak_dua_id' => $faker->randomElement($rand_kode_pajak_id),
   //              'pajak_faktur' => 1,
   //      		'npwp' => null,
   //      		'nppkp' => null,
   //      		'tipe_pajak_id' => $faker->randomElement($rand_tipe_pajak_id),
   //              'tipe_pelanggan_id' => $faker->randomElement($rand_tipe_pelanggan_id),
   //              'tingkatan_harga_jual' => '1',
   //              'disk_default' => null,
   //      		'catatan' => 'PT 123',
   //      	],

   //      	[
   //      		'nama' => 'PT 456',
   //      		'no_pelanggan' => '11011',
   //      		'alamat_pajak' => $faker->address,
   //      		'alamat' => $faker->address,
   //              'kota' => 'Samarinda',
   //      		'prop' => 'Kalimantan Timur',
   //      		'kode_pos' => '75122',
   //      		'negara' => 'Indonesia',
   //      		'telepon' => $faker->phoneNumber,
   //      		'personal_kontak' => $faker->phoneNumber,
   //      		'email' => $faker->email,
   //      		'halaman_web' => 'www.pt_def.com',
   //      		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
   //      		'mata_uang_id' => $faker->randomElement($rand_mata_uang_id),
   //      		'pajak_satu_id' => $faker->randomElement($rand_kode_pajak_id),
   //      		'pajak_dua_id' => $faker->randomElement($rand_kode_pajak_id),
   //              'pajak_faktur' => 1,
   //      		'npwp' => null,
   //      		'nppkp' => null,
   //      		'tipe_pajak_id' => $faker->randomElement($rand_tipe_pajak_id),
   //              'tipe_pelanggan_id' => $faker->randomElement($rand_tipe_pelanggan_id),
   //              'tingkatan_harga_jual' => '2',
   //              'disk_default' => null,
   //      		'catatan' => 'PT 456',
   //      	],

   //      	[
   //      		'nama' => 'PT 789',
   //      		'no_pelanggan' => '11012',
   //      		'alamat_pajak' => $faker->address,
   //      		'alamat' => $faker->address,
   //              'kota' => 'Samarinda',
   //      		'prop' => 'Kalimantan Timur',
   //      		'kode_pos' => '75123',
   //      		'negara' => 'Indonesia',
   //      		'telepon' => $faker->phoneNumber,
   //      		'personal_kontak' => $faker->phoneNumber,
   //      		'email' => $faker->email,
   //      		'halaman_web' => 'www.pt_ghi.com',
   //      		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
   //      		'mata_uang_id' => $faker->randomElement($rand_mata_uang_id),
   //      		'pajak_satu_id' => $faker->randomElement($rand_kode_pajak_id),
   //      		'pajak_dua_id' => $faker->randomElement($rand_kode_pajak_id),
   //              'pajak_faktur' => 1,
   //      		'npwp' => null,
   //      		'nppkp' => null,
   //      		'tipe_pajak_id' => $faker->randomElement($rand_tipe_pajak_id),
   //              'tipe_pelanggan_id' => $faker->randomElement($rand_tipe_pelanggan_id),
   //              'tingkatan_harga_jual' => '3',
   //              'disk_default' => null,
   //      		'catatan' => 'PT 789',
   //      	],

   //      	[
   //      		'nama' => 'PT 111',
   //      		'no_pelanggan' => '11013',
   //      		'alamat_pajak' => $faker->address,
   //      		'alamat' => $faker->address,
   //              'kota' => 'Samarinda',
   //      		'prop' => 'Kalimantan Timur',
   //      		'kode_pos' => '75124',
   //      		'negara' => 'Indonesia',
   //      		'telepon' => $faker->phoneNumber,
   //      		'personal_kontak' => $faker->phoneNumber,
   //      		'email' => $faker->email,
   //      		'halaman_web' => 'www.pt_jkl.com',
   //      		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
   //      		'mata_uang_id' => $faker->randomElement($rand_mata_uang_id),
   //      		'pajak_satu_id' => $faker->randomElement($rand_kode_pajak_id),
   //      		'pajak_dua_id' => $faker->randomElement($rand_kode_pajak_id),
   //              'pajak_faktur' => 1,
   //      		'npwp' => null,
   //      		'nppkp' => null,
   //      		'tipe_pajak_id' => $faker->randomElement($rand_tipe_pajak_id),
   //              'tipe_pelanggan_id' => $faker->randomElement($rand_tipe_pelanggan_id),
   //              'tingkatan_harga_jual' => '4',
   //              'disk_default' => null,
   //      		'catatan' => 'PT 111',
   //      	],

   //      	[
   //      		'nama' => 'PT 222',
   //      		'no_pelanggan' => '11014',
   //      		'alamat_pajak' => $faker->address,
   //      		'alamat' => $faker->address,
   //              'kota' => 'Samarinda',
   //      		'prop' => 'Kalimantan Timur',
   //      		'kode_pos' => '75125',
   //      		'negara' => 'Indonesia',
   //      		'telepon' => $faker->phoneNumber,
   //      		'personal_kontak' => $faker->phoneNumber,
   //      		'email' => $faker->email,
   //      		'halaman_web' => 'www.pt_mno.com',
   //      		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
   //      		'mata_uang_id' => $faker->randomElement($rand_mata_uang_id),
   //      		'pajak_satu_id' => $faker->randomElement($rand_kode_pajak_id),
   //      		'pajak_dua_id' => $faker->randomElement($rand_kode_pajak_id),
   //              'pajak_faktur' => 1,
   //      		'npwp' => null,
   //      		'nppkp' => null,
   //      		'tipe_pajak_id' => $faker->randomElement($rand_tipe_pajak_id),
   //              'tipe_pelanggan_id' => DB::table('tipe_pelanggan')->where('nama', 'Anggota')->first()->id ?? null,
   //              'tingkatan_harga_jual' => '5',
   //              'disk_default' => null,
   //      		'catatan' => 'PT 222',
			// ],
   //          [
   //              'nama' => 'Pelanggan POS 1',
   //              'no_pelanggan' => 'POS-111',
   //              'alamat_pajak' => null,
   //              'alamat' => null,
   //              'kota' => 'Samarinda',
   //              'prop' => 'Kalimantan Timur',
   //              'kode_pos' => '75125',
   //              'negara' => 'Indonesia',
   //              'telepon' => null,
   //              'personal_kontak' => null,
   //              'email' => null,
   //              'halaman_web' => null,
   //              'syarat_pembayaran_id' => null,
   //              'mata_uang_id' => null,
   //              'pajak_satu_id' => null,
   //              'pajak_dua_id' => null,
   //              'pajak_faktur' => 0,
   //              'npwp' => null,
   //              'nppkp' => null,
   //              'tipe_pajak_id' => null,
   //              'tipe_pelanggan_id' =>  DB::table('tipe_pelanggan')->where('nama', 'POS')->first()->id ?? null,
   //              'tingkatan_harga_jual' => '1',
   //              'disk_default' => null,
   //              'catatan' => 'Pelanggan POS 1',
   //          ],
   //          [
   //              'nama' => 'Pelanggan POS 2',
   //              'no_pelanggan' => 'POS-222',
   //              'alamat_pajak' => null,
   //              'alamat' => null,
   //              'kota' => 'Samarinda',
   //              'prop' => 'Kalimantan Timur',
   //              'kode_pos' => '75125',
   //              'negara' => 'Indonesia',
   //              'telepon' => null,
   //              'personal_kontak' => null,
   //              'email' => null,
   //              'halaman_web' => null,
   //              'syarat_pembayaran_id' => null,
   //              'mata_uang_id' => null,
   //              'pajak_satu_id' => null,
   //              'pajak_dua_id' => null,
   //              'pajak_faktur' => 0,
   //              'npwp' => null,
   //              'nppkp' => null,
   //              'tipe_pajak_id' => null,
   //              'tipe_pelanggan_id' =>  DB::table('tipe_pelanggan')->where('nama', 'POS')->first()->id ?? null,
   //              'tingkatan_harga_jual' => '2',
   //              'disk_default' => null,
   //              'catatan' => 'Pelanggan POS 2',
   //          ],
   //          [
   //              'nama' => 'Pelanggan POS 3',
   //              'no_pelanggan' => 'POS-333',
   //              'alamat_pajak' => null,
   //              'alamat' => null,
   //              'kota' => 'Samarinda',
   //              'prop' => 'Kalimantan Timur',
   //              'kode_pos' => '75125',
   //              'negara' => 'Indonesia',
   //              'telepon' => null,
   //              'personal_kontak' => null,
   //              'email' => null,
   //              'halaman_web' => null,
   //              'syarat_pembayaran_id' => null,
   //              'mata_uang_id' => null,
   //              'pajak_satu_id' => null,
   //              'pajak_dua_id' => null,
   //              'pajak_faktur' => 0,
   //              'npwp' => null,
   //              'nppkp' => null,
   //              'tipe_pajak_id' => null,
   //              'tipe_pelanggan_id' =>  DB::table('tipe_pelanggan')->where('nama', 'POS')->first()->id ?? null,
   //              'tingkatan_harga_jual' => '3',
   //              'disk_default' => null,
   //              'catatan' => 'Pelanggan POS 3',
   //          ],
			// [
   //      		'nama' => 'Pelanggan POS',
   //      		'no_pelanggan' => 'Pelanggan POS',
   //      		'alamat_pajak' => null,
   //      		'alamat' => null,
   //              'kota' => 'Samarinda',
   //      		'prop' => 'Kalimantan Timur',
   //      		'kode_pos' => '75125',
   //      		'negara' => 'Indonesia',
   //      		'telepon' => null,
   //      		'personal_kontak' => null,
   //      		'email' => null,
   //      		'halaman_web' => null,
   //      		'syarat_pembayaran_id' => null,
   //      		'mata_uang_id' => null,
   //      		'pajak_satu_id' => null,
   //      		'pajak_dua_id' => null,
   //              'pajak_faktur' => 0,
   //      		'npwp' => null,
   //      		'nppkp' => null,
   //      		'tipe_pajak_id' => null,
   //              'tipe_pelanggan_id' =>  DB::table('tipe_pelanggan')->where('nama', 'POS')->first()->id ?? null,
   //              'tingkatan_harga_jual' => '1',
   //              'disk_default' => null,
   //      		'catatan' => 'Pelanggan POS',
   //      	],
            [
                'nama' => 'Pelanggan Umum',
                'no_pelanggan' => 'Pelanggan Umum',
                'alamat_pajak' => null,
                'alamat' => null,
                'kota' => 'Samarinda',
                'prop' => 'Kalimantan Timur',
                'kode_pos' => '75125',
                'negara' => 'Indonesia',
                'telepon' => null,
                'personal_kontak' => null,
                'email' => null,
                'halaman_web' => null,
                'syarat_pembayaran_id' => null,
                'mata_uang_id' => null,
                'pajak_satu_id' => null,
                'pajak_dua_id' => null,
                'pajak_faktur' => 0,
                'npwp' => null,
                'nppkp' => null,
                'tipe_pajak_id' => null,
                'tipe_pelanggan_id' =>  DB::table('tipe_pelanggan')->where('nama', 'POS')->first()->id ?? null,
                'tingkatan_harga_jual' => '1',
                'disk_default' => null,
                'catatan' => 'Pelanggan Umum',
            ],
        ];

        if ($exists == null) {
        	foreach ($datas as $data) {
        		InformasiPelanggan::create($data);
        	}
        }else {
        	$this->command->info('data sudah ada');
        }
    }
}
