<?php

use App\Modules\Akuntansi\Models\TipeAktivaTetap;
use App\Modules\Akuntansi\Models\TipeAktivaTetapPajak;
use Illuminate\Database\Seeder;

class TipeAktivaTetapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = TipeAktivaTetap::first();

        $data = [
        	[
        		'tipe_aktiva_tetap'				=> 'Bangunan',
        		'tipe_aktiva_tetap_pajak_id'	=> TipeAktivaTetapPajak::where('tipe_aktiva_tetap_pajak','like','%Bangunan Permanen%')->first()->id,
        	],
        	[
        		'tipe_aktiva_tetap'				=> 'Kendaraan 1',
        		'tipe_aktiva_tetap_pajak_id'	=> TipeAktivaTetapPajak::where('tipe_aktiva_tetap_pajak','like','%Gol 1 [Saldo Menurun]%')->first()->id,	
        	],
        	[
        		'tipe_aktiva_tetap'				=> 'Kendaraan 2',
        		'tipe_aktiva_tetap_pajak_id'	=> TipeAktivaTetapPajak::where('tipe_aktiva_tetap_pajak','like','%Gol 2 [Saldo Menurun]%')->first()->id,	
        	],
        	[
        		'tipe_aktiva_tetap'				=> 'Peralatan Kantor',
        		'tipe_aktiva_tetap_pajak_id'	=> TipeAktivaTetapPajak::where('tipe_aktiva_tetap_pajak','like','%Gol 1 [Garis Lurus]%')->first()->id,	
        	],
        	[
        		'tipe_aktiva_tetap'				=> 'Peralatan Toko',
        		'tipe_aktiva_tetap_pajak_id'	=> TipeAktivaTetapPajak::where('tipe_aktiva_tetap_pajak','like','%Gol 1 [Garis Lurus]%')->first()->id,	
        	],
        	[
        		'tipe_aktiva_tetap'				=> 'Tanah',
        		'tipe_aktiva_tetap_pajak_id'	=> TipeAktivaTetapPajak::where('tipe_aktiva_tetap_pajak','like','%Tidak Disusutkan%')->first()->id,	
        	],
        ];
        if ($check === null) {
        	foreach ($data as $item) {
        		TipeAktivaTetap::create($item);
        	}
        }else {
        	$this->command->info('data sudah ada');
        }
    }
}
