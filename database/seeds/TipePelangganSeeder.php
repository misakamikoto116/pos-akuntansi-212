<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\TipePelanggan;
class TipePelangganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = DB::table('tipe_pelanggan')->first();
        $namas   = ['Tetap','Tidak Tetap','Baik','Buruk','Anggota', 'POS','Trust'];

        foreach ($namas as $nama) {
            TipePelanggan::firstOrcreate([
                'nama' => $nama
            ]);
        }
    }
}
