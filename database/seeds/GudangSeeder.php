<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Modules\Akuntansi\Models\Gudang;


class GudangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $limit = 5;
        $exists = DB::table('gudang')->first();
        $nama_gudang = ['Gudang A','Gudang B','Gudang C','Gudang D','Gudang E', 'Gudang Toko'];
        $keterangan  = ['Gudang A berisi besi','Gudang B berisi pasir','Gudang C berisi kayu','Gudang D berisi semen','Gudang E berisi besi koral', 'Gudang Toko'];

        $faker = Factory::create('id_ID');
        
        foreach ($nama_gudang as $key => $gudang) {

            $existsGudang = Gudang::where('nama', $gudang)->first();

            if ($existsGudang === null) {
                Gudang::create([
                    'nama'       			=> $gudang,
                    'keterangan'       		=> $keterangan[$key],
                    'alamat'       			=> $faker->address,
                    'penanggung_jawab'       => $faker->name,
                ]);
            }
            
        }
    }
}
