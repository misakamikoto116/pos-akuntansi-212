<?php

use Illuminate\Database\Seeder;

class DataAwalPerusahaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check      = DB::table('identitas')->first();
        $mata_uang  = DB::table('mata_uang')->first()->id; 
        if(null == $check){
            // DB::table('identitas')->insert([
            //     'nama_perusahaan' => '212 Mart Samarinda',
            //     'alamat'          => 'Jl. A. Wahab Syahranie, Gn. Kelua, Samarinda Ulu, Kota Samarinda, Kalimantan Timur 75124',
            //     // 'nama_perusahaan' => 'Chichi Jaya',
            //     // 'alamat'          => 'Samarinda Seberang',
            //     'tanggal_mulai'   => '2018-12-12',
            //     'mata_uang_id'    => $mata_uang,
            //     'footer_struk'    => 'TERIMAKASIH TELAH BERBELANJA DI 212 MART Samarinda *AMANAH*BERJAMAAH*IZZAH*',
            // ]);
            DB::table('identitas')->insert([
                'nama_perusahaan' => '212 Mart Bontang',
                'alamat'          => 'Jl. Imam Bonjol, Api-Api, Bontang Utara, Kota Bontang, Kalimantan Timur 75325',
                // 'nama_perusahaan' => 'Chichi Jaya',
                // 'alamat'          => 'Samarinda Seberang',
                'tanggal_mulai'   => '2019-05-10',
                'mata_uang_id'    => $mata_uang,
                'footer_struk'    => $this->formattingToJson(),
            ]);
        }else{
        	$this->command->info('data '.__class__.' sudah ada');
        }
    }

    public function formattingToJson()
    {
        return json_encode(['pesan' => 'TERIMAKASIH TELAH BERBELANJA', 'slogan' => '*AMANAH*BERJAMAAH*IZZAH*']);
    }
}
