<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\DetailInformasiPelanggan;
use Faker\Factory;

class DetailInformasiPelangganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = DB::table('detail_informasi_pelanggan')->first();

        $syarat_pembayarans = DB::table('syarat_pembayaran')
            ->select('id')
            ->get();

        $rand_syarat_pembayaran_id = [];
        foreach ($syarat_pembayarans as $row_syarat_pembayaran) {
            $rand_syarat_pembayaran_id[] = $row_syarat_pembayaran->id;
        }

        $informasi_pelanggans = DB::table('informasi_pelanggan')
            ->select('id')
            ->get();

        $rand_informasi_pelanggan_id = [];
        foreach ($informasi_pelanggans as $row_informasi_pelanggan) {
            $rand_informasi_pelanggan_id[] = $row_informasi_pelanggan->id;
        }
        $faker = Factory::create('id_ID');
        $datas = [
        	[
        		'no_faktur' => '001',
        		'tanggal' => $faker->dateTime(),
        		'saldo_awal' => '1000000',
                'no_pesanan' => 'P001',
        		'informasi_pelanggan_id' => $faker->randomElement($rand_informasi_pelanggan_id),
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        	],
        	[
        		'no_faktur' => '002',
        		'tanggal' => $faker->dateTime(),
        		'saldo_awal' => '2000000',
                'no_pesanan' => 'P002',
        		'informasi_pelanggan_id' => $faker->randomElement($rand_informasi_pelanggan_id),
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        	],
        	[
        		'no_faktur' => '003',
        		'tanggal' => $faker->dateTime(),
        		'saldo_awal' => '3000000',
                'no_pesanan' => 'P003',
        		'informasi_pelanggan_id' => $faker->randomElement($rand_informasi_pelanggan_id),
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        	],
        	[
        		'no_faktur' => '004',
        		'tanggal' => $faker->dateTime(),
        		'saldo_awal' => '4000000',
                'no_pesanan' => 'P004',
        		'informasi_pelanggan_id' => $faker->randomElement($rand_informasi_pelanggan_id),
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        	],
        	[
        		'no_faktur' => '005',
        		'tanggal' => $faker->dateTime(),
        		'saldo_awal' => '5000000',
                'no_pesanan' => 'P005',
        		'informasi_pelanggan_id' => $faker->randomElement($rand_informasi_pelanggan_id),
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        	],
        ];

        if (null == $exists) {
	            foreach ($datas as $data) { 
	                DetailInformasiPelanggan::create($data);
	            }
	        } else {
	            $this->command->info('data sudah ada');
	    }
    }
}
