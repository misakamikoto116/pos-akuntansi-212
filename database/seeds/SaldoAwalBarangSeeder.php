<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use Faker\Factory;
class SaldoAwalBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = DB::table('produk_detail')->first();

        $gudangs = DB::table('gudang')
            ->select('id')
            ->get();

        $rand_gudang_id = [];
        foreach ($gudangs as $row_gudang) {
            $rand_gudang_id[] = $row_gudang->id;
        }

        $produks = DB::table('produk')
            ->select('id')
            ->get();

        $rand_produk_id = [];
        foreach ($produks as $row_produk) {
            $rand_produk_id[] = $row_produk->id;
        }

        $faker = Factory::create('id_ID');
        $datas = [
        	[
        		'no_faktur' => '111',
        		'tanggal' => $faker->dateTime(),
        		'kuantitas' => '0',
        		'sn' => null,
                'harga_modal' => '0',
                'harga_terakhir' => '0',
                'gudang_id' => 6,
                'produk_id' => $rand_produk_id[1],
                'transaksi_id' => null,
            ],
            [
                'no_faktur' => '121',
                'tanggal' => $faker->dateTime(),
                'kuantitas' => '0',
                'sn' => null,
                'harga_modal' => '0',
                'harga_terakhir' => '0',
                'gudang_id' => 6,
                'produk_id' => $rand_produk_id[2],
                'transaksi_id' => null,
            ],
            [
                'no_faktur' => '131',
                'tanggal' => $faker->dateTime(),
                'kuantitas' => '0',
                'sn' => null,
                'harga_modal' => '0',
                'harga_terakhir' => '0',
                'gudang_id' => 6,
                'produk_id' => $rand_produk_id[3],
                'transaksi_id' => null,
            ],
            [
                'no_faktur' => '141',
                'tanggal' => $faker->dateTime(),
                'kuantitas' => '0',
                'sn' => null,
                'harga_modal' => '0',
                'harga_terakhir' => '0',
                'gudang_id' => 6,
                'produk_id' => $rand_produk_id[4],
                'transaksi_id' => null,
            ],
            [
                'no_faktur' => '151',
                'tanggal' => $faker->dateTime(),
                'kuantitas' => '0',
                'sn' => null,
                'harga_modal' => '0',
                'harga_terakhir' => '0',
                'gudang_id' => 6,
                'produk_id' => $rand_produk_id[5],
                'transaksi_id' => null,
        	],

            [
                'no_faktur' => '151',
                'tanggal' => $faker->dateTime(),
                'kuantitas' => '0',
                'sn' => null,
                'harga_modal' => '0',
                'harga_terakhir' => '0',
                'gudang_id' => $rand_gudang_id[5],
                'produk_id' => $rand_produk_id[6],
                'transaksi_id' => null,
            ],

            [
                'no_faktur' => '151',
                'tanggal' => $faker->dateTime(),
                'kuantitas' => '0',
                'sn' => null,
                'harga_modal' => '0',
                'harga_terakhir' => '0',
                'gudang_id' => $rand_gudang_id[5],
                'produk_id' => $rand_produk_id[7],
                'transaksi_id' => null,
            ],

            [
                'no_faktur' => '151',
                'tanggal' => $faker->dateTime(),
                'kuantitas' => '0',
                'sn' => null,
                'harga_modal' => '0',
                'harga_terakhir' => '0',
                'gudang_id' => $rand_gudang_id[5],
                'produk_id' => $rand_produk_id[8],
                'transaksi_id' => null,
            ],
        ];

        if (null == $exists) {
	            foreach ($datas as $data) { 
	                SaldoAwalBarang::create($data);
	            }
	        } else {
	            $this->command->info('data sudah ada');
	    }
    }
}
