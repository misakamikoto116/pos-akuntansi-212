<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\InformasiPemasok;
use Faker\Factory;

class PemasokSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = DB::table('informasi_pemasok')->first();

        $syarat_pembayarans = DB::table('syarat_pembayaran')
            ->select('id')
            ->get();

        $rand_syarat_pembayaran_id = [];
        foreach ($syarat_pembayarans as $row_syarat_pembayaran) {
            $rand_syarat_pembayaran_id[] = $row_syarat_pembayaran->id;
        }


        $mata_uangs = DB::table('mata_uang')
            ->select('id')
            ->get();

        $rand_mata_uang_id = [];
        foreach ($mata_uangs as $row_mata_uang) {
            $rand_mata_uang_id[] = $row_mata_uang->id;
        }

        $kode_pajaks = DB::table('kode_pajak')
            ->select('id')
            ->get();

        $rand_kode_pajak_id = [];
        foreach ($kode_pajaks as $row_kode_pajak) {
            $rand_kode_pajak_id[] = $row_kode_pajak->id;
        }

        $tipe_pajaks = DB::table('tipe_pajak')
            ->select('id')
            ->get();

        $rand_tipe_pajak_id = [];
        foreach ($tipe_pajaks as $row_tipe_pajak) {
            $rand_tipe_pajak_id[] = $row_tipe_pajak->id;
        }

        $faker = Factory::create('id_ID');
        $datas = [
        	[
        		'nama' => 'PT ABC',
        		'no_pemasok' => '11010',
        		'alamat_pajak' => $faker->address,
        		'alamat' => $faker->address,
                'kota' => 'Samarinda',
        		'prop' => 'Kalimantan Timur',
        		'kode_pos' => '75121',
        		'negara' => 'Indonesia',
        		'telepon' => $faker->phoneNumber,
        		'personal_kontak' => $faker->phoneNumber,
        		'email' => $faker->email,
        		'halaman_web' => 'www.pt_abc.com',
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        		'mata_uang_id' => $faker->randomElement($rand_mata_uang_id),
        		'keterangan' => 'PT ABC',
        		'pajak_satu_id' => $faker->randomElement($rand_kode_pajak_id),
        		'pajak_dua_id' => $faker->randomElement($rand_kode_pajak_id),
                'pajak_faktur' => 1,
        		'npwp' => null,
        		'no_pkp' => null,
        		'tipe_pajak_id' => $faker->randomElement($rand_tipe_pajak_id),
        		'catatan' => 'PT ABC',
        	],

        	[
        		'nama' => 'PT DEF',
        		'no_pemasok' => '11011',
        		'alamat_pajak' => $faker->address,
        		'alamat' => $faker->address,
                'kota' => 'Samarinda',
        		'prop' => 'Kalimantan Timur',
        		'kode_pos' => '75122',
        		'negara' => 'Indonesia',
        		'telepon' => $faker->phoneNumber,
        		'personal_kontak' => $faker->phoneNumber,
        		'email' => $faker->email,
        		'halaman_web' => 'www.pt_def.com',
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        		'mata_uang_id' => $faker->randomElement($rand_mata_uang_id),
        		'keterangan' => 'PT DEF',
        		'pajak_satu_id' => $faker->randomElement($rand_kode_pajak_id),
        		'pajak_dua_id' => $faker->randomElement($rand_kode_pajak_id),
                'pajak_faktur' => 1,
        		'npwp' => null,
        		'no_pkp' => null,
        		'tipe_pajak_id' => $faker->randomElement($rand_tipe_pajak_id),
        		'catatan' => 'PT DEF',
        	],

        	[
        		'nama' => 'PT GHI',
        		'no_pemasok' => '11012',
        		'alamat_pajak' => $faker->address,
        		'alamat' => $faker->address,
                'kota' => 'Samarinda',
        		'prop' => 'Kalimantan Timur',
        		'kode_pos' => '75123',
        		'negara' => 'Indonesia',
        		'telepon' => $faker->phoneNumber,
        		'personal_kontak' => $faker->phoneNumber,
        		'email' => $faker->email,
        		'halaman_web' => 'www.pt_ghi.com',
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        		'mata_uang_id' => $faker->randomElement($rand_mata_uang_id),
        		'keterangan' => 'PT GHI',
        		'pajak_satu_id' => $faker->randomElement($rand_kode_pajak_id),
        		'pajak_dua_id' => $faker->randomElement($rand_kode_pajak_id),
                'pajak_faktur' => 1,
        		'npwp' => null,
        		'no_pkp' => null,
        		'tipe_pajak_id' => $faker->randomElement($rand_tipe_pajak_id),
        		'catatan' => 'PT GHI',
        	],

        	[
        		'nama' => 'PT JKL',
        		'no_pemasok' => '11013',
        		'alamat_pajak' => $faker->address,
        		'alamat' => $faker->address,
                'kota' => 'Samarinda',
        		'prop' => 'Kalimantan Timur',
        		'kode_pos' => '75124',
        		'negara' => 'Indonesia',
        		'telepon' => $faker->phoneNumber,
        		'personal_kontak' => $faker->phoneNumber,
        		'email' => $faker->email,
        		'halaman_web' => 'www.pt_jkl.com',
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        		'mata_uang_id' => $faker->randomElement($rand_mata_uang_id),
        		'keterangan' => 'PT JKL',
        		'pajak_satu_id' => $faker->randomElement($rand_kode_pajak_id),
        		'pajak_dua_id' => $faker->randomElement($rand_kode_pajak_id),
                'pajak_faktur' => 1,
        		'npwp' => null,
        		'no_pkp' => null,
        		'tipe_pajak_id' => $faker->randomElement($rand_tipe_pajak_id),
        		'catatan' => 'PT JKL',
        	],

        	[
        		'nama' => 'PT MNO',
        		'no_pemasok' => '11014',
        		'alamat_pajak' => $faker->address,
        		'alamat' => $faker->address,
                'kota' => 'Samarinda',
        		'prop' => 'Kalimantan Timur',
        		'kode_pos' => '75125',
        		'negara' => 'Indonesia',
        		'telepon' => $faker->phoneNumber,
        		'personal_kontak' => $faker->phoneNumber,
        		'email' => $faker->email,
        		'halaman_web' => 'www.pt_mno.com',
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        		'mata_uang_id' => $faker->randomElement($rand_mata_uang_id),
        		'keterangan' => 'PT MNO',
        		'pajak_satu_id' => $faker->randomElement($rand_kode_pajak_id),
        		'pajak_dua_id' => $faker->randomElement($rand_kode_pajak_id),
                'pajak_faktur' => 1,
        		'npwp' => null,
        		'no_pkp' => null,
        		'tipe_pajak_id' => $faker->randomElement($rand_tipe_pajak_id),
        		'catatan' => 'PT MNO',
        	],
        ];

        if ($exists == null) {
        	foreach ($datas as $data) {
        		InformasiPemasok::create($data);
        	}
        }else {
        	$this->command->info('data sudah ada');
        }
    }
}
