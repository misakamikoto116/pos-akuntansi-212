<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\DetailInformasiPemasok;
use Faker\Factory;

class DetailInformasiPemasokSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = DB::table('detail_informasi_pemasok')->first();

        $syarat_pembayarans = DB::table('syarat_pembayaran')
            ->select('id')
            ->get();

        $rand_syarat_pembayaran_id = [];
        foreach ($syarat_pembayarans as $row_syarat_pembayaran) {
            $rand_syarat_pembayaran_id[] = $row_syarat_pembayaran->id;
        }

        $informasi_pemasoks = DB::table('informasi_pemasok')
            ->select('id')
            ->get();

        $rand_informasi_pemasok_id = [];
        foreach ($informasi_pemasoks as $row_informasi_pemasok) {
            $rand_informasi_pemasok_id[] = $row_informasi_pemasok->id;
        }
        $faker = Factory::create('id_ID');
        $datas = [
        	[
        		'no_faktur' => '006',
        		'tanggal' => $faker->dateTime(),
        		'saldo_awal' => '6000000',
                'pajak' => '600',
                'taxable' => 1,
                'in_tax' => 0,
        		'informasi_pemasok_id' => $faker->randomElement($rand_informasi_pemasok_id),
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        	],
        	[
        		'no_faktur' => '0007',
        		'tanggal' => $faker->dateTime(),
        		'saldo_awal' => '7000000',
                'pajak' => '700',
                'taxable' => 1,
                'in_tax' => 0,
        		'informasi_pemasok_id' => $faker->randomElement($rand_informasi_pemasok_id),
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        	],
        	[
        		'no_faktur' => '008',
        		'tanggal' => $faker->dateTime(),
        		'saldo_awal' => '8000000',
                'pajak' => '800',
                'taxable' => 1,
                'in_tax' => 0,
        		'informasi_pemasok_id' => $faker->randomElement($rand_informasi_pemasok_id),
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        	],
        	[
        		'no_faktur' => '009',
        		'tanggal' => $faker->dateTime(),
        		'saldo_awal' => '9000000',
                'pajak' => '900',
                'taxable' => 1,
                'in_tax' => 0,
        		'informasi_pemasok_id' => $faker->randomElement($rand_informasi_pemasok_id),
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        	],
        	[
        		'no_faktur' => '0010',
        		'tanggal' => $faker->dateTime(),
        		'saldo_awal' => '10000000',
                'pajak' => '1000',
                'taxable' => 1,
                'in_tax' => 0,
        		'informasi_pemasok_id' => $faker->randomElement($rand_informasi_pemasok_id),
        		'syarat_pembayaran_id' => $faker->randomElement($rand_syarat_pembayaran_id),
        	],
        ];

        if (null == $exists) {
	            foreach ($datas as $data) { 
	                DetailInformasiPemasok::create($data);
	            }
	        } else {
	            $this->command->info('data sudah ada');
	    }
    }
}
