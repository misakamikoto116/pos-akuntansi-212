<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\MetodePenyusutan;

class MetodePenyusutanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $check = DB::table('metode_penyusutan')->first();
       $metode_penyusutan = [
          [
            'nama' => 'Metode Garis Lurus',
            'metode' => 'Metode Garis Lurus',
          ],
          [
            'nama' => 'Metode Double Declining',
            'metode' => 'Metode Double Declining',
          ],
          [
            'nama' => 'Tidak terdepresiasi',
            'metode' => 'Tidak terdepresiasi',
          ],
          [
            'nama' => 'Metode Sum Of Year Digit',
            'metode' => 'Metode Sum Of Year Digit',
          ],
       ];
       if (null == isset($check)) {
            foreach ($metode_penyusutan as $datas) {
                MetodePenyusutan::create($datas);
              }
          } else{
              $this->command->info('sudah mempunyai data');
       }
    }
}
