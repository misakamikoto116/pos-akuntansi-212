<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\KodePajak;
use App\Modules\Pos\Models\PreferensiPos;

class PreferensiPOSSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = PreferensiPos::first();
        if (!empty($table)) {
            if (empty($table->akun_cash_id) || empty($table->diskon_penjualan_id)) {
                // new pref pos chichi baru
                // PreferensiPos::findOrFail($table->id)->update([
                //     'akun_cash_id'                          => Akun::where('kode_akun','1000.01')->pluck('id')->first(),
                //     'akun_bank_id'                          => Akun::where('kode_akun','1102-001')->pluck('id')->first(),
                //     'akun_hutang_id'                        => Akun::where('kode_akun','1100.01')->pluck('id')->first(),
                //     // 'kode_pajak_id'                         => KodePajak::where('kode','199')->pluck('id')->first(),
                //     'diskon_penjualan_id'                   => Akun::where('kode_akun','4000.04')->pluck('id')->first(),
                //     // 'status'                                => 1,
                //     'status'                                => 0,
                //     'uang_modal'                            => 1000000,
                // ]);

                // lama
                PreferensiPos::findOrFail($table->id)->update([
                        'akun_cash_id'                         => Akun::where('kode_akun','1101-001')->pluck('id')->first(),
                        'akun_bank_id'                         => Akun::where('kode_akun','1102-001')->pluck('id')->first(),
                        'akun_hutang_id'                       => Akun::where('kode_akun','1103-001')->pluck('id')->first(),
                        'kode_pajak_id'                        => KodePajak::where('kode','199')->pluck('id')->first(),
                        'diskon_penjualan_id'                  => Akun::where('kode_akun','4202')->pluck('id')->first(),
                        'status'                               => 0,
                        'uang_modal'                           => 675000,
                ]);
                
            }else {
                $this->command->info('data sydag ada');
            }
        }else if (empty($table)) {
            // new pref pos chichi baru
            // PreferensiPos::updateOrCreate([
            //     'akun_cash_id'                          => Akun::where('kode_akun','1000.01')->pluck('id')->first(),
            //     'akun_bank_id'                          => Akun::where('kode_akun','1102-001')->pluck('id')->first(),
            //     'akun_hutang_id'                        => Akun::where('kode_akun','1100.01')->pluck('id')->first(),
            //     // 'kode_pajak_id'                         => KodePajak::where('kode','199')->pluck('id')->first(),
            //     'diskon_penjualan_id'                   => Akun::where('kode_akun','4000.04')->pluck('id')->first(),
            //     // 'status'                                => 1,
            //     'status'                                => 0,
            //     'uang_modal'                            => 1000000,
            // ]);
            
            // lama
        	PreferensiPos::updateOrCreate([
             'akun_cash_id'           				=> Akun::where('kode_akun','1101-001')->pluck('id')->first(),
        		'akun_bank_id'          				=> Akun::where('kode_akun','1102-001')->pluck('id')->first(),
             'akun_hutang_id'           				=> Akun::where('kode_akun','1103-001')->pluck('id')->first(),
             'kode_pajak_id'           				=> KodePajak::where('kode','199')->pluck('id')->first(),
             'diskon_penjualan_id'     				=> Akun::where('kode_akun','4202')->pluck('id')->first(),
             'status'                 				=> 0,
             'uang_modal'               				=> 675000,
        	]);
        }else {
        	$this->command->info('data sudah ada');
        }
    }
}
