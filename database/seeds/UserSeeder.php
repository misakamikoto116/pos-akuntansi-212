<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\User;
use App\Modules\Akuntansi\Models\Role;
use App\Modules\Akuntansi\Models\Permission;

// use Faker\Factory;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // $faker = Factory::create('id_ID');
        Role::get()->each(function ($role) {
            $user = User::where('email', $role->name.'@gmail.com')->first();
            $permissionX = Permission::get();
            if ($user === null) {
                $user = User::create([
                    'name' => $role->name,
                    'email' => $role->name.'@gmail.com',
                    'password' => bcrypt('123456'),
                ]);

                $user->assignRole($role->name);
                
                if($user->hasRole('akuntansi')){
                    foreach ($permissionX as $permission) {
                        $user->givePermissionTo($permission->name);
                    }
                }
            }
        });
    }
}
