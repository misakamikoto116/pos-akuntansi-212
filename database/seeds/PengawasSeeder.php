<?php

use App\Modules\Akuntansi\Models\Role;
use App\Modules\Akuntansi\Models\User;
use Illuminate\Database\Seeder;

class PengawasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'pengawas',
            'email' => 'pengawas@gmail.com',
            'password' => bcrypt('123456'),
        ]);

        $role = Role::create(['name' => 'pengawas']);

        $user->assignRole('pengawas');
    }
}
