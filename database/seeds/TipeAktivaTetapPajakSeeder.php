<?php

use App\Modules\Akuntansi\Models\MetodePenyusutan;
use App\Modules\Akuntansi\Models\TipeAktivaTetapPajak;
use Illuminate\Database\Seeder;
class TipeAktivaTetapPajakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check 				= TipeAktivaTetapPajak::first();
        $data = [
        	[
        		'tipe_aktiva_tetap_pajak'		=> 'Bangunan Permanen',
        		'metode_penyusutan_id'			=> MetodePenyusutan::where('metode','like','%Metode Garis Lurus%')->first()->id,
        		'estimasi_umur'					=> 20,
        		'tarif_penyusutan'				=> 5,
        	],
        	[
        		'tipe_aktiva_tetap_pajak'		=> 'Gol 1 [Saldo Menurun]',
        		'metode_penyusutan_id'			=> MetodePenyusutan::where('metode','like','%Metode Double Declining%')->first()->id,
        		'estimasi_umur'					=> 4,
        		'tarif_penyusutan'				=> 25,
        	],
        	[
        		'tipe_aktiva_tetap_pajak'		=> 'Gol 2 [Saldo Menurun]',
        		'metode_penyusutan_id'			=> MetodePenyusutan::where('metode','like','%Metode Double Declining%')->first()->id,
        		'estimasi_umur'					=> 8,
        		'tarif_penyusutan'				=> 25,
        	],
        	[
        		'tipe_aktiva_tetap_pajak'		=> 'Gol 1 [Garis Lurus]',
        		'metode_penyusutan_id'			=> MetodePenyusutan::where('metode','like','%Metode Garis Lurus%')->first()->id,
        		'estimasi_umur'					=> 4,
        		'tarif_penyusutan'				=> 25,
        	],
        	[
        		'tipe_aktiva_tetap_pajak'		=> 'Tidak Disusutkan',
        		'metode_penyusutan_id'			=> MetodePenyusutan::where('metode','like','%Tidak terdepresiasi%')->first()->id,
        		'estimasi_umur'					=> 0,
        		'tarif_penyusutan'				=> 0,
        	],
        ];
        if ($check === null) {
        	foreach ($data as $item) {
        		TipeAktivaTetapPajak::create($item);
        	}
        }else {
        	$this->command->info('data sudah ada');
        }
    }
}
