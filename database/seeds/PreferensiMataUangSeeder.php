<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\PreferensiMataUang;

class PreferensiMataUangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = PreferensiMataUang::first();
        if (!empty($table)) {
            if (empty($table->akun_hutang_id) || empty($table->akun_piutang_id)) {
                    PreferensiMataUang::findOrFail($table->id)->update([
                        // akun chichi
                        // 'akun_hutang_id'                 => Akun::where('kode_akun','2000-01')->pluck('id')->first(),
                        // 'akun_piutang_id'                => Akun::where('kode_akun','1100-01')->pluck('id')->first(),
                        // 'uang_muka_pembelian_id'     => Akun::where('kode_akun','1100-05')->pluck('id')->first(),
                        // 'uang_muka_penjualan_id'     => Akun::where('kode_akun','2000-02')->pluck('id')->first(),
                        // 'diskon_penjualan_id'            => Akun::where('kode_akun','400004')->pluck('id')->first(),
                        // 'laba_rugi_terealisir_id'        => Akun::where('kode_akun','910002')->pluck('id')->first(),
                        // 'laba_rugi_tak_terealisir_id'   => Akun::where('kode_akun','910003')->pluck('id')->first(),
                        // 'akun_penyesuaian_id'           => Akun::where('kode_akun','3300')->pluck('id')->first()

                        // akun lama
                        'akun_hutang_id'                => Akun::where('kode_akun','2101-001')->pluck('id')->first(),
                        'akun_piutang_id'               => Akun::where('kode_akun','1103-001')->pluck('id')->first(),
                        'uang_muka_pembelian_id'        => Akun::where('kode_akun','1104-001')->pluck('id')->first(),
                        'uang_muka_penjualan_id'        => Akun::where('kode_akun','2102-001')->pluck('id')->first(),
                        'diskon_penjualan_id'           => Akun::where('kode_akun','4202')->pluck('id')->first(),
                        'laba_rugi_terealisir_id'       => Akun::where('kode_akun','7300-001')->pluck('id')->first(),
                        'laba_rugi_tak_terealisir_id'   => Akun::where('kode_akun','7400-001')->pluck('id')->first(),
                        'akun_penyesuaian_id'           => Akun::where('kode_akun','3300')->pluck('id')->first()

                        // akun baru bawah pos
                        // 'akun_hutang_id'                => Akun::where('kode_akun','210102')->pluck('id')->first(),
                        // 'akun_piutang_id'               => Akun::where('kode_akun','1100.01')->pluck('id')->first(),
                        // 'uang_muka_pembelian_id'        => Akun::where('kode_akun','1100.04')->pluck('id')->first(),
                        // 'uang_muka_penjualan_id'        => Akun::where('kode_akun','2000.03')->pluck('id')->first(),
                        // 'diskon_penjualan_id'           => Akun::where('kode_akun','410104')->pluck('id')->first(),
                        // 'laba_rugi_terealisir_id'       => Akun::where('kode_akun','910002')->pluck('id')->first(),
                        // 'laba_rugi_tak_terealisir_id'   => Akun::where('kode_akun','910003')->pluck('id')->first(),
                        // 'akun_penyesuaian_id'           => Akun::where('kode_akun','3300')->pluck('id')->first()
                    ]);
            }else {
                $this->command->info('data sudah ada');
            }
        }else if (empty($table)) {
        	PreferensiMataUang::create([
                // akun chichi
        		// 'akun_hutang_id' 				=> Akun::where('kode_akun','2000-01')->pluck('id')->first(),
        		// 'akun_piutang_id'				=> Akun::where('kode_akun','1100-01')->pluck('id')->first(),
        		// 'uang_muka_pembelian_id'		=> Akun::where('kode_akun','1100-05')->pluck('id')->first(),
        		// 'uang_muka_penjualan_id'		=> Akun::where('kode_akun','2000-02')->pluck('id')->first(),
        		// 'diskon_penjualan_id'			=> Akun::where('kode_akun','400004')->pluck('id')->first(),
        		// 'laba_rugi_terealisir_id'		=> Akun::where('kode_akun','910002')->pluck('id')->first(),
                // 'laba_rugi_tak_terealisir_id'   => Akun::where('kode_akun','910003')->pluck('id')->first(),
                // 'akun_penyesuaian_id'           => Akun::where('kode_akun','3300')->pluck('id')->first()

                // akun lama
                'akun_hutang_id'                => Akun::where('kode_akun','2101-001')->pluck('id')->first(),
                'akun_piutang_id'               => Akun::where('kode_akun','1103-001')->pluck('id')->first(),
                'uang_muka_pembelian_id'        => Akun::where('kode_akun','1104-001')->pluck('id')->first(),
                'uang_muka_penjualan_id'        => Akun::where('kode_akun','2102-001')->pluck('id')->first(),
                'diskon_penjualan_id'           => Akun::where('kode_akun','4202')->pluck('id')->first(),
                'laba_rugi_terealisir_id'       => Akun::where('kode_akun','7300-001')->pluck('id')->first(),
                'laba_rugi_tak_terealisir_id'   => Akun::where('kode_akun','7400-001')->pluck('id')->first(),
                'akun_penyesuaian_id'           => Akun::where('kode_akun','3300')->pluck('id')->first()

                // akun baru bawah pos
                // 'akun_hutang_id'                => Akun::where('kode_akun','210102')->pluck('id')->first(),
                // 'akun_piutang_id'               => Akun::where('kode_akun','1100.01')->pluck('id')->first(),
                // 'uang_muka_pembelian_id'        => Akun::where('kode_akun','1100.04')->pluck('id')->first(),
                // 'uang_muka_penjualan_id'        => Akun::where('kode_akun','2000.03')->pluck('id')->first(),
                // 'diskon_penjualan_id'           => Akun::where('kode_akun','410104')->pluck('id')->first(),
                // 'laba_rugi_terealisir_id'       => Akun::where('kode_akun','910002')->pluck('id')->first(),
                // 'laba_rugi_tak_terealisir_id'   => Akun::where('kode_akun','910003')->pluck('id')->first(),
                // 'akun_penyesuaian_id'           => Akun::where('kode_akun','3300')->pluck('id')->first()
        	]);
        }else {
        	$this->command->info('data sudah ada');
        }
    }
}
