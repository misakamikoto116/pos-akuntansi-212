<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\KodePajak;
use Faker\Factory;

class KodePajakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = DB::table('kode_pajak')->first();


        $akuns = DB::table('akun')
            ->select('id')
            ->get();

        $rand_akun_id = [];
        foreach ($akuns as $row) {
            $rand_akun_id[] = $row->id;
        }

		$faker = Factory::create('id_ID');
        $namas = [
        	[
	        	'nama' => 'PPh',
	        	'kode' => '100',
	        	'nilai' => '20',
	        	'keterangan' => 'Pajak Penghasilan',
	        	'akun_pajak_penjualan_id' => $akuns->where('id',33)->first()->id,
			    'akun_pajak_pembelian_id' => $akuns->where('id',61)->first()->id
        	],
        	[
        		'nama' => 'PPN',
	        	'kode' => '199',
	        	'nilai' => '10',
	        	'keterangan' => 'Pajak Pertambahan Nilai',
	        	'akun_pajak_penjualan_id' => $akuns->where('id',32)->first()->id,
			    'akun_pajak_pembelian_id' => $akuns->where('id',60)->first()->id
        	],
        	[
        		'nama' => 'PPn BM',
	        	'kode' => '200',
	        	'nilai' => '35',
	        	'keterangan' => 'Pajak Penjualan Atas Barang Mewah',
	        	'akun_pajak_penjualan_id' => $faker->randomElement($rand_akun_id),
			    'akun_pajak_pembelian_id' => $faker->randomElement($rand_akun_id)
        	],
        	[
        		'nama' => 'PBB',
	        	'kode' => '300',
	        	'nilai' => '15',
	        	'keterangan' => 'Pajak Bumi dan Bangunan',
	        	'akun_pajak_penjualan_id' => $faker->randomElement($rand_akun_id),
			    'akun_pajak_pembelian_id' => $faker->randomElement($rand_akun_id)
        	],
        	[
        		'nama' => 'BPHTB',
	        	'kode' => '310',
	        	'nilai' => '25',
	        	'keterangan' => 'Bea Perolehan Hak atas Tanah dan Bangunan',
	        	'akun_pajak_penjualan_id' => $faker->randomElement($rand_akun_id),
			    'akun_pajak_pembelian_id' => $faker->randomElement($rand_akun_id)
        	],
        ];

        if (null == $exists) {
        	foreach ($namas as $nama) {
        		KodePajak::create($nama);
        	}
        }else {
        	$this->command->info('data sudah ada');
        }

    }
}
