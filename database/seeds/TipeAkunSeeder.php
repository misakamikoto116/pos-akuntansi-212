<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\TipeAkun;
class TipeAkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $check = DB::table('tipe_akun')->first();
       $data =['Kas/Bank',
		       'Akun Piutang',
		       'Aktiva Tetap',
			   'Persediaan',
			   'Aktiva Lancar',
			   'Aktiva Lancar lainnya',
			   'Akumulasi Penyusutan',
			   'Akun Hutang',
			   'Hutang Lancar Lainnya',
			   'Hutang Jangka Panjang',
			   'Ekuitas',
			   'Pendapatan',
			   'Harga Pokok Penjualan',
			   'Beban',
			   'Pendapatan Lain',
			   'Beban lain-lain',
       ];
       if (null == isset($check)) {
            foreach($data as $datas) {
                TipeAkun::create([
                        'title'     => $datas,
                    ]);
            }
        } else {
            $this->command->info('sudah mempunyai data');
        }

    }
}
