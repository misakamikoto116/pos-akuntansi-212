<?php

use Illuminate\Database\Seeder;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\PreferensiBarang;

class PreferensiBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = PreferensiBarang::first();
        if (!empty($table)) {
            if (empty($table->akun_persediaan_id) || empty($table->akun_penjualan_id)) {
                // new sesuaikan dari akun chichi bawah
                // PreferensiBarang::findOrFail($table->id)->update([
                //     'akun_persediaan_id'                => Akun::where('kode_akun','1200.01')->pluck('id')->first(),
                //     'akun_penjualan_id'                 => Akun::where('kode_akun','4000.01')->pluck('id')->first(),
                //     'akun_retur_penjualan_id'           => Akun::where('kode_akun','4000.03')->pluck('id')->first(),
                //     'akun_diskon_barang_id'             => Akun::where('kode_akun','4000.04')->pluck('id')->first(),
                //     'akun_barang_terkirim_id'           => Akun::where('kode_akun','1201')->pluck('id')->first(),
                //     'akun_hpp_id'                       => Akun::where('kode_akun','5000')->pluck('id')->first(),
                //     'akun_retur_pembelian_id'           => Akun::where('kode_akun','1200.01')->pluck('id')->first(),
                //     'akun_beban_id'                     => Akun::where('kode_akun','6203.12')->pluck('id')->first(),
                //     'akun_belum_tertagih_id'            => Akun::where('kode_akun','2200')->pluck('id')->first(),
                //     'estimasi_kada_luarsa'              => 10,
                // ]);

                // lama sesuai dengan kaun dummy
                PreferensiBarang::findOrFail($table->id)->update([
                    'akun_persediaan_id'                => Akun::where('kode_akun','1105-001')->pluck('id')->first(),
                    'akun_penjualan_id'                 => Akun::where('kode_akun','4101-001')->pluck('id')->first(),
                    'akun_retur_penjualan_id'           => Akun::where('kode_akun','4201-001')->pluck('id')->first(),
                    'akun_diskon_barang_id'             => Akun::where('kode_akun','4202')->pluck('id')->first(),
                    'akun_barang_terkirim_id'           => Akun::where('kode_akun','1105-999')->pluck('id')->first(),
                    'akun_hpp_id'                       => Akun::where('kode_akun','5100-001')->pluck('id')->first(),
                    'akun_retur_pembelian_id'           => Akun::where('kode_akun','1105-001')->pluck('id')->first(),
                    'akun_beban_id'                     => Akun::where('kode_akun','7200-004')->pluck('id')->first(),
                    'akun_belum_tertagih_id'            => Akun::where('kode_akun','2106')->pluck('id')->first(),
                    'estimasi_kada_luarsa'              => 10,
                ]);
                
            }else {
                $this->command->info('data sudah ada');
            }
        }else if (empty($table)) {

                // new sesuaikan dari akun chichi bawah
                // PreferensiBarang::updateOrCreate([
                //     'akun_persediaan_id'                => Akun::where('kode_akun','1200.01')->pluck('id')->first(),
                //     'akun_penjualan_id'                 => Akun::where('kode_akun','4000.01')->pluck('id')->first(),
                //     'akun_retur_penjualan_id'           => Akun::where('kode_akun','4000.03')->pluck('id')->first(),
                //     'akun_diskon_barang_id'             => Akun::where('kode_akun','4000.04')->pluck('id')->first(),
                //     'akun_barang_terkirim_id'           => Akun::where('kode_akun','1201')->pluck('id')->first(),
                //     'akun_hpp_id'                       => Akun::where('kode_akun','5000')->pluck('id')->first(),
                //     'akun_retur_pembelian_id'           => Akun::where('kode_akun','1200.01')->pluck('id')->first(),
                //     'akun_beban_id'                     => Akun::where('kode_akun','6203.12')->pluck('id')->first(),
                //     'akun_belum_tertagih_id'            => Akun::where('kode_akun','2200')->pluck('id')->first(),
                //     'estimasi_kada_luarsa'              => 10,
                // ]);

            // new sesuaikan dari akun chichi
        	// PreferensiBarang::create([
        	// 	'akun_persediaan_id'                => Akun::where('kode_akun','1200-10')->pluck('id')->first(),
            //  'akun_penjualan_id'                 => Akun::where('kode_akun','400007')->pluck('id')->first(),
            //  'akun_retur_penjualan_id'           => Akun::where('kode_akun','400008')->pluck('id')->first(),
            //  'akun_diskon_barang_id'             => Akun::where('kode_akun','400004')->pluck('id')->first(),
            //  'akun_barang_terkirim_id'           => Akun::where('kode_akun','1200-09')->pluck('id')->first(),
            //  'akun_hpp_id'                       => Akun::where('kode_akun','5000')->pluck('id')->first(),
            //  'akun_retur_pembelian_id'           => Akun::where('kode_akun','1200-10')->pluck('id')->first(),
            //  'akun_beban_id'                     => Akun::where('kode_akun','620320')->pluck('id')->first(),
            //  'akun_belum_tertagih_id'            => Akun::where('kode_akun','2200')->pluck('id')->first(),
            //  'estimasi_kada_luarsa'              => 10,
        	// ]);

            // lama sesuai dengan kaun dummy
            PreferensiBarang::updateOrCreate([
                'akun_persediaan_id'                => Akun::where('kode_akun','1105-001')->pluck('id')->first(),
                'akun_penjualan_id'                 => Akun::where('kode_akun','4101-001')->pluck('id')->first(),
                'akun_retur_penjualan_id'           => Akun::where('kode_akun','4201-001')->pluck('id')->first(),
                'akun_diskon_barang_id'             => Akun::where('kode_akun','4202')->pluck('id')->first(),
                'akun_barang_terkirim_id'           => Akun::where('kode_akun','1105-999')->pluck('id')->first(),
                'akun_hpp_id'                       => Akun::where('kode_akun','5100-001')->pluck('id')->first(),
                'akun_retur_pembelian_id'           => Akun::where('kode_akun','1105-001')->pluck('id')->first(),
                'akun_beban_id'                     => Akun::where('kode_akun','7200-004')->pluck('id')->first(),
                'akun_belum_tertagih_id'            => Akun::where('kode_akun','2106')->pluck('id')->first(),
                'estimasi_kada_luarsa'              => 10,
            ]);

        }else {
        	$this->command->info('data sudah ada');
        }
    }
}
