<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {


            // BARU
            // // $this->call(UsersTableSeeder::class);
            // $this->call(PermissionSeeder::class);
            // $this->call(RoleSeeder::class);
            // $this->call(UserSeeder::class);
            // $this->call(TipeAkunSeeder::class);
            // $this->call(MataUangSeeder::class);
            // $this->call(MetodePenyusutanSeeder::class);
            // $this->call(OpeningBalanceSeeder::class);
            // // $this->call(NewAkunSeeder::class);
            // // $this->call(DataAwalPerusahaanSeeder::class);
            // // $this->call(AkunSeeder::class);

            // $this->call(SyaratPembayaranSeeder::class);
            // // $this->call(KodePajakSeeder::class);
            // $this->call(TipePelangganSeeder::class);
            // $this->call(TipePajakSeeder::class);
            // $this->call(KategoriProdukSeeder::class);
            // $this->call(GudangSeeder::class);
            // $this->call(PemasokSeeder::class);
            // $this->call(PelangganSeeder::class);
            // // $this->call(ProdukSeeder::class);
            // $this->call(JasaPengirimanSeeder::class);

            // // $this->call(DetailInformasiPelangganSeeder::class);
            // // $this->call(DetailInformasiPemasokSeeder::class);
            // // $this->call(SaldoAwalBarangSeeder::class);
            // $this->call(PreferensiMataUangSeeder::class);
            // $this->call(PreferensiBarangSeeder::class);
            // $this->call(TipeAktivaTetapPajakSeeder::class);
            // $this->call(TipeAktivaTetapSeeder::class);
            // // $this->call(PromosiSeeder::class);  
            // $this->call(PreferensiPOSSeeder::class);
            // $this->call(TingkatHargaBarangSeeder::class);
            // // Pos
            // $this->call(KasirMesinSeeder::class);
            // //Kang Mulia
            // // $this->call(ApiUserSeeder::class);
            ////////////////////////////////////////////////////////// 


            // // LAMA
            // $this->call(UsersTableSeeder::class);
            $this->call(PermissionSeeder::class);
            $this->call(RoleSeeder::class);
            $this->call(UserSeeder::class);
            $this->call(TipeAkunSeeder::class);
            $this->call(MataUangSeeder::class);
            $this->call(MetodePenyusutanSeeder::class);
            $this->call(NewAkunSeeder::class);
            $this->call(DataAwalPerusahaanSeeder::class);
            // $this->call(AkunSeeder::class);

            $this->call(SyaratPembayaranSeeder::class);
            $this->call(KodePajakSeeder::class);
            $this->call(TipePelangganSeeder::class);
            $this->call(TipePajakSeeder::class);
            $this->call(KategoriProdukSeeder::class);
            $this->call(GudangSeeder::class);
            $this->call(PemasokSeeder::class);
            $this->call(PelangganSeeder::class);
            // $this->call(ProdukSeeder::class);
            $this->call(JasaPengirimanSeeder::class);

            // $this->call(DetailInformasiPelangganSeeder::class);
            // $this->call(DetailInformasiPemasokSeeder::class);
            // $this->call(SaldoAwalBarangSeeder::class);
            $this->call(PreferensiMataUangSeeder::class);
            $this->call(PreferensiBarangSeeder::class);
            $this->call(TipeAktivaTetapPajakSeeder::class);
            $this->call(TipeAktivaTetapSeeder::class);
            // $this->call(PromosiSeeder::class);  
            $this->call(PreferensiPOSSeeder::class);
            // $this->call(TingkatHargaBarangSeeder::class);
            // Pos
            $this->call(KasirMesinSeeder::class);
            //Kang Mulia
            // $this->call(ApiUserSeeder::class);
            // /////////////////////////////////////////////////////////////////
            $this->call(PengawasSeeder::class);
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
