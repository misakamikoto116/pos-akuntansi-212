<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords setidaknya memiliki 6 karakter atau lebih.',
    'reset' => 'Password Anda telah direset!',
    'sent' => 'Kami telah mengirimkan email untuk link password Anda!',
    'token' => 'Token reset password ini tidak valid.',
    'user' => "Alamat email tidak ditemukan.",

];
