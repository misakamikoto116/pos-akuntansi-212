<div class="row" style="padding: 0px;">
	<div class="col-md-12" style="padding: 0px;">
		<div class="card-body">
			@if ($barang_expired === null)
				<div class="alert alert-warning"> Tidak Ada Data.</div>
			@else
				<table class="table">
					<tr>
						<th>No Barang</th>
						<th>Nama Barang</th>
						<th>Satuan</th>
						<th>Tanggal Kada Luarsa</th>
						<th>Estimasi Kada Luarsa</th>
					</tr>
					@foreach ($barang_expired->chunk(10) as $barang)
						@foreach ($barang as $item)
							<tr>
								<td>{{ $item['no_barang'] }}</td>
								<td>{{ $item['nama_barang'] }}</td>
								<td>{{ $item['satuan'] }}</td>
								<td>{{ $item['expired_date'] }}</td>
								<td>{{ $item['estimasi'] }}</td>
							</tr>
						@endforeach
					@endforeach
					@if (count($barang_expired) > 10)
						<td colspan="5" align = 'center'>
							<a href="{{ url('akuntansi/kada-luarsa') }}" target="_blank" class="btn btn-info" style="width:100%"><i class="fa fa-info-circle"> Barang Lainnya</i></a>
						</td>
					@endif
				</table>
			@endif
			{{-- @if ($barang_expired !== null)
				<div class="pull-right">
					{!! $barang_expired->links('vendor.pagination.bootstrap-4'); !!}
				</div>
			@endif --}}
		</div>
	</div>
</div>