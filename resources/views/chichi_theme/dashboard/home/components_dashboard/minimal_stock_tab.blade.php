<div class="row" style="padding: 0px;">
	<div class="col-md-12" style="padding: 0px;">
		<div class="card-body">

			@if (empty($qty_minimal_stock))
				<div class="alert alert-warning"> Tidak Ada Data.</div>
			@else
				<table class='table'>
					<tr>
						<th>No Barang</th>
						<th>Nama Barang</th>
						<th>Satuan</th>
						<th>Minimal Jumlah Stock</th>
						<th>Kuantitas Sekarang</th>
					</tr>
					@foreach ($qty_minimal_stock->chunk(10) as $stock)
						@if ($stock !== null)
							@foreach ($stock as $barang)
								<tr>
									<td>{{ $barang['no_barang'] }}</td>
									<td>{{ $barang['nama_barang'] }}</td>
									<td>{{ $barang['satuan'] }}</td>
									<td>{{ $barang['minimal_stock'] }}</td>
									<td>{{ $barang['qty_sekarang'] }}</td>
								</tr>
							@endforeach
						@endif
					@endforeach
					@if (count($qty_minimal_stock) > 10)
						<td colspan = {{ count($barang) + 2 }} align = 'center'>
							<a href="{{ url('akuntansi/minimal-stock') }}" target="_blank" class="btn btn-info" style="width:100%"><i class="fa fa-info-circle"> Barang Lainnya</i></a>
						</td>
					@endif
				</table>
			@endif
			{{-- <div class="pull-right">
                {!! $qty_minimal_stock->links('vendor.pagination.bootstrap-4'); !!}
            </div> --}}
		</div>
	</div>
</div>