<div class="row" style="padding: 0px;">
	<div class="col-md-12" style="padding: 0px;">
		<div class="card-body">
			@if ($harga_jual_barang == null)
				<div class="alert alert-warning"> Tidak Ada Data.</div>
			@else
				<table class="table">
					<tr>
						<th>No Barang</th>
						<th>Nama Barang</th>
						<th>Kuantitas</th>
						<th>Satuan</th>
						<th>Harga Jual</th>
					</tr>
					@foreach ($harga_jual_barang->chunk(10) as $barang)
						@foreach ($barang as $item)
							<tr>
								<td>{{ $item->no_barang }}</td>
								<td>{{ $item->keterangan }}</td>
								<td>{!! $item->updated_qty !!}</td>
								<td>{{ $item->unit }}</td>
								<td>{{ $item->harga_jual }}</td>
							</tr>
						@endforeach
					@endforeach
					@if (count($harga_jual_barang) > 10)
						<td colspan="5" align = 'center'>
							<a href="{{ url('akuntansi/harga-jual-nol') }}" target="_blank" class="btn btn-info" style="width:100%"><i class="fa fa-info-circle"> Barang Lainnya</i></a>
						</td>
					@endif
				</table>
			@endif
				{{-- @if ($harga_jual_barang !== null)
			<div class="pull-right">
                {!! $harga_jual_barang->links('vendor.pagination.bootstrap-4'); !!}
            </div>
					@endif --}}
		</div>
	</div>
</div>