<div class="row" style="padding: 0px;">
	<div class="col-md-12" style="padding: 0px;">
		<div class="card-body">
			@if (empty($harga_modal))
				<div class="alert alert-warning"> Tidak Ada Data.</div>
			@else
				<table class="table">
					<tr>
						<th>No Barang</th>
						<th>Nama Barang</th>
						<th>Satuan</th>
						<th>Harga Jual Sekarang</th>
						<th>Harga Modal Barang</th>
					</tr>
					@foreach ($harga_modal->chunk(10) as $modal)
						@foreach ($modal as $item)
							<tr>
								<td>{{ $item['no_barang'] }}</td>
								<td>{{ $item['nama_barang'] }}</td>
								<td>{{ $item['satuan'] }}</td>
								<td>{{ number_format($item['harga_jual_sekarang']) }}</td>
								<td>{{ number_format($item['harga_modal_terakhir']) }}</td>
							</tr>
						@endforeach
					@endforeach
					@if (count($harga_modal) > 10)
						<td colspan="5" align = 'center'>
							<a href="{{ url('akuntansi/harga-modal') }}" target="_blank" class="btn btn-info" style="width:100%"><i class="fa fa-info-circle"> Barang Lainnya</i></a>
						</td>
					@endif
				</table>
			@endif
			{{-- <div class="pull-right">
                {!! $harga_modal->links('vendor.pagination.bootstrap-4'); !!}
            </div> --}}
		</div>
	</div>
</div>