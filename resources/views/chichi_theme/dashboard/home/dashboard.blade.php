@extends('chichi_theme.layout.app')

@section('custom_css')
	<!--Morris Chart CSS -->
	<link rel="stylesheet" href="{{ asset('assets/plugins/morris/morris.css') }}">
	<style type="text/css">
		big{
			font-weight: bold;
		}
		.nav .nav-pills{
			border: 1px solid rgba(97,135,136,0.3);
		}
		.tab-content{
			box-shadow: none;
		}
		.tab-content>.active{
			padding: 10px;
			padding-top: 30px;
			margin-bottom: 20px;
		}
		.nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active{
			position: relative;
			top: 1px;
			border-top: 1px solid rgba(97,135,136,0.3);
			border-left: 1px solid rgba(97,135,136,0.3);
			border-right: 1px solid rgba(97,135,136,0.3);
			background: white;
			border-radius: 0px;
			color: #27393d;
		}
		.nav-pills .nav-link{
			position: relative;
			top: 1px;
			padding: 10px 30px;
		}
		.nav-pills li a{
			font-weight: bold;
			color: #618788;
			letter-spacing: 0.5px;
		}

		.form-check-input{
			position: relative;
			margin-left: 0px;
			margin-top: 0px;
			margin-right: 1rem;
		}
		.disabled-select {
			background-color:#d5d5d5;
			opacity:0.5;
			border-radius:3px;
			cursor:not-allowed;
			position:absolute;
			top:0;
			bottom:0;
			right:0;
			left:0;
		}
		.morris-hover-point{
			-webkit-text-fill-color: white;
			-webkit-text-stroke-width: 0.8px;;
			-webkit-text-stroke-color: black;
		}

		.morris-hover-row-label{
			color: #27393d !important;
		}
	</style>
@endsection

@section('content')
	<!-- Page-Title -->
	<div class="row">
		<div class="col-sm-12">
			<!-- Judul Halaman -->
			<h4 class="page-title">Dashboard</h4>

			<!-- Judul Halaman -->
			<ol id="breadcrumb">
				<li><a href="#"><span class="icon icon-home"> </span></a></li>
				<li><a href="#">Dashboard</a></li>
			</ol>

		</div>
	</div>
	<!-- END Page-Title -->

	<div class="row">
		<div class="col-md-6 col-lg-6 col-xl-4">
			<div class="widget-bg-color-icon card-box fadeInDown animated">
				<div class="bg-icon bg-icon-info pull-left">
					<i class="md md-attach-money text-info"></i>
				</div>
				<div class="text-right">
					<h3 class="text-dark"><b class="counter">{{ $total_penjualan }}</b></h3>
					<p class="text-muted mb-0">Total Penjualan</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>

		<div class="col-md-6 col-lg-6 col-xl-4">
			<div class="widget-bg-color-icon card-box">
				<div class="bg-icon bg-icon-pink pull-left">
					<i class="md md-add-shopping-cart text-pink"> </i>
				</div>
				<div class="text-right">
					<h3 class="text-dark"><b class="counter">{{ $total_transaksi }}</b></h3>
					<p class="text-muted mb-0">Jumlah Transaksi</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>



		<div class="col-md-6 col-lg-6 col-xl-4">
			<div class="widget-bg-color-icon card-box">
				<div class="bg-icon bg-icon-success pull-left">
					<i class="md md-remove-red-eye text-success"></i>
				</div>
				<div class="text-right">
					<h3 class="text-dark"><b class="counter">{{ $laba_rugi }}</b></h3>
					<p class="text-muted mb-0">Laba Rugi</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	{{-- Notifikasi --}}
	<div class="row">
		<div class="col-lg-12 col-xl-12">
			<div class="portlet">
				<!-- /primary heading -->
				<div class="portlet-heading">
					<h3 class="portlet-title text-dark"> Notifikasi Barang </h3>
					<div class="clearfix"></div>
				</div>
				<div id="bg-default1" class="panel-collapse collapse show">
					<div class="portlet-body">

						{{-- Nav --}}
						<ul class="nav nav-pills" id="pills-tab" role="tablist" style="border-bottom: 1px solid rgba(97,135,136,0.3);">
							<li class="nav-item">
								<a class="nav-link" style="font-size: 12px!important; color:black !important" id="pills-harga-jual-tab" data-toggle="pill" href="#pills-harga-jual" role="tab" aria-controls="pills-harga-jual" aria-selected="true">
									Barang dengan harga jual Nol <span class="badge badge-{{ $badge_harga_jual }} noti-icon-badge" style="margin-bottom: 30px;">{{ $harga_jual_barang == null ? 0 : $harga_jual_barang->total() }}</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" style="font-size: 12px!important; color:black !important" id="pills-minimal-stock-tab" data-toggle="pill" href="#pills-minimal-stock" role="tab" aria-controls="pills-minimal-stock" aria-selected="true">
									Qty barang kurang dari minimal stock <span class="badge badge-{{ $badge_minimal_stock }} noti-icon-badge" style="margin-bottom: 30px;">{{ $qty_minimal_stock == null ? 0 : $qty_minimal_stock->total() }}</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" style="font-size: 12px!important; color:black !important" id="pills-harga-modal-tab" data-toggle="pill" href="#pills-harga-modal" role="tab" aria-controls="pills-harga-modal" aria-selected="true">
									Harga jual barang kurang dari harga modal <span class="badge badge-{{ $badge_harga_modal }} noti-icon-badge" style="margin-bottom: 30px;">{{ $harga_modal == null ? 0 : $harga_modal->total() }}</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" style="font-size: 12px!important; color:black !important" id="pills-barang-expired-tab" data-toggle="pill" href="#pills-barang-expired" role="tab" aria-controls="pills-barang-expired" aria-selected="true">
									Barang Kada Luarsa <span class="badge badge-{{ $badge_barang_expired }} noti-icon-badge" style="margin-bottom: 30px;">{{ $barang_expired == null ? 0 : $barang_expired->total() }}</span>
								</a>
							</li>
						</ul>

						{{-- Content --}}
						<div class="tab-content" id="pills-tabContent">
							<div class="tab-pane fade show" id="pills-harga-jual">
								@include('chichi_theme.dashboard.home.components_dashboard.harga_jual_tab')
							</div>
							<div class="tab-pane fade show" id="pills-minimal-stock" role="tabpanel" aria-labelledby="pills-minimal-stock-tab">
								@include('chichi_theme.dashboard.home.components_dashboard.minimal_stock_tab')
							</div>
							<div class="tab-pane fade show" id="pills-harga-modal" role="tabpanel" aria-labelledby="pills-harga-modal-tab">
								@include('chichi_theme.dashboard.home.components_dashboard.harga_modal_tab')
							</div>
							<div class="tab-pane fade show" id="pills-barang-expired" role="tabpanel" aria-labelledby="pills-barang-expired-tab">
								@include('chichi_theme.dashboard.home.components_dashboard.barang_expired')
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 col-xl-6">
			<div class="portlet">
				<!-- /primary heading -->
				<div class="portlet-heading">
					<h3 class="portlet-title text-dark"> Grafik Penjualan </h3>
					<div class="text-center">
						<div class="clearfix"></div>
						<ul class="list-inline chart-detail-list">
							<li class="list-inline-item">
								<h5><i class="fa fa-circle m-r-5" style="color: #5fbeaa;">
									</i>Penjualan</h5>
							</li>
						</ul>
					</div>
				</div>
				<div id="bg-default1" class="panel-collapse collapse show">
					<div class="portlet-body">
						<div id="bar-penjualan" style="color: #0000cc;height: 400px;width: 550px"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12 col-xl-6">
			<div class="portlet">
				<!-- /primary heading -->
				<div class="portlet-heading">
					<h3 class="portlet-title text-dark"> Grafik Laba Rugi </h3>
					<div class="text-center">
						<div class="clearfix"></div>
						<ul class="list-inline chart-detail-list">
							<li class="list-inline-item">
								<h5><i class="fa fa-circle m-r-5" style="color: #5d9cec;">
									</i>Modal</h5>
							</li>

							<li class="list-inline-item">
								<h5><i class="fa fa-circle m-r-5" style="color: #F28975;">
									</i>Laba Rugi</h5>
							</li>

							<li class="list-inline-item">
								<h5><i class="fa fa-circle m-r-5" style="color: #D3F259;">
									</i>Penjualan</h5>
							</li>
						</ul>
					</div>
				</div>
				<div id="bg-default1" class="panel-collapse collapse show">
					<div class="portlet-body" style="overflow:auto;">
						<div id="bar-laba-rugi" style="color: #0000cc;height: 400px;width: 1040px"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-xl-6">
			<div class="portlet">
				<!-- /primary heading -->
				<div class="portlet-heading">
					<h3 class="portlet-title text-dark"> Grafik Barang dibeli Favorit </h3>
					<div class="text-center">
						<div class="clearfix"></div>
						<ul class="list-inline chart-detail-list">
							@if($grafik_pie->isEmpty())
								<li class="list-inline-item">
									<h5><i class="fa fa-circle m-r-5" style="color: #5fbeaa;">
										</i>{{ "No Data"  }}</h5>
								</li>

							@else
								@foreach($grafik_pie as $item)
									@php
										$color = "";
                                        switch ($loop->iteration){
                                            case 1 : $color = "color: #5fbeaa;";
                                            break;
                                            case 2 : $color = "color: #5d9cec;";
                                            break;
                                            default:
                                                 $color = "color: #dcdcdc;";
                                        }
									@endphp
									<li class="list-inline-item">
										<h5><i class="fa fa-circle m-r-5" style="{{ $color }}">
											</i>{{ $item->produk->keterangan }}</h5>
									</li>

								@endforeach
							@endif
						</ul>
					</div>

				</div>
				<div id="bg-default1" class="panel-collapse collapse show">
					<div class="portlet-body">
						<div id="grafik-barang-favorit" style="color: #0000cc;height: 400px;width: 550px"></div>
					</div>
				</div>
			</div>
		</div>
		@endsection
		@section('custom_js')

			<script src="{{ asset('assets/plugins/morris/morris.min.js') }}"></script>
			<script src="{{ asset('assets/plugins/raphael/raphael-min.js') }}"></script>

			<script>
                $( document ).ready(function() {
                    function barChart(element, data, xkey, ykeys, labels, lineColors) {
                        return new Morris.Bar({
                            element: element,
                            data: data,
                            xkey: xkey,
                            ykeys: ykeys,
                            labels: labels,
                            hideHover: 'auto',
                            resize: true, //defaulted to true
                            gridLineColor: '#eeeeee',
                            barColors: lineColors
                        });
                    }
                    //creates Stacked chart
                    function createStackedChart(element, data, xkey, ykeys, labels, lineColors) {
                        return new Morris.Bar({
                            element: element,
                            data: data,
                            xkey: xkey,
                            ykeys: ykeys,
                            stacked: true,
                            labels: labels,
                            hideHover: 'auto',
                            resize: true, //defaulted to true
                            gridLineColor: '#eeeeee',
                            barColors: lineColors
                        });
                    };
                    //creating bar chart
                    var $barData  = [
                        <?php
                        //TODO nanti diLoad pakai fetch skarang nampil dulu
                        foreach($grafik_penjualan as $item){
                            echo "\t\t"."{ y:". '"' . $item['bulan_text'] .'", penjualan: "'.$item['total_penjualan'].'" },'."\n";
                        }
                        ?>
                    ];

                    barChart("bar-penjualan", $barData, 'y', ['penjualan'], ['Penjualan'], ['#5fbeaa'])
                    //creating Stacked chart
                    var $stckedData  = [
                        <?php
                        //TODO nanti diLoad pakai fetch skarang nampil dulu
                        if($grafik_penjualan->isEmpty()){
                            echo '{y: "No Data", modal: 0, laba_rugi: 0, penjualan: 0},';
                        }else {
							for ($index = 0; $index < 12; $index++) {
								$a = $grafik_penjualan->get($index);
								if ( $a != null ) {
								    $data_penjualan = $grafik_penjualan[$index];
									echo "{ y:". '"' . $data_penjualan['bulan_text'] .'", modal: "'. $data_penjualan['modal'] .'", laba_rugi: '. $data_penjualan['laba_rugi'] .', penjualan: '. $data_penjualan['total_penjualan'] .'  },'."";
								}elseif ( $a == null ){
									switch ( $index ) {
										case '0':
											$title = 'Jan';
										break;
										case '1':
											$title = 'Feb';
										break;
										case '2':
											$title = 'Mar';
										break;
										case '3':
											$title = 'Apr';
										break;
										case '4':
											$title = 'Mei';
										break;
										case '5':
											$title = 'Jun';
										break;
										case '6':
											$title = 'Jul';
										break;
										case '7':
											$title = 'Agu';
										break;
										case '8':
											$title = 'Sept';
										break;
										case '9':
											$title = 'Okt';
										break;
										case '10':
											$title = 'Nop';
										break;
										case '11':
											$title = 'Des';
										break;
										default:
											$title = 'No Data';
										break;
									}
									echo "{ y:". '"' . $title .'", modal: 0, laba_rugi: 0, penjualan: 0 },'."";
								}
							}
							// dd('di luar for');
                            // foreach($grafik_penjualan as $item){
							// 	dd($grafik_penjualan);
                            //     echo "\t\t"."{ y:". '"' . $item['bulan_text'] .'", modal: "'.$item['modal'].'", laba_rugi: '.$item['laba_rugi'].', penjualan: '.$item['total_penjualan'].'  },'."\n";
                            // }
                        }
                        ?>
                    ];
                    createStackedChart('bar-laba-rugi', $stckedData, 'y', ['modal', 'laba_rugi', 'penjualan'], ['Modal', 'Laba Rugi', 'Penjualan'], ['#5d9cec', '#F28975', '#D3F259']);
                    //creates Donut chart
//creates Donut chart
                    function createDonutChart(element, data, colors) {
                        return Morris.Donut({
                            element: element,
                            data: data,
                            resize: true, //defaulted to true
                            colors: colors
                        });
                    }
                    var $donutData = [
                        <?php

                        //TODO nanti diLoad pakai fetch skarang nampil dulu
                        if($grafik_pie->isEmpty()){
                            echo '{label: "No Data", value: 0},';
                        }else {
                            foreach($grafik_pie as $item){
                                echo "\t\t"."{ label:". '"' . $item->produk->keterangan_with_out_string .'", value: '.$item['jml_penjualan'].' },'."\n";
                            }
                        }
                        ?>
                    ];
                    createDonutChart('grafik-barang-favorit', $donutData, ['#5fbeaa', '#5d9cec', '#dcdcdc']);
                });


			</script>

@endsection