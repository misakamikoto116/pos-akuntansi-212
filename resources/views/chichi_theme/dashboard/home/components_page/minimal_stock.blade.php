@extends('chichi_theme.layout.app')

@section('custom_css')
@endsection

@section('content')
    <!-- Page-Title -->
    <div class="row">
		<div class="col-sm-12">
			<!-- Judul Halaman -->
			<h4 class="page-title">Barang Kurang dari Minimal Stok</h4>

			<!-- Judul Halaman -->
			<ol id="breadcrumb">
				<li><a href="#"><span class="icon icon-home"> </span></a></li>
				<li><a href="#">Barang Kurang dari Minimal Stok</a></li>
			</ol>

		</div>
	</div>
	<!-- END Page-Title -->
    <div class="row" style="padding: 0px;">
        <div class="col-md-12" style="padding: 0px;">
            <div class="card-body">
    
                @if ($item->isEmpty())
                    <div class="alert alert-warning"> Tidak Ada Data.</div>
                @else
                    <table class='table'>
                        <tr>
                            <th>No Barang</th>
                            <th>Nama Barang</th>
                            <th>Satuan</th>
                            <th>Minimal Jumlah Stock</th>
                            <th>Kuantitas Sekarang</th>
                        </tr>
                        @foreach ($item as $stock)
                            @if ($stock !== null)
                                <tr>
                                    <td>{{ $stock['no_barang'] }}</td>
                                    <td>{{ $stock['nama_barang'] }}</td>
                                    <td>{{ $stock['satuan'] }}</td>
                                    <td>{{ $stock['minimal_stock'] }}</td>
                                    <td>{{ $stock['qty_sekarang'] }}</td>
                                </tr>
                            @endif
                        @endforeach
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection