@extends('chichi_theme.layout.app')

@section('custom_css')
@endsection

@section('content')
    <!-- Page-Title -->
    <div class="row">
		<div class="col-sm-12">
			<!-- Judul Halaman -->
			<h4 class="page-title">Barang dengan harga jual Nol</h4>

			<!-- Judul Halaman -->
			<ol id="breadcrumb">
				<li><a href="#"><span class="icon icon-home"> </span></a></li>
				<li><a href="#">Barang dengan harga jual Nol</a></li>
			</ol>

		</div>
	</div>
	<!-- END Page-Title -->
    <div class="row" style="padding: 0px;">
        <div class="col-md-12" style="padding: 0px;">
            <div class="card-body">
                @if ($item == null)
                    <div class="alert alert-warning"> Tidak Ada Data.</div>
                @else
                    <table class="table">
                        <tr>
                            <th>No Barang</th>
                            <th>Nama Barang</th>
                            <th>Kuantitas</th>
                            <th>Satuan</th>
                            <th>Harga Jual</th>
                        </tr>
                        @foreach ($item as $barang)
                            <tr>
                                <td>{{ $barang->no_barang }}</td>
                                <td>{{ $barang->keterangan }}</td>
                                <td>{!! $barang->updated_qty !!}</td>
                                <td>{{ $barang->unit }}</td>
                                <td>{{ $barang->harga_jual }}</td>
                            </tr>
                        @endforeach
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection