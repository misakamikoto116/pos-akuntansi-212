@extends('chichi_theme.layout.app')

@section('custom_css')
@endsection

@section('content')
    <!-- Page-Title -->
    <div class="row">
		<div class="col-sm-12">
			<!-- Judul Halaman -->
			<h4 class="page-title">Barang Kada Luarasa</h4>

			<!-- Judul Halaman -->
			<ol id="breadcrumb">
				<li><a href="#"><span class="icon icon-home"> </span></a></li>
				<li><a href="#">Barang Kada Luarasa</a></li>
			</ol>

		</div>
	</div>
	<!-- END Page-Title -->
    <div class="row" style="padding: 0px;">
        <div class="col-md-12" style="padding: 0px;">
            <div class="card-body">
                @if ($item === null)
                    <div class="alert alert-warning"> Tidak Ada Data.</div>
                @else
                    <table class="table">
                        <tr>
                            <th>No Barang</th>
                            <th>Nama Barang</th>
                            <th>Satuan</th>
                            <th>Tanggal Kada Luarsa</th>
                            <th>Estimasi Kada Luarsa</th>
                        </tr>
                        @foreach ($item as $barang)
                            <tr>
                                <td>{{ $barang['no_barang'] }}</td>
                                <td>{{ $barang['nama_barang'] }}</td>
                                <td>{{ $barang['satuan'] }}</td>
                                <td>{{ $barang['expired_date'] }}</td>
                                <td>{{ $barang['estimasi'] }}</td>
                            </tr>
                        @endforeach
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection