@extends('chichi_theme.layout.app')

@section('custom_css')
@endsection

@section('content')
    <!-- Page-Title -->
    <div class="row">
		<div class="col-sm-12">
			<!-- Judul Halaman -->
			<h4 class="page-title">Harga Jual Kurang dari Harga Modal</h4>

			<!-- Judul Halaman -->
			<ol id="breadcrumb">
				<li><a href="#"><span class="icon icon-home"> </span></a></li>
				<li><a href="#">Harga Jual Kurang dari Harga Modal</a></li>
			</ol>

		</div>
	</div>
	<!-- END Page-Title -->
    <div class="row" style="padding: 0px;">
        <div class="col-md-12" style="padding: 0px;">
            <div class="card-body">
                @if ($item->isEmpty())
                    <div class="alert alert-warning"> Tidak Ada Data.</div>
                @else
                    <table class="table">
                        <tr>
                            <th>No Barang</th>
                            <th>Nama Barang</th>
                            <th>Satuan</th>
                            <th>Harga Jual Sekarang</th>
                            <th>Harga Modal Barang</th>
                        </tr>
                        @foreach ($item as $modal)
                            <tr>
                                <td>{{ $modal['no_barang'] }}</td>
                                <td>{{ $modal['nama_barang'] }}</td>
                                <td>{{ $modal['satuan'] }}</td>
                                <td>{{ number_format($modal['harga_jual_sekarang']) }}</td>
                                <td>{{ number_format($modal['harga_modal_terakhir']) }}</td>
                            </tr>
                        @endforeach
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection