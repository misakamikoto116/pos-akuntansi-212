@include('chichi_theme.layout.header')
        <style type="text/css">
            body{
                font-size: 13px;
            }
            footer{
                display: none;
            }
            .row{
                padding: 5% 12%;
            }
            form{
                padding-top: 60px;
            }
            .bg{
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-image: url('{{ asset("assets/images/gallery/wizard.jpeg")}}');
                background-size: cover;
                background-position: center;
                filter: blur(5px);
            }
            .gradient{
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background: /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#27393d+0,618788+100 */
                background: rgb(39,57,61); /* Old browsers */
                background: -moz-linear-gradient(-45deg, rgba(39,57,61,0.8) 0%, rgba(97,135,136,0.8) 100%); /* FF3.6-15 */
                background: -webkit-linear-gradient(-45deg, rgba(39,57,61,0.8) 0%,rgba(97,135,136,0.8) 100%); /* Chrome10-25,Safari5.1-6 */
                background: linear-gradient(135deg, rgba(39,57,61,0.8) 0%,rgba(97,135,136,0.8) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#27393d', endColorstr='#618788',GradientType=1 );
            }
            .input-group-addon{
                background: #618788;
                border: 1px solid #618788;
            }
            .mid label{
                position: relative;
                transform: translateY(-25%);
                text-align: center;
            }
            .card{
                position: relative;
                margin-top: 30px;
            }
            .card-heade{
                padding: .75rem 1.25rem;
                position: absolute;
                width: 60%;
                top: -35px;
                left: 50%;
                transform: translateX(-50%);
                background: #27393d;
                border-radius: 100px !important;
                box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
            }

            .card-heade h4 b{
                color: white;
            }
            .row {
                padding: 2px 12%;
            }
        </style>

    <body class="fixed-left">
                        <div class="bg">
                        </div>
                        <div class="gradilogoutent"></div>
                        <div class="row" style="padding: 5% 12%">
                            <div class="col-md-12">
                                <div class="card">
                                <div class="card-heade">
                                    <h4 align="center"><b>Import Data Awal</b></h4>
                                </div>
                                    <div class="card-body">
                                        <div class="p-20">
                                            {!! Form::open(['route' => ['akuntansi.post.produk-upload-by-akun',0], 'files' => true ,'method' => 'POST', 'class' => 'form form-horizontal form-label-left', 'id' => 'formUpload']) !!}

                                            @include('akuntansi::barang.components.akun_default_barang')

                                            <hr>

                                            <div class="form-group row">
                                                <div class="col-4 col-form-label"><b>File CSV</b></div>
                                                <div class="col-8">
                                                    <input name="import_file" required type="file" data-parsley-fileextension='csv' class="filestyle form-control" data-iconname="fa fa-cloud-upload">
                                                </div>
                                            </div>
                                            <div class="submit">
                                                @if(!is_null($data['fileFormatUrl']))
                                                <a href="{!! $data['fileFormatUrl'] !!}" class="btn btn-default">Download Contah format</a>
                                                @endif
                                                {!! Form::button('<i class="fa fa-check"></i> Upload',['type' => 'submit','class' => 'btn btn-default','id' => 'btn-submit', 'disabled' => 'disabled']) !!}
                                            </div>
                                            {!! Form::close() !!}
                                            <small>CSV pastikan di save as dengan format csv dan separator ";"</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END -->

@include('chichi_theme.layout.footer')

</body>
