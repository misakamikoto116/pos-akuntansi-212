<h3>Informasi Pemasok</h3>
<section>
    <div class="form-group clearfix">
        <h5 align="center">Langkah 4</h5>
        <label class="col-lg-12">
            Masukkan pemasok-pemasok Anda beserta nilai hutangnya saat ini:
        </label>
        <div class="col-lg-12 warning">
            <small>* Isi 'Saldo Awal' untuk mengisi faktur-faktur yang belum terbayar.</small>
        </div>
    </div>
    <div class="row" style="padding: 0;">
        <div class="col-4" align="center">Nama</div>
        <div class="col-4" align="center">Saldo Awal</div>
    </div>
    <div class="tambah-sections pemasok_sections">
    </div>  <!-- End .duplicate-sections --> 
    <a class="btn btn-primary btn-rounded addzsection" href="#" role="button" data-section='0' style="margin-bottom: 30px;">
    <span class="btn-label"><i class="fa fa-plus"></i></span>
    Tambah Informasi Pemasok</a>

    <script id="form_section_pemasok" type="text/template"/>
        <div class="formz-section" data-id="1">
        <div class="form-group row clearfix mid" style="padding: 5px 10px;">
      <div class="col-4">
        <input type="text" class="form-control nama_pemasok_umum pemasok_required" id="nama_pemasok_umum" placeholder="Pemasok umum" name="nama_pemasok_umum[1]" required="">
      </div>
      <div class="col-4">
        <input type="text" class="form-control mask saldo_awal_pemasok pemasok_required" id="saldo_awal_pemasok" name="saldo_awal_pemasok[1]" required="" readonly="">
      </div>
      <div class="col-3">
        <button type="button" class="btn btn-default waves-effect waves-light tombol_pemasok" id="">Saldo Awal</button>
      </div>
      <div class="col-1"><a href="#" class="removez">X</a></div>
    </div>
    <!-- sample modal content -->
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0" id="myModalLabel">Saldo Awal</h4>
                    </div>
                    <div class="modal-body">
                    <table align="center" class="table">
                        <thead>
                        <tr>
                            <td style="text-align: center;">No. Faktur</td>
                            <td style="text-align: center;">Tanggal</td>
                            <td style="text-align: center;">Saldo Awal</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        </thead>
                                <tbody class="purchaseItemsPemasok"> 
                                </tbody>
                    </table>
                        <a class="btn btn-primary btn-rounded addPemasok " href="#" role="button" style="margin-bottom: 30px;">
                            <span class="btn-label"><i class="fa fa-plus"></i></span>
                            Tambah Saldo Awal</a>
                    </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary waves-effect waves-light simpan-saldo-pemasok">Save changes</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
      </div> <!-- End .form-section -->
    </script>

    <script id="table_section_pemasok" type="text/template"/>
    <tr>   
        <td>
            <input type="text" name="no_faktur_pemasok" value=""  class="form-control no_faktur_pemasok pemasok_required" id=""  required="">
        </td>
        <td>
            <input type="text" name="tanggal_pemasok" value="" class="form-control date-saldo tanggal_pemasok pemasok_required" id=""  required="">
        </td>
        <td>
            <input type="text" name="saldo_pemasok" value=""  class="form-control mask saldo_pemasok pemasok_required" id="" required="">
        </td>
        <td>
            <button type="button" class="removeRowPemasok btn btn-danger btn-sm">-</button>
        </td>
    </tr>
    </script>                
</section>