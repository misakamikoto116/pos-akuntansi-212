<h3>Informasi Pelanggan</h3>
<section>
    <div class="form-group clearfix">
        <h5 align="center">Langkah 3</h5>
        <label class="col-lg-12">
            Masukkan pelanggan-pelanggan Anda beserta nilai piutangnya saat ini:
        </label>
        <div class="col-lg-12 warning">
            <small>* Isi 'Saldo Awal' untuk mengisi faktur-faktur yang belum terbayar.</small>
        </div>
    </div>
    <div class="row" style="padding: 0;">
        <div class="col-4" align="center">Nama</div>
        <div class="col-4" align="center">Saldo Awal</div>
    </div>
    <div class="duplicate-sections pelanggan_sections">
    </div>  <!-- End .duplicate-sections -->
    <a class="btn btn-primary btn-rounded addsection" href="#" role="button" data-section='0' style="margin-bottom: 30px;">
    <span class="btn-label"><i class="fa fa-plus"></i></span>
    Tambah Informasi Pelanggan</a>

    <script id="form_section_pelanggan" type="text/template"/>
        <div class="form-section">
            <div class="form-group row clearfix mid" style="padding: 5px 10px;">
                <div class="col-4">
                    <input type="text" class="form-control nama_pelanggan_umum pelanggan_required" id="nama_pelanggan_umum" placeholder="Pelanggan umum" name="nama_pelanggan_umum" required>
                </div>
                <div class="col-4">
                    <input type="text" class="form-control mask saldo_awal_pelanggan pelanggan_required" id="saldo_awal_pelanggan" name="saldo_awal_pelanggan" required readonly="">
                </div>
                <div class="col-3">
                    <button type="button" class="btn btn-default waves-effect waves-light tombol_pelanggan">Saldo Awal</button>
                </div>
                <div class="col-1"><a href="#" class="remove">X</a></div>
            </div>
        <!-- sample modal content -->
            <div class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title mt-0" id="myModalLabel">Saldo Awal</h4>
                        </div>
                        <div class="modal-body">
                            <table align="center" class="table">
                                <thead>
                                    <tr>
                                        <td style="text-align: center;">No. Faktur</td>
                                        <td style="text-align: center;">Tanggal</td>
                                        <td style="text-align: center;">Saldo Awal</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </thead>
                                <tbody class="purchaseItemsPelanggan">
                                </tbody>
                            </table>
                            <a class="btn btn-primary btn-rounded add" href="#" role="button" style="margin-bottom: 30px;">
                            <span class="btn-label"><i class="fa fa-plus"></i></span>
                            Tambah Saldo Awal</a>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary waves-effect waves-light simpan-saldo-pelanggan">Save changes</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div> <!-- End .form-section -->
    </script>

    <script id="table_section_pelanggan" type="text/template"/>
        <tr>
            <td>
                <input type="text" name="no_faktur_pelanggan" value=""  class="form-control no_faktur_pelanggan pelanggan_required" id=""  required>
            </td>
            <td>
                <input type="text" name="tanggal_pelanggan" value="" class="form-control date-saldo tanggal_pelanggan pelanggan_required" id=""  required>
            </td>
            <td>
                <input type="text" name="saldo_pelanggan" value="0"  class="form-control mask saldo_pelanggan pelanggan_required" id="" required>
            </td>
            <td>
                <button type="button" class="removeRow btn btn-danger btn-sm">-</button>
            </td>
        </tr>    
    </script>

</section>
