<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="shortcut icon" href="{{asset('assets/images/favicon_1.ico')}}">

        <title>@yield('title', chichi_title ())</title>
        @include('chichi_theme.layout.header')
    </head>
 
        <style>
            .bg{
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-image: url('{{ asset("assets/images/gallery/wizard.jpeg")}}');
                background-size: cover;
                background-position: center;
                filter: blur(5px);
            }
            .gradient{
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background: rgba(40,53,56,0.4); /* Old browsers */
            }
              .card-box{
                background-image: url('{{ asset("assets/images/gallery/wizard.jpeg")}}');
                background-size: cover;
                background-position: center;
                border: none;
                position: relative; top: 40%; left: 50%; transform: translate(-50%, 15%);
                padding: 10% 2%;
                border-radius: 10px;
              }
            .wrapper-page .card-box{
                border: none;
            } 
              .mask{
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 406px;
                border-radius: 8px;
                /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#27393d+0,27393d+27,27393d+43,27393d+43,618788+100 */
                background: rgb(39,57,61); /* Old browsers */
                background: -moz-linear-gradient(-45deg, rgba(39,57,61,0.9) 0%, rgba(39,57,61,0.9) 27%, rgba(39,57,61,0.9) 43%, rgba(39,57,61,0.9) 43%, rgba(97,135,136m0.9) 100%); /* FF3.6-15 */
                background: -webkit-linear-gradient(-45deg, rgba(39,57,61m0.9) 0%,rgba(39,57,61m0.9) 27%,rgba(39,57,61m0.9) 43%,rgba(39,57,61m0.9) 43%,rgba(97,135,136,1) 100%); /* Chrome10-25,Safari5.1-6 */
                background: linear-gradient(135deg, rgba(39,57,61,0.9) 0%,rgba(39,57,61,0.9) 27%,rgba(39,57,61,0.9) 43%,rgba(39,57,61,0.9) 43%,rgba(97,135,136,0.9) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#27393d', endColorstr='#618788',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
              }
              .form-control{
                background-color: rgba(0,0,0,0.0);
                border-radius: 0;
                border-bottom: 2px solid white;
                border-right: none;
                border-top: none;
                border-left: none;
                color: white;
              }
              .form-control:focus{
                background-color: rgba(0,0,0,0.0);
                border-radius: 0;
                border-bottom: 2px solid white;
                border-right: none;
                border-top: none;
                border-left: none;
                color: white;
              }
              .form-group{

              }
              .btn-login{
                background-color: white;
                color: #618788;
                font-weight: 600;
                border-radius: 100px;
              }
              .col-1 i{
                color: white;
                position: relative; top: 50%; left: 50%; transform: translate(-50%, -50%);
              }
              .panel-heading h2 i{
                color: white !important;
                position: relative;
                z-index: 3;
              }
              footer{
                display: none;
              }
              .form-control::placeholder{
                color: white;
              }
            input:-webkit-autofill,
            input:-webkit-autofill:hover, 
            input:-webkit-autofill:focus{
            border: none;
            background-color: transparent;
            border-bottom: 2px solid white;
          -webkit-text-fill-color: white;
          transition: background-color 5000s ease-in-out 0s;
            }
        </style>

    </head>
    <body>

        <div class="bg">    
        </div>
        <div class="gradient"></div>   
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="card-box">
                <div class="mask"></div>
                <div class="panel-heading">
                    <h2 class="text-center"><i class="fa fa-area-chart"></i></h2>
                </div>


                <div class="p-20">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        @if ($errors->has('email'))
                            <div class="form-group row">
                                <span class="help-block col-12">
                                    <strong style="color : red !important">{{ $errors->first('email') }}</strong>
                                </span>
                            </div>
                        @endif
                        <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-1"><i class="fa fa-user"></i></div>
                            <div class="col-10">
                                <input class="form-control" type="text" required="" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-1"><i class="fa fa-key"></i></div>
                            <div class="col-10">
                                <input class="form-control" type="password" required="" placeholder="Password" name="password">
                            </div>
                        </div>

                        <!-- <div class="form-group ">
                            <div class="col-12">
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox-signup" type="checkbox">
                                    <label for="checkbox-signup">
                                        Remember me
                                    </label>
                                </div>

                            </div>
                        </div> -->

                        <div class="form-group text-center m-t-40">
                            <div class="col-12">
                                <button class="btn btn-login btn-block text-uppercase waves-effect waves-light"
                                        href="/dashboard" type="submit">LOGIN
                                </button>
                            </div>
                        </div>

                        @if (chichi_account()['logged'] != 1)
                            <div class="form-group row" style="
                                position: absolute;
                                background: white;
                                margin-right: 18px;
                                height: 100px;
                                border-radius: 5px;
                                width: 385px;
                            ">
                                <div style="padding: 10px">
                                    <p>
                                        <b>Username</b> : {{ chichi_account()['user_name'] }}
                                    </p>
                                    <p>
                                        <b>Password</b> : 123456
                                    </p>
                                </div>
                            </div>
                        @endif

                    </form>

                </div>
            </div>
            
        </div>

@include('chichi_theme.layout.footer')
</body>
</html>