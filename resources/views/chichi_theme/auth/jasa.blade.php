<h3>Informasi Jasa</h3>
<section class="mini" style="padding: 0;">
    <div class="form-group clearfix">
        <h5 align="center">Langkah 7</h5>
        <label class="col-lg-12">
            Jika perusahaan anda menjual jasa (bukan barang), maka masukan jasa - jasa tersebut :
        </label>
        <div class="col-lg-12">
        <div class="row" style="padding: 10px 0;">
            <div class="col-3">Kode</div>
            <div class="col-4">Tanggal Jasa</div>
            <div class="col-4">Harga Jual</div>
        </div>
        <!-- Main element container -->
        <div class="duplicate-jasa jasa_sections">
        </div>
        </div>
    </div>
        <!-- Add more button -->
        <a href="#" class="btn btn-primary addsectionJasa" style="margin-bottom: 30px;">Tambah Jasa</a>

        <!-- Addmore template -->
        <script id="form_section_jasa" type="text/template">
            <div class="form-jasa">
                <div class="form-group row" style="padding: 0;">
                    <div class="col-3">
                    <input type="text" class="form-control jasa_required" name="kode_jasa[]" required="">
                  </div>
                  <div class="col-4">
                    <input type="text" class="form-control jasa_required date-saldo" name="tanggal_jasa[]" required="">
                  </div>
                  <div class="col-4">
                    <input type="text" class="form-control jasa_required mask" name="harga_jual_jasa[]" required="">
                  </div>
                  <div class="col-1">
                    <a href="#" data-rowid="key" class="removeJasa">X</a>
                  </div>
                </div>    
            </div>
        </script>
</section>