<h3>Informasi Barang</h3>
<section class="mini" style="padding: 0;">
    <div id="input-barang">
        <div class="form-group clearfix">
            <h5 align="center">Langkah 5</h5>
            <label class="col-lg-12">
                Masukkan barang - barang yang Anda jual atau beli beserta stok terakhir :
            </label>
            <div class="col-lg-12">
            <!-- Main element container -->
            <div class="duplicate-barang barang_sections">
            </div>
            </div>
        </div>
        <!-- Add more button -->
        <hr>
        <a href="#" class="btn btn-primary addsectionBarang" style="margin-bottom: 30px;">Tambah Barang</a>
    </div>

    <div id="import-barang" style="display: none;">
        <div class="form-group clearfix">
            <h5 align="center">Langkah 5</h5>
            <label class="col-lg-12">
                Import Barang Melalui CSV
            </label>
            <div class="col-lg-12">
                <div class="card-body">
                    <div class="p-20">
                        {{-- {!! Form::open(['route' => ['akuntansi.post.produk-upload-by-akun',0], 'files' => true ,'method' => 'POST', 'class' => 'form form-horizontal form-label-left', 'id' => 'formUpload']) !!} --}}
                        <style type="text/css">
                            .row {
                                padding: 0% 12%;
                            }
                        </style>
                            @include('akuntansi::barang.components.akun_default_barang')

                        <hr>

                        <div class="form-group row">
                            <div class="col-4 col-form-label" style="padding-top: 18px"><b>File CSV</b></div>
                            <div class="col-8">
                               <div style="float: right;">
                                <input type="file" name="import_file" class="form-control">
                               </div> 
                            </div>
                        </div>
                        <hr>
                        <div class="submit">
                            @if(!is_null($data['fileFormatUrl']))
                            <a href="{!! $data['fileFormatUrl'] !!}" class="btn btn-default">Download Contah format</a>
                            @endif
                        </div>
                        {{-- {!! Form::close() !!} --}}
                        <small>CSV pastikan di save as dengan format csv dan separator ";"</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Addmore template -->
<script id="form_section_barang" type="text/template"/>
    <div class="form-barang">
        <hr>
        <div class="row" style="padding: 10px 0">
            <div class="col-md-5">
                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="control-label">No. Barang</label>
                    <input class="form-control barang_required" id="alamat" name="no_barang[]" required="">
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="control-label">Nama</label>
                    <input class="form-control barang_required" id="alamat" name="nama_barang[]" required="">
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="control-label">Tanggal Stok</label>
                    <input type="text" class="form-control date-saldo barang_required" name="tanggal_stok_barang[]" required="">
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="control-label">Kuantitas</label>
                    <input class="form-control barang_required" id="alamat" name="kuantitas[]" required="">
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="control-label">Satuan</label>
                    <input class="form-control barang_required" id="alamat" name="satuan[]" required="">
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="control-label">Harga beli akhir</label>
                    <input type="text" class="form-control mask barang_required" id="alamat" name="harga_beli[]" required="">
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="control-label">Harga Jual</label>
                    <input type="text" class="form-control mask barang_required" id="alamat" name="harga_jual_barang[]" required="">
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="control-label">Gudang</label>
                    {!! Form::select('gudang_id[]',$gudang,null,['class' => 'form-control select2','placeholder' => '- Pilih -']) !!}
                </div>
            </div>
            <div class="col-md-2">
                <a href="#"  class="removeBarang">X</a>
            </div>
        </div>        
    </div>
</script>