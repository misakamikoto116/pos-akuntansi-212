<h3>Kas/Bank</h3>
<section style="overflow: hidden;">
    <div class="form-group clearfix">
        <h5 align="center">Langkah 2</h5>
        <label class="col-lg-12">
            Masukan Kas & Bank yang terakhir anda miliki :
        </label>
    </div>
    <div class="row" style="padding: 10px;">
        <div class="col-4">Akun</div>
        <div class="col-3">Tanggal</div>
        <div class="col-4">Nominal</div>
    </div>

    <div class="duplicate-kas-bank kas_bank_sections">
      
    </div>
    <a href="#" class="btn btn-primary addsectionKasBank" style="margin-bottom: 30px;">Tambah Akun</a>
    {{-- @foreach ($akun as $key => $value) --}}
    <script id="form_section_kas_bank" type="text/template">
      <div class="form-group row clearfix mid form-kas-bank" style="padding: 5px 10px;">
        <div class="col-4">
        {!! Form::select('akun_id[]',$akun_bank, null ,['class' => 'form-control select2 kas_bank_required','id' => '','required' => '']) !!}    
        </div>
        <div class="col-3">
          <input type="text" name="tanggal_akun[]" class="form-control date-saldo kas-bank kas_bank_required" required="">
        </div>
        <div class="col-4">
          <input class="form-control mask kas-bank kas_bank_required" type="text" name="nominal_akun[]" id="example-text-input" required="">
        </div>
        <div class="col-1">
          <a href="#"  class="removeKasBank">X</a>
        </div>
      </div>
    </script>
    {{-- @endforeach --}}
</section>
