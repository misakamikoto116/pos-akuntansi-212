<h3>Metode Persediaan</h3>
<section>
    <div class="form-group clearfix">
        <h5 align="center">Langkah 5</h5>
        <label class="col-lg-12">
            Pilih Metode Persediaan
        </label>
       <div class="col-lg-12">
            <div class="form-group row">
                <div class="col-1"><input type="radio" name="metode" checked="" value="average"></div>
                <div class="col-11">
                    <h4>Rata - rata (Direkomendasikan)</h4>
                </div>
                <div class="col-1"></div>
                <div class="col-11"><p>Metode perhitungan ini didasarkan pada rata-rata harga beli saat itu</p></div>
            </div>
            <div class="form-group row">
                <div class="col-1"><input type="radio" name="metode" value="fifo"></div>
                <div class="col-11">
                    <h4>First In First Out</h4>
                </div>
                <div class="col-1"></div>
                <div class="col-11"><p>Metode ini menghitung harga barang yang keluar berdasarkan harga barang yang masuk lebih dulu</p>
                </div>
            </div>
        </div>
        <div class="col-lg-12 warning">
            <i class="fa fa-warning"></i> &nbsp;Metode ini tidak dapat diganti setelah Anda mulai melakukan transaksi
        </div>
    </div>
</section>