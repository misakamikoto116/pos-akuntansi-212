@include('chichi_theme.layout.header')

        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery.steps/css/jquery.steps.css" />
        <link href="assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <style type="text/css">
            body{
                font-size: 13px;
            }
            footer{
                display: none;
            }
            .row{
                padding: 5% 12%;
            }
            form{
                padding-top: 60px;
            }
            .bg{
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-image: url('{{ asset("assets/images/gallery/wizard.jpeg")}}');
                background-size: cover;
                background-position: center;
                filter: blur(5px);
            }
            .gradient{
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background: /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#27393d+0,618788+100 */
                background: rgb(39,57,61); /* Old browsers */
                background: -moz-linear-gradient(-45deg, rgba(39,57,61,0.8) 0%, rgba(97,135,136,0.8) 100%); /* FF3.6-15 */
                background: -webkit-linear-gradient(-45deg, rgba(39,57,61,0.8) 0%,rgba(97,135,136,0.8) 100%); /* Chrome10-25,Safari5.1-6 */
                background: linear-gradient(135deg, rgba(39,57,61,0.8) 0%,rgba(97,135,136,0.8) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#27393d', endColorstr='#618788',GradientType=1 );
            }
            .input-group-addon{
                background: #618788;
                border: 1px solid #618788;
            }
            .mid label{
                position: relative;
                transform: translateY(-25%);
                text-align: center;
            }
            .card{
                position: relative;
                margin-top: 30px;
            }
            .card-heade{
                padding: .75rem 1.25rem;
                position: absolute;
                width: 60%;
                top: -35px;
                left: 50%;
                transform: translateX(-50%);
                background: #27393d;
                border-radius: 100px !important;
                box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
            }

            .card-heade h4 b{
                color: white;
            }
            .wizard > .content{
                box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                background: white;
                margin-bottom: 20px !important;
            }
            .wizard > .steps .current a, .wizard > .steps .current a:hover, .wizard > .steps .current a:active{
                background: #618788;
                box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
            }
            .wizard > .steps .done a, .wizard > .steps .done a:hover, .wizard > .steps .done a:active{
                background: rgba(97,135,136,0.6);
                box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
            }
            .wizard > .actions a{
                background: #618788;
                box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                transition: 0.5s;
                margin-bottom: 20px;
            }
            .wizard > .actions a:hover, .wizard > .actions a:active{
                box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
                background: #618788;
             }
             .warning{
                color: red;
             }
             .remove{
                color: #d9534f;
                font-weight: 1000;
             }
             .wizard > .content {
                overflow: auto;
             }
             input[type="radio"]{
                position: relative;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
             }
              .steps ul li {
                pointer-events: none
              }
        </style>

    <body class="fixed-left">
                        <div class="bg">
                        </div>
                        <div class="gradilogoutent"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                <div class="card-heade">
                                    <h4 align="center"><b>Getting Started</b></h4>
                                    </div>
                                    {!! Form::open(['route' => 'save.starting','method' => 'POST','id' => 'wizard-vertical', 'class' => 'submitForm','enctype' => 'multipart/form-data']) !!}
                                        <h3>Mata Uang & Info Perusahaan</h3>
                                        <section>
                                            <h5 align="center">Langkah 1</h5>
                                            <div class="form-group clearfix">
                                                <label class="control-label " for="userName1">Nama Perusahaan</label>
                                                <div class="">
                                                    <input class="form-control perusahaan" id="userName1" value="{{ $identitas->nama_perusahaan ?? null }}" name="nama_perusahaan" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="control-label " for="password1">Alamat</label>
                                                <div class="">
                                                    <textarea class="form-control perusahaan" name="alamat" id="alamat" rows="3">{{ $identitas->alamat ?? null }}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group clearfix">
                                                <label>Tanggal Mulai (Tutup Buku)</label>
                                                <div>
                                                <div class="input-group">
                                                    <input type="text" class="form-control perusahaan" id="tutupbuku" value="{{ $identitas->tanggal_mulai ?? null }}" name="tanggal_mulai">
                                                    <span class="input-group-addon bg-custom b-0" style="height: 30px;"><i class="md md-event-note text-white"></i></span>
                                                </div><!-- input-group -->
                                            </div>
                                            </div>

                                          <div class="form-group" style="position: relative;">
                                            {!! Form::label('mata_uang','Mata Uang',['class' => 'col-form-label']) !!}
                                                {!! Form::select('mata_uang_id',$mata_uang, $identitas->mata_uang_id ?? null ,['class' => 'form-control select2 perusahaan','id' => 'mata_uang_id']) !!}
                                            <div class="hide" style="width: 100%; background-color: white; height: 29px; position: absolute; bottom: 0px;"></div>
                                          </div>
                                        </section>
                                        @include('chichi_theme.auth.kas_bank')
                                        @include('chichi_theme.auth.pelanggan')
                                        @include('chichi_theme.auth.pemasok')
                                        {{-- @include('chichi_theme.auth.metode_persediaan') --}}
                                        @include('chichi_theme.auth.barang')
                                        {{-- @include('chichi_theme.auth.jasa') --}}
                                    {!! Form::close() !!}
                                    <!-- End #wizard-vertical -->

                                </div>
                            </div>
                        </div>
                        <!-- END -->

@include('chichi_theme.layout.footer')
    <script type="text/javascript">
        $('#mata_uang_id').select2();
    </script>
    <!--Form Wizard-->
    <script src="{{ asset('assets/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

    <!--wizard initialization-->
    <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/pages/jquery.form-pickers.init.js') }}"></script>
    <script src="{{ asset('assets/plugins/addel/jqery.fieldsaddmore.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/starting.js') }}"></script>


</body>
