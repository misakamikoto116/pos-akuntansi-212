<div class="col-sm-12">
    <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalPencarian"><i class="fa fa-search"></i> Pencarian ...</button>
</div>

    <div id="modalPencarian" class="modal fade bs-example-modal-lg" role="dialog" aria-labelledby="modalPencarian" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title mt-0">Pencarian</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div> 
                @if ($type == 'pelanggan')
                    {!! Form::open(['route' => ['akuntansi.pelanggan.history-buku-besar', $type, $id], 'method' => 'GET']) !!}
                @else
                    {!! Form::open(['route' => ['akuntansi.pemasok.history-buku-besar', $type, $id], 'method' => 'GET']) !!}
                @endif
                
                    <div class="modal-body">
                        <div class="form-group row">
                            {!! Form::label('search','Search by keyword',['class' => 'col-3 col-form-label']) !!}
                                <div class="col-9">
                                    {!! Form::text('search', old('search'), ['class' => 'form-control','placeholder' => 'Cari ...']) !!}
                                </div>
                        </div>

                        @if(in_array('no_transaksi',$filter))
                            <div class="form-group row">
                                {!! Form::label('no_sumber','No Sumber',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_transaksi', old('no_transaksi'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan No Sumber']) !!}
                                    </div>
                            </div>
                        @endif

                        @if(in_array('no_akun',$filter))
                            <div class="form-group row">
                                {!! Form::label('no_akun','No Akun',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_akun', old('no_akun'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan No Akun']) !!}
                                    </div>
                            </div>
                        @endif

                        @if(in_array('akun_id',$filter))
                            <div class="form-group row">
                                {!! Form::label('akun_id','Akun',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('akun_id', $akun, old('akun_id'), ['class' => 'form-control select2','placeholder' => 'Pilih Akun']) !!}
                                    </div>
                            </div>
                        @endif

                        @if(in_array('tipe_akun_id',$filter))
                            <div class="form-group row">
                                {!! Form::label('tipe_akun_id','Tipe Akun',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('tipe_akun_id', $tipe_akun, old('tipe_akun_id'), ['class' => 'form-control select2','placeholder' => 'Pilih Tipe Akun']) !!}
                                    </div>
                            </div>
                        @endif

                        @if(in_array('tanggal_dari_sampai',$filter))
                            <div class="form-group row">
                                {!! Form::label('dari','Dari',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-3">
                                        {!! Form::date('tanggal_dari', old('tanggal_dari'), ['class' => 'form-control']) !!}
                                    </div>
                                {!! Form::label('sampai','s/d',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-3">
                                        {!! Form::date('tanggal_sampai', old('tanggal_sampai'), ['class' => 'form-control']) !!}
                                    </div>
                            </div>
                        @endif

                        @if(in_array('filter_name_space',$filter))
                            <hr>
                            <div class="col-sm-12" style="border: 2px solid rgba(0,0,0,.1); border-radius: 15px;">
                                <table class="table">
                                    <thead style="display:table;width:100%;table-layout:fixed;width: calc( 100% - 1em )">
                                        <tr>
                                            <th>Tipe</th>
                                            <th style="float: right;">
                                                {!! Form::button('Pilih Semua',['class' => 'btn btn-success btn-sm button-checkbox-filter-menu','onclick' => 'changeCheckBoxFilterMenu()','id' => 'button-all-checked']) !!}
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody style="display:block;max-height:200px;overflow:auto;">
                                        @foreach ($name_space as $key => $data_name_space)
                                            <tr>
                                                <td>{{ $key }}</td>
                                                <td><input name="filter_name_space[]" value="{{ $data_name_space }}" class="checkbox-custom checkbox-filter-menu" checked="" type="checkbox"></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        {!! Form::submit('Cari !', ['class' => 'btn btn-primary']) !!}
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div> 