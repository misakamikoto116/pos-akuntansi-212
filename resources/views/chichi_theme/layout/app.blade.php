<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Chichi Jaya Akuntansi">
        <meta name="author" content="Thortech Asia">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <link rel="shortcut icon" href="{{asset('assets/images/favicon_1.ico')}}">

        <title>@yield('title', chichi_title())</title>
        
        @include('chichi_theme.layout.header')
        @yield('custom_css')
        <audio id="audio-btn" src="{{ asset('assets/sound/beep-01.mp3') }}" autostart="false" ></audio>
        <audio id="audio-btn-err" src="{{ asset('assets/sound/beep-03.mp3') }}" autostart="false" ></audio>
    </head>

    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            <!-- Navigation Bar-->
            <div id="page-loader"></div>
            <p id="page-loader-text">
                Loading ...
            </p>

            <div id="page-show">
                <header id="topnav">
                    <div class="topbar-main">
                        <div class="pull-left" style="padding: 0px 6px; background: #618788; border-radius: 10px; margin: 2.5px 5px;">
                            <button class="button-menu-mobile open-left waves-effect waves-light">
                                <i class="md md-menu"></i>
                            </button>
                            <span class="clearfix"></span>
                        </div>
                        <div class="container kostum-bar-samping">
                            <!-- Logo container-->
                            <div class="logo">
                                <!-- Text Logo -->
                                <!--<a href="index.html" class="logo">-->
                                <!--UBold-->
                                <!--</a>-->
                                <!-- Image Logo -->
                                <a href="{{ url('/') }}" class="logo">
                                    @if ( pathinfo(chichi_logo(), PATHINFO_EXTENSION) )
                                        <img src="{{ asset('storage/produk/'.chichi_logo()) }}" alt="" height="60" class="logo-lg" style="margin : 3px auto;">
                                    @else
                                        <img src="{{ asset('assets/images/brand/no-logo.png') }}" alt="" height="60" class="logo-lg" style="margin : 3px auto;">
                                    @endif
                                    {{-- <img src="assets/images/logo_sm.png" alt="" height="24" class="logo-sm"> --}}
                                </a>

                            </div>
                            <!-- End Logo container-->


                            <div class="menu-extras topbar-custom">

                                <ul class="list-inline float-right mb-0">

                                    {{-- <li class="menu-item list-inline-item">
                                        <!-- Mobile menu toggle-->
                                        <a class="navbar-toggle nav-link">
                                            <div class="lines">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                        </a>
                                        <!-- End mobile menu toggle-->
                                    </li> --}}

                                    {{-- <li class="list-inline-item notification-list">
                                        <a class="nav-link waves-light waves-effect" href="#" id="btn-fullscreen">
                                            <i class="dripicons-expand noti-icon"></i>
                                        </a>
                                    </li> --}}

                                    <li class="list-inline-item dropdown notification-list">
                                        <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                                        aria-haspopup="false" aria-expanded="false">
                                            <i class="dripicons-bell noti-icon"></i>
                                            <span class="badge badge-pink noti-icon-badge">4</span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
                                            <!-- item-->
                                            <div class="dropdown-item noti-title">
                                                <h5><span class="badge badge-danger float-right">5</span>Notification</h5>
                                            </div>

                                            <!-- item-->
                                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                                <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                                                <p class="notify-details">Robert S. Taylor commented on Admin<small class="text-muted">1 min ago</small></p>
                                            </a>

                                            <!-- item-->
                                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                                <div class="notify-icon bg-info"><i class="icon-user"></i></div>
                                                <p class="notify-details">New user registered.<small class="text-muted">1 min ago</small></p>
                                            </a>

                                            <!-- item-->
                                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                                <div class="notify-icon bg-danger"><i class="icon-like"></i></div>
                                                <p class="notify-details">Carlos Crouch liked <b>Admin</b><small class="text-muted">1 min ago</small></p>
                                            </a>

                                            <!-- All-->
                                            <a href="javascript:void(0);" class="dropdown-item notify-item notify-all">
                                                View All
                                            </a>

                                        </div>
                                    </li>

                                    <li class="list-inline-item dropdown notification-list">
                                        <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                                        aria-haspopup="false" aria-expanded="false">
                                            <img src="{{asset('assets/images/users/avatar-1.jpg')}}" alt="user" class="rounded-circle">
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                            <!-- item-->
                                            <a href="javascript:void(0);" class="dropdown-item noti-title">
                                                <h5><span>Welcome! {{ ucwords(auth()->user()->name ?? '') }}</span></h5>
                                            </a>
                                            <a href="{{ route('profile') }}" class="dropdown-item notify-item">
                                                <i class="fa fa-user"></i> <span>Profile</span>
                                            </a>
                                            <!-- item-->
                                            {{-- @hasanyrole('admin|akuntansi|supervisi|gudang') --}}
                                                <a href="{{ route('logout') }}" class="dropdown-item notify-item">
                                                    <i class="fa fa-power-off"></i> <span>Logout</span>
                                                </a>
                                            {{-- @endhasanyrole --}}
                                        </div>
                                    </li>

                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div> <!-- end container -->
                    </div>
                    <!-- end topbar-main -->

                </header>
                @include('chichi_theme.layout.vertical_sidebar')
                <div class="content-page" style="padding-left: 20px; padding-right: 20px; padding-bottom: 100px;">
                    <!-- Start content -->
                    <div id="main" style="padding: 100px 15px 5px 15px;">
                        @include('chichi_theme.layout.flash')
                        
                        @yield('content')
                    </div>
                    <!-- End Content -->
                </div>
                <div class="clearfix"></div>
                <!-- Footer -->
                <div class="foot">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 text-center">
                                ©2018 - Thortech.Asia
                            </div>
                        </div>
                    </div>
                </div>
                @include('chichi_theme.layout.footer')
                @yield('custom_js')
                <!-- End Footer -->        
            </div>
        </div>
    </body>
</html>
@include('chichi_theme.layout.sweet')
@include('chichi_theme.layout.sweet_error')
@include('chichi_theme.layout.sweet_delete')