<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft" style="overflow-y: scroll;">
        <div id="sidebar-menu">
            <ul style="font-size:14px!important">
                <li class="text-muted menu-title">Navigation</li>
                @include('chichi_theme.layout.sidebars.sidebar_akuntansi')
                @include('chichi_theme.layout.sidebars.sidebar_admin')
                @include('chichi_theme.layout.sidebars.sidebar_pos')
                @include('chichi_theme.layout.sidebars.sidebar_gudang')
                @include('chichi_theme.layout.sidebars.sidebar_pengawas')
            </ul>
        </div>
    </div>
</div>