@role('pos')
    <li class="has_sub">
        <a href="{{ url('pos') }}" class="waves-effect"><i class="ti-home"> </i> <span> Beranda </span></a>
    </li>
    <li class="has_sub">
        <a href="#" class="waves-effect"><i class=" ti-settings"></i> <span> Kasir </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @if ( ! session()->get('kasir_cek', false) )
                <li><a href="{{ route('pos.penjualan.kasir.check.in') }}">Cek In</a></li>
            @else
                <li><a href="#" data-toggle="modal" data-target="#modal-check-out">Cek Out</a></li>
            @endif
                <li><a target="_blank" href="{{ route('pos.pos.display.kasir') }}">Display Kasir</a></li>
        </ul>
    </li>
    <li class="has_sub">
        <a href="{{ url('pos/penjualan') }}" class="waves-effect"> <i class="ti-money"></i> <span> Penjualan </span> </a>
    </li>
    <li class="has_sub">
        <a href="{{ url('pos/promosi') }}" class="waves-effect"> <i class="ti-gift"></i> <span> Promosi </span> </a>
    </li>
    <li class="has_sub">
        <a href="{{ route('pos.penjualan.kasir.transaksi') }}" class="waves-effect"> <i class="ti-money"></i> <span> Rekap Penjualan </span> </a>
    </li>
    <li class="has_sub">
        <a href="{{ url('pos/barang') }}" class="waves-effect"> <i class="ti-layout-width-default"></i> <span> List Barang </span> </a>
    </li>
    <li class="has_sub">
        <a href="{{ url('pos/laporan') }}" class="waves-effect"> <i class="fa fa-archive"></i> <span> Daftar Laporan </span> </a>
    </li>
@endrole