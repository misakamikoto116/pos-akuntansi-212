@role('gudang')
    <li class="has_sub">
        <a href="{{ url('gudang') }}" class="waves-effect"><i class="ti-home"> </i> <span> Beranda </span></a>
    </li>
    <li class="has_sub">
        <a class="waves-effect" href="{{ url('gudang/barang') }}"><i class="ti-server"> </i> <span>Daftar Barang & Jasa</span></a>
    </li>    
    <li class="has_sub">
        <a class="waves-effect" href="{{ url('gudang/serial-number') }}"><i class="ti-notepad"> </i> <span>Pencatatan Nomor Serial</span></a>
    </li>    
    <li class="has_sub">
        <a class="waves-effect" href="{{ url('gudang/kategori-produk') }}"><i class="ti-clipboard"> </i> <span>Kategori Barang</span></a>
    </li>    
    <li class="has_sub">
        <a href="#" class="waves-effect"><i class="ti-world"> </i> <span> Gudang </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ url('gudang/daftar-gudang') }}">Daftar Gudang</a></li>
            <li><a href="{{ url('gudang/barang-pergudang') }}">Barang Pergudang</a></li>
            <li><a href="{{ url('gudang/pemindahan-barang') }}">Pemindahan Barang</a></li>
        </ul>
    </li>    
    <li class="has_sub">
        <a href="#" class="waves-effect"><i class="ti-package"> </i> <span> Penyesuaian Gudang </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ url('gudang/penyesuaian-persediaan') }}">Penyesuaian Persediaan</a></li>
            <li><a href="{{ url('gudang/penyesuaian-harga-jual') }}">Penyesuaian Harga Jual</a></li>
        </ul>
    </li>
@endrole