@role('akuntansi')
    <li class="has_sub">
        <a href="{{ url('akuntansi') }}" class="waves-effect"><i class="ti-home"> </i> <span> Beranda </span></a>
    </li>
    <li class="has_sub">
        <a href="#" class="waves-effect"><i class="ti-settings"></i> <span> Masters </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ route('akuntansi.identitas.edit', chichi_identitas_id() ?? 1) }}">Info Perusahaan</a></li>
            <li><a href="{{ url('akuntansi/preferensi') }}">Preferensi</a></li>
            <li><a href="{{ url('akuntansi/preferensi-pos') }}">Preferensi POS</a></li>
            <li><a href="#">Data Pengguna</a></li>
            <li><a href="{{ url('akuntansi/syarat-pembayaran') }}">Syarat Pembayaran</a></li>
            <li><a href="{{ url('akuntansi/tipe-pelanggan') }}">Tipe Pelanggan</a></li>
            <li><a href="{{ url('akuntansi/cart-log') }}">Cart Log</a></li>
            <li><a href="{{ url('akuntansi/type-edc') }}">Type EDC</a></li>
        </ul>
    </li>
    <li class="has_sub">
        <a href="#" class="waves-effect"><i class="ti-files"></i> <span> Import </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ route('akuntansi.get.produk-upload') }}">Import Produk</a></li>
            <li><a href="{{ route('akuntansi.get.produk-upload-by-akun') }}">Import Produk By Akun</a></li>
            <li><a href="{{ route('akuntansi.get.pelanggan-upload') }}">Import Pelanggan</a></li>
            <li><a href="{{ route('akuntansi.get.pemasok-upload') }}">Import Pemasok</a></li>
            <li><a href="{{ route('akuntansi.get.akun-upload') }}">Import Akun</a></li>
            <li><a href="{{ route('akuntansi.get.jasa-pengiriman-upload') }}">Import Jasa Pengiriman</a></li>
            <li><a href="{{ route('akuntansi.get.syarat-pembayaran-upload') }}">Import Syarat Pembayaran</a></li>
            <li><a href="{{ route('akuntansi.get.import-barcode') }}">Import Barcode</a></li>
        </ul>
    </li>
    <li class="has_sub">
        <a href="#" class="waves-effect"><i class="ti-cloud-down"></i> <span> Berkas </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="#">Data Baru</a></li>
            <li><a href="#">Buka Data</a></li>
            <li><a href="#">Menutup Data</a></li>
            <li><a href="#">Cadangan Data</a></li>
            <li><a href="#">Import Jurnal</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-tutup-buku">Tutup Buku Perusahaan</a></li>
            <li><a href="#">Export Import Transaksi</a></li>
        </ul>
    </li>
    <li class="has_sub">
        <a href="#" class="waves-effect"><i class="ti-agenda"></i> <span> Buku Besar </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ url('akuntansi/history-buku-besar') }}">History Buku Besar</a></li>
            <li><a href="{{ url('akuntansi/saldo-akun') }}">Saldo Akun</a></li>
            <li><a href="{{ url('akuntansi/tipe-akun') }}">Tipe Akun</a></li>
            <li><a href="{{ url('akuntansi/akun') }}">Daftar Akun</a></li>
            <li><a href="{{ url('akuntansi/mata-uang') }}">Mata Uang</a></li>
            <li><a href="{{ url('akuntansi/jurnal-umum') }}">Bukti Jurnal Umum</a></li>
        </ul>
    </li>
    <li class="has_sub">
        <a href="#" class="waves-effect"><i class="ti-money"></i> <span> Kas & Bank </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ route('akuntansi.buku_bank.show') }}">Buku Bank</a></li>
            <li><a href="{{ url('akuntansi/pembayaran') }}">Pembayaran</a></li>
            <li><a href="{{ url('akuntansi/penerimaan') }}">Penerimaan</a></li>
            <li><a href="{{ url('akuntansi/rekonsiliasi-bank') }}">Rekonsiliasi Bank</a></li>
        </ul>
    </li>
    <li class="has_sub">
        <a href="#" class="waves-effect"><i class="ti-server"></i> <span> Daftar </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li class="has_sub">
                <a href="#" class="waves-effect"><i class="ti-package"></i><span> Barang & Jasa </span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                    <li><a href="{{ url('akuntansi/barang') }}">Daftar Barang & Jasa</a></li>
                    <li><a href="{{ url('akuntansi/serial-number') }}">Pencatatan Nomor Serial</a></li>
                    <li><a href="{{ url('akuntansi/kategori-produk') }}">Kategori Barang</a></li>
                    <li><a href="{{ url('akuntansi/penyesuaian-persediaan') }}">Penyesuaian Persediaan</a></li>
                    <li><a href="{{ url('akuntansi/gudang') }}">Daftar Gudang</a></li>
                    <li><a href="{{ url('akuntansi/barang-pergudang') }}">Barang Pergudang</a></li>
                    <li><a href="{{ url('akuntansi/pemindahan-barang') }}">Pemindahan Barang</a></li>
                    {{-- <li><a href="{{ url('akuntansi/penyesuaian-harga-jual') }}">Penyesuaian Harga Jual</a></li> --}}
                </ul>
            </li>
            <li class="has_sub">
                <a href="#" class="waves-effect"><i class="fa fa-cart-arrow-down"></i><span> Aktiva Tetap </span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                    <li><a href="{{ url('akuntansi/tipe-aktiva-tetap-pajak') }}">Tipe Aktiva Tetap Pajak</a></li>
                    <li><a href="{{ url('akuntansi/tipe-aktiva-tetap') }}">Tipe Aktiva Tetap</a></li>
                    <li><a href="{{ url('akuntansi/daftar-aktiva-tetap') }}">Daftar Aktiva Tetap</a></li>
                </ul>
            </li>
            <li class="has_sub">
                <a href="{{ url('akuntansi/informasi-pemasok') }}"><i class="fa fa-users"></i><span> Pemasok </span></a>
            </li>
            <li class="has_sub">
                <a href="{{ url('akuntansi/informasi-pelanggan') }}"><i class="fa fa-users"> </i> <span> Pelanggan </span></a>
            </li>
            <li class="has_sub">
                <a href="{{ url('akuntansi/jasa-pengiriman') }}"><i class="fa fa-truck"> </i> <span> Jasa Pengiriman </span></a>
            </li>
            <li class="has_sub">
                <a href="#" class="waves-effect"> <i class="fa fa-cubes"> </i> <span> Penyelesaian Pesanan </span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                    <li><a href="{{ url('akuntansi/pembiayaan-pesanan') }}">Pembiayaan Pesanan</a></li>
                    <li><a href="{{ url('akuntansi/penyelesaian-pesanan') }}">Penyelesaian Pesanan</a></li>
                </ul>
            </li>
            <li class="has_sub">
                <a href="{{ url('akuntansi/promosi') }}"> <i class="fa fa-hand-o-right"></i> <span> Promosi </span></a>
            </li>
        </ul>
    </li>
    <li class="has_sub">
        <a href="#" class="waves-effect"> <i class="fa fa-money"></i> <span> Aktivitas </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li class="has_sub">
                <a href="#"> <i class="fa fa-dropbox"> </i> <span> Penjualan </span> <span class="menu-arrow"></span> </a>
                <ul class="list-instyled">
                    {{-- <li><a href="{{ url('akuntansi/penawaran-penjualan') }}">Penawaran Penjualan</a></li>
                    <li><a href="{{ url('akuntansi/pesanan-penjualan') }}">Pesanan Penjualan</a></li>
                    <li><a href="{{ url('akuntansi/pengiriman-penjualan') }}">Pengiriman Pesanan</a></li> --}}
                    <li><a href="{{ url('akuntansi/faktur-penjualan') }}">Faktur Penjualan</a></li>
                    <li><a href="{{ url('akuntansi/penerimaan-penjualan') }}">Penerimaan Penjualan</a></li>
                    <li><a href="{{ url('akuntansi/retur-penjualan') }}">Retur Penjualan</a></li>
                    <li><a href="{{ url('akuntansi/uang-muka-pelanggan') }}">Uang Muka Pelanggan</a></li>
                </ul>
            </li>
            <li class="has_sub">
                <a href="#"> <i class="fa fa-dropbox"></i> <span> Pembelian</span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                    {{-- <li><a href="{{ url('akuntansi/permintaan-pembelian') }}">Permintaan Barang</a></li>
                    <li><a href="{{ url('akuntansi/pesanan-pembelian') }}">Pesanan Pembelian</a></li>
                    <li><a href="{{ url('akuntansi/penerimaan-barang') }}">Penerimaan Barang</a></li> --}}
                    <li><a href="{{ url('akuntansi/faktur-pembelian') }}">Faktur Pembelian</a></li>
                    <li><a href="{{ url('akuntansi/pembayaran-pembelian') }}">Pembayaran Pembelian</a></li>
                    <li><a href="{{ url('akuntansi/retur-pembelian') }}">Retur Pembelian</a></li>
                    <li><a href="{{ url('akuntansi/uang-muka-pemasok') }}">Uang Muka Pemasok</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li class="has_sub">
        <a href="#" class="waves-effect"><i class="fa md-layers"></i> <span> Perusahaan</span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li class="has_sub">
                <a href="#"> Karyawan <i class="menu-arrow"></i></a>
                <ul class="list-unstyled">
                    <li><a href="{{url('akuntansi/data-karyawan')}}">Data Karyawan</a></li>
                    <li><a href="{{url('akuntansi/pemberitahuan')}}">Pemberitahuan</a></li>
                </ul>
            </li>
            <li class="has_sub">
                <a href="#">Lowongan <i class="menu-arrow"></i></a>
                <ul class="list-unstyled">
                    <li><a href="#">Data Lowongan</a></li>
                </ul>
            </li>
            <li class="has_sub">
                <a href="#">Absensi</a>
            </li>
            <li class="has_sub">
                <a href="#">Pelacakan</a>
            </li>
            <li class="has_sub">
                <a href="#">Pajak <i class="menu-arrow"></i></a>
                <ul class="list-unstyled">
                    <li><a href="{{ url('akuntansi/kode-pajak') }}">Kode Pajak</a></li>
                    <li><a href="{{ url('akuntansi/nomor-pajak') }}">Nomor Pajak</a></li>
                </ul>
            </li>
            <li class="has_sub">
                <a href="{{ route('akuntansi.upload.stiker.index') }}">Upload Image Stiker</a>
            </li>
            <li class="has_sub">
                <a href="{{ url('akuntansi/display-iklan') }}">Iklan Display</a>
            </li>
        </ul>
    </li>
    <li class="has_sub">
        <a href="#" class="waves-effect"> <i class="fa fa-clipboard"></i> <span> Rekap Transaksi </span> <span class="menu-arrow"></span> </a>
        <ul class="list-unstyled">
            <li><a href="{{ route('akuntansi.penjualan.rekap.transaksi') }}"><span>Rekap Transaksi Pos Harian</span></a></li>
        </ul>
    </li>
    <li class="has_sub">
        <a href="#" class="waves-effect"> <i class="fa fa-archive"></i> <span> Laporan </span> <span class="menu-arrow"></span> </a>
        <ul class="list-unstyled">
            <li><a href="{{ url('akuntansi/laporan') }}">Daftar Laporan</a></li>
            <li><a href="#">Laporan Keuangan Kustomisasi</a></li>
            <li><a href="#">SPT Tahunan</a></li>
            <li><a href="#">PPN / PPN BM</a></li>
            <li><a href="#">Laporan Tersimpan</a></li>
            <li><a href="#">Laporan Terdesain</a></li>
            <li><a href="#">Berkas Laporan (*.fr3)</a></li>
        </ul>
    </li>
@endrole