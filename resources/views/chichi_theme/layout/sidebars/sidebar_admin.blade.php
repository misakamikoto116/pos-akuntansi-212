@role('admin')
	<li class="has_sub">
	    <a href="{{ url('admin') }}" class="waves-effect"><i class="ti-home"> </i> <span> Beranda </span></a>
	</li>
    <li>
        <a href="{{ url('admin/user') }}" class="waves-effect"><i class=" ti-user"> </i> <span> Data User </span></a>
    </li>
    <li>
        <a href="{{ url('admin/permission') }}" class="waves-effect"><i class=" ti-key"> </i> <span>Data Permission </span></a>
    </li>
    <li>
        <a href="{{ url('admin/role') }}" class="waves-effect"><i class=" ti-unlock"> </i> <span>Data Role </span></a>
    </li>
@endrole