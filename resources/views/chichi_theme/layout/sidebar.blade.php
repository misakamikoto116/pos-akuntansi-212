<div class="navbar navbar-custom">
    <div class="container">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                @role('akuntansi')
                    <li>
                        <a href="{{ url('akuntansi') }}"><i class="ti-home"></i>Beranda</a>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class=" ti-settings"></i>Master</a>
                        <ul class="submenu">
                            <li><a href="{{ route('akuntansi.identitas.edit', chichi_identitas_id() ?? 1) }}">Info Perusahaan</a></li>
                            <li><a href="{{ url('akuntansi/preferensi') }}">Preferensi</a></li>
                            <li><a href="{{ url('akuntansi/preferensi-pos') }}">Preferensi POS</a></li>
                            <li><a href="#">Data Pengguna</a></li>
                            <li><a href="{{ url('akuntansi/syarat-pembayaran') }}">Syarat Pembayaran</a></li>
                            <li><a href="{{ url('akuntansi/tipe-pelanggan') }}">Tipe Pelanggan</a></li>
                            <li><a href="{{ url('akuntansi/cart-log') }}">Cart Log</a></li>
                            <li><a href="{{ url('akuntansi/type-edc') }}">Type EDC</a></li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class=" ti-files"></i>Import</a>
                        <ul class="submenu">
                        	<li><a href="{{ route('akuntansi.get.produk-upload') }}">Import Produk</a></li>
                        	<li><a href="{{ route('akuntansi.get.produk-upload-by-akun') }}">Import Produk By Akun</a></li>
                        	<li><a href="{{ route('akuntansi.get.pelanggan-upload') }}">Import Pelanggan</a></li>
                        	<li><a href="{{ route('akuntansi.get.pemasok-upload') }}">Import Pemasok</a></li>
                        	<li><a href="{{ route('akuntansi.get.akun-upload') }}">Import Akun</a></li>
                            <li><a href="{{ route('akuntansi.get.jasa-pengiriman-upload') }}">Import Jasa Pengiriman</a></li>
                        	<li><a href="{{ route('akuntansi.get.syarat-pembayaran-upload') }}">Import Syarat Pembayaran</a></li>
                            <li><a href="{{ route('akuntansi.get.import-barcode') }}">Import Barcode</a></li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class=" ti-cloud-down"></i>Berkas</a>
                        <ul class="submenu">
                            <li><a href="#">Data Baru</a></li>
                            <li><a href="#">Buka Data</a></li>
                            <li><a href="#">Menutup Data</a></li>
                            <li><a href="#">Cadangan Data</a></li>
                            <li><a href="#">Import Jurnal</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#modal-tutup-buku">Tutup Buku Perusahaan</a></li>
                            <li><a href="#">Export Import Transaksi</a></li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class=" ti-agenda"></i>Buku Besar</a>
                        <ul class="submenu">
                            <li><a href="{{ url('akuntansi/history-buku-besar') }}">History Buku Besar</a></li>
                             <li><a href="{{ url('akuntansi/saldo-akun') }}">Saldo Akun</a></li>
                            <li><a href="{{ url('akuntansi/tipe-akun') }}">Tipe Akun</a></li>
                            <li><a href="{{ url('akuntansi/akun') }}">Daftar Akun</a></li>
                            <li><a href="{{ url('akuntansi/mata-uang') }}">Mata Uang</a></li>
                            <li><a href="{{ url('akuntansi/jurnal-umum') }}">Bukti Jurnal Umum</a></li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class=" ti-money"></i>Kas & Bank</a>
                        <ul class="submenu">
                            <li><a href="{{ route('akuntansi.buku_bank.show') }}">Buku Bank</a></li>
                            <li><a href="{{ url('akuntansi/pembayaran') }}">Pembayaran</a></li>
                            <li><a href="{{ url('akuntansi/penerimaan') }}">Penerimaan</a></li>
                            <li><a href="{{ url('akuntansi/rekonsiliasi-bank') }}">Rekonsiliasi Bank</a></li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class=" ti-server"></i>Daftar</a>
                        <ul class="submenu">
                            <li class="has-submenu">
                                <a href="#">Barang & Jasa</a>
                                <ul class="submenu">
                                    <li><a href="{{ url('akuntansi/barang') }}">Daftar Barang & Jasa</a></li>
                                    <li><a href="{{ url('akuntansi/serial-number') }}">Pencatatan Nomor Serial</a></li>
                                    <li><a href="{{ url('akuntansi/kategori-produk') }}">Kategori Barang</a></li>
                                    <li><a href="{{ url('akuntansi/penyesuaian-persediaan') }}">Penyesuaian Persediaan</a></li>
                                    <li><a href="{{ url('akuntansi/gudang') }}">Daftar Gudang</a></li>
                                    <li><a href="{{ url('akuntansi/barang-pergudang') }}">Barang Pergudang</a></li>
                                    <li><a href="{{ url('akuntansi/pemindahan-barang') }}">Pemindahan Barang</a></li>
                                    {{-- <li><a href="{{ url('akuntansi/penyesuaian-harga-jual') }}">Penyesuaian Harga Jual</a></li> --}}
                                </ul>
                            </li>
                            <li class="has-submenu">
                                <a href="#">Aktiva Tetap</a>
                                <ul class="submenu">
                                    <li><a href="{{ url('akuntansi/tipe-aktiva-tetap-pajak') }}">Tipe Aktiva Tetap Pajak</a></li>
                                    <li><a href="{{ url('akuntansi/tipe-aktiva-tetap') }}">Tipe Aktiva Tetap</a></li>
                                    <li><a href="{{ url('akuntansi/daftar-aktiva-tetap') }}">Daftar Aktiva Tetap</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ url('akuntansi/informasi-pemasok') }}">Pemasok</a></li>
                            <li><a href="{{ url('akuntansi/informasi-pelanggan') }}">Pelanggan</a></li>
                            <li><a href="{{ url('akuntansi/jasa-pengiriman') }}">Jasa Pengiriman</a></li>
                            <li class="has-submenu">
                            <a href="#">Penyelesaian Pesanan</a>
                            <ul class="submenu">
                                <li><a href="{{ url('akuntansi/pembiayaan-pesanan') }}">Pembiayaan Pesanan</a></li>
                                <li><a href="{{ url('akuntansi/penyelesaian-pesanan') }}">Penyelesaian Pesanan</a></li>
                            </ul>
                            <li><a href="{{ url('akuntansi/promosi') }}">Promosi</a></li>
                        </li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class="ti-stats-up"></i>Aktivitas</a>
                        <ul class="submenu megamenu" style="padding-right: 30px;">
                            <li>
                                <ul>
                                    <li>
                                        <span>Penjualan</span>
                                    </li>
                                    <li><a href="{{ url('akuntansi/penawaran-penjualan') }}">Penawaran Penjualan</a></li>
                                    <li><a href="{{ url('akuntansi/pesanan-penjualan') }}">Pesanan Penjualan</a></li>
                                    <li><a href="{{ url('akuntansi/pengiriman-penjualan') }}">Pengiriman Pesanan</a></li>
                                    <li><a href="{{ url('akuntansi/faktur-penjualan') }}">Faktur Penjualan</a></li>
                                    <li><a href="{{ url('akuntansi/penerimaan-penjualan') }}">Penerimaan Penjualan</a></li>
                                    <li><a href="{{ url('akuntansi/retur-penjualan') }}">Retur Penjualan</a></li>
                                    <li><a href="{{ url('akuntansi/uang-muka-pelanggan') }}">Uang Muka Pelanggan</a></li>
                                </ul>
                            </li>
                            <li>
                                <ul>
                                    <li>
                                        <span>Pembelian</span>
                                    </li>
                                    <li><a href="{{ url('akuntansi/permintaan-pembelian') }}">Permintaan Barang</a></li>
                                    <li><a href="{{ url('akuntansi/pesanan-pembelian') }}">Pesanan Pembelian</a></li>
                                    <li><a href="{{ url('akuntansi/penerimaan-barang') }}">Penerimaan Barang</a></li>
                                    <li><a href="{{ url('akuntansi/faktur-pembelian') }}">Faktur Pembelian</a></li>
                                    <li><a href="{{ url('akuntansi/pembayaran-pembelian') }}">Pembayaran Pembelian</a></li>
                                    <li><a href="{{ url('akuntansi/retur-pembelian') }}">Retur Pembelian</a></li>
                                    <li><a href="{{ url('akuntansi/uang-muka-pemasok') }}">Uang Muka Pemasok</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class=" md-layers"></i>Perusahaan</a>
                        <ul class="submenu">
                            <li class="has-submenu">
                                <a href="#">Karyawan</a>
                                <ul class="submenu">
                                    <li><a href="{{url('akuntansi/data-karyawan')}}">Data Karyawan</a></li>
                                    <li><a href="{{url('akuntansi/pemberitahuan')}}">Pemberitahuan</a></li>
                                </ul>
                            </li>
                            <li class="has-submenu">
                                <a href="#">Lowongan</a>
                                <ul class="submenu">
                                    <li><a href="#">Data Lowongan</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Absensi</a></li>
                            <li><a href="#">Pelacakan</a></li>
                            <li class="has-submenu">
                                <a href="#">Pajak</a>
                                <ul class="submenu">
                                    <li><a href="{{ url('akuntansi/kode-pajak') }}">Kode Pajak</a></li>
                                    <li><a href="{{ url('akuntansi/nomor-pajak') }}">Nomor Pajak</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{ route('akuntansi.upload.stiker.index') }}">Upload Image Stiker</a>
                            </li>
                            <li><a href="{{ url('akuntansi/display-iklan') }}">Iklan Display</a></li>

                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class=" md-layers"></i>Rekap Transaksi</a>
                        <ul class="submenu">
                            <li><a href="{{ route('akuntansi.penjualan.rekap.transaksi') }}">Rekap Transaksi Pos Harian</a></li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class=" md-my-library-books"></i>Laporan</a>
                        <ul class="submenu">
                            <li><a href="{{ url('akuntansi/laporan') }}">Daftar Laporan</a></li>
                            <li><a href="#">Laporan Keuangan Kustomisasi</a></li>
                            <li><a href="#">SPT Tahunan</a></li>
                            <li><a href="#">PPN / PPN BM</a></li>
                            <li><a href="#">Laporan Tersimpan</a></li>
                            <li><a href="#">Laporan Terdesain</a></li>
                            <li><a href="#">Berkas Laporan (*.fr3)</a></li>
                        </ul>
                    </li>
                @endrole

                @role('pos')
                    <li>
                        <a href="{{ url('pos') }}"><i class="ti-home"></i>Beranda</a>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class=" ti-settings"></i>Kasir</a>
                        <ul class="submenu">

                            @if ( ! session()->get('kasir_cek', false) )
                                <li><a href="{{ route('pos.penjualan.kasir.check.in') }}">Cek In</a></li>
                            @else
                                <li><a href="#" data-toggle="modal" data-target="#modal-check-out">Cek Out</a></li>
                            @endif
                                {{-- <li><a target="_blank" href="{{ route('pos.pos.display.kasir') }}">Pesanan</a></li> --}}
                                <li><a target="_blank" href="{{ route('pos.pos.display.kasir') }}">Display Kasir</a></li>
                        </ul>
                    </li>
                    
                    <li>
                        <a href="{{ url('pos/penjualan') }}"><i class=" ti-money"></i>Penjualan</a>
                    <li>
                        <a href="{{ url('pos/promosi') }}"><i class=" ti-gift"></i> Promosi</a>
                    </li>

                    <li>
                        <a href="{{ route('pos.penjualan.kasir.transaksi') }}"><i class=" ti-money"></i> Rekap Penjualan</a>
                    </li>

                    <li>
                        <a href="{{ url('pos/barang') }}"><i class=" ti-layout-width-default"></i>List Barang</a>
                    </li>

                    <li>
                        <a href="{{ url('pos/laporan') }}"><i class=" md-my-library-books"></i>Daftar Laporan</a>
                    </li>

                @endrole

                @role('admin')
                    <li>
                        <a href="{{ url('admin') }}"><i class="ti-home"></i>Beranda</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/user') }}"><i class=" ti-user"></i>Data User</a>
                    </li>

                    <li>
                        <a href="{{ url('admin/permission') }}"><i class=" ti-key"></i>Data Permission</a>
                    </li>

                    <li>
                        <a href="{{ url('admin/role') }}"><i class=" ti-unlock"></i>Data Role</a>
                    </li>
                @endrole
                
                @role('gudang')
                    <li>
                        <a href="{{ url('gudang') }}"><i class="ti-home"></i>Beranda</a>
                    </li>
                    <li>
                        <a href="{{ url('gudang/barang') }}"><i class="ti-server"></i>Daftar Barang & Jasa</a>
                    </li>
                    <li>
                        <a href="{{ url('gudang/serial-number') }}"><i class="ti-notepad"></i>Pencatatan Nomor Serial</a>
                    </li>
                    <li>
                        <a href="{{ url('gudang/kategori-produk') }}"><i class="ti-clipboard"></i>Kategori Barang</a>
                    </li>
                    <li class="has-submenu">
                        <a href="#"><i class="ti-world"></i>Gudang</a>
                        <ul class="submenu">
                            <li><a href="{{ url('gudang/daftar-gudang') }}">Daftar Gudang</a></li>
                            <li><a href="{{ url('gudang/barang-pergudang') }}">Barang Pergudang</a></li>
                            <li><a href="{{ url('gudang/pemindahan-barang') }}">Pemindahan Barang</a></li>
                        </ul>
                    </li>
                    <li class="has-submenu">
                        <a href="#"><i class="ti-package"></i>Penyesuaian Gudang</a>
                        <ul class="submenu">
                            <li><a href="{{ url('gudang/penyesuaian-persediaan') }}">Penyesuaian Persediaan</a></li>
                            <li><a href="{{ url('gudang/penyesuaian-harga-jual') }}">Penyesuaian Harga Jual</a></li>
                        </ul>
                    </li>
                @endrole
            </ul><!-- End Navigation -->
        </div>
    </div>
</div>