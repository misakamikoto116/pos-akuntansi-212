


        <script>
            var resizefunc = [];
        </script>

                <!-- jQuery  -->
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
        <script type="text/javascript">
          var BASE_URL = "{{ url('/') }}/";
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
         </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
        <script>
       
         @if(auth()->check())
            var GROUP = "{{ auth()->user()->roles()->first()->name }}";
            var KASIR = "{!! 'kasir_'.auth()->user()->id !!}";
         @endif
            $(document).ready(function(){
                $('#btn-submit').attr('disabled',false);
                //let buttons = document.getElementsByName('lanjutkan');
                //$.each(buttons, function(){
                //    this.onclick = function(event){ 
                //        playSound();
                //    }
                //});

                //$.each($('button').find(':submit').prevObject, function(){
                //    if(this.innerText == 'Simpan & Tutup'){
                //        this.onclick = function(event){ 
                            //event.preventDefault(); //delete this if you want to fix it
                //            playSound();
                            //setTimeout( function () {  //delete this if you want to fix it
                            //    $('.form-horizontal').submit(); //delete this if you want to fix it
                            // }, 1000); //delete this if you want to fix it.
                            // also check on app.blade, and remove beep-01. change it to beep-07
                //        }
                        
                //    }
                //});
            });

            function playSound(){
                // let sound = document.getElementById("audio-btn");
                // sound.play();
            }

            function playSoundErr(){
                // let sound = document.getElementById("audio-btn-err");
                // sound.play();
            }
            
        </script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="{{asset('assets/js/tether.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('assets/js/detect.js') }}"></script>
        <!-- Typeahead.js Bundle -->
        <script src="{{asset('js/typeahead.bundle.js')}}"></script>
        {{-- <script src="{{asset('assets/js/typeahead.bundle.js')}}"></script> --}}
        <script src="{{asset('assets/js/fastclick.js')}}"></script>
        <script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
        <script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
        <script src="{{asset('assets/js/waves.js')}}"></script>
        <script src="{{asset('assets/js/wow.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
        <script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>
        <script src="{{ asset('assets/plugins/notifyjs/js/notify.js') }}"></script>
        <script src="{{ asset('assets/plugins/notifications/notify-metro.js') }}"></script>
        {{-- <script src="{{ asset('assets/plugins/nestable/jquery.nestable.js') }}"></script> --}}
        {{-- <script src="{{ asset('assets/pages/nestable.js') }}"></script> --}}

        <script src="{{asset('assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')}}"></script>
        <script src="{{asset('assets/plugins/switchery/js/switchery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/plugins/multiselect/js/jquery.multi-select.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/plugins/jquery-quicksearch/jquery.quicksearch.js')}}"></script>
        <script src="{{ asset('assets/plugins/select2/js/select2.full.js') }}" type="text/javascript"></script>
        {{-- <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
        <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>

        <script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
        <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.js')}}"></script>
        <script src="{{asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
        <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js')}}"></script>
        <script src="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

        <script type="text/javascript" src="{{asset('assets/pages/jquery.form-advanced.init.js')}}"></script>

        <script src="{{asset('assets/js/jquery.core.js')}}"></script>
        <script src="{{asset('assets/js/jquery.app.js')}}"></script>
        <script src="{{asset('assets/js/sweetalert.js')}}"></script>
        <script src="{{ asset('assets/plugins/autoNumeric/autoNumeric.js') }}" type="text/javascript"></script>

        <script src="{{ asset('assets/plugins/peity/jquery.peity.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
        <script src="{{ asset('assets/plugins/counterup/jquery.counterup.min.js') }}"></script>

        <script src="{{ asset('assets/plugins/morris/morris.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/raphael/raphael-min.js') }}"></script>

        <script src="{{ asset('assets/plugins/jquery-knob/jquery.knob.js') }}"></script>


        <script src="{{asset('assets/js/icheck.js')}}"></script>

        <script type="text/javascript">
            
          // const socket = io("{{ env('SOCKET_URL', '/') }}", {
          //     path: '/node/socket.io',
          //   //   namespace: '/node/',
          //   //   secure:location.protocol === 'https:',
          //   //   rejectUnauthorized : false
          //   // //  transports: ['polling']
          // });

          // const socket = io("{{ env('SOCKET_URL','/') }}");
          

          // socket.on(GROUP, function (data) {
          //     $.Notification.autoHideNotify('custom', 'top right', "Pesan", data);
          // });

          // socket.on(KASIR, function (data) {
          //     $.Notification.autoHideNotify('custom', 'top right', "Pesan", data);
          // });
          jQuery(function($) {
              $('.mask').autoNumeric('init', {aForm:false, aPad: false});
          });

        var url_get_started               = $(location).attr('href');
        var split_segment_get_started     = url_get_started.split('/');
        var last_segment_get_started      = split_segment_get_started.pop();

        if (last_segment_get_started != 'get-started') {
            $('.select2').select2();
        }

          $('.selectpicker').selectpicker();
            
        </script>

        <script type="text/javascript">
            $('.tanggal').datepicker({format: 'dd MM yyyy', autoclose: true});
            $('.tanggal').datepicker('setDate', new Date());

            $('.month-only').datepicker({
                format: "mm-yyyy",
                viewMode: "months", 
                minViewMode: "months",
                autoclose: true
            });
        </script>

        <script>
                $(document).ready(function() {
                    @if(auth()->check())
                        if ("{{ auth()->user()->roles()->pluck('name')->first() }}" == 'akuntansi') {
                            $('body').on('keydown', 'input, select, textarea', function (e) {
                                var self = $(this)
                                , form = self.parents('form:eq(0)')
                                , focusable
                                , next
                                ;
                                var keyCode = e.keyCode || e.which;
                                if (keyCode === 13 ) {
                                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                                    next = focusable.eq(focusable.index(this)+1);
                                    if (next.length) {
                                        next.focus();
                                    } else {
                                        form.submit();
                                    }
                                    return false;
                                }
                            });
                        }
                    @endif
                    
                    var route = "{{ route('pos.penjualan.index') }}";
                    var url_sekarang = document.URL;
                    
                    setTimeout(function(){
                    //     var logoWidth = $('div.logo').width();
                    //     var marginCompressed = logoWidth - 158;
                        if (url_sekarang == route) {
                            $('body').removeClass('fixed-left');
                            $('body').addClass('fixed-left-void');
                            $('#wrapper').addClass('enlarged');
                    //         $(window).on("scroll", function() {
                    //             if ($(window).scrollTop() >= 35) {
                    //                 if($(".navbar-toggle").hasClass("open")){
                    //                     $(".navbar-toggle").removeClass("open");
                    //                 }
                    //                 $(".navbar").removeClass("compressed");
                    //                 $(".navbar").addClass("compressed")
                    //                 $('.compressed > .container').attr('style', 'margin : 5px auto !important;');
                    //                 $('.compressed .navigation-menu li').attr('style', 'margin: 0px -5px !important;');
                    //             } else {
                    //                 $('.compressed > .container').removeAttr('style');
                    //                 $('.compressed .navigation-menu li').removeAttr('style');
                    //                 $(".navbar").addClass("compressed");
                    //                 $(".navbar").removeClass("compressed");
                    //             }
                    //         });
                        }
                    //     else {
                    //         $(".navbar").addClass('compressed');
                    //     }
                    },300);
                    showPage();

                });

            function showPage() {
                if ($("#page-loader").length > 0) {
                    document.getElementById("page-loader").style.display = "none";
                    document.getElementById("page-loader-text").style.display = "none";
                    $('#page-show').show(300);
                }
            }

            function hidePage() {
                $('#page-show').hide(300);
                document.getElementById("page-loader").style.display = "block";
                document.getElementById("page-loader-text").style.display = "block";
            }
        </script>
        <script>
            $(document).ready(function(){
                // Refresh
                var ObjRefresh = $('<a href="#" onclick="location.reload()" class="btn btn-info btn-rounded waves-effect waves-light"><span class="btn-label"><i class="fa fa-refresh"></i></span>Refresh</a>');
                $('.menu-header').prepend(ObjRefresh);
                
                @if(auth()->check())
                //     if ("{{ auth()->user()->roles()->pluck('name')->first() }}" == 'akuntansi') {
                //         $('a').attr('target','_blank');
                //     }
                @endif
                $('a[href^="#"]').on('click',function (e) {
                    e.preventDefault();
                    var target = this.hash,
                    $target = $(target);
                });
            });
        </script>
        <script>
            // $(window).bind('scroll', function () {
            //     if ($(window).scrollTop() > 120) {
            //         $('.card-header').addClass('fixch transre');
            //     } else {
            //         $('.card-header').removeClass('fixch');
            //     }
            // });
        </script>

        <script>
            $('body').on('click', '#btn-check-out', function (e) {                
                if ($("#uang-fisik").val() !== null && $("#uang-fisik").val() !== "") {
                    $.ajax({
                        url: '{{ route("pos.penjualan.check-out-kasir") }}',
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            uang_fisik: $("#uang-fisik").val(),
                        },
                        success: function (result) {
                                if ($.isFunction(window.printCheckOut)) {
                                    window.printCheckOut(result)
                                    window.location.replace("{{ route('logout') }}");
                                }else{

                                    // console.log(result);
                                    // Object.entries(result).map(e => e.join('=')).join('&')
                                    window.open(result.url)

                                    // // print thermal local
                                    // $.ajax({
                                    //     url: 'http://localhost:8901/print-local-checkout',
                                    //     type: 'GET',
                                    //     dataType: 'JSON',
                                    //     data: {
                                    //         struk: result, 
                                    //     },
                                    // })
                                    // .done(function() {
                                    //     console.log("success");
                                    // })
                                    // .fail(function() {
                                    //     console.log("error");
                                    // })
                                    // .always(function() {
                                    //     console.log("complete");
                                    // });

                                    window.location.replace("{{ route('logout') }}");
                                  // window.open("http://localhost:8901/print-local-checkout?data="+encodeURIComponent(JSON.stringify(result)));

                                }
                        },
                        error: function (err) {
                            console.log(err)
                        }
                    });
                }else {
                    alert('Uang fisik tidak boleh kosong');
                }
            });         
            function returFocus(){
                setTimeout(function(){
                    $('.form-focus').focus();
                }, 400);
            }
        </script>

        <script type="text/javascript">
            function changeCheckBoxFilterMenu() {
                if ($("#button-all-checked").length > 0) {
                    $('.checkbox-filter-menu').iCheck('uncheck');
                    $('.button-checkbox-filter-menu').removeAttr('id');
                    $('.button-checkbox-filter-menu').attr('id','button-all-unchecked');
                    $('.button-checkbox-filter-menu').removeClass('btn-success');
                    $('.button-checkbox-filter-menu').addClass('btn-warning');
                }else {
                    $('.checkbox-filter-menu').iCheck('check');
                    $('.button-checkbox-filter-menu').removeAttr('id');
                    $('.button-checkbox-filter-menu').attr('id','button-all-checked');
                    $('.button-checkbox-filter-menu').removeClass('btn-warning');
                    $('.button-checkbox-filter-menu').addClass('btn-success');
                }
            }
        </script>

        <script type="text/javascript">
            $(document).ready(function($) {
                var height_tbody = $('div tbody').height();
                if (height_tbody > 600) {
                    $('.table').find('.btn-group').css("position","relative");
                }else if(height_tbody < 599) {
                    $('.table').find('.btn-group').css("position","absolute");
                }   
            });
        </script>

        <!-- Modal Check Out -->
        <div class="modal fade" id="modal-check-out" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Check Out Kasir</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <table id="demo-foo-filtering" class="table table-bordered">
                    <tr>
                        <td>Uang Fisik</td>
                        <td>{!! Form::text('uang_fisik',null,['class' => 'form-control mask', 'id' => 'uang-fisik','required']) !!}</td>
                    </tr>
                </table>
              </div>
              <div class="modal-footer">
                <button type="button" id="btn-check-out" class="btn btn-default">Check Out</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>
              </div>
            </div>
          </div>
        </div>
        <!-- END Modal -->

        <!-- Retur Modal --> 
        <div id="ReturModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                {!! Form::open(['route' => 'pos.retur.check.transasction', 'method' => 'post']) !!}
                {!! Form::hidden('currentUrl', URL::current()) !!}
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Retur Penjualan</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-5 col-form-label" for="kasir_mesin">Scan nomor transaksi <span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            {!! Form::text('no_transaksi', null, ['class' => 'form-control form-focus','autofocus' => 'autofocus']) !!}
                        </div>
                    </div>   
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning waves-effect waves-light w-lg btn-check-in" type="submit">
                        <span>Submit</span>
                    </button> 
                </div>
                </div>
                {!! Form::close() !!}    
            </div>
        </div> 
        <!-- END Modal -->

        <!-- Modal Tutup Buku -->
        <div class="modal fade" id="modal-tutup-buku" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tutup Buku Perusahaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                
              </div>
              <div class="modal-footer">
                <button type="button" id="btn-check-out" class="btn btn-default">Tutup Buku Perusahaan</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger">Batal</button>
              </div>
            </div>
          </div>
        </div>
        <!-- END Modal -->