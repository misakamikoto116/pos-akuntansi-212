
        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="{{ asset('assets/plugins/morris/morris.css') }}">

        <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        {{-- <link href="{{ asset('assets/css/sidebar.css')}}" rel="stylesheet" type="text/css" /> --}}
        <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/_breadcrumb.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <link href="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
        <link href="{{ asset('assets/plugins/nestable/jquery.nestable.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css">
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="{{ asset('assets/js/modernizr.min.js')}}"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <style type="text/css">
        .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active {
            color: black !important;
        }
        .nav-pills li a{
            color: #618788 !important;
        }
        .card-body *{
            font-size: 13px;
        }
        .card-header h5{
            font-size: 16px !important;
        }
        .navbar-custom{
                /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#27393d+0,618788+80 */
                background: rgb(39,57,61); /* Old browsers */
                background: -moz-linear-gradient(-45deg, rgba(39,57,61,1) 0%, rgba(97,135,136,1) 80%); /* FF3.6-15 */
                background: -webkit-linear-gradient(-45deg, rgba(39,57,61,1) 0%,rgba(97,135,136,1) 80%); /* Chrome10-25,Safari5.1-6 */
                background: linear-gradient(135deg, rgba(39,57,61,1) 0%,rgba(97,135,136,1) 80%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#27393d', endColorstr='#618788',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
            }
            .button-menu-mobile {
                background-color: rgba(0,0,0,0.0);
            }
            .topbar .topbar-left{
                background-color: #283B3F;
            }
            #topnav .navbar-custom{
                box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
            }
            html{
                background: #f2f2f2;
            }
            body{
                background: inherit;
                padding-bottom: 60px;
            }
            /*.card-header{
                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
                z-index: 3;
                position: relative;
                background-color: #618788;
            }
            .card-header .title{
                color: white;
                font-size: 20px;
            }*/
            .page-title{
                display: inline-block;
                position: relative;
                top: 50%;
                transform: translateY(-100%);
            }
            #breadcrumb{
                float: right;   
            }
            .btn {
                box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
            }

            .btn:hover {
                box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15); 
                cursor: pointer;
            }
            .card-box {
                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
            }
            .card-header{
                z-index: 3;
                position: absolute;
                top: -30px;
                left: 50%;
                transform: translateX(-50%);
                background-color: #618788;
                width: 95%;
                box-shadow: 0 8px 17px 0 rgba(0,0,0,.12), 0 6px 20px 0 rgba(0,0,0,.12);
            }
            .card-header .title{
                color: white;
                font-size: 18px;
            }
            .card-header:first-child{
                border-radius: 8px;
            }
            .card{
                position: relative;
                padding-top: 40px;
                margin-top: 30px;
            margin-bottom: 30px;
                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
            }
            .menu-header{
                position: absolute;
                right: 10px;
                top: 50%;
                transform: translateY(-50%);
            }
            .card-body{
              padding: 20px;
              padding-top: 30px;
              margin-bottom: 20px;
              background-clip: padding-box;
              background-color: #ffffff;
            }
            .submit{
                float: right;
            }
            .form-control{
                height: auto;
            }
            select.form-control:not([size]):not([multiple]){
                height: auto;
            }
            .submit button{
                margin: 0 5px;
            }
            form .submit{
                float: right;
            }
            form .submit button{
                margin: 0 5px;
            }
            .select2-container--default .select2-selection--single .select2-selection__arrow {
                height: 100%;
                padding: 0 15px;
            }
            .input-group-addon{
                background: #94BBBC;
                border: 1px solid #94BBBC;
                color: white;
            }
            .btn-group .dropdown-menu{
                border: none;
                left: -5px;
                top: 98%;
                height: auto;
                padding: 0;
            }
            .btn-group .dropdown-item{
                padding: 5px 10px;
                padding-left: 20px;
                border-radius: 20px;
                margin: 5px;
                width: 90%;
                color: white;
                box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                transition: 0.5s;
            }
            .btn-group .dropdown-item:hover{
                color: white;
                cursor: pointer;
                box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
            }
            .btn-group .dropdown-menu .edit{
                background: #ffc107;
            }
            .btn-group .dropdown-menu .jurnal{
                background: #17a2b8;
            }
            .btn-group .dropdown-menu .deleteBtn{
                background: #dc3545;
            }
            .block-hide {
                display: none;
            }

            /*Topnav Active*/
           /*  #topnav .has-submenu.active a i{
                color: rgba(255, 255, 255, 0.7);
            } */
            /* #topnav .has-submenu.active a{
                color: rgba(255, 255, 255, 0.7);
                background-color: rgba(255, 255, 255, 0.0) !important;
            } */
            #topnav .navigation-menu > li > a{
                font-size: 12px;
            }
            #topnav .navigation-menu li .submenu li a{
                font-size: 13px;
            }
            /*End Topnav Active*/

            .notification-list .dropdown-menu{
                top: 50px;
                border: none;
                box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
            }
            .foot{
                font-size: 13px;
                box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                background: white;
                color: gray;
                padding: 15px;
                margin-top: 40px;
                position: absolute;
                bottom: 0;
                width: 100%;
            }
            #topnav .navigation-menu > li .submenu{
                border: none;
                box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
            }
            fieldset{
                margin-bottom: 40px;
                padding-left: 20px;
            }
            legend{
                font-weight: bold;
                font-size: 14px;
                position: relative;
                left: -15px;
                margin-bottom: 18px;
            }
            .warn{
                color: red;
            }
            .select2-container .select2-selection--single:nth-of-type(2){
                position: absolute;
            }
                        .scale *{ 
                font-size: 11px; 
            }
            .card{
                padding-top: 15px;
            }
            .card-header h5{
                font-size: 13px !important;
            }
            .form-control{
                height: 30px !important;
            }
            .select2-container .select2-selection--single{
                height: 30px !important;
            } 
            .select2-container .select2-selection--single .select2-selection__rendered{
                line-height: 27px !important;
            }
            .drop{
                height: 30px !important;
                overflow: hidden;
            }
           /* #topnav .navigation-menu > li .submenu{
                max-height: 600px;
                width:100%;
                overflow-y: auto;
            } */
            option{
                font-size: 13px !important;
            }
            .modal.show .modal-dialog .modal-dialog-centered{
                position: relative;
                top: 50%;
                transform: translate(0,-80%);
            }
            .modal.show .modal-dialog .modal-content{
                box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
            }
            .btn-sm{
                padding: .35rem .8rem;
            }
            @media screen and (max-width: 1007px){
                #topnav .topbar-main{
                    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                }
            }            
            .compressed {
                background: none !important;
                width : 100%;
                height: 75px;
                box-shadow: none !important;
                transition: all ease-in-out 1s !important;
                margin: -100px 0px!important;
                top: 0;
                right:0;
                left:0; 
                position : fixed;
            }
            .compressed > .container{
                position: fixed;
                top: 0;
                right:0;
                left:0;              
            }
            .compressed .navigation-menu li > a{
                color: black !important;
                margin: 5px -10px !important;
                padding: 0px 25px !important;
            }
            @media screen and (min-width: 400px) and (max-width: 1084px) {
                #navigation{
                    margin: 0px 0px;
                    position: fixed;
                    top: 0;
                    left: 0;
                    right: 0;
                }
                .navbar{
                    height: 70px;
                    /* background: none !important; */
                    box-shadow: none !important;
                }
                .navigation-menu li{
                    margin: 60px -5px;
                }
                li.has-submenu > ul > li,
                li.has-submenu,
                li.has-submenu > ul > li > ul > li {
                    margin: 0!important;
                    padding: 0!important;
                }
                .dripicons-expand{
                    display: none;
                }
                .compressed .navigation-menu li{
                    margin: 10px -5px!important;
                }
            }

            #topnav {
                border-bottom: 2px solid #283B3F;
            }

            .fixch:first-child{
                position: fixed;
                margin: 95px 0px;
                width:90%;
            }
            .transre{
                transition: all ease-in-out 0.4s !important;
            }
            .dropdown-menu li:first-child{
                padding:5px;
                padding-left: 10px;
                padding-right: 10px;
            }

            .dropdown-menu li{
                padding:5px; 
                padding-right: 15px;
                padding-left: 10px;
            }

            .dropdown-menu li:last-child{
                padding-left: 5px;
            }
            #page-show{
                display: none;
            }
            #page-loader {
                border: 16px solid #f3f3f3; /* Light grey */
                border-top: 16px solid #618788; /* Green */
                border-bottom: 16px solid #618788;
                border-radius: 50%;
                width: 120px;
                height: 120px;
                position:fixed;
                top: 40%;
                left: 45%;
                margin: auto;
                animation: spin 2s linear infinite;
            }

            #page-loader-text{
                position:fixed;
                top: 66%;
                left: 47%;
                margin: auto;
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }

            .form-focus:focus{
                box-shadow: 0 0 5px rgba(81, 203, 238, 1);
                padding: 3px 0px 3px 3px;
                margin: 5px 1px 3px 0px;
                border: 1px solid rgba(81, 203, 238, 1);
            }
        </style>
