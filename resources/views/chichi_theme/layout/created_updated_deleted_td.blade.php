<td>{{ is_array($item) ? $item['created_at'] : \Carbon\Carbon::parse($item->created_at)->format('d F Y') }}</td>
<td>{{ is_array($item) ? $item['updated_at'] : \Carbon\Carbon::parse($item->updated_at)->format('d F Y') }}</td>
<td>{{ is_array($item) ? $item['created_by'] : $item->user_created }}</td>
<td>{{ is_array($item) ? $item['updated_by'] : $item->user_updated }}</td>
