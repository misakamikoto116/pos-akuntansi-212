<div class="navbar navbar-custom">
    <div class="container">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
            
                <li>
                    <a href="{{ url('pos') }}"><i class="ti-home"></i>Beranda</a>
                </li>

                <li>
                    <a href="{{ route('pos.penjualan.index') }}"><i class=" ti-settings"></i>Penjualan</a>
                </li>

            </ul><!-- End Navigation -->
        </div>
    </div>
</div>