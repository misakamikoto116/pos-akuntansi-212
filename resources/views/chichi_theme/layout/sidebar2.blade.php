            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li>
                                <a href="{{ url('dashboard') }}"><i class="ti-home"></i>Beranda</a>
                            </li>

                            {{-- <li class="has-submenu">
                                <a href="#"><i class=" ti-bookmark-alt"></i>Data Master</a>
                                <ul class="submenu">
                                    <li><a href="{{ url('akun') }}">Akun</a></li>
                                    <li><a href="{{ url('tipe-akun') }}">Tipe Akun</a></li>
                                    <li><a href="{{ url('mata-uang') }}">Mata Uang</a></li>
                                </ul>
                            </li> --}}

                            <li class="has-submenu">
                                <a href="#"><i class=" md-view-headline"></i>Berkas</a>
                                <ul class="submenu">
                                    <li><a href="#">Data Baru</a></li>
                                    <li><a href="#">Membuka Data</a></li>
                                    <li><a href="#">Menutup Data</a></li>
                                    <li><a href="#">Simpan Dengan Alias</a></li>
                                    <li><a href="#">Cadangan Data</a></li>
                                    <li><a href="#">Pakai Data Cadangan</a></li>
                                    <li><a href="#">Perbaikan Data</a></li>
                                    <li><a href="#">Review Akuntan</a></li>
                                    <li><a href="#">Impor Jurnal</a></li>
                                    <li><a href="#">Batal Impor Jurnal</a></li>
                                    <li><a href="#">Tutup Buku Perusahaan</a></li>
                                    <li><a href="#">Expor Impor Transaksi</a></li>
                                    <li><a href="#">Buat Data Cabang Baru</a></li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><i class=" md-view-headline"></i>Persiapan</a>
                                <ul class="submenu">
                                    <li><a href="{{ url('identitas') }}">Info Perusahaan</a></li>
                                    <li><a href="#">Preferensi</a></li>
                                    <li><a href="#">Profil Pengguna</a></li>
                                    <li><a href="{{ url('edit-password') }}">Ubah Kata Kunci</a></li>
                                    <li><a href="#">Persiapan Singkat</a></li>
                                    <li><a href="#">Rancangan Formulir</a></li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><i class=" md-view-headline"></i>Daftar</a>
                                    <ul class="submenu">
                                        <li><a href="{{ url('tipe-akun') }}">Tipe Akun</a></li>
                                        <li><a href="{{ url('mata-uang') }}">Mata Uang</a></li>
                                        <li><a href="{{ url('akun') }}">Daftar Akun</a></li>
                                        <li class="has-submenu">
                                            <a href="#">Departemen</a>
                                        <ul class="submenu">
                                                <li><a href="#"><span>Upah Pekerja</span></a></li>
                                                <li><a href="#"><span>Peny. Anggaran & Upah Proyek</span></a></li>
                                                <li><a href="#"><span>Peny. Anggaran & Harga Bahan</span></a></li>
                                                <li><a href="#"><span>Kategori AHP</span></a></li>
                                                <li><a href="#"><span>Analisa Harga Kerja</span></a></li>
                                                <li><a href="#"><span>Proyek</span></a></li>
                                                <li><a href="#"><span>Bahan Baku Terpakai</span></a></li>
                                                <li><a href="#"><span>Survei Proyek</span></a></li>
                                                <li><a href="#"><span>Kuantitas & Kontrol Biaya</span></a></li>
                                                <li><a href="#"><span>Penyelesaian Proyek</span></a></li>
                                            </ul>
                                        </li>
                                        <li class="has-submenu">
                                        <a href="#">Pabrikasi</a>
                                        <ul class="submenu">
                                            <li><a href="#"><span>Penanggung Jawab</span></a></li>
                                            <li><a href="#"><span>Departemen Produksi</span></a></li>
                                            <li><a href="#"><span>Biaya-biaya Produksi</span></a></li>
                                            <li><a href="#"><span>Harga Standar Persediaan</span></a></li>
                                            <li><a href="#"><span>Daftar Standar Biaya Produksi</span></a></li>
                                            <li><a href="#"><span>Formula Produk</span></a></li>
                                            <li><a href="#"><span>Surat Perintah Kerja</span></a></li>
                                            <li><a href="#"><span>Pengeluaran Bahan Baku</span></a></li>
                                            <li><a href="#"><span>Produk dan Bahan Baku Keluaran</span></a></li>
                                            <li><a href="#"><span>Status Produk dan Bahan Baku</span></a></li>
                                            <li><a href="#"><span>Jadwal Permintaan Produksi</span></a></li>
                                            <li><a href="#"><span>Jadwal Ketersediaan Bahan Baku</span></a></li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#">Buku Besar</a>
                                        <ul class="submenu">
                                            <li><a href="{{ url('jurnal-umum') }}"><span>Bukti Jurnal Umum</span></a></li>
                                            <li><a href="#"><span>Histori Buku Besar</span></a></li>
                                            <li><a href="#"><span>Saldo Akun</span></a></li>
                                            <li><a href="#"><span>Anggaran Akun</span></a></li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#">Kas & Bank</a>
                                        <ul class="submenu">
                                            <li><a href="{{ route('buku_bank.show') }}"><span>Buku Bank</span></a></li>
                                            <li><a href="{{ url('pembayaran') }}"><span>Pembayaran</span></a></li>
                                            <li><a href="{{ url('penerimaan') }}"><span>Penerimaan</span></a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{ url('informasi-pelanggan') }}">Pelanggan</a></li>
                                    <li class="has-submenu">
                                        <a href="#">Penjualan</a>
                                        <ul class="submenu">
                                            <li><a href="#"><span>Penawaran Penjualan</span></a></li>
                                            <li><a href="#"><span>Pesanan Penjualan</span></a></li>
                                            <li><a href="#"><span>Pengiriman Pensanan</span></a></li>
                                            <li><a href="#"><span>Faktur Penjualan</span></a></li>
                                            <li><a href="#"><span>Retur Penjualan</span></a></li>
                                            <li><a href="#"><span>Penerimaan Penjualan</span></a></li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#">Otorisasi Pengembalian Barang</a>
                                        <ul class="submenu">
                                            <li><a href="#"><span>Histori Klaim</span></a></li>
                                            <li><a href="#"><span>Aktifitas Proses Klaim</span></a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Pemasok</a></li>
                                    <li class="has-submenu">
                                        <a href="#">Pembelian</a>
                                        <ul class="submenu">
                                            <li><a href="#"><span>Permintaan Barang</span></a></li>
                                            <li><a href="#"><span>Pesanan Pembelian</span></a></li>
                                            <li><a href="#"><span>Penerimaan Barang</span></a></li>
                                            <li><a href="#"><span>Faktur Pembelian</span></a></li>
                                            <li><a href="#"><span>Retur Pembelian</span></a></li>
                                            <li><a href="#"><span>Pembayaran Pembelian</span></a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Barang dan Jasa</a></li>
                                    <li><a href="#">Pembiayaan Pesanan</a></li>
                                    <li class="has-submenu">
                                        <a href="#">Persediaan</a>
                                        <ul class="submenu">
                                            <li><a href="#"><span>Pencatatan Nomor Serial</span></a></li>
                                            <li><a href="{{ url('gudang') }}"><span>Daftar Gudang</span></a></li>
                                            <li><a href="#"><span>Barang per Gudang</span></a></li>
                                            <li><a href="#"><span>Pemindahan Barang</span></a></li>
                                            <li><a href="#"><span>Penyesuaian Persediaan</span></a></li>
                                            <li><a href="#"><span>Kategori Barang</span></a></li>
                                            <li><a href="#"><span>Penyesuaian Harga Jual</span></a></li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#">Aktiva Tetap</a>
                                        <ul class="submenu">
                                            <li><a href="{{ url('tipe-aktiva-tetap-pajak') }}"><span>Tipe Aktiva Tetap Pajak</span></a></li>
                                            <li><a href="{{ url('tipe-aktiva-tetap') }}"><span>Tipe Aktiva Tetap</span></a></li>
                                            <li><a href="#"><span>Daftar Aktiva Tetap</span></a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Transaksi berulan</a></li>
                                    <li><a href="#">Salinan Transaksi</a></li>
                                    <li class="has-submenu">
                                        <a href="#"><span>Daftar Lain</span></a>
                                        <ul class="submenu">
                                            <li><a href="{{ url('kode-pajak') }}"><span>Kode Pajak</span></a></li>
                                            <li><a href="{{ url('nomor-pajak') }}"><span>Nomor Pajak</span></a></li>
                                            <li><a href="{{ url('syarat-pembayaran') }}"><span>Syarat Pembayaran</span></a></li>
                                            <li><a href="{{ url('jasa-pengeriman') }}"><span>Jasa Pengiriman</span></a></li>
                                            <li><a href="{{ url('tipe-pelanggan') }}"><span>Tipe Pelanggan</span></a></li>
                                        </ul>
                                    </li>
                                        </ul>
                                    </li>

                            <li class="has-submenu">
                                <a href="#"><i class=" md-view-headline"></i>Aktifitas</a>
                                <ul class="submenu">
                                    <li class="has-submenu">
                                        <a href="#">Buku Besar</a>
                                        <ul class="submenu">
                                            <li><a href="{{ url('jurnal-umum') }}"><span>Bukti Jurnal Umum</span></a></li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#">Kas & Bank</a>
                                        <ul class="submenu">
                                            <li><a href="{{ url('pembayaran') }}"><span>Pembayaran</span></a></li>
                                            <li><a href="{{ url('penerimaan') }}"><span>Penerimaan</span></a></li>
                                            <li><a href="{{ route('rekonsiliasi_bank.show') }}"><span>Rekonsiliasi Bank</span></a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Penjualan</a></li>
                                    <li><a href="#">Otorisasi Pengembalian Barang</a></li>
                                    <li><a href="#">Pembelian</a></li>
                                    <li><a href="#">Pabrikasi</a></li>
                                    <li><a href="#">Barang dan Jasa</a></li>
                                    <li><a href="#">Proyek</a></li>
                                    <li><a href="#">Pembiayaan Pesanan</a></li>
                                    <li><a href="#">Aktiva Tetap Baru</a></li>
                                    <li><a href="#">Periodik</a></li>
                                    <li><a href="#">Lain-lain</a></li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><i class=" md-my-library-books"></i>Laporan</a>
                                <ul class="submenu">
                                    <li><a href="#">Daftar Laporan</a></li>
                                    <li><a href="#">Laporan Keuangan Kustomisasi</a></li>
                                    <li><a href="#">SPT Tahunan</a></li>
                                    <li><a href="#">PPN / PPN BM</a></li>
                                    <li><a href="#">Laporan Tersimpan</a></li>
                                    <li><a href="#">Laporan Terdesain</a></li>
                                    <li><a href="#">Berkas Laporan (*.fr3)</a></li>
                                </ul>
                            </li>
                        </ul>
                        <!-- End navigation menu -->

                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->