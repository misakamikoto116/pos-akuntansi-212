<script type="text/javascript">
@if (session()->has('error_message'))
    @if(session()->has('icon'))
        swal({
            icon: "{{ session()->get('icon') }}",
            text: "{{ session()->get('error_message') }}",
            confirmButtonColor: "#AEDEF4"
        });
    @else 
        swal({
            icon: "error",
            text: "{{ session()->get('error_message') }}",
            confirmButtonColor: "#AEDEF4"
        });
    @endif
@endif
</script>