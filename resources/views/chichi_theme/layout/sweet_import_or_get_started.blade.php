<script type="text/javascript">
	$(document).ready(function($) {
		swal("Pilih untuk import atau input data awal ?", {
		  closeOnClickOutside: false,
		  closeOnEsc: false,
		  icon: "warning",
		  buttons: {
		    import: {
		      text: "Import Data",
		      value: "import",
		    },
		    input: {
		    	text: "Input Data",
		    	value: "input",
		    },
		  },
		})
		.then((value) => {
		  switch (value) {
		 
		    case "import":
		    	window.location.replace("{{ route('import-get-started') }}");
		      break;
		 
		    case "input":
		      break;

		  }
		});	
	});
</script>