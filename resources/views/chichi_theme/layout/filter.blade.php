@if(isset($module_url))
    @if($module_url === null)
        {{-- nothing --}}
    @else
        <div class="col-sm-12">
            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalPencarian"><i class="fa fa-search"></i> Pencarian ...</button>
        </div>

        <div id="modalPencarian" class="modal fade bs-example-modal-lg" role="dialog" aria-labelledby="modalPencarian" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0">Pencarian</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    @if(in_array('module_url_show',$filter))
                        {!! Form::model($item,['route' => [$module_url->show,$item->id],'method' => 'GET']) !!}
                    @else 
                        {!! Form::open(['route' => $module_url->index, 'method' => 'GET']) !!}
                    @endif
                    <div class="modal-body">
                        <div class="form-group row">
                            {!! Form::label('search','Search by keyword',['class' => 'col-3 col-form-label']) !!}
                                <div class="col-9">
                                    {!! Form::text('search', old('search'), ['class' => 'form-control','placeholder' => 'Cari ...']) !!}
                                </div>
                        </div>
                        {{-- Aktiva --}}
                        @if(in_array('kode_aktiva',$filter))
                            <div class="form-group row">
                                {!! Form::label('kode_aktiva','Kode Aktiva',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('kode_aktiva', old('kode_aktiva'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Kode Aktiva']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('keterangan_aktiva',$filter))
                            <div class="form-group row">
                                {!! Form::label('keterangan_aktiva','Keterangan',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('keterangan_aktiva', old('keterangan_aktiva'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Keterangan Aktiva']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('tipe_aktiva',$filter))
                            <div class="form-group row">
                                {!! Form::label('tipe_aktiva','Tipe Aktiva',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('tipe_aktiva', $listTipeAktivaTetap, old('tipe_aktiva'), ['class' => 'form-control select2','placeholder' => 'Pilih Tipe Aktiva']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('metode_penyusutan',$filter))
                            <div class="form-group row">
                                {!! Form::label('metode_penyusutan','Metode Penyusutan',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('metode_penyusutan', $listMetodePenyusutan, old('metode_penyusutan'), ['class' => 'form-control select2','placeholder' => 'Pilih Metode Penyusutan']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('tidak_berwujud',$filter))
                            <div class="form-group row">
                                {!! Form::label('tidak_berwujud','Tidak Berwujud',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('tidak_berwujud', $status_aktiva, old('tidak_berwujud'), ['class' => 'form-control select2','placeholder' => 'Pilih']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('pajak',$filter))
                            <div class="form-group row">
                                {!! Form::label('pajak','Pajak',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('pajak', $status_aktiva, old('pajak'), ['class' => 'form-control select2','placeholder' => 'Pilih']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('tanggal_pakai',$filter))
                            <div class="form-group row">
                                {!! Form::label('dari','Tanggal Pakai',['class' => 'col-3 col-form-label']) !!}
                            </div>
                            <hr>
                            <div class="form-group row">
                                {!! Form::label('dari','Dari',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-3">
                                        {!! Form::date('tanggal_dari_pakai', old('tanggal_dari_pakai'), ['class' => 'form-control']) !!}
                                    </div>
                                {!! Form::label('sampai','s/d',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-3">
                                        {!! Form::date('tanggal_sampai_pakai', old('tanggal_sampai_pakai'), ['class' => 'form-control']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('tanggal_perolehan',$filter))
                            <div class="form-group row">
                                {!! Form::label('dari','Tanggal Perolehan',['class' => 'col-3 col-form-label']) !!}
                            </div>
                            <hr>
                            <div class="form-group row">
                                {!! Form::label('dari','Dari',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-3">
                                        {!! Form::date('tanggal_dari_perolehan', old('tanggal_dari_perolehan'), ['class' => 'form-control']) !!}
                                    </div>
                                {!! Form::label('sampai','s/d',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-3">
                                        {!! Form::date('tanggal_sampai_perolehan', old('tanggal_sampai_perolehan'), ['class' => 'form-control']) !!}
                                    </div>
                            </div>
                        @endif
                        {{--  --}}
                        {{-- Akun --}}
                        @if(in_array('nama_akun',$filter))
                            <div class="form-group row">
                                {!! Form::label('nama_akun','Nama Akun',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('nama_akun', old('nama_akun'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Nama Akun']) !!}
                                    </div>
                            </div>
                        @endif
                        {{--  --}}
                        {{-- Retur Pembelian --}}
                        @if(in_array('no_retur',$filter))
                            <div class="form-group row">
                                {!! Form::label('no_retur','No PR',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_retur', old('no_retur'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Nomor PR']) !!}
                                    </div>
                            </div>
                        @endif
                        {{--  --}}
                        {{-- Pembayaran Pembelian --}}
                        @if(in_array('no_cek',$filter))
                            <div class="form-group row">
                                {!! Form::label('no_cek','No Cek',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_cek', old('no_cek'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Nomor Cek']) !!}
                                    </div>
                            </div>
                        @endif
                        {{--  --}}
                        {{-- Penyesuaian Persediaan --}}
                        @if(in_array('no_faktur',$filter))
                            <div class="form-group row">
                                {!! Form::label('no_faktur','No Faktur',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_faktur', old('no_faktur'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Nomor Faktur']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('tipe_penyesuaian',$filter))
                            <div class="form-group row">
                                {!! Form::label('tipe_penyesuaian','Tipe Penyesuaian',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('tipe_penyesuaian', $tipe_penyesuaian, old('tipe_penyesuaian'), ['class' => 'form-control select2','placeholder' => 'Pilih Tipe Penyesuaian']) !!}
                                    </div>
                            </div>
                        @endif
                        {{--  --}}
                        {{-- Pemindahan Barang --}}
                        @if(in_array('no_transfer',$filter))
                            <div class="form-group row">
                                {!! Form::label('no_transfer','No Transfer',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_transfer', old('no_transfer'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Nomor Transfer']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('pindah_dari',$filter))
                            <div class="form-group row">
                                {!! Form::label('pindah_dari','Pindah Dari',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('pindah_dari', $gudang, old('pindah_dari'), ['class' => 'form-control select2','placeholder' => 'Pindah Dari']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('pindah_ke',$filter))
                            <div class="form-group row">
                                {!! Form::label('pindah_ke','Pindah Ke',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('pindah_ke', $gudang, old('pindah_ke'), ['class' => 'form-control select2','placeholder' => 'Pindah ke']) !!}
                                    </div>
                            </div>
                        @endif
                        {{--  --}}
                        {{-- Pembiaayaan Pesanan --}}
                        @if(in_array('no_batch',$filter))
                            <div class="form-group row">
                                {!! Form::label('no_batch','No Batch',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_batch', old('no_batch'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Nomor Batch']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('status_pembiayaan',$filter))
                            <div class="form-group row">
                                {!! Form::label('status_pembiayaan','Status',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('status_pembiayaan', $status_pembiayaan, old('status_pembiayaan'), ['class' => 'form-control select2','placeholder' => 'Pilih Status']) !!}
                                    </div>
                            </div>
                        @endif
                        {{--  --}}
                        {{-- Informasi Pemasok --}}
                        @if(in_array('no_pemasok',$filter))
                            <div class="form-group row">
                                {!! Form::label('no_pemasok','No Pemasok',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_pemasok', old('no_pemasok'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Nomor Pemasok']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('nama_pemasok',$filter))
                            <div class="form-group row">
                                {!! Form::label('nama_pemasok','Nama Pemasok',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('nama_pemasok', old('nama_pemasok'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Nama Pemasok']) !!}
                                    </div>
                            </div>
                        @endif
                        {{--  --}}
                        {{-- Informasi Pelanggan --}}
                        @if(in_array('no_pelanggan',$filter))
                            <div class="form-group row">
                                {!! Form::label('no_pelanggan','No Pelanggan',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_pelanggan', old('no_pelanggan'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Nomor Pelanggan']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('nama_pelanggan',$filter))
                            <div class="form-group row">
                                {!! Form::label('nama_pelanggan','Nama Pelanggan',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('nama_pelanggan', old('nama_pelanggan'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Nama Pelanggan']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('tipe_pelanggan',$filter))
                            <div class="form-group row">
                                {!! Form::label('tipe_pelanggan','Tipe Pelangan',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('tipe_pelanggan', $tipePelanggan, old('tipe_pelanggan'), ['class' => 'form-control select2','placeholder' => 'Pilih Tipe Pelanggan']) !!}
                                    </div>
                            </div>
                        @endif
                        {{--  --}}
                        {{-- Pembayaran & Penerimaan --}}
                        @if(in_array('no_pembayaran',$filter))
                            <div class="form-group row">
                                {!! Form::label('no_pembayaran','No',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_pembayaran', old('no_pembayaran'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan No']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('keterangan_pembayaran',$filter))
                            <div class="form-group row">
                                {!! Form::label('keterangan_pembayaran','Keterangan',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('keterangan_pembayaran', old('keterangan_pembayaran'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan Keterangan']) !!}
                                    </div>
                            </div>
                        @endif
                        {{--  --}}
                        @if(in_array('role',$filter))
                            <div class="form-group row">
                                {!! Form::label('role','Role/Peran',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('role', $role, old('role'), ['class' => 'form-control select2','placeholder' => 'Pilih Role']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('no_transaksi',$filter))
                            <div class="form-group row">
                                {!! Form::label('no_sumber','No Sumber',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_transaksi', old('no_transaksi'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan No Sumber']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('no_akun',$filter))
                            <div class="form-group row">
                                {!! Form::label('no_akun','No Akun',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_akun', old('no_akun'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan No Akun']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('akun_id',$filter))
                            <div class="form-group row">
                                {!! Form::label('akun_id','Akun',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('akun_id', $akun, old('akun_id'), ['class' => 'form-control select2','placeholder' => 'Pilih Akun']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('tipe_akun_id',$filter))
                            <div class="form-group row">
                                {!! Form::label('tipe_akun_id','Tipe Akun',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('tipe_akun_id', $tipe_akun, old('tipe_akun_id'), ['class' => 'form-control select2','placeholder' => 'Pilih Tipe Akun']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('pelanggan_select',$filter))
                            <div class="form-group row">
                                {!! Form::label('pelanggan_select','Pelanggan',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('pelanggan_select', $pelanggan, old('pelanggan_select'), ['class' => 'form-control select2','placeholder' => 'Pilih Pelanggan']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('pemasok_select',$filter))
                            <div class="form-group row">
                                {!! Form::label('pemasok_select','Pemasok',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('pemasok_select', $pemasok, old('pemasok_select'), ['class' => 'form-control select2','placeholder' => 'Pilih Pemasok']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('status_penjualan',$filter))
                            <div class="form-group row">
                                {!! Form::label('status','Status',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('status_penjualan', $status_penjualan, old('status_penjualan'), ['class' => 'form-control select2','placeholder' => 'Pilih Status']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('tanggal_dari_sampai',$filter))
                            <div class="form-group row">
                                {!! Form::label('dari','Dari',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-3">
                                        {!! Form::date('tanggal_dari', old('tanggal_dari'), ['class' => 'form-control']) !!}
                                    </div>
                                {!! Form::label('sampai','s/d',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-3">
                                        {!! Form::date('tanggal_sampai', old('tanggal_sampai'), ['class' => 'form-control']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('tanggal_dari_sampai_rekap',$filter))
                            <div class="form-group row">
                                {!! Form::label('dari','Dari',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-3">
                                        {!! Form::date('tanggal_dari_rekap', old('tanggal_dari_rekap'), ['class' => 'form-control']) !!}
                                    </div>
                                {!! Form::label('sampai','s/d',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-3">
                                        {!! Form::date('tanggal_sampai_rekap', old('tanggal_sampai_rekap'), ['class' => 'form-control']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('filter_menu',$filter))
                            <hr>
                            <div class="col-sm-12" style="border: 2px solid rgba(0,0,0,.1); border-radius: 15px;">
                                <table class="table">
                                    <thead style="display:table;width:100%;table-layout:fixed;width: calc( 100% - 1em )">
                                        <tr>
                                            <th>Tipe</th>
                                            <th style="float: right;">
                                                {!! Form::button('Pilih Semua',['class' => 'btn btn-success btn-sm button-checkbox-filter-menu','onclick' => 'changeCheckBoxFilterMenu()','id' => 'button-all-checked']) !!}
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody style="display:block;max-height:200px;overflow:auto;">
                                        @foreach ($name_space as $key => $data_name_space)
                                            <tr>
                                                <td>{{ $key }}</td>
                                                <td><input name="filter_menu[]" value="{{ $data_name_space }}" class="checkbox-custom checkbox-filter-menu" checked="" type="checkbox"></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                        @if(in_array('tanggal',$filter))
                            <div class="form-group row">
                                {!! Form::label('tanggal','Tanggal',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::date('tanggal', old('tanggal'), ['class' => 'form-control']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('tanggal-jangka',$filter))
                            <div class="form-group row">
                                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
                                <div class="col-9">
                                    {!! Form::date('start-date', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-keseluruhan-jurnal"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
                                <div class="col-9">
                                    {!! Form::date('end-date', null, ['class' => 'form-control','id' => 'end-date-keseluruhan-jurnal']) !!}          
                                </div>
                            </div>
                        @endif
                        @if(in_array('pelanggan',$filter))
                            <div class="form-group row">
                                {!! Form::label('nomor_pelanggan','Pelanggan',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('search_pelanggan', old('search_pelanggan'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan nomor/nama pelanggan']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('pemasok',$filter))
                            <div class="form-group row">
                                {!! Form::label('nomor_pemasok','Pemasok',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('search_pemasok', old('search_pemasok'), ['class' => 'form-control','placeholder' => 'Cari berdasarkan nomor/nama pemasok']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('kategori',$filter))
                            <div class="form-group row">
                                {!! Form::label('kategori','Kategori',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('kategori', $kategori, old('kategori'), ['class' => 'form-control select2','placeholder' => 'Pilih Kategori']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('tipe_barang',$filter))
                            <div class="form-group row">
                                {!! Form::label('tipe_barang','Tipe Barang',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('tipe_barang', $tipe_barang, old('tipe_barang'), ['class' => 'form-control select2','placeholder' => 'Pilih Tipe Barang']) !!}
                                    </div>
                            </div>
                        @endif
                        @if(in_array('mata_uang',$filter))
                            <div class="form-group row">
                                {!! Form::label('mata_uang','Mata Uang',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::select('mata_uang', $mataUang, old('mata_uang'), ['class' => 'form-control select2','placeholder' => 'Pilih Mata Uang']) !!}
                                    </div>
                            </div>
                        @endif
                        {{-- Filter Kode Transaksi Rekap Penjualan --}}
                        @if(in_array('kode_transaksi_pos', $filter))
                            <div class="form-group row">
                                {!! Form::label('mata_uang','Kode Transaksi',['class' => 'col-3 col-form-label']) !!}
                                    <div class="col-9">
                                        {!! Form::text('no_faktur', null,['class' => 'form-control']) !!}      
                                    </div>
                            </div>
                        @endif
                        {{--  --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        {!! Form::submit('Cari !', ['class' => 'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
@endif
    