@php
    
    function ambilTransaksi($mf, $trans)
    {
        $sum = $mf;
        foreach($trans as $tran) {
            if ($tran->status == 0) {
                $sum -= $tran->nominal;
            } else {
                $sum += $tran->nominal;
            }
        }

        return $sum;
    }

@endphp


@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                   <div class="card-body">
                <table class="table">
                    <tr>
                        <th>Id</th>
                        <th>kode akun</th>
                        <th>Money</th>
                        
                        <th></th>
                    </tr>
                    @foreach ($test as $element)
                    <tr>
                        <td>{{$element->id}}</td>
                        <td>{{$element->kode_akun}}</td>
                        <td>{{ ambilTransaksi($element->money_function, $element->transaksi) }}</td>
                        <td>
                            <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Option<span class="caret"></span></button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> Lihat</a>
                                <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit</a>
                                <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Hapus</a>
                            </div>
                        </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
