<div class="wrapper-page" style="padding : 10px 0px">
    <div class="ex-page-content text-center">
        <div class="text-error">
            <span class="text-primary" style="font-size: 100px;">4</span><i class="ti-face-sad text-pink"></i><span class="text-info" style="font-size: 100px;">3</span>
            <p style="font-size: 25px;">Unauthorized Action</p>
        </div>
        <p class="text-muted" style="font-size: 16px;">Akun Anda tidak memiliki Akses Pada Halaman Ini</p>
    </div>
</div>