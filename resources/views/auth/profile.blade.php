@extends('chichi_theme.layout.app')

@section('custom_css')

@endsection

@section('content')

<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                <div class="warn_pass">
                    <div class="container">
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-exclamation-circle"></i> Password tidak sesuai
                        </div>
                    </div>
                </div>
                    <!-- Judul Halaman -->
                    <h4 class="page-title">Edit Profile</h4>

                    <!-- Judul Halaman -->
                    <ol id="breadcrumb">
                      <li><a href="{{ route('home') }}"><span class="icon icon-home"> </span></a></li>
                      <li><a href="#">Edit Profile</a></li>
                    </ol>

                </div>
            </div>


<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">{{ $title_document }}</h5>
            </div>
            <div class="card-body">
                <div class="p-20">
                {!! Form::open(['url' => 'profile/update', 'method' => 'PATCH', 'files' => 'false', 'class' => 'form-horizontal form-label-left']) !!}
                    <div class="form-group row">
                        {!! Form::label('name','Nama',['class' => 'col-2 col-form-label']) !!}
                            <div class="col-10">
                                {!! Form::text('name', $data->name, ['class' => 'form-control','required']) !!}
                            </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('email','Email',['class' => 'col-2 col-form-label']) !!}
                            <div class="col-10">
                                {!! Form::email('email', $data->email, ['class' => 'form-control','required']) !!}
                            </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('old_password','Password Saat ini',['class' => 'col-2 col-form-label']) !!}
                            <div class="col-10">
                                {!! Form::password('old_password', ['class' => 'form-control','required']) !!}
                                <small>* Wajib diisi untuk mengkonfirmasi bahwa Anda sendirilah yang merubah informasi akun Anda.</small>
                            </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('password','Password Baru',['class' => 'col-2 col-form-label']) !!}
                            <div class="col-10">
                                {!! Form::password('password', ['class' => 'form-control password']) !!}
                            </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('passwords','Konfirmasi Password',['class' => 'col-2 col-form-label']) !!}
                            <div class="col-10">
                                {!! Form::password('passwords', ['class' => 'form-control password2']) !!}
                            <small>* Kosongkan password apabila tidak diubah</small>
                            </div>
                    </div>
                <div class="submit">
                {!! Form::button('<i class="fa fa-check"></i> Perbarui',['type' => 'submit','class' => 'btn btn-default btn-submit']) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh',['type' => 'reset','class' => 'btn btn-danger' ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("custom_js")
  <script src="{{ asset('js/user.js') }}"></script>
@endsection