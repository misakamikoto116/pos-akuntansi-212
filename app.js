var app = require('express')();
var http = require('https');
var fs = require('fs');
var express = require('express');
var helmet = require('helmet');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var bodyParser = require('body-parser');
var _app_token = '1c2VyX2lkIjoxLCJpYXQiOjE1MzQxNDg4OD';

var allowCrossDomain = function (req, res, next) {
    res.header('X-Powered-By', 'THORTECH');
    res.header('X-XSS-Protection', 0);
    res.header('Access-Control-Allow-Origin', '*');
    next();
};


var cekTokenHeader = function (req, res, next) {
    var _token = req.headers['x-token'];
    if (!_token) 
        return res.status(401).send({status: 401, message: 'kesalahan token tidak sesuai', type: 'internal'});
    if (_token != _app_token) {
        console.log('kesalahan token tidak sesuai');
        return res.status(401).send({status: 401, message: 'kesalahan token tidak sesuai', type: 'internal'});
    } else {
        next()
    }
}

app.use(allowCrossDomain);
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(helmet());
io.on('connection', function (socket) {
    //console.log(socket);
    console.log("new client connected");
});

app.get('/', function (req, res) {
    console.log('req request : ' + req.ip);
    res.send('Notif NodeJs Server');
});

app.post('/channel', cekTokenHeader, function (req, res, next) {
    var _channel = req.body._channel_socket;
    var _pesan = req.body._message;
    io.sockets.emit(_channel, _pesan);
    res.status(200).send({sukses: true, tujuan: _channel, pesan: _pesan});
});

app.post('/displaykasir', cekTokenHeader, function (req, res, next) {

    var _channel = req.body['kasir'];
    var _data = req.body;
    io.sockets.emit(_channel, _data);
    console.log(req.body);
    console.log(req.body['barang']);
    res.status(200).send({sukses: true, tujuan: req.body['kasir'], data: _data});
});

server.listen(3500, function () {
    console.log('NodeJS server Socket Jalan ' + server.address().port);
});
