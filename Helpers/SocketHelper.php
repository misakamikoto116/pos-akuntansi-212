<?php
/**
 * Created by PhpStorm.
 * User: efendihariyadi
 * Date: 12/11/18
 * Time: 14.18
 */

namespace Helpers;


use GuzzleHttp\Client;

class SocketHelper
{
    // TODO ini seharusnya baca dari config atau env variable
    protected $API_URL = 'http://localhost:9000/';

    public function __construct()
    {
    }

    /**
     * @param $barangUpdate array
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    // $sokcet = new Helpers\SocketHelper();
    // $sokcet->sendUpdateData([
    //  ["harga_modal" => 2000, "harga_terakhir" => 2500, "id" => 77, "kategori" => "PCS", "nama" => "BARANG EFG", "no_barang" => "0123950", "satua" => "psc"]
    //])
    //
    public function sendUpdateData($barangUpdate)
    {
        try {
            $client = new Client();
            $url = $this->API_URL . "product/update-baru";
            $response = $client->request('POST', $url, [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'x-token' => '1c2VyX2lkIjoxLCJpYXQiOjE1MzQxNDg4OD',
                    'Accept' => 'application/json'
                ],
                'form_params' => [
                    'barang' => $barangUpdate,
                ],
            ]);
            $status = $response->getStatusCode();
            if ($status == 200) {
                return json_decode($response->getBody());
            } else {
                return false;
            }
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            return false;
        }
    }
}