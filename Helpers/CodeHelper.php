<?php
namespace Helpers;

use App\Modules\Akuntansi\Models\JurnalUmum;
use Carbon\Carbon;

/**
 * @param collection berupa model
 * @param string berupa nama kolom
 * @param string berupa value dari repository untuk dicek, apakah dia memiliki nilai?
 *
 * @return string Code
 *                ==============================================
 *                Feature :
 *
 * 1. Auto Number when records is empty
 *
 * 2. Auto increment when the field is integer
 *
 * 3. Auto Continue when  the field is formatted.
 * Ex : KS001 it will continue to KS002
 *
 * 4. Auto Detect if the field is fully String,
 * and create the string old record to formatted
 * Ex : old = KMZ, and it will continue to KMZ1
 *
 * 5. Support for abnormal code
 * Ex : 001KMZ
 * ==============================================
 *
 * @author : axeltjs
 * @TODO : Buat support 3 Digit spt : KS001KMZ
 */
class CodeHelper
{
	public function autoGenerate($data, $nama_kolom, $val = null)
	{
		if (null == $val) {
			if ($data instanceof Model) {
				$toArrayQuery = $data->orderBy('id', 'desc')->first()->toArray();
			} else {
				$toArrayQuery = $data;
			}
			if (empty($toArrayQuery)) {
				return 1;
			}

			if (is_numeric($toArrayQuery[$nama_kolom])) {
				return $toArrayQuery[$nama_kolom] + 1;
			} else {
				//Data ada.. namun si kolomnya kosong..
				if (empty($toArrayQuery[$nama_kolom])) {
					return $toArrayQuery['id'] + 1;
				}
				preg_match_all('/\d+|[a-z]+/i', $toArrayQuery[$nama_kolom], $result);
				$str_digit = strlen((string) $result[0][0]);
				if (count($result[0]) > 1) {
					//Jika numeric adalah No.2 alias Normal spt : K0001
					if (is_numeric($result[0][1])) {
						$num_digit = strlen((string) $result[0][1]);
						$increment = substr($toArrayQuery[$nama_kolom], $str_digit, $num_digit) + 1;
						$done      = $result[0][0] . sprintf('%0' . $num_digit . 's', $increment);
					} elseif (is_numeric(end($result[0]))) {
						$num_digit  = strlen((string) end($result[0]));
						$increment  = end($result[0]) + 1;
						$all_string = substr($toArrayQuery[$nama_kolom], 0, -$num_digit);
						$done       = $all_string . sprintf('%0' . $num_digit . 's', $increment);
					} else {
						//Jika memang formatnya abnormal spt : 0001K
						$num_digit       = strlen((string) $result[0][0]);
						$beforeIncrement = substr($toArrayQuery[$nama_kolom], 0, $num_digit);

						if (!is_numeric($beforeIncrement)) {
							$increment = $beforeIncrement . '1';
						} else {
							$increment = $beforeIncrement + 1;
						}

						$done = sprintf('%0' . $num_digit . 's', $increment) . $result[0][1];
					}
				} else {
					// Ternyata dia hanyalah seorang string;
					$done = $result[0][0] . '1';
				}

				return $done;
			}
		}

		return $val;
	}

	/**
	 * Generate Barcode format EAN 13 (13 digit number).
	 *
	 * @param $number
	 *
	 * @return string
	 */
	public function generateBarcode($number)
	{
		$code       = '899' . str_pad($number, 9, '0');
		$weightflag = true;
		$sum        = 0;
		for ($i = strlen($code) - 1; $i >= 0; --$i) {
			$sum += (int) $code[$i] * ($weightflag ? 3 : 1);
			$weightflag = !$weightflag;
		}
		$code .= (10 - ($sum % 10)) % 10;

		return $code;
	}

	public function makeNoPos($id)
	{
		$id     = $id + 1;
		$no_pos = 'POS/' . date('Y/m/d') . '/' . $id;

		return $no_pos;
	}

	public function generateNoFakturPelangganImport($value)
	{
		$format_no_faktur = null;
		if (empty($value->no_faktur)) {
			$format_no_faktur = empty($value['no_pelanggan']) ? 'saldo-awal-pelanggan-' . $value->no_pelanggan : 'saldo-awal-pelanggan-' . $value['no_pelanggan'];
		} else {
			$format_no_faktur = empty($value['no_faktur']) ? 'saldo-awal-pelanggan-' . $value->no_faktur : 'saldo-awal-pelanggan-' . $value['no_faktur'];
		}

		return $format_no_faktur;
	}

	public function generateNoFakturPemasokImport($value)
	{
		$format_no_faktur = null;
		if (empty($value->no_faktur)) {
			$format_no_faktur = empty($value['no_pemasok']) && '0' != $value['no_pemasok'] ? 'saldo-awal-pemasok-' . $value->no_pemasok : 'saldo-awal-pemasok-' . $value['no_pemasok'];
		// $format_no_faktur = 'saldo-awal-pemasok-'.$value->no_pemasok;
		} else {
			$format_no_faktur = empty($value['no_faktur']) && '0' != $value['no_faktur'] ? 'saldo-awal-pemasok-' . $value->no_faktur : 'saldo-awal-pemasok-' . $value['no_faktur'];
			// $format_no_faktur = 'saldo-awal-pemasok-'.$value->no_faktur;
		}

		return $format_no_faktur;
	}

	public function generateNoFaktur($tanggal)
	{
		$tanggal     = Carbon::parse($tanggal)->format('d-m-Y');
		$last_jurnal = JurnalUmum::orderBy('id', 'DESC')->first();
		if (null === $last_jurnal) {
			$last_jurnal = $last_jurnal + 1;
		} else {
			$last_jurnal = $last_jurnal->id + 1;
		}
		$format_no_faktur = sprintf('saldo-awal/%s/%s', $tanggal, $last_jurnal);

		return $format_no_faktur;
	}
}
