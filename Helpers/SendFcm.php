<?php
/**
 * Created by PhpStorm.
 * User: efendihariyadi
 * Date: 19/11/18
 * Time: 15.08
 */

namespace Helpers;


use GuzzleHttp\Exception\BadResponseException;

class SendFcm
{
    private $apiUrl = "https://fcm.googleapis.com/fcm/send";
    private $fcmServerKey = "key=AAAAk4qNCrQ:APA91bHuWNYKF6rpWwhJCXoSWCSZGcm12ach6fwbfwjeTHk9H0ocAFn9DSPqfKwV7O-ztj_kuHZ2KzfBnWt8ZyTKrmNjxbqAMPlAM6bBH0TXmyl31UIugxZBk0XmBw34fgmw6bmIB1BW";
    //sampel
    //ctBw_lcEzEk:APA91bGxsPlxx-K9WFEKQcWYQVxktJwH7Y2h5VuZHAXhx9Z95wn7Be5vF-fR9czIvQiKM-OTlKk6-0ZMP_PJSQDkKsL-oGtEvpZA9wPQwkMFu_2QY8CEeQ8df0_iHWndpYjx3nrtO0Ts
    public function sendNotfyUpdateBarng($data, $destionation){
        $message = [
            'data' => [
                'title' => 'update_produk',
                'body' => $data
            ],
            'registration_ids' => $destionation
        ];
        try{
            $client = new \GuzzleHttp\Client([
                'headers' => [
                    'Authorization' => $this->fcmServerKey,
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],

            ]);
            $response = $client->post($this->apiUrl,
                ['body' => json_encode($message)]
            );
            echo $response->getBody();
        }catch (BadResponseException $e){
            echo $e->getMessage();
        }
        return true;
    }
}