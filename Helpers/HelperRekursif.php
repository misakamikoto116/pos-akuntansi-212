<?php

namespace Helpers;
use App\Modules\Akuntansi\Models\Akun;

class HelperRekursif
{
    protected $model;
    protected $rekursif = [];

    function __construct()
    {
        $this->model = new Akun;
    }

    public function recursiveType($parent, $number = 0)
    {
        $branch = [
            'model' => $parent,
            'number' => $number,
            'child' => [],
            'jumlah' => 0
        ];
            
        $query = $this->model->where('parent_id', $parent->id)->get();

        $sum1 = 0;

        foreach ($query as $key => $rows) {
            // $branch['child'][$rows->id] = [
            //     'model' => $rows,
            //     'number' => $branch['number'] + 5
            // ];

            // if ($this->has_child($rows->id)) {
            //     $branch['child'][$rows->id] = $this->recursiveType($rows, $branch['number'] + 5);
            // }

            $c_info = $this->recursiveType($rows, $branch['number'] + 5);

            $sum = 0;

            foreach ($c_info['child'] as $c_key => $c_data)
            {
                $sum += $c_data['jumlah'];
            }
            
            $branch['child'][$key] = [
                'jumlah' => $this->getTotalAkun($rows) + $sum,
                'number' => $branch['number'] + 5,
                'model' => $rows
            ];

            $sum1 += $this->getTotalAkun($rows) + $sum;

            if ($this->has_child($rows->id)) {
                $branch['child'][$key] = $c_info;
            }

            $branch['jumlah'] = $sum1;
        }

        return $branch;
    }

    public function has_child($parent_id){
        $akun = $this->model->where('parent_id', $parent_id)->get();

        if ($akun->isEmpty()) {
            return false;
        }

        return true;
    }

    public function getTotalAkun($item)
    {
        // $akun = $this->model->where('parent_id', $item->id)->get();
        
        // if ($akun->isEmpty()) return 1;

        // return 0;

        $sum = $item->money_function;

        if ($item->transaksi !== null) {
            foreach ($item->transaksi as $transaksi) {
                if (!is_null($transaksi->status)) {
                    if ($transaksi->status == 1) {
                        $sum += $transaksi->nominal;
                    }else if ($transaksi->status == 0){
                    $sum -= $transaksi->nominal;
                    }
                }else{
                    $sum;
                }
            }
        }

        return $sum === null
               ? 0
               : $sum;

    }

    public function get()
    {
        foreach ($this->model->whereNull('parent_id')->get() as $model) {
            $this->rekursif[] = $this->recursiveType($model);
        }

        return $this->rekursif;
    }
}
