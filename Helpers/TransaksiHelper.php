<?php

namespace Helpers;

use App\Modules\Akuntansi\Models\Transaksi;
use App\Modules\Akuntansi\Models\Produk;
use App\Modules\Akuntansi\Models\SaldoAwalBarang;
use App\Modules\Pos\Models\CartLog;
use Auth;


class TransaksiHelper
{
    /**
     *
     * @param data array
     * @nb : required param delete = type, model, data, key
     *  'nominal' => integer,
     *  'produk' => integer (nullable / hilangkan paramnya juga bisa),
     *  'akun' => akun_id,
     *  'id' => nomor transaksi (modifable),
     */
    public function ExecuteTransactionMultiple($type, $model, $data = null, $dk = 0, $id = null)
    {
        $new_data = [];
        $nama_kelas = (new \ReflectionClass(get_class($model)))->getShortName();
        
        foreach ($data as $item) {
            if (0 != $item['nominal']) {
                if (!empty($item['id'])) {
                    $no_transaksi = $item['id'];
                } else {
                    $no_transaksi = $id;
                }
                $new_data[] = [
                    'nominal' => $item['nominal'],
                    'tanggal' => (!empty($item['tanggal'])) ? $item['tanggal'] : date('Y-m-d H:i:s'),
                    'akun_id' => (!empty($item['akun'])) ? $item['akun'] : null,
                    'produk_id' => (!empty($item['produk'])) ? $item['produk'] : null,
                    'item_id' => (!empty($data['item_id'])) ? $data['item_id'] : $model->id,
                    'item_type' => get_class($model),
                    'status' => $dk,
                    'no_transaksi' => $no_transaksi,
                    'sumber' => $this->fromCamelCase($nama_kelas, ' '),
                    'keterangan' => $nama_kelas == 'PenerimaanPembelian' ? $this->fromCamelCase('PenerimaanBarang', ' ').' '.$no_transaksi : $this->fromCamelCase($nama_kelas, ' ').' '.$no_transaksi,
                    'dari' => (!empty($item['dari'])) ? $item['dari'] : null,
                    'status_rekonsiliasi' => null,
                    'desc_dev' => (!empty($item['desc'])) ? $item['desc'] : null,
                    'created_at' => date('Y-m-d H:i:s'),
                ];
            }
        }
        if ('insert' == $type) {
            $transaksi = Transaksi::insert($new_data);
        } else {
            $transaksi = Transaksi::where(['item_id' => $model->id, 'item_type' => get_class($model)])->delete();
        }

        return $transaksi;
    }

    public function ExecuteTransactionSingle($type, $model, $data = null, $dk = 0, $id = null)
    {
        $new_data = [];
        $nama_kelas = (new \ReflectionClass(get_class($model)))->getShortName();

        if (!empty($data['id'])) {
            $no_transaksi = $data['id'];
        } else {
            $no_transaksi = $id;
        }

        if ($data['nominal'] == 0 || $data['nominal'] === null) {
            $nominal = null;
        }else {
            $nominal = $data['nominal'];
        }

        $new_data = [
            'nominal' => $nominal,
            'tanggal' => (!empty($data['tanggal'])) ? $data['tanggal'] : date('Y-m-d H:i:s'),
            'akun_id' => (!empty($data['akun'])) ? $data['akun'] : null,
            'produk_id' => (!empty($data['produk'])) ? $data['produk'] : null,
            'item_id' => (!empty($data['item_id'])) ? $data['item_id'] : $model->id,
            'item_type' => (!empty($data['item_type'])) ? $data['item_type'] : get_class($model),
            'status' => $dk,
            'no_transaksi' => $no_transaksi,
            'sumber' => $this->fromCamelCase($nama_kelas, ' '),
            'keterangan' => $nama_kelas == 'PenerimaanPembelian' ? $this->fromCamelCase('PenerimaanBarang', ' ').' '.$no_transaksi : $this->fromCamelCase($nama_kelas, ' ').' '.$no_transaksi,
            'dari' => (!empty($data['dari'])) ? $data['dari'] : null,
            'status_rekonsiliasi' => null,
            'desc_dev' => (!empty($data['desc'])) ? $data['desc'] : null,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by'    => (!empty($data['created_by'])) ? $data['created_by'] : null,
            'updated_by'    => (!empty($data['updated_by'])) ? $data['updated_by'] : null,
        ];

        if ('insert' == $type) {
            $transaksi = Transaksi::create($new_data);
        } else {
            $transaksi = Transaksi::where(['item_id' => $model->id, 'item_type' => get_class($model)])->delete();
        }

        return $transaksi;
    }

    /**
     * @param namespace
     * @param string nama anaknya
     * @param int item_id
     *
     * @return url
     */
    public function filterClass($kelas, $anak, $id)
    {
        $model = new $kelas();
        $nama_kelas = (new \ReflectionClass($model))->getShortName();
     
        $data = null;

        if (!empty($model->$anak)) {
            $data = $model->with([$anak => function ($data) use ($id) {
                $data->where('id', $id);
            }])->first();
        }

        if (empty($data)) {
            return null;
        }
        $url = '/akuntansi'.'/'.$this->fromCamelCase($nama_kelas, '-').'/'.$data->id.'/edit';

        return $url;
    }

    public function fromCamelCase($input, $seperator)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }

        return implode($seperator, $ret);
    }

    public function checkGudang($data)
    {
        if (isset($data['gudang_id'])) {
            foreach ($data['produk_id'] as $i => $produkID) {
                $produk_gudang = Produk::select('gudang_def_id')->find($produkID);
                $gudangID = $data['gudang_id'][$i];
                if (null === $gudangID) {
                    continue;
                }
                // if ($gudangID != $produk_gudang->gudang_def_id) {
                //     if (empty($data['keterangan'][$i])) {
                //         throw new \Exception('Maaf, Barang '.$data['keterangan'][$i].' Tidak sesuai dengan gudangnya.', 451);
                //     } else {
                //         throw new \Exception('Maaf, Barang '.$data['keterangan_produk'][$i].' Tidak sesuai dengan gudangnya.', 451);
                //     }
                // }
            }
        }

        return true;
    }

    /**
     * @nb ini bukan bagian dari transaksi.. tapi yasudahlah
     *
     * @param collection model utama
     * @param collection model untuk sub item
     * @param string tipe masukkan
     * @param array data tambahan khusus update
     * ===========================================================
     * @nb : required param update = type, model, data, key, data
     * @nb : required param delete = type, model, data, key
     */
    public function UpdateParentStatus($model, $subItem, $type, $key, $data = null)
    {
        if ('update' == $type && !empty($data)) {
            $model->whereIn('id', $data[$key])->update(['status' => 2]);
        } else {
            if (!empty($data)) {
                $array = $data;
            } else {
                $array = $subItem->pluck('pengiriman_penjualan_id');
            }
            if (!empty($array) && 'delete' == $type) {
                $model->whereIn('id', $array)->update(['status' => 3]);
            } elseif (!empty($array) && 'available' == $type) {
                $model->whereIn('id', $array)->update(['status' => 0]);
            }
        }
    }

    /**
     * Just like
     * D E S P A C I T O
     * we also have
     * U P D A T E  P A R E N T  S T A T U S  2.
     *
     * @param eloquent object model
     * @param type of execution could be 0, 1, 2, or 3
     * @param array id. ex : [1, 4, 6]
     * @param array additional item format :
     * $array = [
     *      'model' => $model,
     *      'key_id' => 'foreign_key_id'
     * ];
     */
    public function UpdateParentStatusDua($modelTujuan, $type, $data_id = null, $additional = null)
    {
        if (!empty($additional)) {
            $data_id = $additional['model']->pluck($additional['key_id']);
        }

        return $modelTujuan->whereIn('id', $data_id)->update(['status' => $type]);
    }

    public function recalculateTransaction($model, $submodel)
    {
        try {
            $short_name = (new \ReflectionClass($model))->getShortName();
            $model_field_name = $this->fromCamelCase($short_name, '_').'_id';

            $currentSubItem = $submodel->where($model_field_name, $model->id)->get();
            // so we delete something that should does not exist
            $submodel->where($model_field_name, $model->id)->delete();

            foreach ($currentSubItem as $cbi) {
                // we got everything we need
                $changed_item = $submodel->where($model_field_name, '>', $model->id)
                ->where('produk_id', $cbi->produk_id)->get();
                foreach ($changed_item as $ci) {
                    // ambil item diatasnya
                    $all_item = $submodel->where('produk_id', $cbi->produk_id)
                    ->where('id', '<=', $ci->id)->get();

                    $qty = $all_item->sum('jumlah');
                    $harga = $all_item->sum(function ($detail) {
                        return $detail->jumlah * $detail->harga_final;
                    });

                    $total = $harga / $qty;
                    // hubungkan ke transaksi. kemudian ke saldoAwalBarang
                    $transaksi = Transaksi::where([
                        'item_id' => $ci->toArray()[$model_field_name],
                        'item_type' => get_class($model), ])
                    ->first();
                    SaldoAwalBarang::where([
                        'transaksi_id' => $transaksi->id,
                    ])->update([
                        'harga_modal' => $total,
                        'harga_terakhir' => $total,
                    ]);
                }
            }

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param KauBacaSendiri
     * @return Collection
     */

    public function makeCartLog(...$data)
    {
        $cart = CartLog::create([
            'user_id'       => Auth::user()->id,
            'produk_id'     => $data[0],
            'qty'           => $data[1],
            'harga'         => $data[2],
            'status'        => $data[3],
            'no_transaksi'  => $data[4] ?? null,
        ]);   
        return $cart;
    }

    public function deleteCartLog($produk_id, $no_transaksi, $status)
    {
        $cart = CartLog::where([
            'produk_id' => $produk_id, 
            'no_transaksi' => $no_transaksi,
            'status' => $status])->delete();
        return $cart;
    }

}
