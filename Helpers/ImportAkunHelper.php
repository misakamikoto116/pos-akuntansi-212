<?php 
namespace Helpers;
use App\Modules\Akuntansi\Models\Akun;
use App\Modules\Akuntansi\Models\DetailJurnalUmum;
use App\Modules\Akuntansi\Models\JurnalUmum;
use Carbon\Carbon;
use Helpers\CodeHelper;
use Helpers\TransaksiHelper;

class ImportAkunHelper
{
    public function insertTransaction($model, $value, $skr)
    {
        $nominal                    = empty($value->nominal) ? $value['nominal'] : $value->nominal;
        $tanggal_masuk              = empty($value->tanggal_masuk) ? $value['tanggal_masuk'] : $value->tanggal_masuk;
        $tipe_akun_id               = empty($value->tipe_akun_id) ? $value['tipe_akun_id'] : $value->tipe_akun_id;
        $money_function_transaction = empty($value->money_function_transaction) ? null : $value->money_function_transaction;
    	$code 						= new CodeHelper();
    	$transaksi_helper			= new TransaksiHelper();
    	$akun_opening 				= Akun::where('nama_akun','Opening Balance Equity')->first();
    	$nominal     		        = str_replace(['Rp ', '.00', ','], '', $nominal);
    	$tanggal_masuk            	= !empty($tanggal_masuk) ? Carbon::parse($tanggal_masuk)->format('Y-m-d H:i:s') : $skr;

        $tanggal_sekarang = date("Y-m-d H:i:s");
        if ($tanggal_masuk !== null) {

            $inputJurnalUmum = [
                'no_faktur'   => $code->generateNoFaktur($tanggal_masuk),
                'tanggal'     => $tanggal_masuk,
                'description' => 'Saldo Awal',
                'status'      => 0,
                'created_by'  => auth()->check() ? auth()->user()->id : null,
            ];

            $modelJurnalUmum =  JurnalUmum::create($inputJurnalUmum);


            if ($tipe_akun_id === "10"
                || $tipe_akun_id === "9"
                || $tipe_akun_id === "11"
                || $tipe_akun_id === "12"
                || $tipe_akun_id === "15") {

                $detail[] = [
                    'akun_id'        => $model->id,
                    'jurnal_umum_id' => $modelJurnalUmum->id,
                    'debit'          => 0,
                    'kredit'         => $nominal,
                    'created_at'     => $skr,
                    'updated_at'     => $skr,
                    'created_by'     => auth()->check() ? auth()->user()->id : null,
                ];

                $detail[] = [
                    'akun_id'        => $akun_opening->id,
                    'jurnal_umum_id' => $modelJurnalUmum->id,
                    'debit'          => $nominal,
                    'kredit'         => 0,
                    'created_at'     => $skr,
                    'updated_at'     => $skr,
                    'created_by'     => auth()->check() ? auth()->user()->id : null,
                ];

                DetailJurnalUmum::insert($detail);

                # T R A N S A K S I 

                $transaksi = [
                    'nominal'       => $nominal,
                    'tanggal'       => $tanggal_masuk,
                    'created_by'    => auth()->check() ? auth()->user()->id : null,
                ];

                $kredit             = $transaksi;
                $debit              = $transaksi;
                $kredit['akun']     = $model->id;
                $debit['akun']      = $akun_opening->id;
                $kredit['dari']     = 'Saldo Awal Akun per item '.$modelJurnalUmum->id;
                $debit['dari']      = 'Saldo Awal Akun per item balance '.$modelJurnalUmum->id;

                $transaksi_helper->ExecuteTransactionSingle('insert', $modelJurnalUmum, $debit, 1, $code->generateNoFaktur($tanggal_masuk));
                $transaksi_helper->ExecuteTransactionSingle('insert', $modelJurnalUmum, $kredit, 0,$code->generateNoFaktur($tanggal_masuk));    

            }else {

                $detail[] = [
                    'akun_id'        => $model->id,
                    'jurnal_umum_id' => $modelJurnalUmum->id,
                    'debit'          => $nominal,
                    'kredit'         => 0,
                    'created_at'     => $skr,
                    'updated_at'     => $skr,
                    'created_by'     => auth()->check() ? auth()->user()->id : null,
                ];

                $detail[] = [
                    'akun_id'        => $akun_opening->id,
                    'jurnal_umum_id' => $modelJurnalUmum->id,
                    'debit'          => 0,
                    'kredit'         => $money_function_transaction,
                    'created_at'     => $skr,
                    'updated_at'     => $skr,
                    'created_by'     => auth()->check() ? auth()->user()->id : null,
                ];

                DetailJurnalUmum::insert($detail);

                # T R A N S A K S I 

                $transaksi = [
                    'nominal'           => $nominal,
                    'tanggal'           => $tanggal_masuk,
                    'created_by'        => auth()->check() ? auth()->user()->id : null,
                ];

                $debit  = $transaksi;
                $kredit = $transaksi;
                $debit['akun']  = $model->id;
                $kredit['akun'] = $akun_opening->id;
                $debit['dari']  = 'Saldo Awal Akun per item '.$modelJurnalUmum->id;
                $kredit['dari'] = 'Saldo Awal Akun per item balance '.$modelJurnalUmum->id;

                $transaksi_helper->ExecuteTransactionSingle('insert', $modelJurnalUmum, $debit, 1, $code->generateNoFaktur($tanggal_masuk));
                $transaksi_helper->ExecuteTransactionSingle('insert', $modelJurnalUmum, $kredit, 0,$code->generateNoFaktur($tanggal_masuk));

            }
        }
    }

}