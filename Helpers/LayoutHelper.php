<?php 
namespace Helpers;

use Session;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
/** 
 * This is layout helper which is helping you on prettify your UI/UX
 * Insert anything you want here.
 * @NB : works on controller or model
 */

class LayoutHelper 
{
    public function sessionFlash($status, $pesan)
    {
        $flash = Session::flash('flash_notification', [
            'level'   => $status,
            'message' => $pesan
        ]);
        return $flash;
    }

    public function formLabel($status, $string){
		return "<span class='label label-".$status."'>".$string."</span>";
    }
    
    public function batalkanProses($html){
        Session::flash('error_message', $html); 
        Session::flash('icon', 'error'); 
 
		return false;
    }
    
    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function generateTahun()
    {
        $data = [];
        $i = date('Y',strtotime('+50 years'));
        for($i; $i < date('Y')+50; $i++){
            $data[$i] = $i;
        }
        return collect($data);
    }
}
