<?php
/**
 * Created by PhpStorm.
 * User: efendihariyadi
 * Date: 15/11/18
 * Time: 17.20
 */

namespace Helpers;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
class PenjualanApi
{
    protected $pos_api_url;
    protected $pos_clientId;
    protected $pos_client_screet;
    protected $pos_client_grant_type;
    protected $pos_username;
    protected $pos_userpass;

    private $h2_server = 'https://javah2h.com/';
    private $user_id = 'H1030';
    private $pin = '6265';
    private $key = '54aef346126bab0d1067c13d3b1c4157';
    private $secreet = 'ab8adc058db0a7a8418ba70a58e79a91ffa81d13342a1feb7fef01226af5563a';
    // private $defaultHeader ;

    public function __construct()
    {
        $this->pos_api_url = env('API_KOP', null);
        $this->pos_clientId = env('API_KOP_CLIENT_ID', null);
        $this->pos_client_screet = env('API_KOP_CLIENT_SCREET', null);;
        $this->pos_username = env('API_KOP_USERNAME', null);
        $this->pos_userpass = env('API_KOP_PASSWORD', null);
        $this->pos_client_grant_type = env('API_KOP_GRANT_TYPE', null);;
    }

    private function _getToken()
    {
        $client = new Client;
        $token = '';
        try {
            $response = $client->post($this->pos_api_url .'/oauth/token', [
                'form_params' => [
                    'client_id' => $this->pos_clientId,
                    'client_secret' => $this->pos_client_screet,
                    'grant_type' => $this->pos_client_grant_type,
                    'username' => $this->pos_username,
                    'password' => $this->pos_userpass,
                    'scope' => '*',
                ]
            ]);

            $resBody = json_decode( (string) $response->getBody() );
            $token = $resBody->access_token;
        } catch (BadResponseException $e) {
            echo $e->getMessage();
        }
        return $token;
    }

    /**
     * @param $data array
     * @return mixed
     */
    public function sendPenjualanApi($data)
    {
        $client = new Client;
        try{
            $response = $client->post($this->pos_api_url.'api/penjualan', [
                'headers' => [
                    //'Authorization' => 'Bearer '.$this->_getToken(),
                    'Accept' => 'application/json'
                ],
                'form_params' => [
                    'tanggal' => $data['tanggal'],
                    'total' => $data['total'],
                    'jml_umkm' => $data['jml_umkm'],
                    'jml_non' => $data['jml_non'],
                    'nomor_anggota' => $data['nomor_anggota'],
                    'anggota_id' => $data['anggota_id'],
                ]
            ]);
            $res = json_decode( (string) $response->getBody() );
        }catch (BadResponseException $e){
            echo $e->getMessage();
        }
        return true;
    }
    public function sendTransaksiToApi($data){
        $data['tanggal'];
        $data['total'];
        $data['jml_umkm'];
        $data['jml_non'];
        $data['nomor_anggota'];
        $data['anggota_id'];
        $data['created_at'];
    }

    public function topUpPulsaRegular($code, $no_tujuan, $trx_id)
    {
        try {
            $client = new \GuzzleHttp\Client(['base_uri'=> $this->h2_server]);
            $request = $client->post('api/connect/', [
                'headers' => [
                    'h2h-key' => $this->key,
                    'h2h-secret' => $this->secreet,
                    'h2h-userid' => $this->user_id
                ],
                'form_params' => [
                    'inquiry' => 'I', // konstan
                    'code' => $code, // kode produk
                    'phone' => $no_tujuan,// nohp pembeli
                    'trxid_api' => $trx_id, // Trxid / Reffid dari sisi client
                ]
            ]);
            $status = $request->getStatusCode();
            $body = $request->getBody();
            if($status == 200){
                return json_decode($body, true);
            }else{
                return json_decode($body);
            }
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            return false;
        }
    }

    // public function topUpPulsaRegularTest($code, $no_tujuan, $trx_id)
    // {
    //     return response()->json(['test' => 'masuk']);
    //     try {
    //         $client = new \GuzzleHttp\Client(['base_uri'=> 'localhost:8000/']);
    //         $request = $client->post('beli-pulsa-proses', [
    //             'headers' => [
    //                 'h2h-key' => $this->key,
    //                 'h2h-secret' => $this->secreet,
    //                 'h2h-userid' => $this->user_id
    //             ],
    //             'form_params' => [
    //                 'inquiry' => 'I', // konstan
    //                 'code' => $code, // kode produk
    //                 'phone' => $no_tujuan,// nohp pembeli
    //                 'trxid_api' => $trx_id, // Trxid / Reffid dari sisi client
    //             ]
    //         ]);
    //         $status = $request->getStatusCode();
    //         $body = $request->getBody();
    //         if($status == 200){
    //             return json_decode($body, true);
    //         }else{
    //             return json_decode($body);
    //         }
    //     } catch (\GuzzleHttp\Exception\ConnectException $e) {
    //         return false;
    //     }
    // }
}